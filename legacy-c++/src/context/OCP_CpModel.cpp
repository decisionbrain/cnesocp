/*
 * $Id: OCP_CpModel.cpp 947 2011-01-07 14:43:53Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * VERSION : 2-0 : FA : artf660903 : 30/11/2010 : Calcul du planning apr�s r�servation manuelle de cr�neaux NOK (https://coconet2.capgemini.com/sf/go/artf660903/")
 * VERSION : 2-0 : FA : artf665024 : 03/11/2010 : Am�lioration du moteur de calcul  (https://coconet2.capgemini.com/sf/go/artf665024/")
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilctrace.h>

#include <OcpPeriod.h>
#include <PHRDate.h>
#include <resources/OcpSatellite.h>
#include <ConfAccel.h>
#include <boost/format.hpp>
#include <constraints/OcpConstraintSatType1.h>
#include <constraints/OcpConstraintSatType2.h>
#include <constraints/OcpConstraintSatType3.h>
#include <constraints/OcpConstraintSatType4.h>
#include <constraints/OcpConstraintSatType5.h>
#include <constraints/OcpConstraintSatType6.h>
#include <constraints/OcpConstraintSatType7.h>
#include <constraints/OcpConstraintSatType8.h>
#include <constraints/OcpConstraintStaType1.h>
#include <constraints/OcpConstraintStaType2.h>
#include <constraints/OcpConstraintStaType3.h>
#include <constraints/OcpConstraintStaType4.h>
#include <constraints/OcpConstraintStaType5.h>
#include <constraints/OcpConstraintStaType6.h>
#include <constraints/OCP_IloCpSatelliteConstraint3.h>


#include "OCP_CpHomere.h"
#include "context/OCP_CpModel.h"
#include "planif/OCP_CpSearch.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpMaintenanceSlot.h"
#include "context/OCP_CpObject.h"
#include "resources/OCP_CpSatellite.h"
#include "resources/OCP_CpStation.h"
#include "constraints/OCP_CpInternalConstraint.h"
#include "OCP_CpManager.h"


#include "constraints/OCP_CpStationConstraint1.h"
#include "constraints/OCP_CpStationConstraint2.h"
#include "constraints/OCP_CpStationConstraint3.h"
#include "constraints/OCP_CpStationConstraint4.h"
#include "constraints/OCP_CpStationConstraint5.h"
#include "constraints/OCP_CpStationConstraint6.h"

#include "constraints/OCP_CpSatelliteConstraint1.h"
#include "constraints/OCP_CpSatelliteConstraint2.h"
#include "constraints/OCP_CpSatelliteConstraint3.h"
#include "constraints/OCP_CpSatelliteConstraint4.h"
#include "constraints/OCP_CpSatelliteConstraint5.h"
#include "constraints/OCP_CpSatelliteConstraint6.h"
#include "constraints/OCP_CpSatelliteConstraint7.h"
#include "constraints/OCP_CpSatelliteConstraint8.h"
#include "constraints/OCP_CpObjectiveOverSlots.h"


#include <sstream>
#include <iostream>


using namespace std;
using namespace boost;
using namespace ocp::solver;
using namespace ocp::commons;
using namespace ocp::commons::io;



static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpModel);


OCP_CpModel::OCP_CpModel()
{
    /**
    * permet d'avoir proprement une implémentation nulle de _env
    */
    _env = IloEnv();
    _env.end();
    _allSatellites = SatelliteMap();
    _allStations = StationMap();
    _fixedSlots = ConfAccel::getInstance()->getFixedSlotsId();//lu depuis listener.conf
}

OCP_CpModel::~OCP_CpModel()
{
    this->free();
}

/**
* libération de l'ancien environnement ILOG SOLVER et des maps de satellites et stations
*/
void OCP_CpModel::free()
{
    _allSatellites.clear();
    _allStations.clear();
    _satelliteScopes.clear();
    _cpConstraints.clear();
    _rankedSearchCriterias.clear();

    if(_env.getImpl() != NULL)
    {
        _env.end();
    }
    _setVarsToTrace = IloAnySetVarArray();
    _allCardinalitySetVars = IloAnySetVarArray();
    _allCardinalityVars = IloIntVarArray();
    _sat3IloConstraints = IloConstraintArray();
    //_allCardinalitySlack = IloIntExprArray();

}


/**
* Traitement en cas d'interruption utilisateur.
*/
void OCP_CpModel::onSignalHandler()
{
    this->free();
}


/**
* Préparation du nouvel environnement ILOG Solver après libération de l'ancien et des maps de satellites et stations
* @param modeInstanciationDisplay : NOMINAL/REDUIT/MINIMAL , permet de renseigner la chaîne commentaire en post-optim sur les
* @param beginDay jour de la fenêtre de calcul
* @param solutionDayBefore : flag indiquant qu'il y a eu un calcul avec succès la veille
* créneaux sauvegardés
*/
void OCP_CpModel::prepare(std::string modeInstanciationDisplay,PHRDate& beginDay, bool solutionDayBefore)
{
    this->free();
    _env    = IloEnv();
    _model  = IloModel(_env);
    _solver = IloSolver();

    std::ostringstream msg;
    const boost::posix_time::ptime now= boost::posix_time::second_clock::local_time();
    boost::posix_time::time_facet*const f=new boost::posix_time::time_facet("%d-%b-%Y %H:%M:%S");
    msg.imbue(std::locale(msg.getloc(),f));
    msg << now;

    boost::format commentStr = boost::format("Réservé le [%1$s] par la calcul de la journée du [%2$s] en mode [%3$s]")
        % msg.str() % beginDay.getStringValue() % modeInstanciationDisplay;

   _comment = commentStr.str();

   /*
    * effacement de la trace des branchements
    */
   _branchingIds.clear();
   _branchingTypes.clear();
   
   /*
    * mémorisation du calcul précédent
    */
   _solutionDayBefore = solutionDayBefore;

   /*
    * lecture et tri des critères de priorité depuis le fichier de conf
    */
   initRankSearchCriterias();
}

/**
  * lecture des critères de priorité depuis le fichier de conf. on trie ces critères du plus important
  * au moins important et on range ces critères dans le tableau _rankedSearchCriterias
  */
 void OCP_CpModel::initRankSearchCriterias()
 {
     int rankPrio1 = ConfAccel::getInstance()->getPrioSatBeforeMaintenance();
     int rankPrio2 = ConfAccel::getInstance()->getPrioSatMaintenanceDuration();
     int rankPrio3 = ConfAccel::getInstance()->getPrioLongestSat1() ;
     int rankPrio4 = ConfAccel::getInstance()->getPrioSatisfyMinPass();
     int rankPrio5 = ConfAccel::getInstance()->getPrioSat2Max();
     int rankPrio6 = ConfAccel::getInstance()->getPrioSat();
     int rankPrio7 = ConfAccel::getInstance()->getPrioSta();
     int rankPrio8 = ConfAccel::getInstance()->getPrioDeterministic();
     int rankPrio9 = ConfAccel::getInstance()->getPrioSatisfySat3MinDur();

     boost::format msg = boost::format( _("Hiérarchie des critères de piorité (0 : non pris en compte, puis le plus petit niveau est le plus prioritaire) ") );
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());

     msg = boost::format( _("Critère \"Créneau satellite avant créneau maintenance\"\t\t%1$d  ") ) % rankPrio1;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Entre deux créneaux de maintenance, le plus court\"\t\t%1$d  ") ) % rankPrio2;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Créneau le plus long pour une SAT1 le notifiant\"\t\t%1$d  ") ) % rankPrio3;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Satisfaction des minimums de passage sur les SAT1\"\t\t%1$d  ") ) % rankPrio4;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Ecartement maximal compatible avec SAT2MAX\"\t\t%1$d  ") ) % rankPrio5;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Priorité aggrégée\"\t\t%1$d  ") ) % rankPrio6;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Priorité stations\"\t\t%1$d  ") ) % rankPrio7;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Déterminisme\"\t\t%1$d  ") ) % rankPrio8;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
     msg = boost::format( _("Critère \"Satisfaction des minimums de durée sur les SAT3\"\t\t%1$d  ") ) % rankPrio9;
     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());


     int rankPriosInit[] = {rankPrio1, rankPrio2, rankPrio3, rankPrio4 , rankPrio5,rankPrio6, rankPrio7, rankPrio8, rankPrio9};

     std::vector<OCP_CpTimeSlot*> rankInfos;//liste des paires "rank, numéro de critère"
     int size = (sizeof rankPriosInit)/(sizeof rankPriosInit[0]);
     for(int i=0;i<size;i++)
     {
         int rankPrio = rankPriosInit[i];
         if (rankPrio>0)
         {
             OCP_CpTimeSlot* ts = new OCP_CpTimeSlot(rankPrio,i+1);
             rankInfos.push_back(ts);
         }
     }
     std::sort(rankInfos.begin(), rankInfos.end(), OCP_CpTimeSlot::slotsBeginSortPredicate);

     /*
      * le critère : choisir un créneau non assigné quand il n'y a pas encore de meilleur créneau choisi
      * est systématiquement prioritaire
      */
     _rankedSearchCriterias.push_back(0);

     for(std::vector<OCP_CpTimeSlot*>::iterator it = rankInfos.begin(); it != rankInfos.end() ; it++)
     {
         OCP_CpTimeSlot* ts = (*it);
         _rankedSearchCriterias.push_back(ts->getEnd());
         delete ts;
     }


 }

/**
 * création des structures permettant de collecter les différentes variables ensemblistes et leurs variables de
 * cardinalité sur chacune des contraintes SAT1 instanciées
 */
void OCP_CpModel::prepareSat1CardinalityInfos()
{
    _allCardinalitySetVars = IloAnySetVarArray(_env);
    _allCardinalityVars = IloIntVarArray(_env);
    _sat3IloConstraints = IloConstraintArray(_env);
    //_allCardinalitySlack = IloIntExprArray(_env);
}

/**
 * positionne le flag réversible liées à la violation des bornes min des contraintes SAT1
 */
void OCP_CpModel::initReversibleFlags()
{
    if (ConfAccel::getInstance()->getPrioSatisfyMinPass()>0)
        _hasSlackMinPass.setValue(_solver , IlcTrue);
    else
        _hasSlackMinPass.setValue(_solver , IlcFalse);

    if (ConfAccel::getInstance()->getPrioSatisfySat3MinDur()>0)
        _hasSlackSat3MinDur.setValue(_solver , IlcTrue);
    else
        _hasSlackSat3MinDur.setValue(_solver , IlcFalse);

}

/**
 * pattern de création du modèle de résolution sur une journée.
 * @return une nouvelle instance à chaque nouvelle fenêtre de calcul, sinon l'instance en cours
 */
OCP_CpModel* OCP_CpModel::getInstance()
{
    if(! instanceFlag)
    {
        _cpModel = new OCP_CpModel();
        instanceFlag = true;
        return _cpModel;
    }
    else
    {
        return _cpModel;
    }
}

/**
* Map des objets OCP_CpSatellite et OCP_CpStation à partir des paramètres de lancement
* création de leurs contraintes
* @param optimScope : fenêtre courante de calcul
* @param parameters : paramètres de lancement
*/
void OCP_CpModel::mapSatellitesAndStations(OCP_CpTimeSlotPtr optimScope,OcpParametersPtr parameters)
{
    mapSatellites(optimScope,parameters);
    mapStations(parameters);
}

/**
* Map des objets OCP_CpSatellite à partir des paramètres de lancement
* création de leurs contraintes
* @param optimScope : fenêtre courante de calcul
* @param parameters : paramètres de lancement
*/
void OCP_CpModel::mapSatellites(OCP_CpTimeSlotPtr optimScope,OcpParametersPtr parameters)
{
    static const std::string pseudoClazz = _Clazz + "::mapSatellites";

    OcpSatParams mapSatellite = parameters->getSetType4EachSatellite();
    /*
     * artf 646064 : chargement de tous les satellites
     */
    OcpContextPtr context = OcpXml::getInstance()->getContext();
    MapIdSatellite allsatellites = context->getMapSatellite();


    OcpSatellitePtr aSat;
    OCP_CpSatellitePtr satellite;

    //for(OcpSatParams::iterator it = mapSatellite.begin(); it!=mapSatellite.end(); it++)
    for(MapIdSatellite::iterator it = allsatellites.begin(); it!=allsatellites.end(); it++)
    {
        aSat      = (OcpSatellitePtr) ((*it).second);
        bool scheduled = (mapSatellite.find(aSat) != mapSatellite.end());

        if (!OCP_CpModel::loadAllRessources() && !scheduled) continue;//TODO tmp

        satellite = OCP_CpSatellitePtr ( new OCP_CpSatellite(aSat,scheduled) );



        boost::format msg = boost::format( _("Nouveau satellite %1$s scheduled %2$d: ") ) % aSat->getName() % scheduled;

        if (satellite->isGeoStationary())
        {
            msg = boost::format( _("%1$s (géostationnaire) ") ) % msg.str();
        }
        else
        {
            msg = boost::format( _("%1$s (défilant) ") ) % msg.str();
        }
        OcpLogManager::getInstance()->debug(pseudoClazz, msg.str());
        _allSatellites.insert(SatelliteMap::value_type(aSat, satellite));

        /*
         * initialisation de la portée de chargement des slots à la fenêtre courante de calcul
         */
        OCP_CpTimeSlotPtr satScope( new OCP_CpTimeSlot( *optimScope.get() ) ); //copie car modification
        _satelliteScopes.insert(std::map<OCP_CpSatellitePtr, OCP_CpTimeSlotPtr>::value_type(satellite, satScope));

    }
}

/**
* Map des objets OCP_CpStation à partir des paramètres de lancement
* création de leurs contraintes
* @param parameters : paramètres de lancement
*/
void OCP_CpModel::mapStations(OcpParametersPtr parameters)
{
    static const std::string pseudoClazz = _Clazz + "::mapStations";
    OcpStaParams mapStation = parameters->getSetType4EachStation();
    /*
     * artf 646064 : chargement de tous les satellites
     */
    OcpContextPtr context = OcpXml::getInstance()->getContext();
    MapIdStation allstations = context->getMapStation();

    OcpStationPtr    aStation;
    OCP_CpStationPtr station;


    //for(OcpStaParams::iterator it = mapStation.begin();it!=mapStation.end();it++)
    for(MapIdStation::iterator it = allstations.begin();it!=allstations.end();it++)
    {
        aStation = (OcpStationPtr) ((*it).second);
        bool scheduled = (mapStation.find(aStation) != mapStation.end());
        if (!OCP_CpModel::loadAllRessources() && !scheduled) continue;//TODO tmp
        station  = getStation(aStation);

        if(station == NULL)
        {
            station = OCP_CpStationPtr( new OCP_CpStation(aStation, scheduled) );
            _allStations.insert(StationMap::value_type(aStation, station));

        }
    }
}


/**
* Récupération de l'objet OCP_CpSatellite
* @param aSat satellite origine
* @return l'objet OCP_CpSatellite
*/
OCP_CpSatellitePtr OCP_CpModel::getSatellite(OcpSatellitePtr aSat)
{
    SatelliteMap::iterator iter;
    iter = _allSatellites.find(aSat);

    if(iter == _allSatellites.end())
    {
        boost::format msg = boost::format( _("OCP_CpModel::getSatellite Satellite non reconnu  %1$s  ") ) % aSat->getName();
        throw OCP_XRoot(msg.str());
    }
    else
    {
        return (*iter).second;
    }
}

/**
 * Récupération de l'objet OCP_CpStation
 * @param aStation : station origine
 * @return l'objet OCP_CpStation
 */

OCP_CpStationPtr OCP_CpModel::getStation(OcpStationPtr aStation)
{
    StationMap::iterator iter;
    iter = _allStations.find(aStation);

    if(iter == _allStations.end())
    {
        return OCP_CpStationPtr();
    }
    else
    {
        return (*iter).second;
    }
}

/**
 * parcourt les contrainte satellites et modifie en conséquence l'intervalle de chargement des slots du satellite
 */
void OCP_CpModel::updateSatelliteScope()
{
    OCP_CpConstraintPtr ctr;
    OCP_CpSatellitePtr  satellite;
    OCP_CpTimeSlotPtr   satScope;
    OCP_CpTimeSlotPtr   extendedScope;
    OCP_CpSatelliteBaseConstraintPtr satCtr;
    std::map<OCP_CpSatellitePtr, OCP_CpTimeSlotPtr>::iterator itMap;

    for(VectorOCP_CpConstraint::iterator it = _cpConstraints.begin() ; it != _cpConstraints.end() ; it++)
     {
         ctr = (*it);

         if ( ctr->isSatelliteConstraint() )
         {
             satCtr    = boost::dynamic_pointer_cast<OCP_CpSatelliteBaseConstraint>(ctr);
             satellite = satCtr->getSatellite();

             itMap = _satelliteScopes.find(satellite);

             satScope      = (*itMap).second;
             extendedScope = satCtr->getExtendedScope();

             satScope->enlarge( *extendedScope.get() );
         }
     }
}


/**
* Instanciation des contraintes OCP en vue de créer les contraintes ILOG
* @param optimScope
*/
void OCP_CpModel::instantiateOCPConstraints(OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::instantiateOCPConstraints";
    int nbCtr = _cpConstraints.size();//cardinalité de toutes les contraintes
    int nbCtrWithInstance = 0;
    int nbCtrImpl = 0;//cardinalité des contraintes avec implémentation ILOG SOLVER

    boost::format msg = boost::format( _("Lancement de l'instanciation de %1$i contraintes OCP candidates ") ) % nbCtr;
    OcpLogManager::getInstance()->info(pseudoClazz, msg);

    OCP_CpConstraintPtr ctr;
    VectorOCP_CpConstraint::iterator it;

    /*
     * ILOG SUPPORT : Numéro de demande de service  10251 999 706
     * artf626147
     * on met les contraintes de cardinalité en queue de liste
     * ce hack pourra être enlevé une fois enrichie les méthodes "propagate()" des contraintes spécifiques OCP_IloCpXXXConstraint
     */
    VectorOCP_CpConstraint hackIloCard;
    VectorOCP_CpConstraint hackAllConstraints;
    for(it = _cpConstraints.begin() ; it != _cpConstraints.end() ; it++)
    {
        ctr = (*it);
        if (ctr->isSAT1() || ctr->isSTA2() || ctr->isSTA3() || ctr->isSTA4() )
            hackIloCard.push_back(ctr);
        else
            hackAllConstraints.push_back(ctr);
    }
    for(it = hackIloCard.begin() ; it != hackIloCard.end() ; it++)
    {
        ctr = (*it);
        hackAllConstraints.push_back(ctr);
    }

    for(it = hackAllConstraints.begin() ; it != hackAllConstraints.end() ; it++)
    {
        ctr = (*it);

        if(ctr->hasInstance())
        {
            msg = boost::format( _("instantiate contrainte %1$s ") ) % ctr->getName();
            OcpLogManager::getInstance()->debug(pseudoClazz, msg);

            ctr->instantiate(optimScope);

            //msg = boost::format( _("Fin instantiate contrainte ") );
            //OcpLogManager::getInstance()->debug(pseudoClazz, msg);
            nbCtrWithInstance++;
        }

        if (ctr->getIloConstraint().getImpl() != NULL)
        {
            nbCtrImpl++;
        }
    }

    msg = boost::format( _("%1$i contraintes instanciées parmi %2$i, dont %3$i ont une implémentation ILOG SOLVER ") )
            % nbCtrWithInstance % nbCtr % nbCtrImpl ;
    OcpLogManager::getInstance()->info(pseudoClazz, msg);
}

/**
  * pour chaque satellite, on partitionne les slots candidats (ou une partie d'entre eux) en contraintes SAT1
  * (une partie d'entre elles de manière à ce qu'il n'y ait pas de candidat commun entre deux SAT1)
  * de manière à pouvoir poser ultérieurement une contrainte de stable max sur l'ensemble des partitions
  * multi-satellites en sommant les bornes inférieures des contraintes SAT1
  */
 void OCP_CpModel::partitionStableCandidates()
 {
     SatelliteMap::iterator iterSatellite;
     for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
     {
         OCP_CpSatellitePtr satellite = (*iterSatellite).second;
         satellite->partitionStable();
     }
 }

/**
 * suppression des slots superflus (artf618698)
 * @param parameters paramètres d'entrée donnant accès aux satellites
 * @return renvoie le nombre de créneaux supprimés
 */
int OCP_CpModel::removeUselessSlots(OcpParametersPtr parameters)
{
    static const std::string pseudoClazz = _Clazz + "::removeUselessSlots";
    OcpSatellitePtr sat;
    OCP_CpSatellitePtr satellite;
    OcpStationPtr aStation;
    OCP_CpStationPtr station;

    int nbRemovedAllSat = 0;//slots de tracking
    int nbRemovedAllSta = 0;//slots de maintenance


    /**
     * parcours de tous les satellites : renseigner en premier les défilants de façon à générer pour chaque station les
     * dates sur lesquels se découperont les slots géostationnaires ou à visibilité partielle possible (flag SAT1)
     */
     OcpSatParams mapSatellite = parameters->getSetType4EachSatellite();
     for(OcpSatParams::iterator it = mapSatellite.begin();it!=mapSatellite.end();it++)
     {
         sat = (OcpSatellitePtr) ((*it).first);
         satellite = getSatellite(sat);
         int nbRemovedSat = satellite->removeUselessSlots(satellite->getTrackingSlots());
         nbRemovedAllSat += nbRemovedSat;
     }
     boost::format msg = boost::format( _("%1$d slots de tracking superflus supprimés au total " ) ) % nbRemovedAllSat;
     OcpLogManager::getInstance()->debug(pseudoClazz, msg );


     /*
      * notification pour les stations
      */
     OcpStaParams mapStation = parameters->getSetType4EachStation();
     for(OcpStaParams::iterator it = mapStation.begin();it!=mapStation.end();it++)
     {
         aStation = (OcpStationPtr) ((*it).first);
         station = getStation(aStation);
         int nbRemovedSta = station->removeUselessSlots(station->getMaintenanceSlots());
         nbRemovedAllSta += nbRemovedSta;

     }
     msg = boost::format( _("%1$d slots de maintenance superflus supprimés au total " ) ) % nbRemovedAllSta;
     OcpLogManager::getInstance()->debug(pseudoClazz, msg );

     return nbRemovedAllSat+nbRemovedAllSta;
}

/**
 * chargement de tous les slots satellite par satellite
 * @param optimScope : fenêtre jour de calcul courante
 * @parameters paramètres d'entrée donnant accès aux satellites
 */
void OCP_CpModel::parseAllSlots(OCP_CpTimeSlotPtr optimScope,OcpParametersPtr parameters)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(parameters);
    static const std::string pseudoClazz = _Clazz + "::parseAllSlots";
    OCP_CpSatellitePtr satellite;


    /**
     * parcours de tous les satellites : renseigner en premier les défilants de façon à générer pour chaque station les
     * dates sur lesquels se découperont les slots géostationnaires ou à visibilité partielle possible (flag SAT1)
     */
    //OcpSatParams mapSatellite = parameters->getSetType4EachSatellite();
    //OcpContextPtr context = OcpXml::getInstance()->getContext();
    //MapIdSatellite allsatellites = context->getMapSatellite();

     //for(OcpSatParams::iterator it = mapSatellite.begin();it!=mapSatellite.end();it++)
    //for(MapIdSatellite::iterator it = allsatellites.begin(); it!=allsatellites.end(); it++)
    SatelliteMap::iterator iterSatellite;
    for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
     {
        OCP_CpSatellitePtr satellite = (*iterSatellite).second;
        if(OCP_CpModel::_loadAllResources || satellite->isScheduled()) //TODO tmp
             parseSlot(satellite , optimScope);
     }
}

/**
 * split des créneaux vus dans des contraintes SAT7 ou SAT8
 * @parameters paramètres d'entrée donnant accès aux satellites
 */
void OCP_CpModel::splitAllSlots(OcpParametersPtr parameters)
{
    static const std::string pseudoClazz = _Clazz + "::splitAllSlots";
    OcpSatellitePtr sat;
    OCP_CpSatellitePtr satellite;
    std::map<OCP_CpSatellitePtr, OCP_CpTimeSlotPtr>::iterator itMap;

    OCP_CpTimeSlotPtr satScope;
    OCP_CpTrackingSlotPtr aSplit;
    OCP_CpTrackingSlot* trackingSlot;


    /*
      * stockage temporaire des splits slots pour éviter un bouclage par rajout aux ressources
      */
     std::vector <  OCP_CpTrackingSlot* > splitCandidates;

     /*
       * split des créneaux satellites des géostationnaires et de ceux qui ont des contraintes SAT1 avec visibilité partielle
       * * créer la liste globale de tous les nouveaux candidats splittés PUIS les ajouter ensuite aux trackingSlots des satellites
       * et stations. Si on ne le fait pas ainsi, il y a création et ajout dynamique de slots sur les ressources et récursion
       * c'est le rôle du tableau splitCandidates ci-dessous
       */
     OcpSatParams mapSatellite = parameters->getSetType4EachSatellite();

     for(OcpSatParams::iterator it = mapSatellite.begin(); it!=mapSatellite.end(); ++it)
     {
         sat = (OcpSatellitePtr) ((*it).first);
         satellite = getSatellite(sat);
         if( satellite == NULL)
         {
             boost::format msg = boost::format( _("satellite %1$s non trouvé dans OCP_CpModel ") )
                     % sat->getName();
             OcpLogManager::getInstance()->error(pseudoClazz, msg );
             throw OCP_XRoot(msg.str());
         }

         itMap =  _satelliteScopes.find(satellite);

         satScope = (*itMap).second; //portée jour éventuellement étendue par les contraintes

         satellite->generateSplitSlots(splitCandidates);

     }

     /*
      * rajout de tous les slots splittés aux ressources correspondantes
      */
     boost::format msg = boost::format( _("Ajout aux ressources des %1$i nouveaux créneaux splittés") )  % splitCandidates.size();
     OcpLogManager::getInstance()->debug(pseudoClazz, msg );

     for(std::vector <  OCP_CpTrackingSlot* >::iterator it = splitCandidates.begin() ; it != splitCandidates.end() ; it++)
     {
         trackingSlot = (*it);
         trackingSlot->getSatellite()->addTrackingSlot(trackingSlot);
         trackingSlot->getStation()->addTrackingSlot(trackingSlot);
     }
}

/**
 * parcours des zones de tracking de chaque station sur le jour de calcul (avec plages à cheval possible) et notification des dates
 * de début et fin de tracking possible pour calcul ultérieur de split des créneaux SAT1-visibilité partielle
 * @param optimScope : fenêtre de calcul courante
 * @param parameters : paramètres d'entrée pour lecture des stations concernées
 */
void OCP_CpModel::notifyStationTrackingZones(OCP_CpTimeSlotPtr optimScope,OcpParametersPtr parameters)
{
    /**
    * parcours des stations
    */
    OcpStaParams mapStation = parameters->getSetType4EachStation();

    for(OcpStaParams::iterator it = mapStation.begin();it!=mapStation.end();it++)
    {
        OcpStationPtr aStation = (OcpStationPtr) ((*it).first);
        OCP_CpStationPtr station = getStation(aStation);
        station->notifyTrackingZones(optimScope);
    }

}


/**
 * chargement de tous les slots d'un satellite, avec élargissement potentiel de la fenêtre de jour autour de la fenêtre de calcul
 * pour les satellites ayant des contraintes l'imposant. Dans ce cas, les slots demandés commençant avant le début de la fenêtre de calcul
 * seront filtrés par le fait qu'ils sont reservés
 * @param satellite : satellite sur lequel on charge les slots
 * @param optimScope : la fenêtre de calcul jour
 */
void OCP_CpModel::parseSlot(OCP_CpSatellitePtr satellite , OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::parseSlot";
    PHRDate beginScope(optimScope->getBegin());
    PHRDate endScope(optimScope->getEnd());


    if (satellite->getOcpReserveConstraint().size() == 0)//aucune contrainte, aucune réservation artf605882
    {
        format logMsg = format( _("Aucune contrainte OCP de type CSatType1, CSatType7 ou CSatType8 pour le satellite %1$s sur %2$s %3$s") )
                    % satellite->getName() % beginScope.getStringValue() % endScope.getStringValue();
        OcpLogManager::getInstance()->warning(pseudoClazz, logMsg.str() );
    }

    if (!satellite->hasSat4Constraint() && satellite->getOcpReserveConstraint().size()>0 )//aucune contrainte SAT4 : pas de création de créneau sur ce satellite
    {
        format logMsg = format( _("Aucune contrainte OCP de type CSatType4 pour le satellite %1$s sur %2$s %3$s alors qu'il existe des contraintes réservantes : les aos-los ne peuvent être définis") )
                            % satellite->getName() % beginScope.getStringValue() % endScope.getStringValue();
        throw OCP_XRoot(logMsg.str());
    }


    OcpContextPtr context = OcpXml::getInstance()->getContext();


    std::map<OCP_CpSatellitePtr, OCP_CpTimeSlotPtr>::iterator it =  _satelliteScopes.find(satellite);
    OCP_CpTimeSlotPtr satScope = (*it).second;//portée jour éventuellement étendue par les contraintes
    PHRDate beginExtendedDay(satScope->getBegin());
    PHRDate endExtendedDay(satScope->getEnd());



    VectorSatelliteSlot listSlot ;
    try
    {
        listSlot = context->getSatelliteSlotsByPeriod(beginExtendedDay, endExtendedDay, satellite->getOrigin());

    }
    catch(OcpDayNotFoundException& err)
    {
        format logMsg = format( _("Aucun slot satellite lu pour le satellite %1$s sur la fenêtre étendue %2$s %3$s") )
                    % satellite->getName() % beginExtendedDay.getStringValue() % endExtendedDay.getStringValue();
        OcpLogManager::getInstance()->warning(pseudoClazz, logMsg );
    }


    format logMsg = format( _("Lu '%1$i' slots satellites pour le satellite %2$s sur la fenêtre étendue %3$s %4$s") )
                                        % listSlot.size() % satellite->getName() % beginExtendedDay.getStringValue() % endExtendedDay.getStringValue();
    OcpLogManager::getInstance()->info(pseudoClazz, logMsg.str() );

    int nbFiltered = 0;
    for(VectorSatelliteSlot::iterator iterSatSlot = listSlot.begin() ; iterSatSlot != listSlot.end() ; iterSatSlot++)
    {
        OcpSatelliteSlotPtr aSatSlot = *iterSatSlot;
        if (!parseOneSlot(aSatSlot,optimScope))
        {
            nbFiltered++;
        }

    }


    int checkSatTrackings = satellite->getTrackingSlots().getSize();

    OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("%1$d passages filtrés , reste %2$d (%3$d tracking slots) ") )
            % nbFiltered % (listSlot.size() - nbFiltered)  % checkSatTrackings );

    //Waechter : fill in factor. non zeros in factor Iterative Linear Solvers

}



/**
 * création d'un tracking Slot
 * @param aSatSlot créneau satellite origine
 * @param optimScope fenêtre de calcul courante
 * @return vrai ssi on a pu créer un tracking slot
 */
bool OCP_CpModel::parseOneSlot(OcpSatelliteSlotPtr aSatSlot,OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::parseOneSlot";
    OCP_CpSatellitePtr satellite = getSatellite(aSatSlot->getSatellite());
    OCP_CpStationPtr station = getStation( aSatSlot->getStation() );

    bool trackable = false;
    /*
     * filtrage par les stations entrées en paramètres
     */
    if (station != NULL)
    {
        OcpPeriodPtr satSlotPeriod = aSatSlot->getStartEnd();
        OCP_CpTimeSlot ocpSatSlot (satSlotPeriod );

        /*
         * filtrer les slots par les périodes d'ouverture pour suivi satellite de la station
         */

        bool beginsBeforeScope = ocpSatSlot.getBegin() < optimScope->getBegin() ;
        bool trackableByStation = station->isTrackable(ocpSatSlot);
        bool isReserved = aSatSlot->getBookingState()  == SlotBookingStates::RESERVED
                || aSatSlot->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
        bool available = aSatSlot->getBookingState() == SlotBookingStates::AVAILABLE;
        /*
         * on suppose que les slots reservés et commençant avant avant la fenêtre de calcul sont dans une plage valide
         * pour le suivi par la station. Pour les autres on applique la vérification
         */

        if (beginsBeforeScope)
        {
            trackable = isReserved;//on ne garde que les reservés avant le début de la fenêtre
        }
        else
        {
            trackable = (isReserved || available) &&  trackableByStation;
        }
        if(!trackable)
        {
            OcpLogManager::getInstance()->warning(pseudoClazz, boost::format( _("%1$s sera ignoré car en dehors des zones de suivi de sa station ou non disponible ou commençant avant la fenêtre de calcul mais non reservé") )
                    % OCP_CpTrackingSlot::getOriginFullName(aSatSlot)  );
            if(isReserved && !beginsBeforeScope && !trackableByStation)
            {
                boost::format msg = boost::format( _("%1$s est à la fois réservé mais ignoré ") )
                        % OCP_CpTrackingSlot::getOriginFullName(aSatSlot);
                OcpLogManager::getInstance()->error(pseudoClazz, msg );
                throw OCP_XRoot(msg.str());
            }
        }
        /*
         * quand le satellite ou la station n'est pas à scheduler, ne garder qu'un créneau reservé
         */
        else if(OCP_CpModel::_loadAllResources && (!satellite->isScheduled() || !station->isScheduled()) && !isReserved)
        {
            OcpLogManager::getInstance()->warning(pseudoClazz, boost::format( _("%1$s sera ignoré car sur une station ou un satellite non planifié") )
                                % OCP_CpTrackingSlot::getOriginFullName(aSatSlot)  );
        }
        else
        {
            /*
             * en mode debug de fixation des slots on ne garde que les créneaux reservés ou ceux de la map
             * solver_fixed_slots définies dans listener.conf . Sinon, on rajoute le créneau en mode normal
             *
             * en mode debug replay à 0, on garde tous les slots (les "deux types de reservés" et les autres)
             */
            bool toBeLoaded = true;//par défaut, on charge le créneau
            bool replay = (1 == ConfAccel::getInstance()->getReplay());
            if(isReserved || !replay)
                toBeLoaded = true;//quel que soit le mode, un créneau reservé est chargé
            else
            {
                /*
                 * ne garder QUE les slots reservés (initialement et par paramètrage utilisateur conf
                 */
                toBeLoaded = isReservationForced(aSatSlot->getSlotID());
            }
            if(toBeLoaded)
                trackable = addTrackingSlot(aSatSlot,optimScope);
            else
                trackable = false;
        }
    }
    return trackable;
}

/**
 * utile en debug pour lire une liste de slots imposés depuis listener.conf
 * @param slotId : id du slot candidat
 * @return vrai ssi l'id du slot candidat est dans la liste des slots figés de listener.conf
 */
bool OCP_CpModel::isReservationForced(uint32_t slotId)
{
    int nbFixed = _fixedSlots.size();
    if(nbFixed == 0)
        return true;
    else
    {
        std::map<int,int>::const_iterator it  = _fixedSlots.find(slotId);
        return it != _fixedSlots.end();
    }
}


/**
* Flaggue les slots choisis par le moteur. Les figés le restent, les autres sont au status RESERVED
* @param beginDay début du jour de sauvegarde
* @param nbSavedTrackings nombre de slots de trackings sauvegardés
* @param nbSavedMaintenances nombre de slots de maintenance sauvegardés
*/
void OCP_CpModel::saveSlots(PHRDate beginDay,int& nbSavedTrackings, int& nbSavedMaintenances)
{
    static const std::string pseudoClazz = _Clazz + "::saveSlots";

    /**
    * On parcourt la liste des stations où l'on a sauvegardé la liste des slots satellites + slots de maintenance
    */

    IlcAnySet requiredSet = _solver.getAnySetVar(_globalSlotSetVar).getRequiredSet();
    IlcAnySet possibledSet = _solver.getAnySetVar(_globalSlotSetVar).getPossibleSet();


    if (possibledSet.getSize() != requiredSet.getSize())
    {
        boost::format msg = boost::format( _("Erreur possible sur variable globale . requis de taille %1$i possible %2$i") ) % requiredSet.getSize() % possibledSet.getSize()  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg.str() );

    }

    StationMap::iterator iterStation;
    nbSavedTrackings = 0;//incrémenté à chaque station
    int nbResaMaintenance = 0;
    nbSavedMaintenances=0;

    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;
        format logMsg = format( _("Sauvegarde des slots satellites vus par la station %2$s choisis le jour '%1$s'") ) %  beginDay.getStringValue() % currentStation->getName();
        OcpLogManager::getInstance()->info(pseudoClazz, logMsg.str() );
        currentStation->saveSlots(_solver,nbSavedTrackings,nbResaMaintenance);
        nbSavedMaintenances += nbResaMaintenance;
    }

}

/**
* instanciation sur un horizon via les paramètres de choix des contraintes
* @param parameters : l'objet contenant les modes de lancement et d'instanciation des contraintes
*/
void OCP_CpModel::pilot(OcpParametersPtr parameters)
{
    static const std::string pseudoClazz = _Clazz + "::pilot";

    ConstraintSetTypesEnum modeInstanciationLevel;
    std::string modeInstanciationDisplay;

    if (parameters->areSetsGlobal())
    {
        OcpLogManager::getInstance()->info(pseudoClazz, _("Pilotage avec paramètres globaux") );
    }
    else
    {
        OcpLogManager::getInstance()->info(pseudoClazz, _("Pilotage avec paramètres manuels") );
    }

    //renvoie 0 si le mode est local, sinon en mode global, donne le nombre de niveaux (exemple : NOMINAL/REDUIT => 2 niveaux)
    int nbGlobalInfos;
    if (parameters->areSetsGlobal())
    {
        nbGlobalInfos = (int) parameters->getSetTypes2Use().size() - 1;
    }
    else
    {
        nbGlobalInfos = 0;
    }


    OcpPeriodPtr horizon = parameters->getHorizon();


    /*
     * indique qu'on a correctement calculé la veille du jour courant
     */
    IloBool solutionDayBefore = false;

    // Pour chaque jour de la période, on va lancer le calcul de la planification
    VectorPHRDate listOfDaysInPeriod = horizon->getListOfDaysInPeriod();
    BOOST_FOREACH( PHRDatePtr element, listOfDaysInPeriod )
    {
        // La date du jour à planifier
        PHRDate beginDay = *(element.get());
        // Pour l'affichage propre dans les logs, on affiche juste la date sans l'heure
        beginDay.setDateFormat(PHRDate::_DateFormat);

        // La date du lendemain
        PHRDate beginNextDay(beginDay.addDuration(PHRDate::SECONDS_IN_A_DAY));


        int rankDay = 1 + beginDay.getDays() - horizon->getStart().getDays(); //rang du jour pour affichage dans le jdb

        bool stop = false;
        for(int scope = 0; (scope <= nbGlobalInfos) && (stop == false); scope++)
        {
            modeInstanciationDisplay = _("Manuel");
            modeInstanciationLevel = ConstraintSetTypes::NOMINAL; //valeur par défaut si paramètres manuels

            if (parameters->areSetsGlobal())
            {
                modeInstanciationLevel  = parameters->getSetTypes2Use()[scope];
                modeInstanciationDisplay = OcpConstraint::getStringFrom(modeInstanciationLevel);
            }

            format logMsg0 = format( _("Lancement de la planification en mode %1$s pour la journée du %2$s") )
                    % modeInstanciationDisplay % beginDay.getStringValue();

            OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::INFO, logMsg0, rankDay, modeInstanciationLevel);

            format debutMsg = format( _("**** %1$s. Info technique : %2$i/%3$i sur la période [%4$s; %5$s]") )
                    % logMsg0.str() % modeInstanciationLevel % nbGlobalInfos % beginDay.getStringValue() % beginNextDay.getStringValue();
            OcpLogManager::getInstance()->info(pseudoClazz, debutMsg.str());

            try
            {
                int nbSavedTrackings = 0;
                int nbSavedMaintenances = 0;
                int nbLocalRun = 0;//en mode restart
                IloNum totalTime = 0;
                IlcFloat totalTimeModel = 0;
                int res = run( beginDay, beginNextDay, parameters, scope, rankDay , modeInstanciationLevel,nbSavedTrackings,nbSavedMaintenances,totalTime,nbLocalRun,totalTimeModel,solutionDayBefore) ;
                switch(res)
                {
                    case 0:
                    {
                        //le calcul s'est correctement passé, on peut arrêter la boucle
                        stop = true;
                        format logMsg = format( _("Solution trouvée lors de la planification de la journée du %1$s %2$s (durée : %3$f, création modèle %4$f itérations : %5$d).") )
                                                        % beginDay.getStringValue() % modeInstanciationDisplay % totalTime % totalTimeModel % nbLocalRun;
                        solutionDayBefore = true;
                        OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::INFO, logMsg, rankDay, modeInstanciationLevel);

                        logMsg = format( _("**** Résultat de la planification sur le jour %1$s %4$s: Solution trouvée à %2$i créneaux satellites et %3$i créneaux de maintenance (durée : %5$f, création modèle %6$f itérations : %7$d).") )
                                                        % beginDay.getStringValue() % nbSavedTrackings % nbSavedMaintenances % modeInstanciationDisplay % totalTime % totalTimeModel % nbLocalRun;
                        OcpLogManager::getInstance()->info(pseudoClazz, logMsg);
                    }
                    break;
                    case 1:
                    {
                        format logMsg = format( _("Echec lors de la planification de la journée du %1$s %2$s pour la raison suivante : le solveur a prouvé l'absence de solution  (durée : %3$f, création modèle %4$f itérations : %5$d).") )
                                                        % beginDay.getStringValue() % modeInstanciationDisplay % totalTime % totalTimeModel % nbLocalRun;
                        solutionDayBefore = false;
                        OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::ALARM, logMsg, rankDay, modeInstanciationLevel);

                        logMsg = format( _("**** Résultat de la planification sur le jour %1$s %2$s : Echec - la raison est : le solveur a prouvé l'absence de solution (durée : %3$f, création modèle %4$f itérations : %5$d).") )
                                                        % beginDay.getStringValue() % modeInstanciationDisplay % totalTime % totalTimeModel % nbLocalRun;
                        OcpLogManager::getInstance()->warning(pseudoClazz, logMsg);
                    }
                    break;
                    case 2:
                    {
                        format logMsg = format( _("Echec lors de la planification de la journée du %1$s %2$s pour la raison suivante : le solveur n'a pas trouvé de solution à l'issue du timeout  (durée : %3$f, création modèle %4$f itérations : %5$d).") )
                                                        % beginDay.getStringValue() % modeInstanciationDisplay % totalTime % totalTimeModel % nbLocalRun;
                        solutionDayBefore = false;
                        OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::ALARM, logMsg, rankDay, modeInstanciationLevel);

                        logMsg = format( _("**** Résultat de la planification sur le jour %1$s %2$s : Echec - la raison est : le solveur n'a pas trouvé de solution à l'issue du timeout (durée : %3$f, création modèle %4$f itérations : %5$d).") )
                                                        % beginDay.getStringValue() % modeInstanciationDisplay % totalTime % totalTimeModel % nbLocalRun;
                        OcpLogManager::getInstance()->warning(pseudoClazz, logMsg);
                    }
                    break;
                    default:throw new OCP_XRoot("Status de sortie du solveur non reconnu");
                }


            }
            catch (OcpXMLInvalidField& err)
            {
                solutionDayBefore = false;
                format errorMsg = format( _("une erreur est survenue lors du chargement des données xml : %1$s") ) % err.what();

                format logMsg = format( _("Echec lors de la planification de la journée du %1$s %2$s pour la raison suivante : %3$s.") )
                        % beginDay.getStringValue() % modeInstanciationDisplay % errorMsg;
                OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::ALARM, logMsg, rankDay, modeInstanciationLevel);

                logMsg = format( _("**** Résultat de la planification sur le jour %1$s : %2$s.") )
                        % beginDay.getStringValue() % errorMsg;
                OcpLogManager::getInstance()->error(pseudoClazz, logMsg);
            }
            catch (OCP_XRoot& err)
            {
                solutionDayBefore = false;
                format errorMsg = format( _("une erreur est survenue lors de la préparation des données avant ou pendant la planification : %1$s") ) % err.what();

                format logMsg = format( _("Echec lors de la planification de la journée du %1$s %2$s pour la raison suivante : %3$s.") )
                        % beginDay.getStringValue() % modeInstanciationDisplay % errorMsg;
                OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::ALARM, logMsg, rankDay, modeInstanciationLevel);

                logMsg = format( _("**** Résultat de la planification sur le jour %1$s : %2$s.") )
                        % beginDay.getStringValue() % errorMsg;
                OcpLogManager::getInstance()->error(pseudoClazz, logMsg);
            }
            catch (IloException& err)
            {
                solutionDayBefore = false;
                format errorMsg = format( _("le moteur de calcul ILOG a rencontré une erreur inattendue : %1$s") ) % err.getMessage();

                format logMsg = format( _("Echec lors de la planification de la journée du %1$s %2$s pour la raison suivante : %3$s.") )
                        % beginDay.getStringValue() % modeInstanciationDisplay % errorMsg;
                OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::ALARM, logMsg, rankDay, modeInstanciationLevel);

                logMsg = format( _("**** Résultat de la planification sur le jour %1$s : %2$s.") )
                        % beginDay.getStringValue() % errorMsg;
                OcpLogManager::getInstance()->error(pseudoClazz, logMsg);
            }
            catch (std::exception& err)
            {
                solutionDayBefore = false;
                format errorMsg = format( _("une erreur inattendue est survenue lors de la planification : %1$s") ) % err.what();

                format logMsg = format( _("Echec lors de la planification de la journée du %1$s %2$s pour la raison suivante : %3$s.") )
                        % beginDay.getStringValue() % modeInstanciationDisplay % errorMsg;
                OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::ALARM, logMsg, rankDay, modeInstanciationLevel);

                logMsg = format( _("**** Résultat de la planification sur le jour %1$s : %2$s.") )
                        % beginDay.getStringValue() % errorMsg;
                OcpLogManager::getInstance()->error(pseudoClazz, logMsg);
            }
        }
    }

    /*
     * affichage récapitulatif (cf artf641221 : [Calcul] Pas d'erreur remontée suite à calcul planning)
     */
    //TODO ?

}

/*
 *  define IlcTraceDemons ((IlcUInt) 1)
#define IlcTraceConstraint ((IlcUInt) 2)
#define IlcTraceProcess ((IlcUInt) 4)
#define IlcTraceVars ((IlcUInt) 8)
#define IlcTraceFail ((IlcUInt) 16)
#define IlcTraceNoEvent ((IlcUInt) 32)
#define IlcTraceAllEvent ((IlcUInt) 31)
 *
 */

ILOCPTRACEWRAPPER1(PrintGeneralTrace, solver,int, level) {
  solver.setTraceMode(IlcTrue);

  static const std::string pseudoClazz = "OCP_CpModel::PrintGeneralTrace";

  OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Trace de propagation ILOG niveau %1$d ") ) % level );

  IlcPrintTrace trace(solver,level);

  solver.setTrace(trace);
}


ILOCPTRACEWRAPPER1(TraceVarArray, solver, IloAnySetVarArray, vars)
{
    static const std::string pseudoClazz = "OCP_CpModel::TraceVarArray";
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Propagation ILOG ciblée sur %1$d variables ensemblistes ") )
        % vars.getSize() );

    bool traceAdvanceNotifiedVars = (ConfAccel::getInstance()->traceVarsLevel()==2 );

    IlcAnySetVarArray svars=solver.getAnySetVarArray(vars);
    solver.setTraceMode(IlcTrue);
    IlcPrintTrace trace(solver, IlcTraceVars);
    for(int i=0;i<vars.getSize();i++)
    {
        IlcAnySetVar ilcVar = svars[i];
        trace.trace(ilcVar);

        /*
         * parcourir chaque variable notifiée , afficher le pointeur, le pointeur converti et l'id du slot
         * puis afficher ensuite  cette équivalence entre valeur décimale de l'IlcAny associée et l'id du slot
         */
        if(traceAdvanceNotifiedVars)
        {
            IloAnySetVar aVar = vars[i];
            IloAnySet aSet = aVar.getPossibleSet();
            IlcAnySet ilcSet = solver.getAnySet(aSet);
            OcpLogManager::getInstance()->debug(pseudoClazz,  boost::format( _("Correspondances sur Ilc %1$i Taille IloAnySet %2$i Taille IlcAnySet %3$i") )
            % ilcVar % aSet.getSize() % ilcSet.getSize() );

            for(IloAnySetIterator iterPossible(aSet); iterPossible.ok(); ++iterPossible)
            {
                IloAny currentPossible = *iterPossible;
                OCP_CpTrackingSlot* currentSlot = (OCP_CpTrackingSlot*)currentPossible;

                boost::format xmsg = boost::format( _("%1$i")) % currentPossible;
                unsigned int x;
                    std::stringstream ss;
                    ss << std::hex << xmsg.str();
                    ss >> x;


                    unsigned int castInt = x;


                OcpLogManager::getInstance()->debug(pseudoClazz,  boost::format( _("Correspondance entre IloAny %1$i int %2$i et %3$s") )
                % currentPossible % castInt % currentSlot->getFullName() );


            }
        }
    }
}

/**
 * ajout d'une fonction objectif à maximiser : somme des (10- prioSAT4)+(10-prioSTA6) pour chacun des créneaux satellites
 * dans la solution
 */
void OCP_CpModel::addObjective()
{
    IloIntVar sumPrio(getEnv() , 0, 1000000 , "objectiveOverSlots");
    getModel().add(IloObjectiveOverSlotsConstraint(_env, _globalSlotSetVar,sumPrio));
    IloObjective obj = IloMaximize(getEnv() , sumPrio);
    _model.add(obj);
}

/**
* affichage des paramètres de configuration pour la résolution solveur
*/
void OCP_CpModel::displayConfParameters()
{
    static const std::string pseudoClazz = _Clazz + "::displayConfParameters";

    int choixGoal =  ConfAccel::getInstance()->getSolverStrategy();
    bool withObjective = ConfAccel::getInstance()->solverWithObjective();

    int levelIlogPropagation =  ConfAccel::getInstance()->traceIlog();
    int outputIlogConsole = ConfAccel::getInstance()->traceOutputLevel();
    int traceChoicePoints = ConfAccel::getInstance()->traceChoicePointsLevel();
    int traceNotifiedVars = ConfAccel::getInstance()->traceVarsLevel();


    int removeUselessSlotsParam = ConfAccel::getInstance()->doRemoveUselessSlots();
    std::string fixedSlotsStr = ConfAccel::getInstance()->getFixedSlotsStr();
    int nbFixedSlots = _fixedSlots.size();
    float sta1Tolerance = ConfAccel::getInstance()->getToleranceSta1();
    int solverGanttAll = ConfAccel::getInstance()->getSolverGanttAll();
    int witoutReserveConstraints = ConfAccel::getInstance()->doPostWithoutReserveConstraints();
    float restart = ConfAccel::getInstance()->getSolverRestart();

    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Paramètre 'choix goal solveur' : %1$d") ) % choixGoal );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Paramètre 'choix withObjective'  : %1$d") ) % withObjective );

    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("\tNiveau de trace de propagation interne ILOG %1$d") )  % levelIlogPropagation );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("\tTrace de propagation des variables notifiées %1$d") )  % traceNotifiedVars );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("\tSortie ILOG vers console %1$d") )  % outputIlogConsole );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("\tAffichage des points de choix du goal d'instanciation %1$d") )  % traceChoicePoints );

    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Paramètre 'remove useless slots'  : %1$d") ) % removeUselessSlotsParam );
    OcpLogManager::getInstance()->warning(pseudoClazz, boost::format( _("Mode de préfixation debug de %1$d créneaux définis dans listener.conf solver_fixed_slots : %2$s") )
    % nbFixedSlots % fixedSlotsStr );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Paramètre 'tolérance sta1'  : %1$f") ) % sta1Tolerance );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Paramètre 'solverGanttAll '  : %1$i") ) % solverGanttAll );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Paramètre 'restart '  : %1$i secondes") ) % restart );
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Paramètre 'without reserve constraints '  : %1$i") ) % witoutReserveConstraints );

}

/**
* choix du goal de recherche
* @return le goal de recherch
*/
IloGoal OCP_CpModel::chooseGoal()
{
    static const std::string pseudoClazz = _Clazz + "::chooseGoal";

    int traceChoicePoints = ConfAccel::getInstance()->traceChoicePointsLevel() ;
    IloGoal gCompleteInstantiate = OCP_CpCompleteInstantiateAny(getEnv(), _globalSlotSetVar,traceChoicePoints);
    IloGoal gIloInstantiate      = IloInstantiate(getEnv(), _globalSlotSetVar);//incomplet !
    IloGoal gOcpInstantiate      = OCP_CpInstantiate(getEnv(), _globalSlotSetVar);
    IloGoal gCompleteGenerate    = OCP_CpCompleteGenerate(getEnv(), _globalSlotSetVar);
    IloGoal gRemoveFirst         = OCP_CpRemoveFirst(getEnv(), _globalSlotSetVar,traceChoicePoints);
    IloGoal gPropagate           = OCP_CpPropagate(getEnv());
    IloGoal gReservation         = OCP_CpReserveSlots(getEnv());

    /*
     * choix des différentes stratégies : complète, light, ultralight
     */
    IloGoal STRATEGIE_COMPLETE_INSTANTIATE = IloAndGoal(getEnv(), gReservation,gCompleteInstantiate);
    IloGoal STRATEGIE_ILO_INSTANTIATE      = IloAndGoal(getEnv(), gReservation,gIloInstantiate);
    IloGoal STRATEGIE_CP_INSTANTIATE       = IloAndGoal(getEnv(), gReservation,gOcpInstantiate);
    IloGoal STRATEGIE_COMPLETE_GENERATE    = IloAndGoal(getEnv(), gReservation,gCompleteGenerate);
    IloGoal STRATEGIE_REMOVE_FIRST         = IloAndGoal(getEnv(), gReservation,gRemoveFirst);
    IloGoal STRATEGIE_PROPAGATE            = IloAndGoal(getEnv(), gReservation,gPropagate);



    IloGoal goalChoisi;


    int choixGoal =  ConfAccel::getInstance()->getSolverStrategy();
    switch(choixGoal)
    {
        case 0 :
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("STRATEGIE_COMPLETE_INSTANTIATE") ) );
            goalChoisi = STRATEGIE_COMPLETE_INSTANTIATE;
            break;

        case 1 :
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("STRATEGIE_ILO_INSTANTIATE") ) );
            goalChoisi = STRATEGIE_ILO_INSTANTIATE;
            break;

        case 2 :
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("STRATEGIE_CP_INSTANTIATE") ) );
            goalChoisi = STRATEGIE_CP_INSTANTIATE;
            break;

        case 3 :
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("STRATEGIE_COMPLETE_GENERATE") ) );
            goalChoisi = STRATEGIE_COMPLETE_GENERATE;
            break;

        case 4 :
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("STRATEGIE_REMOVE_FIRST") ) );
            goalChoisi = STRATEGIE_REMOVE_FIRST;
            break;

        case 5 :
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("STRATEGIE_PROPAGATE") ) );
            goalChoisi = STRATEGIE_PROPAGATE;
            break;

        default :
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("STRATEGIE_COMPLETE_INSTANTIATE") ) );
            goalChoisi = STRATEGIE_COMPLETE_INSTANTIATE;
            break;
    }

    return goalChoisi;
}


/**
* Lancement du modèle  sur une portée [beginDay, beginNextDay) qui doit être un intervalle semi-ouvert
* @param beginDay : le début du jour courant
* @param beginNextDay : le début du jour suivant
* @param parameters: les paramètres de lancement, globaux ou par satellite et station
* @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
* @param rankDay : le rang du jour courant (de 1 à nombre de jours) pour affichage au jdb
* @param modeInstanciationDisplay : mode NOMINAL/... pour affichage au jdb
* @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
* @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
* @param totalTime temps total d'éxécution
* @param totalRun nombre de run successifs pour trouver (ou échouer) une solution en mode restart
* @param timeModel : temps consommé pour la création du modèle dans le ou les runs
* @param solutionDayBefore : flag indiquant qu'il y a eu un calcul avec succès la veille
* @return le status du solveur 0 : solution trouvée, 1 absence de solution prouvée, 2 pas de solution au bout du timeout
*/
int OCP_CpModel::run(PHRDate& beginDay, PHRDate& beginNextDay, OcpParametersPtr parameters, int scope, int rankDay, ConstraintSetTypesEnum modeInstanciationLevel,int& nbSavedTrackings,int& nbSavedMaintenances, IloNum& totalTime,int& totalRun, IlcFloat& timeModel,IloBool solutionDayBefore)
{
    static const std::string pseudoClazz = _Clazz + "::run";

    displayConfParameters();
    IloNum timeOut = parameters->getTimeout();
    IloEnv timerEnv;//TODO gérer ça par un vrai timer ! on n'a pas encore accès ici à _solver.getTime() car _solver non créé
    IloNum startTime = timerEnv.getTime();
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format( _("Global Time Out positionné à %1$i seconds") ) % timeOut);


    int found = 2;//par défaut, on initialise found au status 'pas de solution à l'issue du timeout'

    // libération des structures du run précédent
    prepare(OcpConstraint::getStringFrom(modeInstanciationLevel), beginDay, solutionDayBefore);
    IloNum time0 = timerEnv.getTime();

    OcpLogManager::getInstance()->info(pseudoClazz, _("Lecture du modèle..."));

    _origin  = (OCP_CpDate) beginDay.getSeconds();
    _horizon = (OCP_CpDate) beginNextDay.getSeconds();

    // fenêtre de calcul
    OCP_CpTimeSlotPtr optimScope ( new OCP_CpTimeSlot ((OCP_CpDate) beginDay.getSeconds() , (OCP_CpDate) beginNextDay.getSeconds()) );

    mapSatellitesAndStations(optimScope , parameters);

    createOCPSatelliteConstraints(beginDay , beginNextDay , parameters , scope);
    createOCPStationConstraints  (beginDay , beginNextDay , parameters , scope);

    _origin  = (OCP_CpDate) beginDay.getSeconds();
    _horizon = (OCP_CpDate) beginNextDay.getSeconds();

    // Extension des fenêtres de chargement des slots pour les satellites
    updateSatelliteScope();

    //rajouter les points de début et fin de périodes de tracking
    notifyStationTrackingZones(optimScope, parameters);//TODO gèrer aussi le dépassement d'horizon ?
    parseAllSlots(optimScope, parameters);
    splitAllSlots(parameters);
    feedModel(optimScope, parameters);

    if(ConfAccel::getInstance()->solverWithObjective())
    {
        addObjective();
    }

//    OcpLogManager::getInstance()->info(pseudoClazz, _("Recherche d'une solution..."));

    _solver = IloSolver(_model);

    IloGoal goalChoisi = chooseGoal();

    OCP_CpModel::getInstance()->initReversibleFlags();//une fois le solveur créé, on peut instancier les flag reversibles

    timeModel += timerEnv.getTime() - time0;
    boost::format timeMsg = boost::format( _("Temps pour la création et l'extraction du modèle %1$f seconds") ) % timeModel;
    OcpLogManager::getInstance()->debug(pseudoClazz, timeMsg.str() );

    int outputIlogConsole = ConfAccel::getInstance()->traceOutputLevel();
    if(0== outputIlogConsole)
    {
        _solver.setOut(getEnv().getNullStream()); //désactivation du trace solveur.
    }

    /*
     * activation sous paramètre des trace ILOG
     */
    prepareTraceIlogInfos();


    /*
     * temps maximal pour un run de division du time out
     */
    float localTimeOut = ConfAccel::getInstance()->getSolverRestart();
    IloNum tophase1 = timeOut;
    IloNum tophase2 = timeOut /2;
    IloBool twoPhase = false;
    if (localTimeOut > 0)
    {
        /*
         * division du time out initial par 2, puis par nbMaxRuns pour la première passe
         */
        tophase1 = localTimeOut;
        twoPhase = true;
    }

    int cpt = 1;
    IloNum cptTime=0;
    IloNum startRunTime = timerEnv.getTime();
    localTimeOut = tophase1;
    OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::INFO, _("Recherche d'une solution..."), rankDay, (ConstraintSetTypesEnum)modeInstanciationLevel);
    while(true)
    {
        boost::format timeMsg = boost::format( _("Run %1$d time out local %2$f : le calcul va etre lance: youpi !") ) % cpt % localTimeOut;
        OcpLogManager::getInstance()->debug(pseudoClazz, timeMsg.str() );

        _solver.setTimeLimit(localTimeOut);
        int oneRunFound=2;
        if(ConfAccel::getInstance()->solverWithObjective())
        {
            oneRunFound = searchOptimality(goalChoisi,beginDay,beginNextDay , parameters,scope,nbSavedTrackings,nbSavedMaintenances,twoPhase,timerEnv,timeOut);
        }
        else
        {
            oneRunFound = searchFeasibility(goalChoisi,beginDay,beginNextDay , parameters,scope,nbSavedTrackings,nbSavedMaintenances,timerEnv);
        }
        IloNum solverTime = timerEnv.getTime();//temps absolu à l'issue du run

        /*
         * s'arrêter dès le premier essai trouvé
         * Note : en mode optimisation, ce "premier essai" fait de surcroît les itérations améliorantes en tenant compte
         * du time out restant, il n'y a pas non plus lieu de relancer le calcul pour améliorer la précédente solution
         */
        if (oneRunFound==0 || oneRunFound==1)
        {
            found = oneRunFound;//solution trouvée
            break;
        }
        else if (oneRunFound==2 )
        {
            /*
             * le calcul précédent a prouvé l'absence de solution : il n'y a pas lieu de faire un restart
             */
            found = oneRunFound;
           // break;
        }

        /*
         * en mode mono-phase ou après la dernière passe, on s'arrête au premier essai
         */
        if(!twoPhase)
            break;

        /*
         * mise à jour des compteurs
         */
        cpt++;
        cptTime += localTimeOut;
        startRunTime = solverTime;

        /*
         * quand on dépasse la moitié du temps pour les premiers run, on relance avec une moitié du temps un dernier run.
         * On affecte twoPhase à false pour indiquer qu'il s'agit du dernier run
         */
        if (cptTime >= tophase2)
        {
            timeMsg = boost::format( _("Phase2 %1$d time out local %2$f ") ) % cpt % tophase2;
            OcpLogManager::getInstance()->debug(pseudoClazz, timeMsg.str() );
            localTimeOut = tophase2;
            twoPhase = false;//pour le prochain passage
        }

    }
    totalRun = cpt;
    totalTime = timerEnv.getTime() - startTime;
    timerEnv.end();
    return found;
}

/**
 * définition des trace ILOG
 */
void OCP_CpModel::prepareTraceIlogInfos()
{
    int tracePropagation = ConfAccel::getInstance()->traceIlog();
    int traceNotifiedVars = ConfAccel::getInstance()->traceVarsLevel();

    /**
    * suivi de la propagation ILOG
    */
    if(tracePropagation > 0)
    {
        _solver.addTrace(PrintGeneralTrace(getEnv() , tracePropagation));

    }
    if(traceNotifiedVars>0 && _setVarsToTrace.getImpl() != NULL)
    {
        _solver.addTrace(TraceVarArray(getEnv() ,_setVarsToTrace) );
    }
}

/**
* lancement de la résolution ILOG Solver et recherche de la meilleure solution au regard du critère de la fonction objectif
* @param goalChoisi goal choisi pour la recherche
* @param beginDay : le début du jour courant
* @param beginNextDay : le début du jour suivant
* @param parameters: les paramètres de lancement, globaux ou par satellite et station
* @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
* @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
* @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
* @param twoPhase : indique qu'on lance en mode restart
* @param timerEnv : timer pour comptabilisation des temps
* @param timeOut : rappel du timeOut pour la seconde passe d'optimisation
* @return le status du solveur 0 : solution trouvée, 1 absence de solution prouvée, 2 pas de solution au bout du timeout
*/
int OCP_CpModel::searchOptimality(IloGoal goalChoisi,PHRDate& beginDay,PHRDate& beginNextDay, OcpParametersPtr parameters, int scope,int& nbSavedTrackings,int& nbSavedMaintenances,IloBool twoPhase,IloEnv timerEnv,IloNum timeOut)
{
    static const std::string pseudoClazz = _Clazz + "::searchOptimality";


    _solver.setOptimizationStep(1);
    IloSolution solution(getEnv());
    solution.add(getGlobalSlotSetVar());
    IlcFloat time0 = timerEnv.getTime();
    IloGoal goalAndSave = goalChoisi && IloStoreSolution(getEnv(), solution);
    _solver.startNewSearch(goalAndSave);

    int result = 0;

    int cpt=0;
    int objValue = 0;
    while (_solver.next())
    {
        cpt++;

        objValue = (int) _solver.getObjValue();
        int nbChosenSlots = (int) _solver.getAnySetVar(_globalSlotSetVar).getRequiredSize();
        boost::format msg = boost::format( _("Solution numero %1$d trouvée à %2$d slots objectif %3$d après %4$f") ) % cpt % nbChosenSlots % objValue % time;
        OcpLogManager::getInstance()->info(pseudoClazz, msg );

        IloAlgorithm::Status status = _solver.getStatus();
        switch(status)
        {
            case IloAlgorithm::Unknown:  OcpLogManager::getInstance()->info(pseudoClazz,boost::format( _("Amélioration %1$d status Unknown")) % cpt) ; result = 2 ;break;
            case IloAlgorithm::Infeasible   :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Infeasible")) % cpt) ; result = 1 ;break;
            case IloAlgorithm::Unbounded   :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Unbounded")) % cpt); result = 1 ;break;
            case IloAlgorithm::InfeasibleOrUnbounded   :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status InfeasibleOrUnbounded")) % cpt) ; result = 1 ;break;
            case IloAlgorithm::Error :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Error")) % cpt) ; result = 2 ;break;
            case IloAlgorithm::Optimal:  OcpLogManager::getInstance()->info(pseudoClazz,boost::format( _("Amélioration %1$d status Optimal")) % cpt) ; result = 0 ;break;
            case IloAlgorithm::Feasible:  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Feasible")) % cpt) ; result = 0 ;break;
            default:throw new OCP_XRoot("Status non reconnu pour le solveur ILOG");
        }
    }
    _solver.endSearch();


    OcpLogManager::getInstance()->info(pseudoClazz, _("Information sur le process Solver :"));
    _solver.printInformation();
    _solver.getFailureStatus();//TODO a mettre dans le logger

    /*
     * Si l'on est en mode restart avec une solution, on va relancer le calcul pour améliorer cette solution jusqu'à épuiser
     * le time out global
     */

    if(cpt==0)
    {
        OcpLogManager::getInstance()->debug(pseudoClazz, _("Aucune solution en mode optimisation"));
        return result;
    }
    else
    {
        IloNum newTimeOut =  timeOut - (timerEnv.getTime()-time0);
        if (twoPhase && newTimeOut>0)
        {
            /*
             * relancer l'optimisation avec le time out global
             * TODO : à revoir, il semble qu'on peut ici dégrader l'objectif précédemment trouvé...
             */

            boost::format msg = boost::format( _("Seconde phase de l'optimisation avec un nouveau time out %1$f") ) % newTimeOut;
            OcpLogManager::getInstance()->info(pseudoClazz, msg );
            _solver.setTimeLimit(newTimeOut);
            //_solver.restartSearch();//incorrect search use
            //_solver.setObjMin()
            //_solver.add( _solver.getIntVar(_sumPrioVar) >= objValue+1);//You cannot call IloSolver::add here
            _solver.startNewSearch(goalAndSave);
            while (_solver.next())
            {
                cpt++;

                objValue = (int) _solver.getObjValue();
                IlcFloat time = timerEnv.getTime() - time0;
                int nbChosenSlots = (int) _solver.getAnySetVar(_globalSlotSetVar).getRequiredSize();
                boost::format msg = boost::format( _("Solution numero %1$d trouvée à %2$d slots objectif %3$d après %4$f") ) % cpt % nbChosenSlots % objValue % time;
                OcpLogManager::getInstance()->info(pseudoClazz, msg );
                /*IloAlgorithm::Status status = _solver.getStatus();
                 switch(status)
                 {
                     case IloAlgorithm::Unknown:  OcpLogManager::getInstance()->info(pseudoClazz,boost::format( _("Amélioration %1$d status Unknown")) % cpt) ; result = 2 ;break;
                     case IloAlgorithm::Infeasible   :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Infeasible")) % cpt) ; result = 1 ;break;
                     case IloAlgorithm::Unbounded   :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Unbounded")) % cpt); result = 1 ;break;
                     case IloAlgorithm::InfeasibleOrUnbounded   :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status InfeasibleOrUnbounded")) % cpt) ; result = 1 ;break;
                     case IloAlgorithm::Error :  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Error")) % cpt) ; result = 2 ;break;
                     case IloAlgorithm::Optimal:  OcpLogManager::getInstance()->info(pseudoClazz,boost::format( _("Amélioration %1$d status Optimal")) % cpt) ; result = 0 ;break;
                     case IloAlgorithm::Feasible:  OcpLogManager::getInstance()->info(pseudoClazz, boost::format( _("Amélioration %1$d status Feasible")) % cpt) ; result = 0 ;break;
                     default:throw new OCP_XRoot("Status non reconnu pour le solveur ILOG");
                 }*/

            }
            _solver.endSearch();
        }

        OcpLogManager::getInstance()->debug(pseudoClazz, _("Restauration de la meilleure solution trouvée"));
        bool found = _solver.solve(IloRestoreSolution(getEnv(),solution));

        if( ! found )
        {
            throw OCP_XRoot("Impossible de rejouer la meilleure solution trouvée");
        }
        else
        {
            int objValue = (int) _solver.getObjValue();
            int nbChosenSlots = (int) _solver.getAnySetVar(_globalSlotSetVar).getRequiredSize();
            boost::format msg = boost::format( _("Meilleure solution numéro %3$d trouvée à %1$d slots et objectif %2$d ") )  % nbChosenSlots % objValue % cpt;
            OcpLogManager::getInstance()->debug(pseudoClazz, msg);
            boost::format tmpMsg = boost::format( _("Temps total d'utilisation du solver ILOG %1$f : ") )
            % (timerEnv.getTime() - time0) ;
            OcpLogManager::getInstance()->info(pseudoClazz, tmpMsg  );
            postOptim(beginDay,beginNextDay,parameters,scope,nbSavedTrackings,nbSavedMaintenances);
        }
        return result;
    }
}
/**
* lancement de la résolution ILOG Solver et arrêt dès la première solution réalisable
* @param goalChoisi goal choisi pour la recherche
* @param beginDay : le début du jour courant
* @param beginNextDay : le début du jour suivant
* @param parameters: les paramètres de lancement, globaux ou par satellite et station
* @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
* @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
* @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
* @param timerEnv : timer pour comptabilisation des temps
* @return le status du solveur 0 : solution trouvée, 1 absence de solution prouvée, 2 pas de solution au bout du timeout
*/
int OCP_CpModel::searchFeasibility(IloGoal goalChoisi,PHRDate& beginDay,PHRDate& beginNextDay, OcpParametersPtr parameters, int scope,int& nbSavedTrackings,int& nbSavedMaintenances,IloEnv timerEnv)
{
    static const std::string pseudoClazz = _Clazz + "::searchSolutions";

    IloNum time0 = timerEnv.getTime();
    _solver.startNewSearch(goalChoisi);

    IloBool found = _solver.next();
    int result = 2;

    IloAlgorithm::Status status = _solver.getStatus();
    if (!found)
    {
        OcpLogManager::getInstance()->info(pseudoClazz, _("Pas de solution..."));

        switch(status)
        {
            case IloAlgorithm::Unknown:  OcpLogManager::getInstance()->info(pseudoClazz, _("status Unknown") ) ; result = 2 ;break;
            case IloAlgorithm::Infeasible   :  OcpLogManager::getInstance()->info(pseudoClazz, _("status Infeasible") ) ; result = 1 ;break;
            case IloAlgorithm::Unbounded   :  OcpLogManager::getInstance()->info(pseudoClazz, _("status Unbounded") ) ; result = 1 ;break;
            case IloAlgorithm::InfeasibleOrUnbounded   :  OcpLogManager::getInstance()->info(pseudoClazz, _("status InfeasibleOrUnbounded") ) ; result = 1 ;break;
            case IloAlgorithm::Error :  OcpLogManager::getInstance()->info(pseudoClazz, _("status Error") ) ; result = 2 ;break;
            default:throw new OCP_XRoot("Status non reconnu pour le solveur ILOG");break;
        }
    }
    else
    {
        switch(status)
        {
            case IloAlgorithm::Feasible:  OcpLogManager::getInstance()->info(pseudoClazz, _("status Feasible") ) ; result = 0 ;break;
            case IloAlgorithm::Optimal   :  OcpLogManager::getInstance()->info(pseudoClazz, _("status Optimal") ) ; result = 0 ;break;
            default:throw new OCP_XRoot("Status non reconnu pour le solveur ILOG");break;
        }
        int nbChosenSlots = (int) _solver.getAnySetVar(_globalSlotSetVar).getRequiredSize();
        IloNum elapsedTime = timerEnv.getTime() - time0;
        boost::format msg = boost::format( _("Solution trouvée à %1$d créneaux after %2$f...") )
        % nbChosenSlots % elapsedTime ;
        OcpLogManager::getInstance()->info(pseudoClazz, msg) ;
        postOptim(beginDay,beginNextDay,parameters,scope,nbSavedTrackings,nbSavedMaintenances);
    }


    boost::format tmpMsg = boost::format( _("Temps total d'utilisation du solver ILOG %1$f : ") )
                % (timerEnv.getTime() - time0) ;
    OcpLogManager::getInstance()->info(pseudoClazz, tmpMsg  );
    OcpLogManager::getInstance()->info(pseudoClazz,
            boost::format( _("Information sur le process Solver : %1$d échecs %2$d points de choix") )
            % _solver.getNumberOfFails()
            % _solver.getNumberOfChoicePoints());
               
    //
    _solver.printInformation();//TODO a mettre dans le logger
    _solver.getFailureStatus();//TODO a mettre dans le logger

    _solver.endSearch();

    /*
     * affichage de la trace de branchement
     */
    logTraceChoicePoints();

    return result;
}

/**
* affichage de l'arbre de branchement parcouru
*/
void OCP_CpModel::logTraceChoicePoints()
{
    static const std::string pseudoClazz = _Clazz + "::logTraceChoicePoints";

    if(_branchingIds.size()>0)
    {
        OcpLogManager::getInstance()->info(pseudoClazz, _(""));
        boost::format msg = boost::format( _("Affichage de la trace des branchements de taille %1$d") ) % _branchingIds.size();
        OcpLogManager::getInstance()->info(pseudoClazz, msg  );

        int limitDisplay = 2000;
        if ((int) _branchingIds.size() > limitDisplay)
        {
            boost::format msg = boost::format( _("Affichage tronqué aux %1$d premiers poins de choix ou backtrack") ) % limitDisplay;
        }
        if (limitDisplay>0)
        {
            boost::format msgId = boost::format( _("") );
            boost::format msgT = boost::format( _("") );
            std::vector<long>::iterator itId = _branchingIds.begin();
            std::vector<long>::iterator itT = _branchingTypes.begin();
            for( ; itId < _branchingIds.end() && (--limitDisplay)>0 ; itId++, itT++)
            {
                msgId = boost::format( _("%1$s %2$d") ) % msgId.str() % (*itId);
                msgT = boost::format( _("%1$s %2$d") ) % msgT.str() % (*itT);
            }
            OcpLogManager::getInstance()->info(pseudoClazz, msgId  );
            OcpLogManager::getInstance()->info(pseudoClazz, msgT  );
        }

        _branchingTypes.clear();
        _branchingIds.clear();
    }
}

/**
* sauvegarde des slots réservés à l'issue du calcul ILOG solver
* @param beginDay : le début du jour courant
* @param beginNextDay : le début du jour suivant
* @param parameters: les paramètres de lancement, globaux ou par satellite et station
* @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
* @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
* @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
 */
void OCP_CpModel::postOptim(PHRDate& beginDay,PHRDate& beginNextDay, OcpParametersPtr parameters, int scope,int& nbSavedTrackings,int& nbSavedMaintenances)
{
    static const std::string pseudoClazz = _Clazz + "::postOptim";
    saveSlots(beginDay,nbSavedTrackings,nbSavedMaintenances);

    /*
     * notifier sur chaque jour et chaque ressource le niveau de sauvegarde atteint (NOMINAL/ REDUIT...)
     */
    notifySavedResources(beginDay, parameters , scope);

    IlcAnySet requiredSet  = _solver.getAnySetVar(_globalSlotSetVar).getRequiredSet();
    IlcAnySet possibledSet = _solver.getAnySetVar(_globalSlotSetVar).getPossibleSet();

    if (possibledSet.getSize() != requiredSet.getSize())
    {
        boost::format msg = boost::format( _("GLOBAL Attention possible requis de taille %1$i possible %2$i") ) % requiredSet.getSize() % possibledSet.getSize()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg.str() );
    }


#ifdef ILM_GENIGRAPH_LICENSE
    OcpLogManager::getInstance()->info(pseudoClazz, _("Export du planning calculé au format GanttView de Scheduler et au format OCP"));
    exportSolution2Xml(beginDay,beginNextDay);
    OcpLogManager::getInstance()->info(pseudoClazz, _("Export du modèle au format csv pour exploitation OPL"));
    exportOplModel(beginDay);
#endif

}

/**
 * Sauvegarde de l'information du mode d'instanciation pour chacune des ressources à l'issue du calcul sur un jour
 * @param beginDay le jour de calcul
 * @param parameters paramètres d'entrée, global ou manuel
 * @param scope rang courant dans le tableau parameters->getSetTypes2Use()
 */
void OCP_CpModel::notifySavedResources(PHRDate beginDay,OcpParametersPtr parameters, int scope)
{
    static const std::string pseudoClazz = _Clazz + "::notifySavedResources";
    OcpContextPtr context = OcpXml::getInstance()->getContext();
    OcpDayPtr saveDay  = context->getDay(beginDay) ;

    /*
     * notification pour les satellites
     */
    OcpSatParams mapSatellite = parameters->getSetType4EachSatellite();
    for(OcpSatParams::iterator it = mapSatellite.begin();it!=mapSatellite.end();it++)
    {
        OcpSatellitePtr sat = (OcpSatellitePtr) ((*it).first);
        OCP_CpSatellitePtr satellite = getSatellite(sat);

        boost::format msg = format( _("Notification du mode d'instanciation sauvegardé pour le satellite '%1$s'") ) % sat->getName();
        OcpLogManager::getInstance()->debug(pseudoClazz, msg.str());

        ConstraintSetTypesEnum modeInstanciation;
        if (parameters->areSetsGlobal())
            modeInstanciation  = parameters->getSetTypes2Use()[scope];
        else
            modeInstanciation = (*it).second;

       /*
        * pour la vérification, sauvegarder le niveau dans lequel ce satellite a vu ses contraintes
        */
        OcpConstrainedRsrcPtr rsc = boost::dynamic_pointer_cast<OcpConstrainedRsrc>(sat);
        saveDay->setConstraintSetTypeUsed(*rsc,modeInstanciation);
    }

    /*
     * notification pour les stations
     */
    OcpStaParams mapStation = parameters->getSetType4EachStation();
    for(OcpStaParams::iterator it = mapStation.begin();it!=mapStation.end();it++)
    {
        OcpStationPtr aStation = (OcpStationPtr) ((*it).first);
        OCP_CpStationPtr station = getStation(aStation);

        boost::format msg = format( _("Notification du mode d'instanciation sauvegardé pour la station '%1$s'") ) % aStation->getName() ;
        OcpLogManager::getInstance()->debug(pseudoClazz, msg.str());
        ConstraintSetTypesEnum modeInstanciation;

        if (parameters->areSetsGlobal())
            modeInstanciation  = parameters->getSetTypes2Use()[scope];
        else
            modeInstanciation = it->second;

        /*
         * pour la vérification, sauvegarder le niveau dans lequel cette station a vu ses contraintes
         */
        OcpConstrainedRsrcPtr rsc = boost::dynamic_pointer_cast<OcpConstrainedRsrc>(aStation);
        saveDay->setConstraintSetTypeUsed(*rsc,modeInstanciation);
    }
}

void OCP_CpModel::printSolution()
{
    StationMap::iterator iterStation;
    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;
        currentStation->printSolution(_solver);
    }

    SatelliteMap::iterator iterSatellite;
    for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
    {
        OCP_CpSatellitePtr currentSatellite = (*iterSatellite).second;
        currentSatellite->printSolution(_solver);
    }
}


/**
 * Remplissage du modèle sur une portée [beginDay, beginNextDay) qui doit être un intervalle semi-ouvert
 * @param optimScope : fenêtre de calcul courante
 * @param parameters paramètres d'entrée donnant accès aux satellites
 */
void OCP_CpModel::feedModel(OCP_CpTimeSlotPtr optimScope, OcpParametersPtr parameters)
{
    static const std::string pseudoClazz = _Clazz + "::feedModel";

    OcpLogManager::getInstance()->debug(pseudoClazz, _("OCP_CpModel::feedModel"));
    OcpLogManager::getInstance()->debug(pseudoClazz, _("\tgenerate MAINTENANCE_SLOTS"));
    generateMaintenanceSlots();
    OcpLogManager::getInstance()->debug(pseudoClazz, _("\tCreate TRACKING_SLOTS"));
    createTrackingSlotVars();
    OcpLogManager::getInstance()->debug(pseudoClazz, _("\tCreate MAINTENANCE_SLOTS"));
    createMaintenanceSlots();
    OcpLogManager::getInstance()->debug(pseudoClazz, _("\tCreate GLOBAL_SLOTS"));
    createGlobalSlots();

    prepareSat1CardinalityInfos();

    OcpLogManager::getInstance()->debug(pseudoClazz, _("\tInstantiate OCP_CONSTRAINTS"));
    instantiateOCPConstraints(optimScope);


    OcpLogManager::getInstance()->debug(pseudoClazz, _("\tPosting OCP_CONSTRAINTS"));
    addOCPConstraints();

    OcpLogManager::getInstance()->debug(pseudoClazz, _("\tPosting INTERNAL_CONSTRAINTS"));
    addInternalConstraints();

    int removeUselessSlotsParam = ConfAccel::getInstance()->doRemoveUselessSlots();
    if(removeUselessSlotsParam)
    {
        OcpLogManager::getInstance()->debug(pseudoClazz, _("\tRemove useless slots"));
        removeUselessSlots(parameters);
    }
    updateTrackingSlotsPriorities(parameters);
    OcpLogManager::getInstance()->debug(pseudoClazz, _("OCP_CpModel::feedModel ... DONE"));
}

/**
 * une fois supprimé les slots superflus, on peut calculer une priorité aggrégée pour chaque slot*
 * en tenant compte des priorités de SAT4, STA6 et des conflits potentiels
 * @param parameters paramètres d'entrée donnant accès aux satellites
 */
void OCP_CpModel::updateTrackingSlotsPriorities(OcpParametersPtr parameters)
{
    OcpStaParams mapStation = parameters->getSetType4EachStation();
    OcpStationPtr aStation;
    OCP_CpStationPtr station;

    for(OcpStaParams::iterator it = mapStation.begin();it!=mapStation.end();it++)
    {
        aStation = (OcpStationPtr) ((*it).first);
        station = getStation(aStation);
        station->updateTrackingSlotsPriorities();

    }
}


/**
 * Instanciations de toutes les contraintes sur une portée jour
 * @param beginDay : début du jour courant
 * @param beginNextDay : début du prochain jour
 * @param param : paramètres NOMINAL/REDUIT...
 * @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
 */
void OCP_CpModel::createOCPSatelliteConstraints(PHRDate beginDay, PHRDate beginNextDay,OcpParametersPtr parameters, int scope)
{
    static const std::string pseudoClazz = _Clazz + "::createOCPSatelliteConstraints";
    OcpContextPtr context = OcpXml::getInstance()->getContext();


    OcpPeriodPtr aPeriod = boost::dynamic_pointer_cast<OcpPeriod>(OcpPeriod::createInstance());
    aPeriod->setStart(beginDay);
    aPeriod->setEnd(beginNextDay);
    OcpDayPtr day  = context->getDay(beginDay) ;


    OCP_CpTimeSlotPtr period( new OCP_CpTimeSlot( aPeriod) );

    format msg;

    // map des satellites planifiés
    OcpSatParams mapSatellite = parameters->getSetType4EachSatellite();
    //for(OcpSatParams::iterator it = mapSatellite.begin(); it != mapSatellite.end(); it++)
    //MapIdSatellite allsatellites = context->getMapSatellite();
    //for(MapIdSatellite::iterator it = allsatellites.begin(); it!=allsatellites.end(); it++)
    SatelliteMap::iterator iterSatellite;
    for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
    {
        OCP_CpSatellitePtr satellite = (*iterSatellite).second;
        OcpSatellitePtr sat = satellite->getOrigin();

        msg = format( _("creation des contraintes pour le Satellite '%1$s' sur la portée %2$s -> %3$s") ) %
                sat->getName() % beginDay.getStringValue() % beginNextDay.getStringValue();
        OcpLogManager::getInstance()->debug(pseudoClazz, msg.str());

        ConstraintSetTypesEnum modeInstanciation;
        bool modeInstanciationFound = true;
        if (parameters->areSetsGlobal() /*|| !satellite->isScheduled()*/)
        {
            modeInstanciation  = parameters->getSetTypes2Use()[scope];
        }
        else
        {
            OcpSatParams::iterator it2 = mapSatellite.find(sat);
            if (it2 != mapSatellite.end())
                modeInstanciation = (*it2).second;
        }

        /*
         * voir si on a déjà notifié la sauvegarde de cette ressource et si oui, dans quel mode
         * cela permettra de charger SAT4 uniquement pour ce satellite
         */
        if(!satellite->isScheduled())
        {
            try
            {
                modeInstanciation = day->getConstraintSetTypeUsed(*sat.get());
            }
            catch(OcpDataAccessException& err)
            {
                msg = format( _("Pas de traitement de contrainte SAT4 sur le satellite non planifié '%1$s' (il n'a pas été sauvegardé précédemment ) ") ) %
                        sat->getName() ;
                OcpLogManager::getInstance()->warning(pseudoClazz, msg.str());
                modeInstanciationFound = false;
            }
        }

        if(modeInstanciationFound)//TODO aérer la méthode !!
        {
            // liste des types de contraintes à charger, SAT4 en premier si elle est présente
            int STA4index = 0; //lancer une erreur si des contraintes type SAT4 existent mais ne sont pas positionnées en premier
            VectorConstraintTypes ctrTypes = parameters->getSatelliteConstraintTypes2Use();
            for(VectorConstraintTypes::iterator it = ctrTypes.begin() ; it != ctrTypes.end() ; it++, STA4index++)
            {
                ConstraintTypesEnum crtType = *it;

                if (crtType == ConstraintTypes::CSatType4 && STA4index > 0)
                {
                    msg = format( _("La contrainte de type SAT4 pour le Satellite '%1$s' n'a pas été lue en premier mais en position %2$i ") ) %
                            sat->getName() % STA4index;
                    OcpLogManager::getInstance()->error(pseudoClazz, msg.str());
                    throw OCP_XRoot( msg.str());
                }

                /*
                 * on crée les contraintes pour les satellites à planifier et pour les autres uniquement la contrainte SAT4
                 */
                if(satellite->isScheduled() || crtType == ConstraintTypes::CSatType4)
                {

                    // Collecter toutes les contraintes satellites de la portée , du type pour le satellite courant
                    VectorOcpConstraint ctrsSat = sat->getConstraintsByLevelTypeAndDate(modeInstanciation, crtType, *aPeriod);

                    for (VectorOcpConstraint::iterator iter = ctrsSat.begin(); iter != ctrsSat.end(); iter++)
                    {
                        OcpConstraintPtr aCtr = *iter;
                        try
                        {
                            OCP_CpConstraintPtr ocpCtr = createOneOCPSatelliteConstraint(crtType, period, aCtr, satellite);
                            storeOneOcpConstraint(ocpCtr , satellite.get() );

                        }
                        catch(OcpXMLInvalidField& e)
                        {
                            msg = format( _("La lecture des attributs XML de la contrainte %1$s de type %2$s pour le Satellite '%3$s' a provoqué l' erreur %4$s") )
                                            % aCtr->getName() %  OcpConstraint::getStringFrom( crtType) % sat->getName() % e.what();
                            OcpLogManager::getInstance()->error(pseudoClazz, msg.str());
                            throw OCP_XRoot( msg.str());
                        }

                    }
                }
            }
        }

    }
}

/**
* instanciation d'une contrainte type satellite
* @param ctrType le type de contrainte satellite
* @param period  la portée d'instanciation
* @param aCtr  la contrainte métier origine
* @param satellite  le satellite associé
*/
OCP_CpConstraintPtr OCP_CpModel::createOneOCPSatelliteConstraint(ConstraintTypesEnum ctrType, OCP_CpTimeSlotPtr period, OcpConstraintPtr aCtr, OCP_CpSatellitePtr satellite)
{

    switch(ctrType)
    {
        case ConstraintTypes::CSatType1:
        {
            OcpConstraintSatType1Ptr cstrSat1 = boost::dynamic_pointer_cast<OcpConstraintSatType1>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint1> newCtr ( new OCP_CpSatelliteConstraint1(period, cstrSat1, satellite) );
            return newCtr;
        }
        case ConstraintTypes::CSatType2:
        {
            OcpConstraintSatType2Ptr cstrSat2 = boost::dynamic_pointer_cast<OcpConstraintSatType2>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint2> newCtr ( new OCP_CpSatelliteConstraint2(period,cstrSat2,satellite) );
            return newCtr;
        }
        case ConstraintTypes::CSatType3:
        {
            OcpConstraintSatType3Ptr cstrSat3 = boost::dynamic_pointer_cast<OcpConstraintSatType3>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint3> newCtr ( new OCP_CpSatelliteConstraint3(period,cstrSat3,satellite) );
            return newCtr;
        }
        case ConstraintTypes::CSatType4:
        {
            OcpConstraintSatType4Ptr cstrSat4 = boost::dynamic_pointer_cast<OcpConstraintSatType4>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint4> newCtr ( new OCP_CpSatelliteConstraint4(period,cstrSat4,satellite) );
            return newCtr;
        }
        case ConstraintTypes::CSatType5:
        {
            //Note : cette contrainte est appelée sur son "premier satellite" uniquement
            OcpConstraintSatType5Ptr cstrSat5 = boost::dynamic_pointer_cast<OcpConstraintSatType5>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint5> newCtr ( new OCP_CpSatelliteConstraint5(period,cstrSat5,satellite) );
            return newCtr;
        }
        case ConstraintTypes::CSatType6:
        {
            OcpConstraintSatType6Ptr cstrSat6 = boost::dynamic_pointer_cast<OcpConstraintSatType6>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint6> newCtr(new OCP_CpSatelliteConstraint6(period,cstrSat6,satellite) );
            return newCtr;
        }
        case ConstraintTypes::CSatType7:
        {
            OcpConstraintSatType7Ptr cstrSat7 = boost::dynamic_pointer_cast<OcpConstraintSatType7>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint7> newCtr(new OCP_CpSatelliteConstraint7(period,cstrSat7,satellite) );
            return newCtr;
        }
        case ConstraintTypes::CSatType8:
        {
            OcpConstraintSatType8Ptr cstrSat8 = boost::dynamic_pointer_cast<OcpConstraintSatType8>(aCtr);
            shared_ptr<OCP_CpSatelliteConstraint8> newCtr(new  OCP_CpSatelliteConstraint8(period,cstrSat8,satellite) );
            return newCtr;
        }
        default:throw OCP_XRoot("Type de contrainte non reconnue");break;
    }

}

/**
* Instanciations de toutes les contraintes station sur une portée jour
* @param beginDay : début du jour courant
* @param beginNextDay : début du prochain jour
* @param param : paramètres NOMINAL/REDUIT...
* @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
*/
void OCP_CpModel::createOCPStationConstraints(PHRDate beginDay, PHRDate beginNextDay,OcpParametersPtr parameters, int scope)
{
    static const std::string pseudoClazz = _Clazz + "::createOCPStationConstraints";
    OcpContextPtr context = OcpXml::getInstance()->getContext();

    OcpPeriodPtr aPeriod = boost::dynamic_pointer_cast<OcpPeriod>(OcpPeriod::createInstance());
    aPeriod->setStart(beginDay);
    aPeriod->setEnd(beginNextDay);

    OCP_CpTimeSlotPtr period( new OCP_CpTimeSlot( aPeriod) );

    format msg;

    /**
    * parcours des stations
    */
    OcpStaParams mapStation = parameters->getSetType4EachStation();

    for(OcpStaParams::iterator it = mapStation.begin();it!=mapStation.end();it++)
    {
        OcpStationPtr aStation = (OcpStationPtr) ((*it).first);
        OCP_CpStationPtr station = getStation(aStation);

        msg = format( _("creation des contraintes pour la station '%1$s' sur la portée %2$s -> %3$s") ) %
                aStation->getName() % beginDay.getStringValue() % beginNextDay.getStringValue();
        OcpLogManager::getInstance()->debug(pseudoClazz, msg.str());
        ConstraintSetTypesEnum modeInstanciation;

        if (parameters->areSetsGlobal())
            modeInstanciation  = parameters->getSetTypes2Use()[scope];
        else
            modeInstanciation = it->second;

        /*
         * parcours des types de contraintes station
         */
        VectorConstraintTypes ctrTypes = parameters->getStationConstraintTypes2Use();
        for(VectorConstraintTypes::iterator it = ctrTypes.begin() ; it != ctrTypes.end(); it++ )
        {
            ConstraintTypesEnum crtType = (*it);
            /**
            * Collecter toutes les contraintes satellites de la portée , du type pour le satellite courant
            */
            VectorOcpConstraint ctrsSat = aStation->getConstraintsByLevelTypeAndDate(modeInstanciation,crtType,*aPeriod);

            for (VectorOcpConstraint::iterator iter = ctrsSat.begin(); iter != ctrsSat.end(); iter++)
            {
                OcpConstraintPtr aCtr = *iter;
                try
                 {
                    OCP_CpConstraintPtr ocpCtr = createOneOCPStationConstraint(crtType, period, aCtr, station);
                    storeOneOcpConstraint(ocpCtr , station.get() );
                 }
                 catch(OcpXMLInvalidField& e)
                 {
                     msg = format( _("La lecture des attributs XML de la contrainte %1$s de type %2$s pour la station '%3$s' a provoqué l' erreur %4$s") )
                             % aCtr->getName() %  OcpConstraint::getStringFrom( crtType) % aStation->getName() % e.what();
                     OcpLogManager::getInstance()->error(pseudoClazz, msg.str());
                     throw OCP_XRoot( msg.str());
                 }

            }

        }

    }
}

/**
* stockage d'une contrainte OCP dans le modèle
* @param aCtr contrainte OCP
* @param resource : la ressource satellite ou station
*/
void OCP_CpModel::storeOneOcpConstraint(OCP_CpConstraintPtr aCtr,OCP_CpTrackingResource* resource)
{
    _cpConstraints.push_back(aCtr);
    resource->storeOcpConstraintOrigin(aCtr);

    if(aCtr->isSAT4() )
    {
        _cpSAT4Constraints.push_back(aCtr);
    }
    if(aCtr->isSTA6() )
    {
        _cpSTA6Constraints.push_back(aCtr);
    }
    if(aCtr->isSTA1() )
    {
        _cpSTA1Constraints.push_back(aCtr);
    }

}

/**
* instanciation d'une contrainte type station
* @param ctrType le type de contrainte station
* @param period  la portée d'instanciation
* @param aCtr  la contrainte métier origine
* @param satellite  le satellite associé
*/
OCP_CpConstraintPtr OCP_CpModel::createOneOCPStationConstraint(ConstraintTypesEnum ctrType, OCP_CpTimeSlotPtr period, OcpConstraintPtr aCtr, OCP_CpStationPtr station)
{
    switch(ctrType)
    {
        case ConstraintTypes::CStaType1:
        {

            OcpConstraintStaType1Ptr cstrSta1 = boost::dynamic_pointer_cast<OcpConstraintStaType1>(aCtr);
            shared_ptr<OCP_CpStationConstraint1> newCtr ( new OCP_CpStationConstraint1(period,cstrSta1,station) );
            return newCtr;
        }
        case ConstraintTypes::CStaType2:
        {
            OcpConstraintStaType2Ptr cstrSta2 = boost::dynamic_pointer_cast<OcpConstraintStaType2>(aCtr);
            shared_ptr<OCP_CpStationConstraint2> newCtr ( new OCP_CpStationConstraint2(period,cstrSta2,station) );
            return newCtr;
        }
        case ConstraintTypes::CStaType3:
        {
            OcpConstraintStaType3Ptr cstrSta3 = boost::dynamic_pointer_cast<OcpConstraintStaType3>(aCtr);
            shared_ptr<OCP_CpStationConstraint3> newCtr ( new OCP_CpStationConstraint3(period,cstrSta3,station) );
            return newCtr;
        }
        case ConstraintTypes::CStaType4:
        {
            OcpConstraintStaType4Ptr cstrSta4 = boost::dynamic_pointer_cast<OcpConstraintStaType4>(aCtr);
            shared_ptr<OCP_CpStationConstraint4> newCtr ( new OCP_CpStationConstraint4(period,cstrSta4,station) );
            return newCtr;
        }
        case ConstraintTypes::CStaType5:
        {
            OcpConstraintStaType5Ptr cstrSta5 = boost::dynamic_pointer_cast<OcpConstraintStaType5>(aCtr);
            shared_ptr<OCP_CpStationConstraint5> newCtr ( new OCP_CpStationConstraint5(period,cstrSta5,station) );
            return newCtr;
        }
        case ConstraintTypes::CStaType6:
        {
            OcpConstraintStaType6Ptr cstrSta6 = boost::dynamic_pointer_cast<OcpConstraintStaType6>(aCtr);
            shared_ptr<OCP_CpStationConstraint6> newCtr(new OCP_CpStationConstraint6(period,cstrSta6,station) );
            return newCtr;
        }
        default:throw OCP_XRoot("Type de contrainte non reconnue");break;
    }
}



/**
  * Ajout d'un slot satellite à la liste
  * @param aSlot : passage satellite
  * @param optimScope fenêtre de calcul courante
  * @return vrai ssi le slot est candidat (non invalidé par une contrainte SAT4)
  */
bool OCP_CpModel::addTrackingSlot(OcpSatelliteSlotPtr aSatSlot,OCP_CpTimeSlotPtr optimScope)
{

    static const std::string pseudoClazz = _Clazz + "::addTrackingSlot";
    OcpSatellitePtr aSat = aSatSlot->getSatellite();
    OcpStationPtr aStation = aSatSlot->getStation();


    OCP_CpSatellitePtr satellite = getSatellite(aSat);
    if((satellite==NULL))
    {

        boost::format msg = boost::format( _("Satellite non mappé %1$s : ") ) % aSat->getName();
        OcpLogManager::getInstance()->error(pseudoClazz, msg.str());
        throw OCP_XRoot(msg.str());
    }

    OCP_CpStationPtr station = getStation(aStation);
    if((station==NULL))
    {
        boost::format msg = boost::format( _("Station non mappée%1$s %2$s ") ) % aStation->getName();
        OcpLogManager::getInstance()->error(pseudoClazz, msg.str());
        throw OCP_XRoot(msg.str());
    }

    /*
     * création du slot de tracking , mais on ne l'ajoutera effectivement au modèle que s'il n'est pas à supprimer par
     * SAT4 ou STA6
     */
    OCP_CpTrackingSlot* tmp = new OCP_CpTrackingSlot(satellite, station, aSatSlot);

    for(VectorOCP_CpConstraint::iterator it = _cpSAT4Constraints.begin() ; it != _cpSAT4Constraints.end() ; it++ )
    {
        OCP_CpConstraintPtr aCtr = (*it);

        OCP_CpSatelliteConstraint4Ptr cstrSat4 = boost::dynamic_pointer_cast<OCP_CpSatelliteConstraint4>(aCtr);
        if(satellite == cstrSat4->getSatellite())
        {
            //boost::format msg = boost::format( _("OCP_CpModel::impactSAT4Constraints On satellite %1$s which has %2$d tracking slots ")) % satellite->getName() %  satellite->getTrackingSlots().getSize();
            //OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );

            cstrSat4->notifyOneTrackingSlot(optimScope , tmp);
        }
    }
    //filtrage par STA6
    for(VectorOCP_CpConstraint::iterator it = _cpSTA6Constraints.begin() ; it != _cpSTA6Constraints.end() ; it++ )
    {
        OCP_CpConstraintPtr aCtr = (*it);

        OCP_CpStationConstraint6Ptr ctrSta6 = boost::dynamic_pointer_cast<OCP_CpStationConstraint6>(aCtr);
        if(station == ctrSta6->getStation() && !ctrSta6->concerns(aSat) )
        {
            /*créneau <sat,sta> à supprimer car il existe une contrainte STA6 sur sta qui n'inclut pas
             * sat             */
            boost::format msg = boost::format( _("OCP_CpStationConstraint6::instantiate exclut %1$s car son satellite n'est pas référencé dans les propriétés de sa station"))
            % tmp->getFullName().str();
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
            tmp->setToRemove();
        }
    }

    if(!tmp->toRemove())
    {
        satellite->addTrackingSlot(tmp);
        station->addTrackingSlot(tmp);

        boost::format msg = boost::format( _("satellite %3$s a %1$d tracking slots. dernier ajouté %2$s") )
        % satellite->getTrackingSlots().getSize() % tmp->getFullName() % satellite->getName();
        OcpLogManager::getInstance()->debug(pseudoClazz, msg);
        return true;
    }
    else
        return false;

}

/**
* Génération des variables Solver liés aux créneaux de maintenance
*/

void OCP_CpModel::generateMaintenanceSlots()
{
    StationMap::iterator iterStation;
    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;

        /*
         * s'il n'y a pas lieu de créer des slots de maintenance, on ne fait rien
         */
        VectorOCP_CpTimeSlot ofs;
        bool doGenerate = currentStation->isScheduled() && currentStation->prepareMaintenanceSlots(ofs);
        if (! doGenerate )
        {
            OcpLogManager::getInstance()->debug(ocp::solver::loggerInst,
                    boost::format( _("Pas de slot de maintenance possible pour la station %1$s ou station non planifiée ")) % currentStation->getName()  );

        }
        else
        {

            /*
             * passe d'ajout des points de maintenance additionnels : tenir compte des propriétés de STA1
             *
             */
            //prise en compte des propriétés de STA1 pour créer de nouveaux points de temps de maintenance
            for(VectorOCP_CpConstraint::iterator it = _cpSTA1Constraints.begin() ; it != _cpSTA1Constraints.end() ; it++ )
            {
                OCP_CpConstraintPtr aCtr = (*it);

                OCP_CpStationConstraint1Ptr ctrSta1 = boost::dynamic_pointer_cast<OCP_CpStationConstraint1>(aCtr);
                if(currentStation == ctrSta1->getStation()  )
                {
                    ctrSta1->updateStationMaintenanceTimePoints();
                }
            }


            currentStation->generateMaintenanceSlots(ofs);
        }
    }
}

/**
 * création des variables ensemblistes associées aux passages satellites, mémorisés à la fois sur les satellites et les stations
 */
void OCP_CpModel::createTrackingSlotVars()
{
    StationMap::iterator iterStation;
    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;
        currentStation->createTrackingSlotVar();
    }

    SatelliteMap::iterator iterSatellite;
    for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
    {
        OCP_CpSatellitePtr currentSatellite = (*iterSatellite).second;
        currentSatellite->createTrackingSlotVar();
    }
}

/**
 * création des slots associés aux maintenances, mémorisés sur les stations
 */
void OCP_CpModel::createMaintenanceSlots()
{
    static const std::string pseudoClazz = _Clazz + "::createMaintenanceSlots";
    StationMap::iterator iterStation;
    int nbMaintenanceSlots=0;
    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;
        nbMaintenanceSlots+=currentStation->createMaintenanceSlots();
    }
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("\t\tCreate %1$d maintenance slots")) % nbMaintenanceSlots);
}

/**
 * création de la variable ensembliste globale dont le domaine est l'union des passages satellites et des slots de maintenance
 */
void OCP_CpModel::createGlobalSlots()
{
    static const std::string pseudoClazz = _Clazz + "::createGlobalSlots";

    _globalSlots = IloAnyArray(_env);
    _allTrackingSlots = IloAnyArray(_env);

    StationMap::iterator iterStation;
    int nbAllMaintenanceSlots = 0;
    int nbAllTrackingSots = 0;
    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;
        _globalSlots.add(currentStation->getMaintenanceSlots());
        _globalSlots.add(currentStation->getTrackingSlots());
        _allTrackingSlots.add(currentStation->getTrackingSlots());
        nbAllMaintenanceSlots += currentStation->getMaintenanceSlots().getSize();
        nbAllTrackingSots += currentStation->getTrackingSlots().getSize();
    }

    _globalSlotSetVar = IloAnySetVar(_env, _globalSlots);

    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("\t\tCreation de %1$d global slots : %2$d tracking et %3$d maintenance "))
        % _globalSlots.getSize() % nbAllTrackingSots % nbAllMaintenanceSlots);
}

/**
 * création des contraintes internes liant les variables ensemblistes
 */
void OCP_CpModel::addInternalConstraints()
{
    StationMap::iterator iterStation;
    SatelliteMap::iterator iterSatellite;

    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;
        currentStation->addInternalConstraints();
    }

    for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
    {
        OCP_CpSatellitePtr currentSatellite = (*iterSatellite).second;
        currentSatellite->addInternalConstraints();
    }

    getModel().add(IloLinkGlobalSlotConstraint(_env, _globalSlotSetVar));

    //_sumCardinalitySlack = IloSum(_allCardinalitySlack);
    //getModel().add(_sumCardinalitySlack);

    if (ConfAccel::getInstance()->getSolverStableMax() == 1)
      addStableMaxConstraint();

}

/**
* Ajout à l'ensemble des requis de la variable globale des slots reservés
* Suppression des possibles des slots interdits
* Appel depuis un goal
* @param solver le solver interne PPC
*/
void OCP_CpModel::addReservationConstraints(IloSolver solver)
{
    static const std::string pseudoClazz = _Clazz + "::addReservationConstraints";
    IlcAnySetVar ilcGlobalSetVar = solver.getAnySetVar(_globalSlotSetVar);
    StationMap::iterator iterStation;

    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;

        /*
         * sauvegarde des slots de tracking
         */
        for(int i=0; i<currentStation->getTrackingSlots().getSize(); i++)
        {
            OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(currentStation->getTrackingSlots()[i]);
            tmp->setNextSat2MaxCandidate(solver, IlcFalse);
            if (tmp->isReserved() && tmp->toRemove())
            {
                boost::format msg = boost::format( _("Le passage %1$s a été flaggué à la fois requis et non possibles") )
                        %  tmp->getFullName().str() ;
                OcpLogManager::getInstance()->error(pseudoClazz, msg );
                throw OCP_XRoot(msg.str());
            }
            if (tmp->isReserved() ||
                    ( _fixedSlots.size()>0 && isReservationForced(tmp->getSlotOrigin()->getSlotID() ) )
                    )
            {
                if(ConfAccel::getInstance()->traceRemovedSlotsLevel() ==1)
                {
                    boost::format msg = boost::format( _("Le passage %1$s a été reservé") )
                    %  tmp->getFullName().str() ;
                    OcpLogManager::getInstance()->warning(pseudoClazz, msg );
                }
                ilcGlobalSetVar.addRequired(tmp);
            }
            if ( tmp->toRemove() )
            {
                if(ConfAccel::getInstance()->traceRemovedSlotsLevel() ==1)
                {
                    boost::format msg = boost::format( _("Le passage %1$s est supprimé (SAT4 ou superflu) ") )
                    %  tmp->getFullName().str() ;
                    OcpLogManager::getInstance()->warning(pseudoClazz, msg );
                }
                ilcGlobalSetVar.removePossible(tmp);
            }

            /*
             * debug pour afficher la correspondance entre id des slots et id ILOG
             */

            /*if (tmp->getSlotOrigin() != NULL){
                boost::format msg = boost::format( _("Correspondance %1$d -> ILOG %2$d") )
                %  tmp->getSlotOrigin()->getSlotID() % solver.get
                OcpLogManager::getInstance()->error(pseudoClazz, msg );
            }*/


        }

        /*
         * sauvegarde des slots de maintenance
         */
        for(int i=0; i<currentStation->getMaintenanceSlots().getSize(); i++)
        {
            OCP_CpMaintenanceSlot* tmp = (OCP_CpMaintenanceSlot*)(currentStation->getMaintenanceSlots()[i]);
            if (tmp->isReserved() && tmp->toRemove())
            {
                boost::format msg = boost::format( _("Le slot de maintenance %1$s a été flaggué à la fois requis et non possibles") )
                %  tmp->getFullName().str() ;
                OcpLogManager::getInstance()->error(pseudoClazz, msg );
                throw OCP_XRoot(msg.str());
            }
            if (tmp->isReserved() ||
                    ( _fixedSlots.size()>0 && (
                            isReservationForced(tmp->getSlotOrigin()->getSlotID()) ||
                                    isReservationForced(tmp->getId())
                            )
                    )
            )
            {
                if(ConfAccel::getInstance()->traceRemovedSlotsLevel() ==1)
                {
                    boost::format msg = boost::format( _("Le slot de maintenance %1$s a été reservé") )
                    %  tmp->getFullName().str() ;
                    OcpLogManager::getInstance()->warning(pseudoClazz, msg );
                }
                ilcGlobalSetVar.addRequired(tmp);
            }
            if ( tmp->toRemove() )
            {
                if(ConfAccel::getInstance()->traceRemovedSlotsLevel() ==1)
                {
                    boost::format msg = boost::format( _("Le passage %1$s est supprimé (SAT4 ou superflu) ") )
                    %  tmp->getFullName().str() ;
                    OcpLogManager::getInstance()->warning(pseudoClazz, msg );
                }
                ilcGlobalSetVar.removePossible(tmp);
            }
        }
    }
}

/**
* ajout au modèle PPC des contraintes métiers traduites en OCP_CpConstraints
*/
void OCP_CpModel::addOCPConstraints()
{
    static const std::string pseudoClazz = _Clazz + "::addOCPConstraints";

    StationMap::iterator iterStation;
    int n = 0;

    for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
    {
        OCP_CpStationPtr currentStation = (*iterStation).second;
        IloConstraintArray tmp = currentStation->getOcpConstraints();
        _model.add(tmp);
        for(int i=0;i<tmp.getSize();i++)
        {
            OCP_CpConstraint* originCtr = (OCP_CpConstraint*) tmp[i].getObject();
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("Ajout à IloModel de la contrainte %1$s")) % originCtr->getName());
        }
        n+=tmp.getSize();
    }

    SatelliteMap::iterator iterSatellite;
    for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
    {
        OCP_CpSatellitePtr currentSatellite = (*iterSatellite).second;
        IloConstraintArray tmp = currentSatellite->getOcpConstraints();
        _model.add(tmp);
        for(int i=0;i<tmp.getSize();i++)
        {
            OCP_CpConstraint* originCtr = (OCP_CpConstraint*) tmp[i].getObject();
            OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("Ajout à IloModel de la contrainte %1$s")) % originCtr->getName());
        }
        n+=tmp.getSize();
    }

    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("\t %1$d OCP_CONSTRAINTS")) % n);
}

/**
* export dans format texte pour préparation de données pour IBM-ILOG OPL
* @param beginDay debut de la fenetre de calcul
*/
void OCP_CpModel::exportOplModel(PHRDate beginDay)
{

    static const std::string pseudoClazz = _Clazz + "::exportOplModel";
    IloSolver solver = getSolver();

    PHRDate debutJour2(beginDay.getSeconds() , PHRDate::_DateFormat);
    boost::format csvFile = boost::format(_("homere%1$s.dat")) % debutJour2.getStringValue();
    ofstream outputFile(csvFile.str().c_str(), ios::out);
    OcpLogManager::getInstance()->debug(pseudoClazz, csvFile );
    if(outputFile)
    {
        /*
         * stations
         */
        StationMap::iterator iterStation;
        //outputFile<<"idStation;nameStation"<<endl;
        outputFile<<"stations = {\n";
        for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); ++iterStation)
        {
            OCP_CpStationPtr currentStation = (*iterStation).second;
            int id = currentStation->getId();
            string name = currentStation->getName();
            //outputFile << id <<";"<<name<<endl;
            outputFile <<"<"<<id<<" \"" << name << "\" >"<<endl;//<82 "2Ghz Aussaguel">

        }
        /*
          * // Generated on Tue Nov 02 10:43:35 2010
     // . OcpUnJourartf665024-1
     stations = {<82 "2Ghz Aussaguel"> <83 "2Ghz Kourou"> <84 "2Ghz Kiruna">
              };
     satellites = {<2 "SPOT 4"> <8 "HELIOS 1A"> <4 "CALIPSO"> <6 "SPOT 5">
              };
     creneaux = {<30290
                      <82>
                      <2>
                  1288000548
                  1288001971
                  1288001148
                  1288001911
                  0
                  1>
                };
     allcontraintes = {<93 "" 0 0> <96 "SAT1" 1 2>
              <101 "SAT1" 1 2> <106 "SAT1" 1 2>
              <111 "SAT1" 1 2> <116 "SAT2" 0 6060>
              };
     candidats = {<<30290>
                      <116>>
                  };
          *
          */
        /*
         * satellites
         */
        //outputFile<<"idSatellite;nameSatellite"<<endl;
        outputFile << "\n};\nsatellites={\n";
        SatelliteMap::iterator iterSatellite;
        for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
        {
            OCP_CpSatellitePtr currentSatellite = (*iterSatellite).second;
            int id = currentSatellite->getId();
            string name = currentSatellite->getName();
            //outputFile <<id <<";"<<name<<endl;
            outputFile <<"<"<<id<<" \"" << name << "\" >"<<endl;//<2 "SPOT 4">
        }


        // créneaux
        //outputFile << "idCreneau;idSta;idSat;start;end;aos;los;reserved;prio"<<endl;
        outputFile << "\n};\ncreneaux={\n";
        for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
        {
            OCP_CpStationPtr currentStation = (*iterStation).second;

            IloAnySetVar actT = currentStation->getTrackingSlotSet();
            IlcAnySetVar setVar = solver.getAnySetVar(actT);

            /*
             * affichage de tous les créneaux non supprimés (remove)
             */
            for(int i=0; i < currentStation->getTrackingSlots().getSize() ; i++)
            {
                OCP_CpTrackingSlot* ts  = (OCP_CpTrackingSlot*)(currentStation->getTrackingSlots()[i]);
                if (!ts->toRemove())
                {
                    int id2 = ts->getSlotOrigin()->getSlotID();
                    int idSat = ts->getSatellite()->getId();
                    int idSta = ts->getStation()->getId();
                    int aStart = ts->getAmplitude()->getBegin();
                    int aEnd = ts->getAmplitude()->getEnd();
                    int aos = -1;
                    int los = -1;
                    try
                    {
                        aos = ts->getBegin();
                        los = ts->getEnd();
                    }
                    catch(OCP_XRoot& e)
                    {

                    }
                    bool isReserv = ts->isReserved();
                    bool inSolution = setVar.isRequired(ts);
                    int prio = ts->getSearchPriority();
                    //outputFile<< id2 << ";" << idSta << ";" << idSat << ";" << aStart << ";" << aEnd << ";" << aos << ";" << los << ";"  << isReserv << ";" << prio <<endl;

                    /*
                      * creneaux = {<30290
                                   <82>
                                   <2>
                               1288000548
                               1288001971
                               1288001148
                               1288001911
                               0
                               1>
                             };
                      */

                     // créneaux
                     //outputFile << "idCreneau;idSta;idSat;start;end;aos;los;reserved;prio"<<endl;
                    outputFile << "<"<<id2<<" <"<<idSta <<">"<<" <"<<idSat<<"> "<<aStart << " " << aEnd << " " << aos << " " << los << " "  << isReserv << " " << inSolution <<" "<< prio <<">"<<endl;

                }
            }
        }

        /*
         * affichage des paires <idCreneau, idContrainte>
         *    candidats = {<<30290>
                      <116>>
                  };

         */
        //outputFile << "idCreneau;idContrainte"<<endl;
        outputFile << "\n};\ncandidats={\n";

        for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
        {
            OCP_CpStationPtr currentStation = (*iterStation).second;
            for(int i=0; i < currentStation->getTrackingSlots().getSize() ; i++)
            {
                OCP_CpTrackingSlot* ts  = (OCP_CpTrackingSlot*)(currentStation->getTrackingSlots()[i]);
                if (!ts->toRemove())
                {
                    int id2 = ts->getSlotOrigin()->getSlotID();
                    VectorOCP_CpConstraint ocpConstraints = ts->getOcpConstraints();
                    for(VectorOCP_CpConstraint::iterator itCtr = ocpConstraints.begin() ; itCtr != ocpConstraints.end() ; itCtr++)
                    {
                        OCP_CpConstraintPtr aCtr = (*itCtr);
                        //outputFile << id2 << ";" << aCtr->getId() << endl;
                        outputFile << "<<" << id2 << "> <" << aCtr->getId() << ">>" <<endl;
                    }
                }
            }
        }

        /*
         * affichage des attributs des contraintes
         */
        outputFile << "\n};\nallcontraintes={\n";
        for(VectorOCP_CpConstraint::iterator itCtr = _cpConstraints.begin() ; itCtr != _cpConstraints.end() ; itCtr++)
        {
            OCP_CpConstraintPtr aCtr = (*itCtr);
            aCtr->exportOplModel(outputFile);
        }

        outputFile << "\n}" <<endl;

        outputFile.close();
    }
    else
    {
        OcpLogManager::getInstance()->error(pseudoClazz, _("XML output fails!"));
    }
}

/**
* export dans format XML pour Schedulerviewer
* @param beginDay debut de la fenetre de calcul
* @param beginNextDay fin de la fenetre de calcul
*/
void OCP_CpModel::exportSolution2Xml(PHRDate beginDay,PHRDate beginNextDay)
{
    static const std::string pseudoClazz = _Clazz + "::exportSolution2Xml";
    IloSolver solver = getSolver();
    bool solverGanttAll = ( 1==ConfAccel::getInstance()->getSolverGanttAll() );

    PHRDate debutJour2(beginDay.getSeconds() , PHRDate::_DateFormat);
    boost::format xmlFile = boost::format(_("homere%1$s.xml")) % debutJour2.getStringValue();
    ofstream outputFile(xmlFile.str().c_str(), ios::out);
    OcpLogManager::getInstance()->debug(pseudoClazz, xmlFile );
    if(outputFile)
    {
        outputFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
        outputFile << "<!DOCTYPE schedule SYSTEM \"sdxl.dtd\">" << endl;
        outputFile << "<schedule version=\"3.5\">" << endl;
        outputFile << "\t<resources>" << endl;
        int nbStations = getStations().size();
        int nbResources = getSatellites().size() +  nbStations;
        outputFile << "\t\t<resource id=\"All Resources\" name=\"All Resources\" quantity=\"" << nbResources << "\">" << endl;

        StationMap::iterator iterStation;
        for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); ++iterStation)
        {
            OCP_CpStationPtr currentStation = (*iterStation).second;
            int id = currentStation->getId();
            string name = currentStation->getName();
            outputFile << "\t\t\t<resource id=\"" << id << "\" name=\"" << name << "\" quantity=\"1\" />" << endl;
        }

        SatelliteMap::iterator iterSatellite;
        for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
        {
            OCP_CpSatellitePtr currentSatellite = (*iterSatellite).second;
            int id = currentSatellite->getId();
            string name = currentSatellite->getName();
            outputFile << "\t\t\t<resource id=\"" << id << "\" name=\"" << name << "\" quantity=\"" << nbStations << "\" />" << endl;
        }
        outputFile << "\t\t</resource>" << endl;
        outputFile << "\t</resources>" << endl;

        outputFile << "\t<activities dateFormat=\"yyyy-M-d H:m:s\">" << endl;
        beginDay.setDateFormat(PHRDate::_DateTimeFormat2);
        beginNextDay.setDateFormat(PHRDate::_DateTimeFormat2);
        outputFile << "\t\t<activity id=\"All Activities\" name=\"All Activities\" start=\""<< beginDay.getStringValue() <<"\" end=\""<< beginNextDay.getStringValue() <<"\">" << endl;

        // station view
        for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
        {
            OCP_CpStationPtr currentStation = (*iterStation).second;

            //création des activités liées aux tracking

            IloAnySetVar actT = currentStation->getTrackingSlotSet();
            IlcAnySetVar setVar = solver.getAnySetVar(actT);

            /*
             * création des activités de tracking
             */
            for(IlcAnySetIterator it(setVar.getRequiredSet()); it.ok(); ++it)
            {
                OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*)(*it);
                int id = ts->getId();
                int id2 = ts->getSlotOrigin()->getSlotID();
                string name = ts->getName();
                OcpSatelliteSlotPtr aSatSlot = ts->getSlotOrigin();
                PHRDate pBegin = aSatSlot->getStartEnd()->getStart();
                PHRDate pEnd = aSatSlot->getStartEnd()->getEnd();
                pBegin.setDateFormat(PHRDate::_DateTimeFormat2);
                pEnd.setDateFormat(PHRDate::_DateTimeFormat2);
                outputFile << "\t\t\t<activity id=\"" << id << "\" name=\"" << id2 << "\" start=\"" << pBegin.getStringValue() << "\" end=\"" << pEnd.getStringValue() << "\" />" << endl;
            }


            /*
             * debug : création des activités non retenues mais candidates pour SAT4
             */
            if(solverGanttAll)
            {
            boost::format msg = boost::format( _("Affichage en négatif des slots non retenus sur la station %1$s ") ) % currentStation->getName();
            OcpLogManager::getInstance()->debug(pseudoClazz, msg );
            for(int i=0; i < currentStation->getTrackingSlots().getSize() ; i++)
            {
                OCP_CpTrackingSlot* ts  = (OCP_CpTrackingSlot*)(currentStation->getTrackingSlots()[i]);
                if (!setVar.getRequiredSet().isIn(ts))
                {
                    int id = ts->getId();
                    int id2 = ts->getSlotOrigin()->getSlotID();
                    OCP_CpConstraintPtr ctr;

                    for(VectorOCP_CpConstraint::iterator it = _cpConstraints.begin() ; it != _cpConstraints.end() ; it++ )
                    {
                        ctr = (*it);
                        if (ctr->isSAT4())
                        {
                            OcpLogManager::getInstance()->debug(pseudoClazz, ts->getFullName() );
                            string name = ts->getName();
                            OcpSatelliteSlotPtr aSatSlot = ts->getSlotOrigin();
                            PHRDate pBegin = aSatSlot->getStartEnd()->getStart();
                            PHRDate pEnd = aSatSlot->getStartEnd()->getEnd();
                            pBegin.setDateFormat(PHRDate::_DateTimeFormat2);
                            pEnd.setDateFormat(PHRDate::_DateTimeFormat2);
                            outputFile << "\t\t\t<activity id=\"" << id << "\" name=\"" << -id2 << "\" start=\"" << pBegin.getStringValue() << "\" end=\"" << pEnd.getStringValue() << "\" />" << endl;
                        }
                    }
                }
            }
            }


            //création des activités liées aux maintenances
            IloAnySetVar actM = currentStation->getMaintenanceSlotSet();

            for(IlcAnySetIterator it(solver.getAnySetVar(actM).getRequiredSet()); it.ok(); ++it)
            {
                OCP_CpMaintenanceSlot* ms = (OCP_CpMaintenanceSlot*)(*it);
                int id = ms->getId();
                // not so explicit: string name = ms->getName();
                string name = ms->getName();
                PHRDate pBegin(ms->getBegin() , PHRDate::_DateTimeFormat2);
                PHRDate pEnd(ms->getEnd() , PHRDate::_DateTimeFormat2);
                outputFile << "\t\t\t<activity id=\"" << id << "\" name=\"" << name << "\" start=\"" << pBegin.getStringValue() << "\" end=\"" << pEnd.getStringValue() << "\" />" << endl;
            }
        }
        outputFile << "\t\t</activity>" << endl;
        outputFile << "\t</activities>" << endl;

        outputFile << "\t<reservations>" << endl;

        for(iterStation = _allStations.begin(); iterStation!=_allStations.end(); iterStation++)
        {
            /*
             * réservations trackings
             */
            OCP_CpStationPtr currentStation = (*iterStation).second;
            IloAnySetVar actT = currentStation->getTrackingSlotSet();
            int id1 = currentStation->getId();

            for(IlcAnySetIterator it(solver.getAnySetVar(actT).getRequiredSet()); it.ok(); ++it)
            {
                OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*)(*it);
                int id2 = ts->getId();
                outputFile << "\t\t\t<reservation resource=\"" << id1 << "\" activity=\"" << id2 << "\" />" << endl;
            }

            /*
             * debug réservations trackings SAT4 non choisis
             */
            if(solverGanttAll)
            {
                IlcAnySetVar setVar = solver.getAnySetVar(actT);
                for(int i=0; i < currentStation->getTrackingSlots().getSize() ; i++)
                {
                    OCP_CpTrackingSlot* ts  = (OCP_CpTrackingSlot*)(currentStation->getTrackingSlots()[i]);
                    if (!setVar.getRequiredSet().isIn(ts))
                    {
                        int id2 = ts->getId();
                        outputFile << "\t\t\t<reservation resource=\"" << id1 << "\" activity=\"" << id2 << "\" />" << endl;
                    }
                }
            }

            /*
             * reservation maintenance slots
             */

            IloAnySetVar actM = currentStation->getMaintenanceSlotSet();

            for(IlcAnySetIterator it(solver.getAnySetVar(actM).getRequiredSet()); it.ok(); ++it)
            {
                OCP_CpMaintenanceSlot* ms = (OCP_CpMaintenanceSlot*)(*it);
                int id2 = ms->getId();
                outputFile << "\t\t\t<reservation resource=\"" << id1 << "\" activity=\"" << id2 << "\" />" << endl;
            }
        }

        for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); ++iterSatellite)
        {
            OCP_CpSatellitePtr currentSatellite = (*iterSatellite).second;
            IloAnySetVar actT = currentSatellite->getTrackingSlotSet();
            int id1 = currentSatellite->getId();

            /*
             * réservations côté satellite des slots retenus
             */
            for(IlcAnySetIterator it(solver.getAnySetVar(actT).getRequiredSet()); it.ok(); ++it)
            {
                OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*)(*it);
                int id2 = ts->getId();
                outputFile << "\t\t\t<reservation resource=\"" << id1 << "\" activity=\"" << id2 << "\" />" << endl;
            }

            /*
             * debug : réservations liées aux slots non retenus par SAT4
             */
            if(solverGanttAll)
            {
                IlcAnySetVar setVar = solver.getAnySetVar(actT);
                for(int i=0; i < currentSatellite->getTrackingSlots().getSize() ; i++)
                {
                    OCP_CpTrackingSlot* ts  = (OCP_CpTrackingSlot*)(currentSatellite->getTrackingSlots()[i]);
                    if (!setVar.getRequiredSet().isIn(ts))
                    {
                        int id2 = ts->getId();
                        outputFile << "\t\t\t<reservation resource=\"" << id1 << "\" activity=\"" << id2 << "\" />" << endl;
                    }
                }
            }

        }

        outputFile << "\t</reservations>" << endl;
        outputFile << "</schedule>" << endl;
        outputFile.close();
    }
    else
    {
        OcpLogManager::getInstance()->error(pseudoClazz, _("XML output fails!"));
    }
}

void OCP_CpModel::displayVarForDebug(IloAnySetVar act)
{
    static const std::string pseudoClazz = _Clazz + "::displayVarForDebug";

    OcpLogManager::getInstance()->debug(pseudoClazz, _("OCP_CpModel::displayVarForDebug"));

    for(IlcAnySetIterator it(_solver.getAnySetVar(act).getPossibleSet()); it.ok(); ++it)
    {
        OCP_CpTimeSlot* ts = (OCP_CpTimeSlot*)(*it);
        OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("Possible [%1$d, %2$d]")) % ts->getBegin() % ts->getEnd());
    }
    for(IlcAnySetIterator it(_solver.getAnySetVar(act).getRequiredSet()); it.ok(); ++it)
    {
        OCP_CpTimeSlot* ts = (OCP_CpTimeSlot*)(*it);
        OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("Required [%1$d, %2$d]")) % ts->getBegin() % ts->getEnd());
    }
}

/**
* debug : trace d'une variable ensembliste
* @param setVarToTrace_
*/
void OCP_CpModel::addOneSetVarToTrace(IloAnySetVar setVarToTrace_)
{
    if (_setVarsToTrace.getImpl() == NULL)
        _setVarsToTrace = IloAnySetVarArray(getEnv());

    /*
     * ajout sans doublon
     */
    int i=0;
    for(i=0;i<_setVarsToTrace.getSize() && _setVarsToTrace[i].getImpl() != setVarToTrace_.getImpl();i++)
        ;
    if (i == _setVarsToTrace.getSize())
        _setVarsToTrace.add(setVarToTrace_);
}

/**
 * ajout de la contrainte redondante basée sur la cardinalité minimale d'un stable maximum
 * dans le graphe des conflits
 */
void OCP_CpModel::addStableMaxConstraint()
{
    /*
     * 1. retrouver pour chaque satellite des ensembles disjoints de créneaux correspondant à des SAT1
     * dont les domaines sont deux à deux disjoints
     */
    partitionStableCandidates();

    /*
     * 2. collecter les variables candidates, l'union des domaines et la somme des cardinalités
     */
    IloAnyArray allslots(_env);
    IloAnySetVarArray vars(_env);
    int minCardinality = 0;

    SatelliteMap::iterator iterSatellite;
    for(iterSatellite = _allSatellites.begin(); iterSatellite!=_allSatellites.end(); iterSatellite++)
    {
        OCP_CpSatellitePtr satellite = (*iterSatellite).second;
        minCardinality += satellite->collectStable( vars, allslots);
    }

    /*
     * 3. création de la variable ensembliste dont le domaine est l'ensemble des contraintes SAT1
     * poser les contraintes:
     *   3.1 cette variable est incluse dans la variable ensembliste globale
     *   3.2 cette variable est l'union des variables ensemblistes collectées
     *
     */

    IloAnySetVar stableDomainVar(_env, allslots);
    getModel().add( IloSubsetEq(_env,stableDomainVar,_globalSlotSetVar) );
    getModel().add( IloEqUnion(_env,stableDomainVar,vars) );

    /*
     * 4. pose de la contrainte indiquant qu'il doit exister toujours un stable maximum de cardinalité
     * supérieure ou égale à la somme des cardinalités min des SAT1
     */
    boost::format msg = boost::format( _("STABLEMAX cardinalité %1$d sur un ensemble de %2$d variables ensemblistes totalisant %3$d créneaux") )
    %  minCardinality % vars.getSize() % allslots.getSize() ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
    getModel().add( IloStableMaxConstraint(_env,stableDomainVar,minCardinality,"ctrStableMax" ));
}

/**
* affichage des cardinalités courantes des variables ensemblistes collectées par SAT1
*/
void OCP_CpModel::displayCardinalityInfos()
{
    boost::format msg = boost::format("displayCardinalityInfos");
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    for(int i=0;i<_allCardinalitySetVars.getSize();i++)
    {
        IloAnySetVar setVar = _allCardinalitySetVars[i];
        IloIntVar cardVar = _allCardinalityVars[i];
        IlcIntVar ilcCardVar = _solver.getIntVar(cardVar);
        IlcAnySetVar ilcSetVar = _solver.getAnySetVar(setVar);

        msg = boost::format( _("%1$s [%2$d,%3$d] courant [%4$d,%5$d] ") )
            % setVar.getName() % cardVar.getLB() % cardVar.getUB()
            % ilcSetVar.getRequiredSize() % ilcSetVar.getPossibleSize() ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    }
}

/**
 * A la création des instances des SAT1, on mémorise la variable ensembliste et sa cardinalité
 * @param setVar  variable ensembliste liée à la contrainte SAT1
 * @param cardVar cardinalité de la variable ensembliste liée à la contrainte SAT1
 * @param trackingDomain créneaux candidats (peut différer à la création du domaine de setVar)
 */
void OCP_CpModel::addCardinalityInfo(IloAnySetVar setVar, IloIntVar cardVar,IloAnyArray trackingDomain)
{
    int index = _allCardinalitySetVars.getSize();
    _allCardinalitySetVars.add(setVar);
    _allCardinalityVars.add(cardVar);
    for(int i=0;i<trackingDomain.getSize();i++)
    {
        OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*) trackingDomain[i];
        ts->addSat1Index(index);
    }
}

/**
 * ajout d'une instance de contrainte IloSatelliteConstraint3
 * @param sat3Ctr
 * @param trackingDomain les slots candidats pour cette contrainte
 */
void OCP_CpModel::addSat3IloConstraint(IloConstraint sat3Ctr,IloAnyArray trackingDomain)
{
    int index = _sat3IloConstraints.getSize();
    _sat3IloConstraints.add(sat3Ctr) ;
    for(int i=0;i<trackingDomain.getSize();i++)
    {
        OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*) trackingDomain[i];
        ts->addSat3Index(index);
    }
}



/**
 * renvoie le meilleur gain possible si on assignait le slot, le gain étant mesuré comme le maximum
 * parmi les contraintes SAT1 contenant le slot du ratio A/B avec
 * A = écart courant à la borne min de la cardinalité courante de la variable ensembliste = max(0, card(setVar.required)-lb)
 * B = nombre de créneaux encore possible = card(setVar.possible() - card(setVar.required() )
* @param slot slot courant dont on examine le gain possible
* @return la meilleure amélioration possible (entre 0 et 1)
*/
bool OCP_CpModel::hasBestImprovementSat1MinPass(OCP_CpTrackingSlot* slot)
{
    //int globalSlackVal = _solver.getIntExp(_sumCardinalitySlack).getMin();
    //boost::format msg = boost::format( _("getBestImprovement slack %1$d") ) % globalSlackVal;
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    std::vector<int>& slotSat1Indexes = slot->getSat1Indexes();
    for(std::vector<int>::iterator it = slotSat1Indexes.begin() ; it != slotSat1Indexes.end() ; it++)
    {
        int i = (*it);
        if( _bestSlackMinPassSupport.find(i) != _bestSlackMinPassSupport.end() )
            return true;
    }

    //msg = boost::format( _("best improve %1$f pour %2$d ") ) % bestImprove % slot->getSlotId();
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);


    return false;
}


/**
* parcourt les différentes instances des contraintes SAT1 et regarde celles qui n'atteignent pas leur
* borne min
* @return vrai ssi il existe au moins une contrainte SAT1 qui n'atteint pas sa borne min
*/
bool OCP_CpModel::computeBestImprovementMinPass()
{
    float bestImprove= 0.0;
    //int globalSlackVal = _solver.getIntExp(_sumCardinalitySlack).getMin();
    //boost::format msg = boost::format( _("getBestImprovementSat1MinPass slack %1$d") ) % globalSlackVal;
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);


    _bestSlackMinPassSupport.clear();

    for(int i=0;i<_allCardinalitySetVars.getSize();i++)
    {
        IloAnySetVar setVar = _allCardinalitySetVars[i];
        IloIntVar cardVar = _allCardinalityVars[i];

        IlcIntVar ilcCardVar = _solver.getIntVar(cardVar);
        IlcAnySetVar ilcSetVar = _solver.getAnySetVar(setVar);

        int missing = (int) cardVar.getLB() - ilcSetVar.getRequiredSize() ;
        //IlcInt slackVal = _solver.getIntExp(_allCardinalitySlack[i]).getMin();
        //msg = boost::format( _("Missing %1$d slack %2$d") ) % missing % slackVal;
        //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        if (missing > 0)
        {
            int freedom = ilcSetVar.getPossibleSize() - ilcSetVar.getRequiredSize() ;
            if (freedom==0)
                throw new OCP_XRoot("degre de liberte 0 non autorise dans le best improvement");
            float relgap = missing / (float) freedom;
            if (relgap >= bestImprove)
            {
                if (relgap > bestImprove)
                {
                    _bestSlackMinPassSupport.clear();
                    bestImprove = relgap;
                }
                _bestSlackMinPassSupport[i]=i;
            }
        }
    }

    if(_bestSlackMinPassSupport.size()>0)
    {
        _hasSlackMinPass.setValue(_solver,IlcTrue);
        return true;
    }
    else
    {
        _hasSlackMinPass.setValue(_solver,IlcFalse);
        return false;
    }

    //msg = boost::format( _("best improve %1$f pour %2$d ") ) % bestImprove % slot->getSlotId();
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

}
/**
 * parcourt les différentes instances des contraintes SAT3 et regarde celles qui n'atteignent pas leur
 * borne min
 * @return vrai ssi il existe au moins une contrainte SAT3 qui n'atteint pas sa borne min
 */
bool OCP_CpModel::computeBestImprovementSat3MinDur()
{
    _bestSlackSat3MinDurSupport.clear();
    IlcFloat bestImprovement = 0.0;

    for(int i=0;i<_sat3IloConstraints.getSize();i++)
    {
        IlcSatelliteConstraint3I* ctr3I = (IlcSatelliteConstraint3I*) _solver.getConstraint(_sat3IloConstraints[i]).getImpl();
        IlcFloat improvement = ctr3I->getRelativeSlack();
        if (improvement>0 && improvement >= bestImprovement)
        {
            if (improvement > bestImprovement)
            {
                _bestSlackSat3MinDurSupport.clear();
                bestImprovement = improvement;
            }

            _bestSlackSat3MinDurSupport[i] = i;
        }
    }

    if (_bestSlackSat3MinDurSupport.size()>0)
    {
        _hasSlackSat3MinDur.setValue(_solver, IlcTrue);

/*        for(std::map<int, int>::iterator it = _bestSlackSat3MinDurSupport.begin(); it!=_bestSlackSat3MinDurSupport.end(); ++it)
        {
            int i =  ((*it).first);
            cout << "SAT3 improvement candidates index " << i << " sur " << _sat3IloConstraints.getSize() << endl;
            IlcSatelliteConstraint3I* ctr3I = (IlcSatelliteConstraint3I*) _solver.getConstraint(_sat3IloConstraints[i]).getImpl();
            ctr3I->displayCandidates();

        }
*/
        return true;
    }
    else
    {
        _hasSlackSat3MinDur.setValue(_solver, IlcFalse);
        return false;
    }

}
/**
 * renvoie le meilleur gain possible si on assignait le slot, le gain étant mesuré comme le maximum
 * parmi les contraintes SAT3 contenant le slot du ratio A/B avec
 * A = écart courant à la durée min
 * B = durée max courante - durée min courante
 * @param slot slot courant dont on examine le gain possible
 * @return la meilleure amélioration possible (entre 0 et 1)
 */
bool OCP_CpModel::hasBestImprovementSat3MinDur(OCP_CpTrackingSlot* slot)
{
     //int globalSlackVal = _solver.getIntExp(_sumCardinalitySlack).getMin();
     //boost::format msg = boost::format( _("getBestImprovement slack %1$d") ) % globalSlackVal;
     //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);


     std::vector<int>& slotSat3Indexes = slot->getSat3Indexes();
     for(std::vector<int>::iterator it = slotSat3Indexes.begin() ; it != slotSat3Indexes.end() ; it++)
     {
         int i = (*it);
         if( _bestSlackSat3MinDurSupport.find(i) != _bestSlackSat3MinDurSupport.end() )
         {
             return true;
         }
     }

     //msg = boost::format( _("best improve %1$f pour %2$d ") ) % bestImprove % slot->getSlotId();
     //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);


     return false;
}
