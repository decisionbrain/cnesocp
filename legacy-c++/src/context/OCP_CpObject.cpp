/*
 * $Id: OCP_CpObject.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpObject.cpp
 *
 */
#include <string>

#include "context/OCP_CpObject.h"
#include "OCP_CpManager.h"
#include "planif/OCP_CpTimeSlot.h"
#include "OCP_CpDate.h"
#include "resources/OCP_CpSatellite.h"
#include "planif/OCP_CpTrackingSlot.h"

using namespace std;

OCP_CpObject::OCP_CpObject(){
	setName("No name for me...");
	OCP_CpManager* cpManager = OCP_CpManager::getInstance();
	_id = cpManager->getCurrentId();
}

OCP_CpObject::OCP_CpObject(string newName){
    setName(newName);
    OCP_CpManager* cpManager = OCP_CpManager::getInstance();
    _id = cpManager->getCurrentId();

}

void OCP_CpObject::setName(string newName) {
	_name = newName;
}
