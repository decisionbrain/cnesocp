/*
 * $Id: OCP_CpSumSlotConstraint.cpp 942 2010-12-21 13:11:22Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * VERSION : 2-0 : FA : artf660903 : 30/11/2010 : Calcul du planning apr�s r�servation manuelle de cr�neaux NOK (https://coconet2.capgemini.com/sf/go/artf660903/")
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSumSlotConstraint.cpp
 */

/*
*	OCP_CpSumSlotConstraint: lien entre les slots et une variable comptabilisant la somme des dur�es de ces slots
*/
#include <planif/OCP_CpTrackingSlot.h>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

class IlcSumSlotConstraintI : public IlcConstraintI {
protected:
  IlcAnySetVar _set;
  IlcIntVar _cumul;
  OCP_CpTimeSlotPtr _horizon;
 public:
  IlcSumSlotConstraintI(IloSolver s, IlcAnySetVar x, IlcIntVar y,OCP_CpTimeSlotPtr horizon) :
      IlcConstraintI(s), _set(x), _cumul(y),_horizon(horizon) {}
  ~IlcSumSlotConstraintI() {}
  virtual void post();
  virtual void propagate();
};

void IlcSumSlotConstraintI::post () {
	_set.whenDomain(this);
	// TODO: voir comment reduire les slots a l'appel de post en supprimant les slots dont la duree depasse le max...
	// TODO: voir comment propager si la valeur max diminue...
	// _cumul.whenDomain(this); 
}

void IlcSumSlotConstraintI::propagate () 
{
	if(!_set.isInProcess()) return;

	for(IlcAnyDeltaRequiredIterator iterRequired(_set); iterRequired.ok(); ++iterRequired) 
	{
	    OCP_CpTimeSlot* newRequired =(OCP_CpTimeSlot*) *iterRequired;
	    IlcInt currentDuration = 0;
	    if (newRequired->isTrackingSlot())
	    {
	        OCP_CpTimeSlotPtr amplitude = ((OCP_CpTrackingSlot*) newRequired)->getAmplitude();
	        currentDuration = amplitude->getIntersectionDuration(_horizon);
	    }
	    else
	        currentDuration = (newRequired)->getIntersectionDuration(_horizon);
		_cumul.setMin(_cumul.getMin()+ currentDuration);
	}
}

IlcConstraint IlcSumSlotConstraint(IloSolver s, IlcAnySetVar x, IlcIntVar y, OCP_CpTimeSlotPtr horizon) {
  return new (s.getHeap()) IlcSumSlotConstraintI(s, x, y,horizon);
}

ILOCPCONSTRAINTWRAPPER3(IloSumSlotConstraint, solver, IloAnySetVar, _var1, IloIntVar, _var2, OCP_CpTimeSlotPtr, horizon) {
  use(solver, _var1);
  use(solver, _var2);
  return IlcSumSlotConstraint(solver, solver.getAnySetVar(_var1), solver.getIntVar(_var2),horizon);
}
