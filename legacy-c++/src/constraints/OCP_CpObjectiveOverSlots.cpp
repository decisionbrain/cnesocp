/*
 * $Id: OCP_CpObjectiveOverSlots.cpp 926 2010-11-12 14:56:15Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpObjectiveOverSlots.cpp
 */

/*
*	OCP_CpObjectiveOverSlots: définir une variable maintenant la somme des poids des slots de trackings
*/
#include <planif/OCP_CpTrackingSlot.h>
#include <constraints/OCP_CpObjectiveOverSlots.h>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN


ILCCTDEMON0(PropagateSetOnCumulWithDemon, IlcObjectiveOverSlotsConstraintI, propagateSetOnCumul);

void IlcObjectiveOverSlotsConstraintI::post () {
	_set.whenDomain(PropagateSetOnCumulWithDemon(getSolver(), this));
}



/**
 * pour chacun des slots, calcule son poids correspondant dans la fonction objectif
 * @param slot le slot candidat, tracking ou maintenance
 * @return le poids du slot candidat dans la fonction objectif
 */
int IlcObjectiveOverSlotsConstraintI::computeWeight(IlcAny slot)
{
    if (! ((OCP_CpTimeSlot*) slot)->isTrackingSlot())
        return 0;
    else
    {
        int result = 0;
        OCP_CpTrackingSlot* slotT = (OCP_CpTrackingSlot*) slot;
        int prioSAT4 = slotT->getSearchPriority();
        int prioSTA6 = slotT->getStationPriority();
        if (prioSAT4>0)
            result += 10 - prioSAT4;
        if (prioSTA6>0 )
            result += 10 - prioSTA6;
        return result;
    }
}


void IlcObjectiveOverSlotsConstraintI::propagate ()
{
	//if(!_set.isInProcess()) return;

    int sumMin = 0;
    int sumMax = 0;
    for(IlcAnySetIterator iterPossible(_set.getPossibleSet()); iterPossible.ok(); ++iterPossible)
    {
        IlcAny slot = *iterPossible;
        int inObj = computeWeight(slot);

        sumMax += inObj;
        if (_set.isRequired(slot))
            sumMin += inObj;

	}
    _cumul.setMin(sumMin);
    _cumul.setMax(sumMax);
}

/**
 * propagation incrémentale : toute modification du domaine de la variable ensembliste modifie les bornes du cumul
 */
void IlcObjectiveOverSlotsConstraintI::propagateSetOnCumul ()
{
    //if(!_set.isInProcess()) return;

    for(IlcAnyDeltaRequiredIterator iterRequired(_set); iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        int inObj = computeWeight(newRequired);
        _cumul.setMin(_cumul.getMin()+  inObj);
    }

    for(IlcAnyDeltaPossibleIterator iterPossible(_set); iterPossible.ok(); ++iterPossible)
    {
        IlcAny oldPossible = *iterPossible;
        int inObj = computeWeight(oldPossible);
        _cumul.setMax(_cumul.getMax() - inObj );
    }

}


IlcConstraint IlcObjectiveOverSlotsConstraint(IloSolver s, IlcAnySetVar x, IlcIntVar y) {
  return new (s.getHeap()) IlcObjectiveOverSlotsConstraintI(s, x, y);
}

ILOCPCONSTRAINTWRAPPER2(IloObjectiveOverSlotsConstraint, solver, IloAnySetVar, _var1, IloIntVar, _var2) {
  use(solver, _var1);
  use(solver, _var2);
  return IlcObjectiveOverSlotsConstraint(solver, solver.getAnySetVar(_var1), solver.getIntVar(_var2));
}
