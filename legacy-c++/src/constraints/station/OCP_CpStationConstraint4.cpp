/*
 * $Id: OCP_CpStationConstraint4.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint4.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpStationConstraint4.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpStation.h"

using namespace std;

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	create a new setVar with all tracking slots verifying the condition (time interval)
	compute the intersection between this setVar and the original one
	store the cardinality constraint on this setVar using parameter n
	save the constraint by setIloConstraint
	take care, the constraint is not stored in the model!
*/	


/**
   * Constructeur à partir de la contrainte métier
   * @param period période d'instanciation de la contrainte
   * @param ctrSat1 contrainte métier origine
   * @param station station associé à la contrainte
   * @return
   */
OCP_CpStationConstraint4::OCP_CpStationConstraint4(OCP_CpTimeSlotPtr period, OcpConstraintStaType4Ptr ctrSat4,OCP_CpStationPtr station)
:OCP_CpStationBaseConstraint(period,ctrSat4,station)
{

    _maxPassages = ctrSat4->getMaxFlightPass();

    boost::format msg = boost::format(_("StationConstraint4-%1$s-(%2$d)")) % station->getName().c_str() % _maxPassages ;
    setName(msg.str());
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());

}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpStationConstraint4::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();


    for(int i=0; i<getStation()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getStation()->getTrackingSlots()[i]);
        if(!tmp->toRemove()
                && !tmp->hasBookingOrder()
                && tmp->getAmplitude()->isInside( optimScope ))
        {
            addCandidateToDomain(tmp);
        }
        else
        {
            _complementarySlots.add(tmp);
        }
    }

    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots, _domainSlots);
    
    // creating anySetVar (intersect) with all slots possible
    boost::format intersectName = boost::format( _("MyIntersectSTA4-%1$s--%2$s ") ) % getStation()->getName().c_str() %  getId();
    IloAnySetVar intersect(env, getStation()->getTrackingSlots(),intersectName.str().c_str());

    // intersect = TrackingSlotSetVar from the current station INTER goodTrackingSlotsVar
    IloConstraint c1 =  IloEqIntersection(env, intersect, getStation()->getTrackingSlotSet(), goodTrackingSlotsVar);

    boost::format ctrName = boost::format( _("MyIloEqIntersectionSTA4_%1$s_%2$d") )
    % getStation()->getName() % getId() ;
    c1.setName(ctrName.str().c_str());


    // the cardinality of intersect <= _maxPassages
    //TODO FOR TESTING: modify the operator >= to == exactly n tracking slot for the current satellite
    IloConstraint c2 =  (IloCard(intersect) <= _maxPassages);

    setIloConstraint(c1 && c2);

    getStation()->addOcpConstraint(getIloConstraint());
}
