/*
 * $Id: OCP_CpStationConstraint2.cpp 943 2010-12-21 13:12:51Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint2.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpStationConstraint2.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpMaintenanceSlot.h"
#include "resources/OCP_CpStation.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpStationConstraint2);

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	create a new setVar with all maintenance slots verifying the condition (time interval and duration)
	compute the intersection between this setVar and the original one
	store the cardinality constraint on this setVar using parameter n
	save the constraint by setIloConstraint
	take care, the constraint is not stored in the model!
*/	


/**
   * Constructeur à partir de la contrainte métier
   * @param optimScope période d'instanciation de la contrainte
   * @param ctrSat1 contrainte métier origine
   * @param station station associé à la contrainte
   * @return
   */
OCP_CpStationConstraint2::OCP_CpStationConstraint2(OCP_CpTimeSlotPtr optimScope, OcpConstraintStaType2Ptr ctrSta2,OCP_CpStationPtr station)
:OCP_CpStationBaseConstraint(optimScope,ctrSta2,station)
{
    static const std::string pseudoClazz = _Clazz + "::OCP_CpStationConstraint2";

    if(station->isClosed(optimScope->getBegin()))
    {
        PHRDate aBegin(optimScope->getBegin());
        boost::format msg = boost::format( _("StationConstraint2-%1$s-  Station fermée le %2$s : la contrainte sera ignorée sur la fenêtre de calcul courante") )
        %  station->getName().c_str() % aBegin.getStringValue();
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg ) ;
        setNoInstance();
        return;
    }

    _durationMin = 0;
    try
    {
        _durationMin = (OCP_CpDate) 60 * ctrSta2->getMinDuration();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire quand les champs ne sont pas lus : on se contentera des valeurs par défaut
    }

    if(_durationMin <=0 )
    {
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tContrainte toujours vraie, car durée min demandée %1$i")) % _durationMin );
        setNoInstance();
        return;
    }


    _offVisibility = false;
    try
    {
        _offVisibility = ctrSta2->getOutOfVisibility();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire quand les champs ne sont pas lus : on se contentera des valeurs par défaut
    }

    //_slotType = NULL;
    try
    {
        _slotType = ctrSta2->getSlotType();
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("TYPE_CRENEAU non renseigné sur %1$s ") )
        %  station->getName()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg );
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }

    _maintenanceScope = OCP_CpTimeSlotPtr ( new OCP_CpTimeSlot(*optimScope.get()) );
    VectorOCP_CpTimeSlot plageHoraires;
    try
    {
        OcpTimePeriodPtr offsetInterval = ctrSta2->getConstraintRange();

        OCP_CpTimeSlot tmp(offsetInterval);
        OCP_CpConstraint::buildTimeAreas(plageHoraires , optimScope->getBegin(),optimScope->getEnd(), tmp.getBegin(), tmp.getEnd());
        if(plageHoraires.size() != 1)
        {
            /*
             * les maintenances ne sont générées que dans la fenêtre jour, il est donc impossible de traiter correctement
             * des plages à cheval
             */
            boost::format msg = boost::format(_("Contrainte non respectée pour la station %1$s :OCP_CpStationConstraint2 plage horaire à cheval entre deux jours non autorisée"))
                % station->getName() ;
            OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg.str());
            throw OCP_XRoot(msg.str());
        }
        VectorOCP_CpTimeSlot::const_iterator it = plageHoraires.begin();
        _maintenanceScope = *it;
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire : on garde la période de la fenêtre de calcul
    }

    /*
     * la plage horaire et la durée min permettent de cocher de nouveaux points de maintenance potentiels sur la station
     * afin de générer des slots de maintenance ad-hoc pour cette contrainte
     */
    updateStationMaintenanceTimePoints();

    /*
     * affichage et nommage
     */

    PHRDate plageBegin(_maintenanceScope->getBegin() , PHRDate::_TimeFormat);
    PHRDate plageEnd(_maintenanceScope->getEnd() , PHRDate::_TimeFormat);
    PHRDate filterDurationMin(_durationMin , PHRDate::_TimeFormat);
    boost::format msg = boost::format(_("StationConstraint2-%1$s-(filtre durée min %2$s) offVisibility %3$d plage %4$s %5$s"))
            % station->getName().c_str() % filterDurationMin.getStringValue() %  _offVisibility % plageBegin.getStringValue() % plageEnd.getStringValue();
    setName(msg.str());
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());
}

/**
 * prendre en compte la durée min et les plages horaires de la contrainte pour la mise à jour des créations de slots de maintenance
 */
void OCP_CpStationConstraint2::updateStationMaintenanceTimePoints()
{
    OCP_CpStationPtr aStation = getStation();
    OCP_CpDate mBegin = _maintenanceScope->getBegin();
    OCP_CpDate mEnd = _maintenanceScope->getEnd();

    /*
     * création d'intervalles de durée _durationMin par pas de _durationMin depuis le début de la plage
     */
    OCP_CpDate forwardBegin = mBegin;
    do
    {
        OCP_CpDate forwardEnd =  forwardBegin + _durationMin;
        aStation->addSpecialTimePoints(forwardBegin , forwardEnd);
        forwardBegin += _durationMin;
    }
    while (forwardBegin < mEnd);

    /*
     * création d'intervalles de durée _durationMin par pas de _durationMin depuis la fin de la plage
     */
    OCP_CpDate backwardEnd  = mEnd;
    do
    {
        OCP_CpDate backwardBegin = backwardEnd - _durationMin;
        aStation->addSpecialTimePoints(backwardBegin , backwardEnd);
        backwardEnd -= _durationMin;
    }
    while(backwardEnd >= mBegin);

}


/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpStationConstraint2::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();
    for(int i=0; i<getStation()->getMaintenanceSlots().getSize(); i++)
    {
        OCP_CpMaintenanceSlot* tmp = (OCP_CpMaintenanceSlot*)(getStation()->getMaintenanceSlots()[i]);

        if( tmp->isInside( _maintenanceScope )
                && tmp->getIntersectionDuration(_maintenanceScope ) >= _durationMin
                // filtering offVisibility
                &&(!_offVisibility || tmp->isOffVisibility())
                && !tmp->hasBookingOrder()

        )
        {
            addCandidateToDomain(tmp);
        }
        else
        {
            _complementarySlots.add(tmp);
        }
    }


    if(_domainSlots.getSize()==0)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour la station %2$s :StationConstraint2-%1$s- : aucun candidat ") )
                %  getName() % getStation()->getName();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );
        throw OCP_XRoot( msg.str() );
    }


    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodMaintenanceSlotsVar(env, _domainSlots, _domainSlots);

    // creating anySetVar (intersect) with all slots possible
    boost::format intersectName = boost::format( _("MyIntersectSTA2-%1$s--%2$s ") ) % getStation()->getName().c_str() %  getId();
    IloAnySetVar intersect(env, getStation()->getMaintenanceSlots(),intersectName.str().c_str());

    // intersect = MaintenanceSlotSetVar from the current getStation() INTER goodMaintenanceSlotsVar
    IloConstraint c1 =  IloEqIntersection(env, intersect, getStation()->getMaintenanceSlotSet(), goodMaintenanceSlotsVar);
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSTA2_%1$s_%2$d") )
    % getStation()->getName() % getId() ;
    c1.setName(ctrName.str().c_str());

    //cpModel->addOneSetVarToTrace(intersect);

    IloConstraint c2 =  (IloCard(intersect) == 1);
    //IloConstraint c3 =  (IloCard(intersect) <= 2);//tolérance sur la cardinalité

    setIloConstraint(c1 && c2 /*&& c3*/);

    getStation()->addOcpConstraint(getIloConstraint());

}
/**
 * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
 * @param slot le créneau à sauver
 */
void OCP_CpStationConstraint2::postOptim(OCP_CpTimeSlot* slot)
{
    /*
     * positionnement du TYPE_CRENEAU
     */
    OCP_CpConstraint::setSupportType(slot , _slotType);
}
