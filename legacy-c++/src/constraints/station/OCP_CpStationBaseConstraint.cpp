/*
 * $Id: OCP_CpStationBaseConstraint.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationBaseConstraint.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpStationBaseConstraint.h"
#include "constraints/OCP_CpConstraint.h"
#include "OCP_CpDate.h"
#include "resources/OCP_CpStation.h"

using namespace std;


/**
     * constructeur de la classe mère des contraintes station
     * @param period période d'instanciation de la contrainte
     * @param origine contrainte métier origine
     * @param station station associée
     * @return
     */
OCP_CpStationBaseConstraint::OCP_CpStationBaseConstraint(OCP_CpTimeSlotPtr period, OcpConstraintPtr origine,OCP_CpStationPtr station):
    OCP_CpConstraint(period,origine), _station(station)
{
}



