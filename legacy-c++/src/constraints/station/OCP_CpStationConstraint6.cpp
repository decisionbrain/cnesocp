/*
 * $Id: OCP_CpStationConstraint6.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint6.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpStationConstraint6.h"
#include "OCP_CpDate.h"
#include "context/OCP_CpModel.h"
#include "resources/OCP_CpStation.h"
#include "planif/OCP_CpTrackingSlot.h"

using namespace std;


/**
   * Constructeur à partir de la contrainte métier
   * @param period période d'instanciation de la contrainte
   * @param ctrSat1 contrainte métier origine
   * @param station station associé à la contrainte
   * @return
   */
OCP_CpStationConstraint6::OCP_CpStationConstraint6(OCP_CpTimeSlotPtr period, OcpConstraintStaType6Ptr ctrSat6,OCP_CpStationPtr station)
:OCP_CpStationBaseConstraint(period,ctrSat6,station)
 {

    boost::format msg = boost::format(_("StationConstraint6-%1$s- with priorities on satellite "))
        % station->getName().c_str() ;

    try
    {
        _satelliteProperties = ctrSat6->getSatellitesProperties();
    }

    catch(OcpXMLInvalidField& e )
    {
        boost::format errMsg = boost::format(_("%1$s pas de propriété  de stations renseignées sur la contrainte %1$s")) % msg.str();
        throw OCP_XRoot(errMsg.str() );
    }

    /*
     * affichage et nommage
     */
    for(VectorOcpSatelliteProperties::iterator it=_satelliteProperties.begin() ; it != _satelliteProperties.end() ; it++)
    {
        OcpSatellitePropertiesPtr property = (*it);
        OcpSatellitePtr aSat = property->getSatellite();
        try
        {
            uint16_t satellitePriority = property->getSatellitePriority();
            msg = boost::format(_("%1$s [%2$s %3$d]")) % msg.str() % aSat->getName().c_str() % satellitePriority;
        }
        catch(OcpXMLInvalidField& e )
        {
            boost::format warnsMsg = boost::format(_("OCP_CpStationConstraint6::instantiate propriété incorrecte liée au satellite %1$s non reconnu dans la liste paramétrée des satellites "))
            % aSat->getName();
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, warnsMsg.str());
        }
    }

    setName(msg.str());
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());

 }

/**
 * indique si le satellite apparait dans la liste des propriétés de la station
 * @param aSat : satellite à examiner
 * @return vrai ssi le satellite apparait dans la liste des propriétés de la station
 */
bool OCP_CpStationConstraint6::concerns(OcpSatellitePtr aSat ) const
{
    for(VectorOcpSatelliteProperties::const_iterator it=_satelliteProperties.begin() ; it != _satelliteProperties.end() ; it++)
    {
        OcpSatellitePropertiesPtr property = (*it);
        OcpSatellitePtr aSatCurrent = property->getSatellite();
        if(aSat == aSatCurrent)
            return true;
    }
    return false;
}


/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpStationConstraint6::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

    /*
      * parcours des propriétés satellites
      */

     boost::format prefixSatellites = boost::format(_(" Satellites "));
     for(VectorOcpSatelliteProperties::iterator it=_satelliteProperties.begin() ; it != _satelliteProperties.end() ; it++)
     {
         OcpSatellitePropertiesPtr property = (*it);
         OcpSatellitePtr aSat = property->getSatellite();
         try
         {

             uint16_t satellitePriority = property->getSatellitePriority();
             if (satellitePriority <=0 )
             {
                 boost::format errMsg = boost::format(_(" OCP_CpStationConstraint6::instantiate station %1$s niveau de priorité %2$d  du satellite %3$s n'est pas une valeur autorisée "))
                     % getStation()->getName() % satellitePriority % aSat->getName() ;
                 throw OCP_XRoot(errMsg.str());
             }

             OCP_CpSatellitePtr currentSatellite = cpModel->getSatellite(aSat);
             prefixSatellites = boost::format(_(" %1$s %2$s ")) % prefixSatellites.str() % currentSatellite->getName() ;
             for(int i=0; i<currentSatellite->getTrackingSlots().getSize(); i++)
             {
                 OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(currentSatellite->getTrackingSlots()[i]);
                 if (getStation() == tmp->getStation() )
                 {
                     /*
                      *lire sur la propriété satellite le niveau de priorité de ce satellite pour la station courante
                      */

                     tmp->setPriorityStation(satellitePriority);
                 }
             }
         }
         catch(OcpXMLInvalidField& e )
         {
             boost::format warnsMsg = boost::format(_("OCP_CpStationConstraint6::instantiate propriété incorrecte liée au satellite %1$s non reconnu dans la liste paramétrée des satellites "))
             % aSat->getName();
             OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, warnsMsg.str());
         }
         catch(OCP_XRoot& e )
         {
             boost::format warnsMsg = boost::format(_("OCP_CpStationConstraint6::instantiate le satellite %1$s est concerné par la contrainte mais non chargé dans les paramètres "))
             % aSat->getName();
             OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, warnsMsg.str());
         }


    }

    //pas d'implémentation ILOG SOLVER pour cette contrainte

}
