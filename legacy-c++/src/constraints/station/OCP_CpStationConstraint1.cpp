/*
 * $Id: OCP_CpStationConstraint1.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint1.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpStationConstraint1.h"
#include "OCP_CpDate.h"
#include "context/OCP_CpModel.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpExternWrapper.h"
#include <ConfAccel.h>
#include "planif/OCP_CpMaintenanceSlot.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpStationConstraint1);

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	create a new intVar with domain from (minimal duration)..(sum of all maintenance duration in the interval)
	at each removing operation from the possible set, reduce the max with the duration of the lost slot if it was in the interval
	save the constraint by setIloConstraint
	take care, the constraint is not stored in the model!
*/	

/**
   * Constructeur à partir de la contrainte métier
   * @param optimScope période d'instanciation de la contrainte
   * @param ctrSta1 contrainte métier origine
   * @param station station associé à la contrainte
   * @return
   */
OCP_CpStationConstraint1::OCP_CpStationConstraint1(OCP_CpTimeSlotPtr optimScope, OcpConstraintStaType1Ptr ctrSta1,OCP_CpStationPtr station)
:OCP_CpStationBaseConstraint(optimScope,ctrSta1,station)
{
    static const std::string pseudoClazz = _Clazz + "::OCP_CpStationConstraint1";

    _cumulMin = 0;
    _interSlotMinDuration = 0;

    if(station->isClosed(optimScope->getBegin()))
    {
        PHRDate aBegin(optimScope->getBegin());
        boost::format msg = boost::format( _("StationConstraint1-%1$s-  Station fermée le %2$s : la contrainte sera ignorée sur la fenêtre de calcul courante") )
        %  station->getName().c_str() % aBegin.getStringValue();
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg ) ;
        setNoInstance();
        return;
    }

    try
    {
        _cumulMin = (OCP_CpDate) 60 * ctrSta1->getCombinedDuration();
        float sta1Tolerance = ConfAccel::getInstance()->getToleranceSta1();
        _cumulMax = (OCP_CpDate) ( (1+sta1Tolerance)*_cumulMin );

    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("StationConstraint1-%1$s- borne min de durée cumulée non renseignée : contrainte ignorée ") )
        %  station->getName().c_str();
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg ) ;
        setNoInstance();
        return;
    }
    try
    {
        _interSlotMinDuration = (OCP_CpDate) ctrSta1->getMinGap() * 3600;
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }

    /*
     * affichage et nommage
     */
    boost::format msg = boost::format( _("StationConstraint1-%1$s- cumulMin %2$i  cumulMax %3$i ") )
            %  station->getName() % _cumulMin % _cumulMax;
    setName(msg.str().c_str());
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());
    setName(msg.str());

    /*
     * affichage warning si on n'est pas le dernier jour
     */
    PHRDate aDate(optimScope->getBegin());
    if(!getStation()->isLastOpenDayInItsWeek(optimScope->getBegin() ))
    {
        msg = boost::format( _("La partie de la contrainte relative à la durée minimum ne sera pas traitée sur ce jour car ce n'est pas le dernier jour de la semaine où la station est ouverte ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
    }

    //_slotType = NULL;
    try
    {
        _slotType = ctrSta1->getSlotType();
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("TYPE_CRENEAU non renseigné sur %1$s ") )
        %  station->getName()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg );
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }


}

/**
 * cumul des maintenances passés sur les 6 premiers jours de la semaine
 * @param optimScope fenêtre de calcul courante
 * @return la durée cumulée de maintenance
 */
int OCP_CpStationConstraint1::computeCumulDone(OCP_CpTimeSlotPtr optimScope)
{
    /*
     * récupérer les maintenances passées : uniquement celles dans le jour pour la partie interSlotMinDuration
     * et toutes celles du lundi au dimanche si on est le dimanche. Rappel : pas de gestion de maintenance préfixées dans la création
     * des slots de maintenance.
     */

    /*
     * cumul déjà effectué sur les jours précédents
     */
    OCP_CpDate cumulDone = 0;

    PHRDate aDate(optimScope->getBegin());

    int nbDaysBefore = 0;
    switch(aDate.getDayInWeek())
    {

        case WeekDays::MONDAY:nbDaysBefore=0;break;
        case WeekDays::TUESDAY:nbDaysBefore=1;break;
        case WeekDays::WEDNESDAY:nbDaysBefore=2;break;
        case WeekDays::THURSDAY:nbDaysBefore=3;break;
        case WeekDays::FRIDAY:nbDaysBefore=4;break;
        case WeekDays::SATURDAY:nbDaysBefore=5;break;
        case WeekDays::SUNDAY:nbDaysBefore=6;break;
        default : nbDaysBefore=0;break;

    }



    //récupérer tous les slots de maintenance mémorisés sur la semaine et sommer leurs durées déjà accomplie
    PHRDate beginWeek(optimScope->getBegin() - nbDaysBefore * PHRDate::SECONDS_IN_A_DAY);
    OcpContextPtr context = OcpXml::getInstance()->getContext();
    VectorStationSlot maintenancePast = context->getStationSlotsByPeriod(beginWeek, aDate, getStation()->getOrigin());
    for(VectorStationSlot::iterator it = maintenancePast.begin() ; it != maintenancePast.end() ; it++)
    {
        OcpStationSlotPtr maintenanceSlot = (*it);

        bool reserved = maintenanceSlot->getBookingState()  == SlotBookingStates::RESERVED || maintenanceSlot->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
        bool hasBookingOrder = false;
        try
        {
            hasBookingOrder = maintenanceSlot->hasBeenBookedByRequest();
        }
        catch(OcpXMLInvalidField)
        {
            //rien à faire quand les champs ne sont pas lus : on se contentera des valeurs par défaut
        }

        if (reserved && !hasBookingOrder)
        {
            OCP_CpTimeSlot slot ( maintenanceSlot->getStartEnd() );
            cumulDone += slot.getDuration();
        }
    }

    return cumulDone;
}

/**
* création de la contrainte ILOG SOLVER. La partie interSlotMinDuration est posée sur chaque fenêtre de calcul tandis
* que la partie durée cumulée ne peut être posée que le dimanche
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpStationConstraint1::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    PHRDate aDate(optimScope->getBegin());
    int cumulDone = 0;
    bool isLastDay = getStation()->isLastOpenDayInItsWeek(optimScope->getBegin());//le dernier jour, on poste la contrainte de cumul
    bool isMonday = aDate.getDayInWeek() == WeekDays::MONDAY;//hors le lundi, on peut regarder le cumul des jours précédents
    if (!isMonday)
        cumulDone = computeCumulDone(optimScope);

    /*
     * maximorum de la durée de maintenance sur le jour courant
     */
    int maximorum = getStation()->getPossibleMaintenanceDuration(optimScope->getBegin() , optimScope->getEnd() );

    OCP_CpDate remainMin = _cumulMin-cumulDone;

    if (cumulDone > _cumulMax)//quel que soit le jour, il y a erreur si on dépasse la borne max
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour la station %1$s : StationConstraint1-%2$s ") )
        %  getName() %  getStation()->getName() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );
        PHRDate cumulDoneMinutes(cumulDone,PHRDate::_TimeFormat);
        PHRDate cumulMaxMinutes(_cumulMax,PHRDate::_TimeFormat);
        msg = boost::format( _("La contrainte imposant au plus  %1$s de maintenance par semaine ne peut être satisfaite. Maintenance réalisée=%2$s ") )
         % cumulMaxMinutes.getStringValue() % cumulDoneMinutes;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );
        throw OCP_XRoot( msg.str() );

    }
    else if(isLastDay && remainMin > maximorum)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour la station %1$s : StationConstraint1-%2$s- : contrainte de cumul impossible à satisfaire le dernier jour ouvré") )
        %  getName() %  getStation()->getName() ;
        // cumulDone %2$i _cumulMin %3$i restant possible maximorum de maintenance %4$i  cumulDone % _cumulMin % maximorum %
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );

        PHRDate cumulDoneMinutes(cumulDone,PHRDate::_TimeFormat);
        PHRDate cumulMinMinutes(_cumulMin,PHRDate::_TimeFormat);
        PHRDate remainMinMinutes(remainMin,PHRDate::_TimeFormat);
        PHRDate maximorumMinutes(maximorum,PHRDate::_TimeFormat);

        msg = boost::format( _("La contrainte imposant au moins  %1$s de maintenance par semaine ne peut être satisfaite. Maintenance réalisée=%2$s, Maintenance restant à réalisé= %3$s. Maintenance possible sur la journée = %4$s ") )
        % cumulMinMinutes.getStringValue() % cumulDoneMinutes.getStringValue() % remainMinMinutes.getStringValue()  % maximorumMinutes.getStringValue() ;
        throw OCP_XRoot( msg.str() );
    }

    else if (isLastDay && cumulDone >= _cumulMin && cumulDone+maximorum <= _cumulMax)
    {
        /*
         * on pourrait garder la contrainte interSlotMinDuration mais elle n'est là que pour aider le cumul
         */
        PHRDate cumulMinMinutes(_cumulMin,PHRDate::_TimeFormat);
        PHRDate cumulMaxMinutes(_cumulMax,PHRDate::_TimeFormat);
        PHRDate cumulDoneMinutes(cumulDone,PHRDate::_TimeFormat);
        PHRDate maximorumMinutes(maximorum,PHRDate::_TimeFormat);

        boost::format msg = boost::format( _("StationConstraint1-%1$s- : contrainte de cumul d'au moins %2$s et d'au plus %3$s toujours vraie :  Maintenance réalisée=%4$s  Maintenance possible sur la journée = %5$s") )
                %  getName() % cumulMinMinutes.getStringValue() % cumulMaxMinutes.getStringValue() % cumulDoneMinutes.getStringValue() % maximorumMinutes.getStringValue() ;
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg );
    }
    else
    {
        /*
         * traitement final de la contrainte sur la portée courante
         */
        int maxTarget = _cumulMax - cumulDone ; //>0 borne max de maintenance sur la portée courante
        int minTarget = 0;//borne min sur la portée courante, 0 sauf le dernier jour ouvré où l'on doit faire le minimum demandé
        if(isLastDay && _cumulMin > cumulDone)
            minTarget = _cumulMin - cumulDone ;

        PHRDate cumulMinMinutes(_cumulMin,PHRDate::_TimeFormat);
        PHRDate cumulMaxMinutes(_cumulMax,PHRDate::_TimeFormat);
        PHRDate targetMinMinutes(minTarget,PHRDate::_TimeFormat);
        PHRDate targetMaxMinutes(maxTarget,PHRDate::_TimeFormat);
        PHRDate cumulDoneMinutes(cumulDone,PHRDate::_TimeFormat);

        boost::format msg = boost::format( _("StationConstraint1-%1$s- : traduction de la contrainte semaine (d'au moins %2$s et d'au plus %3$s) sur la fenêtre courante cible min= %4$s objectif cible max= %5$s (Maintenance réalisée= %6$s)") )
        %  getName()
        % cumulMinMinutes
        % cumulMaxMinutes
        % targetMinMinutes
        % targetMaxMinutes
        % cumulDoneMinutes;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

        instantiateWithFilter(optimScope , minTarget , maxTarget );
    }
}


/**
 * instanciation finale de la contrainte avec reconnaissance des booking order
 * @param optimScope : fenêtre de calcul courante
 * @param minTarget : cible min de durée de maintenance
 * @param maxTarget : cible max de durée de maintenance
 */
void OCP_CpStationConstraint1::instantiateWithFilter(OCP_CpTimeSlotPtr optimScope,int minTarget, int maxTarget)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();
    int maximorum = 0;

    /*
     * collecter les bons créneaux de maintenance candidats (hors booking order)
     */
    int nbCandidates = 0;
    for(int i=0;i < getStation()->getMaintenanceSlots().getSize() ; i++)
    {
        OCP_CpMaintenanceSlot* mslot = (OCP_CpMaintenanceSlot*) getStation()->getMaintenanceSlots()[i];
        if (mslot->hasBookingOrder() || mslot->getDuration()>maxTarget)
        {
            _complementarySlots.add(mslot);
        }
        else
        {
            addCandidateToDomain(mslot);
            nbCandidates++;
            maximorum += mslot->getDuration();
        }
    }

    boost::format msg = boost::format( _("StationConstraint1-%1$s- : instantiateWithFilter minTarget (%2$d secondes) maxtarget (%3$d secondes) %4$d candidats avec un maximorum de %5$d") )
    %  getName() % minTarget % maxTarget % nbCandidates % maximorum ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );


    /*
     * poser la contrainte sur les bons créneaux candidats
     */
    IloAnySetVar goodMaintenanceSlotsVar(env, _domainSlots, _domainSlots);
    // anySetVar (intersect) avec tous les slots possibles
    
    boost::format intersectName = boost::format( _("MyIntersectSTA1-%1$s--%2$s ") ) % getStation()->getName().c_str() %  getId();
    IloAnySetVar intersect(env, getStation()->getMaintenanceSlots(),intersectName.str().c_str());

    // intersect = MaintenanceSlotSetVar de la station INTER goodMaintenanceSlotsVar (les candidats)
    IloConstraint c1 =  IloEqIntersection(env, intersect, getStation()->getMaintenanceSlotSet(), goodMaintenanceSlotsVar);
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSTA1_%1$s_%2$d") )
    % getStation()->getName() % getId() ;
    c1.setName(ctrName.str().c_str());

    //IloConstraint c2 = IloStationConstraint1(env, optimScope,isLastDay, intersect , minTarget , maxTarget, _interSlotMinDuration);
    /*
     * les contraintes satellite IloSatelliteConstraint2 et IloSatelliteConstraint3 sont valables également lorsque les slots candidats
     * sont des slots de maintenance
     */

    IloConstraint c2 = IloSatelliteConstraint2(env , intersect , _interSlotMinDuration);
    SpecialDurationPtr noSpecialDurations();
    IloConstraint c3 = IloSatelliteConstraint3(env,  minTarget , maxTarget, intersect, maximorum , SpecialDurationPtr());



    setIloConstraint(c1 && c2 && c3 ) ;
    getStation()->addOcpConstraint(getIloConstraint());

}

/**
 * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
 * @param slot le créneau à sauver
 */
void OCP_CpStationConstraint1::postOptim(OCP_CpTimeSlot* slot)
{
    /*
     * positionnement du TYPE_CRENEAU
     */
    OCP_CpConstraint::setSupportType(slot , _slotType);
}

/**
 * tient compte des propriétés de durée min et max sur la fenêtre courante, ainsi que de l'écart interslot, pour positionner
 * des points de temps additionnels permettant la création de slots de maintenance candidats pour la contrainte
 */
void OCP_CpStationConstraint1::updateStationMaintenanceTimePoints()
{
    std::vector<OCP_CpDate> previousBegins = getStation()->getBeginsForMaintenance();
    std::vector<OCP_CpDate> previousEnds = getStation()->getEndsForMaintenance();
    std::vector<OCP_CpDate> newBegins;
    std::vector<OCP_CpDate> newEnds;

    /*
     * on parcourt les "fins" existantes : un début à _cumulMin/_cumulMax de cette fin permet de construire un candidat
     * [fin,debut) . On rajoute après ces "début" l'écart interduration pour pouvoir construire une nouvelle "fin" pour pouvoir recoller
     * de nouveaux intervalles démarrant juste après cet écart
     */

    for(std::vector<OCP_CpDate>::iterator itEnd = previousEnds.begin();itEnd != previousEnds.end() ; itEnd++)
    {
        OCP_CpDate aEnd = (*itEnd);
        OCP_CpDate aBegin1 = aEnd + _cumulMin;
        OCP_CpDate aEnd1 = aBegin1 + _interSlotMinDuration;
        OCP_CpDate aBegin2 = aEnd + _cumulMax;
        OCP_CpDate aEnd2 = aBegin2 + _interSlotMinDuration;

        newBegins.push_back(aBegin1);
        newBegins.push_back(aBegin2);
        newEnds.push_back(aEnd1);
        newEnds.push_back(aEnd2);

    }

    /*
     * on parcourt les "débuts" existants : une "fin" à -_cumulMin/-_cumulMax de ce début permet de construire un candidat
     * [fin,debut) . On retranche avant ces "fins" l'écart interduration pour construire un nouveau "début" pour pouvoir recoller
     * de nouveaux intervalles finissant juste avant cet écart
     */
    for(std::vector<OCP_CpDate>::iterator itBegin = previousBegins.begin();itBegin != previousBegins.end() ; itBegin++)
    {
        OCP_CpDate aBegin = (*itBegin);
        OCP_CpDate aEnd1 = aBegin - _cumulMin;
        OCP_CpDate aBegin1 = aEnd1 - _interSlotMinDuration;
        OCP_CpDate aEnd2 = aBegin - _cumulMax;
        OCP_CpDate aBegin2 = aEnd2 - _interSlotMinDuration;

        newBegins.push_back(aBegin1);
        newBegins.push_back(aBegin2);
        newEnds.push_back(aEnd1);
        newEnds.push_back(aEnd2);

    }

    std::vector<OCP_CpDate>::iterator newEndIt = newEnds.begin();
    for(std::vector<OCP_CpDate>::iterator newBeginIt = newBegins.begin()  ; newBeginIt != newBegins.end(); newBeginIt++)
    {
        OCP_CpDate aBegin = (*newBeginIt);
        OCP_CpDate aEnd = (*newEndIt);
        getStation()->addSpecialTimePoints(aEnd , aBegin);
        newEndIt++;
    }


}
