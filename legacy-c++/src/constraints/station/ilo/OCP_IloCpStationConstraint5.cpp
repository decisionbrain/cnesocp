/*
 * $Id: OCP_IloCpStationConstraint5.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
/*
 * OCP_IloCpStationConstraint5.cpp
 *
 */

#include "planif/OCP_CpMaintenanceSlot.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

using namespace ocp::commons;

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	create a new intVar with domain from 0 to (maximal duration)
	at each adding operation in the maintenance required set, reduce the max with the duration of the new slot if it was in the interval
*/	
class IlcStationConstraint5I : public IlcConstraintI
{
protected:


	/**
	 * portée englobante
	 */
	OCP_CpTimeSlotPtr _optimScope;

	/**
	 * variables ensemblistes associée à tous les passages candidats
	 */
	IlcAnySetVar _trackingSet;

	/**
	 * borne max du cumul des passages
	 */
	int _max;

	/**
	 * variable bornant la durée cumulée des passages
	 */
	IlcIntVar _cumul;

public:
	IlcStationConstraint5I(IloSolver s, OCP_CpTimeSlotPtr optimScope, IlcAnySetVar x, int max)
		: IlcConstraintI(s), _trackingSet(x),  _max(max), _optimScope(optimScope) {
		_cumul = IlcIntVar(s, 0, _max);
	}
	~IlcStationConstraint5I() {}
	virtual void post();
	virtual void propagate();
	void propagateTrackingDemon();
	void propagateCumulDemon();

	void verifyCumul();
	void verifyTracking();
};

// propagate demon on maxValue
ILCCTDEMON0(PropagateStationConstraint5TrackingDemon, IlcStationConstraint5I, propagateTrackingDemon);

// propagate demon on cumul
ILCCTDEMON0(PropagateStationConstraint5CumulDemon, IlcStationConstraint5I, propagateCumulDemon);

void IlcStationConstraint5I::post ()
{
	_trackingSet.whenDomain(PropagateStationConstraint5TrackingDemon(getSolver(), this));
	_cumul.whenRange(PropagateStationConstraint5CumulDemon(getSolver(), this));
}

void IlcStationConstraint5I::propagateCumulDemon()
{
/*
	si max cumul diminue,
	il faut retirer des maintenance slots possibles ceux dont la duree depasse la difference entre max et min
*/
	int newDuration = _cumul.getMax() - _cumul.getMin();
	IlcAnySet possibleTrackingSlotSet = _trackingSet.getPossibleSet();
	for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
	{
		OCP_CpTrackingSlot* currentTrackingSlot = (OCP_CpTrackingSlot*) (*iter);
		OCP_CpDate duration = currentTrackingSlot->getAmplitude()->getIntersectionDuration(_optimScope);

		if(!_trackingSet.isRequired(currentTrackingSlot) &&
		        duration > newDuration)
		{
			_trackingSet.removePossible(currentTrackingSlot);
		}
	}
}

// propagation demon on the removing maintenance slot event
void IlcStationConstraint5I::propagateTrackingDemon()
{
	// si ajout d'un requis dans maintenance alors modifier la borne min de la variable cumul
	for(IlcAnyDeltaRequiredIterator iterRequired(_trackingSet); iterRequired.ok(); ++iterRequired)
	{
		IlcAny newRequired = *iterRequired;
		OCP_CpTrackingSlot* current = (OCP_CpTrackingSlot*)newRequired;
		if(current->getAmplitude()->intersect(_optimScope))
		{
			int delta = current->getAmplitude()->getIntersectionDuration(_optimScope);
			_cumul.setMin(_cumul.getMin() + delta); // possible fail if newMin is greater than current max
		}
	}
}

void IlcStationConstraint5I::verifyCumul()
{
      propagateCumulDemon();
}



void IlcStationConstraint5I::verifyTracking()
{

    // si ajout d'un requis dans tracking alors modifier la borne min de la variable cumul

    int cumul = 0;

    for(IlcAnySetIterator iterRequired(_trackingSet.getRequiredSet()); iterRequired.ok(); ++iterRequired)
    {
        OCP_CpTrackingSlot* currentRequired = (OCP_CpTrackingSlot*) (*iterRequired);

        if(currentRequired->getAmplitude()->intersect(_optimScope))
        {
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,
                    boost::format( _("IlcStationConstraint5I::verifyTracking current required[%1$d , %2$d] ") )
            % currentRequired->getBegin() % currentRequired->getEnd() );

            cumul+= currentRequired->getAmplitude()->getIntersectionDuration(_optimScope);
        }

    }

    //cout << "_cumul = " << _cumul << " ==(" << cumul << ")== " ;
    _cumul.setMin(cumul);
    //cout << _cumul << endl;

}



// old propagate called only one time during the initialization without post

void IlcStationConstraint5I::propagate() {

      cout << "IlcStationConstraint5I::propagate BEGIN" << endl;
      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, boost::format( _("IlcStationConstraint5I::propagate BEGIN ") ) );

      verifyCumul();

      verifyTracking();

      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, boost::format( _("IlcStationConstraint5I::propagate END ") ) );

}

IlcConstraint IlcStationConstraint5(IloSolver s, OCP_CpTimeSlotPtr optimScope, IlcAnySetVar x, int max)
{
    return new (s.getHeap()) IlcStationConstraint5I(s,  optimScope, x, max);
}

ILOCPCONSTRAINTWRAPPER3(IloStationConstraint5, solver, OCP_CpTimeSlotPtr , optimScope, IloAnySetVar, x, int, _max)
{
    use(solver, x);
    if (x.getPossibleSet().getSize()==0)
    {
        boost::format msg = boost::format( _("IloStationConstraint5 : extraction d'un ensemble possible vide ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
    }

    return IlcStationConstraint5(solver, optimScope, solver.getAnySetVar(x), _max);
}
