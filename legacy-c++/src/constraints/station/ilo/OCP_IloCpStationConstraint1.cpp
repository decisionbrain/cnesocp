/*
 * $Id: OCP_IloCpStationConstraint1.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpStationConstraint1.cpp
 */

#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

using namespace ocp::commons;

/** Principe général
    On crée une variable entière de domaine (durée minimal requise)..(somme de toutes les durées des intervales de maintenance sur la portée)
	A chaque 'remove' d'une maintenance de l'ensemble des possibles, on réduit le max du slot perdu s'il était dans l'intervalle
*/	
class IlcStationConstraint1I : public IlcConstraintI
{
protected:

	/**
	 * portée englobante
	 */
	OCP_CpTimeSlotPtr _ts;

	/**
	 * variable ensembliste de domaine l'ensemble des slots de maintenance
	 */
	IlcAnySetVar _maintenance;
	
	/**
	 * durée minimale requise
	 */
	int _min;

	/**
	 * majorant de la somme des durées
	 */
	int _max;

	/**
	 * durée minimale interslot de maintenance
	 */
	OCP_CpDate _interSlotMinDuration;

	/**
	 * indique si l'on poste la partie cumul de la contrainte (sinon uniquement interSlotMinDuration)
	 */
	bool _withCumul;
	/**
	 * variable représentant la somme des durées des slots de maintenance
	 */
	IlcIntVar _cumul;

public:
	/**
	 * constructeur de l'IlcConstraintI
	 * @param s solver PPC
	 * @param d1 début de la portée englobante
	 * @param d2 fin de la portée englobante
	 * @param maintenanceSetVar variable ensembliste des slots de maintenance
	 * @param min minimum requis pour la somme des durées des slots de maintenance
	 * @param max majorant de la somme des durées des slots de maintenance
	 * @param interSlotMinDuration_ durée minimale entre deux slots consécutifs de maintenance
	 * @return l'implémentation IlcConstraintI de la contrainte
	 */
	IlcStationConstraint1I(IloSolver s, OCP_CpTimeSlotPtr ts, bool withCumul , IlcAnySetVar maintenanceSetVar/*AL20091217, IlcAnySetVar y*/, int min, int max, OCP_CpDate interSlotMinDuration_)
		: IlcConstraintI(s), _maintenance(maintenanceSetVar),/*AL20091217 _tracking(y),*/ _withCumul(withCumul),
		  _ts(ts), _min(min), _max(max), _interSlotMinDuration(interSlotMinDuration_)
        {
            //TODO FOR TESTING: set a maximal duration for maintenance instead of a minimal duration
            // _cumul = IlcIntVar(s, 0, _min);
            if (withCumul)
            {
                _cumul = IlcIntVar(s, _min, _max);
            }
        };
	~IlcStationConstraint1I() {};

	virtual void post();
	virtual void propagate();
	void propagateMaintenanceDemon();
	void propagateBoundCumulDemon();
	void propagateInterMaintenanceSlotDemon();

	void verifyInterMaintenanceSlot();
	void verifyBoundCumul();
	void verifyMaintenance();
};

// propagate demon on maxValue
ILCCTDEMON0(PropagateMaintenanceDemon, IlcStationConstraint1I, propagateMaintenanceDemon);

// propagate demon on cumul
ILCCTDEMON0(PropagateBoundCumulDemon, IlcStationConstraint1I, propagateBoundCumulDemon);

// propagate demon on interMaintenanceSlotMinDuration
ILCCTDEMON0(PropagateInterMaintenanceSlotDemon, IlcStationConstraint1I, propagateInterMaintenanceSlotDemon);


void IlcStationConstraint1I::post ()
{
    if(_withCumul)
    {
        _maintenance.whenDomain(PropagateMaintenanceDemon(getSolver(), this));
    }

	if(_interSlotMinDuration > 0)
	{
		_maintenance.whenDomain(PropagateInterMaintenanceSlotDemon(getSolver(), this));
	}

	if(_withCumul)
	{
	    _cumul.whenValue(PropagateBoundCumulDemon(getSolver(), this));
	}
}

void IlcStationConstraint1I::propagateBoundCumulDemon()
{
/*
	si le cumul devient bound alors aucune suppression de maintenance n'est plus possible,
	il faut donc ajouter tous les maintenance slots encore possibles dans les requis.
*/
    if (_cumul.isBound())
    {
        for(IlcAnySetIterator iter(_maintenance.getPossibleSet()); iter.ok(); ++iter)
        {
            IlcAny possibleMaintenance = *iter;
            _maintenance.addRequired(possibleMaintenance);
        }
    }
}

// propagation demon on the removing maintenance slot event
void IlcStationConstraint1I::propagateMaintenanceDemon()
{
	// si retrait d'un possible alors modifier la borne max de la variable cumul
	for(IlcAnyDeltaPossibleIterator iterPossible(_maintenance); iterPossible.ok(); ++iterPossible)
	{
		IlcAny oldPossible = *iterPossible;
		OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)oldPossible;
		if(current->intersect(_ts))
		{
			int delta = ((OCP_CpTrackingSlot*)oldPossible)->getIntersectionDuration(_ts);
			_cumul.setMax(_cumul.getMax() - delta); // possible fail if newMax is less than current min
		}
	}
}


// propagation demon on the addRequired maintenance slot event to satisfy interSlotMinDuration duration
void IlcStationConstraint1I::propagateInterMaintenanceSlotDemon()
{
	// si ajout d'un slot de maintenance dans requis alors supprimer les slots distants de moins de la duree _interSlotMinDuration
	for(IlcAnyDeltaRequiredIterator iterRequired(_maintenance); iterRequired.ok(); ++iterRequired)
	{
		IlcAny newRequired = *iterRequired;
		OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;
		OCP_CpTimeSlot* tmp = new(getSolver().getHeap()) OCP_CpTimeSlot(current->getBegin() - _interSlotMinDuration, current->getEnd() + _interSlotMinDuration);

		for(IlcAnySetIterator iter(_maintenance.getPossibleSet()); iter.ok(); ++iter)
		{
			IlcAny currentMaintenance = *iter;
			if((currentMaintenance != newRequired) && (((OCP_CpTimeSlot*)currentMaintenance)->intersect(tmp)))
			{
				_maintenance.removePossible(currentMaintenance);
			}
		}
	}
}

void IlcStationConstraint1I::verifyBoundCumul()
{

      propagateBoundCumulDemon();

}



// propagation demon on the removing maintenance slot event
void IlcStationConstraint1I::verifyMaintenance()
{
    OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("IlcStationConstraint1I::verifyMaintenance ") ) );

    // si retrait d'un slot de maintenance des possibles alors modifier la borne max de la variable cumul

    int cumul = 0;

    for(IlcAnySetIterator iterPossible(_maintenance.getPossibleSet()); iterPossible.ok(); ++iterPossible)
    {
        IlcAny currentPossible = *iterPossible;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)currentPossible;

        if(current->intersect(_ts))
        {
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("IlcStationConstraint1I::verifyMaintenance current possiblt[%1$d , %2$d] ") )
                % current->getBegin() % current->getEnd() );
            cumul += current->getIntersectionDuration(_ts);
        }

    }

    _cumul.setMax(cumul); // possible fail if newMax is less than current min

    OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("IlcStationConstraint1I::verifyMaintenance _cumul %1$d ") ) % _cumul );

}



// verification on the required maintenance set to satisfy interSlotMinDuration duration
void IlcStationConstraint1I::verifyInterMaintenanceSlot()
{
    OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("IlcStationConstraint1I::verifyInterMaintenanceSlot ") ) );

    // Supprimer les slots distants de moins de la duree _interSlotMinDuration de tous les slots des requis

    for(IlcAnySetIterator iterRequired(_maintenance.getRequiredSet()); iterRequired.ok(); ++iterRequired)
    {

        IlcAny currentRequired = *iterRequired;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)currentRequired;
        OCP_CpTimeSlot tmp(current->getBegin() - _interSlotMinDuration, current->getEnd() + _interSlotMinDuration);

        for(IlcAnySetIterator iter(_maintenance.getPossibleSet()); iter.ok(); ++iter)
        {
            IlcAny currentMaintenance = *iter;
            if((currentMaintenance != currentRequired) &&
                    (tmp.intersect(((OCP_CpTimeSlot*)currentMaintenance))))
                _maintenance.removePossible(currentMaintenance);

        }
    }

}



void IlcStationConstraint1I::propagate()
{

      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("IlcStationConstraint1I::propagate BEGIN ") ) );


      if(_withCumul)
      {
          verifyBoundCumul();
          verifyMaintenance();
      }

      if(_interSlotMinDuration > 0)
          verifyInterMaintenanceSlot();

      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("IlcStationConstraint1I::propagate END ") ) );
}



IlcConstraint IlcStationConstraint1(IloSolver s, OCP_CpTimeSlotPtr ts,bool withCumul ,IlcAnySetVar x, int minValue, int maxValue, OCP_CpDate d3)
{
  return new (s.getHeap()) IlcStationConstraint1I(s, ts, withCumul, x, minValue, maxValue, d3);
}

ILOCPCONSTRAINTWRAPPER6(IloStationConstraint1, solver, OCP_CpTimeSlotPtr, ts , bool, withCumul,IloAnySetVar, x, int, minValue, int, maxValue, int, interSlot)
{
  use(solver, x);
  if (x.getPossibleSet().getSize()==0)
  {
      boost::format msg = boost::format( _("IloStationConstraint1 : extraction d'un ensemble possible vide ") );
      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
  }

  return IlcStationConstraint1(solver, ts, withCumul, solver.getAnySetVar(x), minValue, maxValue, interSlot);
}
