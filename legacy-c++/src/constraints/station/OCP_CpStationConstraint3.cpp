/*
 * $Id: OCP_CpStationConstraint3.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint3.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpStationConstraint3.h"
#include "OCP_CpDate.h"
#include "context/OCP_CpModel.h"
#include "resources/OCP_CpStation.h"
#include "planif/OCP_CpMaintenanceSlot.h"
#include "OCP_CpExternWrapper.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpStationConstraint1);

/**
   * Constructeur à partir de la contrainte métier
   * @param period période d'instanciation de la contrainte
   * @param ctrSta3 contrainte métier origine
   * @param station station associé à la contrainte
   * @return
   */
OCP_CpStationConstraint3::OCP_CpStationConstraint3(OCP_CpTimeSlotPtr period, OcpConstraintStaType3Ptr ctrSta3,OCP_CpStationPtr station)
:OCP_CpStationBaseConstraint(period,ctrSta3,station)
{
    static const std::string pseudoClazz = _Clazz + "::OCP_CpStationConstraint1";

    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("StationConstraint3 lue") )  );

    try
    {
        _horizon = ctrSta3->getHorizon();
        _minimalDuration = (OCP_CpDate) 3600 * ctrSta3->getMinDuration();//conversion heure - secondes

    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour la station %1$s :Champs horizon ou durée minimale non renseigné sur OCP_CpStationConstraint3 ") )
                %  station->getName().c_str();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg ) ;
        throw OCP_XRoot(msg.str());
    }

    //_slotType = NULL;
    try
    {
        _slotType = ctrSta3->getSlotType();
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("TYPE_CRENEAU non renseigné sur %1$s ") )
        %  station->getName()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg );
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }

    /*
     * affichage et nommage
     */
    boost::format msg = boost::format(_("StationConstraint3-%1$s-(%2$d) offVisibility %3$d"))
    % station->getName().c_str() % _horizon%  _minimalDuration;
    setName(msg.str());
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());




}
/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpStationConstraint3::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    /*
     * récupérer les maintenances passées sur les _horizon derniers jours
     */

    int nbPastCandidates = 0;

    //récupérer tous les slots de maintenance existant sur l'horizon (horizon-1 jours plus le jour courant)
    OCP_CpDate beginHorizon = optimScope->getBegin() - (_horizon-1) * PHRDate::SECONDS_IN_A_DAY;
    OCP_CpDate endHorizon = optimScope->getEnd();
    PHRDate aBeginHorizon(beginHorizon);
    PHRDate aEndHorizon(endHorizon);
    OCP_CpTimeSlot horizon(beginHorizon , endHorizon);

    OcpContextPtr context = OcpXml::getInstance()->getContext();
    VectorStationSlot maintenancePast;
    try
    {
        context->getStationSlotsByPeriod(aBeginHorizon, aEndHorizon, getStation()->getOrigin());
    }
    catch(OcpDayNotFoundException& err)
    {
        boost::format logMsg = boost::format( _("OCP_CpStationConstraint3::instantiate . Aucun slot de maintenance pour la station %1$s dans le 'passé' %2$s %3$s") )
                     % getStation()->getName() % aBeginHorizon.getStringValue() % aEndHorizon.getStringValue();
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, logMsg );

    }

    for(VectorStationSlot::iterator it = maintenancePast.begin() ; it != maintenancePast.end() ; it++)
    {
        OcpStationSlotPtr maintenanceSlot = (*it);
        OCP_CpTimeSlot slot ( maintenanceSlot->getStartEnd() );
        OCP_CpDate interDuration = slot.getIntersectionDuration(&horizon);

        /*
         * un candidat de durée minimale trouvée dans l'horizon
         * (plus précisemment dont la durée dans l'horizon est >= à la durée minimale)
         */
        if(interDuration >= _minimalDuration)
            nbPastCandidates++;
    }

    /*
     * ajouter le maximorum de la durée de maintenance sur le jour courant
     */
    int max = nbPastCandidates + getStation()->getNbPossibleMaintenanceCandidates(optimScope->getBegin() , optimScope->getEnd() , _minimalDuration );

    if(0 == max)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour la station %2$s :StationConstraint3%1$s- : contrainte de cumul impossible à satisfaire aucune maintenance candidate ni passée ") )
                %  getName() % getStation()->getName();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );
        throw OCP_XRoot( msg.str() );
    }
    else if ( nbPastCandidates >= 1)
    {
        /*
         * contrainte déjà réalisée sur l'horizon moins le dernier jour (jour courant)
         */
        boost::format msg = boost::format( _("StationConstraint3-%1$s- : contrainte de cumul déjà satisfaite sur les jours précédents cumulDone  ") )
                %  getName() % nbPastCandidates ;
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg );
    }
    else //poser la contrainte ILOG sur la cardinalité >=1 pour les slots de maintenance candidats
        setCardinalityConstraint(optimScope);
}

/**
 * pose la contrainte ILOG sur la cardinalité des slots candidats. Il en faut au moins un de durée minimale dans la fenêtre de calcul
 * @param optimScope fenêtre de calcul
 */
void OCP_CpStationConstraint3::setCardinalityConstraint(OCP_CpTimeSlotPtr optimScope)
{
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();
    for(int i=0; i<getStation()->getMaintenanceSlots().getSize(); i++)
    {
        OCP_CpMaintenanceSlot* tmp = (OCP_CpMaintenanceSlot*)(getStation()->getMaintenanceSlots()[i]);

        if( !tmp->hasBookingOrder() && tmp->getIntersectionDuration(optimScope.get() ) >= _minimalDuration )
        {
            addCandidateToDomain(tmp);
        }
        else
        {
            _complementarySlots.add(tmp);
        }
    }

    if(_domainSlots.getSize()==0)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour la station %2$s :StationConstraint3-%1$s- : aucun candidat ") )
        %  getName() % getStation()->getName();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );
        throw OCP_XRoot( msg.str() );
    }

    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodMaintenanceSlotsVar(env, _domainSlots, _domainSlots);
    
    // creating anySetVar (intersect) with all slots possible
    boost::format intersectName = boost::format( _("MyIntersectSTA3-%1$s--%2$s ") ) % getStation()->getName().c_str() %  getId();
    IloAnySetVar intersect(env, getStation()->getMaintenanceSlots(),intersectName.str().c_str());

    // intersect = MaintenanceSlotSetVar from the current getStation() INTER goodMaintenanceSlotsVar
    IloConstraint c1 =  IloEqIntersection(env, intersect, getStation()->getMaintenanceSlotSet(), goodMaintenanceSlotsVar);
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSTA3_%1$s_%2$d") )
    % getStation()->getName() % getId() ;
    c1.setName(ctrName.str().c_str());

    IloConstraint c2 =  (IloCard(intersect) >= 1);
    IloConstraint c3 =  (IloCard(intersect) <= 2);

    setIloConstraint(c1 && c2 && c3);

    getStation()->addOcpConstraint(getIloConstraint());
}



/**
 * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
 * @param slot le créneau à sauver
 */
void OCP_CpStationConstraint3::postOptim(OCP_CpTimeSlot* slot)
{
    /*
     * positionnement du TYPE_CRENEAU
     */
    OCP_CpConstraint::setSupportType(slot , _slotType);
}
