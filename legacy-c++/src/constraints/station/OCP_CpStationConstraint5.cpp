/*
 * $Id: OCP_CpStationConstraint5.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint5.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpStationConstraint5.h"
#include "OCP_CpDate.h"
#include "context/OCP_CpModel.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpExternWrapper.h"

using namespace std;

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	create a new intVar with domain from 0 to (maximal duration)
	at each adding operation in the maintenance required set, reduce the max with the duration of the new slot if it was in the interval
	save the constraint by setIloConstraint
	take care, the constraint is not stored in the model!
*/	


/**
   * Constructeur à partir de la contrainte métier
   * @param optimScope période d'instanciation de la contrainte
   * @param ctrSat1 contrainte métier origine
   * @param station station associé à la contrainte
   * @return
   */
OCP_CpStationConstraint5::OCP_CpStationConstraint5(OCP_CpTimeSlotPtr optimScope, OcpConstraintStaType5Ptr ctrSat5,OCP_CpStationPtr station)
:OCP_CpStationBaseConstraint(optimScope,ctrSat5,station)
{
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("StationConstraint5 lue, non implémentée") )  );

    /*
     * Lecture de la durée maximale de visibilité cumulée sur la journée (champs obligatoire)
     */
    _maxVisiDuration = 60 * ctrSat5->getMaxVisiDuration();

    /*
     * nommage de la contrainte et affichage
     */
    boost::format msg = boost::format( _("StationConstraint5-%1$s-maxVisiDuration %2$d (minutes) ") )
            %  station->getName().c_str() % (_maxVisiDuration/60);

    setName(msg.str().c_str());
    msg = boost::format( _("Creating %1$s") ) % msg.str() ;
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg.str());
}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpStationConstraint5::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    IloEnv env = OCP_CpModel::getInstance()->getEnv();

    /*
     * Récupérer tous les passages satellites visibles par la station et intersectant la période de calcul
     */

    for(int i=0; i<getStation()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getStation()->getTrackingSlots()[i]);
        if( !tmp->toRemove()
                && !tmp->hasBookingOrder()
                && tmp->getAmplitude()->isInside( optimScope ))
        {
            addCandidateToDomain(tmp);
        }
        else
        {
            _complementarySlots.add(tmp);
        }
    }


    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots, _domainSlots);
    
    // creating anySetVar (intersect) with all slots possible
        boost::format intersectName = boost::format( _("MyIntersectSTA5-%1$s--%2$s ") ) % getStation()->getName().c_str() %  getId();
    IloAnySetVar intersect(env, getStation()->getTrackingSlots(),intersectName.str().c_str());

    // intersect = TrackingSlotSetVar from the current station INTER goodTrackingSlotsVar
    IloConstraint c1 =  IloEqIntersection(env, intersect, getStation()->getTrackingSlotSet(), goodTrackingSlotsVar);
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSTA5_%1$s_%2$d") )
    % getStation()->getName() % getId() ;
    c1.setName(ctrName.str().c_str());


    IloConstraint c2 = IloStationConstraint5(env, optimScope, intersect, _maxVisiDuration);


    setIloConstraint(c1 && c2);

    getStation()->addOcpConstraint(getIloConstraint());

}
