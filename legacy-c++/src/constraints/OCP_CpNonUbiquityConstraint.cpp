/*
 * $Id: OCP_CpNonUbiquityConstraint.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */


/*
 * OCP_CpNonUbiquityConstraint.cpp
 *
 */

#include "constraints/OCP_CpNonUbiquityConstraint.h"

ILOSTLBEGIN

IlcNonUbiquityConstraintI::IlcNonUbiquityConstraintI(IloSolver s, IlcAnySetVar x, IlcAnySetVar y)
: IlcConstraintI(s), _set1(x), _set2(y)
  {

  }


/**
* intersection entre deux créneaux de la station
* @param slot1 premier créneau
* @param slot2 second créneau
* @return vrai ssi les deux créneaux s'intersectent. Tient compte de l'amplitude pour un crénau satellite
*/
bool IlcNonUbiquityConstraintI::intersect(IlcAny slot1, IlcAny slot2)
{
    OCP_CpTimeSlot* a = (OCP_CpTimeSlot*)slot1;
    OCP_CpTimeSlot* b = (OCP_CpTimeSlot*)slot2;

    /**
     * lorsqu'on traite des slots de tracking au niveau de la station, il faut prendre en compte leur intervalle start-end
     */
    if (a->isTrackingSlot())
        a =((OCP_CpTrackingSlot*) a )->getAmplitude().get();

    if (b->isTrackingSlot())
        b =((OCP_CpTrackingSlot*) b )->getAmplitude().get();

    return a->intersect(b);
}

void IlcNonUbiquityConstraintI::propagateNonUbiquityWithDemon (IlcAnySetVar x, IlcAnySetVar y)
{
    for(IlcAnyDeltaRequiredIterator iterRequired(x); iterRequired.ok(); ++iterRequired)
    {
        IlcAny selected = *iterRequired;
        removeFromPossible(selected, y);
    }
}

/**
* supprimer de l'ensemble des possibles les candidats possibles intersectant un nouveau requis
* @param slotRequired : nouveau slot requis
* @param possibleSetVar : ensemble des créneaux possibles concernés par le remove
*/
void IlcNonUbiquityConstraintI::removeFromPossible(IlcAny slotRequired, IlcAnySetVar possibleSetVar)
{
    IlcAnySet possibleSet = possibleSetVar.getPossibleSet();
    for(IlcAnySetIterator iterPossible(possibleSet); iterPossible.ok(); ++iterPossible)
    {
        OCP_CpTimeSlot* currentPossible = (OCP_CpTimeSlot*) *iterPossible;

        if (!currentPossible->toRemove() && intersect(slotRequired, currentPossible))//TODO pourquoi le premier test ??
        {

            if(!possibleSetVar.isRequired(currentPossible))
            {
                possibleSetVar.removePossible(currentPossible);
            }
            else {
                if(slotRequired != currentPossible)
                {
                    fail();
                }
            }
        }
    }
}

ILCCTDEMON2(PropagateNonUbiquityWithDemon, IlcNonUbiquityConstraintI, propagateNonUbiquityWithDemon, IlcAnySetVar, v1, IlcAnySetVar, v2);

void IlcNonUbiquityConstraintI::post () {
    _set1.whenDomain(PropagateNonUbiquityWithDemon(getSolver(), this, _set1, _set1));
    _set1.whenDomain(PropagateNonUbiquityWithDemon(getSolver(), this, _set1, _set2));

    _set2.whenDomain(PropagateNonUbiquityWithDemon(getSolver(), this, _set2, _set2));
    _set2.whenDomain(PropagateNonUbiquityWithDemon(getSolver(), this, _set2, _set1));
}


void IlcNonUbiquityConstraintI::propagate ()
{
    // SET1 ==> SET2
    if(_set1.isInProcess())
    {
        propagateNonUbiquityWithDemon(_set1, _set1);
        propagateNonUbiquityWithDemon(_set1, _set2);
    }
    // SET2 ==> SET1
    if(_set2.isInProcess())
    {
        propagateNonUbiquityWithDemon(_set2, _set2);
        propagateNonUbiquityWithDemon(_set2, _set1);
    }
}

IlcConstraint IlcNonUbiquityConstraint(IloSolver s, IlcAnySetVar x, IlcAnySetVar y)
{
    return new (s.getHeap()) IlcNonUbiquityConstraintI(s, x, y);
}

ILOCPCONSTRAINTWRAPPER2(IloNonUbiquityConstraint, solver, IloAnySetVar, _var1, IloAnySetVar, _var2)
{
    use(solver, _var1);
    use(solver, _var2);
    return IlcNonUbiquityConstraint(solver, solver.getAnySetVar(_var1), solver.getAnySetVar(_var2));
}
