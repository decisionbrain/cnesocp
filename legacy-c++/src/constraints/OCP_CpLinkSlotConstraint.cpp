/*
 * $Id: OCP_CpLinkSlotConstraint.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
 
 /*
*	OCP_CpLinkConstraint: lien entre l'activite d'un satellite et celle d'une station.
*	quand une station suit un satellite, ce satellite est suivi par cette station
*	et vice versa.
*
*/

#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

class IlcLinkSlotConstraintI : public IlcConstraintI {
protected:
	 IlcAnySetVar _set1;
public:
	IlcLinkSlotConstraintI(IloSolver s, IlcAnySetVar x) : IlcConstraintI(s), _set1(x) {}
	~IlcLinkSlotConstraintI() {}
	virtual void post();
	virtual void propagate();
	void propagateLinkWithDemon();
};

ILCCTDEMON0(PropagateLinkWithDemon, IlcLinkSlotConstraintI, propagateLinkWithDemon);

void IlcLinkSlotConstraintI::post () {
	_set1.whenDomain(PropagateLinkWithDemon(getSolver(), this));
}

void IlcLinkSlotConstraintI::propagateLinkWithDemon()
{
	// si nouveau requis dans _set1 (station ou satellite) alors l'ajouter dans les requis de le requiredSet de la seconde ressource (satellite ou station) 
	for(IlcAnyDeltaRequiredIterator iterRequired(_set1); iterRequired.ok(); ++iterRequired)
	{
	    OCP_CpTrackingSlot* newRequired1 = (OCP_CpTrackingSlot*) (*iterRequired);
	    OCP_CpStationPtr currentStation = newRequired1->getStation();
	    OCP_CpSatellitePtr currentSatellite = newRequired1->getSatellite();

	    IlcAnySetVar _set2a = getSolver().getAnySetVar(currentStation->getTrackingSlotSet());
	    IlcAnySetVar _set2b = getSolver().getAnySetVar(currentSatellite->getTrackingSlotSet());

		if(_set2a.isPossible(newRequired1)) {
			_set2a.addRequired(newRequired1);
		}
		else {
			fail();
		}

		if(_set2b.isPossible(newRequired1)) {
			_set2b.addRequired(newRequired1);
		}
		else {
			fail();
		}
	}

	// si suppression d'un possible dans _set1 (station ou satellite) alors suppression des possibles de la seconde ressource (satellite ou station) 
	for(IlcAnyDeltaPossibleIterator iterPossible(_set1); iterPossible.ok(); ++iterPossible)
	{
	    OCP_CpTrackingSlot* oldPossible1 = (OCP_CpTrackingSlot*) (*iterPossible);
	    OCP_CpStationPtr currentStation = oldPossible1->getStation();
	    OCP_CpSatellitePtr currentSatellite = oldPossible1->getSatellite();

		IlcAnySetVar _set2a = getSolver().getAnySetVar(currentStation->getTrackingSlotSet());
		IlcAnySetVar _set2b = getSolver().getAnySetVar(currentSatellite->getTrackingSlotSet());
		
		_set2a.removePossible(oldPossible1);
		_set2b.removePossible(oldPossible1);
	}
}

void IlcLinkSlotConstraintI::propagate() {
	if(!_set1.isInProcess()) return;

	propagateLinkWithDemon();
}

IlcConstraint IlcLinkSlotConstraint(IloSolver s, IlcAnySetVar x) {
  return new (s.getHeap()) IlcLinkSlotConstraintI(s, x);
}

ILOCPCONSTRAINTWRAPPER1(IloLinkSlotConstraint, solver, IloAnySetVar, _var1) {
  use(solver, _var1);
  return IlcLinkSlotConstraint(solver, solver.getAnySetVar(_var1));
}
