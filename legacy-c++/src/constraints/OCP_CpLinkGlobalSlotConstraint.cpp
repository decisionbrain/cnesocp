/*
 * $Id: OCP_CpLinkGlobalSlotConstraint.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
*	OCP_CpLinkGlobalSlotConstraint: lien entre la variable contenant tous les slots sur laquelle la generation va s'effectuer
*	et les activites d'un satellite et/ou d'une station.
*
*/

#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpMaintenanceSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"
#include "context/OCP_CpModel.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

/*
*	OCP_CpLinkGlobalSlotConstraint: lien entre la variable contenant tous les slots sur laquelle la generation va s'effectuer
*	et les activites d'un satellite et/ou d'une station.
*
*/
class IlcLinkGlobalSlotConstraintI : public IlcConstraintI {
protected:
	 IlcAnySetVar _set1;
public:
	IlcLinkGlobalSlotConstraintI(IloSolver s, IlcAnySetVar x) : IlcConstraintI(s), _set1(x) {}
	~IlcLinkGlobalSlotConstraintI() {}
	virtual void post();
	virtual void propagate();
	void propagateLinkGlobalSlotRequired();
	void propagateLinkGlobalSlotPossible();
	void propagateLinkGlobalSlotWithDemon();
};

//rappel : quand ce d�mon se r�veille, il appelle la m�thode IlcLinkGlobalSlotConstraintI::propagateLinkGlobalSlotWithDemon
//sans argument (ILCCTDEMON0)
ILCCTDEMON0(PropagateLinkGlobalSlotWithDemon, IlcLinkGlobalSlotConstraintI, propagateLinkGlobalSlotWithDemon);

void IlcLinkGlobalSlotConstraintI::post () {
	_set1.whenDomain(PropagateLinkGlobalSlotWithDemon(getSolver(), this));
}

void IlcLinkGlobalSlotConstraintI::propagateLinkGlobalSlotRequired() {
	OCP_CpModel* cpModel = OCP_CpModel::getInstance();
	IlcAnySetVar _set4 = getSolver().getAnySetVar(cpModel->getGlobalSlotSetVar());

	// si nouveau requis dans _set1 (global, station ou satellite) 
	// alors l'ajouter dans les requis de le requiredSet de la seconde ressource (satellite ou station, global) 
	for(IlcAnyDeltaRequiredIterator iterRequired(_set1); iterRequired.ok(); ++iterRequired)
	{
	    OCP_CpTimeSlot* newRequired1 = (OCP_CpTimeSlot*) (*iterRequired);
	    OCP_CpStationPtr currentStation = OCP_CpStationPtr();
	    OCP_CpSatellitePtr currentSatellite = OCP_CpSatellitePtr();
	    if (newRequired1->isMaintenanceSlot() )
	        currentStation = ((OCP_CpMaintenanceSlot*)newRequired1)->getStation();
	    else if(newRequired1->isTrackingSlot())
	    {
	        currentStation = ((OCP_CpTrackingSlot*)newRequired1)->getStation();
	        currentSatellite = ((OCP_CpTrackingSlot*)newRequired1)->getSatellite();
	    }

		if (currentStation != NULL)
		{
			if(currentSatellite == NULL)
			{
				// no satellite but a station == maintenance
				IlcAnySetVar _set2 = getSolver().getAnySetVar(currentStation->getMaintenanceSlotSet());
				_set2.addRequired(newRequired1);
			}
			else
			{
				// a satellite AND a station == tracking
				IlcAnySetVar _set2 = getSolver().getAnySetVar(currentStation->getTrackingSlotSet());
				_set2.addRequired(newRequired1);
				IlcAnySetVar _set3 = getSolver().getAnySetVar(currentSatellite->getTrackingSlotSet());
				_set3.addRequired(newRequired1);
			}
		}
		_set4.addRequired(newRequired1);
	}
}

void IlcLinkGlobalSlotConstraintI::propagateLinkGlobalSlotPossible() {
	OCP_CpModel* cpModel = OCP_CpModel::getInstance();
	IlcAnySetVar _set4 = getSolver().getAnySetVar(cpModel->getGlobalSlotSetVar());

	// si ancien possible dans _set1 (global, station ou satellite) 
	// alors le supprimer dans les possibles du possibleSet de la seconde ressource (satellite ou station, global) 
	for(IlcAnyDeltaPossibleIterator iterPossible(_set1); iterPossible.ok(); ++iterPossible)
	{

	      OCP_CpTimeSlot* oldPossible1 = (OCP_CpTimeSlot*) (*iterPossible);
	      OCP_CpStationPtr currentStation = OCP_CpStationPtr();
	      OCP_CpSatellitePtr currentSatellite = OCP_CpSatellitePtr();
	      if (oldPossible1->isMaintenanceSlot() )
	          currentStation = ((OCP_CpMaintenanceSlot*)oldPossible1)->getStation();
	      else if(oldPossible1->isTrackingSlot())
	      {
	          currentStation = ((OCP_CpTrackingSlot*)oldPossible1)->getStation();
	          currentSatellite = ((OCP_CpTrackingSlot*)oldPossible1)->getSatellite();
	      }


		if (currentStation != NULL)
		{

			if(currentSatellite == 0)
			{
				// no satellite but a station == maintenance
				IlcAnySetVar _set2 = getSolver().getAnySetVar(currentStation->getMaintenanceSlotSet());
				_set2.removePossible(oldPossible1);
			}
			else
			{
				// a satellite AND a station == tracking
				IlcAnySetVar _set2 = getSolver().getAnySetVar(currentStation->getTrackingSlotSet());
				_set2.removePossible(oldPossible1);
				IlcAnySetVar _set3 = getSolver().getAnySetVar(currentSatellite->getTrackingSlotSet());
				_set3.removePossible(oldPossible1);
			}
		}
		_set4.removePossible(oldPossible1);
	}
}

void IlcLinkGlobalSlotConstraintI::propagateLinkGlobalSlotWithDemon() {
	propagateLinkGlobalSlotRequired();
	propagateLinkGlobalSlotPossible();
}

void IlcLinkGlobalSlotConstraintI::propagate() {
	if(!_set1.isInProcess()) return;

	propagateLinkGlobalSlotWithDemon();
}

IlcConstraint IlcLinkGlobalSlotConstraint(IloSolver s, IlcAnySetVar x) {
  return new (s.getHeap()) IlcLinkGlobalSlotConstraintI(s, x);
}

ILOCPCONSTRAINTWRAPPER1(IloLinkGlobalSlotConstraint, solver, IloAnySetVar, _var) {
  use(solver, _var);
  return IlcLinkGlobalSlotConstraint(solver, solver.getAnySetVar(_var));
}
