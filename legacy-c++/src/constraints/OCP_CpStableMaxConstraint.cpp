/*
 * $Id: OCP_CpStableMaxConstraint.cpp 928 2010-11-30 16:34:23Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * VERSION : 2-0 : FA : artf660903 : 30/11/2010 : Calcul du planning apr�s r�servation manuelle de cr�neaux NOK (https://coconet2.capgemini.com/sf/go/artf660903/")
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStableMaxConstraint.cpp
 */

/*
*	OCP_CpStableMaxConstraint.cpp: caractérisation d'un ensemble indépendant (stable) maximum dans le graphe des conflits
*/
#include <planif/OCP_CpTrackingSlot.h>
#include <constraints/OCP_CpStableMaxConstraint.h>
#include "planif/OCP_Solver.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

using namespace ocp::commons;
using namespace ocp::commons;
using namespace ocp::commons::io;

ILOSTLBEGIN



/**
 * constructeur de l'IlcConstraint
 * @param s : instance de l'IloSolver ILOG
 * @param domain : union des domaines des différentes SAT1 incriminées
 * @param minCardinality : borne inférieure de la cardinalité d'un stable maximum
 * @return l' instance de contrainte IlcConstraintI
 */
IlcStableMaxConstraintI::IlcStableMaxConstraintI(IloSolver s, IlcAnySetVar domain, int minCardinality) :
    IlcConstraintI(s), _domain(domain), _minCardinality(minCardinality)
{

    setName("ctrStableMax");
    /*
     * instanciation initiale de la variable ensembliste du stable maximum courant
     */
    computeStableMax();
}


ILCCTDEMON0(PropagateRemoveFromStableWithDemon, IlcStableMaxConstraintI, propagateRemoveFromStable);


void IlcStableMaxConstraintI::post ()
{
    //_domain.whenDomain(PropagateRemoveFromStableWithDemon(getSolver(), this));
    _indep.whenDomain(PropagateRemoveFromStableWithDemon(getSolver(), this));
}


void IlcStableMaxConstraintI::propagate ()
{
    /*
     * construction d'un premier stable maximum
     */
    int stableMax = _indep.getPossibleSize();

    /*
     * vérification de la cardinalité
     */
    if (stableMax < _minCardinality)
        getSolver().fail();
}

/**
 * propagation incrémentale : dès qu'un élèment du précédent stable est enlevé, on doit recalculer le stable max
 * , potentiellement de même cardinalité mais sur un nouveau sous-ensemble, et vérifier que la nouvelle cardinalité
 * est toujours >= cardinalité min requise
 */
void IlcStableMaxConstraintI::propagateRemoveFromStable ()
{
    int stableMax = _indep.getPossibleSize();

    boost::format msg = boost::format( _("STABLE courant taille %1$d ") ) % stableMax ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

    if (stableMax < _minCardinality)
    {
        stableMax = computeStableMax();
        if (stableMax < _minCardinality)
            getSolver().fail();
    }

}

/**
 * calcul initial ou recalcul incrémental d'un stable maximum
 * @return : renvoie la cardinalité d'un stable maximum
*/
int IlcStableMaxConstraintI::computeStableMax()
{
    boost::format msg = boost::format( _("computeStableMax") ) ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    std::vector<OCP_CpTrackingSlot*> slotsByEnd;
    for(IlcAnySetIterator iter(_domain.getPossibleSet()); iter.ok(); ++iter)
    {
        OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (*iter);
        slotsByEnd.push_back(slot);
    }

    /*
     * tri par fin croissante
     */
    std::sort(slotsByEnd.begin(), slotsByEnd.end(), OCP_CpTimeSlot::slotsEndSortPredicate);

    std::vector<OCP_CpTrackingSlot*> stable;
    std::map<OCP_CpStationPtr,OCP_CpTrackingSlot*> last;//pour chaque station , le dernier slot

    /*
     * en itérant par date de fin croissante, le slot courant est en conflit
     */
    for(std::vector<OCP_CpTrackingSlot*>::iterator it=slotsByEnd.begin() ; it != slotsByEnd.end() ; it++)
    {
        OCP_CpTrackingSlot* slot = (*it);
        OCP_CpStationPtr aStation = slot->getStation();
        std::map<OCP_CpStationPtr,OCP_CpTrackingSlot*>::iterator itMap = last.find(aStation);
        /*
         * un créneau appartient au stable maximum s'il n'intersecte pas le dernier créneau
         * (e.g dont la fin le précède) sur la même station
         */
        if (itMap  == last.end() || !conflict((*itMap).second , slot ))
        {
            /*
             * mise à jour de l'information "dernier créneau sur la station" et du stable
             */
            //last.insert(std::map<OCP_CpStationPtr,OCP_CpTrackingSlot*>::value_type(aStation, slot));
            last[aStation] = slot;
            stable.push_back(slot);

            msg = boost::format( _("Ajout au stable de %1$s") ) % slot->getFullName() ;
            OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        }
    }

    /*
     * création de la variable ensembliste correspondant au stable, sous-ensemble du domaine
     */
    if (stable.size()>0)
    {
        IlcAnyArray stableArray(getSolver() , stable.size());
        int s=0;
        for(std::vector<OCP_CpTrackingSlot*>::iterator it = stable.begin() ; it != stable.end() ; ++it)
        {
            OCP_CpTrackingSlot* itv =  (*it);
            stableArray[s++] = itv;
        }
        _indep = IlcAnySetVar(getSolver(),stableArray);
        getSolver().add( IlcSubsetEq(_indep,_domain) );

    }

    msg = boost::format( _("Fin computeStableMax") ) ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    return stable.size();
}

/**
 * indique que deux slots s'intersectent sur la même station
 * @param s1 : slot1
 * @param s2 : slot 2
 * @return vrai ssi les deux slots s'intersectent sur la mêmem station
 */
bool IlcStableMaxConstraintI::conflict(OCP_CpTrackingSlot* s1, OCP_CpTrackingSlot* s2)
{
    return s1->getStation() == s2->getStation() && s1->getAmplitude()->intersect(s2->getAmplitude());
}


IlcConstraint IlcStableMaxConstraint(IloSolver s, IlcAnySetVar x,int minCardinality) {
  return new (s.getHeap()) IlcStableMaxConstraintI(s, x, minCardinality);
}

ILOCPCONSTRAINTWRAPPER2(IloStableMaxConstraint, solver, IloAnySetVar, _var1, int, minCardinality) {
  use(solver, _var1);
  return IlcStableMaxConstraint(solver, solver.getAnySetVar(_var1), minCardinality);
}
