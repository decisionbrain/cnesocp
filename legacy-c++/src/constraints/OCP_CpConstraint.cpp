/*
 * $Id: OCP_CpConstraint.cpp 918 2010-11-04 14:41:09Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

#include <string>

#include "constraints/OCP_CpConstraint.h"
#include "context/OCP_CpObject.h"
#include "planif/OCP_Solver.h"
#include "context/OCP_CpModel.h"
#include "planif/OCP_Solver.h"
#include <planif/OcpRsrcSlot.h>


using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpConstraint);

/**
 * classe mère de toutes les contraintes encapsulant des contraintes ILOG SOLVER
 * @param period période de validité de la contrainte
 * @param origine contrainte métier origine
 * @param atBestMode indique si la contrainte sera traitée en mode at Best
 * @return l'instance de la contrainte
 */
OCP_CpConstraint::OCP_CpConstraint(OCP_CpTimeSlotPtr period, OcpConstraintPtr origine, bool atBestMode_)
:OCP_CpObject() , _constraint( IloConstraint() ) , _hasInstance(true) , _atBestMode(atBestMode_), _origin(origine)
{
    IloEnv env = OCP_CpModel::getInstance()->getEnv();
    _domainSlots = IloAnyArray(env);
    _complementarySlots = IloAnyArray(env);

    OCP_CpDate validityBegin = -1;
    OCP_CpDate validityEnd = -1;
    /*
     * si les validités ne sont pas renseignées  et si cela n'intersecte pas la fenêtre courante, on lance une exception
     */
    try
    {
        validityBegin = (OCP_CpDate) origine->getStart().getSeconds();
    }
    catch (OcpXMLInvalidField& e)
    {
        boost::format errMsg = boost::format( _("Validité non renseignée sur %1$s erreur erreur %2$s") ) % origine->getName() % e.what() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg.str());
        throw OCP_XRoot(errMsg.str());
    }

    try
    {
        validityEnd = (OCP_CpDate) origine->getEnd().getSeconds();
    }
    catch (OcpXMLInvalidField& e)
    {
        boost::format errMsg = boost::format( _("Validité non renseignée sur %1$s erreur %2$s") ) % origine->getName() % e.what() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg.str());
        throw OCP_XRoot(errMsg.str());
    }

    _validity = OCP_CpTimeSlotPtr ( new OCP_CpTimeSlot(validityBegin , validityEnd) );

    if (!_validity->intersect(period ))
    {
            PHRDate vStart = origine->getStart();
            PHRDate vEnd = origine->getEnd();
            PHRDate pStart(period->getBegin());
            PHRDate pEnd(period->getEnd());
            boost::format errMsg = boost::format( _("Validité de la contrainte origine %1$s  [%2$s,%3$s] n'intersecte pas la fenêtre courante [%4$s,%5$s]") )
                % origine->getName() % vStart.getStringValue() % vEnd.getStringValue() % pStart.getStringValue() % pEnd.getStringValue() ;
            OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, errMsg.str());
            throw OCP_XRoot(errMsg.str());
    }
}


/**
 * création des plages horaires sur une fenêtre de calcul , avec traitement des périodes, éventuellement à cheval entre deux jours
 * @param plageHoraires les plages horaires à renseigner
 * @param beginRange début de la portée globale
 * @param endRange fin de la portée globale
 * @param beginOffset début relatif de la plage horaire
 * @param endOffset fin relative de la plage horaire
 * @param leftRight indique pour une plage en offset à cheval sur deux jours si l'on doit charger la plage de droite ET celle de gauche, sinon, on ne charge que celle de droite
*/
void OCP_CpConstraint::buildTimeAreas(VectorOCP_CpTimeSlot& plageHoraires , OCP_CpDate beginRange,OCP_CpDate endRange, OCP_CpDate beginOffset, OCP_CpDate endOffset, bool leftRight)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(endRange);
    if (endOffset==beginOffset && beginOffset!=0)
    {
        boost::format errMsg = boost::format( _("Lecture des périodes incohérentes %1$d %2$d")) % beginOffset % endOffset;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, errMsg );
        throw OCP_XRoot(errMsg.str());
    }
    if (endOffset<beginOffset) //périodes inversées [a,b<a], créer deux périodes [a-24,b] et [a,b+24]
    {
        OCP_CpTimeSlotPtr atHorse1( new OCP_CpTimeSlot(beginRange + beginOffset - PHRDate::SECONDS_IN_A_DAY ,beginRange+  endOffset) );
        OCP_CpTimeSlotPtr atHorse2( new OCP_CpTimeSlot(beginRange + beginOffset , beginRange+ endOffset + PHRDate::SECONDS_IN_A_DAY) );
        if(leftRight)
            plageHoraires.push_back(atHorse1);
        plageHoraires.push_back(atHorse2);
    }
    else if(endOffset == beginOffset) //plage 00:00->OO:00 ou non renseignée <=> 1 jour complet
    {
        OCP_CpTimeSlotPtr onePlage( new OCP_CpTimeSlot(beginRange ,beginRange +  PHRDate::SECONDS_IN_A_DAY) );
        plageHoraires.push_back(onePlage);
    }
    else //endOffset > beginOffset, pas de plage à cheval
    {
        OCP_CpTimeSlotPtr onePlage( new OCP_CpTimeSlot(beginRange + beginOffset  ,beginRange +  endOffset) );
        plageHoraires.push_back(onePlage);
    }
}

/**
 * ajout d'un slot de tracking aux slots candidats pour la contrainte
 * @param slot : le candidat
 */
void OCP_CpConstraint::addCandidateToDomain(OCP_CpTimeSlot* slot)
{
    static const std::string pseudoClazz = _Clazz + "::addCandidateToDomain";

    _domainSlots.add(slot);
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("Looks good: %1$s")) % slot->getFullName().str() );

    OCP_CpConstraintPtr aCtr = OCP_CpConstraintPtr(this);//boost::dynamic_pointer_cast<OCP_CpConstraint>(this);
    slot->addOcpConstraint(aCtr);
}

/**
 * flag des contraintes OCP liées à des contraintes réservant des créneaux
 * SAT1, SAT7, SAT8 réservent des créneaux satellites
 * STA1, STA2, STA3 réservent des créneaux de maintenance
 * @return vrai ssi la contrainte réserve des créneaux (satellites ou stations)
 */
bool OCP_CpConstraint::isReserveConstraints() const
{
    if (isSatelliteConstraint())
        return isSAT1() || isSAT7() || isSAT8();
    else
        return isSTA1() || isSTA2() || isSTA3();
}
/**
* en post-optimisation, modifie le champs NIVEAU_CRENEAU
* @param slot : le créneau à modifier
* @param editable : paramètre positionné par la contrainte
*/

void OCP_CpConstraint::setSlotLevel(OCP_CpTimeSlot* slot, bool editable)
{
    boost::shared_ptr<OcpRsrcSlot> rsc = slot->getRscSlot();
    if(!editable)
    {
        rsc->setBookingState(SlotBookingStates::RESERVED_LOCKED);
    }
}

/**
 * en post-optimisation, modifie le champs TYPE_CRENEAU
 * @param slot : le créneau à modifier
 * @param slotType : la chaine de caractère renseignée sur la contrainte satellite qui positionne l'information
 */
void OCP_CpConstraint::setSupportType(OCP_CpTimeSlot* slot, std::string slotType)
{
    static const std::string pseudoClazz = _Clazz + "::setSupportType";

    boost::shared_ptr<OcpRsrcSlot> rsc = slot->getRscSlot();
    std::string previousSlotType = rsc->getSlotType();

    if ( (previousSlotType.length() > 0) && (previousSlotType != slotType) )
    {
        boost::format errMsg = boost::format( _("Tentative d'écrasement du créneau de type %1$s par %2$s pour le créneau %3$s"))
                % previousSlotType  % slotType % slot->getFullName().str();
        OcpLogManager::getInstance()->warning(pseudoClazz, errMsg );

        OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::WARNING, errMsg);
    }

    if (slotType.length() > 0) //chaine vide peut-arriver si la contrainte n'avait pas renseigné la chaine
    {
        rsc->setSlotType(slotType);
    }
}
/**
 * export dans format texte pour préparation de données pour IBM-ILOG OPL
 * @param file
 */
void OCP_CpConstraint::exportOplModel(ofstream& file)
{
    file << getId() << endl;
}
