/*
 * $Id: OCP_CpSatelliteConstraint2.cpp 928 2010-11-30 16:34:23Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * VERSION : 2-0 : FA : artf660903 : 30/11/2010 : Calcul du planning apr�s r�servation manuelle de cr�neaux NOK (https://coconet2.capgemini.com/sf/go/artf660903/")
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint2.cpp
 */
#include <string>

#include "constraints/OCP_CpSatelliteConstraint2.h"
#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "OCP_CpDate.h"
#include "context/OCP_CpModel.h"
#include "resources/OCP_CpSatellite.h"
#include "OCP_CpExternWrapper.h"
#include "planif/OCP_CpTrackingSlot.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpSatelliteConstraint2);

/** GLOBAL IDEA FOR THIS CONSTRAINT:
	at each adding operation from the tracking possible set, remove all other tracking slots in the interval
	begin - delta, end + delta
	take care, the constraint is not stored in the model!
 */

/**
 * Constructeur à partir de l'objet métier
 * @param optimScope portée courante d'instanciation de la contrainte
 * @param cstrSat2 contrainte métier origine
 * @param satellite satellite associé
 * @return
 */
OCP_CpSatelliteConstraint2::OCP_CpSatelliteConstraint2(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType2Ptr cstrSat2 , OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(optimScope,cstrSat2, satellite)
{
    // Ecart en minutes demandés entre chaque slot
    _delta = 60 * cstrSat2->getGap();

    /*
     * il faudra charger hors horizon les slots qui terminent à moins de delta du début de la fenêtre de calcul
     * et ceux qui commencent à moins de delta de la fin de la fenêtre de calcul
     */
    OCP_CpTimeSlot extendedScope( optimScope->getBegin() - _delta , optimScope->getEnd() + _delta);
    enlargeExtenDedScope(extendedScope);

    //pour affichage du gap en HH:MM:SS
    PHRDate deltaDate(_delta , PHRDate::_TimeFormat);

    // Type de l'écart : écart MIN entre deux slots consécutifs ou écart MAX
    _typeEcart = cstrSat2->getGapType();


    // Lecture de la fréquence associée à la contrainte
    _frequencyBand = cstrSat2->getFrequencyBand();

    // Lecture du champs optionel filtrant les durées
    _minDuration = 0;
    try
    {
        _minDuration = 60 * cstrSat2->getMinDuration();
    }
    catch (OcpXMLInvalidField& e)
    {
        //rien à faire si le champs n'est pas renseigné
    }
    boost::format prefix;
    switch(_frequencyBand)
    {
        case FrequencyBands::S : prefix = boost::format ( _("S") );break;
        case FrequencyBands::X : prefix = boost::format ( _("X") );break;
        case FrequencyBands::SX : prefix = boost::format ( _("SX") );break;
        default : prefix = boost::format ( _("?") );break;
    }
    boost::format prefixTypeEcart;
    switch(_typeEcart)
    {
        case SatConstraintTypes::MAX : prefixTypeEcart = boost::format ( _("MAX") );break;
        case SatConstraintTypes::MIN : prefixTypeEcart = boost::format ( _("MIN") );break;
        default : prefixTypeEcart = boost::format ( _("?") );break;
    }
    boost::format msg = boost::format( _("SatelliteConstraint2-id %7$d --%1$s-gap %5$s (%2$i seconds) %3$s %4$s filter duration min %6$i ") )
            % satellite->getName().c_str() % _delta %  prefix.str() % prefixTypeEcart.str() % deltaDate.getStringValue() % _minDuration % getId();

    setName(msg.str().c_str());
    msg = boost::format( _("Creating %1$s") ) % msg.str() ;
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg.str());

}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint2::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);

    static const std::string pseudoClazz = _Clazz + "::instantiate";

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();

    // filtering good slots from all tracking slots
    for(int i=0; i<getSatellite()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getSatellite()->getTrackingSlots()[i]);
        if( !tmp->toRemove() && !tmp->hasBookingOrder() && tmp->getDuration() >= _minDuration
                && OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand,tmp->getStation()->getStationFrequencyBand())
            )
        {
            addCandidateToDomain(tmp);
        }
        else
        {
            _complementarySlots.add(tmp);
        }
    }

    if(_domainSlots.getSize() == 0)
    {
        boost::format msg = boost::format( _("OCP_CpSatelliteConstraint2::instantiate satellite %1$s : pas de slot candidat donc pas de traitement solver") ) % getSatellite()->getName() ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg.str());
        return;
    }

    OCP_CpDate lastEndDayBefore = -1;

    /*
     * la vérification du dernier créneau posé la veille (afin de détecter un éventuel conflit en début de jour courant)
     * est faite s'il y a eu un calcul avec succès la veille au sein du pilote courant
     */
    if ( (_typeEcart == SatConstraintTypes::MAX)
            && OCP_CpModel::getInstance()->hasSolutionCalculatedDayBefore()
            && lastTrackingBeyondScope(optimScope,lastEndDayBefore)
            )
    {
        boost::format msg = boost::format( _("OCP_CpSatelliteConstraint2::instantiate %1$s un slot candidat la veille est à plus de  l'écart max requis du premier candidat sur la fenêtre courante ") ) % getName() ;
        throw OCP_XRoot(msg.str());
    }

    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots,_domainSlots, "MySAT2GoodVar");


    
    boost::format intersectName = boost::format( _("MyIntersectSAT2-%1$s--%2$s ") ) % getSatellite()->getName().c_str() %  getId() ;

    IloAnySetVar intersect(env, getSatellite()->getTrackingSlots(), intersectName.str().c_str());
    // intersect = TrackingSlotSetVar from the current getSatellite() INTER goodTrackingSlotsVar
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT2_%1$s_%2$d") )
    % getSatellite()->getName() % getId() ;
    IloConstraint c1 =  IloEqIntersection(env, intersect, getSatellite()->getTrackingSlotSet(), goodTrackingSlotsVar);
    c1.setName(ctrName.str().c_str());

    if (_typeEcart == SatConstraintTypes::MAX)
    {
        IloConstraint c2 = IloSatelliteConstraint2Max(env,intersect, this->getDelta()  );

        if(lastEndDayBefore >0)
        {
            //TODO artf625727 : en théorie, il faut poser la contrainte sur "le premier créneau candidat pour SAT2 Max
            //doit être dans l'ensemble des créneaux candidats à moins de MAX de  lastEndDayBefore
        }

        setIloConstraint(c1 && c2);
        getSatellite()->addOcpConstraint(getIloConstraint());
    }
    else
    {
        boost::format msg =boost::format( _("delta avant creation de la contrainte %1$i ") ) % this->getDelta() ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        IloConstraint c2 = IloSatelliteConstraint2(env, intersect, this->getDelta()  );
        setIloConstraint(c1 && c2);
        getSatellite()->addOcpConstraint(getIloConstraint());
    }

}

/**
 * pour une contrainte de type MAX, s'il existe un dernier créneau la veille à plus de l'écart, la contrainte est en erreur
 * @param optimScope fenêtre courante de calcul
 * @param maxDate : renseigne la date du dernier créneau candidat la veille (-1 si pas de créneau)
 * @return vrai ssi il existe un créneau "candidat la veille" à plus de max du premier créneau candidat
 */
bool OCP_CpSatelliteConstraint2::lastTrackingBeyondScope(OCP_CpTimeSlotPtr optimScope,    OCP_CpDate& maxDate)
{

    static const std::string pseudoClazz = _Clazz + "::lastTrackingBeyondScope";

    OcpContextPtr context = OcpXml::getInstance()->getContext();
    OCP_CpModel*  cpModel = OCP_CpModel::getInstance();

    /*
     * trier par début croissant les slots de la fenêtre courante
     */
    vector<OCP_CpTrackingSlot*> slotsByBegin;//liste des slots possibles triés par début
    for(int i=0; i < _domainSlots.getSize(); i++)
    {
        OCP_CpTrackingSlot* slot = (OCP_CpTrackingSlot*) _domainSlots[i];
        slotsByBegin.push_back(slot);
    }
    std::sort(slotsByBegin.begin(), slotsByBegin.end(), OCP_CpTimeSlot::slotsBeginSortPredicate);
    OCP_CpDate firstBegin = slotsByBegin.front()->getBegin();

    /*
     * collecter les slots de la veille
     */
    PHRDate day(optimScope->getBegin() );
    PHRDate dayBefore(optimScope->getBegin() - PHRDate::SECONDS_IN_A_DAY);

    VectorSatelliteSlot listSlot = context->getSatelliteSlotsByPeriod(dayBefore, day ,  getSatellite()->getOrigin() );


    /*
     * maximum des los de fin sur la veille
     */

    for(VectorSatelliteSlot::iterator iterSatSlot = listSlot.begin(); iterSatSlot != listSlot.end(); iterSatSlot++)
    {
        OcpSatelliteSlotPtr aSatSlot = *iterSatSlot;
        OCP_CpStationPtr station = cpModel->getStation( aSatSlot->getStation() );

        // filtrage par les stations entrées en paramètres
        if ( (station != NULL) &&
                OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand , station->getStationFrequencyBand())
                )
        {

            OCP_CpDate los = -1;
            bool okAosLos = false;//ce créneau a-t-il une élévation sur SAT4 compatible avec la durée de SAT4 et la surcharge locale éventuelle
            try
            {
                OCP_CpTimeSlot ocpSatSlot = OCP_CpTrackingSlot::getPastAosLos(aSatSlot);
                okAosLos = ( (_minDuration <= 0) || (ocpSatSlot.getDuration() >= _minDuration) );
                los = ocpSatSlot.getEnd();
            }
            catch(OCP_XRoot& err)
            {
                //rien à faire (message déjà loggé dans l'exception lancée depuis OCP_CpTrackingSlot::getPastAosLos)
            }
            bool isReserved = aSatSlot->getBookingState()  == SlotBookingStates::RESERVED
                    || aSatSlot->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
            bool hasBookingOrder = aSatSlot->hasBeenBookedByRequest();
            /*
             * les slots candidats pour les jours précédents sont ceux reservés hors bookingOrder  mais
             * qui ne font pas partie des slots candidats sur le jour courant (possibilité de plage à cheval sur le
             * dernier jour)
             * Par ailleurs, le test sur la durée est fait selon l'hypothèse que les slots prépositionnés dans le passé
             * respectent déjà la durée min et l'élévation requises par SAT4
             */


            if (isReserved
                    && !hasBookingOrder
                    && okAosLos
                    && !getSatellite()->containsTrackingSlots(aSatSlot))
            {
                /*
                 * mise à jour de la date de fin maximale des créneaux candidats la veille
                 */
                if(los>maxDate)
                    maxDate = los;
            }
        }
    }

    boost::format msgDayBefore = boost::format( _(" pas de slot candidat la veille") );
    if(maxDate>0){
        PHRDate maxVeillePhr( maxDate );
        msgDayBefore = boost::format( _(" dernier slot de la veille finit à %1$s") ) % maxVeillePhr.getStringValue();
    }
    PHRDate firstBeginPhr( firstBegin );

    boost::format msg = boost::format( _("SAT2MAX %3$s Chargement des slots de la veille [\n\t%1$s , \n\t%2$s") ) % dayBefore.getStringValue() % day.getStringValue() %getSatellite()->getName();
    msg = boost::format( _("%1$s \n%2$s  et premier du jour courant commence à %3$s") )
        %msg.str() % msgDayBefore % firstBeginPhr.getStringValue() ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);


    /*
     * on renvoie vrai ssi on a trouvé un slot candidat et que la fin de celui-ci est à plus de delta
     */
    return (maxDate > 0) && (firstBegin - maxDate > getDelta());

}

/**
* export dans format texte pour préparation de données pour IBM-ILOG OPL
* @param file
*/
void OCP_CpSatelliteConstraint2::exportOplModel(ofstream& file)
{
    file << getId() << ";SAT2;" << _typeEcart << ";" << _delta << endl;
}

