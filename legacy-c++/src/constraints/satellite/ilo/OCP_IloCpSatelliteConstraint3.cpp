/*
 * $Id: OCP_IloCpSatelliteConstraint3.cpp 943 2010-12-21 13:12:51Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint3.cpp
 *
 */


#include "constraints/OCP_IloCpSatelliteConstraint3.h"

 IlcSatelliteConstraint3I::IlcSatelliteConstraint3I(IloSolver s, OCP_CpDate dMin, OCP_CpDate dMax, IlcAnySetVar x, OCP_CpDate max,SpecialDurationPtr specialDurations)
        : IlcConstraintI(s),  _tracking(x), _dMin(dMin), _dMax(dMax), _max(max)
          {
        _cumulMin = IlcIntVar(s, 0, dMax);


        _cumulMax = IlcIntVar(s, dMin, max);

        //copie de la map des durées
        if(specialDurations != NULL)
        {
            for(SpecialDuration::iterator it = specialDurations->begin() ; it != specialDurations->end(); ++it)
            {
                _specialDurations.insert(SpecialDuration::value_type(it->first , it->second ) );
            }
        }
        else //recopie des durées des intervalles
        {
            for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
            {
                OCP_CpTimeSlot*  current = (OCP_CpTimeSlot*) (*iter);
                _specialDurations.insert(SpecialDuration::value_type(current , current->getDuration()) );
            }
        }
    }

 /**
  * mesure la distance à la borne min de la contrainte, divisée par l'amplitude max-min courante
  * plus ce ratio est grand, moins il y a de liberté pour satisfaire cette contrainte
  * @return
  */
 IlcFloat IlcSatelliteConstraint3I::getRelativeSlack()
 {
     IlcFloat levelMin = (IlcFloat) _cumulMin.getMin();//niveau min déjà atteint
     if (levelMin >= _dMin)
         return 0;
     else
     {
         IlcFloat amplitude = (IlcFloat) _cumulMax.getMax() - levelMin;
         IlcFloat gap = _dMin - levelMin;
         IlcFloat relgap = gap / amplitude;
         return relgap;
     }

 }

 /**
 * pour debug, affichage des candidats possibles/requis pour une instance courante qui n'a pas encore atteint sa borne min
 */
 void IlcSatelliteConstraint3I::displayCandidates()
 {
     for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
     {
         OCP_CpTrackingSlot*  current = (OCP_CpTrackingSlot*) (*iter);
         bool isRequired = _tracking.isRequired(current);
         cout << current->getSlotId() << " " << "required "<< isRequired << " sat3Indexes : ";
         std::vector<int>& ref = current->getSat3Indexes();
         for(std::vector<int>::iterator it = ref.begin() ; it != ref.end() ; it ++)
             cout << (*it) << " ";
         cout<<endl;

     }

 }

// propagate demon 
ILCCTDEMON0(PropagateCumulWithDemon, IlcSatelliteConstraint3I, propagateCumulWithDemon);

void IlcSatelliteConstraint3I::post ()
{
	_tracking.whenDomain(PropagateCumulWithDemon(getSolver(), this));
}

/**
 * méthode invoquée par le démon de propagation
 */
void IlcSatelliteConstraint3I::propagateCumulWithDemon()
{
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("taille des possibles %1$d ")) % _tracking.getPossibleSet().getSize() );
	// si ajout d'un requis alors modifier la borne min de la variable cumulMin
	for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
	{
		IlcAny newRequired = *iterRequired;
		OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;
		OCP_CpDate d = _specialDurations.find(current)->second;
		//OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("IlcSatelliteConstraint3I [%1$d, %2$d]")) % current->getBegin() % current->getEnd());
		//OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("IlcSatelliteConstraint3I, cumulMin= %1$d, duration = %2$d")) % _cumulMin % d);
		_cumulMin.setMin(_cumulMin.getMin()+d);
	}

	// si retrait d'un possible alors modifier la borne max de la variable cumulMax
	for(IlcAnyDeltaPossibleIterator iterPossible(_tracking); iterPossible.ok(); ++iterPossible)
	{
		IlcAny oldPossible = *iterPossible;
		OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)oldPossible;
		OCP_CpDate d = _specialDurations.find(current)->second;
		//OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("IlcSatelliteConstraint3I, cumulMax= %1$d")) % _cumulMax);
		_cumulMax.setMax(_cumulMax.getMax()-d);
	}
}

/**
 * phase de vérification initiale : mise à jour des bornes des variables de cumul en tenant compte des requis courants
 */
void IlcSatelliteConstraint3I::propagate()
{
    //cout << "IlcSatelliteConstraint3I::verifyFirst()" << endl;
    OCP_CpDate startCumulMax=0;

    IlcAnySet possibleSet = _tracking.getPossibleSet();

    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("IlcSatelliteConstraint3I::propagate")) );

    for(IlcAnySetIterator iterPossible(possibleSet); iterPossible.ok(); ++iterPossible)
    {
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)(*iterPossible);
        OCP_CpDate d = _specialDurations.find(current)->second;
        //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("initialCondition - current possible = [%1$d, %2$d]")) % current->getBegin() % current->getEnd());
        //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("%1$s")) % current->getFullName() );
        if(_tracking.isRequired(current))
        {
            // a chaque requis alors modifier la borne min de la variable cumulMin
            //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("REQUIRED"))  );
            _cumulMin.setMin(_cumulMin.getMin()+d);

        }
        else
        {
            // comme on ne sait pas ce qui a ete supprime des possibles,
            // on recompte les possibles (tous avec les requis) et
            // on modifie la borne max de la variable cumulMax
           // OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("POSSIBLE"))  );
        }
        startCumulMax+=d;
        //cout << endl;
    }
    _cumulMax.setMax(startCumulMax);

    /* A MEDITER:
Il est possible d'aller plus loin dans certains cas tant ici que dans les propagateWithDemon:
Si _cumulMin est bound alors il sera impossible d'ajouter de nouveau element dans les requis, il faudrait supprimer tous les possibles non requis
Attention: il y a un impact sur _cumulMax via ces suppressions
Si _cumulMax est bound alors il sera impossible de supprimer des possibles, il faudrait tous les ajouter dans les requis
Attention: il y a un impact sur _cumulMin via ces ajouts
     */
}


IlcConstraint IlcSatelliteConstraint3(IloSolver s,  OCP_CpDate dMin, OCP_CpDate dMax, IlcAnySetVar x, OCP_CpDate tmax,SpecialDurationPtr specialDurations)
{
  return new (s.getHeap()) IlcSatelliteConstraint3I(s, dMin, dMax, x, tmax, specialDurations);
}

ILOCPCONSTRAINTWRAPPER5(IloSatelliteConstraint3, solver,  OCP_CpDate, dMin, OCP_CpDate, dMax, IloAnySetVar, x, OCP_CpDate, tmax,SpecialDurationPtr, specialDurations)
{
  use(solver, x);
  if (x.getPossibleSet().getSize()==0)
  {
      boost::format msg = boost::format( _("IloSatelliteConstraint3 : extraction d'un ensemble possible vide ") );
      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
  }

  return IlcSatelliteConstraint3(solver, dMin, dMax, solver.getAnySetVar(x), tmax, specialDurations);
}
