/*
 * $Id: OCP_IloCpSatelliteConstraint4.cpp 947 2011-01-07 14:43:53Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2Max.cpp
 *
 */

#include "constraints/OCP_IloCpSatelliteConstraint4.h"
#include <ConfAccel.h>
#include "context/OCP_CpModel.h"

using namespace ocp::commons;
using namespace ocp::commons::io;

ILOSTLBEGIN

/**
 * artf586378
 *
 * Idee générale pour cette contrainte :
 * A chaque ajout de nouveau requis NR[a,b] , on regarde comment il s'insère dans les requis pré-existants. Si le premier requis précédent
 * R1[a1,b1<=a] est sur la même station, il faut imposer au moins un passage P[p,q] parmi les possibles tels que b1<=p<q<=a. Si le requis suivant
 * R2[a2>=b,b2] est sur la même station , il faut imposer au moin un passage P'[p',q'] b<=p'<q' <=a2
 * Il faut donc disposer :
 *. d'une liste triée des possibles par date de début pour pouvoir aller chercher le premier requis débutant après b
 *. d'une liste triée des possibles par date de fin pour pouvoir aller chercher le dernier requis terminant avant a
 *.
 *TODO : Q : peut-on avoir en général plus d'un requis dans IlcAnyDeltaRequiredIterator ? Auquel cas, en tenir compte
 *  */

// propagate demon 
ILCCTDEMON0(PropagateNoConsecutiveWithDemon, IlcSatelliteConstraint4I, propagateNoConsecutive);

/**
* constructeur de la contrainte PPC
* @param s solver
* @param trackingVar variable ensembliste concernant aux passages concernés par la contrainte
* @param forbiddenStations stations interdites pour le premier créneau
* @return
*/
IlcSatelliteConstraint4I::IlcSatelliteConstraint4I(IloSolver s, IlcAnySetVar trackingVar,StationMapPtr forbiddenStations)
: IlcConstraintI(s),  _tracking(trackingVar) , _forbiddenStations(forbiddenStations)
  {

  }

/**
 * pose des démons de propagation
 */
void IlcSatelliteConstraint4I::post ()
{
	_tracking.whenDomain(PropagateNoConsecutiveWithDemon(getSolver(), this));
}

/**
  * propagation sur les possibles précédants le requis courant
  * @param slotsByEnd liste des possibles triées par fin décroissantes
  * @param currentRequired le requis courant
  * @return le premier requis dont la fin est< au début du requis courant et sur la même station
  */
OCP_CpTrackingSlot* IlcSatelliteConstraint4I::propagateBackward(std::vector<OCP_CpTrackingSlot*>& slotsByEnd, OCP_CpTrackingSlot* currentRequired)
{

    //boost::format msg = boost::format( _(" propagateBackward %1$s "))
    //% currentRequired->getFullName();
    //boost::format msgFail = boost::format( _(" FAIL "));
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

    OCP_CpTrackingSlot* prevRequired = NULL;//le précédent requis sur la même station (NULL sinon)

    // trouver le premier prédécesseur requis du requis courant
    bool go = true;
    std::vector<OCP_CpTrackingSlot*> set1;//slots précédant le requis courant (fin<=debut du requis courant) et de station différente
    std::vector<OCP_CpTrackingSlot*> set2;//slots précédant le requis courant (fin<=debut du requis courant) et de même station

    //parcours des possibles par date de fin décroissante.
    for(std::vector<OCP_CpTrackingSlot*>::iterator it=slotsByEnd.begin() ; go && it != slotsByEnd.end() ; it++)
    {
        OCP_CpTrackingSlot*  prev =  (OCP_CpTrackingSlot*) (*it);
        if (prev->getEnd() <= currentRequired->getBegin() && _tracking.isPossible(prev))
        {
            if(! _tracking.isRequired(prev))
            {
                if (prev->getStation() != currentRequired->getStation() )
                    set1.push_back(prev);
                else
                    set2.push_back(prev);
            }
            else //on a trouvé le requis précédent
            {
                go = false;
                if (prev->getStation() == currentRequired->getStation() )
                {
                    prevRequired = prev;
                    if (set1.size() == 0)
                    {
                         //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msgFail );
                         getSolver().fail();//aucun possible entre prevRequired et currentRequired de station différente
                     }

                }
                else
                    return NULL;//rien de plus à propager si le précédent requis est d'une station différente
            }
        }
    }
    if (prevRequired != NULL)//le requis précédent est de la même station : il faut imposer un requis entre les deux sur l'ensemble des passages d'une station différente
    {
        //créer la variable ensembliste pour les slots possibles entre les deux requis à moins de max du requis courant
        //note : on ne peut entrer ici avec un set1 vide car ce cas de figure a été traité par un fail au-dessus
        IlcAnyArray set1Array(getSolver() , set1.size());
        int s=0;
        for(std::vector<OCP_CpTrackingSlot*>::iterator it = set1.begin() ; it != set1.end() ; ++it)
            set1Array[s++] = (*it);

        //displayLocalCardConstraint(false,currentRequired,set1);

        IlcAnySetVar vSet1(set1Array);
        //créer une nouvelle contrainte : il faut au moins un slot choisi dans cet ensemble s1
        IlcConstraint ctr1 = IlcSubset(vSet1,_tracking);//strict subset constraint car cet ensemble n'inclut pas  prevRequired
        IlcConstraint ctr2 = (IlcCard(vSet1) >= 1);
        getSolver().add( ctr1 && ctr2 );

    }
    else if (set1.size()==0)//les possibles précédents sont tous de la même station que le requis courant : les supprimer tous
    {
        for(std::vector<OCP_CpTrackingSlot*>::iterator it=set2.begin() ; it != set2.end() ; it++)
        {
           // boost::format msg = boost::format( _("IlcSatelliteConstraint4I::propagateForward remove %s")) % (*it)->getFullName();
            //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
            _tracking.removePossible((*it));

        }

    }
    return prevRequired;
}


/**
 * propagation sur les possibles précédants le requis courant
 * @param slotsByBegin liste des possibles triées par débuts croissants
 * @param currentRequired le requis courant
 * @return le premier requis dont le début est >= fin du requis courant et de même station
 */
OCP_CpTrackingSlot* IlcSatelliteConstraint4I::propagateForward(std::vector<OCP_CpTrackingSlot*>& slotsByBegin, OCP_CpTrackingSlot* currentRequired)
{
    //boost::format msg = boost::format( _(" propagateForward %1$s "))
    //% currentRequired->getFullName();
    //boost::format msgFail = boost::format( _(" FAIL "));
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );


   OCP_CpTrackingSlot* nextRequired = NULL;//le requis suivant sur la même station (NULL sinon)

   // trouver le premier prédécesseur requis du requis courant
   bool go = true;
   std::vector<OCP_CpTrackingSlot*> set1;//successeurs possibles non requis de stations différentes
   std::vector<OCP_CpTrackingSlot*> set2;//successeurs possibles non requis de même station

   //parcours des possibles par date de début croissante.
   for(std::vector<OCP_CpTrackingSlot*>::iterator it=slotsByBegin.begin() ; go && it != slotsByBegin.end() ; it++)
   {
       OCP_CpTrackingSlot*  next =  (OCP_CpTrackingSlot*) (*it);
       if (next->getBegin() >= currentRequired->getEnd() && _tracking.isPossible(next))
       {
           if(! _tracking.isRequired(next))
           {
               if (next->getStation() != currentRequired->getStation() )
                   set1.push_back(next);
               else
                   set2.push_back(next);
           }
           else //on a trouvé le requis suivant
           {
               go = false;
               if (next->getStation() == currentRequired->getStation() ) //requis
               {
                   nextRequired = next;
                   if (set1.size() == 0)
                   {
                       //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msgFail );
                       getSolver().fail();//aucun possible de station différente entre les deux requis de même station
                   }
               }
               else
                   return NULL;//rien de plus à propager si le requis suivant est d'une station différente que le requis courant
           }
       }
   }
   if (nextRequired != NULL)//le prochain requis étant de la même station, il faut imposer au moins un créneau d'une station différente entre ces deux requis
   {
       //créer la variable ensembliste pour les slots possibles entre les deux requis à moins de max du requis courant
       //note : on ne peut entrer ici avec un set1 vide car ce cas de figure a été traité par un fail au-dessus
       IlcAnyArray set1Array(getSolver() , set1.size());
       int s=0;
       for(std::vector<OCP_CpTrackingSlot*>::iterator it = set1.begin() ; it != set1.end() ; ++it)
           set1Array[s++] = (*it);

       //displayLocalCardConstraint(true,currentRequired,set1);

       IlcAnySetVar vSet1(set1Array);
       //créer une nouvelle contrainte : il faut au moins un slot choisi dans cet ensemble s1
       IlcConstraint ctr1 = IlcSubset(vSet1,_tracking);//strict subset constraint car cet ensemble n'inclut pas  nextRequired
       IlcConstraint ctr2 = (IlcCard(vSet1) >= 1);
       getSolver().add( ctr1 && ctr2 );


   }
   else if (set1.size()==0)//tous les possibles suivants sont de la même station : les enlever tous
   {
       for(std::vector<OCP_CpTrackingSlot*>::iterator it=set2.begin() ; it != set2.end() ; it++)
       {
           //boost::format msg = boost::format( _("IlcSatelliteConstraint4I::propagateForward remove %s")) % (*it)->getFullName();
           //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
           _tracking.removePossible((*it));
       }
   }
   return nextRequired;
}

/**
 * en cas de stations interdites, renvoie le premier requis lorsque celui-ci a une station interdite
 * @return le premier requis lorsque celui-ci a une station interdite
 */
OCP_CpTrackingSlot* IlcSatelliteConstraint4I::getFirstWrongRequired() const
{
    OCP_CpTrackingSlot* result = NULL;
    if (_forbiddenStations->size() == 0)
        return NULL;
    else
    {
        for(IlcAnySetIterator iter(_tracking.getRequiredSet()); iter.ok(); ++iter)
        {
            OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (*iter);
            if(isStationForbidden(slot) && (result == NULL || slot->getBegin() <= result->getBegin()))
            {

                result = slot;
            }
        }
    }
    return result;
}


/**
 * propagation des stations interdites pour le premier créneau
 * @param firstWrongRequired : le premier requis , couramment sur une station interdite
 */
void IlcSatelliteConstraint4I::propagateForbiddenStations(OCP_CpTrackingSlot* firstWrongRequired)
{
    /*
     * étape 1 : tri des possibles et requis.
     * Pour les requis, on ne trie que sur les stations interdites
     * Pour les possibles, on ne trie que sur les stations autorisées
     */
    std::vector<OCP_CpTrackingSlot*> set1;//liste des slots possibles avant le firstWrongRequired

    for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
    {
        OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (*iter);
        if(!isStationForbidden(slot) && slot->getBegin() <= firstWrongRequired->getBegin() )
        {
            set1.push_back(slot);
        }
    }

    /*
     * étape 2 : collecter les possibles candidats avant le "mauvais" premier requis et le complémentaire
     */
    if (set1.size() == 0)
    {
        //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msgFail );
        getSolver().fail();//aucun possible de station différente avant le requis
    }
    else
    {
        //créer la variable ensembliste pour les slots possibles entre les deux requis à moins de max du requis courant
        //note : on ne peut entrer ici avec un set1 vide car ce cas de figure a été traité par un fail au-dessus
        IlcAnyArray set1Array(getSolver() , set1.size());
        int s=0;
        for(std::vector<OCP_CpTrackingSlot*>::iterator it = set1.begin() ; it != set1.end() ; ++it)
            set1Array[s++] = (*it);

        IlcAnySetVar vSet1(set1Array);
        //créer une nouvelle contrainte : il faut au moins un slot choisi dans cet ensemble s1
        IlcConstraint ctr1 = IlcSubset(vSet1,_tracking);//strict subset constraint car cet ensemble n'inclut pas firstWrongRequired
        IlcConstraint ctr2 = (IlcCard(vSet1) >= 1);
        getSolver().add( ctr1 && ctr2 );
    }

}

/**
 * le slot a-t-il une station "interdite en début de journée"
 * @param slot slot candidat
 * @return vrai ssi la station du slot est interdite en début de journée
 */
bool IlcSatelliteConstraint4I::isStationForbidden(OCP_CpTrackingSlot* slot) const
{
    return _forbiddenStations->find(slot->getStation()->getOrigin() ) != _forbiddenStations->end();
}


/**
 * démon de propagation
 */
void IlcSatelliteConstraint4I::propagateNoConsecutive()
{

    /*
     * Construction de listes statiques des possibles. On trie ces listes à l'appel de ce démon, même si celui-ci itère sur les
     * requis et peux éventuellement modifier dynamiquement la liste des possibles : il faudra juste dynamiquement filtrer les
     * slots devenus non possibles
     */

    vector<OCP_CpTrackingSlot*> slotsByBegin;//liste des slots possibles triés par début
    vector<OCP_CpTrackingSlot*> slotsByEnd;//liste des slots possibles triés par fin décroissante
    IlcAnySet possibleTrackingSlotSet = _tracking.getPossibleSet();
    for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
    {
        OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (*iter);
        slotsByBegin.push_back(slot);
        slotsByEnd.push_back(slot);
    }
    std::sort(slotsByBegin.begin(), slotsByBegin.end(), OCP_CpTimeSlot::slotsBeginSortPredicate);
    std::sort(slotsByEnd.begin(), slotsByEnd.end(), OCP_CpTimeSlot::slotsReverseEndSortPredicate);


    /*
     * Pour chaque nouveau requis, faire un traitement "avant" et "arrière"
     */

    OCP_CpTrackingSlot* firstWrongRequired = NULL;
	for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
	{
		IlcAny newRequired = *iterRequired;
		OCP_CpTrackingSlot* current = (OCP_CpTrackingSlot*)newRequired;

		if(_forbiddenStations->size()>0
		        && isStationForbidden(current)
                &&  (firstWrongRequired == NULL || current->getBegin() <= firstWrongRequired->getBegin() ) )
		{
		    firstWrongRequired = current;
		}
        //displaySlotStates(current,slotsByBegin);
		propagateBackward(slotsByEnd,current);
		propagateForward(slotsByBegin,current);


	}

	if(firstWrongRequired != NULL)
	    propagateForbiddenStations(firstWrongRequired);//propager le nouveau "premier mauvais requis"


}

/**
 * ancien propagate appelé une fois seulement avant le post
 */
void IlcSatelliteConstraint4I::propagate()
{
    /*
     * Construction de listes statiques des possibles. On trie ces listes à l'appel de ce démon, même si celui-ci itère sur les
     * requis et peux éventuellement modifier dynamiquement la liste des possibles : il faudra juste dynamiquement filtrer les
     * slots devenus non possibles
     */

    vector<OCP_CpTrackingSlot*> slotsByBegin;//liste des slots possibles triés par début
    vector<OCP_CpTrackingSlot*> slotsByEnd;//liste des slots possibles triés par fin décroissante
    IlcAnySet possibleTrackingSlotSet = _tracking.getPossibleSet();
    for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
    {
        OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (*iter);
        slotsByBegin.push_back(slot);
        slotsByEnd.push_back(slot);
    }
    std::sort(slotsByBegin.begin(), slotsByBegin.end(), OCP_CpTimeSlot::slotsBeginSortPredicate);
    std::sort(slotsByEnd.begin(), slotsByEnd.end(), OCP_CpTimeSlot::slotsReverseEndSortPredicate);


    /*
     * Pour chaque nouveau requis, faire un traitement "avant" et "arrière"
     */

    OCP_CpTrackingSlot* firstWrongRequired = NULL;
    for(IlcAnySetIterator iterRequired(_tracking.getRequiredSet()); iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        OCP_CpTrackingSlot* current = (OCP_CpTrackingSlot*)newRequired;

        if(_forbiddenStations->size()>0
                && isStationForbidden(current)
        &&  (firstWrongRequired == NULL || current->getBegin() <= firstWrongRequired->getBegin() ) )
        {
            firstWrongRequired = current;
        }

        //displaySlotStates(current,slotsByBegin);
        propagateBackward(slotsByEnd,current);
        propagateForward(slotsByBegin,current);
    }

    if(firstWrongRequired != NULL)
        propagateForbiddenStations(firstWrongRequired);//propager le "premier mauvais requis"


}

/**
 * déclaration de la contrainte
 * @param s solver PPC
 * @param trackingSetVar variable ensembliste associée aux trackings slots sur la période concernée (avec filtres éventuels)
 * @param forbiddenStations stations interdites pour le premier créneau
 * @return la contrainte PPC
 */
IlcConstraint IlcSatelliteConstraint4(IloSolver s, IlcAnySetVar trackingSetVar,StationMapPtr forbiddenStations)
{
  return new (s.getHeap()) IlcSatelliteConstraint4I(s, trackingSetVar, forbiddenStations);
}

ILOCPCONSTRAINTWRAPPER2(IloSatelliteConstraint4, solver, IloAnySetVar, trackingSetVar,StationMapPtr, forbiddenStations)
{
    use(solver, trackingSetVar);
    if (trackingSetVar.getPossibleSet().getSize()==0)
    {
        boost::format msg = boost::format( _("IloSatelliteConstraint4 : extraction d'un ensemble possible vide ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
    }

    return IlcSatelliteConstraint4(solver,solver.getAnySetVar(trackingSetVar), forbiddenStations);
}


/**
*  pour debug affichage courant de l'état possible/requis des slots de la contrainte
* @param newRequired : nouveau requis
* @param slotsByBegin : les possibles triés par début croissant
*/
void IlcSatelliteConstraint4I::displaySlotStates(OCP_CpTrackingSlot* newRequired,vector<OCP_CpTrackingSlot*>& slotsByBegin)
{
    boost::format msg = boost::format( _(" *************ajout aux requis de %2$d ********************%1$d ETATS ************************************** "))
        % _tracking.getPossibleSize() % newRequired->getId();
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

    //IlcAnySet possibleTrackingSlotSet = _tracking.getPossibleSet();
    //for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
    for(vector<OCP_CpTrackingSlot*>::iterator iter = slotsByBegin.begin() ; iter != slotsByBegin.end() ; ++iter)
    {
        OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (*iter);
        if( _tracking.isRequired(slot))
            msg = boost::format( _(" Requis %1$s ")) % slot->getFullName();
        else
            msg = boost::format( _(" Possible %1$s ")) % slot->getFullName();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
    }
    msg = boost::format( _(" *********************************************************************** "));
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
}


/**
 * pour debug affichage d'une des contraintes locales de cardinalité >=1
 * @param forward : origine de la propagation (forward ou backward)
 * @param currentRequired : déclencheur requis de la contrainte
 * @param set1 : l'ensemble sur lequel on pose la contrainte
 */
void IlcSatelliteConstraint4I::displayLocalCardConstraint(bool forward,OCP_CpTrackingSlot* currentRequired, std::vector<OCP_CpTrackingSlot*>& set1)
{
    //debug affichage de la contrainte de cardinalité

    boost::format msg ;
    if(forward)
        msg = boost::format(_("forward Propagate IloCard >=1 déclenchée par requis %1$s sur l'ensemble suivant \n"))
    % currentRequired->getFullName() ;
    else
        msg = boost::format(_("backward Propagate IloCard >=1 déclenchée par requis %1$s sur l'ensemble suivant \n"))
    % currentRequired->getFullName() ;

    for(std::vector<OCP_CpTrackingSlot*>::iterator it = set1.begin() ; it != set1.end() ; ++it)
    {
        msg = boost::format(_("%1$s %2$s \n")) % msg.str()  % ((*it)->getFullName()) ;

    }
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg  );

}
