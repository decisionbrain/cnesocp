/*
 * $Id: OCP_IloCpSatelliteConstraint6.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */



#include "constraints/OCP_IloCpSatelliteConstraint6.h"
//class OCP_CpSatelliteConstraint2;

using namespace ocp::commons::io;

ILOSTLBEGIN

using namespace ocp::commons;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(IlcSatelliteConstraint6I);


// propagate demon 
ILCCTDEMON0(PropagateDureeRecouvWithDemon, IlcSatelliteConstraint6I, propagateDureeRecouvWithDemon);

/**
 * pose des démons de propagation
 */
void IlcSatelliteConstraint6I::post ()
{
	_tracking.whenDomain(PropagateDureeRecouvWithDemon(getSolver(), this));
}

/**
 * démon de propagation : pour un nouveau requis sur _station2 (resp _station1), poser dynamiquement la contrainte de cardinalité
 * >=1 sur l'ensemble des passages sur la station _station1 (resp _station2) intersectant le nouveau requis
 */
void IlcSatelliteConstraint6I::propagateDureeRecouvWithDemon()
{
    for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        OCP_CpTrackingSlot* current = (OCP_CpTrackingSlot*)newRequired;

        propagateRequiredSlot(current);
    }

}

/**
* propagation d'un nouveau requis (ou existant)
* @param requiredSlot : slot requis
*/
void IlcSatelliteConstraint6I::propagateRequiredSlot(OCP_CpTrackingSlot* requiredSlot)
{
    static const std::string pseudoClazz = _Clazz + "::propagateRequiredSlot";

    boost::format msg0 = boost::format( _("NOUVEAU REQUIS  %1$s") ) % requiredSlot->getFullName();
    OcpLogManager::getInstance()->debug(pseudoClazz, msg0);

    IlcAnySet possibleSet = _tracking.getPossibleSet();
    std::vector<OCP_CpTrackingSlot*> set1;//slots candidats
    for(IlcAnySetIterator iterPossible(possibleSet); iterPossible.ok(); ++iterPossible)
    {
        OCP_CpTrackingSlot* currentPossible = (OCP_CpTrackingSlot*) (*iterPossible);

        if ( currentPossible->getStation() != requiredSlot->getStation() &&
                currentPossible->getIntersectionDuration(requiredSlot)>=_dureeRecouv )
        {
            set1.push_back(currentPossible);
        }
    }

    /*
     * créer la variable ensembliste pour les slots possibles de la station alternative intersectant le candidat avec une durée de
     * recouvrement suffisante
     */
    if (set1.size() == 0)
    {
        msg0 = boost::format( _("Pas de candidat au recouvrement : FAIL") ) ;
        OcpLogManager::getInstance()->debug(pseudoClazz, msg0);
        getSolver().fail();//aucun candidat possible : le requis est isolé
    }
    else
    {
        IlcAnyArray set1Array(getSolver() , set1.size());
        int s=0;
        msg0 = boost::format( _("Candidats au recouvrement : ") ) ;
        for(std::vector<OCP_CpTrackingSlot*>::iterator it = set1.begin() ; it != set1.end() ; ++it)
        {
            OCP_CpTrackingSlot* candidat = (*it);
            msg0 = boost::format( _("%1$s \n\t %2$s") ) % msg0.str() % candidat->getFullName() ;
            set1Array[s++] = candidat;
        }
        OcpLogManager::getInstance()->debug(pseudoClazz, msg0);

        IlcAnySetVar vSet1(set1Array);
        IlcConstraint ctr1 = IlcSubset(vSet1,_tracking);//strict subset constraint car cet ensemble n'inclut pas requiredSlot
        IlcConstraint ctr2 = (IlcCard(vSet1) >= 1);
        getSolver().add( ctr1 && ctr2 );

    }
}
/**
 * ancien propagate appelé une fois seulement avant le post : on fait le même traitement qu'en post, mais sur les
 * requis initiaux
 */
void IlcSatelliteConstraint6I::propagate()
{
    for(IlcAnySetIterator iterRequired(_tracking.getRequiredSet()); iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        OCP_CpTrackingSlot* current = (OCP_CpTrackingSlot*)newRequired;

        propagateRequiredSlot(current);
    }
}

IlcConstraint IlcSatelliteConstraint6(IloSolver s, OCP_CpStationPtr station1, OCP_CpStationPtr station2,OCP_CpDate dureeRecouv, IlcAnySetVar trackingSet)
{
  return new (s.getHeap()) IlcSatelliteConstraint6I(s, station1, station2, dureeRecouv, trackingSet);
}

ILOCPCONSTRAINTWRAPPER4(IloSatelliteConstraint6, solver, OCP_CpStationPtr, station1, OCP_CpStationPtr, station2,OCP_CpDate, dureeRecouv , IloAnySetVar, trackingSet)
{
    use(solver, trackingSet);
    if (trackingSet.getPossibleSet().getSize()==0)
    {
        boost::format msg = boost::format( _("IloSatelliteConstraint6 : extraction d'un ensemble possible vide ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
    }

    return IlcSatelliteConstraint6(solver, station1, station2, dureeRecouv, solver.getAnySetVar(trackingSet));
}

