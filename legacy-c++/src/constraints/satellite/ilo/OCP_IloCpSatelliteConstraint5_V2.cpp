/*
 * $Id: OCP_IloCpSatelliteConstraint5_V2.cpp 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "planif/OCP_CpTimeSlotTimetable.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

using namespace ocp::commons;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(IlcSatelliteConstraint5I);

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	at each adding operation from the possible set, modify the timetable capacity on the interval
*/	
class IlcSatelliteConstraint5I_V2 : public IlcConstraintI
{
protected:
    /**
     * capacité max autorisée de la timetable (on autorise jusqu'à ce nombre simultané d'intervalles,mais pas au dessus)
     */
	int _capacityMax;

	/**
	 * variables ensemblistes associés aux slots candidats
	 */
	IlcAnySetVar _tracking;

	/**
	 * structure de timetable maintenant l' "intégrale" des requis sur la période horaire demandée
	 */
	OCP_CpTimeSlotTimetable* _timeTable;

	/**
	 * plage horaire où l'on maintient la contrainte (l'intégrale doit etre respectée sur cette plage, pas autour)
	 */
	OCP_CpTimeSlotPtr _plageHoraire;

public:
	IlcSatelliteConstraint5I_V2(IloSolver s, int capacityMax, IlcAnySetVar tracking,OCP_CpTimeSlotPtr plageHoraire)
	: IlcConstraintI(s),  _tracking(tracking), _capacityMax(capacityMax),_plageHoraire(plageHoraire)
	  {
	    _timeTable = NULL;//initialisée au propagate pour profiter des éventuelles propagation initiales
	  }

	~IlcSatelliteConstraint5I_V2() {}

	virtual void post();
	virtual void propagate();
	void propagateTimeTableWithDemon();
};

// propagate demon 
ILCCTDEMON0(PropagateTimeTableWithDemonSAT5, IlcSatelliteConstraint5I_V2, propagateTimeTableWithDemon);

void IlcSatelliteConstraint5I_V2::post ()
{
	_tracking.whenDomain(PropagateTimeTableWithDemonSAT5(getSolver(), this));
}

void IlcSatelliteConstraint5I_V2::propagateTimeTableWithDemon()
{
    static const std::string pseudoClazz = _Clazz + "::propagateTimeTableWithDemon";

	// si ajout d'un requis alors modifier la timetable
	for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
	{
		IlcAny newRequired = *iterRequired;
		OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;

	//	boost::format msg = boost::format( _("IlcSatelliteConstraint5I_V2 *************ajout aux requis de %1$s ")) % current->getFullName();
	//	OcpLogManager::getInstance()->debug(pseudoClazz, msg );

		/*
		 * slots à remover une fois qu'un requis a atteint la cible max
		 */
		std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*> atBound;
		bool res = _timeTable->increase(current,atBound);

		if(!res)
		{
		    _timeTable->display(false);
		    fail();
		}

		/*
		 * supprimer les créneaux encore possibles qui intersectent une zone où l'on a atteint le niveau max
		 */
		for(std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>::iterator it = atBound.begin(); it!=atBound.end(); ++it)
		{
		    OCP_CpTimeSlot* toRemove = (OCP_CpTimeSlot*) ((*it).first);
		    _tracking.removePossible(toRemove);
		}
	}
}

// old propagate called only one time during the initialization without post
void IlcSatelliteConstraint5I_V2::propagate()
{
    _timeTable = new(getSolver().getEnv()) OCP_CpTimeSlotTimetable(getSolver() , _tracking, _plageHoraire, -1, _capacityMax);

    /*
     * sur la tt des requis, voir le tronçon le plus haut : sa valeur doit être <= au nombre de supports requis
     */
    int maxMin = _timeTable->getMaxOfLevelMin();
    if(maxMin > _capacityMax)
    {
        boost::format msg = boost::format( _("SatelliteConstraint5-timetable des requis a un niveau initial %1$i > capacité %2$i ") )
        %  maxMin % _capacityMax ;
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
        fail();
    }
}

IlcConstraint IlcSatelliteConstraint5V2(IloSolver s, int capacityMax, IlcAnySetVar x,OCP_CpTimeSlotPtr plageHoraire)
{
  return new (s.getHeap()) IlcSatelliteConstraint5I_V2(s, capacityMax, x,plageHoraire);
}
ILOCPCONSTRAINTWRAPPER3(IloSatelliteConstraint5_V2, solver, int, capacityMax, IloAnySetVar, x,OCP_CpTimeSlotPtr,plageHoraire)
{
  use(solver, x);
  if (x.getPossibleSet().getSize()==0)
  {
      boost::format msg = boost::format( _("IloSatelliteConstraint5 : extraction d'un ensemble possible vide ") );
      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
  }

  return IlcSatelliteConstraint5V2(solver, capacityMax, solver.getAnySetVar(x),plageHoraire);
}
