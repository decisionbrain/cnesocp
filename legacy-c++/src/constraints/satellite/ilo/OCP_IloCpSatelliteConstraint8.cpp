/*
 * $Id: OCP_IloCpSatelliteConstraint8.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpCumulTimeSlot.h"


#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

using namespace ocp::commons;

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	at each adding operation from the possible set, modify the timetable capacity on the interval
*/	
class IlcSatelliteConstraint8I : public IlcConstraintI
{
protected:
    /**
     * capacité max autorisée de la timetable (on autorise jusqu'à ce nombre simultané d'intervalles,mais pas au dessus)
     */
	int _capaMin;

	/**
	 * trou maximal autorisé sur la timetable
	 */
	int _maxHole;

	/**
	 * variables ensemblistes associés aux slots candidats
	 */
	IlcAnySetVar _tracking;

	/**
	 * structure de timetable maintenant l' "intégrale" des requis sur la période horaire demandée
	 */
	OCP_CpCumulTimeSlot* _timeTable;

	/**
	 * plage horaire où l'on maintient la contrainte (l'intégrale doit etre respectée sur cette plage, pas autour)
	 */
	OCP_CpTimeSlotPtr _plageHoraire;

public:
	IlcSatelliteConstraint8I(IloSolver s, int capaMin, int maxHole , IlcAnySetVar x,OCP_CpTimeSlotPtr plageHoraire)
	: IlcConstraintI(s),  _tracking(x), _capaMin(capaMin),_plageHoraire(plageHoraire),_maxHole(maxHole)
	  {
	    _timeTable = new(s.getHeap()) OCP_CpCumulTimeSlot(_capaMin,x.getPossibleSize());
	    _timeTable->setMaxHole(maxHole);

	    /*
	     * intialiser la timetable avec tous les possibles
	     */
	    for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
	    {
	        OCP_CpTimeSlot*  current = (OCP_CpTimeSlot*) (*iter);
	        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
	        if(!_timeTable->addCapacity(OCP_CpTimeSlotPtr( new OCP_CpTimeSlot(ts) )) )
	            throw OCP_XRoot("Initialisation de timetable impossible");
	    }
	  }

	~IlcSatelliteConstraint8I() {}
	virtual void post();
	virtual void propagate();
	void propagateTimeTableWithDemon();
};

// propagate demon 
ILCCTDEMON0(PropagateTimeTableMinAndHoleWithDemon, IlcSatelliteConstraint8I, propagateTimeTableWithDemon);

void IlcSatelliteConstraint8I::post ()
{
	_tracking.whenDomain(PropagateTimeTableMinAndHoleWithDemon(getSolver(), this));
}

/**
 * lorsqu'on retire un possible, on décrémente la timetable d'une unité sur l'intervalle intersectant la plage horaire
*/
void IlcSatelliteConstraint8I::propagateTimeTableWithDemon()
{
    for(IlcAnyDeltaPossibleIterator iterPossible(_tracking); iterPossible.ok(); ++iterPossible)
    {
        IlcAny oldPossible = *iterPossible;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)oldPossible;
        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
        if(!_timeTable->decreaseCapacity(OCP_CpTimeSlotPtr( new OCP_CpTimeSlot(ts) )))
        {
            fail();
        }
    }
}

// old propagate called only one time during the initialization without post
void IlcSatelliteConstraint8I::propagate()
{
	if(_tracking.isInProcess()) {
			_tracking.whenDomain(PropagateTimeTableMinAndHoleWithDemon(getSolver(), this));
	}
}

IlcConstraint IlcSatelliteConstraint8(IloSolver s, int capaMin,int maxHole ,  IlcAnySetVar x,OCP_CpTimeSlotPtr plageHoraire)
{
  return new (s.getHeap()) IlcSatelliteConstraint8I(s, capaMin, maxHole, x,plageHoraire);
}


ILOCPCONSTRAINTWRAPPER4(IloSatelliteConstraint8, solver, int, capaMin, int , maxHole , IloAnySetVar, x,OCP_CpTimeSlotPtr,plageHoraire)
{
  use(solver, x);
  if (x.getPossibleSet().getSize()==0)
  {
      boost::format msg = boost::format( _("IloSatelliteConstraint8 : extraction d'un ensemble possible vide ") );
      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
  }

  return IlcSatelliteConstraint8(solver, capaMin, maxHole , solver.getAnySetVar(x),plageHoraire);
}
