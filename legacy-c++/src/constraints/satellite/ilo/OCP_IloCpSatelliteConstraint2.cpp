/*
 * $Id: OCP_IloCpSatelliteConstraint2.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "constraints/OCP_IloCpSatelliteConstraint2Min.h"
#include "planif/OCP_Solver.h"

using namespace ocp::commons;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(IlcSatelliteConstraint2I);

ILOSTLBEGIN

// propagate demon 
ILCCTDEMON0(PropagateDeltaWithDemon, IlcSatelliteConstraint2I, propagateDeltaWithDemon);

void IlcSatelliteConstraint2I::post ()
{
	_tracking.whenDomain(PropagateDeltaWithDemon(getSolver(), this));
}

/**
     * constructeur de l'implémentation IlcConstraintI
     * @param s solveur
     * @param d1 debut de validité
     * @param d2 fin de validité
     * @param trackingSlot liste des passages satellites concernés
     * @param delta écart min inter satellites
     * @return
     */
IlcSatelliteConstraint2I::IlcSatelliteConstraint2I(IloSolver s, IlcAnySetVar trackingSlot, OCP_CpDate delta)
: IlcConstraintI(s),  _tracking(trackingSlot),  _delta(delta)
  {
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("TTTTTTTTTTTTTT %1$iTTTTTTTTTTTTTTTTTTTTT") ) % delta );
  }

/**
* propagation depuis les nouveaux requis : à chaque nouveau requis, on élimine tous les possibles qui l'intersectent à moins de delta
*/
void IlcSatelliteConstraint2I::propagateDeltaWithDemon()
{
    static const std::string pseudoClazz = _Clazz + "::propagateDeltaWithDemon";

    // si ajout d'un requis alors modifier la borne min de la variable cumul
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("1DELTA %1$i d1 %2$i d2 %3$i ") ) % _delta % _d1 % _d2 );
    for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
    {
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*) (*iterRequired);//nouveau requis
        OCP_CpTimeSlot* ts = new(getSolver().getHeap()) OCP_CpTimeSlot(current->getBegin() -_delta+1, current->getEnd() +_delta-1);

        //boost::format msg0 = boost::format( _("NOUVEAU REQUIS  %1$s") ) % current->getFullName();
        //OcpLogManager::getInstance()->debug(pseudoClazz, msg0);

        IlcAnySet possibleTrackingSlotSet = _tracking.getPossibleSet();
        for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
        {
            OCP_CpTimeSlot* currentTrackingSlot = (OCP_CpTimeSlot*) (*iter);
            /*boost::format msg;
            if(_tracking.isRequired(currentTrackingSlot))
                msg = boost::format( _("DEJA REQUIS  %1$s") ) % currentTrackingSlot->getFullName();
            else
                msg = boost::format( _("POSSIBLE  %1$s") ) % currentTrackingSlot->getFullName();
            OcpLogManager::getInstance()->debug(pseudoClazz, msg);*/

            if( currentTrackingSlot!=current && currentTrackingSlot->intersect(ts) )
            {
                _tracking.removePossible(currentTrackingSlot);

                //boost::format msg = boost::format( _("REMOVED  %1$s") ) % currentTrackingSlot->getFullName();
                //OcpLogManager::getInstance()->debug(pseudoClazz, msg);
            }
        }
    }
}

/**
 * à la première passe de propagation, on parcourt les requis et élimine pour chaque requis tout possible qui l'intersecte à moins
 * de delta
 */
void IlcSatelliteConstraint2I::propagate()
{
    for(IlcAnySetIterator iterRequired(_tracking.getRequiredSet()); iterRequired.ok(); ++iterRequired)
        {
            OCP_CpTimeSlot* current = (OCP_CpTimeSlot*) (*iterRequired);
            OCP_CpTimeSlot* ts = new(getSolver().getHeap()) OCP_CpTimeSlot(current->getBegin() -_delta+1, current->getEnd() +_delta-1);

            //boost::format msg0 = boost::format( _("NOUVEAU REQUIS  %1$s") ) % current->getFullName();
            //OcpLogManager::getInstance()->debug(pseudoClazz, msg0);

            IlcAnySet possibleTrackingSlotSet = _tracking.getPossibleSet();
            for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
            {
                OCP_CpTimeSlot* currentTrackingSlot = (OCP_CpTimeSlot*) (*iter);
                if( currentTrackingSlot!=current && currentTrackingSlot->intersect(ts) )
                {
                    _tracking.removePossible(currentTrackingSlot);
                }
            }
        }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

IlcConstraint IlcSatelliteConstraint2(IloSolver s, IlcAnySetVar x, OCP_CpDate delta)
{
  return new (s.getHeap()) IlcSatelliteConstraint2I(s, x, delta);
}

ILOCPCONSTRAINTWRAPPER2(IloSatelliteConstraint2, solver,  IloAnySetVar, x, OCP_CpDate, delta)
{
    use(solver, x);
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("0DELTA %1$i") ) % delta );

    if (x.getPossibleSet().getSize()==0)
    {
        boost::format msg = boost::format( _("IloSatelliteConstraint2 : extraction d'un ensemble possible vide ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
    }
    return IlcSatelliteConstraint2(solver,solver.getAnySetVar(x), delta);
}
