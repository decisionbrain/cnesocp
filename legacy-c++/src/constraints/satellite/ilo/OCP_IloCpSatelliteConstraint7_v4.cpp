/*
 * $Id: OCP_IloCpSatelliteConstraint7_v4.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "constraints/OCP_IloCpSatelliteConstraint7_v4.h"
#include <planif/OCP_CpTimeSlot.h>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

using namespace ocp::commons;
using namespace std;


ILOSTLBEGIN

// propagate demon
ILCCTDEMON0(PropagateTimeTableEqualWithDemon_v4, IlcSatelliteConstraint7I_v4, propagateTimeTableWithDemon);


/**
* constructeur de l'implémentation de la contrainte spécifique ILOG
* @param solver : solver IloSolver
* @param nbSupports : nombre de supports requis
* @param trackingSet : variable ensembliste des créneaux candidats
* @param plageHoraire : intervalle où la contrainte d'égalité au nombre de supports doit être posé
* @return
*/
IlcSatelliteConstraint7I_v4::IlcSatelliteConstraint7I_v4(IloSolver solver, int nbSupports, IlcAnySetVar trackingSet,
        OCP_CpTimeSlotPtr plageHoraire,bool withMin,bool withMax)
: IlcConstraintI(solver),  _tracking(trackingSet), _nbSupports(nbSupports),_plageHoraire(plageHoraire)
  ,_withMin(withMin)
  ,_withMax(withMax)
  {
    _timetable = NULL;//renseignée au propagate car basée sur les possibles et requis au moment du propagate

  }



void IlcSatelliteConstraint7I_v4::post ()
{
    _tracking.whenDomain(PropagateTimeTableEqualWithDemon_v4(getSolver(), this));
}

/**
* lorsqu'on retire un possible, on décrémente la timetable des possibles d'une unité sur l'intervalle intersectant la plage horaire
* lorsqu'on ajoute un requis, on incrémente la timetable des requis d'une unité sur l'intervalle intersectant la plage horaire
*/
void IlcSatelliteConstraint7I_v4::propagateTimeTableWithDemon()
{
    boost::format msg = boost::format( _("++++++++++++++++++++") );
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    _timetable->display(false);
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    for(IlcAnyDeltaPossibleIterator iterPossible(_tracking); _withMin && iterPossible.ok(); ++iterPossible)
    {
        IlcAny oldPossible = *iterPossible;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)oldPossible;

        /*
         * slots à imposer une fois qu'un possible a atteint la cible min
         */
        std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*> atBound;
        bool res = _timetable->decrease(current,atBound);

        if(!res)
        {
            _timetable->display(false);
            fail();
        }
        /*
         * fixer les créneaux encore possibles qui intersectent une zone où l'on a atteint le niveau min
         */
        for(std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>::iterator it = atBound.begin(); it!=atBound.end(); ++it)
        {
            OCP_CpTimeSlot* toFix = (OCP_CpTimeSlot*) ((*it).first);
            _tracking.addRequired(toFix);
        }

    }


    for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); _withMax && iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;

        /*
         * slots à remover une fois qu'un requis a atteint la cible max
         */
        std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*> atBound;
        bool res = _timetable->increase(current,atBound);

        if(!res)
        {
            _timetable->display(false);
            fail();
        }

        /*
         * supprimer les créneaux encore possibles qui intersectent une zone où l'on a atteint le niveau max
         */
        for(std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>::iterator it = atBound.begin(); it!=atBound.end(); ++it)
        {
            OCP_CpTimeSlot* toRemove = (OCP_CpTimeSlot*) ((*it).first);
            _tracking.removePossible(toRemove);
        }
    }

}



/**
 * propagation initiale : on peut enlever de la timetable des possibles les slots déjà vus comme non possibles dans l'état initial
 * et rajouter à celle des requis les slots déjà vus requis dans l'état initial . On déclenche un échec si les niveaux sont
* non compatibles
*/
void IlcSatelliteConstraint7I_v4::propagate()
{
    int levelMin = -1;
    int levelMax = -1;
    if(_withMin)
        levelMin = _nbSupports;
    if(_withMax)
        levelMax = _nbSupports;

    _timetable = new (getSolver().getEnv()) OCP_CpTimeSlotTimetable(getSolver(),_tracking,_plageHoraire,levelMin ,levelMax);

    /*
     * sur la tt des requis, voir le tronçon le plus haut : sa valeur doit être <= au nombre de supports requis
     */
    int maxMin = _timetable->getMaxOfLevelMin();
    if(_withMin &&  maxMin > _nbSupports)
    {
        boost::format msg = boost::format( _("SatelliteConstraint7-timetable des requis a un niveau initial %1$i mais %2$i supports simultanés requis") )
        %  maxMin % _nbSupports ;
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
        fail();
    }

    /*
     * sur la tt des possibles, voir le tronçon le plus bas : sa valeur doit être >= au nombre de supports requis
     */
    int minMax = _timetable->getMinOfLevelMax();

    if(_withMax && minMax < _nbSupports)
    {
        boost::format msg = boost::format( _("SatelliteConstraint7-timetable des possibles a un niveau initial %1$i mais %2$i supports simultanés requis") )
        %  minMax % _nbSupports ;
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
        fail();
    }
}

IlcConstraint IlcSatelliteConstraint7_v4(IloSolver solver, int nbSupports, IlcAnySetVar trackingSet,OCP_CpTimeSlotPtr plageHoraire,bool withMin,bool withMax)
{
    return new (solver.getHeap()) IlcSatelliteConstraint7I_v4(solver, nbSupports, trackingSet,plageHoraire,withMin,withMax);
}

ILOCPCONSTRAINTWRAPPER3(IloSatelliteConstraint7_v4, solver, int, nbSupports, IloAnySetVar, trackingSet,OCP_CpTimeSlotPtr,plageHoraire)
{
    use(solver, trackingSet);
    if (trackingSet.getPossibleSet().getSize()==0)
    {
        boost::format msg = boost::format( _("IloSatelliteConstraint7 : extraction d'un ensemble possible vide ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
    }

    return IlcSatelliteConstraint7_v4(solver, nbSupports, solver.getAnySetVar(trackingSet),plageHoraire,true,true);
}
