/*
 * $Id: OCP_IloCpSatelliteConstraint7_v2.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "constraints/OCP_IloCpSatelliteConstraint7_v2.h"


#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

using namespace ocp::commons;


ILOSTLBEGIN

// propagate demon
ILCCTDEMON0(PropagateTimeTableEqualWithDemon_v2, IlcSatelliteConstraint7I_v2, propagateTimeTableWithDemon);


/**
* constructeur de l'implémentation de la contrainte spécifique ILOG
* @param solver : solver IloSolver
* @param nbSupports : nombre de supports requis
* @param trackingSet : variable ensembliste des créneaux candidats
* @param plageHoraire : intervalle où la contrainte d'égalité au nombre de supports doit être posé
* @return
*/
IlcSatelliteConstraint7I_v2::IlcSatelliteConstraint7I_v2(IloSolver solver, int nbSupports, IlcAnySetVar trackingSet,OCP_CpTimeSlotPtr plageHoraire)
: IlcConstraintI(solver),  _tracking(trackingSet), _nbSupports(nbSupports),_plageHoraire(plageHoraire),
  _timeTablePossibles(solver.getEnv(),plageHoraire.get()),
  _timeTableRequis(solver.getEnv(),plageHoraire.get())

  {

  }



void IlcSatelliteConstraint7I_v2::post ()
{
    _tracking.whenDomain(PropagateTimeTableEqualWithDemon_v2(getSolver(), this));
}

/**
* lorsqu'on retire un possible, on décrémente la timetable des possibles d'une unité sur l'intervalle intersectant la plage horaire
* lorsqu'on ajoute un requis, on incrémente la timetable des requis d'une unité sur l'intervalle intersectant la plage horaire
*/
void IlcSatelliteConstraint7I_v2::propagateTimeTableWithDemon()
{
    for(IlcAnyDeltaPossibleIterator iterPossible(_tracking); iterPossible.ok(); ++iterPossible)
    {
        IlcAny oldPossible = *iterPossible;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)oldPossible;
        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
        int minLevel = _timeTablePossibles.substract(ts.getBegin() , ts.getEnd() , 1 );
        boost::format msg = boost::format( _("IlcSatelliteConstraint7I_v2 (nbSupports %1$d) : nouveau niveau min %2$d après remove du slot %3$s ") )
        % _nbSupports % minLevel % current->getFullName();
        _timeTablePossibles.display();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        if(minLevel < _nbSupports )
        {
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("FAIL %1$s") ) % msg );
            fail();
        }
    }
    for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;
        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
        int maxLevel = _timeTableRequis.add(ts.getBegin() , ts.getEnd() , 1 );
        boost::format msg = boost::format( _("IlcSatelliteConstraint7I_v2 (nbSupports %1$d) : nouveau niveau max %2$d après ajout du slot %3$s ") )
        % _nbSupports % maxLevel % current->getFullName();
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
        if(maxLevel > _nbSupports )
        {
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  boost::format( _("FAIL %1$s") ) % msg );
            fail();
        }
    }

    boost::format msg = boost::format( _("*****************************    IlcSatelliteConstraint7I_v2 (nbSupports %1$d) : TimeTable des possibles ***************************** ") )
    % _nbSupports;
    _timeTablePossibles.display();

    msg = boost::format( _("*****************************    IlcSatelliteConstraint7I_v2 (nbSupports %1$d) : TimeTable des requis ***************************** ") )
        % _nbSupports;
    _timeTableRequis.display();

}

/**
 * propagation initiale : on peut enlever de la timetable des possibles les slots déjà vus comme non possibles dans l'état initial
 * et rajouter à celle des requis les slots déjà vus requis dans l'état initial . On déclenche un échec si les niveaux sont
* non compatibles
*/
void IlcSatelliteConstraint7I_v2::propagate()
{
    /*
     * intialiser la timetable avec tous les possibles
     */
    boost::format msg = boost::format( _("@@@@@@@@@@@@@@@@@@@@@@@@ SatelliteConstraint7-Construction de la timetable des possibles") ) ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    _timeTablePossibles.display();

    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
    {
        OCP_CpTimeSlot*  current = (OCP_CpTimeSlot*) (*iter);
        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
        _timeTablePossibles.add(ts.getBegin() , ts.getEnd() , 1 );

        msg = boost::format( _("Ajout  du nouveau possible %1$s") ) % current->getFullName();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        _timeTablePossibles.display();

        if (_tracking.isRequired(current))
            _timeTableRequis.add(ts.getBegin() , ts.getEnd() , 1 );
    }
    msg = boost::format( _("@@@@@@@@@@@@@@@@@@@@@@@@ ") ) ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    /*
     * sur la tt des possibles, voir le tronçon le plus bas : sa valeur doit être >= au nombre de supports requis
    */
    IloInt min = _timeTablePossibles.getMin(_plageHoraire.get());
    if (min < _nbSupports)
    {
        boost::format msg = boost::format( _("SatelliteConstraint7-timetable des possibles a un niveau min %1$i mais %2$i supports simultanés requis") )
        %  min % _nbSupports ;
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
        fail();
    }

    /*
     * sur la tt des requis, voir le tronçon le plus haut : sa valeur doit être <= au nombre de supports requis
     */
    IloInt max = _timeTableRequis.getMax(_plageHoraire.get());
    if (max > _nbSupports)
    {
        boost::format msg = boost::format( _("SatelliteConstraint7-timetable des requis a un niveau max %1$i mais %2$i supports simultanés requis") )
        %  max % _nbSupports ;
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
        fail();
    }

}

IlcConstraint IlcSatelliteConstraint7_v2(IloSolver solver, int nbSupports, IlcAnySetVar trackingSet,OCP_CpTimeSlotPtr plageHoraire)
{
    return new (solver.getHeap()) IlcSatelliteConstraint7I_v2(solver, nbSupports, trackingSet,plageHoraire);
}

ILOCPCONSTRAINTWRAPPER3(IloSatelliteConstraint7_v2, solver, int, nbSupports, IloAnySetVar, trackingSet,OCP_CpTimeSlotPtr,plageHoraire)
{
    use(solver, trackingSet);
    if (trackingSet.getPossibleSet().getSize()==0)
    {
        boost::format msg = boost::format( _("IloSatelliteConstraint7 : extraction d'un ensemble possible vide ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
    }

    return IlcSatelliteConstraint7_v2(solver, nbSupports, solver.getAnySetVar(trackingSet),plageHoraire);
}
