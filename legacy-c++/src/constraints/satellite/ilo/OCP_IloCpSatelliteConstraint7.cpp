/*
 * $Id: OCP_IloCpSatelliteConstraint7.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "constraints/OCP_IloCpSatelliteConstraint7.h"


#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

using namespace ocp::commons;


ILOSTLBEGIN

// propagate demon
ILCCTDEMON0(PropagateTimeTableEqualWithDemon, IlcSatelliteConstraint7I, propagateTimeTableWithDemon);


/**
 * constructeur de l'implémentation de la contrainte spécifique ILOG
 * @param solver : solver IloSolver
 * @param nbSupports : nombre de supports requis
 * @param trackingSet : variable ensembliste des créneaux candidats
 * @param plageHoraire : intervalle où la contrainte d'égalité au nombre de supports doit être posé
 * @return
 */
IlcSatelliteConstraint7I::IlcSatelliteConstraint7I(IloSolver solver, int nbSupports, IlcAnySetVar trackingSet,OCP_CpTimeSlotPtr plageHoraire)
: IlcConstraintI(solver),  _tracking(trackingSet), _nbSupports(nbSupports),_plageHoraire(plageHoraire)
  {
    _timeTablePossibles = new(solver.getHeap()) OCP_CpCumulTimeSlot(_nbSupports,trackingSet.getPossibleSize());
    _timeTableRequis = new(solver.getHeap()) OCP_CpCumulTimeSlot(0,_nbSupports);



    /*
     * intialiser la timetable avec tous les possibles
     */
    for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
    {
        OCP_CpTimeSlot*  current = (OCP_CpTimeSlot*) (*iter);
        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
        if(!_timeTablePossibles->addCapacity(OCP_CpTimeSlotPtr( new OCP_CpTimeSlot(ts) )) )
            throw OCP_XRoot("Initialisation de timetable impossible");
    }

    /*
     * sur la tt des possibles, voir le tronçon le plus bas : il doit être au-dessus du nombre de supports requis
     * on ne peut faire l'équivalent au niveau de la tt des requis car ceux-ci ne sont pas encore ajoutés
     */
    OCP_CpTimeSlot tsMax = _timeTablePossibles->getMinMaxLevels();
    if (tsMax.getBegin() < _nbSupports)
    {
        boost::format msg = boost::format( _("SatelliteConstraint7-timetable des possibles a un niveau min %1$i mais %2$i supports simultanés requis") )
        %  tsMax.getBegin() % _nbSupports ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
        throw OCP_XRoot( msg.str() );
    }
  }



void IlcSatelliteConstraint7I::post () {
	_tracking.whenDomain(PropagateTimeTableEqualWithDemon(getSolver(), this));
}

/**
 * lorsqu'on retire un possible, on décrémente la timetable des possibles d'une unité sur l'intervalle intersectant la plage horaire
 * lorsqu'on ajoute un requis, on incrémente la timetable des requis d'une unité sur l'intervalle intersectant la plage horaire
*/
void IlcSatelliteConstraint7I::propagateTimeTableWithDemon()
{
    for(IlcAnyDeltaPossibleIterator iterPossible(_tracking); iterPossible.ok(); ++iterPossible)
    {
        IlcAny oldPossible = *iterPossible;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)oldPossible;
        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
        if(!_timeTablePossibles->decreaseCapacity(OCP_CpTimeSlotPtr( new OCP_CpTimeSlot(ts) )))
        {
            fail();
        }
    }
    for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;
        OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);
        if(!_timeTableRequis->addCapacity(OCP_CpTimeSlotPtr( new OCP_CpTimeSlot(ts) )))
        {
            fail();
        }
    }
}

// old propagate called only one time during the initialization without post
void IlcSatelliteConstraint7I::propagate()
{
    if(_tracking.isInProcess())
    {
        _tracking.whenDomain(PropagateTimeTableEqualWithDemon(getSolver(), this));
    }
}

IlcConstraint IlcSatelliteConstraint7(IloSolver solver, int nbSupports, IlcAnySetVar trackingSet,OCP_CpTimeSlotPtr plageHoraire)
{
  return new (solver.getHeap()) IlcSatelliteConstraint7I(solver, nbSupports, trackingSet,plageHoraire);
}

ILOCPCONSTRAINTWRAPPER3(IloSatelliteConstraint7, solver, int, nbSupports, IloAnySetVar, trackingSet,OCP_CpTimeSlotPtr,plageHoraire)
{
  use(solver, trackingSet);
  if (trackingSet.getPossibleSet().getSize()==0)
  {
      boost::format msg = boost::format( _("IloSatelliteConstraint7 : extraction d'un ensemble possible vide ") );
      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
  }

  return IlcSatelliteConstraint7(solver, nbSupports, solver.getAnySetVar(trackingSet),plageHoraire);
}
