/*
 * $Id: OCP_IloCpSatelliteConstraint5.cpp 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpCumulTimeSlot.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

using namespace ocp::commons;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(IlcSatelliteConstraint5I);

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	at each adding operation from the possible set, modify the timetable capacity on the interval
*/	
class IlcSatelliteConstraint5I : public IlcConstraintI
{
protected:
    /**
     * capacité max autorisée de la timetable (on autorise jusqu'à ce nombre simultané d'intervalles,mais pas au dessus)
     */
	int _capacityMax;

	/**
	 * variables ensemblistes associés aux slots candidats
	 */
	IlcAnySetVar _tracking;

	/**
	 * structure de timetable maintenant l' "intégrale" des requis sur la période horaire demandée
	 */
	OCP_CpCumulTimeSlot* _timeTable;

	/**
	 * plage horaire où l'on maintient la contrainte (l'intégrale doit etre respectée sur cette plage, pas autour)
	 */
	OCP_CpTimeSlotPtr _plageHoraire;

public:
	IlcSatelliteConstraint5I(IloSolver s, int capacityMax, IlcAnySetVar x,OCP_CpTimeSlotPtr plageHoraire)
	: IlcConstraintI(s),  _tracking(x), _capacityMax(capacityMax),_plageHoraire(plageHoraire)
	  {
	    _timeTable = new(s.getHeap()) OCP_CpCumulTimeSlot(0,_capacityMax);
	  }

	~IlcSatelliteConstraint5I() {}
	virtual void post();
	virtual void propagate();
	void propagateTimeTableWithDemon();
};

// propagate demon 
ILCCTDEMON0(PropagateTimeTableWithDemon, IlcSatelliteConstraint5I, propagateTimeTableWithDemon);

void IlcSatelliteConstraint5I::post ()
{
	_tracking.whenDomain(PropagateTimeTableWithDemon(getSolver(), this));
}

void IlcSatelliteConstraint5I::propagateTimeTableWithDemon()
{
    static const std::string pseudoClazz = _Clazz + "::propagateTimeTableWithDemon";

	// si ajout d'un requis alors modifier la timetable
	for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
	{
		IlcAny newRequired = *iterRequired;
		OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;

//		boost::format msg = boost::format( _("IlcSatelliteConstraint5I *************ajout aux requis de %1$s ")) % current->getFullName();
	//	OcpLogManager::getInstance()->debug(pseudoClazz, msg );

		OCP_CpTimeSlot ts = _plageHoraire->getIntersection(*current);

		if(!_timeTable->addCapacity( OCP_CpTimeSlotPtr( new OCP_CpTimeSlot(ts) ) ))
		{
		    cout <<"hop"<<endl;
		//   msg = boost::format( _("IlcSatelliteConstraint5I FAIL dépassement de la capacité  ")) ;
		//    OcpLogManager::getInstance()->debug(pseudoClazz, msg );
		    fail();
		}
	}
}

// old propagate called only one time during the initialization without post
void IlcSatelliteConstraint5I::propagate()
{
    //TODO artf631059 : Phase Propagate suite aux consignes ILOG
	if(_tracking.isInProcess())
	{
			_tracking.whenDomain(PropagateTimeTableWithDemon(getSolver(), this));
	}
}

IlcConstraint IlcSatelliteConstraint5(IloSolver s, int capacityMax, IlcAnySetVar x,OCP_CpTimeSlotPtr plageHoraire)
{
  return new (s.getHeap()) IlcSatelliteConstraint5I(s, capacityMax, x,plageHoraire);
}
ILOCPCONSTRAINTWRAPPER3(IloSatelliteConstraint5, solver, int, capacityMax, IloAnySetVar, x,OCP_CpTimeSlotPtr,plageHoraire)
{
  use(solver, x);
  if (x.getPossibleSet().getSize()==0)
  {
      boost::format msg = boost::format( _("IloSatelliteConstraint5 : extraction d'un ensemble possible vide ") );
      OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
  }

  return IlcSatelliteConstraint5(solver, capacityMax, solver.getAnySetVar(x),plageHoraire);
}
