/*
 * $Id: OCP_IloCpSatelliteConstraint2Max.cpp 947 2011-01-07 14:43:53Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2Max.cpp
 *
 */

#include "constraints/OCP_IloCpSatelliteConstraint2Max.h"
#include <ConfAccel.h>
//class OCP_CpSatelliteConstraint2;

using namespace ocp::commons;
using namespace ocp::commons::io;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_IloCpSatelliteConstraint2Max);

ILOSTLBEGIN

/**
 * artf586378
 * Cette notion d'écart min ou max nécessaire entre 2 créneaux consécutifs concerne uniquement des satellites défilants pour lesquels on a
 * toujours des créneaux du type :
 * [a,b] et [c,d] avec c>=b et c<= b + MAX
 *
 * Idee générale pour cette contrainte :
 * A chaque ajout de nouveau requis NR[a,b] , on regarde comment il s'insère dans les requis pré-existants. Si le premier requis précédent
 * R1[a1,b1] est à plus de max (b1<a-max), il faut imposer au moins un passage P[p,q] parmi les possibles tels que b1+max>=p>=b1 et
 * un passage parmi les passages P'[p',q'] tels que a-max<=q'<=a
 * Il faut donc disposer :
 *. d'une liste triée des possibles par date de début pour pouvoir aller chercher le premier requis débutant après b
 *. d'une liste triée des possibles par date de fin pour pouvoir aller chercher le dernier requis terminant avant a
 *.
 *TODO : Q : peut-on avoir en général plus d'un requis dans IlcAnyDeltaRequiredIterator ? Auquel cas, en tenir compte
 *  */


/**
 * constructeur de la contrainte PPC
 * @param s solver
 * @param validityBegin debut de validité de la contrainte
 * @param validityEnd fin de validité de la contrainte
 * @param trackingVar variable ensembliste concernant aux passages concernés par la contrainte
 * @param delta écart maximal entre deux passages consécutifs
 * @return
 */
IlcSatelliteConstraint2MaxI::IlcSatelliteConstraint2MaxI(IloSolver s, IlcAnySetVar trackingVar, OCP_CpDate delta)
: IlcConstraintI(s),  _tracking(trackingVar), _delta(delta)
  {
    int levelCrit4 = ConfAccel::getInstance()->getPrioSat2Max();
    IlcBool doPropagateOnSelection = (levelCrit4>0);
    _propagateSelection.setValue(s, doPropagateOnSelection);
    for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
    {
        OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (*iter);
        slot->setNextSat2MaxCandidate(getSolver() , IlcFalse);
    }

  }


/**
 * démon de propagation de la contrainte
 */
ILCCTDEMON0(PropagateDeltaMaxWithDemon, IlcSatelliteConstraint2MaxI, propagateDeltaMaxWithDemon);


/**
 * pose des démons de propagation
 */
void IlcSatelliteConstraint2MaxI::post ()
{
	_tracking.whenDomain(PropagateDeltaMaxWithDemon(getSolver(), this));
}



/**
 * propagation sur les variables ensemblistes
 */
void IlcSatelliteConstraint2MaxI::propagateDeltaMaxWithDemon()
{
    static const std::string pseudoClazz = _Clazz + "::propagateDeltaMaxWithDemon";

    /*
     * Construction de listes statiques des possibles. On trie ces listes à l'appel de ce démon, même si celui-ci itère sur les
     * requis et peux éventuellement modifier dynamiquement la liste des possibles : il faudra juste dynamiquement filtrer les
     * slots devenus non possibles
     */
    vector<OCP_CpTimeSlot*> slotsByBegin;//liste des slots possibles triés par début
    vector<OCP_CpTimeSlot*> slotsByEnd;//liste des slots possibles triés par fin décroissante
    IlcAnySet possibleTrackingSlotSet = _tracking.getPossibleSet();
    for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
    {
        OCP_CpTimeSlot*  slot = (OCP_CpTimeSlot*) (*iter);
        slotsByBegin.push_back(slot);
        slotsByEnd.push_back(slot);
    }
    std::sort(slotsByBegin.begin(), slotsByBegin.end(), OCP_CpTimeSlot::slotsBeginSortPredicate);
    std::sort(slotsByEnd.begin(), slotsByEnd.end(), OCP_CpTimeSlot::slotsReverseEndSortPredicate);


	for(IlcAnyDeltaRequiredIterator iterRequired(_tracking); iterRequired.ok(); ++iterRequired)
	{
		IlcAny newRequired = *iterRequired;
		OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;

		//boost::format msg = boost::format( _("IloSatelliteConstraint2Max : propagation du nouveau requis %1$s ") ) % current->getFullName();
		//OcpLogManager::getInstance()->warning(pseudoClazz, msg);


		OCP_CpTimeSlot* prevRequired = propagateBackward(slotsByEnd,current);
		if(prevRequired != NULL)
		    propagateForward(slotsByBegin , prevRequired);
		OCP_CpTimeSlot* nextRequired = propagateForward(slotsByBegin,current);
		if (nextRequired != NULL)
		    propagateBackward(slotsByEnd,nextRequired);
	}

	/*
	 * propagation incrémentale des notifications "nextSAT2MaxCandidats" pour la sélection des créneaux
	 */
	if(_propagateSelection)
	    propagateNextCandidateWithDemon(slotsByEnd);

}

/**
  * propagation sur les possibles précédants le requis courant
  * @param slotsByEnd liste des possibles triées par fin décroissantes
  * @param currentRequired le requis courant
  * @return le premier requis dont la fin est< au début du requis courant-max
  */
OCP_CpTimeSlot* IlcSatelliteConstraint2MaxI::propagateBackward(std::vector<OCP_CpTimeSlot*>& slotsByEnd, OCP_CpTimeSlot* currentRequired)
{
    static const std::string pseudoClazz = _Clazz + "::propagateBackward";
    //boost::format msg = boost::format( _("IloSatelliteConstraint2Max : propagateBackward du nouveau requis %1$s ") ) % currentRequired->getFullName();
    //OcpLogManager::getInstance()->warning(pseudoClazz, msg);

    OCP_CpTimeSlot* prevRequired = NULL;//le précédent requis

    // trouver le premier prédécesseur requis du requis courant
    bool go = true;
    std::vector<OCP_CpTimeSlot*> set1;//slots dont la fin est à moins de max avant le requis courant (s.fin>= r.debut - max)
    std::vector<OCP_CpTimeSlot*> set3;//si les prédécesseurs sont tous à plus de max avant le requis courant

    //parcours des possibles par date de fin décroissante.
    for(std::vector<OCP_CpTimeSlot*>::iterator it=slotsByEnd.begin() ; go && it != slotsByEnd.end() ; it++)
    {
        OCP_CpTimeSlot*  prev =  (OCP_CpTimeSlot*) (*it);
        if (prev->getEnd() <= currentRequired->getBegin() && _tracking.isPossible(prev))
        {
            if(! _tracking.isRequired(prev))
            {
                if (prev->getEnd()>= currentRequired->getBegin() - _delta)
                    set1.push_back(prev); //candidat à moins de delta
                else
                    set3.push_back(prev);  //candidat à plus de delta
            }
            else //on a trouvé le requis précédent
            {
                go = false;
                if (prev->getEnd() < currentRequired->getBegin() - _delta)
                {
                    prevRequired = prev;
                    if (set1.size() == 0)
                    {
                        //boost::format msg = boost::format( _("IloSatelliteConstraint2Max : FAIL  du nouveau requis %1$s : aucun candidat depuis le précédent requis %2$s ") )
                        //% currentRequired->getFullName() % prev->getFullName();
                        //OcpLogManager::getInstance()->warning(pseudoClazz, msg);
                        getSolver().fail();//aucun possible entre prevRequired et currentRequired à moins de max du début de currentRequired
                    }
                }
                else
                    return NULL;//les deux requis sont à moins de max : la contrainte est respectée
            }
        }
    }
    if (prevRequired != NULL)
    {
        //créer la variable ensembliste pour les slots possibles entre les deux requis à moins de max du requis courant
        IlcAnyArray set1Array(getSolver() , set1.size());
        int s=0;

        //msg = boost::format( _("IloSatelliteConstraint2Max : 1<= IlcCard( ") );

        for(std::vector<OCP_CpTimeSlot*>::iterator it = set1.begin() ; it != set1.end() ; ++it)
        {
            OCP_CpTimeSlot* itv =  (*it);
            //msg = boost::format( _("%1$s\n\t %2$s") ) % msg.str() % itv->getFullName();
            set1Array[s++] = itv;
        }
        IlcAnySetVar vSet1(set1Array);
        //OcpLogManager::getInstance()->warning(pseudoClazz, msg);
        //créer une nouvelle contrainte : il faut au moins un slot choisi dans cet ensemble s1
        IlcConstraint ctr1 = IlcSubset(vSet1,_tracking);//strict subset constraint car cet ensemble n'inclut pas  prevRequired
        IlcConstraint ctr2 = (IlcCard(vSet1) >= 1);
        //msg = boost::format( _("FORWARD_%1$s") ) % nextRequired->getFullName();
        //ctr1.setName(msg.str().c_str());
        getSolver().add( ctr1 && ctr2 );

        //getSolver().add( IlcCard(vSet1) >= 1 );
    }
    else if (set1.size()==0)//aucun requis précédent. Si tous les possibles sont à plus de max du début du requis courant, aucun d'entre eux n'est candidat
    {
        for(std::vector<OCP_CpTimeSlot*>::iterator it=set3.begin() ; it != set3.end() ; it++)
        {
            OCP_CpTimeSlot* toRemove = (*it);
            //msg = boost::format( _("IloSatelliteConstraint2Max : remove  de %1$s ") ) % toRemove->getFullName();
            //OcpLogManager::getInstance()->warning(pseudoClazz, msg);
            _tracking.removePossible(toRemove);
        }

    }
    return prevRequired;
}

/**
* propagation sur les possibles précédants le requis courant
* @param slotsByBegin liste des possibles triées par débuts croissants
* @param currentRequired le requis courant
* @return le premier requis dont le début est > fin du requis courant+max
*/
OCP_CpTimeSlot* IlcSatelliteConstraint2MaxI::propagateForward(std::vector<OCP_CpTimeSlot*>& slotsByBegin, OCP_CpTimeSlot* currentRequired)
{
    static const std::string pseudoClazz = _Clazz + "::propagateForward";

    OCP_CpTimeSlot* nextRequired = NULL;//le précédent requis

    //boost::format msg = boost::format( _("IloSatelliteConstraint2Max : propagateForward du nouveau requis %1$s ") ) % currentRequired->getFullName();
    //OcpLogManager::getInstance()->warning(pseudoClazz, msg);

    // trouver le premier prédécesseur requis du requis courant
    bool go = true;
    std::vector<OCP_CpTimeSlot*> set1;//slots dont le début est à moins de max après la fin du requis courant (s.debut <= r.fin - max)
    std::vector<OCP_CpTimeSlot*> set3;//si les successeurs sont tous à plus de max après le requis courant

    //parcours des possibles par date de début croissante.
    for(std::vector<OCP_CpTimeSlot*>::iterator it=slotsByBegin.begin() ; go && it != slotsByBegin.end() ; it++)
    {
        OCP_CpTimeSlot*  next =  (OCP_CpTimeSlot*) (*it);
        if (next->getBegin() >= currentRequired->getEnd() && _tracking.isPossible(next))
        {
            if(! _tracking.isRequired(next))
            {
                if (next->getBegin()<= currentRequired->getEnd() + _delta)
                    set1.push_back(next); //candidat à moins de delta
                else
                    set3.push_back(next);  //candidat à plus de delta
            }
            else //on a trouvé le requis suivant
            {
                go = false;
                if (next->getBegin() > currentRequired->getEnd() + _delta) //requis
                {
                    nextRequired = next;
                    if (set1.size() == 0)
                    {
                        //boost::format msg = boost::format( _("IloSatelliteConstraint2Max : FAIL  du nouveau requis %1$s : aucun candidat avant le prochain requis %2$s ") )
                        //% currentRequired->getFullName() % next->getFullName();
                        //OcpLogManager::getInstance()->warning(pseudoClazz, msg);
                        getSolver().fail();//aucun possible entre nextRequired et currentRequired à moins de max du début de currentRequired
                    }
                }
                else
                    return NULL;//les deux requis sont à moins de max : la contrainte est respectée
            }
        }
    }
    if (nextRequired != NULL)
    {
        //créer la variable ensembliste pour les slots possibles entre les deux requis à moins de max du requis courant
        IlcAnyArray set1Array(getSolver() , set1.size());
        int s=0;
        //msg = boost::format( _("IloSatelliteConstraint2Max : 1<= IlcCard( ") );
        for(std::vector<OCP_CpTimeSlot*>::iterator it = set1.begin() ; it != set1.end() ; ++it)
        {
            OCP_CpTimeSlot* itv =  (*it);
            //msg = boost::format( _("%1$s\n\t %2$s") ) % msg.str() % itv->getFullName();
            set1Array[s++] = itv;
        }
        IlcAnySetVar vSet1(set1Array);
        //créer une nouvelle contrainte : il faut au moins un slot choisi dans cet ensemble s1
        //OcpLogManager::getInstance()->warning(pseudoClazz, msg);
        IlcConstraint ctr1 = IlcSubset(vSet1,_tracking);//strict subset constraint car cet ensemble n'inclut pas  nextRequired
        IlcConstraint ctr2 = (IlcCard(vSet1) >= 1);
        //msg = boost::format( _("FORWARD_%1$s") ) % nextRequired->getFullName();
        //ctr1.setName(msg.str().c_str());
        getSolver().add( ctr1 && ctr2 );

    }
    else if (set1.size()==0)//aucun requis suivant. Si tous les possibles sont à plus de max du début du requis courant, aucun d'entre eux n'est candidat
    {
        for(std::vector<OCP_CpTimeSlot*>::iterator it=set3.begin() ; it != set3.end() ; it++)
        {
            OCP_CpTimeSlot* toRemove = (*it);
            //msg = boost::format( _("IloSatelliteConstraint2Max : remove  de %1$s ") ) % toRemove->getFullName();
            //OcpLogManager::getInstance()->warning(pseudoClazz, msg);
            _tracking.removePossible(toRemove);
        }
    }
    return nextRequired;
}
/**
 * ancien propagate appelé une fois seulement avant le post. Même traitement qu'incrémental , mais à partir du parcours de la liste
 * de tous les requis dans le domaine initial de la variable ensembliste
 */
void IlcSatelliteConstraint2MaxI::propagate()
{

    /*
     * Construction de listes statiques des possibles. On trie ces listes à l'appel de ce démon, même si celui-ci itère sur les
     * requis et peux éventuellement modifier dynamiquement la liste des possibles : il faudra juste dynamiquement filtrer les
     * slots devenus non possibles
     */
    vector<OCP_CpTimeSlot*> slotsByBegin;//liste des slots possibles triés par début
    vector<OCP_CpTimeSlot*> slotsByEnd;//liste des slots possibles triés par fin décroissante
    IlcAnySet possibleTrackingSlotSet = _tracking.getPossibleSet();
    for(IlcAnySetIterator iter(possibleTrackingSlotSet); iter.ok(); ++iter)
    {
        OCP_CpTimeSlot*  slot = (OCP_CpTimeSlot*) (*iter);
        slotsByBegin.push_back(slot);
        slotsByEnd.push_back(slot);
    }
    std::sort(slotsByBegin.begin(), slotsByBegin.end(), OCP_CpTimeSlot::slotsBeginSortPredicate);
    std::sort(slotsByEnd.begin(), slotsByEnd.end(), OCP_CpTimeSlot::slotsReverseEndSortPredicate);

    /*
     * propagation initiale des notifications "nextSAT2MaxCandidats" pour la sélection des créneaux
     */
    if(_propagateSelection)
        propagateInitialCandidateWithDemon(slotsByEnd);

    for(IlcAnySetIterator iterRequired(_tracking.getRequiredSet()); iterRequired.ok(); ++iterRequired)
    {
        IlcAny newRequired = *iterRequired;
        OCP_CpTimeSlot* current = (OCP_CpTimeSlot*)newRequired;

        OCP_CpTimeSlot* prevRequired = propagateBackward(slotsByEnd,current);
        if(prevRequired != NULL)
            propagateForward(slotsByBegin , prevRequired);
        OCP_CpTimeSlot* nextRequired = propagateForward(slotsByBegin,current);
        if (nextRequired != NULL)
            propagateBackward(slotsByEnd,nextRequired);
    }

}

/**
 * propagation incrémentale sur les flags réversibles des candidats pour maintenir le prochain candidat
 * @param slotsByEnd slots triés par fin décroissantes
 */
void IlcSatelliteConstraint2MaxI::propagateNextCandidateWithDemon(vector<OCP_CpTimeSlot*>& slotsByEnd)
{
    propagateInitialCandidateWithDemon(slotsByEnd);
}

/**
 * propagation intiale sur les flags réversibles des candidats pour maintenir le prochain candidat
 * @param slotsByEnd slots triés par fin décroissantes
 */
void IlcSatelliteConstraint2MaxI::propagateInitialCandidateWithDemon(vector<OCP_CpTimeSlot*>& slotsByEnd)
{
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("IlcSatelliteConstraint2MaxI::propagateInitialCandidateWithDemon")) );

    IlcInt nbRequired = _tracking.getRequiredSize();
    IlcInt nbPossible = _tracking.getPossibleSize();

/*    cout << "liste des slots potentiels "<<endl;
    for(std::vector<OCP_CpTimeSlot*>::iterator it=slotsByEnd.begin() ; it != slotsByEnd.end() ; it++)
            {
                OCP_CpTrackingSlot* last = (OCP_CpTrackingSlot*) (*it);
                if (_tracking.isPossible(last))
                    cout << last->getSlotId() << " ";
            }
    cout << endl;*/
    /*
     * si aucun requis ou que le slot le plus tard est déjà requis,
     * on ne peut pas encore positionner le flag du prochain à sélectionner, sinon, on choisit le dernier
     * requis et on sélectionne après celui-ci
     */
    if(nbRequired == 0 || nbPossible ==0 || _tracking.isRequired( slotsByEnd.front()) )
        return;
    else
    {
        /*
         * chercher le premier requis
         */
        OCP_CpTrackingSlot* lastRequired = NULL;
        std::vector<OCP_CpTrackingSlot*> afterRequired;
        for(std::vector<OCP_CpTimeSlot*>::iterator it=slotsByEnd.begin() ; it != slotsByEnd.end() ; it++)
        {
            OCP_CpTrackingSlot* last = (OCP_CpTrackingSlot*) (*it);
            if (_tracking.isRequired(last))
            {
                lastRequired = last;
                //cout << "premier requis " << lastRequired->getSlotId() << " ";
                /*
                 * on s'arrêre une fois vu le dernier requis de la contrainte, on va alors notifier parmi les slots
                 * qui le suivent celui qui serait son prochain bon candidat
                 */
                break;
            }
            else
            {
                afterRequired.push_back(last);
                //cout << "after required " << last->getSlotId() << " ";
            }

        }

        //cout <<endl;

        /*
         * chercher le dernier possible postérieur au premier requis et compatible avec SAT2MAX
         */
        for(std::vector<OCP_CpTrackingSlot*>::iterator it=afterRequired.begin() ; it != afterRequired.end() ; it++)
        {
            OCP_CpTrackingSlot* last = (*it);
            if (last->getBegin() - lastRequired->getEnd() <= _delta && last->getBegin() > lastRequired->getEnd())
            {
                //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("setNextSat2MaxCandidate %1$d ") ) % last->getSlotId());
                for(IlcAnySetIterator iterPossible(_tracking.getPossibleSet()); iterPossible.ok(); ++iterPossible)
                {
                    ( (OCP_CpTrackingSlot*) (*iterPossible))->setNextSat2MaxCandidate(getSolver() , IlcFalse);
                }
                last->setNextSat2MaxCandidate(getSolver() , IlcTrue);

                /*
                 * ces slots étant triés dans l'ordre chronologique inverse, le premier compatible est le plus éloigné
                 * du dernier requis : on s'arrête donc en flagant ce dernier slot compatible pour dire qu'il serait le prochain
                 * à sélectionner
                 */
                break;
            }
        }
    }
}

/**
 * déclaration de la contrainte
 * @param s solver PPC
 * @param d1 début de validité de la contrainte
 * @param d2 fin de validité de la contrainte
 * @param x variable ensembliste associée aux trackings slots sur la période concernée (avec filtres éventuels)
 * @param delta écart max entre deux passages consécutifs
 * @return la contrainte PPC
 */
IlcConstraint IlcSatelliteConstraint2Max(IloSolver s, IlcAnySetVar x, OCP_CpDate delta) {
  return new (s.getHeap()) IlcSatelliteConstraint2MaxI(s,  x, delta);
}

ILOCPCONSTRAINTWRAPPER2(IloSatelliteConstraint2Max, solver,  IloAnySetVar, x, OCP_CpDate, delta)
{
    use(solver, x);
    if (x.getPossibleSet().getSize()==0)
    {
        boost::format msg = boost::format( _("IloSatelliteConstraint2Max : extraction d'un ensemble possible vide ") );
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
    }

    return IlcSatelliteConstraint2Max(solver, solver.getAnySetVar(x), delta);
}

