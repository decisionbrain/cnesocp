/*
 * $Id: OCP_CpSatelliteConstraint5.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint5.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpSatelliteConstraint5.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpSatellite.h"
#include "planif/OCP_Solver.h"
#include "OCP_CpExternWrapper.h"


using namespace std;



/*
 * nombre de passages simultanés max
 * 1 seul passage satellite simultané parmi SPOT2, SPOT4 et SPOT5 entre 14h30 et 24h, quelle que soit la
station
2 passages satellites simultanés parmi ELISA1, ELISA2, ELISA3 et ELISA4 entre 16h et 20h pour les
stations KRN et SKRN01
 *
*/	


/**
 * Constructeur à partir de la contrainte métier
 * @param optimScope période d'instanciation de la contrainte
 * @param ctrSat contrainte métier origine
 * @param satellite satellite associé à la contrainte
 * @return
 */
OCP_CpSatelliteConstraint5::OCP_CpSatelliteConstraint5(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType5Ptr ctrSat,OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(optimScope, ctrSat,satellite)
{
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

    _nbSimultSat = -1;
    try
    {
        _nbSimultSat = ctrSat->getNbSimultSat();
    }

    catch(OcpXMLInvalidField& e )
    {
            boost::format msg = boost::format( _("champs NB_SAT_AUT non renseigné : la contrainte sera ignorée par le solveur ") );
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
            setNoInstance();
            return;
    }

    /*
     * LECTURE DES STATIONS
     */
    VectorOcpStation stations;
    try
    {
        stations = ctrSat->getStations();
    }
    catch(OcpXMLInvalidField& e )
    {
        boost::format msg = boost::format( _("Aucune station renseignée : la contrainte sera ignorée par le solveur. Raison %1$s") )
                % e.what();
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
        setNoInstance();
        return;
    }
    _stationsMap.clear();
    boost::format prefixStations = boost::format(_(" Stations "));
    for(std::vector <OcpStationPtr>::iterator it=stations.begin() ; it != stations.end() ; it++)
    {
        OcpStationPtr aStation = (*it);
        OCP_CpStationPtr station = cpModel->getStation(aStation);
        _stationsMap.insert(StationMap::value_type(aStation , station ));
        prefixStations = boost::format(_(" %1$s %2$s ")) % prefixStations.str() % aStation->getName() ;
    }

    /*
      * LECTURE DES SATELLITES
      */
     VectorOcpSatellite satellites;
     try
     {
         satellites = ctrSat->getSatellites();
     }
     catch(OcpXMLInvalidField& e )
     {
         boost::format msg = boost::format( _("Aucun satellite renseigné : la contrainte sera ignorée par le solveur ") );
         OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
         setNoInstance();
         return;
     }
     _satelliteMap.clear();
     boost::format prefixSatellites = boost::format(_(" Satellites "));
     for(std::vector <OcpSatellitePtr>::iterator it=satellites.begin() ; it != satellites.end() ; it++)
     {
         OcpSatellitePtr aSatellite = (*it);
         OCP_CpSatellitePtr satellite = cpModel->getSatellite(aSatellite);
         _satelliteMap.insert(SatelliteMap::value_type(aSatellite , satellite ));
         prefixSatellites = boost::format(_(" %1$s %2$s ")) % prefixSatellites.str() % aSatellite->getName() ;
     }


     /*
      * Lecture des plages horaires éventuellement à cheval entre deux jours
      */

    OCP_CpDate beginOffset = 0;//relatif
    OCP_CpDate endOffset = 0;//relatif

    try
    {
        PHRDate aStart(ctrSat->getRangeStart().getSeconds() , PHRDate::_TimeFormat);
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tDebut Plage %1$s")) % aStart.getStringValue());
        beginOffset = (OCP_CpDate) aStart.getSeconds();

        PHRDate aEnd( ctrSat->getRangeEnd().getSeconds(), PHRDate::_TimeFormat);
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tFin Plage %1$s")) % aEnd.getStringValue());
        endOffset = (OCP_CpDate) aEnd.getSeconds();

    }
    catch(OcpXMLInvalidField& e )
    {
        //rien à faire si debut de plage horaire non renseignée
    }

    //prévoir 2 plages si période à cheval entre deux jours
    OCP_CpConstraint::buildTimeAreas(_plagesHoraires ,optimScope->getBegin() , optimScope->getEnd() , beginOffset , endOffset);

    /*
     * impacter la période étendue de la contrainte en fonction des plages horaires
     */
    boost::format plagesStr = boost::format( _("Plage(s) : ") );
    for(VectorOCP_CpTimeSlot::iterator it = _plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
    {
        OCP_CpTimeSlotPtr plage = (*it);//plage horaire
        PHRDate aBegin(plage->getBegin());
        PHRDate aEnd(plage->getEnd());
        plagesStr = boost::format( _("%1$s [%2$s %3$s] ") ) % plagesStr.str() % aBegin.getStringValue() % aEnd.getStringValue();
        enlargeExtenDedScope( *plage.get());
    }

    /*
     * affichage et nommage
     */

    boost::format msg = boost::format( _("SatelliteConstraint5-%1$s- %2$s %3$s nbSimultane max %4$d %5$s") )
     %  satellite->getName().c_str()
     % prefixSatellites.str()
     % prefixStations.str()
     % _nbSimultSat
     % plagesStr.str();

     setName(msg.str().c_str());
     OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());
}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint5::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
     /*
      * parcours des plages horaires : une instance de contrainte par plage horaire
      */
     IloAnd ctrAggregate = IloAnd(cpModel->getEnv());
     int cpt = 0;
     for(VectorOCP_CpTimeSlot::iterator it=_plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
     {
         OCP_CpTimeSlotPtr aPeriod = (*it);
         createOneConstraint(aPeriod ,ctrAggregate,cpt++);
     }

    setIloConstraint(ctrAggregate);
    getSatellite()->addOcpConstraint(getIloConstraint());
}

/**
* creation d'une contrainte ILOG SOLVER sur une plage horaire
* @param plageHoraire plage horaire demandée
* @param ctrAggregate contrainte aggrégeant la contrainte créée sur cette plage horaire
* @param cpt : compteur pour nommage de variable ensembliste
*/
void OCP_CpSatelliteConstraint5::createOneConstraint(OCP_CpTimeSlotPtr plageHoraire,IloAnd ctrAggregate,int cpt)
{
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();

    // filtering good slots from all tracking slots
    IloAnyArray allTrackingSlotsTmp = IloAnyArray(env);//aggrégation des slots de tous les satellites concernés
    IloAnyArray oneConstraintSlots(env);

    /*
     * parcours du groupe des satellites concernés par la contrainte
     * et collecter tous les créneaux candidats
     */
    for(std::map<OcpSatellitePtr, OCP_CpSatellitePtr>::iterator it = _satelliteMap.begin() ; it != _satelliteMap.end() ; it++)
    {
        OCP_CpSatellitePtr currentSatellite = (*it).second;
        allTrackingSlotsTmp.add(currentSatellite->getTrackingSlots());
        /*
         * filtrage des slots candidats pour l'un des satellites concernés
         */
        for(int i=0; i<currentSatellite->getTrackingSlots().getSize(); i++)
        {
            OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(currentSatellite->getTrackingSlots()[i]);
            OcpStationPtr aStation = tmp->getStation()->getOrigin();
            if (!tmp->toRemove() && !tmp->hasBookingOrder() && tmp->isInside(plageHoraire)
                    &&  (_stationsMap.size()==0 || _stationsMap.find(aStation) != _stationsMap.end() )
            )
            {
                addCandidateToDomain(tmp);
                oneConstraintSlots.add(tmp);
            }
            else
            {
                _complementarySlots.add(tmp);
            }
        }
    }


    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, oneConstraintSlots, oneConstraintSlots,"goodTrackingSlotsVar");
    // creating anySetVar (intersect) with all slots possible


	boost::format intersectName = boost::format( _("MyIntersectSAT5-%1$s--%2$s_%3$d ") ) % getSatellite()->getName().c_str() %  getId() % cpt;
	
    IloAnySetVar intersect(env, oneConstraintSlots,intersectName.str().c_str());

    //cpModel->addOneSetVarToTrace(intersect);

    /*
     * L'intersection entre les créneaux candidats issus de plusieurs satellites doit être réalisée avec la variable globale
     * et non la variable ensembliste du satellite courant
     */
    IloConstraint c1 =  IloEqIntersection(env, intersect, cpModel->getGlobalSlotSetVar(), goodTrackingSlotsVar);
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT5_%1$s_%2$d") )
    % getSatellite()->getName() % getId() ;
    c1.setName(ctrName.str().c_str());


    // constraint on intersect...
    IloConstraint c2 = IloSatelliteConstraint5_V2(env, _nbSimultSat, intersect , plageHoraire);

    ctrAggregate.add(c1 && c2);

}

