/*
 * $Id: OCP_CpSatelliteConstraint8.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include <string>

#include "constraints/OCP_CpSatelliteConstraint8.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpSatellite.h"
#include "OCP_CpExternWrapper.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpSatelliteConstraint8);

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	manage interSlotMinDuration as in IloSatelliteConstraint2
	manage the cumul variable as in IloSatelliteConstraint3
	manage the continuity with implication between the current slot and a previous and next one.
*/	

/**
 * Constructeur à partir de la contrainte métier
 * @param optimScope période d'instanciation de la contrainte
 * @param ctrSat contrainte métier origine
 * @param satellite satellite associé à la contrainte
 * @return
 */
OCP_CpSatelliteConstraint8::OCP_CpSatelliteConstraint8(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType8Ptr ctrSat,OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(optimScope,ctrSat, satellite)
  {
    static const std::string pseudoClazz = _Clazz + "::OCP_CpSatelliteConstraint8";

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();


    /**
     * Lecture de la fréquence associée à la contrainte
     */
    _frequencyBand = ctrSat->getFrequencyBand();

    /*
     * lecture du filtre de durée min sur les passages
     */
    _durationMin = 0;
    try
    {
        _durationMin = 60 * ctrSat->getMinSlotDuration();
    }
    catch (OcpXMLInvalidField& e)
    {
        //rien à faire si le champs n'est pas renseigné
    }

    /*
     * lecture du filtre de durée min sur les passages
     */
    _durationMax = -1;
    try
    {
        _durationMax = 60 * ctrSat->getMaxSlotDuration();
    }
    catch (OcpXMLInvalidField& e)
    {
        //rien à faire si le champs n'est pas renseigné
    }

    /*
     * lecture du cumul support demandé (champs obligatoire)
     */
    _maxCumulDuration = 60 * ctrSat->getTotalDuration();

    /*
     * lecture de la tolérance (champs obligatoire)
     */
    _tolerance = 60 * ctrSat->getTolerance();

    _interSlotMaxDuration = 0;
    try
    {
        _interSlotMaxDuration = 60 * ctrSat->getMaxGap();
    }
    catch (OcpXMLInvalidField& e)
    {
        //rien à faire si le champs n'est pas renseigné
    }

    _editable = false;
    try
    {
        _editable = ctrSat->getEditable();
    }
    catch(OcpXMLInvalidField&)
    {
        boost::format msg = boost::format( _("NIVEAU_CRENEAU non renseigné sur %1$s ") )
        %  satellite->getName()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg );
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }

    //_slotType = NULL;
    try
    {
        _slotType = ctrSat->getSlotType();
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("TYPE_CRENEAU non renseigné sur %1$s ") )
        %  satellite->getName()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg );
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }

     /*
     * début du support, obligatoire
     */
    PHRDate aSupportStart = ctrSat->getSupportStart();

    /*
     * fin du support, obligatoire
     */
    PHRDate aSupportEnd = ctrSat->getSupportEnd();

    /*
     * Le support doit intersecter la fenêtre courante
     */
   _support = OCP_CpTimeSlotPtr( new OCP_CpTimeSlot( (OCP_CpDate) aSupportStart.getSeconds() , (OCP_CpDate) aSupportEnd.getSeconds() ) );
   PHRDate endRange ( optimScope->getEnd());

   _lastDay = false;
   if (!_support->intersect(optimScope))
   {
       PHRDate beginRange ( optimScope->getBegin());

       boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %5$s : OCP_CpSatelliteConstraint8 Support [%1$s  %2$s] n'intersecte pas la fenêtre de calcul [%3$s  %4$s]") )
       %  aSupportStart.getStringValue() % aSupportEnd.getStringValue() % beginRange.getStringValue() % endRange.getStringValue() % satellite->getName();
       OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
       throw OCP_XRoot( msg.str() );

   }
   else
       /*
        * la fin du support est dans l'intervalle [debut fenêtre de calcul, fin de la fenêtre de calcul )
        */
       _lastDay = (_support->getEnd() <= optimScope->getEnd());

    /*
     * lecture des stations
     */
    boost::format prefixStations = boost::format(_(" Stations "));
    _stationsMap.clear();
    try
    {
        std::vector <OcpStationPtr> stations = ctrSat->getStations();
        for(std::vector <OcpStationPtr>::iterator it=stations.begin() ; it != stations.end() ; it++)
        {
            OcpStationPtr aStation = (*it);
            OCP_CpSatelliteBaseConstraint::onFrequencyError(_origin, aStation ,satellite, _frequencyBand);
            OCP_CpStationPtr station = cpModel->getStation(aStation);
            if(station != NULL)
            {
                _stationsMap.insert(StationMap::value_type(aStation , station ));
                prefixStations = boost::format(_(" %1$s %2$s ")) % prefixStations.str() % aStation->getName() ;
            }
        }
    }
    catch (OcpXMLInvalidField& e)
    {
        //rien à faire si le champs n'est pas renseigné
    }

    /*
     * notification de la zone de split
     */
    OCP_CpTimeSlot splitArea = _support->getIntersection(*optimScope.get());
    addSplitZone(splitArea);


    /*
     * Affichage et nommage de la contrainte
     */
    boost::format prefix;
    switch(_frequencyBand)
    {
        case FrequencyBands::S : prefix = boost::format ( _("S") );break;
        case FrequencyBands::X : prefix = boost::format ( _("X") );break;
        case FrequencyBands::SX : prefix = boost::format ( _("SX") );break;
        default : prefix = boost::format ( _("?") );break;
    }

    boost::format msg = boost::format( _("SatelliteConstraint8-%1$s-%2$ stations %3$s filtre durees (%4$d..%5$d minutes)-support %6$d minutes tolerance inferieure %7$d minutes interSlot (%8$d) minutes periode support [%9$s %10$s]") )
    %  satellite->getName().c_str()
    % prefix.str()
    %  prefixStations.str()
    % (_durationMin/60)
    % (_durationMax/60)
    % (_maxCumulDuration/60)
    % (_tolerance/60)
    % (_interSlotMaxDuration/60)
    % aSupportStart.getStringValue()
    % aSupportEnd.getStringValue();

    setName(msg.str().c_str());
    msg = boost::format( _("Creating %1$s") ) % msg.str() ;
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg.str());
  }

/**
 * calcule en dehors des slots variables (ceux de la fenêtre de calcul), la durée des slots candidats sur la partie
 * effectuée les jours avant le dernier jour
 * @param optimScope fenêtre de calcul courante
 * @return la durée en secondes de la visibilité "continue" avant le jour de la fenêtre de calcul quand celui-ci est le dernier jour
*/
OCP_CpDate OCP_CpSatelliteConstraint8::computePastDuration(OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::computePastDuration";

    OCP_CpDate pastDuration = 0;
    if(_support->getBegin() < optimScope->getBegin())//sinon, le support est complèment inclus dans la fenêtre de calcul
    {

        OcpContextPtr context = OcpXml::getInstance()->getContext();
        OCP_CpModel* cpModel = OCP_CpModel::getInstance();


        OCP_CpTimeSlot area(_support->getBegin() , optimScope->getBegin());
        PHRDate beginArea(area.getBegin());
        PHRDate endArea(area.getEnd());
        VectorSatelliteSlot listSlot = context->getSatelliteSlotsByPeriod(beginArea, endArea ,  getSatellite()->getOrigin() );

        for(VectorSatelliteSlot::iterator iterSatSlot = listSlot.begin() ; iterSatSlot != listSlot.end() ; iterSatSlot++)
        {
            OcpSatelliteSlotPtr aSatSlot = *iterSatSlot;
            OCP_CpStationPtr station = cpModel->getStation( aSatSlot->getStation() );

            /*
             * filtrage par les stations entrées en paramètres
             */
            if (station != NULL
                    && OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand , station->getStationFrequencyBand())
                    )
            {

                bool okAosLos = false;//ce créneau a-t-il une élévation sur SAT4 compatible avec la durée de SAT4 et la surcharge locale éventuelle
                int aPastDuration = 0;
                try
                {
                    OCP_CpTimeSlot ocpSatSlot = OCP_CpTrackingSlot::getPastAosLos(aSatSlot);
                    bool okMinDuration = (_durationMin<=0 || ocpSatSlot.getDuration() >= _durationMin) ;
                    bool okMaxDuration = (_durationMax<0 || ocpSatSlot.getDuration() <= _durationMax);
                    okAosLos = ocpSatSlot.intersect(&area) && okMinDuration && okMaxDuration;
                    if (okAosLos)
                        aPastDuration = ocpSatSlot.getIntersectionDuration(&area);
                }
                catch(OCP_XRoot& err)
                {
                    //rien à faire (message déjà loggé dans l'exception lancée depuis OCP_CpTrackingSlot::getPastAosLos)
                }

                bool isReserved = aSatSlot->getBookingState()  == SlotBookingStates::RESERVED
                        || aSatSlot->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
                bool hasBookingOrder = aSatSlot->hasBeenBookedByRequest();

                bool inMap = (_stationsMap.size()==0 &&
                        OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand , aSatSlot->getStation()->getFrequencyBand() ) )
                        || _stationsMap.find(aSatSlot->getStation()) != _stationsMap.end() ;
                /*
                 * les slots candidats pour les jours précédents sont ceux reservés (hors bookingOrder?) intersectant la partie du support
                 * avant le dernier jour mais qui ne font pas partie des slots candidats sur le jour courant (possibilité de passage intersectant
                 * le dernier jour)
                 */
                if (isReserved
                        && !hasBookingOrder
                        && okAosLos
                        && !getSatellite()->containsTrackingSlots(aSatSlot)
                        && inMap
                )
                    pastDuration += aPastDuration;
            }
        }
    }

    return pastDuration;
}
/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint8::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();

    // filtering good slots from current satellite tracking slots

    // majorant de la durée totale des slots concernés
    OCP_CpDate upperBoundDuration = 0;

    SpecialDurationPtr durations( new SpecialDuration());//par passage, la durée à comptabiliser
    for(int i=0; i<getSatellite()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getSatellite()->getTrackingSlots()[i]);
        if(!tmp->toRemove() && !tmp->hasBookingOrder() && tmp->isInside(_support))
        {
            OCP_CpDate duration = tmp->getDuration();
            bool inMap = (_stationsMap.size()==0 &&
                    OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand , tmp->getSlotOrigin()->getStation()->getFrequencyBand() )
                    ) || _stationsMap.find(tmp->getStation()->getOrigin()) != _stationsMap.end() ;
            if( duration >= _durationMin
                    && (_durationMax<0 || duration <= _durationMax)
                    && inMap
            )
            {
                addCandidateToDomain(tmp);
                OCP_CpDate interDuration = _support->getIntersectionDuration(tmp);
                durations->insert(SpecialDuration::value_type(tmp , interDuration ));
                upperBoundDuration += interDuration;
            }
            else
                _complementarySlots.add(tmp);
        }
    }


    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots, _domainSlots);


    // creating anySetVar (intersect) with all slots possible
    boost::format intersectName = boost::format( _("MyIntersectSAT8-%1$s--%2$s ") ) % getSatellite()->getName().c_str() %  getId();
    IloAnySetVar intersect(env, getSatellite()->getTrackingSlots(),intersectName.str().c_str());
    // intersect = TrackingSlotSetVar from the getSatellite() list INTER goodTrackingSlotsVar

    IloConstraint cSAT8_1 = IloEqIntersection(env, intersect, getSatellite()->getTrackingSlotSet(), goodTrackingSlotsVar);

    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT8_%1$s_%2$d") )
    % getSatellite()->getName() % getId() ;
    cSAT8_1.setName(ctrName.str().c_str());


    /*
     * l'aspect continu au trou près des intervalles choisis est géré par deux contraintes SAT2
     * SAT2 MIN avec un écart minimal de 0 pour assurer qu'on n'ait pas deux slots qui se chevauchent
     * SAT2 MAX avec un écart maximal de _interSlotMaxDuration pour autoriser un trou maximal entre deux slots consécutigs
     */
    IloConstraint cSAT8_Using_SAT2_Min = IloSatelliteConstraint2(env, intersect, 0);
    IloConstraint cSAT8_Using_SAT2_Max = IloSatelliteConstraint2Max(env, intersect, _interSlotMaxDuration);


    IloAnd ctrAggregate = IloAnd(cpModel->getEnv());
    // la contrainte d'intersection et d'écart max sont toujours implémentées
    ctrAggregate.add(cSAT8_1 && cSAT8_Using_SAT2_Min && cSAT8_Using_SAT2_Max);

    // sur le dernier jour, on rajoute la contrainte de durée minimale cumulée
    if (_lastDay)
    {
        OCP_CpDate cumulDone = computePastDuration(optimScope);
        OCP_CpDate remainMin = _maxCumulDuration-cumulDone - _tolerance;//ce qui reste à faire au minimum
        OCP_CpTimeSlot bounds1(0 , upperBoundDuration);//les bornes initiales du cumul au sens large
        OCP_CpTimeSlot bounds2(remainMin , remainMin+_tolerance);//la fourchette demandée par la contrainte pour le cumul
        if(! bounds1.intersect(&bounds2))
        {
            boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %1$s : SatelliteConstraint8-%2$s- : contrainte de cumul impossible à satisfaire cumulDone %3$i [0 , uppperBound : %4$i] ne peut intersecter [%5$i, %6$i]  ") )
                    % getSatellite()->getName() %  getName() % cumulDone % upperBoundDuration % remainMin % (remainMin+_tolerance)  ;
            OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );
            throw OCP_XRoot( msg.str() );
        }
        else if (bounds1.isInside(&bounds2))
        {
            boost::format msg = boost::format( _("SatelliteConstraint8-%1$s- : contrainte de cumul déjà satisfaite sur les jours précédents cumulDone %2$i _maxCumulDuration %3$i ") )
                    %  getName() % cumulDone % _maxCumulDuration;
            OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg );
        }
        else
        {
            IloConstraint cSAT8_Using_SAT3 = IloSatelliteConstraint3(env, remainMin, remainMin+_tolerance, intersect,upperBoundDuration,durations);
            ctrAggregate.add(cSAT8_Using_SAT3);
        }
    }

    setIloConstraint(ctrAggregate);

    //create the constraint on the last satellite
    getSatellite()->addOcpConstraint(getIloConstraint());
  }


/**
* split de la zone de support
* @param splitArea : partie du support dans la fenêtre de calcul
*/
void OCP_CpSatelliteConstraint8::addSplitZone(const OCP_CpTimeSlot& splitArea)
{
    static const std::string pseudoClazz = _Clazz + "::addSplitZone";
    boost::format msg = boost::format( _("Notification de la zone de split des créneaux ") ) ;
    OcpLogManager::getInstance()->debug(pseudoClazz, msg.str());


    if(_stationsMap.size()==0)
    {
        OCP_CpTrackingSlotPtr split( new OCP_CpTrackingSlot(getSatellite(), OCP_CpStationPtr(),  OcpSatelliteSlotPtr()));
        split->setBegin( splitArea.getBegin());
        split->setEnd( splitArea.getEnd());
        getSatellite()->addSplitZone(split);

    }
    else
    {
        for(StationMap::iterator it = _stationsMap.begin() ; it != _stationsMap.end() ; ++it)
        {
            OCP_CpStationPtr aStation = (*it).second;
            OCP_CpTrackingSlotPtr split( new OCP_CpTrackingSlot(getSatellite(), aStation,  OcpSatelliteSlotPtr()));
            split->setBegin( splitArea.getBegin());
            split->setEnd( splitArea.getEnd());
            getSatellite()->addSplitZone(split);
        }

    }

}

/**
 * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
 * @param slot le créneau à sauver
 */
void OCP_CpSatelliteConstraint8::postOptim(OCP_CpTimeSlot* slot)
{
    /*
     * positionnement du NIVEAU_CRENEAU
     */
    OCP_CpConstraint::setSlotLevel(slot,_editable);

    /*
     * positionnement du TYPE_CRENEAU
     */
    OCP_CpConstraint::setSupportType(slot , _slotType);

}
