/*
 * $Id: OCP_CpSatelliteBaseConstraint.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteBaseConstraint.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "constraints/OCP_CpConstraint.h"
#include "OCP_CpDate.h"
#include "planif/OCP_Solver.h"
#include <OcpXmlTypes.h>



using namespace std;
using namespace ocp::commons;



/**
 * constructeur de classe mère des contraintes satellites
 * @param period periode d'instanciation de la contrainte
 * @param origine contrainte métier origine
 * @param s satellite
 * @return
 */
OCP_CpSatelliteBaseConstraint::OCP_CpSatelliteBaseConstraint(OCP_CpTimeSlotPtr period, OcpConstraintPtr origine,  OCP_CpSatellitePtr s)
: OCP_CpConstraint(period,origine), _satellite(s)
  {
    _extendedScope = OCP_CpTimeSlotPtr( new OCP_CpTimeSlot(*period) );
  }

/**
* compatibilité entre la fréquence associée à une contrainte et la fréquence associée à une station donnée
* @param constraintFrequency fréquence associée à la contrainte
* @param stationFrequency fréquence de la station
* @return vrai ssi les fréquences sont compatibles (même fréquence ou patterns (X,SX), (S,SX)
*/
bool OCP_CpSatelliteBaseConstraint::frequencyCompatible(FrequencyBands::FrequencyBands_ENUM constraintFrequency , FrequencyBands::FrequencyBands_ENUM stationFrequency )
{
    if (constraintFrequency == stationFrequency)
        return true;
    else
    {
        if(stationFrequency == FrequencyBands::SX)
        {
            return constraintFrequency== FrequencyBands::S || constraintFrequency== FrequencyBands::X;
        }
        else
            return false;
    }

}


/**
* lance une exception si la station n'est pas compatible avec la fréquence demandée pour la contrainte
* @param origin : contrainte OCP origine
* @param aStation la station
* @param satellite satellite de la contrainte origine
* @param frequencyBand la fréquence demandée par la contrainte
*/
void OCP_CpSatelliteBaseConstraint::onFrequencyError(OcpConstraintPtr origin,OcpStationPtr aStation,OCP_CpSatellitePtr satellite, FrequencyBands::FrequencyBands_ENUM frequencyBand) throw(OCP_XRoot)
{
    if (!frequencyCompatible(frequencyBand,aStation->getFrequencyBand()))
    {
        boost::format prefix;
        switch(frequencyBand)
        {
            case FrequencyBands::S : prefix = boost::format ( _("S") );break;
            case FrequencyBands::X : prefix = boost::format ( _("X") );break;
            case FrequencyBands::SX : prefix = boost::format ( _("SX") );break;
            default : prefix = boost::format ( _("?") );break;
        }
        boost::format prefixStation;
        switch(aStation->getFrequencyBand() )
        {
            case FrequencyBands::S : prefixStation = boost::format ( _("S") );break;
            case FrequencyBands::X : prefixStation = boost::format ( _("X") );break;
            case FrequencyBands::SX : prefixStation = boost::format ( _("SX") );break;
            default : prefixStation = boost::format ( _("?") );break;
        }
        ConstraintTypesEnum ctrType  = origin->getConstraintType();
        boost::format errMsg = boost::format( _("Contrainte de type %1$s non respectée pour le satellite %2$s : OCP_CpSatelliteBaseConstraint frequence station %3$s incompatible avec %4$s") )
        % OcpConstraint::getStringFrom(ctrType)
        % satellite->getName() % prefixStation % prefix ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg.str());

        throw OCP_XRoot(errMsg.str());
    }
    else
    {
        //rien à faire
    }

}

/**
* étend la portée d'une contrainte au delà de la fenêtre de calcul suite à l'analyse de ses plages horaires
* @param timeSlot : la plage horaire
*/
void OCP_CpSatelliteBaseConstraint::enlargeExtenDedScope(const OCP_CpTimeSlot& timeSlot )
{
    _extendedScope->enlarge(timeSlot);
}
