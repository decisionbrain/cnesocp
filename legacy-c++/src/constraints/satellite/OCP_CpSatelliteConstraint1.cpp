/*
 * $Id: OCP_CpSatelliteConstraint1.cpp 943 2010-12-21 13:12:51Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint1.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpSatelliteConstraint1.h"
#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "resources/OCP_CpSatellite.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpSatelliteConstraint1);

/* GLOBAL IDEA FOR THIS CONSTRAINT:
	create a new setVar with all tracking slots verifying the condition (time interval and duration threshold)
	compute the intersection between this setVar and the original one
	store the cardinality constraint on this setVar using parameter n
	save the constraint by setIloConstraint
	take care, the constraint is not stored in the model!
*/

/**
* Constructeur à partir de la contrainte métier
* @param period période d'instanciation de la contrainte
* @param ctrSat1 contrainte métier origine
* @param satellite satellite associé à la contrainte
* @return
*/
OCP_CpSatelliteConstraint1::OCP_CpSatelliteConstraint1(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType1Ptr ctrSat1, OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(optimScope,ctrSat1, satellite)
{

    static const std::string pseudoClazz = _Clazz + "::OCP_CpSatelliteConstraint1";


    _minDuration = 0;//valeur surchargée par rapport à SAT4, si -1 pas de surcharge
    _stations.clear();
    _plagesOffset.clear();
    _plagesHoraires.clear();
    _constraintDays.clear();
    _frequencyBand = ctrSat1->getFrequencyBand();
    _stations = ctrSat1->getStations();
    _nbMinPassages = ctrSat1->getMinFlightPass();
    _nbMaxPassages = ctrSat1->getMaxFlightPass();
    _atHorse = false;



    //_slotType = NULL;
    try
    {
        _slotType = ctrSat1->getSlotType();
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("TYPE_CRENEAU non renseigné sur %1$s ") )
        %  satellite->getName()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg );
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }

    _editable = false;
    try
    {
        _editable = ctrSat1->getEditable();
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _("NIVEAU_CRENEAU non renseigné sur %1$s ") )
        %  satellite->getName()  ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg );
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }

    _useGreatestFlightPass = false;//artf625051 ne semble plus devoir être pris en compte dans la stratégie de recherche
    try
    {
        _useGreatestFlightPass = ctrSat1->useGreatestFlightPass();
    }
    catch(OcpXMLInvalidField&)
    {
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }


    boost::format prefixDays = boost::format(_(" Jours "));
    bool weekMode = false;
    try
    {
        _constraintDays = ctrSat1->getConstraintDays();
        weekMode = (_constraintDays.size()>1);
        boost::format weekMsg = boost::format( _("_constraintDays.size() %1$i weekMode %2$i") ) %  (_constraintDays.size()) % weekMode ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, weekMsg  );
    }
    catch(OcpXMLInvalidField&)
    {
        //rien à faire quand les champs ne sont pas lus : liste de jours vide = contrainte posée sur le jour courant
    }

    boost::format prefixPlages = boost::format(_(" Plages "));
    try
    {

        _plagesOffset = ctrSat1->getConstraintRanges();
    }
    catch(OcpXMLInvalidField&)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %1$s : SatelliteConstraint1 les plages horaires ne sont pas renseignées") )
                %  satellite->getName()  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg  );

        throw OCP_XRoot(msg.str());
    }

    try
    {
         _minDuration = ctrSat1->getMinDuration();
    }
    catch(OcpXMLInvalidField&)
    {
        //rien à faire quand les champs ne sont pas lus : on se contentera des valeurs par défaut
    }

    _useGreatestDuration = false;
    try
    {
        _useGreatestDuration = ctrSat1->useGreatestDuration();
    }
    catch(OcpXMLInvalidField&)
    {
        //rien à faire quand les champs ne sont pas lus : on se contentera des valeurs par défaut
    }

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

    //map pour la liste des stations optionnelles filtrant les passages satellites
    _stationsMap.clear();
    boost::format prefixStations = boost::format(_(" Stations "));
    for(std::vector <OcpStationPtr>::iterator it=_stations.begin() ; it != _stations.end() ; it++)
    {
        OcpStationPtr aStation = (*it);
        OCP_CpSatelliteBaseConstraint::onFrequencyError(_origin, aStation , satellite, _frequencyBand);
        OCP_CpStationPtr station = cpModel->getStation(aStation);
        if (station != NULL)
        {
            _stationsMap.insert(StationMap::value_type(aStation , station ));
            prefixStations = boost::format(_(" %1$s %2$s ")) % prefixStations.str() % aStation->getName() ;
        }
    }

    PHRDate currentDay(optimScope->getBegin());

    // lorsque le champs JOUR est renseigné, il ne faut poser la contrainte que les jours de la liste
    if(_constraintDays.size() >0 )
    {
        bool found = false;//le jour courant fait-il partie de la liste des jours de la contrainte ?
        for(std::vector <WeekDays::WeekDays_ENUM>::iterator it = _constraintDays.begin(); it != _constraintDays.end();it++)
        {
            prefixDays = boost::format(_("%1$s %2$i") ) % prefixDays.str() % (*it) ;//pour affichage des jours
            if ( (*it) == currentDay.getDayInWeek() )
                found = true;
        }
        if (!found ) //pas de contrainte après le dernier jour
        {
            boost::format msg = boost::format( _("SatelliteConstraint1-%1$s- mode semaine ne sera pas posée le jour %2$s qui n'est pas dans la liste des jours de la contrainte %3$s") )
            %  satellite->getName().c_str() % currentDay.getStringValue() % prefixDays.str() ;
            OcpLogManager::getInstance()->warning(pseudoClazz, msg  );
            setNoInstance();
            return;
        }
    }




    boost::format prefixFreq;
    switch(_frequencyBand)
    {
        case FrequencyBands::S:  prefixFreq = boost::format ( _("S") );  break;
        case FrequencyBands::X:  prefixFreq = boost::format ( _("X") );  break;
        case FrequencyBands::SX: prefixFreq = boost::format ( _("SX") ); break;
        default:                 prefixFreq = boost::format ( _("?") );  break;
    }

    if(_nbMinPassages <0  )
    {
        boost::format errMsg = boost::format(_ ("Contrainte non respectée pour le satellite %2$s : OCP_CpSatelliteConstraint1 Le champs _nbMinPassages (%1$i)  est négatif. Non autorisé" ) )
                % _nbMinPassages  % satellite->getName() ;
        OcpLogManager::getInstance()->error(pseudoClazz, errMsg);
        throw OCP_XRoot(errMsg.str());
    }

    // traitement des plages horaires
    /*
     * artf625123 : sans plage horaire renseignée, on crée la plage sur la portée courante
     */
    if (_plagesOffset.size()==0)
    {
        OCP_CpSatelliteBaseConstraint::buildTimeAreas(_plagesHoraires , optimScope->getBegin() , optimScope->getEnd() , 0, 0 );
        PHRDate aBegin(_plagesHoraires.front()->getBegin() );
        PHRDate aEnd(_plagesHoraires.front()->getEnd() );
        prefixPlages = boost::format(_("%1$s Plage horaire [%2$s;%3$s]  "))
            % prefixPlages % aBegin.getStringValue() % aEnd.getStringValue() ;
    }
    else
    {
        for(VectorOcpTimePeriod::iterator it=_plagesOffset.begin() ; it != _plagesOffset.end() ; it++)
        {
            OcpTimePeriodPtr aPeriod = (*it);//plage horaire relative
            prefixPlages = boost::format(_("%1$s Plage horaire [%2$s;%3$s] "))
            % prefixPlages % aPeriod->getStart().getStringValue() % aPeriod->getEnd().getStringValue()
            ;

            OCP_CpTimeSlot ts( (OCP_CpDate) aPeriod->getStart().getSeconds() , (OCP_CpDate) aPeriod->getEnd().getSeconds());
            if(ts.getEnd() < ts.getBegin())
                _atHorse = true;//au moins une plage à cheval

            // construit la ou les plages horaires en secondes absolues
            OCP_CpSatelliteBaseConstraint::buildTimeAreas(_plagesHoraires , optimScope->getBegin() , optimScope->getEnd() , ts.getBegin(), ts.getEnd());
        }
    }


    // nommage et affichage
    boost::format msg = boost::format( _("SatelliteConstraint1-id %12$d -%1$s- mode semaine %10$i %2$s filtres durée min %3$i (secondes) useGreatestDuration %4$i  nbPassages dans [%5$i,%6$i] %7$s %8$s %9$s type creneau %11$s") )
            %  satellite->getName().c_str() % prefixFreq.str() % _minDuration % _useGreatestDuration % _nbMinPassages % _nbMaxPassages
            % prefixStations % prefixDays % prefixPlages % weekMode % _slotType % getId();

    OcpLogManager::getInstance()->info(pseudoClazz, boost::format(_("\tCreating %1$s")) % msg.str());
    setName(msg.str().c_str());

    /*
     * impacter la période étendue de la contrainte en fonction des plages horaires
     * et notifier pour la suite que sur chaque plage horaire, on autorisera suivant la valeur du paramètre VISI_COMPLETE
     * les zones à visibilité partielle (split zone) où l'on créera de nouveaux sous-créneaux
     */
    for(VectorOCP_CpTimeSlot::iterator it = _plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
    {
        OCP_CpTimeSlotPtr plage = (*it);//plage horaire
        enlargeExtenDedScope( *plage.get());

    }
}


/**
* calcul du nombre de passages concernés sur tous les jours avant le jour de la plage courante
* il est nécessaire de le faire à ce moment car certains slots peuvent être à la fois reservés dans un calcul précédent et faire
* partie des possibles du satellite sur la fenêtre courante. Il faut alors ne pas doublonner ceux-ci.
* @param optimScope
* @return nombre de passages passés en mode semaine
*/
int OCP_CpSatelliteConstraint1::computePastPassages(OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::computePastPassages";

    int pastPassages = 0;

    PHRDate aDay(optimScope->getBegin());
    VectorOCP_CpTimeSlot allCandidateAreas;//toutes les plages candidates les jours précédant le jour courant

    /* parcours des jours de la liste sauf le dernier
     * le jour courant doit etre exclu du calcul incrémental car tous les slots concernés sont déjà comptabilisés dans la partie
     * variable (via la cardinalité sur la variable ensembliste)
     */
    for(std::vector <WeekDays::WeekDays_ENUM>::iterator it = _constraintDays.begin();
            it != _constraintDays.end() && (*it) < aDay.getDayInWeek(); it++)
    {
        OCP_CpDate offset = (aDay.getDayInWeek() -(*it)  ) * PHRDate::SECONDS_IN_A_DAY;
        OCP_CpTimeSlot pastRange(optimScope->getBegin() - offset , optimScope->getEnd() - offset);

        /*
         * pour le jour passé 'pastRange', reconstruire ses plages candidates
         */

        if (_plagesOffset.size()==0)
        {
            OCP_CpSatelliteBaseConstraint::buildTimeAreas(allCandidateAreas , pastRange.getBegin() , pastRange.getEnd() , 0, 0 );

        }
        else
        {
            for(VectorOcpTimePeriod::iterator it=_plagesOffset.begin() ; it != _plagesOffset.end() ; it++)
            {
                OcpTimePeriodPtr aPeriod = (*it);//plage horaire relative
                OCP_CpTimeSlot ts( (OCP_CpDate) aPeriod->getStart().getSeconds() , (OCP_CpDate) aPeriod->getEnd().getSeconds());
                // construit la ou les plages horaires en secondes absolues
                OCP_CpSatelliteBaseConstraint::buildTimeAreas(allCandidateAreas , pastRange.getBegin() , pastRange.getEnd() , ts.getBegin(), ts.getEnd());
            }
        }
    }

    /*
     * on doit aussi collecter les passages passés dès lors qu'on est en mode "jour" avec au moins une plage à cheval
     */
    if(_atHorse && _constraintDays.size()==0)
    {
        OCP_CpDate offset = PHRDate::SECONDS_IN_A_DAY;
        OCP_CpTimeSlot pastRange(optimScope->getBegin() - offset , optimScope->getEnd() - offset);
        for(VectorOcpTimePeriod::iterator it=_plagesOffset.begin() ; it != _plagesOffset.end() ; it++)
        {
            OcpTimePeriodPtr aPeriod = (*it);//plage horaire relative
            OCP_CpTimeSlot ts( (OCP_CpDate) aPeriod->getStart().getSeconds() , (OCP_CpDate) aPeriod->getEnd().getSeconds());
            // construit la ou les plages horaires en secondes absolues
            OCP_CpSatelliteBaseConstraint::buildTimeAreas(allCandidateAreas , pastRange.getBegin() , pastRange.getEnd() , ts.getBegin(), ts.getEnd());
        }
    }

    OcpContextPtr context = OcpXml::getInstance()->getContext();
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    // parcours de toutes les plages absolues puis de tous les slots reservés dans ces périodes
    for(VectorOCP_CpTimeSlot::iterator it = allCandidateAreas.begin() ; it != allCandidateAreas.end() ; it++)
    {
        OCP_CpTimeSlotPtr area = (*it);
        // charger les slots sur cette plage
        PHRDate beginArea(area->getBegin());
        PHRDate endArea(area->getEnd());

        VectorSatelliteSlot listSlot = context->getSatelliteSlotsByPeriod(beginArea, endArea ,  getSatellite()->getOrigin() );

        for(VectorSatelliteSlot::iterator iterSatSlot = listSlot.begin() ; iterSatSlot != listSlot.end() ; iterSatSlot++)
        {
            OcpSatelliteSlotPtr aSatSlot = *iterSatSlot;
            OCP_CpStationPtr station = cpModel->getStation( aSatSlot->getStation() );
            FrequencyBands::FrequencyBands_ENUM stationFrequency = station->getOrigin()->getFrequencyBand();

            /*
             * la station du créneau est-elle candidate ?
             * non si la station n'a pas été chargée dans le contexte
             * sinon,  vérifier que la fréquence de la station est compatible avec celle de la contrainte
             * puis que la station fait partie de la liste des stations quand celle-ci est renseignée
             */

            bool inMap = station != NULL && OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand,stationFrequency) &&
                    (_stations.size() == 0 || _stationsMap.find(aSatSlot->getStation()) != _stationsMap.end() );



            // filtrage par les stations entrées en paramètres
            if (inMap)
            {
                bool okAosLos = false;//ce créneau a-t-il une élévation sur SAT4 compatible avec la durée de SAT4 et la surcharge locale éventuelle
                try
                {
                    OCP_CpTimeSlot ocpSatSlot = OCP_CpTrackingSlot::getPastAosLos(aSatSlot);
                    bool okMinDuration = (_minDuration<=0 || ocpSatSlot.getDuration() >= _minDuration) ;
                    okAosLos = ocpSatSlot.isInside(area) && okMinDuration;
                }
                catch(OCP_XRoot& err)
                {
                    //rien à faire (message déjà loggé dans l'exception lancée depuis OCP_CpTrackingSlot::getPastAosLos)
                }
                bool isReserved = aSatSlot->getBookingState()  == SlotBookingStates::RESERVED
                        || aSatSlot->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
                bool hasBookingOrder = aSatSlot->hasBeenBookedByRequest();
                /*
                 * les slots candidats pour les jours précédents sont ceux reservés hors bookingOrder à l'intérieur des plages mais
                 * qui ne font pas partie des slots candidats sur le jour courant (possibilité de plage à cheval sur le
                 * dernier jour)
                 * Par ailleurs, le test sur la durée est fait selon l'hypothèse que les slots prépositionnés dans le passé
                 * respectent déjà la durée min et l'élévation requises par SAT4
                 */

                if (isReserved
                        && !hasBookingOrder
                        && okAosLos
                        && !getSatellite()->containsTrackingSlots(aSatSlot))
                {
                    pastPassages++;
                    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("past passage %1$s")) % OCP_CpTrackingSlot::getOriginFullName(aSatSlot) );
                }

            }
        }

    }
    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("%1$d passages passés contribuant à la contrainte")) % pastPassages);
    return pastPassages;
}

/**
* collection des slots candidats sur la portée courante et calcul sur le passé si nécessaire
* @param optimScope fenêtre de calcul courante
* @param lastDay renseigné pour indiquer si l'on est le dernier jour de la série
* @param pastPassages renseigne le nombre de slots candidats en mode semaine dans le passé
* @param candidates le tableau des crénaux potentiels sur la fenêtre courante
* @param candidatesDayBefore : le tableau des créneaux potentiels instanciant la contrainte la veille en cas de plage à cheval
* @param nbReserved : nombre de créneaux reservés sur la fenêtre courante
* @param nbReservedBefore : nombre de créneaux reservés sur la fenêtre de la veille (mode jour et plage à cheval)
*/
void OCP_CpSatelliteConstraint1::collectAllSlots(OCP_CpTimeSlotPtr optimScope,bool& lastDay,int& pastPassages,IloAnyArray candidates, IloAnyArray candidatesDayBefore,int& nbReserved, int& nbReservedBefore)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);
    static const std::string pseudoClazz = _Clazz + "::collectAllSlots";

    IloEnv env = OCP_CpModel::getInstance()->getEnv();
    OCP_CpDate offset = PHRDate::SECONDS_IN_A_DAY;
    nbReserved = 0;
    nbReservedBefore=0;

    /*
     * nombre de passages reservés dans le "passé" : les jours précédents en mode semaine, la veille en mode jour en cas de plage à cheval
     */
    pastPassages = computePastPassages(optimScope);

    if (_constraintDays.size())
    {
        std::vector <WeekDays::WeekDays_ENUM>::reverse_iterator it = _constraintDays.rbegin();//dernier de la liste JOUR
        PHRDate aDay(optimScope->getBegin());
        lastDay =  (*it)  == aDay.getDayInWeek() ;
    }

    OCP_CpTrackingSlot* longestCandidate = NULL;//candidat le plus long en cas de _useGreatestDuration


    // filtering good slots from all tracking slots

    for(int i = 0; i < getSatellite()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getSatellite()->getTrackingSlots()[i]);
        if(!tmp->toRemove())
        {
            OcpSatelliteSlotPtr slot = tmp->getSlotOrigin();

            /**
            * la station du slot est-elle dans la liste ou bien, si pas de liste de station, cette station a-t-elle la bonne fréquence
            */
            bool inMap = ( _stations.size() == 0 && OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand,slot->getStation()->getFrequencyBand()) ) ||
                    _stationsMap.find(slot->getStation()) != _stationsMap.end();

            /**
            * le slot intersecte-t-il la période de validité de la contrainte et l'une des plages horaires
            */
            bool foundRange = false;
            bool foundRangeDayBefore = false;
            for(VectorOCP_CpTimeSlot::iterator it = _plagesHoraires.begin() ; it != _plagesHoraires.end() && !foundRange ; it++)
            {
                OCP_CpTimeSlotPtr area = (*it);
                foundRange = tmp->isInside(area);
                if(_atHorse && _constraintDays.size()==0)
                {
                    OCP_CpTimeSlot areaBefore(area->getBegin()-offset , area->getEnd() - offset);
                    foundRangeDayBefore = tmp->isInside(&areaBefore);
                }
            }

            /*
            * filtrer les slots respectant toutes les conditions
            */
            bool okMinDuration = (_minDuration<=0 || tmp->getDuration() >= _minDuration) ;//TODO comparaison int32_t ne marche pas avec les négatifs ?
            if( (foundRange || foundRangeDayBefore)
                    && okMinDuration
                    && inMap
                    && !tmp->hasBookingOrder())
            {
                addCandidateToDomain(tmp);

                if(foundRange)
                {
                    candidates.add(tmp);
                    nbReserved += tmp->isReserved();
                }
                if(foundRangeDayBefore)
                {
                    candidatesDayBefore.add(tmp);
                    nbReservedBefore += tmp->isReserved();
                }

                /*
                 * mise à jour du "passage le plus long"
                 */
                if (foundRange && _useGreatestDuration && (longestCandidate==NULL || tmp->getDuration() > longestCandidate->getDuration()))
                {
                    longestCandidate = tmp;
                }
            }
            else
            {
                _complementarySlots.add(tmp);
            }
        }
    }

    if(longestCandidate != NULL)
    {
        longestCandidate->setIsLongestInSat1();
        OcpLogManager::getInstance()->debug(pseudoClazz,
                boost::format(_("Le créneau %1$s est le plus long possible ")) % longestCandidate->getFullName().str());
    }

    OcpLogManager::getInstance()->debug(pseudoClazz, boost::format(_("Nb good slots = %1$d")) % _domainSlots.getSize());
}



/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint1::instantiate(OCP_CpTimeSlotPtr optimScope)
{

    if (_constraintDays.size() >0)
        instantiateWeekMode(optimScope);
    else if(_atHorse)
        instantiateAtHorse(optimScope);
    else
        instantiateNotAtHorse(optimScope);
}

/**
* création de la contrainte ILOG SOLVER en mode multi-jours
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint1::instantiateWeekMode(OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::instantiate";

    IloEnv env = OCP_CpModel::getInstance()->getEnv();
    OCP_CpSatellitePtr satellite = getSatellite();


    /*
     * récupération des slots candidats sur la portée courante
     */

    bool lastDay = false;
    int pastPassages = 0;
    int nbReserved = 0;
    int nbReservedBefore = 0;
    IloAnyArray candidates(env);
    IloAnyArray candidatesDayBefore(env);
    collectAllSlots(optimScope,lastDay,pastPassages,candidates,candidatesDayBefore,nbReserved,nbReservedBefore);

    /*
     * en mode jour ou en mode dernier jour de la semaine, si on ne peut atteindre la borne min on échoue
     */
    if ( lastDay && pastPassages + _domainSlots.getSize() < _nbMinPassages)
    {
        boost::format msg = boost::format( _("Contrainte %1$s non respectée  : concerne %2$i passages potentiels (dont %3$i passés) mais nbMinPassages = %4$i") )
                % getName() % (pastPassages + _domainSlots.getSize()) % pastPassages % _nbMinPassages  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }

    /*
     * en mode jour ou semaine, toujours lancer une erreur si on a dépassé sûrement le nombre maximum demandé
     */
    if (_nbMaxPassages>=0 && nbReserved + pastPassages> _nbMaxPassages)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %4$s : La contrainte %3$s est toujours fausse : %1$i passages reservés > nbMaxPassages = %2$i") )
                % (pastPassages+nbReserved) % _nbMaxPassages % getName() % getSatellite()->getName();
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }

    // anySetVar avec les slots candidats
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots, _domainSlots);
    // anySetVar (intersect) avec tous les slots possibles
    boost::format intersectName = boost::format( _("MyIntersectSAT1-%1$s--%2$d") ) % getSatellite()->getName().c_str() % getId() ;
    IloAnySetVar intersect(env, satellite->getTrackingSlots(),intersectName.str().c_str());

    OCP_CpModel::getInstance()->addOneSetVarToTrace(intersect);

    // intersect = TrackingSlotSetVar du  satellite INTER goodTrackingSlotsVar (les candidats)
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT1_WK_%1$s_%2$d") )
            % getSatellite()->getName() % getId() ;
    IloConstraint c1 =  IloEqIntersection(env, intersect, satellite->getTrackingSlotSet(), goodTrackingSlotsVar);
    c1.setName(ctrName.str().c_str());

    int maxPassages = _nbMaxPassages;
    int minPassages = _nbMinPassages;

    /*
     * quand l'attribut de max passages n'a pas été renseigné, il n'y a pas de limite max
     */
    if(_nbMaxPassages < 0)
    {
        maxPassages = _domainSlots.getSize();
    }
    else
    {
        maxPassages -= pastPassages;
    }

    /*
     * en mode semaine, on ne peut poser la contrainte de cardinalité minimum que le dernier jour, sinon, on prend 0 comme borne min pour les
     * jours précédents
     */
    if (!lastDay)
            minPassages = 0;
        else
            minPassages -= pastPassages;


    boost::format msg = boost::format( _("Instanciation de cardinalité entre [%1$d, %2$d] pour %3$s") )
    % minPassages % maxPassages % getName() ;
    OcpLogManager::getInstance()->debug(pseudoClazz, msg);


    IloIntVar nbPassages(env,minPassages,maxPassages);
    IloConstraint c2 =  (nbPassages == IloCard(intersect));
    setIloConstraint(c1 && c2);


    satellite->addOcpConstraint(getIloConstraint());

    OCP_CpModel::getInstance()->addCardinalityInfo(intersect,nbPassages,_domainSlots);
    getSatellite()->pushStableCandidate(getId() , intersect , minPassages, _domainSlots);

}

/**
* création de la contrainte ILOG SOLVER en mode jour sans période à cheval
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint1::instantiateNotAtHorse(OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::instantiate";

    IloEnv env = OCP_CpModel::getInstance()->getEnv();
    OCP_CpSatellitePtr satellite = getSatellite();

    /*
     * récupération des slots candidats sur la portée courante (ici, lastDay et pastPassages ne sont pas modifiés)
     */
    bool lastDay = false;
    int pastPassages = 0;
    int nbReserved = 0;
    int nbReservedBefore = 0;
    IloAnyArray candidates(env);
    IloAnyArray candidatesDayBefore(env);
    collectAllSlots(optimScope,lastDay,pastPassages,candidates,candidatesDayBefore,nbReserved,nbReservedBefore);



    /*
     * en mode jour ou en mode dernier jour de la semaine, si on ne peut atteindre la borne min on échoue
     */
    if ( _domainSlots.getSize() < _nbMinPassages)
    {
        boost::format msg = boost::format( _("Contrainte %1$s non respectée  : concerne %2$i passages potentiels (dont %3$i passés) mais nbMinPassages = %4$i") )
                % getName() % (pastPassages + _domainSlots.getSize()) % pastPassages % _nbMinPassages  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }

    /*
     * en mode jour ou semaine, toujours lancer une erreur si on a dépassé sûrement le nombre maximum demandé
     */
    if (_nbMaxPassages>=0 && nbReserved + pastPassages> _nbMaxPassages)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %4$s : La contrainte %3$s est toujours fausse : %1$i passages reservés > nbMaxPassages = %2$i") )
                % (pastPassages+nbReserved) % _nbMaxPassages % getName() % getSatellite()->getName();
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }

    // anySetVar avec les slots candidats
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots, _domainSlots);
    // anySetVar (intersect) avec tous les slots possibles
    boost::format intersectName = boost::format( _("MyIntersectSAT1-%1$s--%2$d") ) % getSatellite()->getName().c_str() % getId() ;
    IloAnySetVar intersect(env, satellite->getTrackingSlots(),intersectName.str().c_str());

    OCP_CpModel::getInstance()->addOneSetVarToTrace(intersect);

    // intersect = TrackingSlotSetVar du  satellite INTER goodTrackingSlotsVar (les candidats)
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT1_NAH_%1$s_%2$d") )
                % getSatellite()->getName() % getId() ;

    IloConstraint c1 =  IloEqIntersection(env, intersect, satellite->getTrackingSlotSet(), goodTrackingSlotsVar);
    c1.setName(ctrName.str().c_str());
    //if (getId()==96)
    OCP_CpModel::getInstance()->addOneSetVarToTrace(intersect);

    int maxPassages = _nbMaxPassages;
    int minPassages = _nbMinPassages;

    /*
     * quand l'attribut de max passages n'a pas été renseigné, il n'y a pas de limite max
     */
    if(_nbMaxPassages < 0)
    {
        maxPassages = _domainSlots.getSize();
    }
    else
    {
        maxPassages -= pastPassages;
    }


    boost::format msg = boost::format( _("Instanciation de cardinalité entre [%1$d, %2$d] pour %3$s") )
    % minPassages % maxPassages % getName() ;
    OcpLogManager::getInstance()->debug(pseudoClazz, msg);


    IloIntVar nbPassages(env,minPassages,maxPassages);
    IloConstraint c2 =  (nbPassages == IloCard(intersect));
    setIloConstraint(c1 && c2);


    satellite->addOcpConstraint(getIloConstraint());

    OCP_CpModel::getInstance()->addCardinalityInfo(intersect,nbPassages,_domainSlots);
    getSatellite()->pushStableCandidate(getId() , intersect , minPassages, _domainSlots);

}

/**
* création de deux contraintes ILOG SOLVER en mode jour avec période à cheval
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint1::instantiateAtHorse(OCP_CpTimeSlotPtr optimScope)
{
    static const std::string pseudoClazz = _Clazz + "::instantiateAtHorse";

    IloEnv env = OCP_CpModel::getInstance()->getEnv();
    OCP_CpSatellitePtr satellite = getSatellite();

    /*
     * récupération des slots candidats sur la portée courante
     */
    bool lastDay = false;
    int pastPassages = 0;
    int nbReserved = 0;
    int nbReservedBefore=0;
    /*
     * candidats sur la fenêtre courante, candidats passés la veille et reservés sur la fenêtre courante
     */
    IloAnyArray candidates(env);
    IloAnyArray candidatesDayBefore(env);
    collectAllSlots(optimScope,lastDay,pastPassages,candidates,candidatesDayBefore,nbReserved,nbReservedBefore);

    /*
     * en mode jour sur la fenêtre courante, si on ne peut atteindre la borne min on échoue
     */
    if ( candidates.getSize() < _nbMinPassages)
    {
        boost::format msg = boost::format( _("Contrainte %1$s non respectée  : concerne %2$i passages potentiels mais nbMinPassages = %3$i") )
                % getName() %  candidates.getSize() % _nbMinPassages  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }

    /*
     * en mode jour lancer une erreur si on a dépassé sûrement le nombre maximum demandé
     */
    if (_nbMaxPassages>=0 && nbReserved > _nbMaxPassages)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %1$s : La contrainte %2$s est toujours fausse : %3$i passages reservés > nbMaxPassages = %4$i") )
        % getSatellite()->getName() % getName() % nbReserved % _nbMaxPassages  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }

    /*
     * en mode jour et à cheval lancer une erreur si on a dépassé sûrement le nombre maximum demandé la veille
     */
    if (_nbMaxPassages>=0 && nbReservedBefore + pastPassages> _nbMaxPassages)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %1$s : La contrainte %2$s est toujours fausse : %3$i passages reservés > nbMaxPassages = %4$i") )
        % getSatellite()->getName() % getName() % (pastPassages+nbReservedBefore) % _nbMaxPassages  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }

    IloAnd ctrAggregate = IloAnd(env);

    /*
     * 1. Pose de la contrainte la veille : 0 <= card <= maxPassages
     * on ne pose pas la contrainte min pour la veille car soit elle a été vérifiée dans le calcul précédent
     * et ne pourra donc être violée sur la fenêtre courante, soit il n'y a pas eu de calcul précédent et on risque
     * de ne pas pouvoir la satisfaire
     */
    if(_nbMaxPassages <0 || pastPassages+candidatesDayBefore.getSize() <= _nbMaxPassages )
    {
        boost::format msg = boost::format( _("Contrainte 'à cheval la veille' %1$s déjà respectée pour le satellite %2$s : %3$i passages passés et  %4$i potentiels <= max passages %5$i") )
        % getName() % getSatellite()->getName() % pastPassages % candidatesDayBefore.getSize() % _nbMaxPassages  ;
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
    }
    else
    {
        instantiateOneDayAtHorse(candidatesDayBefore,ctrAggregate,0,_nbMaxPassages -pastPassages );
    }

    /*
     * 2. Pose de la contrainte le jour courant
     */
    int maxPassages = _nbMaxPassages;
    if(_nbMaxPassages < 0)
    {
        maxPassages = candidates.getSize();
    }
    instantiateOneDayAtHorse(candidates,ctrAggregate,_nbMinPassages,maxPassages);

    setIloConstraint(ctrAggregate);
    getSatellite()->addOcpConstraint(getIloConstraint());
}

/**
* instantiation d'une contrainte en mode "jour à cheval"
* @param candidates les slots candidats de la journée courante
* @param ctrAggregate la contrainte aggrégeant le jour de la fenêtre de calcul et la veille
* @param minPassages nombre min de passages sur la journée courante
* @param maxPassages nombre max de passages sur la journée courante
*/
void OCP_CpSatelliteConstraint1::instantiateOneDayAtHorse(IloAnyArray candidates,IloAnd ctrAggregate,int minPassages, int maxPassages)
{
    static const std::string pseudoClazz = _Clazz + "::instantiateOneDayAtHorse";

    IloEnv env = OCP_CpModel::getInstance()->getEnv();

    // anySetVar avec les slots candidats
    IloAnySetVar goodTrackingSlotsVar(env, candidates, candidates);
    // anySetVar (intersect) avec tous les slots possibles
    boost::format intersectName = boost::format( _("MyIntersectSAT1-%1$s--%2$d") ) % getSatellite()->getName().c_str() % getId() ;
    IloAnySetVar intersect(env, getSatellite()->getTrackingSlots(),intersectName.str().c_str());

    OCP_CpModel::getInstance()->addOneSetVarToTrace(intersect);

    // intersect = TrackingSlotSetVar du  getSatellite() INTER goodTrackingSlotsVar (les candidats)
    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT1_AH_%1$s_%2$d") )
        % getSatellite()->getName() % getId() ;
    IloConstraint c1 =  IloEqIntersection(env, intersect, getSatellite()->getTrackingSlotSet(), goodTrackingSlotsVar);
    c1.setName(ctrName.str().c_str());



    boost::format msg = boost::format( _("Instanciation de cardinalité entre [%1$d, %2$d] pour %3$s") )
    % minPassages % maxPassages % getName() ;
    OcpLogManager::getInstance()->debug(pseudoClazz, msg);


    IloIntVar nbPassages(env,minPassages,maxPassages);
    IloConstraint c2 =  (nbPassages == IloCard(intersect));
    ctrAggregate.add(c1 && c2);


    OCP_CpModel::getInstance()->addCardinalityInfo(intersect,nbPassages,candidates);
    getSatellite()->pushStableCandidate(getId() , intersect , minPassages, candidates);



}


/**
 * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
 * @param slot le créneau à sauver
 */
void OCP_CpSatelliteConstraint1::postOptim(OCP_CpTimeSlot* slot)
{
    /*
     * positionnement du NIVEAU_CRENEAU
     */
    OCP_CpConstraint::setSlotLevel(slot,_editable);

    /*
     * positionnement du TYPE_CRENEAU
     */
    OCP_CpConstraint::setSupportType(slot , _slotType);
}
/**
* export dans format texte pour préparation de données pour IBM-ILOG OPL
* @param file
*/
void OCP_CpSatelliteConstraint1::exportOplModel(ofstream& file)
{
    file << getId() << ";SAT1;" << _nbMinPassages << ";" << _nbMaxPassages << endl;
}
