/*
 * $Id: OCP_CpSatelliteConstraint4.cpp 899 2010-10-19 17:37:56Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint4.cpp
 *
 */
#include <string>
#include <list>
#include <algorithm>

#include "constraints/OCP_CpSatelliteConstraint4.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpSatellite.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpExternWrapper.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpSatelliteConstraint4);

/**
* Constructeur à partir de la contrainte métier
* @param optimScope fenêtre de calcul
* @param ctrSat contrainte métier origine
* @param satellite satellite associé à la contrainte
* @return
*/
OCP_CpSatelliteConstraint4::OCP_CpSatelliteConstraint4(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType4Ptr ctrSat,OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(optimScope, ctrSat,satellite)
  {

    _consecutive = false; //par défaut on n'autorise pas le suivi consécutif
    VectorOcpStationProperties  stationProperties = ctrSat->getStationsProperties();
    if (stationProperties.size()==0)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %1$s : OCP_CpSatelliteConstraint4 Pas de station properties : contrainte ignorée "))
        % satellite->getName() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg );
        throw OCP_XRoot(msg.str());
    }
    else
    {
        satellite->setHasSat4Constraint();
        _properties.clear();
        mapProperties(stationProperties);

        // affichage des stations concernées
        for(std::map<OcpStationPtr, OcpStationPropertiesPtr>::iterator it = _properties.begin() ; it != _properties.end() ; ++it)
        {
            OcpStationPropertiesPtr stationProperty = (*it).second;
            boost::format msg = boost::format( _(" station property lue pour station %1$s "))
            % stationProperty->getStation()->getName() ;
            OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg );
        }
    }
    try
    {
        _consecutive = ctrSat->getConsecutive();
    }
    catch(OcpXMLInvalidField& e)
    {
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, _("Le flag sur la consécutivité de la CSat4 n'a pas pu être lu"));
    }

    // recherche d'extension de chargement des slots satellites
    PHRDate aDate(optimScope->getBegin());
    phr::int16u indexDay =  aDate.getDayInWeek();

    for(std::map<OcpStationPtr, OcpStationPropertiesPtr>::iterator it = _properties.begin() ; it != _properties.end() ; ++it)
    {
        OcpStationPropertiesPtr stationProperty = (*it).second;
        searchForExtendedScope(optimScope , stationProperty,indexDay);
    }

    boost::format msg = boost::format( _("SatelliteConstraint4-%1$s consecutivité %2$i") )
    %  satellite->getName().c_str() % _consecutive;
    setName(msg.str().c_str());
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());

  }


/**
* regarde s'il faut étendre l'optimScope de la contrainte en fonction des périodes horaires
* @param optimScope fenêtre de calcul
* @param stationProperty : les propriétés d'une station
* @param dayIndex : index du jour courant du calcul. Si ce jour ne correspond pas à l'un des jours de la semaine pour lequel la station est disponible, rien à faire
*/
void OCP_CpSatelliteConstraint4::searchForExtendedScope(OCP_CpTimeSlotPtr optimScope, OcpStationPropertiesPtr stationProperty,phr::int16u dayIndex)
{

    // parcours des jours et plages horaires de la contrainte et vérification de l'inclusion pour le slot courant
    std::vector <WeekDays::WeekDays_ENUM> constraintDaysProperty;
    try
    {
        constraintDaysProperty = stationProperty->getConstraintDays();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné : on considère qu'il n'y a pas de filtre par jour
    }


    // parcours des jours autorisant les slots
    bool isIncludedJour = constraintDaysProperty.size()==0;
    for(std::vector <WeekDays::WeekDays_ENUM>::iterator it = constraintDaysProperty.begin() ; !isIncludedJour && it != constraintDaysProperty.end() ; it++)
    {
        WeekDays::WeekDays_ENUM numDay = (*it);
        if (numDay == dayIndex)
            isIncludedJour = true;
    }
    if (isIncludedJour) //sinon, aucun slot ne sera conservé, donc pas d'extension possible de l'optimScope de la contrainte
    {
        // parcours des plages horaires autorisant le slot
        VectorOcpTimePeriod constraintRangesProperty;
        try
        {
            constraintRangesProperty = stationProperty->getConstraintRanges();
        }
        catch(OcpXMLInvalidField& e)
        {
            //pas de période renseignée, le slot a déjà été filtré par les zones de suivi satellite (cf OCP_CpModel::parseSlots )
        }
        VectorOCP_CpTimeSlot plagesHoraires;//collecter toutes les plages horaires à cheval 24/24
        for(VectorOcpTimePeriod::iterator it = constraintRangesProperty.begin() ; it != constraintRangesProperty.end() ; it++)
        {
            OcpTimePeriodPtr trackPeriod = (*it);
            OCP_CpTimeSlot offsetPlage(trackPeriod);

            // construction des plages horaires (en cas de chevauchement 24/24)
            OCP_CpConstraint::buildTimeAreas(plagesHoraires,optimScope->getBegin() , optimScope->getEnd(),offsetPlage.getBegin() , offsetPlage.getEnd(), true);
            // impacter la période étendue de la contrainte en fonction des plages horaires

        }
        // mise à jour du scope étendu de la contrainte si des plages horaires dépassent l'optimScope
        for(VectorOCP_CpTimeSlot::iterator it = plagesHoraires.begin() ; it != plagesHoraires.end() ; it++)
        {
            OCP_CpTimeSlotPtr plage = (*it);//plage horaire
            enlargeExtenDedScope (*plage.get());
        }

    }
}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint4::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);
    /*
     * Traitement de la non-consecutivité des slots encore possibles
     */
    if(!_consecutive)
    {
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format( _(" Instanciation de la contrainte %1$s de non consécutivité ")) % getName()  );
        createNonConsecutiveConstraint(optimScope);

    }
}

/**
* renseigner les propriétés d'un slot
* @param optimScope fenêtre de calcul
* @return vrai ssi le créneau candidat n'est pas à prendre en compte
*/
bool OCP_CpSatelliteConstraint4::notifyOneTrackingSlot(OCP_CpTimeSlotPtr optimScope, OCP_CpTrackingSlot* tmp)
{
    bool removed = false;
    OcpSatelliteSlotPtr slot = tmp->getSlotOrigin();
    std::map<OcpStationPtr, OcpStationPropertiesPtr>::iterator it = _properties.find(slot->getStation());
    PHRDate aDate(optimScope->getBegin());
    phr::int16u indexDay =  aDate.getDayInWeek();


    if (it == _properties.end() )//un passage sans station associée est à enlever
    {
        boost::format msg = boost::format( _("OCP_CpSatelliteConstraint4::instantiate exclut %1$s car sa station n'est pas dans la liste "))
        % tmp->getFullName().str();
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
        tmp->setToRemove();
        removed = true;
    }
    else
    {
        OcpStationPropertiesPtr stationProperty = (*it).second;
        removed = processOneSlot(optimScope,tmp , stationProperty,indexDay);
    }
    if(!removed)
    {
        /*
         * notification des débuts fins du slot sur la station pour construction des slots de maintenance
         */
        tmp->getStation()->addTimePoints(tmp);
        //possibleSlots.push_back(tmp);
    }

    return removed;

}

/**
* contrainte de non-consecutivité entre créneaux du satellite. Sur une boucle chronologique de visibilités
* STA1->STA2->STA3->...->STA1->
* on doit assurer que STA1 n'a pas pour suivant/précédent STA1
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint4::createNonConsecutiveConstraint(OCP_CpTimeSlotPtr optimScope)
{
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _(" instancieNonConsecutiveConstraint ")) );

    /*
     * mémorise les dernières stations de la veille : elles seront interdites pour le premier créneau candidat
     */
    StationMapPtr forbiddenStations = storeForbiddenStations(optimScope);

    IloEnv env = OCP_CpModel::getInstance()->getEnv();

    for(int i=0; i<getSatellite()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getSatellite()->getTrackingSlots()[i]);
        OcpSatelliteSlotPtr slot = tmp->getSlotOrigin();

        bool inMap = _properties.find(slot->getStation()) != _properties.end();//ce slot a-t-il une station concernée par la contrainte

        if(!tmp->toRemove()
                && !tmp->hasBookingOrder()
                && inMap)
        {
            addCandidateToDomain(tmp);
        }
        else
        {
            _complementarySlots.add(tmp);
        }
    }

    if(_domainSlots.getSize() == 0)
    {
        boost::format msg = boost::format( _("%1$s : pas de slot candidat donc pas de traitement solver") ) % getName() ;
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg.str());
        return;
    }

    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots, _domainSlots);
    // creating anySetVar (intersect) with all slots possible
    IloAnySetVar intersect(env, getSatellite()->getTrackingSlots());

    // intersect = TrackingSlotSetVar from the current getSatellite() INTER goodTrackingSlotsVar

    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT4_%1$s_%2$d") )
    % getSatellite()->getName() % getId() ;
    IloConstraint c1 =  IloEqIntersection(env, intersect, getSatellite()->getTrackingSlotSet(), goodTrackingSlotsVar);
    c1.setName(ctrName.str().c_str());


    // constraint on intersect...
    IloConstraint c2 = IloSatelliteConstraint4(env, intersect,forbiddenStations);

    setIloConstraint(c1 && c2);

    getSatellite()->addOcpConstraint(getIloConstraint());
}

/**
* renseigner et afficher les propriétés de la contrainte en parcourant les stationProperties
* @param stationProperties : propriétés renseignées sur la contrainte
*/
void OCP_CpSatelliteConstraint4::mapProperties(VectorOcpStationProperties& stationProperties)
{
    for(VectorOcpStationProperties::iterator it = stationProperties.begin() ; it != stationProperties.end() ; it++)
    {
        OcpStationPropertiesPtr stationProperty = (*it);
        OcpStationPtr aStation = stationProperty->getStation();
        _properties.insert(std::map<OcpStationPtr, OcpStationPropertiesPtr>::value_type(aStation,stationProperty));
        getSatellite()->storeOcpStationProperties(stationProperty);

        bool isRFFiltered = false;
        try
        {
            isRFFiltered = stationProperty->isRFFiltered();
        }
        catch(OcpXMLInvalidField& e)
        {
            //rien à faire si champs non renseigné
        }
        bool isPhysFiltered = false;
        try
        {
            isPhysFiltered = stationProperty->isPhysFiltered();
        }
        catch(OcpXMLInvalidField& e)
        {
            //rien à faire si champs non renseigné
        }
        float minElevation = 0;
        try
        {
            minElevation =  stationProperty->getMinElevation() ;
        }
        catch(OcpXMLInvalidField& e)
        {
            //rien à faire si champs non renseigné
        }
        uint16_t stationPriority = 0;
        try
        {
            stationPriority = stationProperty->getStationPriority() ;
        }
        catch(OcpXMLInvalidField& e)
        {
            //rien à faire si champs non renseigné
        }
        boost::format msg = boost::format( _(" Station %1$s has following properties : isRFFiltered %2$d,  isPhysFiltered %3$d, elevationMin %4$s, priority %5$d "))
        % aStation->getName() % isRFFiltered % isPhysFiltered % minElevation % stationPriority;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );


    }


}

/**
* indique si un passage satellite est à prioritiser par cette contrainte, et éventuellement flaggué impossible
* @param optimScope fenêtre de calcul
* @param slot : le passage satellite
* @param stationProperty : les propriétés d'une station
* @param dayIndex : index du jour courant du calcul
* @return vrai ssi le slot est conservé pour le calcul
*/
bool OCP_CpSatelliteConstraint4::processOneSlot(OCP_CpTimeSlotPtr optimScope,OCP_CpTrackingSlot* tmp, OcpStationPropertiesPtr stationProperty,phr::int16u dayIndex) const
{
    OcpSatelliteSlotPtr slot = tmp->getSlotOrigin();

    /*
     * lecture des informations sur les trous de visibilité RF et PHSYIQUE
     */
    bool slotIsPhysHoled = false;
    try
    {
        slotIsPhysHoled = slot->getVisibility()->isPhysHoled();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }

    bool slotIsRFHoled = false;
    try
    {
        slotIsRFHoled = slot->getVisibility()->isRFHoled();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }

    uint32_t minDurationHoleProperty = 0;
    try
    {
        minDurationHoleProperty = stationProperty->getMinDurationHoleRF();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }

    bool isRFFilteredProperty = false;
    try
    {
        isRFFilteredProperty = stationProperty->isRFFiltered();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }

    bool isPhysFilteredProperty = false;
    try
    {
        isPhysFilteredProperty = stationProperty->isPhysFiltered();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }

    /*
     * lecture de l'élévation minimale requise
     */
    float minElevationProperty = 0;
    try
    {
        minElevationProperty =  stationProperty->getMinElevation() ;
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
        boost::format msg = boost::format( _("OCP_CpSatelliteConstraint4::processOneSlot minElevationProperty non renseignée : on prendra élévation 0 par défaut "));
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
    }

    /*
     * lecture de la priorité du satellite pour cette station
     */
    uint16_t stationPriorityProperty = 0;
    try
    {
        stationPriorityProperty = stationProperty->getStationPriority() ;
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }

    /*
     * parcours des jours et plages horaires de la contrainte et vérification de l'inclusion pour le slot courant
     */
    std::vector <WeekDays::WeekDays_ENUM> constraintDaysProperty;
    try
    {
        constraintDaysProperty = stationProperty->getConstraintDays();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné : on considère qu'il n'y a pas de filtre par jour
    }

    int minDuration = 0;
    try
    {

        minDuration = stationProperty->getMinDuration();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }
    /*
     * filtrage par le site minimum de la station
     */
    bool validElevation = false;
    try
    {
        OcpAosLosPtr aoslos = slot->getAosLos(AOSTypes::ELEVATION , minElevationProperty);
        if (aoslos == NULL)
        {
            boost::format msg = boost::format( _("OCP_CpTrackingSlot::getDurationAbove '%1$s' unable to read duration above %2$f") )
            % OCP_CpTrackingSlot::getOriginFullName(slot)
            % minElevationProperty ;
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg);
        }
        else
        {
            OCP_CpDate aosBegin( (OCP_CpDate) aoslos->getStart().getSeconds());
            OCP_CpDate aosEnd( (OCP_CpDate) aoslos->getEnd().getSeconds() );
            if (aosEnd - aosBegin  >= minDuration)
            {
                validElevation = true;
                tmp->setBegin(aosBegin);
                tmp->setEnd(aosEnd);
            }
        }
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
    }
    catch(OCP_XRoot& e)
    {
        //erreur lancée si pas d'aos los
    }

    if (! validElevation)
    {
        boost::format msg = boost::format( _(" %1$s filtré car pas de durée au dessus du site minimum (%2$f) qui soit de durée supérieure à la durée minimum %3$i"))
        % tmp->getFullName().str() % minElevationProperty % minDuration;
        OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
        try
        {
            tmp->setToRemove();
        }
        catch(OCP_XRoot& err)
        {
            boost::format errorMsg = boost::format( _("La suppression du slot (%1$s) génère l'erreur : %2$s") ) % msg.str() % err.what();
            throw OCP_XRoot(errorMsg.str());


        }
    }
    else
    {

        /*
         * parcours des jours autorisant les slots : si pas de jours renseignés,le slot est autorisé, sinon, si le debut de la fenêtre de calcul
         * n'est pas dans la liste des jours autorisés, il faut exclure tout slot débutant dans cette fenêtre
         */
        bool isIncludedJour = constraintDaysProperty.size()==0;
        for(std::vector <WeekDays::WeekDays_ENUM>::iterator it = constraintDaysProperty.begin() ; !isIncludedJour && it != constraintDaysProperty.end() ; it++)
        {
            WeekDays::WeekDays_ENUM numDay = (*it);
            if (numDay == dayIndex)
                isIncludedJour = true;
        }
        if (!isIncludedJour && tmp->getSlotOrigin()->getStartEnd()->getStart().getDayInWeek() == dayIndex)
        {
            boost::format msg = boost::format( _(" %1$s filtré car jour non valide ")) % tmp->getFullName().str() ;
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
            tmp->setToRemove();
        }
        else
        {
            /*
             * parcours des plages horaires autorisant le slot
             */
            VectorOcpTimePeriod constraintRangesProperty;
            try
            {
                constraintRangesProperty = stationProperty->getConstraintRanges();
            }
            catch(OcpXMLInvalidField& e)
            {
                //pas de période renseignée, le slot a déjà été filtré par les zones de suivi satellite (cf OCP_CpModel::parseSlots )
            }
            bool trackable = constraintRangesProperty.size()==0;//si pas de plage renseignée, on considère ce slot valide

            VectorOCP_CpTimeSlot plagesHoraires;//collecter toutes les plages horaires à cheval 24/24
            for(VectorOcpTimePeriod::iterator it = constraintRangesProperty.begin() ; it != constraintRangesProperty.end() ; it++)
            {
                OcpTimePeriodPtr trackPeriod = (*it);
                OCP_CpTimeSlot offsetPlage(trackPeriod);

                /*
                 * construction des plages horaires (en cas de chevauchement 24/24)
                 */
                OCP_CpConstraint::buildTimeAreas(plagesHoraires,optimScope->getBegin() , optimScope->getEnd(),offsetPlage.getBegin() , offsetPlage.getEnd(), true);
                /*
                 * impacter la période étendue de la contrainte en fonction des plages horaires
                 */

            }
            for(VectorOCP_CpTimeSlot::iterator it = plagesHoraires.begin() ; !trackable && it != plagesHoraires.end() ; it++)
            {
                OCP_CpTimeSlotPtr plage = (*it);//plage horaire
                if(tmp->isInside(plage))
                    trackable = true;
            }

            if (!trackable)
            {
                boost::format msg = boost::format( _(" %1$s filtré car non inclus dans une plage horaire  ")) % tmp->getFullName().str() ;
                OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
                tmp->setToRemove();
            }
            else
            {
                /*
                 *lire sur la propriété station le niveau de priorité de cette station pour le satellite courant
                 */
                tmp->setPrioritySatellite(stationPriorityProperty);
                tmp->setPrioritySearch(stationPriorityProperty);


                /*
                 * filtrage par les filtre RF et PHYSIQUE
                 */
                float sumHoles = 0;
                /*
                 * trou RF
                 */
                if (isRFFilteredProperty && slotIsRFHoled && slot->getVisibility()->getRFHoleDuration() > minDurationHoleProperty)
                    sumHoles += slot->getVisibility()->getRFHoleDuration();

                /*
                 * trou Physique
                 */
                if( isPhysFilteredProperty &&  slotIsPhysHoled &&  slot->getVisibility()->getPhysHoleDuration() > minDurationHoleProperty )
                    sumHoles+= slot->getVisibility()->getPhysHoleDuration();

                tmp->setDurationHole((OCP_CpDate) sumHoles);

                if (tmp->getDuration() < minDuration)
                {
                    boost::format msg = boost::format( _(" %1$s filtré car la durée effective (%2$d secondes) tenant compte de %3$d secondes de trou de visibilité ne permet pas d'avoir une durée au dessus du siteminimum (%4$f) qui soit de durée supérieure à la durée minimum %5$d"))
                    % tmp->getFullName().str() % tmp->getDuration() % sumHoles % minElevationProperty % minDuration;
                    OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg );
                    tmp->setToRemove();
                }
            }
        }
    }


    return tmp->toRemove();
}


/**
 * mémorise les stations de la veille correspondant au(x) dernier()s passages
 * @param optimScope fenêtre de calcul coutante
 * @return la map des stations interdites
 */
StationMapPtr OCP_CpSatelliteConstraint4::storeForbiddenStations(OCP_CpTimeSlotPtr optimScope)
{

    static const std::string pseudoClazz = _Clazz + "::storeForbiddenStations";

    OcpContextPtr context = OcpXml::getInstance()->getContext();
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

      /*
     * collecter les slots de la veille
     */
    PHRDate day(optimScope->getBegin() );
    PHRDate dayBefore(optimScope->getBegin() - PHRDate::SECONDS_IN_A_DAY);

    VectorSatelliteSlot listSlot = context->getSatelliteSlotsByPeriod(dayBefore, day ,  getSatellite()->getOrigin() );
    VectorSatelliteSlot tmpSlots;//on collecte tous les candidats réservés la veille

    OCP_CpDate maxDate = -1;

    for(VectorSatelliteSlot::iterator iterSatSlot = listSlot.begin() ; iterSatSlot != listSlot.end() ; iterSatSlot++)
    {
        OcpSatelliteSlotPtr aSatSlot = *iterSatSlot;
        OCP_CpStationPtr station = cpModel->getStation( aSatSlot->getStation() );

        // filtrage par les stations entrées en paramètres
        if (station != NULL)
        {

            OCP_CpDate los = -1;
            bool okAosLos = false;//ce créneau a-t-il une élévation sur SAT4 compatible avec la durée de SAT4 et la surcharge locale éventuelle
            try
            {
                OCP_CpTimeSlot ocpSatSlot = OCP_CpTrackingSlot::getPastAosLos(aSatSlot);
                los = ocpSatSlot.getEnd();
                okAosLos = true;
            }
            catch(OCP_XRoot& err)
            {
                //rien à faire, okAosLos reste à false (message déjà loggé dans l'exception lancée depuis OCP_CpTrackingSlot::getPastAosLos)
            }
            bool isReserved = aSatSlot->getBookingState()  == SlotBookingStates::RESERVED
                    || aSatSlot->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
            bool hasBookingOrder = aSatSlot->hasBeenBookedByRequest();
            /*
             * les slots candidats pour les jours précédents sont ceux reservés hors bookingOrder  mais
             * qui ne font pas partie des slots candidats sur le jour courant (possibilité de plage à cheval sur le
             * dernier jour)
             * Par ailleurs, le test sur la durée est fait selon l'hypothèse que les slots prépositionnés dans le passé
             * respectent déjà la durée min et l'élévation requises par SAT4
             */


            if (isReserved
                    && !hasBookingOrder
                    && okAosLos
                    && !getSatellite()->containsTrackingSlots(aSatSlot))
            {
                /*
                 * mise à jour de la date de fin maximale des créneaux candidats la veille
                 */
                if(los>maxDate)
                {
                    maxDate = los;
                    tmpSlots.push_back(aSatSlot);
                }
            }
        }
    }

    /*
     * seconde passe : mémoriser toutes les stations correspondant aux slots terminant à maxDate
     */
    StationMapPtr forbiddenStations( new StationMap() );
    boost::format msg = boost::format( _(" %1$s stations interdites pour le premier créneau car dernières stations placées la veille  ")) % pseudoClazz  ;

    for(VectorSatelliteSlot::iterator iterSatSlot = tmpSlots.begin() ; iterSatSlot != tmpSlots.end() ; iterSatSlot++)
    {
        OcpSatelliteSlotPtr aSatSlot = *iterSatSlot;
        OCP_CpTimeSlot ocpSatSlot = OCP_CpTrackingSlot::getPastAosLos(aSatSlot);
        if (ocpSatSlot.getEnd() == maxDate)
        {
            OcpStationPtr aStation = aSatSlot->getStation();
            try
            {
                OCP_CpStationPtr station = cpModel->getStation(aStation);
                forbiddenStations->insert( StationMap::value_type(aStation,station) );
                msg = boost::format( _(" %1$s %2$s  ")) % msg.str() % aStation->getName() ;
                
            }
            catch(OCP_XRoot& e)
            {
                //si la station n'existe pas dans la fenêtre courante, elle n'influencera pas la contrainte
            }

        }
    }
    
    OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, boost::format( _("%1$d %2$s  ")) % forbiddenStations->size() % msg.str() );

    return forbiddenStations;

}
