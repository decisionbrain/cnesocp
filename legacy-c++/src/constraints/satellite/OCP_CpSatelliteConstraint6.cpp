/*
 * $Id: OCP_CpSatelliteConstraint6.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint6.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpSatelliteConstraint6.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpSatellite.h"
#include "OCP_CpExternWrapper.h"

using namespace std;


/**
 * Constructeur à partir de la contrainte métier
 * @param period période d'instanciation de la contrainte
 * @param ctrSat contrainte métier origine
 * @param satellite satellite associé à la contrainte
 * @return
 */
OCP_CpSatelliteConstraint6::OCP_CpSatelliteConstraint6(OCP_CpTimeSlotPtr period, OcpConstraintSatType6Ptr ctrSat,OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(period, ctrSat,satellite)
{

    /*
    * Lecture de la fréquence associée à la contrainte
    */
    _frequencyBand = ctrSat->getFrequencyBand();

    /*
     * lecture des deux stations
     */
    OcpStationPtr aSta1 = ctrSat->getFirstStation();
    OcpStationPtr aSta2 = ctrSat->getSecondStation();

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    _station1 = cpModel->getStation(aSta1);
    _station2 = cpModel->getStation(aSta2);

    if(_station1 ==NULL || _station2 == NULL)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %3$s : OCP_CpSatelliteConstraint6 Station non reconnue %1$s ou %2$s  ") ) %
                aSta1->getName() % aSta2->getName() % satellite->getName();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
        throw OCP_XRoot(msg.str());
    }
    OCP_CpSatelliteBaseConstraint::onFrequencyError(_origin,aSta1 , satellite,_frequencyBand);
    OCP_CpSatelliteBaseConstraint::onFrequencyError(_origin,aSta2 ,satellite, _frequencyBand);

    /*
     * durée convertie de minutes à secondes
     */
    _dureeRecouv = 60 * ctrSat->getMinCoveryDuration();


    /*
     * nommage de la contrainte
     */
    boost::format prefix;
    switch(_frequencyBand)
    {
        case FrequencyBands::S : prefix = boost::format ( _("S") );break;
        case FrequencyBands::X : prefix = boost::format ( _("X") );break;
        case FrequencyBands::SX : prefix = boost::format ( _("SX") );break;
        default : prefix = boost::format ( _("?") );break;
    }
    boost::format msg = boost::format( _("SatelliteConstraint6-satellite %5$s %1$s-%2$s recouvrement %3$i secondes %4$s ") )
            %  aSta1->getName() % aSta2->getName() %  _dureeRecouv % prefix.str() % satellite->getName();

    setName(msg.str().c_str());
    msg = boost::format( _("Creating %1$s") ) % msg.str() ;
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, msg);

}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint6::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);

    /*
     * Aller chercher toutes les visibilités liées à ces deux stations
     */
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();

    int nbCandidatesStation1 = 0;//nombre de candidats pour station1
    int nbCandidatesStation2 = 0;//nombre de candidats pour station2
    for(int i=0; i<getSatellite()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getSatellite()->getTrackingSlots()[i]);
        if((tmp->getStation() == _station1 || tmp->getStation() == _station2)
                && !tmp->toRemove()
                && !tmp->hasBookingOrder())
        {
            addCandidateToDomain(tmp);
            if (tmp->getStation() == _station1)
                nbCandidatesStation1++;
            else
                nbCandidatesStation2++;
        }
        else
        {
            _complementarySlots.add(tmp);
        }
    }

    if(nbCandidatesStation1==0 || nbCandidatesStation2==0)
    {
        boost::format msg = boost::format( _("Contrainte non respectée pour le satellite %3$s : OCP_CpSatelliteConstraint6 %2$s Nombre de passages candidats %1$i insuffisants  ") ) %
                _domainSlots.getSize() % getName() % getSatellite()->getName() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
        throw OCP_XRoot(msg.str());
    }

    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, _domainSlots, _domainSlots);

    boost::format intersectName = boost::format( _("MyIntersectSAT6-%1$s--%2$s_%3$d ") ) % getSatellite()->getName().c_str() %  getId();
    
    IloAnySetVar intersect(env, getSatellite()->getTrackingSlots(),intersectName.str().c_str());
    // intersect = TrackingSlotSetVar from the current getSatellite() INTER goodTrackingSlotsVar

    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT6_%1$s_%2$d") ) % getSatellite()->getName() % getId() ;
    IloConstraint c1 =  IloEqIntersection(env, intersect, getSatellite()->getTrackingSlotSet(), goodTrackingSlotsVar);
    c1.setName(ctrName.str().c_str());


    IloConstraint c2 = IloSatelliteConstraint6(env,_station1,_station2,_dureeRecouv,intersect);

    setIloConstraint(c1 && c2);
    getSatellite()->addOcpConstraint(getIloConstraint());

}
