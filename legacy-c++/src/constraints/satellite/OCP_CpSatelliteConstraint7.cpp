/*
 * $Id: OCP_CpSatelliteConstraint7.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint7.cpp
 *
 */
#include <string>

#include "constraints/OCP_CpSatelliteConstraint7.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpSatellite.h"
#include "OCP_CpExternWrapper.h"


using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpSatelliteConstraint7);

/**
 * Constructeur à partir de la contrainte métier
 * @param optimScope période d'instanciation de la contrainte
 * @param ctrSat contrainte métier origine
 * @param satellite satellite associé à la contrainte
 * @return
 */
OCP_CpSatelliteConstraint7::OCP_CpSatelliteConstraint7(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType7Ptr ctrSat,OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(optimScope,ctrSat, satellite)
{
    // Lecture de la fréquence associée à la contrainte
    _frequencyBand = ctrSat->getFrequencyBand();

    // traitement des plages horaires
    OcpTimePeriodPtr aPeriod;//périodes HH:MM
    try
    {
        aPeriod = ctrSat->getConstraintRanges();
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format errMsg = boost::format ( _("Contrainte non respectée pour le satellite %1$s : SatelliteConstraint7 contrainte en erreur car la plage horaire n'est pas renseignée") )
                % satellite->getName();
        OcpLogManager::getInstance()->info(ocp::solver::loggerInst, errMsg);
        throw OCP_XRoot(errMsg.str());
    }

    boost::format plageStr = boost::format(_("Plage horaire [%1$s;%2$s] (isDateTime? %5$d;%6$d) ou en sec '%3$d' - '%4$d'"))
            % aPeriod->getStart().getStringValue() % aPeriod->getEnd().getStringValue() % aPeriod->getStart().getSeconds() % aPeriod->getEnd().getSeconds() % aPeriod->getStart().isDateTime() % aPeriod->getEnd().isDateTime() ;

    OCP_CpTimeSlot ts( (OCP_CpDate) aPeriod->getStart().getSeconds() , (OCP_CpDate) aPeriod->getEnd().getSeconds());

    // construit la ou les plages horaires en secondes absolues
    OCP_CpSatelliteBaseConstraint::buildTimeAreas(_plagesHoraires , optimScope->getBegin() , optimScope->getEnd() , ts.getBegin(), ts.getEnd());

    // impacter la période étendue de la contrainte en fonction des plages horaires
    for(VectorOCP_CpTimeSlot::iterator it = _plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
    {
        OCP_CpTimeSlotPtr plage = (*it);//plage horaire
        enlargeExtenDedScope(*plage.get());
    }



    // Nombre de supports demandés : on veut un minimum de N supports simultanés sur la plage horaire
    _nbSupports = ctrSat->getNbSupports();

    /*
     * notification de la zone de split
     */
    addSplitZone();

    // nommage de la contrainte et affichage
     boost::format prefix;
     switch(_frequencyBand)
     {
         case FrequencyBands::S : prefix = boost::format ( _("S") );break;
         case FrequencyBands::X : prefix = boost::format ( _("X") );break;
         case FrequencyBands::SX : prefix = boost::format ( _("SX") );break;
         default : prefix = boost::format ( _("?") );break;
     }

     boost::format msg = boost::format( _("SatelliteConstraint7-%1$s pour satellite %2$s : %3$d supports simultanés exactement requis sur %4$s") )
             %  prefix  % satellite->getName() %  _nbSupports % plageStr.str();

     setName(msg.str().c_str());
     OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());

}

/**
 * split de la zone de support
 */
void OCP_CpSatelliteConstraint7::addSplitZone()
{
    static const std::string pseudoClazz = _Clazz + "::addSplitZone";
    boost::format msg = boost::format( _("Notification de la zone de split des créneaux ") ) ;
    OcpLogManager::getInstance()->debug(pseudoClazz, msg.str());


    for(VectorOCP_CpTimeSlot::iterator it=_plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
    {
        OCP_CpTimeSlotPtr aPeriod = (*it);
        OCP_CpTrackingSlotPtr split( new OCP_CpTrackingSlot(getSatellite(), OCP_CpStationPtr(),  OcpSatelliteSlotPtr()));
        split->setBegin( aPeriod->getBegin());
        split->setEnd( aPeriod->getEnd());
        getSatellite()->addSplitZone(split);
    }
}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint7::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

    // parcours des plages horaires : une instance de contrainte par plage horaire
    IloAnd ctrAggregate = IloAnd(cpModel->getEnv());
    int cpt=0;
    for(VectorOCP_CpTimeSlot::iterator it=_plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
    {
        OCP_CpTimeSlotPtr aPeriod = (*it);
        createOneConstraint(aPeriod ,ctrAggregate,cpt++);
    }

    setIloConstraint(ctrAggregate);
    getSatellite()->addOcpConstraint(getIloConstraint());
}

/**
 * creation d'une contrainte ILOG SOLVER sur une plage horaire
 * @param plageHoraire plage horaire demandée
* @param ctrAggregate contrainte aggrégeant la contrainte créée sur cette plage horaire
* @param cpt compteur pour nommage de variable ensembliste
*/
void OCP_CpSatelliteConstraint7::createOneConstraint(OCP_CpTimeSlotPtr plageHoraire, IloAnd ctrAggregate, int cpt)
{
    static const std::string pseudoClazz = _Clazz + "::createOneConstraint";

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();
    IloAnyArray oneConstraintSlots(env);

    for(int i=0; i<getSatellite()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getSatellite()->getTrackingSlots()[i]);

        if (!tmp->toRemove()
                && !tmp->hasBookingOrder()
                && tmp->isInside(plageHoraire)
                && OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand , tmp->getStation()->getStationFrequencyBand())
                )
        {

            addCandidateToDomain(tmp);
            oneConstraintSlots.add(tmp);
        }
        else
        {
            _complementarySlots.add(tmp);
        }

    }

    if(oneConstraintSlots.getSize() < _nbSupports)
    {
        boost::format msg =  boost::format(_("La contrainte %1$s a au total %2$i créneaux candidats sur la plage %4$s mais le nombre de supports simultanés est %3$d"))
        % getName() % oneConstraintSlots.getSize() % _nbSupports % plageHoraire->getFullName();
        OcpLogManager::getInstance()->error(pseudoClazz, msg  );

        throw OCP_XRoot(msg.str());
    }
    else
    {
        // creating anySetVar with all good slots required and all good slots possible
        IloAnySetVar goodTrackingSlotsVar(env, oneConstraintSlots, oneConstraintSlots);
        // creating anySetVar (intersect) with all slots possible

        boost::format intersectName = boost::format( _("MyIntersectSAT7-%1$s--%2$s_%3$d ") ) % getSatellite()->getName().c_str() %  getId() % cpt;
        
        IloAnySetVar intersect(env, oneConstraintSlots,intersectName.str().c_str() );

        // intersect = TrackingSlotSetVar du satellite list INTER goodTrackingSlotsVar

        boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT7_%1$s_%2$d") )
        % getSatellite()->getName() % getId() ;
        IloConstraint c1 =  IloEqIntersection(env, intersect, getSatellite()->getTrackingSlotSet(), goodTrackingSlotsVar);
        c1.setName(ctrName.str().c_str());

        // constraint on intersect...
        IloConstraint c2 = IloSatelliteConstraint7_v4(env, _nbSupports, intersect , plageHoraire);

        //setIloConstraint(c1 && c2); non, on va charger plusieurs contraintes, une par plage horaire
        ctrAggregate.add(c1 && c2);
    }






}
