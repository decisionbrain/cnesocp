/*
 * $Id: OCP_CpSatelliteConstraint3.cpp 943 2010-12-21 13:12:51Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint3.cpp
 *
 */
#include <string>
#include <list>
#include <algorithm>

#include "constraints/OCP_CpSatelliteConstraint3.h"
#include "context/OCP_CpModel.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "resources/OCP_CpSatellite.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpExternWrapper.h"



using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpSatelliteConstraint3);

/* GLOBAL IDEA FOR THIS CONSTRAINT:

	save the constraintg by setIloConstraint
	take care, the constraint is not stored in the model!
*/	


/**
 * Constructeur à partir de la contrainte métier
 * @param optimScope période d'instanciation de la contrainte
 * @param ctrSat contrainte métier origine
 * @param satellite satellite associé à la contrainte
 * @return
 */
OCP_CpSatelliteConstraint3::OCP_CpSatelliteConstraint3(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType3Ptr ctrSat,OCP_CpSatellitePtr satellite)
: OCP_CpSatelliteBaseConstraint(optimScope, ctrSat,satellite)
{
    _frequencyBand = ctrSat->getFrequencyBand();
    _typeEcart = ctrSat->getDurationType() ;
    _dureeCumulee = 60 * ctrSat->getCombinedDuration();
    OCP_CpDate offsetBegin = 0;//relatif
    OCP_CpDate offsetEnd = 0;//relatif
    _durationMin = 0;//initialisé à la lecture des slots candidats
    _durationMax = -1;//initialisé à la lecture des slots candidats



    try
    {
        offsetBegin = (OCP_CpDate) ctrSat->getConstraintRange()->getStart().getSeconds();
        offsetEnd = (OCP_CpDate) ctrSat->getConstraintRange()->getEnd().getSeconds();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("INFOS1: offsetBegin %1$d, offsetEnd %2$d "))
        % offsetBegin % offsetEnd);
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si le champs n'est pas renseigné
    }

    // construction des plages horaires (en cas de chevauchement 24/24)
    OCP_CpConstraint::buildTimeAreas(_plagesHoraires,optimScope->getBegin() , optimScope->getEnd(),offsetBegin , offsetEnd);

    // impacter la période étendue de la contrainte en fonction des plages horaires
    for(VectorOCP_CpTimeSlot::iterator it = _plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
    {
        OCP_CpTimeSlotPtr plage = (*it);//plage horaire
        enlargeExtenDedScope( *plage.get() );
    }

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    std::vector<OcpStationPtr> stations = ctrSat->getStations();
    boost::format prefixStations = boost::format(_(" Stations "));
    for(std::vector<OcpStationPtr>::iterator it = stations.begin(); it != stations.end() ; it++)
    {
        OcpStationPtr aStation = *it;
        OCP_CpSatelliteBaseConstraint::onFrequencyError(_origin,aStation,satellite,_frequencyBand);
        OCP_CpStationPtr station = cpModel->getStation(aStation);
        _stationsMap.insert(StationMap::value_type(aStation , station ));
        prefixStations = boost::format(_(" %1$s %2$s ")) % prefixStations.str() % aStation->getName() ;
    }

    // affichage et nommage

    boost::format prefixFreq;
    switch(_frequencyBand)
    {
        case FrequencyBands::S : prefixFreq = boost::format ( _("S") );break;
        case FrequencyBands::X : prefixFreq = boost::format ( _("X") );break;
        case FrequencyBands::SX : prefixFreq = boost::format ( _("SX") );break;
        default : prefixFreq = boost::format ( _("?") );break;
    }
    boost::format prefixTypeEcart;
    switch(_typeEcart)
    {
        case SatConstraintTypes::MAX : prefixTypeEcart = boost::format ( _("MAX") );break;
        case SatConstraintTypes::MIN : prefixTypeEcart = boost::format ( _("MIN") );break;
        default : prefixTypeEcart = boost::format ( _("?") );break;
    }

    PHRDate cumulPHR(_dureeCumulee , PHRDate::_TimeFormat);
    PHRDate debutPeriodePHR(offsetBegin , PHRDate::_TimeFormat);
    PHRDate finPeriodePHR(offsetEnd , PHRDate::_TimeFormat);

    boost::format msg = boost::format( _("SatelliteConstraint3-%1$s- %2$s durée cumulée %3$s (%4$d  secondes) %5$s  periode [%6$s %7$s] %8$s") )
            %  satellite->getName().c_str() % prefixFreq.str() % cumulPHR.getStringValue() % _dureeCumulee  % prefixTypeEcart
            % debutPeriodePHR.getStringValue() % finPeriodePHR.getStringValue()
            % prefixStations;
    setName(msg.str().c_str());

    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format(_("\tCreating %1$s")) % msg.str());
    OcpLogManager::getInstance()->info(ocp::solver::loggerInst, boost::format( _("durée cumulée %1$s ") ) % cumulPHR.getStringValue() );

}

/**
* création de la contrainte ILOG SOLVER
* @param optimScope fenêtre de calcul courante
*/
void OCP_CpSatelliteConstraint3::instantiate(OCP_CpTimeSlotPtr optimScope)
{
    SUPPRESS_UNUSED_PARAMETER_WARNING(optimScope);

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

     // parcours des plages horaires : une instance de contrainte par plage horaire
     IloAnd ctrAggregate = IloAnd(cpModel->getEnv());
     for(VectorOCP_CpTimeSlot::iterator it=_plagesHoraires.begin() ; it != _plagesHoraires.end() ; it++)
     {
         OCP_CpTimeSlotPtr aPeriod = (*it);
         createOneConstraint(aPeriod , ctrAggregate);
     }

     setIloConstraint(ctrAggregate);
     getSatellite()->addOcpConstraint(getIloConstraint());
}

/**
* création d'une instance sur "la" plage horaire de la contrainte
* @param plageHoraire la plage horaire, potentiellement à cheval sur la fenêtre de calcul
* @param ctrAggregate contrainte aggrégeant les instances sur deux plages horaires à cheval
*/
void OCP_CpSatelliteConstraint3::createOneConstraint(OCP_CpTimeSlotPtr plageHoraire, IloAnd ctrAggregate)
{
    static const std::string pseudoClazz = _Clazz + "::createOneConstraint";

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();
    IloAnyArray oneConstraintSlots(env);
    // filtering good slots from all tracking slots

    /*
     * Localement à cette contrainte, chaque intervalle a une durée associée égale à l'intersection du passage au-dessus du site cible
     * et de l'intervalle de validité en entrée (période de calcul intersection avec la période de validité de la contrainte)
     */
    SpecialDurationPtr durationsAboveSite( new SpecialDuration());//TODO supprimer s'il s'avère qu'on n'aura jamais à tenir compte de créneau à cheval sur la plage
    OCP_CpDate maximorum = 0;
    for(int i=0; i<getSatellite()->getTrackingSlots().getSize(); i++)
    {
        OCP_CpTrackingSlot* tmp = (OCP_CpTrackingSlot*)(getSatellite()->getTrackingSlots()[i]);
        if(!tmp->toRemove() && !tmp->hasBookingOrder())
        {
            OcpSatelliteSlotPtr slot = tmp->getSlotOrigin();


            // si pas de station renseignée, on teste quand même la fréquence du slot
            bool inMap = (_stationsMap.size()==0 &&
                    OCP_CpSatelliteBaseConstraint::frequencyCompatible(_frequencyBand , slot->getStation()->getFrequencyBand())
                    )
                    || _stationsMap.find(slot->getStation()) != _stationsMap.end();

            if(inMap
                    &&  tmp->isInside(plageHoraire)

            )

            {
                addCandidateToDomain(tmp);
                oneConstraintSlots.add(tmp);
                // dans cette contrainte , on associe à chaque intervalle sa durée au dessus du site
                durationsAboveSite->insert(SpecialDuration::value_type(tmp , tmp->getDuration() ));
                maximorum+=tmp->getDuration();//durationAbove;
            }
            else
            {
                _complementarySlots.add(tmp);
            }

        }
    }
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("Nb good slots = %1$d")) % oneConstraintSlots.getSize());
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("Max maximorum = %1$d")) % maximorum);

    switch(_typeEcart)
     {
         case SatConstraintTypes::MAX : _durationMin=0 ; _durationMax = _dureeCumulee ; break;
         case SatConstraintTypes::MIN : _durationMin=_dureeCumulee ; _durationMax = maximorum  ;break;
         default :
             {
                 boost::format msg = boost::format(_("Contrainte non respectée pour le satellite %1$s : type non reconnu sur le type de durée"))
                         % getSatellite()->getName();

                 throw OCP_XRoot(msg.str());
                 break;
             }
     }

    if (oneConstraintSlots.getSize() == 0)
    {
        if(_durationMin>0)
        {
            boost::format msg = boost::format(_("Contrainte non respectée pour le satellite %2$s : contrainte OCP_CpSatelliteConstraint3 %1$s  en erreur car aucun candidat potentiel parmi les passages "))
                    % getName() % getSatellite()->getName();
            OcpLogManager::getInstance()->error(pseudoClazz, msg);
            throw OCP_XRoot(msg.str());
        }
        else
        {
            boost::format msg = boost::format(_("La contrainte %1$s  est toujours vraie (aucun candidat potentiel parmi les passages et _durationMin à zéro ")) % getName();
            OcpLogManager::getInstance()->debug(pseudoClazz, msg);
            return;
        }
    }

    if(_durationMin>maximorum)

    {
        boost::format msg = boost::format(_("Contrainte non respectée OCP_CpSatelliteConstraint3 %1$s  en erreur car la somme des durées des candidats potentiels %2$d est inférieure à la durée min requise %3$d"))
                            % getName() % maximorum % _durationMin;
        OcpLogManager::getInstance()->error(pseudoClazz, msg);
        throw OCP_XRoot(msg.str());
    }


    // creating anySetVar with all good slots required and all good slots possible
    IloAnySetVar goodTrackingSlotsVar(env, oneConstraintSlots,oneConstraintSlots);
    // creating anySetVar (intersect) with all slots possible
    IloAnySetVar intersect(env, getSatellite()->getTrackingSlots());

    // intersect = TrackingSlotSetVar from the current satellite INTER goodTrackingSlotsVar

    boost::format ctrName = boost::format( _("MyIloEqIntersectionSAT3_%1$s_%2$d") )
    % getSatellite()->getName() % getId() ;
    IloConstraint c1 =  IloEqIntersection(env, intersect, getSatellite()->getTrackingSlotSet(), goodTrackingSlotsVar);
    c1.setName(ctrName.str().c_str());

    // constraint on intersect...
    IloConstraint c2 = IloSatelliteConstraint3(env,  _durationMin, _durationMax, intersect, maximorum,durationsAboveSite);

    cpModel->addSat3IloConstraint(c2,oneConstraintSlots);

    ctrAggregate.add(c1 && c2);


}


