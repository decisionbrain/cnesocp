/*
 * $Id: OCP_CpTrackingSlot.cpp 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpTrackingSlot.cpp
 *
 */

#include "OCP_CpDate.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_Solver.h"
#include "context/OCP_CpModel.h"
#include "planif/OCP_CpCstIntTimetable.h"

using namespace std;
using namespace ocp::commons;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpTrackingSlot);

/**
 *
 * @param satellite
 * @param station
 * @param debut
 * @param fin
 * @param slot_ origine dans le modèle métier
 * @return
*/
OCP_CpTrackingSlot::OCP_CpTrackingSlot(OCP_CpSatellitePtr s1, OCP_CpStationPtr s2,
        OcpSatelliteSlotPtr slot_)
: OCP_CpTimeSlot(-1,-1),
  _fatherSplit(NULL),
  _processedBuddies(false),
  _isLongestInSat1(false),
  _prioritySatellite(-1),
  _priorityStation(-1),
  _searchPriority(-1)

  {

    _satellite = s1;
    _station = s2;
    _slot = slot_;


    if (slot_ != NULL)
    {
        /*
         * renseigner l'amplitude du créneau
         */
        OCP_CpDate aBegin( (OCP_CpDate) slot_->getStartEnd()->getStart().getSeconds() );
        OCP_CpDate aEnd( (OCP_CpDate) slot_->getStartEnd()->getEnd().getSeconds() );
        _amplitude = OCP_CpTimeSlotPtr (new OCP_CpTimeSlot(aBegin, aEnd));

        /*
         * renseigner l'aos los de référence qui donnera ultérieurement la durée effective du slot
         */
        //_aosLosRef = OCP_CpTrackingSlot::getAosLosRef(slot_);

    }
    _durationHole = 0;
    _toRemove = false;//positionné ultérieurement par SAT4 (ou heures d'ouverture de la station correspondante)
    _reserved = slot_!=NULL && isOriginReserved();//slot_ peut etre NULL pour des split zones (intervalles artificiels sans satellite slot origine)
    _hasBookingOrder = false;
    try
    {
        _hasBookingOrder = slot_!=NULL && slot_->hasBeenBookedByRequest();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire quand les champs ne sont pas lus : on se contentera des valeurs par défaut
    }
    if (slot_!=NULL && _hasBookingOrder && ! _reserved)
    {
        PHRDate pBegin = slot_->getStartEnd()->getStart();
        PHRDate pEnd = slot_->getStartEnd()->getEnd();
        boost::format msg = boost::format( _("Satellite slot :  '%1$s' on station '%2$s' from '%3$s' to '%4$s' a été vu 'bookedByRequest' sans être reservé") )
                   % s1->getName()   % s2->getName() % pBegin.getStringValue() % pEnd.getStringValue() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
        throw OCP_XRoot(msg.str());
    }
}

/**
 * statut reservé du slot orgine
 * @param slot_ slot origine
 * @return vrai ssi le slot origine est reservé
 */
bool OCP_CpTrackingSlot::isOriginReserved() const
{
    return _slot->getBookingState()  == SlotBookingStates::RESERVED
            || _slot->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
}

/**
* récupère sur un passage reservé dans le passé l'aoslos à partir des informations courantes renseignées sur la contrainte
* SAT4
* @param slot_ slot origine
* @return l'aos los associé au slot ayant une durée supérieure à la durée min renseignée par SAT4 au dessus de l'élévation
* renseignée par SAT4
*/
OCP_CpTimeSlot OCP_CpTrackingSlot::getPastAosLos(OcpSatelliteSlotPtr slot_)
{
    static const std::string pseudoClazz = _Clazz + "::getPastAosLos";

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    OcpSatellitePtr aSatellite = slot_->getSatellite();

    OCP_CpSatellitePtr satellite = cpModel->getSatellite(aSatellite);
    float minElevationProperty = 0;
    int minDuration = 0;

    satellite->getSat4ElevationAndDuration(slot_->getStation() ,minElevationProperty , minDuration);


    OcpAosLosPtr aoslos = slot_->getAosLos(AOSTypes::ELEVATION , minElevationProperty);
    if (aoslos == NULL)
    {
        boost::format msg = boost::format( _(" '%1$s' ce créneau n'a pas de durée au-dessus de l'élévation %2$f") )
        % OCP_CpTrackingSlot::getOriginFullName(slot_)
        % minElevationProperty ;
        OcpLogManager::getInstance()->warning(pseudoClazz, msg);
        throw OCP_XRoot( msg.str() );
    }
    else
    {
        OCP_CpDate aosBegin( (OCP_CpDate) aoslos->getStart().getSeconds());
        OCP_CpDate aosEnd( (OCP_CpDate) aoslos->getEnd().getSeconds() );
        if (aosEnd - aosBegin  < minDuration)
        {
            boost::format msg = boost::format( _(" '%1$s' a une durée au-dessus de %2$f non compatible avec la durée minimum requise %3$f  ") )
            % OCP_CpTrackingSlot::getOriginFullName(slot_)
            % minElevationProperty
            % minDuration
            ;

            OcpLogManager::getInstance()->warning(pseudoClazz, msg);
            throw OCP_XRoot( msg.str() );
        }
        else
            return OCP_CpTimeSlot(aosBegin , aosEnd);
    }
}


/**
* redéfinition de l'amplitude pour tenir compte des durées des trous si positionnés
* @return l'amplitude de l'intervalle éventuellement amputée de la somme des durées des trous
*/
OCP_CpDate OCP_CpTrackingSlot::getDuration() const //TODO faut-il interdire cette méthode et toujours demander avec élévation ?
{
    return getEnd() - getBegin() - _durationHole;
}

/**
* chaîne de caractères pour le slot
* @return une chaîne de caractères représentant le slot
*/
string OCP_CpTrackingSlot::getName() const
{
    return   "Tracking -" + _station->getName()+ "-" + _satellite->getName();
}

/**
 * affichage du slot avec info satellite , station, debut, fin
 * @return affichage complet
 */
boost::format OCP_CpTrackingSlot::getFullName() const
{
    boost::format msg = OCP_CpTrackingSlot::getOriginFullName(getSlotOrigin());
    boost::format aoslos = boost::format( _("aoslos ") );
    try
    {
        PHRDate aos( _begin, PHRDate::_TimeFormat);
        PHRDate los( _end, PHRDate::_TimeFormat);
        aoslos = boost::format( _("%1$s aos %2$s los %3$s (%4$d %5$d)") )
        % aoslos.str()
        % aos.getStringValue()
        % los.getStringValue()
        % _begin
        % _end
        ;
    }
    catch(OCP_XRoot& err)
    {
        //erreur lancée si begin end non renseigné
    }

    /*boost::format full = boost::format( _("R%1$d (internal id %2$d) prios[%3$d %4$d %5$d] %6$s %7$s") )
        % _reserved
        % getId()
        % _prioritySatellite
        % _priorityStation
        % _searchPriority
        % msg.str()
        % aoslos.str()
        ;*/

    boost::format full = boost::format( _("R%1$d  prios[%2$d %3$d %4$d] %5$s %6$s") )
            % _reserved
            % _prioritySatellite
            % _priorityStation
            % _searchPriority
            % msg.str()
            % aoslos.str()
            ;

    return full;
}

/**
* raccourci pour afficher le nom d'un satellite slot
* @param aSatSlot satellite slot
* @return le nom complet du passage satellite
*/
boost::format OCP_CpTrackingSlot::getOriginFullName(OcpSatelliteSlotPtr aSatSlot)
{
    boost::format prefix;
    switch(aSatSlot->getStation()->getFrequencyBand())
    {
        case FrequencyBands::S:
            prefix = boost::format ( _("S") );
            break;
        case FrequencyBands::X:
            prefix = boost::format ( _("X") );
            break;
        case FrequencyBands::SX:
            prefix = boost::format ( _("SX") );
            break;
        default:
            prefix = boost::format ( _("?") );
            break;
    }

    boost::format msg = boost::format( _("SlotId %1$i '%2$s' on station '%3$s' %4$s  from '%5$s' to '%6$s' ") )
            % aSatSlot->getSlotID()
            % aSatSlot->getSatellite()->getName()
            % aSatSlot->getStation()->getName()
            % prefix.str()
            % aSatSlot->getStartEnd()->getStart().getStringValue()
            % aSatSlot->getStartEnd()->getEnd().getStringValue()
            ;
    return msg;
}

/**
 * predicat de comparaison par priorité des slots satellites : 1 est le plus prioritaire, 2 moins prioritaire... 0 sans priorité
 * @param slot1 : premier slot à comparer
 * @param slot2 : second slot à comparer
 * @return vrai ssi slot1 est plus prioritaire que slot2
 */
bool OCP_CpTrackingSlot::prioritySortPredicate(OCP_CpTrackingSlot* slot1, OCP_CpTrackingSlot* slot2)
{
    int prio1 = slot1->getSearchPriority();
    int prio2 = slot2->getSearchPriority();

    if (prio1 == 0)
    {
        return false;
    }
    else if (prio2 == 0)
    {
        return true;
    }
    else
    {
        return prio1 < prio2;
    }
}


/**
 * traitement du créneau satellite pour la sauvegarde : renseignemenent des champs TYPE_CRENEAU, NIVEAU_CRENEAU
 */
void OCP_CpTrackingSlot::postOptim()
{
    OCP_CpConstraintPtr ctr;

    for(VectorOCP_CpConstraint::iterator it = _cpConstraints.begin() ; it != _cpConstraints.end() ; ++it )
    {
        ctr = (*it);
        ctr->postOptim(this);
    }

    /*
     * flagguer le slot à RESERVED à moins qu'il ne soit déjà RESERVED_LOCKED
     */
    if (_slot->getBookingState() != SlotBookingStates::RESERVED_LOCKED)
        _slot->setBookingState(SlotBookingStates::RESERVED);

    /*
     * mise à jour du champs commentaire
     */
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    _slot->setComment(cpModel->getComment());
}



/**
* renseigne le père d'un slot splitté
* @param fatherSlot père du slot splitté
*/
void OCP_CpTrackingSlot::setFatherSplit(OCP_CpTrackingSlot* fatherSlot)
{
    _fatherSplit = fatherSlot;
    _fatherSplit->addSonSplit(this);
}


/**
 * algorithme de mise en relation des slots buddies. TODO à revoir pour le nouveau traitement des fusions de slots splittés
 */
void OCP_CpTrackingSlot::processSlotBuddies()
{
    if (_processedBuddies || _fatherSplit==NULL) // pas de traitement à faire ou déjà fait
    {
        _processedBuddies = true;
    }
    else
    {
        std::vector<OCP_CpTrackingSlot*>& sonsSplits = _fatherSplit->getSonsSplits();
        for(std::vector<OCP_CpTrackingSlot*>::iterator it = sonsSplits.begin() ; it != sonsSplits.end() ; it++)
        {
            OCP_CpTrackingSlot* son = (*it);
            if(son->isOriginReserved() ) //indique qu'on a retenu ce split slot dans la solution
            {
                for(std::vector<OCP_CpTrackingSlot*>::iterator it2 = sonsSplits.begin() ; it2 != sonsSplits.end() ; it2++)
                {
                    OCP_CpTrackingSlot* brother = (*it2);
                    if(brother != son && brother->isOriginReserved()) //indique qu'on a retenu ce second split slot dans la solution
                    {
                        son->getSlotOrigin()->addSlotBuddy(brother->getSlotOrigin());
                    }
                }
                son->setIsSlotBuddiesProcessed();//permet d'éviter le traitement ultérieur pour ce fils
            }
        }
    }
}


/**
 * sauvegarde des fils à sauvegarder d'un tracking slot splitté
 * ce traitement fusionne les mini-slots contigus de la solution et crée des nouveaux slots pour les trous
 * @param solver : solver ILOG
 * @return le nombre de splits slots issus de fatherSplit et qu'il faut sauvegarder
 */
int OCP_CpTrackingSlot::saveSplitSons(IloSolver solver)
{
    OcpContextPtr context = OcpXml::getInstance()->getContext();

    /*
     * accès aux requis de la station pour décider quels splits slots sont à sauvegarder (les autres en "trous" étant créés ou modifiant le créneau père origine)
     */
    IloAnySetVar act = getStation()->getTrackingSlotSet();
    IlcAnySet requiredSet = solver.getAnySetVar(act).getRequiredSet();

    /*
     * lecture des temps de configuration / déconfiguration
     */
    uint32_t reconf=0;
    uint32_t deconf=0;
    getStation()->getReconfDeconfDuration(_satellite.get(), reconf, deconf);


    /*
     * la création des nouveaux splits se fait sur toute l'amplitude du créneau père
     * HACK artf646051 : en réalité, l'amplitude est égale à aos los pour les créneaux père !
     */
    OCP_CpDate begin = getBegin() ;//- reconf ; HACK artf646051
    OCP_CpDate end = getEnd() ; //+deconf ; HACK artf646051

    OCP_CpCstIntTimetable tt(OCP_CpModel::getInstance()->getEnv() , begin , end , 0);
    std::vector<OCP_CpTrackingSlot* > sons = getSonsSplits();
    int nbReserved = 0;
    for(std::vector<OCP_CpTrackingSlot* >::iterator it = sons.begin(); it != sons.end(); it++)
    {
        OCP_CpTrackingSlot* son = *it;
        if (requiredSet.isIn(son))
        {
            /*
             * sur toute l'amplitude du créneau de split choisi , la timetable est mise à 1
             */
            tt.setValue(son->getAmplitude().get(),1);
            nbReserved++;
        }
    }

    if(nbReserved==0)
    {
        /*
         * aucun sous-créneau de split n'a été sauvegardé, il n'y a pas lieu de sauver de nouveau créneau fusionné ou additionnel
         */
    }
    else
    {
        std::vector <  OCP_CpTrackingSlot* > savedSplits;//les slots de splits sauvegardés avec fusion

        OCP_CpCstIntTimetableCursor iter(tt,begin);
        bool firstTrackUnreserved = true;//le premier mini-créneau non reservé permet de recadrer le créneau père
        std::vector<OCP_CpTrackingSlot*> buddies;//liste des slots buddies, e.g les slots de splits à sauvegarder
        while (iter.ok())
        {
            IloInt aVal=iter.getValue();
            OCP_CpTimeSlot amplitude( iter.getTimeMin(),  iter.getTimeMax());
            OCP_CpDate aos = amplitude.getBegin() + reconf;
            OCP_CpDate los = amplitude.getEnd() - deconf;


            /*
             * le premier créneau non reservé doit simplement modifier les dates du créneau origine
             */
            if (firstTrackUnreserved && aVal==0)
            {
                firstTrackUnreserved = false;//le premier slot non reservé modifie le slot origine, mais les suivants sont créés
                PHRDate aBeginReconf(amplitude.getBegin() );
                PHRDate aEndReconf(amplitude.getEnd() );
                _slot->getStartEnd()->setStart(aBeginReconf);
                _slot->getStartEnd()->setEnd(aEndReconf);

                PHRDate aAos(aos);
                PHRDate aLos(los);
                _slot->getAosLosRef()->setStart(aAos);
                _slot->getAosLosRef()->setEnd(aLos);

                /*
                 * mise à jour du champs commentaire
                 */
                _slot->setComment(OCP_CpModel::getInstance()->getComment());

            }
            else
            {
                /*
                  * création du nouveau créneau fusionné
                  */
                 OCP_CpTrackingSlot* newSplitSlot = getSatellite()->addOneSplitTrackingSlot(this,savedSplits,amplitude,reconf, deconf);
                 if(aVal==1)
                 {
                     /*
                      * notification du status reservé pour le nouveau créneau
                      */
                     boost::format msg = boost::format( _("Sauvegarde du split slot %1$s") ) % newSplitSlot->getFullName().str();
                     OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str() );

                     /*
                      * notification des TYPE_CRENEAU et NIVEAU_CRENEAU par les contraintes ad-hoc SAT1 ou SAT8
                      */
                     newSplitSlot->postOptim();

                     if (newSplitSlot->getSlotOrigin()->getBookingState() != SlotBookingStates::RESERVED_LOCKED)
                         newSplitSlot->getSlotOrigin()->setBookingState(SlotBookingStates::RESERVED);

// TODO : mis en commantaire car problème avec le modéle a traiter dans le futur
//                     /*
//                      * traitement des slots buddies
//                      */
//                     for(std::vector<OCP_CpTrackingSlot*>::iterator itBuddy = buddies.begin() ; itBuddy != buddies.end() ; itBuddy++)
//                     {
//                         OCP_CpTrackingSlot* aBuddy = (*itBuddy);
//                         aBuddy->getSlotOrigin()->addSlotBuddy(newSplitSlot->getSlotOrigin());
//                         newSplitSlot->getSlotOrigin()->addSlotBuddy(aBuddy->getSlotOrigin());
//                     }
//                     buddies.push_back(newSplitSlot);
                 }

                 /*
                  * pour les splits slots en dehors du premier, leur slot origine a été créé ex-nihilo et doit donc être rajouté au contexte
                  */
                context->addSatelliteSlot(newSplitSlot->getSlotOrigin());
            }

            ++iter;
        }
    }
    return nbReserved;
}

/**
 * mise à jour de la priorité d'un slot de tracking en fonction des informations contenues dans STA6
 */
void OCP_CpTrackingSlot::updateSearchPriority()
{
    if(_priorityStation >0)
        _searchPriority += (_priorityStation-1);
}

/**
* slot id d'un slot de tracking
* @return l'id du slot de tracking
*/
long OCP_CpTrackingSlot::getSlotId() const
{
    return (long) getSlotOrigin()->getSlotID();
}
