/*
 * $Id: OCP_CpTimeSlotTimetable.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "planif/OCP_CpTimeSlotTimetable.h"
#include "planif/OCP_Solver.h"

using namespace ocp::commons;

/**
* constructeur de timetable
* @param solver solveur ILOG
* @param tracking variable ensembliste de domaine l'ensemble des time slots concernés par la timetable
* @param scope étendue concernée par la timetable
* @param levelMinRequired niveau min requis sur l'ensemble du scope
* @param levelMaxRequired niveau max requis sur l'ensemble du scope
* @return nouvelle instance de timetable
*/
OCP_CpTimeSlotTimetable::OCP_CpTimeSlotTimetable(IloSolver solver,IlcAnySetVar tracking,OCP_CpTimeSlotPtr scope, int levelMinRequired, int levelMaxRequired):
_solver(solver) ,
_tracking(tracking),
_scope(scope),
_levelMinRequired(levelMinRequired)
,_levelMaxRequired(levelMaxRequired)
{
    initialize();
}
/**
 * affichage de la timetable
 * @param withPHR : affichage des dates formattées (sinon entier)
 */
void OCP_CpTimeSlotTimetable::display(bool withPHR)
{
    boost::format msg;

    for(std::vector<OCP_CpTimeSlotValue*>::iterator it = _plages.begin();it != _plages.end() ; it++)
    {
        OCP_CpTimeSlotValue* slotValue = *it;

        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, slotValue->getName(withPHR));
    }
}

/**
* initialiser la timetable avec tous les possibles
*/
void OCP_CpTimeSlotTimetable::initialize()
{
    initializeDates();
    initializeValues();
}

/**
* initialise les dates des plages
*/
void OCP_CpTimeSlotTimetable::initializeDates()
{
    std::vector<int> dates;
    dates.push_back(_scope->getBegin());
    dates.push_back(_scope->getEnd());

    for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
    {
        OCP_CpTimeSlot*  current = (OCP_CpTimeSlot*) (*iter);
        OCP_CpTimeSlot ts = _scope->getIntersection(*current);

        dates.push_back(ts.getBegin());
        dates.push_back(ts.getEnd());
    }

    /*
     * trier toutes les dates de début fin de slots par date croissante
     */
    std::sort(dates.begin() , dates.end() );
    /*
     * remove des doublons possibles
     */

    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("Affichage des %1$i dates de fin ") ) % _endTrackings.size() );
    int prev = -1;
    std::vector<int>::iterator it = dates.begin();
    while(it!=dates.end())
    {
        int current = (*it);
        if (current == prev)
            dates.erase(it);//supprimer le doublon courant, it pointe vers son successeur
        else
        {
            prev = current;//mise à jour du nouveau prédécesseur pour la boucle suivante
            it++;
        }
    }

    /*
     * création d'une plage entre une date et sa suivante
     */
    for(it = dates.begin() ;  ; )
    {
        int aDate = *it;
        it++;
        if(it != dates.end() )
        {
            int bDate = *it;
            OCP_CpTimeSlot* plage = new(_solver.getEnv()) OCP_CpTimeSlot(aDate , bDate);
            OCP_CpTimeSlotValue* slotValue = new(_solver.getEnv()) OCP_CpTimeSlotValue(_solver, plage);
            _plages.push_back(slotValue);
        }
        else
            break;

    }
}

/**
* initialise les valeurs des plages
*/
void OCP_CpTimeSlotTimetable::initializeValues()
{
    /*
     * parcourir chacun des slots candidats et l'ajouter aux plages qu'il intersecte
     */
    for(IlcAnySetIterator iter(_tracking.getPossibleSet()); iter.ok(); ++iter)
    {
        OCP_CpTimeSlot*  current = (OCP_CpTimeSlot*) (*iter);
        OCP_CpTimeSlot ts = _scope->getIntersection(*current);

        int first = -1;//indice de la première plage intersectée par ts
        int last = -1;//indice de la dernière plage intersectée par ts
        int cpt=-1;

        for(std::vector<OCP_CpTimeSlotValue*>::iterator it = _plages.begin() ; it != _plages.end() ; it++ )
        {
            cpt++;
            OCP_CpTimeSlotValue* slotValue = *it;
            if(ts.intersect(slotValue->getPlage()))
            {
                if(first==-1)
                    first = cpt;
                last=cpt;

                bool isRequired = _tracking.isRequired(current);
                slotValue->addInitialSlot(current, isRequired);
                //_plages.at()
            }
        }

        _firstPlage.insert(std::map<OCP_CpTimeSlot*, int>::value_type(current, first));
        _lastPlage.insert(std::map<OCP_CpTimeSlot*, int>::value_type(current, last));

    }
    //display(false);
}


/**
* ajout d'un requis à la timetable
* @param newRequired nouveau requis
* @param atBound renseigne les plages ayant atteint la borne supérieure (si renseignée)
* @return vrai ssi le niveau max n'est pas dépassé
*/
bool OCP_CpTimeSlotTimetable::increase(OCP_CpTimeSlot* newRequired,std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>& atBound)
{
    OCP_CpTimeSlot ts = _scope->getIntersection(*newRequired);

    /*
     * accès aux indices de première et dernière plage intersectée par l'intervalle ts
     */
    std::map<OCP_CpTimeSlot*, int>::iterator it1 = _firstPlage.find(newRequired);
    int first = (*it1).second;
    std::map<OCP_CpTimeSlot*, int>::iterator it2 = _lastPlage.find(newRequired);
    int last = (*it2).second;


    for(int cpt = first ; cpt <= last ; cpt++)
    {
        OCP_CpTimeSlotValue* slotValue = _plages.at(cpt);

        int newLevelMin = slotValue->incLevelMin();
        if(_levelMaxRequired>=0 && newLevelMin > _levelMaxRequired )
        {
            boost::format msg = boost::format( _("FAIL  à l'ajout du créneau [%1$d %2$d] sur la plage  [%3$d,%4$d] a une valeur %5$d > %6$d ") )
            % newRequired->getBegin()
            % newRequired->getEnd()
            % slotValue->getPlage()->getBegin()
            % slotValue->getPlage()->getEnd()
            % slotValue->getLevelMin()
            % _levelMaxRequired ;
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  msg );

            return false;
        }
        if(_levelMaxRequired>=0 && newLevelMin == _levelMaxRequired )
        {
            std::vector<OCP_CpTimeSlot*>& initialSlots = slotValue->getInitialSlots();
            for(std::vector<OCP_CpTimeSlot*>::iterator it2 = initialSlots.begin();it2!=initialSlots.end();it2++)
            {
                OCP_CpTimeSlot* possibleSlot = *it2;
                if(_tracking.isPossibleNotRequired( possibleSlot ))
                    atBound.insert(std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>::value_type(possibleSlot, possibleSlot));
            }
        }

    }


    return true;
}

/**
* suppression d'un possible à la timetable
* @param oldPossible ancien possible
* @param atBound renseigne les plages ayant atteint la borne inférieure (si renseignée)
* @return vrai ssi on reste au-dessus du niveau min
*/
bool OCP_CpTimeSlotTimetable::decrease(OCP_CpTimeSlot* oldPossible, std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>& atBound )
{
    OCP_CpTimeSlot ts = _scope->getIntersection(*oldPossible);
    /*
     * accès aux indices de première et dernière plage intersectée par l'intervalle ts
     */
    std::map<OCP_CpTimeSlot*, int>::iterator it1 = _firstPlage.find(oldPossible);
    int first = (*it1).second;
    std::map<OCP_CpTimeSlot*, int>::iterator it2 = _lastPlage.find(oldPossible);
    int last = (*it2).second;

    for(int cpt = first ; cpt <= last ; cpt++)
    {
        OCP_CpTimeSlotValue* slotValue = _plages.at(cpt);

        int newLevelMax = slotValue->decLevelMax();
        if(_levelMinRequired>=0 && newLevelMax < _levelMaxRequired )
        {
            boost::format msg = boost::format( _("FAIL  au remove du créneau [%1$d %2$d] sur la plage  [%3$d,%4$d] a une valeur %5$d < %6$d ") )
            % oldPossible->getBegin()
            % oldPossible->getEnd()
            % slotValue->getPlage()->getBegin()
            % slotValue->getPlage()->getEnd()
            % slotValue->getLevelMax()
            % _levelMinRequired ;
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst,  msg );

            return false;
        }
        if(_levelMinRequired>=0 && newLevelMax == _levelMinRequired )
        {
            std::vector<OCP_CpTimeSlot*>& initialSlots = slotValue->getInitialSlots();
            for(std::vector<OCP_CpTimeSlot*>::iterator it2 = initialSlots.begin();it2!=initialSlots.end();it2++)
            {
                OCP_CpTimeSlot* possibleSlot = *it2;
                if(_tracking.isPossibleNotRequired( possibleSlot ))
                    atBound.insert(std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>::value_type(possibleSlot, possibleSlot));
            }
        }

    }
    return true;
}

/**
 * renvoie le maximum des niveau requis sur le scope
 * @return le maximum des niveau requis sur le scope
 */
int OCP_CpTimeSlotTimetable::getMaxOfLevelMin()
{
    int max = 0;
    for(std::vector<OCP_CpTimeSlotValue*>::iterator it = _plages.begin() ; it != _plages.end() ; it++)
    {
        OCP_CpTimeSlotValue* slotValue = *it;
        if (slotValue->getLevelMin() > max)
            max = slotValue->getLevelMin();
    }
    return max;
}

/**
* renvoie le minimum des niveau possibles sur le scope
* @return le minimum des niveau possibles sur le scope
*/
int OCP_CpTimeSlotTimetable::getMinOfLevelMax()
{
    int min=-1;
    for(std::vector<OCP_CpTimeSlotValue*>::iterator it = _plages.begin() ; it != _plages.end() ; it++)
    {
        OCP_CpTimeSlotValue* slotValue = *it;
        if (min<0 || slotValue->getLevelMax() < min)
            min = slotValue->getLevelMax();
    }
    return min;
}
