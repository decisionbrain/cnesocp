/*
 * $Id: OCP_CpSearch.cpp 942 2010-12-21 13:11:22Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 *
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSearch.cpp
 *
 */
#include <ilsolver/ilosolver.h>

#include "context/OCP_CpObject.h"
#include "planif/OCP_CpSearch.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpMaintenanceSlot.h"
#include "context/OCP_CpModel.h"
#include <ConfAccel.h>
#include <math.h>

using namespace std;

ILOSTLBEGIN

/*
* Redefinition d'un instantiate simple de maniere exhaustive, sous la forme d'une classe.
* Totalement equivalent a l'utilisation de la macro suivante:
*
* ILCGOAL1(OCP_CpSimpleInstantiate, IlcIntVar, var) {
* 	if(!var.isBound()) {
* 		IlcInt v = var.getMin();
* 		s.out() << "Testing " << v << " for var" << endl;
* 		return IlcOr(var == v, IlcAnd(var != v, this));
* 	}
* 	s.out() << "var is bound" << endl;
* 	return 0;
* }
*
*
*/

class OCP_CpSimpleInstantiateI : public IlcGoalI {
public:
	IlcIntVar var;
	OCP_CpSimpleInstantiateI(IloSolver solver, IlcIntVar);
	IlcGoal execute();
};

OCP_CpSimpleInstantiateI::OCP_CpSimpleInstantiateI(IloSolver solver, IlcIntVar arg1):IlcGoalI(solver), var(arg1){}

IlcGoal OCP_CpSimpleInstantiateI :: execute() {
	IloSolver s = getSolver();
	if(var.getObject()!= 0)
		s.out() << "Attached Object " << ((OCP_CpObject*)(var.getObject()))->getName() << endl;

	if(!var.isBound()) {
		IlcInt v = var.getMin();
		s.out() << "Testing " << v << " for var" << endl;
		return IlcOr(var == v, IlcAnd(var != v, this));
	}
	s.out() << "x is bound" << endl;
	return 0;
}

IlcGoal OCP_CpSimpleInstantiate(IloSolver s, IlcIntVar var){
	return new (s.getHeap()) OCP_CpSimpleInstantiateI(s.getImpl(), var);
}

/*
* Redefinition d'un generate simple
*/
ILCGOAL1(OCP_CpSimpleGenerate, IlcIntVarArray, vars) {
	IlcInt index = IlcChooseFirstUnboundInt(vars);
	if(index == -1) return 0;
	return IlcAnd(OCP_CpSimpleInstantiate(getSolver(), vars[index]), this);
}

/*
* wrapper concert-solver pour le generate
*/
ILOCPGOALWRAPPER1(OCP_CpSimpleConcertGenerate, solver, IloIntVarArray, vars) {
	// link associated object between concertVars and solverVars; not done during extraction!
	IlcIntVarArray tmp = solver.getIntVarArray(vars);
	for(int i=0; i<tmp.getSize(); i++) {
		tmp[i].setObject(vars[i].getObject());
	}

	return OCP_CpSimpleGenerate(solver, solver.getIntVarArray(vars));
}

/**
 * méthode de sélection du prochain possible basée sur différents critères (cf aussi l'algorithme artf591547)
 * On utilise l'ordre lexicographique suivant (c1 "avant" c2 indique qu'on sélectionnera c1 avant c2 dans l'arbre de recherche) :
 * CRIT0 : un créneau est avant le créneau NULL
 * CRIT1 : Un créneau satellite est avant un crénau de maintenance
 * CRIT 2 : Un créneau de maintenance de durée d1 est avant un créneau satellite de durée d2 > d1
 * CRIT3 : Un créneau satellite prioritaire pour SAT1 (plus long créneau) est avant un créneau satellite qui ne l'est pas
 * CRIT4 : un créneau le plus éloigné du dernier SAT2MAX assigné est prioritaire par rapport à un créneau qui ne l'est pas
 * CRIT 5 : Un créneau satellite de priorité aggrégée PR1 est avant un créneau satellite de priorité PR2 si PR1!=0 && PR1<PR2
 * CRIT 6 : Un créneau satellite de priorité station PR1 est avant un créneau satellite de priorité station PR2 si PR1!=0 && PR1<PR2
 * CRIT 7 : un créneau d'id interne id id1 est avant un créneau d'id interne id2 si id1 < id2
 *
 * L'algorithme plus général de sélection du prochain meilleur créneau à assigner suit donc globalement l'algorithme suivant :

1) Créneaux satellites avant créneaux de maintenance
2) Pour chaque SAT1 qui le demande, le créneau le plus long disponible en premier
3) Les créneaux les plus prioritaires en premier

En cas d'égalité de priorité aggrégée , on discrimine par la priorité station (TODO ?)

 * @param var la variable ensembliste
 * @return le choix du prochain élèment (singleton) à rajouter aux requis
 */
/***************************************************************************************************************
 *                                  Selecteur de base
 ***************************************************************************************************************/
IlcAny OCP_CpSelectorI::select(IlcAnySetVar var) //TODO optimisation du tri
{
    //boost::format msg = boost::format( _("++++  DEBUT DE LE PHASE DE SELECTION +++++") ) ;
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    /*
     * accès temporaire à l'ancien sélecteur non déterministe
     */


    OCP_CpTimeSlot* bestSlot = NULL;
    //cout << "POSSIBLE SET " << var.getPossibleSet() << endl;

    /*
     * liste des meilleurs slots avec ex-aequo
     */
    //std::vector<OCP_CpTimeSlot*> bestties;

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloSolver solver = cpModel->getSolver();
    //cpModel->displayCardinalityInfos();

    std::vector<int>& rankedCriterias = cpModel->getRankedCriterias();
    bool withRestart = (ConfAccel::getInstance()->getSolverRestart()>0);

    /*
     * satisfaire les seuils min des contraintes de cardinalité SAT1 :
     * recalcul du support des contraintes SAT1 qui n'atteignent pas leur borne min
     * ceci n'est pas fait si l'on a déjà précédemment montré qu'aucune n'était violée
     * (ou que ce critère n'est pas pris en compte)
     */
    if (cpModel->hasSlackMinPass() )
    {
        //cout << "All Slack Min Pass KO" << endl;
        cpModel->computeBestImprovementMinPass();
    }
    //else
      //  cout << "All Slack Min Pass OK" << endl;

    /*
     * satisfaire les seuils min des contraintes de durée cumulée  SAT3:
     * recalcul du support des contraintes SAT3 qui n'atteignent pas leur borne min
     * ceci n'est pas fait si l'on a déjà précédemment montré qu'aucune n'était violée
     * (ou que ce critère n'est pas pris en compte)
     */
    if (cpModel->hasSlackSat3MinDur() )
    {
        //cout << "All SAT3 Min Dur KO" << endl;
        cpModel->computeBestImprovementSat3MinDur();
    }
    //else
      //  cout << "All SAT3 Min Dur OK" << endl;

    for(IlcAnySetIterator iter(var.getPossibleSet());  iter.ok(); ++iter)
    {
        if (!var.isRequired(*iter))
        {

            OCP_CpTimeSlot* slot = (OCP_CpTimeSlot*) (*iter);
            //boost::format msg = boost::format( _("--- possible %1$s") ) % slot->getFullName();
            //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

            int crit = 0;
            for(std::vector<int>::iterator it = rankedCriterias.begin(); it != rankedCriterias.end() ; it++)
            {
                int level = (*it);

                crit = compareCRIT(level,bestSlot,slot);

                /*int bestSlotId = -1;
                if (bestSlot != NULL)
                    bestSlotId = bestSlot->getSlotId();
                cout << "compare " << bestSlotId << " et " << slot->getSlotId() << "suivant critère "<< level << " returns " << crit << endl;
*/
                if(crit==1)
                    break;//slot est dominé par bestSlot pour le critère courant : on passe au slot suivant

                if(crit==-1)
                {
                    bestSlot = slot;//slot domine bestSlot : on update bestSlot et on passe au slot suivant
                    break;
                }
                //les deux slots sont ex-aequo pour le critère courant : on passe au critère suivant
            }

            /*
             * bestSlot et slot sont équivalents suivant tous les critères :
             * en mode restart,on perturbe en remplaçant aléatoirement bestSlot par slot
             */
            if (crit==0 && withRestart)
            {
                int rndVal = solver.getRandom().getInt(2);//choisit aléatoirement un nombre sur [0,2)
                if (rndVal == 1 )
                {
                    //cout << "random switch " << bestSlot->getSlotId() << " et " << slot->getSlotId() << endl;
                    bestSlot = slot;
                }
            }
        }
    }

    //msg = boost::format( _("++++  FIN DE LE PHASE DE SELECTION +++++") ) ;
    //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);


    return bestSlot;

}

/**
 * comparaison par niveau hiérarchique de critère
 * @param levelCrit : niveau hiérarchique de critère
 * @param bestSlot : meilleur slot courant
 * @param slot : slot candidat
 * @return : 1 si si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT(int levelCrit , OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    switch(levelCrit)
    {
        case 0:return compareCRIT0(bestSlot,slot);
        case 1:return compareCRIT1(bestSlot,slot);
        case 2:return compareCRIT2(bestSlot,slot);
        case 3:return compareCRIT3(bestSlot,slot);
        case 4:return compareCRIT4(bestSlot,slot);
        case 5:return compareCRIT5(bestSlot,slot);
        case 6:return compareCRIT6(bestSlot,slot);
        case 7:return compareCRIT7(bestSlot,slot);
        case 8:return compareCRIT8(bestSlot,slot);
        case 9:return compareCRIT9(bestSlot,slot);
        default:throw OCP_XRoot("niveau de comparaison non reconnu");
    }
}

/**
 * comparaison suivant le critère CRIT0 : Un créneau NULL est toujours dominé
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT0(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if(bestSlot != NULL && slot ==NULL)
        return 1;

    if(bestSlot == NULL && slot != NULL)
        return -1;

    return 0;
}

/**
 * comparaison suivant le critère CRIT1 : Un créneau satellite est avant un crénau de maintenance
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT1(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if(bestSlot->isMaintenanceSlot() && slot->isTrackingSlot())
        return -1;

    if (bestSlot->isTrackingSlot() && slot->isMaintenanceSlot())
        return 1;

    return 0;
}
/**
 * comparaison suivant le critère CRIT2 : Un créneau de maintenance de durée d1 est avant un créneau maintenance de durée d2 > d1
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT2(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if (bestSlot->isTrackingSlot() || slot->isTrackingSlot())
        return 0;//cette comparaison n'a de sens que pour des créneaux de maintenance

    //artf679718 : [Calcul] : CSta2 r�serve des cr�neaux trop longs
    //L'ordre etait inverse
    if (slot->getDuration()< bestSlot->getDuration())
    {
        return -1;
    }
    else
    {
        return 1;
    }
}

/**
 * comparaison suivant le critère CRIT3 : Un créneau satellite prioritaire pour SAT1 (plus long créneau) est avant un créneau satellite qui ne l'est pas
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT3(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if (!bestSlot->isTrackingSlot() || !slot->isTrackingSlot())
        return 0;//cette comparaison n'a de sens que pour des créneaux de tracking

    OCP_CpTrackingSlot* slotT = (OCP_CpTrackingSlot*) slot;
    OCP_CpTrackingSlot* bestSlotT = (OCP_CpTrackingSlot*) bestSlot;
    if( !bestSlotT->isLongestInSat1()  && slotT->isLongestInSat1())
    {
        return -1;
    }
    if( bestSlotT->isLongestInSat1()  && !slotT->isLongestInSat1())
    {
        return 1;
    }
    return 0;
}

/**
 * comparaison suivant le critère CRIT4 : Un créneau satellite améliorant une contrainte SAT1 MIN la moins satisfaite est prioritaire
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT4(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if (!bestSlot->isTrackingSlot() || !slot->isTrackingSlot())
        return 0;//cette comparaison n'a de sens que pour des créneaux de tracking
    /*
     * lorsque toutes les contraintes SAT1 sont vérifiées pour leur borne min, les créneaux ne sont plus comparables
     */
    if (! OCP_CpModel::getInstance()->hasSlackMinPass() )
        return 0;

    OCP_CpTrackingSlot* slotT = (OCP_CpTrackingSlot*) slot;
    OCP_CpTrackingSlot* bestSlotT = (OCP_CpTrackingSlot*) bestSlot;

    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

    bool bestSlotImprovement = cpModel->hasBestImprovementSat1MinPass(bestSlotT);
    bool slotImprovement = cpModel->hasBestImprovementSat1MinPass(slotT);

    if( slotImprovement &&  !bestSlotImprovement)
    {
        bestSlotImprovement = slotImprovement;//maj du bestSlotImprovement
        //boost::format msg = boost::format( _("Improvement %1$f") ) % bestSlotImprovement ;
        //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        return -1;
    }
    if( !slotImprovement && bestSlotImprovement)
    {
        return 1;
    }
    return 0;
}


/**
 * comparaison suivant le critère CRIT5 : un créneau le plus éloigné du dernier SAT2MAX assigné est prioritaire par rapport à un créneau qui ne l'est pas
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT5(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if (!bestSlot->isTrackingSlot() || !slot->isTrackingSlot())
        return 0;//cette comparaison n'a de sens que pour des créneaux de tracking

    OCP_CpTrackingSlot* slotT = (OCP_CpTrackingSlot*) slot;
    OCP_CpTrackingSlot* bestSlotT = (OCP_CpTrackingSlot*) bestSlot;
    if (! bestSlotT->isNextSAT2MaxCandidate() && slotT->isNextSAT2MaxCandidate() )
    {
       return -1;
    }
    if ( bestSlotT->isNextSAT2MaxCandidate() &&  !slotT->isNextSAT2MaxCandidate() )
    {
        return 1;
    }
    return 0;
}

/**
 * comparaison suivant le critère CRIT6 : Un créneau satellite de priorité aggrégée PR1 est avant un créneau satellite de priorité PR2 si PR1!=0 && PR1<PR2
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT6(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if (!bestSlot->isTrackingSlot() || !slot->isTrackingSlot())
        return 0;//cette comparaison n'a de sens que pour des créneaux de tracking

    OCP_CpTrackingSlot* slotT = (OCP_CpTrackingSlot*) slot;
    OCP_CpTrackingSlot* bestSlotT = (OCP_CpTrackingSlot*) bestSlot;
    if(slotT->getSearchPriority() >0 && (bestSlotT->getSearchPriority()==0 || slotT->getSearchPriority() < bestSlotT->getSearchPriority()) )
    {
        return -1;
    }
    if(bestSlotT->getSearchPriority() >0 && (slotT->getSearchPriority() ==0 || bestSlotT->getSearchPriority() < slotT->getSearchPriority() ) )
    {
       return 1;
    }

    return 0;
}

/**
 * comparaison suivant le critère CRIT7 : Un créneau satellite de priorité station PR1 est avant un créneau satellite de priorité station PR2 si PR1!=0 && PR1<PR2
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT7(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if (!bestSlot->isTrackingSlot() || !slot->isTrackingSlot())
        return 0;//cette comparaison n'a de sens que pour des créneaux de tracking

    OCP_CpTrackingSlot* slotT = (OCP_CpTrackingSlot*) slot;
    OCP_CpTrackingSlot* bestSlotT = (OCP_CpTrackingSlot*) bestSlot;
    //(e.g entre les ex-aequo de priorité aggrégée), choisir celui de plus faible priorité SAT4
    if(slotT->getStationPriority() >0 && (bestSlotT->getStationPriority()==0 || slotT->getStationPriority() < bestSlotT->getStationPriority() ))
    {
        bestSlot = slot;
        //bestties.clear();
        //bestties.push_back(bestSlot);

        //cout << "CRIT6 best slot " << bestSlot->getFullName() << endl;
        return -1;
    }
    if(bestSlotT->getStationPriority() >0 && (slotT->getStationPriority()==0 || bestSlotT->getStationPriority() < slotT->getStationPriority()))
    {
        return 1;//le slot courant est dominé par bestSlot sur la base du critère 6
    }

    return 0;
}

/**
 * comparaison suivant le critère CRIT8 : un créneau d'id interne id id1 est avant un créneau d'id interne id2 si id1 < id2
 * @param bestSlot : meilleur slot pour l'instant
 * @param slot : slot courant
 * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
 */
int OCP_CpSelectorI::compareCRIT8(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    //CRIT7: un créneau d'id interne id id1 est avant un créneau d'id interne id2 si id1 < id2
    if (slot->getId() < bestSlot->getId() )
    {
        bestSlot = slot;
        //cout << "CRIT7 best slot " << bestSlot->getFullName() << endl;
        return -1;
    }
    else
    {
        return 1;//le slot courant est dominé par bestSlot sur la base du critère 7
    }

}

/**
* comparaison suivant le critère CRIT9 : satisfaire d'abord les contraintes SAT3 qui n'atteignent pas leur durée min
* @param bestSlot : meilleur slot pour l'instant
* @param slot : slot courant
* @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
*/
int OCP_CpSelectorI::compareCRIT9(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const
{
    if (!bestSlot->isTrackingSlot() || !slot->isTrackingSlot())
        return 0;//cette comparaison n'a de sens que pour des créneaux de tracking
    /*
     * lorsque toutes les contraintes SAT3 sont vérifiées pour leur borne min, les créneaux ne sont plus comparables
     */
    if (! OCP_CpModel::getInstance()->hasSlackSat3MinDur() )
        return 0;

    OCP_CpTrackingSlot* slotT = (OCP_CpTrackingSlot*) slot;
    OCP_CpTrackingSlot* bestSlotT = (OCP_CpTrackingSlot*) bestSlot;


    OCP_CpModel* cpModel = OCP_CpModel::getInstance();

    bool bestSlotImprovement = cpModel->hasBestImprovementSat3MinDur(bestSlotT);
    bool slotImprovement = cpModel->hasBestImprovementSat3MinDur(slotT);

    //cout << "CRIT9" << bestSlot->getSlotId() << " "<<slot->getSlotId() << " bestSlotImprove " << bestSlotImprovement << " slotImprove "<<slotImprovement<<endl;

    if( slotImprovement &&  !bestSlotImprovement)
    {
        bestSlotImprovement = slotImprovement;//maj du bestSlotImprovement
        //boost::format msg = boost::format( _("Improvement in SAT3 min") )  ;
        //OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        return -1;
    }
    if( !slotImprovement && bestSlotImprovement)
    {
        return 1;
    }



    return 0;
}


/**
 * méthode créant l'implémentation du sélecteur OCP_CpSelectorI
 * @param s solver
 * @return le sélecteur à intégrer dans un goal d'instanciation d'une variable ensembliste
 */
IlcAnySetSelect OCP_CpSelect(IloSolver s)
{
   //return new (s.getHeap()) myAnySetSelect2I();
    return new (s.getHeap()) OCP_CpSelectorI();
}


ILCGOAL2(OCP_DisplaySelected, OCP_CpTimeSlot*,slot,int,trace)
{
    if(trace==2)
    {
        boost::format msg = boost::format( _("S %1$d") ) % slot->getSlotId();
        if (slot->isTrackingSlot())
        {
            OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*) slot;
            OCP_CpSatellitePtr aSat = ts->getSatellite();
            msg = boost::format( _("%1$s %2$s") ) % msg.str() % aSat->getName();
        }
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    }
    if (trace>0)
        OCP_CpModel::getInstance()->push(slot->getSlotId(), slot->isTrackingSlot());
    return 0;
}
ILCGOAL2(OCP_DisplayRemoved, OCP_CpTimeSlot*,slot,int,trace)
{
    if(trace==2)
    {
        boost::format msg = boost::format( _("R %1$d") ) % slot->getSlotId();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    }
    if (trace>0)
        OCP_CpModel::getInstance()->back(slot->getSlotId(), slot->isTrackingSlot());
    return 0;
}

/**
 * goal complet avec point de choix sur l'instanciation des AnySetVar
 */
ILCGOAL3(OCP_IlcCompleteInstantiateAny,IlcAnySetVar,ilcSetVar,OCP_CpSelectorI*,selector2,int, trace)
{
    if(ilcSetVar.isBound())
        return 0;

    //IlcIntSetVar v(ilcSetVar.getImpl());
    //OCP_CpTimeSlot* val = (OCP_CpTimeSlot*) selector2->select(v);
    OCP_CpTimeSlot* val = (OCP_CpTimeSlot*) selector2->select(ilcSetVar);

    IlcGoal gSel = OCP_DisplaySelected(getSolver(),val,trace);
    IlcGoal gRem = OCP_DisplayRemoved(getSolver(),val,trace);

    return IlcAnd(
            IlcOr(
                    IlcAnd( gSel,IlcMember(val, ilcSetVar) ),//ilcSetVar.addRequired(val),
                            IlcAnd(gRem ,IlcNotMember(val , ilcSetVar)) //ilcSetVar.removePossible(val)
            )
            ,this);

}

/**
 * goal complet avec point de choix sur l'instanciation des AnySetVar : stratégie remove first
 */
ILCGOAL3(OCP_IlcReverseInstantiateAny,IlcAnySetVar,ilcSetVar,IlcIntSetSelectI*,selector2, bool, trace)
{
    if(ilcSetVar.isBound())
        return 0;

    IlcIntSetVar v(ilcSetVar.getImpl());
    OCP_CpTimeSlot* val = (OCP_CpTimeSlot*) selector2->select(v);
    IlcGoal gSel = OCP_DisplaySelected(getSolver(),val,trace);
    IlcGoal gRem = OCP_DisplayRemoved(getSolver(),val,trace);

    return IlcAnd(
            IlcOr(
                    IlcAnd(IlcNotMember(val , ilcSetVar), gRem),//ilcSetVar.addRequired(val),
                            IlcAnd( IlcMember(val, ilcSetVar), gSel )  //ilcSetVar.removePossible(val)
            )
            ,this);


}

/**
 * wrapper concert-solver pour le generate
 */
ILOCPGOALWRAPPER2(OCP_CpCompleteInstantiateAny,solver,IloAnySetVar,myvar,int,traceChoicePoints)
{
    boost::format msg = boost::format( _("LANCEMENT DE LA PHASE RECHERCHE") ) ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    OCP_CpSelectorI* sel2 = new (solver.getHeap()) OCP_CpSelectorI();
    return OCP_IlcCompleteInstantiateAny(solver,solver.getAnySetVar(myvar), sel2 , traceChoicePoints);
}

ILOCPGOALWRAPPER1(OCP_CpCompleteGenerate,solver,IloAnySetVar,myvar)
{
    IlcAnySetVarArray mySingleton(solver,1,solver.getAnySetVar(myvar));
    return IlcGenerate(mySingleton, IlcChooseFirstUnboundAnySet, OCP_CpSelect(solver));
}

ILOCPGOALWRAPPER0(OCP_CpReserveSlots,solver)
{
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    cpModel->addReservationConstraints(solver);

    /*boost::format msg = boost::format( _("Etat des requis à l'issue de la phase de réservation") ) ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
    IlcAnySetVar ilcGlobal = solver.getAnySetVar(cpModel->getGlobalSlotSetVar());
    IlcAnySet allrequireds = ilcGlobal.getRequiredSet();
    for(IlcAnySetIterator iter(allrequireds); iter.ok(); ++iter)
    {
        OCP_CpTimeSlot* currentTrackingSlot = (OCP_CpTimeSlot*) (*iter);
        msg = boost::format( _("Forced %1$s") ) % currentTrackingSlot->getFullName();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    }
    msg = boost::format( _("Slots supprimés à l'issue de la phase de réservation") ) ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

    IloAnyArray globalSlots = cpModel->getGlobalSlots();
    for(int i=0;i<globalSlots.getSize();i++)
    {
        OCP_CpTimeSlot* currentTrackingSlot  = (OCP_CpTimeSlot*) globalSlots[i];
        if(!ilcGlobal.isPossible(currentTrackingSlot))
        {
            msg = boost::format( _("Removed %1$s") ) % currentTrackingSlot->getFullName();
            OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
        }
    }*/

    return 0;
}

ILOCPGOALWRAPPER0(OCP_CpPropagate,solver)
{
    solver.propagate();
    return 0;
}
ILOCPGOALWRAPPER1(OCP_CpInstantiate,solver,IloAnySetVar,myvar)
{
    return IlcInstantiate(solver.getAnySetVar(myvar));
}
ILOCPGOALWRAPPER2(OCP_CpRemoveFirst,solver,IloAnySetVar,myvar,bool,traceChoicePoints)
{
    IlcIntSetSelectI* sel2 = new (solver.getHeap()) OCP_CpSelectorI();
    return OCP_IlcReverseInstantiateAny(solver,solver.getAnySetVar(myvar), sel2, traceChoicePoints );
}
