/*
 * $Id: OCP_CpMaintenanceSlot.cpp 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpMaintenanceSlot.cpp
 *
 */
#include <string>

#include "OCP_CpDate.h"
#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpStation.h"
#include "planif/OCP_CpMaintenanceSlot.h"
#include "planif/OCP_Solver.h"
#include "context/OCP_CpModel.h"


using namespace std;

/**
 * constructeur généré par l'algorithme de création des créneaux de maintenance
 * @param aStation station origine
 * @param begin debut du créneau
 * @param end fin du créneau
 * @return nouvel objet de type slot de maintenance pour solveur
 */
OCP_CpMaintenanceSlot::OCP_CpMaintenanceSlot(OCP_CpStationPtr s,  OCP_CpDate x, OCP_CpDate y)
: OCP_CpTimeSlot(x, y), _station(s), _offVisibility(true)
{
	setName("Maintenance");

    _slotOrigine = boost::dynamic_pointer_cast<OcpStationSlot>(OcpStationSlot::createInstance());
    PHRDate begin(x);
    PHRDate end(y);
    OcpPeriodPtr period = boost::dynamic_pointer_cast<OcpPeriod>(OcpPeriod::createInstance());
    period->setStart(begin);
    period->setEnd(end);
    _slotOrigine->setStartEnd(period);
    _slotOrigine->setStation(s->getOrigin());//renseigner la station origine
    _hasBookingOrder = false;

}

/**
 * constructeur depuis un slot de maintenance donné préfixé par l'utilisateur
 * @param slot_
 * @return nouvel objet de type slot de maintenance pour solveur
 */
OCP_CpMaintenanceSlot::OCP_CpMaintenanceSlot(OcpStationSlotPtr slot_)
: OCP_CpTimeSlot(-1,-1), _offVisibility(true)
{
    setName("Maintenance");
    _slotOrigine = slot_;
    _station = OCP_CpModel::getInstance()->getStation(slot_->getStation());

    setBegin( (OCP_CpDate) slot_->getStartEnd()->getStart().getSeconds() );
    setEnd( (OCP_CpDate) slot_->getStartEnd()->getEnd().getSeconds() );

    _toRemove = false;//positionné ultérieurement par SAT4 (ou heures d'ouverture de la station correspondante)
    _reserved = slot_->getBookingState()  == SlotBookingStates::RESERVED || slot_->getBookingState()  == SlotBookingStates::RESERVED_LOCKED;
    _hasBookingOrder = false;

    try
    {
        _hasBookingOrder = slot_->hasBeenBookedByRequest();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire quand les champs ne sont pas lus : on se contentera des valeurs par défaut
    }
    if (_hasBookingOrder && ! _reserved)
    {
        PHRDate pBegin = slot_->getStartEnd()->getStart();
        PHRDate pEnd = slot_->getStartEnd()->getEnd();
        boost::format msg = boost::format( _("Station slot on station '%1$s' from '%2$s' to '%3$s' a été vu 'bookedByRequest' sans être reservé") )
                   % _station->getName()  % pBegin.getStringValue() % pEnd.getStringValue() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
        throw OCP_XRoot(msg.str());
    }
}

string OCP_CpMaintenanceSlot::getName() const
{
	return _station->getName() + "-"+OCP_CpTimeSlot::getName();
}


/**
 * traitement du créneau satellite pour la sauvegarde : renseignemenent des champs TYPE_CRENEAU, NIVEAU_CRENEAU
 */
void OCP_CpMaintenanceSlot::postOptim()
{
    OCP_CpConstraintPtr ctr;

    for(VectorOCP_CpConstraint::iterator it = _cpConstraints.begin() ; it != _cpConstraints.end() ; ++it )
    {
        ctr = (*it);
        ctr->postOptim(this);
    }

    /*
     * flagguer le slot à RESERVED à moins qu'il ne soit déjà RESERVED_LOCKED
     */
    if (_slotOrigine->getBookingState() != SlotBookingStates::RESERVED_LOCKED)
        _slotOrigine->setBookingState(SlotBookingStates::RESERVED);

    /*
     * mise à jour du champs commentaire
     */
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    _slotOrigine->setComment(cpModel->getComment());

}

/**
* chaine plus complete
* @return description complète
*/
boost::format OCP_CpMaintenanceSlot::getFullName() const
{

    uint32_t slotId = _slotOrigine->getSlotID();
    if (slotId <= 0)
        slotId = getId();//slot de maintenance non précédemment créé : utiliser l'id interne

    boost::format bookingOrderMsg = boost::format( _("") );
    if (_hasBookingOrder ==true)
        bookingOrderMsg = boost::format( _("*") );

    boost::format msg = boost::format( _("R%1$d%2$s Maintenance SlotId %3$i on station '%4$s' from '%5$s' to '%6$s'") )
            % _reserved
            % bookingOrderMsg.str()
            % slotId
            % _station->getName()
            % _slotOrigine->getStartEnd()->getStart().getStringValue()
            % _slotOrigine->getStartEnd()->getEnd().getStringValue()
            ;
    return msg;
}

/**
* slot id d'un slot de maintenance
* @return l'id du slot de maintenance
*/
long OCP_CpMaintenanceSlot::getSlotId() const
{
    long slotId = (long) _slotOrigine->getSlotID();
    if (slotId <= 0)
        slotId = (long) getId();//slot de maintenance non précédemment créé : utiliser l'id interne
    return slotId;
}
