/*
 * $Id: OCP_CpTimeSlotValue.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "planif/OCP_CpTimeSlotValue.h"

/**
* mémorisation initiale d'un slot candidat
* @param slot
* @param requis vrai ssi le slot est déjà vu comme requis
*/
void OCP_CpTimeSlotValue::addInitialSlot(OCP_CpTimeSlot* slot,bool isRequired)
{
    _initialSlots.push_back(slot);

    /*
     * on incrémente le niveau des possibles
     */
    incLevelMax();

    /*
     * si le slot est requis, on peut incrémenter le niveau des requis
     */
    if(isRequired)
        incLevelMin();
}

/**
* affichage de la plage et niveaux
* @param withPHR : affichage des dates formattées (sinon entier)
*/
boost::format OCP_CpTimeSlotValue::getName(bool withPHR) const
{
    if(withPHR)
    {

        boost::format msg = boost::format( _("[%1$s MIN %2$d MAX %3$d]") )
        % _plage->getFullName() % getLevelMin() % getLevelMax() ;
        return msg;
    }
    else
    {

        boost::format msg = boost::format( _("[%1$d,%2$d]->[MIN %3$d MAX %4$d]") )
        % _plage->getBegin() % _plage->getEnd() % getLevelMin() % getLevelMax() ;
        return msg;
    }
}
