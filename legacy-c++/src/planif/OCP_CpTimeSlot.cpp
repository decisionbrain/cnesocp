/*
 * $Id: OCP_CpTimeSlot.cpp 928 2010-11-30 16:34:23Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * VERSION : 2-0 : FA : artf660903 : 30/11/2010 : Calcul du planning apr�s r�servation manuelle de cr�neaux NOK (https://coconet2.capgemini.com/sf/go/artf660903/")
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpTimeSlot.cpp
 *
 */
#include <string>

#include "planif/OCP_Solver.h"

#include "OCP_CpDate.h"
#include "planif/OCP_CpTimeSlot.h"


using namespace std;
using namespace ocp::commons;

/**
* Constructeur d'un objet OCP_CpTimeSlot à partir de 2 dates
* @param x Date de début
* @param y Date de fin
* @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot
*/
OCP_CpTimeSlot::OCP_CpTimeSlot(OCP_CpDate x, OCP_CpDate y)
:_begin(x), _end(y),_capacity(1),
 _reserved(false),
 _toRemove(false),
 _hasBookingOrder(false)
 {
 }

/**
* Constructeur de copie d'un objet OCP_CpTimeSlot. Cela permet de contrôler la duplication
* d'une instance d'un tel objet
* @param ts l'objet OCP_CpTimeSlot source de la copie
* @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot identique à ts
*/
OCP_CpTimeSlot::OCP_CpTimeSlot(const OCP_CpTimeSlot& ts)
: _begin(ts._begin), _end(ts._end) ,
  _reserved(false),
  _toRemove(false),
  _hasBookingOrder(false)
  {

  }



/**
 * constructeur à partir d'un intervalle métier : conversion en secondes
 * @param period l'intervalle métier
 * @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot basé sur la conversion en secondes de l'intervalle métier
 */
OCP_CpTimeSlot::OCP_CpTimeSlot(OcpPeriodPtr period):
_begin( (OCP_CpDate) period->getStart().getSeconds() ),
_end( (OCP_CpDate) period->getEnd().getSeconds() ),
_reserved(false),
_toRemove(false),
_hasBookingOrder(false)
{

}

/**
 * constructeur à partir d'un intervalle métier : conversion en secondes
 * @param period l'intervalle métier
 * @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot basé sur la conversion en secondes de l'intervalle métier
 */
OCP_CpTimeSlot::OCP_CpTimeSlot(OcpTimePeriodPtr period):
_begin( (OCP_CpDate) period->getStart().getSeconds() ),
_end( (OCP_CpDate) period->getEnd().getSeconds() ),
_reserved(false),
_toRemove(false),
_hasBookingOrder(false)
{

}


/**
 * Intersection de deux intervalles semi-ouverts [a,b) [a',b') ssi a<b' et a'<b
 * @param other : intervalle  à comparer
 * @return : vrai ssi les deux intervalles s'intersectent
 *
 *///TODO enlever une fois que les OCP_CpTimeSlotPtr sont généralisés
bool OCP_CpTimeSlot::intersect(const OCP_CpTimeSlot* other) const
{
	return this->getBegin() < other->getEnd() && other->getBegin() < this->getEnd();
}

/**
 * Intersection de deux intervalles semi-ouverts [a,b) [a',b') ssi a<b' et a'<b
 * @param other : intervalle  à comparer
 * @return : vrai ssi les deux intervalles s'intersectent
 *
 */
bool OCP_CpTimeSlot::intersect(OCP_CpTimeSlotPtr other) const
{
    return this->getBegin() < other->getEnd() && other->getBegin() < this->getEnd();
}

/**
* l'intervalle courant est-il inclus dans l'intervalle source
* @param timeSlot intervalle source
* @return vrai ssi this est inclus dans timeSlot
*///TODO enlever une fois que les OCP_CpTimeSlotPtr sont généralisés
bool OCP_CpTimeSlot::isInside(const OCP_CpTimeSlot* timeSlot) const
{
    return this->getBegin() >=  timeSlot->getBegin() && this->getEnd()  <= timeSlot->getEnd();
}

/**
* l'intervalle courant est-il inclus dans l'intervalle source
* @param timeSlot intervalle source
* @return vrai ssi this est inclus dans timeSlot
*/
bool OCP_CpTimeSlot::isInside(OCP_CpTimeSlotPtr timeSlot) const
{
    return this->getBegin() >=  timeSlot->getBegin() && this->getEnd()  <= timeSlot->getEnd();
}

/**
* renvoie l'intersection de deux intervalles
* @param timeSlot intervalle à intersecter
* @return l'intervalle d'intersection entre this et timeSlot
*/
OCP_CpTimeSlot OCP_CpTimeSlot::getIntersection(const OCP_CpTimeSlot& timeSlot) const throw(OCP_XRoot)
{
    if (this->intersect(&timeSlot))
    {
        return OCP_CpTimeSlot(max(getBegin(),timeSlot.getBegin()) , min(getEnd(),timeSlot.getEnd()));
    }
    else
    {
        boost::format errMsg = boost::format( _("Intersection impossible entre %5$s et %6$s [%1$d, %2$d) et [%3$d, %4$d) "))
        % getFullName() % timeSlot.getFullName() % getBegin() % getEnd() % timeSlot.getBegin() % timeSlot.getEnd();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg );
        throw OCP_XRoot(errMsg.str());
    }
}

/**
 * Durée d'intersection entre deux intervalles semi-ouverts [a,b) et [a',b')
 * @param other : intervalle à intersecter
 * @return : la durée d'intersection (0 si pas d'intersection), sinon min(b,b') - max(a,a')
 *///TODO à supprimer
OCP_CpDate OCP_CpTimeSlot::getIntersectionDuration(OCP_CpTimeSlot* other) const
{
	int answer = 0;
	if(this->getEnd() <= other->getBegin() || other->getEnd() <= this->getBegin())
		return answer;

	int a = this->getBegin();
	int b = this->getEnd();
	if(this->getBegin() < other->getBegin()) 
		a = other->getBegin();
	if(this->getEnd() > other->getEnd()) 
		b = other->getEnd();

	answer = (b-a);
	return answer;
}

/**
 * Durée d'intersection entre deux intervalles semi-ouverts [a,b) et [a',b')
 * @param other : intervalle à intersecter
 * @return : la durée d'intersection (0 si pas d'intersection), sinon min(b,b') - max(a,a')
 */
OCP_CpDate OCP_CpTimeSlot::getIntersectionDuration(OCP_CpTimeSlotPtr other) const
{
    int answer = 0;
    if(this->getEnd() <= other->getBegin() || other->getEnd() <= this->getBegin())
        return answer;

    int a = this->getBegin();
    int b = this->getEnd();
    if(this->getBegin() < other->getBegin())
        a = other->getBegin();
    if(this->getEnd() > other->getEnd())
        b = other->getEnd();

    answer = (b-a);
    return answer;
}


/**
 * renvoie la durée d'intersection avec deux intervalles qui s'intersectent
 * @param itv1 premier intervalle (doit intersecter this)
 * @param itv2 second intervalle (doit intersecter this et itv1)
 * @return la durée d'intersection des 3 intervalles
 */
OCP_CpDate OCP_CpTimeSlot::getIntersectionDuration(boost::shared_ptr < OCP_CpTimeSlot > itv1 , boost::shared_ptr < OCP_CpTimeSlot > itv2) const throw(OCP_XRoot)
{
    /*
     * note : pour des intervalles temporels A inter B , A inter C et B inter C implique A inter B inter C
     */
    if ( this->intersect(itv1) && this->intersect(itv2) && itv1->intersect(itv2) )
    {
        OCP_CpTimeSlot itv3(max(getBegin(),itv1->getBegin()) , min(getEnd(),itv1->getEnd()));
        OCP_CpTimeSlot itv4(max(itv3.getBegin(),itv2->getBegin()) , min(itv3.getEnd(),itv2->getEnd()));
        return itv4.getDuration();
    }
    else
    {
        boost::format errMsg = boost::format( _("Intersection impossible entre les 3 intervalles [%1$d, %2$d) [%3$d, %4$d) et [%5$d, %6$d)  "))
        % getBegin() % getEnd() % itv1->getBegin() % itv1->getEnd() % itv2->getBegin() % itv2->getEnd();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg );
        throw OCP_XRoot(errMsg.str());
    }
}

/**
* modifie l'intervalle courant de manière à ce qu'il couvre l'intervalle source
* @param source intervalle qui permettra d'allonger l'intervalle courant
*/
void OCP_CpTimeSlot::enlarge(const OCP_CpTimeSlot& source)
{

    if (source.getBegin() < getBegin() )
        setBegin(source.getBegin());

    if (source.getEnd() > getEnd())
        setEnd( source.getEnd() );
}

/**
* debut de l'intervalle. Erreur si non renseigné
* @return debut de l'intervalle
*/
OCP_CpDate OCP_CpTimeSlot::getBegin() const
{
    if (_begin <0)
    {
        boost::format errMsg = boost::format( _("Impossible d'accèder au début d'intervalle : pour un créneau satellite, vérifier qu'une contrainte SAT4 est positionnée. \n créneau concerné : %1$s "  ))
            % getFullName().str();
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg );
        throw OCP_XRoot(errMsg.str());
    }
    return _begin;
}

/**
 * fin de l'intervalle. Erreur si non renseigné
 * @return fin de l'intervalle
 */
OCP_CpDate OCP_CpTimeSlot::getEnd() const
{
    if(_end <0)
    {
        boost::format errMsg = boost::format( _("Impossible d'accèder à la fin d'intervalle : pour un créneau satellite, vérifier qu'une contrainte SAT4 est positionnée. \n créneau concerné : %1$s "  ))
        % getFullName().str() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg );
        throw OCP_XRoot(errMsg.str());
    }

    return _end;
}



/**
 * indique que le slot ne pourra être retenu comme possible dans le calcul
 * lance une exception si le slot était initialement reservé
 */
void OCP_CpTimeSlot::setToRemove()
{
    if (_reserved)
    {
        boost::format msg = boost::format( _("%1$s doit être supprimé alors qu'il est réservé") )
                    % getFullName().str() ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
         throw OCP_XRoot(msg.str());

    }
    else
        _toRemove = true;
}

/**
 * affichage du slot avec info satellite , station, debut, fin
 * @return affichage complet
 */
boost::format OCP_CpTimeSlot::getFullName() const
{
    PHRDate aBegin(getBegin() );
    PHRDate aEnd(getEnd() );
    boost::format msg = boost::format( _("%1$s debut %2$s fin %3$s") ) % getName() % aBegin.getStringValue() % aEnd.getStringValue() ;
    return msg;
}

/**
 * predicat de comparaison par début pour deux passages
 * @param t1
 * @param t2
 * @return
 */
bool OCP_CpTimeSlot::slotsBeginSortPredicate(OCP_CpTimeSlot* t1, OCP_CpTimeSlot* t2)
{
  return t1->getBegin() < t2->getBegin();
}

/**
 * predicat de comparaison par fin pour deux passages
 * @param t1
 * @param t2
 * @return
 */
bool OCP_CpTimeSlot::slotsEndSortPredicate(OCP_CpTimeSlot* t1, OCP_CpTimeSlot* t2)
{
  return t1->getEnd() < t2->getEnd();
}


/**
 * predicat de comparaison par fin pour deux passages
 * @param t1
 * @param t2
 * @return
 */
bool OCP_CpTimeSlot::slotsReverseEndSortPredicate(OCP_CpTimeSlot* t1, OCP_CpTimeSlot* t2)
{
  return t1->getEnd() > t2->getEnd();
}

/**
* notification que le créneau est candidat pour une contrainte
* @param aCtr la contrainte ayant ce créneau comme candidat
*/
void OCP_CpTimeSlot::addOcpConstraint(boost::shared_ptr < OCP_CpConstraint > aCtr)
{
    _cpConstraints.push_back(aCtr);
}
