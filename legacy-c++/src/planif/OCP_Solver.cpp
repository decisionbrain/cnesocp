/*
 * $Id: OCP_Solver.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

#include "planif/OCP_Solver.h"

using namespace ocp;
using namespace std;

namespace ocp {
namespace solver {


/**
 * Constructeur par défaut
 * @return Retourne un objet de type OcpSolver
 */
OcpSolver::OcpSolver() {
    // TODO Auto-generated constructor stub
}

/**
 * Destructeur par défaut
 * @return
 */
OcpSolver::~OcpSolver() {
    // TODO Auto-generated destructor stub
}


}  // namespace solver
}  // namespace ocp
