/*
 * $Id: OCP_CpCumulTimeSlot.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include <list>
#include <iostream>

#include "OCP_CpDate.h"
#include "planif/OCP_CpCumulTimeSlot.h"
#include "planif/OCP_Solver.h"

using namespace std;
using namespace ocp::commons;


/**
 * constructeur de la structure réversible de timetable
 * @param capacityMin capacité min de la timetable (0 si non renseignée)
 * @param capacityMax capacité max de la timetable (-1 si non renseignée)
 * @return instance de timetable
 */
OCP_CpCumulTimeSlot::OCP_CpCumulTimeSlot(int capacityMin , int capacityMax)
:_capacityMin(capacityMin),_capacityMax(capacityMax),_maxHole(-1)
 {
    if (_capacityMin > _capacityMax || _capacityMin<0)
    {
        boost::format msg = boost::format( _("Capacités incorrectes (%1$d,%2$d) pour la timetable ") )
        %  _capacityMin % _capacityMax;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg);
        throw OCP_XRoot( msg.str() );
    }
 };

/**
 * traitement de la partie "droite" de l'intersection de deux intervalles
 * @param tsCAND intervalle candidat
 * @param tsREF intervalle de référence
 * @return l'intervalle allant de la fin la plus tôt à la fin la plus tard
 */
OCP_CpTimeSlotPtr OCP_CpCumulTimeSlot::getForwardTimeSlot(OCP_CpTimeSlotPtr tsCAND, OCP_CpTimeSlotPtr tsREF)
{
    OCP_CpTimeSlotPtr rep = OCP_CpTimeSlotPtr();
    if((tsCAND->getEnd() > tsREF->getEnd()) && (tsCAND->getBegin() < tsREF->getEnd()))
    {//intersection avec candidat finissant le plus tard
        rep = OCP_CpTimeSlotPtr (new OCP_CpTimeSlot(tsREF->getEnd(), tsCAND->getEnd()) );
    }
    else //intersection avec référence finissant le plus tard : le nouvel intervalle prend la capacité de la référence
    {
        if((tsREF->getEnd() > tsCAND->getEnd()) && (tsREF->getBegin() < tsCAND->getEnd()))
        {
            rep = OCP_CpTimeSlotPtr (new OCP_CpTimeSlot(tsCAND->getEnd(), tsREF->getEnd()) );
            rep->setCapacity(tsREF->getCapacity());
        }
    }

    return rep;
}

/**
 * traitement de la partie "gauche" de l'intersection de deux intervalles
 * @param tsCAND intervalle candidat
 * @param tsREF intervalle de référence
 * @return l'intervalle allant du début le plus tôt au début le plus tard
 */
OCP_CpTimeSlotPtr OCP_CpCumulTimeSlot::getBackwardTimeSlot(OCP_CpTimeSlotPtr tsCAND, OCP_CpTimeSlotPtr tsREF)
{
    OCP_CpTimeSlotPtr rep = OCP_CpTimeSlotPtr();
    if((tsCAND->getBegin() < tsREF->getBegin()) && (tsCAND->getEnd() > tsREF->getBegin()))
    {
        rep = OCP_CpTimeSlotPtr(new OCP_CpTimeSlot(tsCAND->getBegin(), tsREF->getBegin()));//intersection avec candidat commençant avant
    }
    else //intersection avec référence commençant avant : le nouvel intervalle prend la capacité de la référence
    {
        if((tsREF->getBegin() < tsCAND->getBegin()) && (tsREF->getEnd() > tsCAND->getBegin()))
        {
            rep = OCP_CpTimeSlotPtr(new OCP_CpTimeSlot(tsREF->getBegin(), tsCAND->getBegin()));
            rep->setCapacity(tsREF->getCapacity());
        }
    }
    return rep;
}

/**
 * intervalle d'intersection entre deux intervalles qui s'intersectent, sans calcul de capacité sur l'intervalle retourné
 * @param tsCAND intervalle candidat
 * @param tsREF intervalle de référence
 * @return intervalle d'intersection entre le candidat et la référence
 */
OCP_CpTimeSlotPtr OCP_CpCumulTimeSlot::getIntersectTimeSlot(OCP_CpTimeSlotPtr tsCAND, OCP_CpTimeSlotPtr tsREF)
{
    OCP_CpDate begin = tsCAND->getBegin();
    OCP_CpDate end = tsCAND->getEnd();
    if(begin < tsREF->getBegin()) begin = tsREF->getBegin();
    if(end > tsREF->getEnd()) end = tsREF->getEnd();
    OCP_CpTimeSlotPtr rep = OCP_CpTimeSlotPtr (new OCP_CpTimeSlot(begin, end) );
    return rep;
}

/**
* affichage de tous les intervalles de la timetable
*/
void OCP_CpCumulTimeSlot::showCapacitySlots()
{
    cout << "LIST capacity_max = " << getCapacityMax() << endl;
    TTList::iterator iterTTSlot;
    for(iterTTSlot = _timeTable.begin(); iterTTSlot!=_timeTable.end(); iterTTSlot++)
    {
        OCP_CpTimeSlotPtr tsREF = (*iterTTSlot);
        cout << "[ " << tsREF->getBegin() << " , " << tsREF->getEnd() << " ] ( " << tsREF->getCapacity() << " )" << endl;
    }
}

/**
* ajout d'un intervalle temporel et modification de la timetable
* @param tsCAND nouvel intervalle temporel
* @return vrai ssi la capacitée mise à jour ne dépasse pas strictement la capacité max
*/
bool OCP_CpCumulTimeSlot::addCapacity(OCP_CpTimeSlotPtr tsCAND)
{
    bool rep = true;
    OCP_CpTimeSlotPtr tsREF = OCP_CpTimeSlotPtr();//slot courant de la timetable
    if(_timeTable.size() > 0)
    {
        TTList::iterator iterTTSlot;
        for(iterTTSlot = _timeTable.begin(); iterTTSlot!=_timeTable.end(); iterTTSlot++)
        {
            tsREF = (*iterTTSlot);
            // must exist an intersection
            if(tsCAND->intersect(tsREF))
            {
                if(tsREF->getCapacity() < _capacityMax)
                {
                    //compute all before because REF is changing during insertion!
                    OCP_CpTimeSlotPtr tmp1 = getBackwardTimeSlot(tsCAND, tsREF);//nouvel intervalle "à gauche"
                    OCP_CpTimeSlotPtr tmp2 = getIntersectTimeSlot(tsCAND, tsREF);//intervalle "intersection"
                    OCP_CpTimeSlotPtr tmp3 = getForwardTimeSlot(tsCAND, tsREF);//nouvel intervalle "à droite"

                    if(tmp1 != 0)
                        _timeTable.insert(iterTTSlot, tmp1);//insertion en tête de liste

                    // modifier tsREF à l'intersection : incrément de la capacité et début/fin
                    if(tmp2 != 0)
                    {
                        tsREF->setBegin(tmp2->getBegin());
                        tsREF->setEnd(tmp2->getEnd());
                        tsREF->increaseCapacity();
                    }
                    if(tmp3 != 0)
                        rep = addCapacity(tmp3);//ajout de l'intervalle à droite : il faut continuer sur la suite de la liste
                }
                else
                {
                    // dépassement de capacité si l'on réalisait l'ajout : fail
                    rep = false;
                    break;
                }
            }
            else
                // insertion en tête de liste s'il n'intersecte pas le premier de la liste
                if(tsCAND->getEnd() <= tsREF->getBegin())
                {
                    _timeTable.insert(iterTTSlot, tsCAND);
                    break;
                }
        }
        // insertion après le dernier (si on n'a vu aucune intersection)
        if(tsCAND->getBegin() >= tsREF->getEnd())
            _timeTable.insert(iterTTSlot, tsCAND);
    }
    else
    {
        // insertion en tête quand la liste est vide
        _timeTable.insert(_timeTable.begin(), tsCAND);
    }
    return rep;

}


/**
* retrait d'un intervalle temporel et modification de la timetable
* @param tsCAND nouvel intervalle temporel
* @return vrai ssi la capacitée mise à jour reste supérieure ou égale à la capacité min
*/
bool OCP_CpCumulTimeSlot::decreaseCapacity(OCP_CpTimeSlotPtr tsCAND)
{
    bool rep = true;
    OCP_CpTimeSlotPtr tsREF = OCP_CpTimeSlotPtr();//slot courant de la timetable
    if(_timeTable.size() > 0)
    {
        TTList::iterator iterTTSlot;
        for(iterTTSlot = _timeTable.begin(); iterTTSlot!=_timeTable.end(); iterTTSlot++)
        {
            tsREF = (*iterTTSlot);
            // must exist an intersection
            if(tsCAND->intersect(tsREF))
            {
                if(tsREF->getCapacity() > _capacityMin)
                {
                    //compute all before because REF is changing during insertion!
                    OCP_CpTimeSlotPtr tmp1 = getBackwardTimeSlot(tsCAND, tsREF);//nouvel intervalle "à gauche"
                    OCP_CpTimeSlotPtr tmp2 = getIntersectTimeSlot(tsCAND, tsREF);//intervalle "intersection"
                    OCP_CpTimeSlotPtr tmp3 = getForwardTimeSlot(tsCAND, tsREF);//nouvel intervalle "à droite"

                    if(tmp1 != 0)
                        return false;//impossible de soustraire un intervalle dans une zone vide

                    // modifier tsREF à l'intersection : décrément de la capacité et début/fin
                    if(tmp2 != 0)
                    {
                        tsREF->setBegin(tmp2->getBegin());
                        tsREF->setEnd(tmp2->getEnd());
                        tsREF->decreaseCapacity();
                        //if ( _maxHole>0 && tsREF->getCapacity()==0 && computeLocalHole(iterTTSlot)>_maxHole )
                        //    return false;//trou d'amplitude supérieur au seuil maximal fixé
                    }
                    if(tmp3 != 0)
                        rep = decreaseCapacity(tmp3);//ajout de l'intervalle à droite : il faut continuer sur la suite de la liste
                }
                else
                {
                    // on passerait sous le seuil de capacité min si l'on enlevait le candidat : fail
                    rep = false;
                    break;
                }
            }
            else
                // insertion en tête de liste s'il n'intersecte pas le premier de la liste
                if(tsCAND->getEnd() <= tsREF->getBegin())
                    return false;//impossible de soustraire un intervalle dans une zone vide
        }
        // insertion après le dernier (si on n'a vu aucune intersection)
        if(tsCAND->getBegin() >= tsREF->getEnd())
            return false;//impossible de soustraire un intervalle dans une zone vide
    }
    else
    {
        // insertion en tête quand la liste est vide
        return false;//impossible de soustraire un intervalle dans une zone vide
    }
    return rep;

}

/**
 * pour un nouvel intervalle de capacité nulle, parcourir les autres intervalles de capacité nulle autour de cet intervalle
 * @param current le nouvel intervalle de capacité nulle
 * @return la somme des durées des intervalles de durée nulle autour de l'intervalle courant (y compris celui-ci)
 */
int OCP_CpCumulTimeSlot::computeLocalHole(TTList::iterator current)
{
    int hole = 0;
    /*
     * regarder à droite de l'intervalle courant
     */
    for(TTList::iterator iterTTSlot = current; iterTTSlot!=_timeTable.end() && (*iterTTSlot)->getCapacity()==0; iterTTSlot++)
        hole+=(*iterTTSlot)->getDuration();

    /*
     * regarder à gauche de l'élément courant
     */
    if(current != _timeTable.begin()) //sinon premier de la liste, déjà comptabilisé
    {
        TTList::iterator iterTTSlot = current;
        do
        {
            iterTTSlot--;
            if ( (*iterTTSlot)->getCapacity()==0 )
                hole+=(*iterTTSlot)->getDuration();
        }
        while(iterTTSlot!=_timeTable.begin() && (*iterTTSlot)->getCapacity()==0 );
    }
    return hole;
}

/**
* calcule l'intervalle fournissant le niveau (capacité) minimum/maximum parmi tous les intervalles
* @return l'intervalle fournissant le niveau (capacité) minimum/maximum parmi tous les intervalles
*/
OCP_CpTimeSlot OCP_CpCumulTimeSlot::getMinMaxLevels() const
{

    int aMin = -1;//capacité min
    int aMax = 0;

    for(TTList::const_iterator iterTTSlot = _timeTable.begin(); iterTTSlot!=_timeTable.end(); iterTTSlot++)
    {
        OCP_CpTimeSlotPtr tsREF = (*iterTTSlot);
        int capa = tsREF->getCapacity();
        if (capa > aMax)
            aMax = capa;
        if (aMin == -1 || capa < aMin)
            aMin = capa;
    }
    if (aMin == -1)//pas de slot rencontrés
        aMin = 0;
    return OCP_CpTimeSlot( aMin , aMax);
}
