/*
 * $Id: OCP_CpCstIntTimetable.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#include "planif/OCP_CpCstIntTimetable.h"
#include "planif/OCP_Solver.h"

using namespace ocp::commons;

//**************************************************************************
//********************************** table ********************************
//**************************************************************************

OCP_CpCstIntTimetableI::OCP_CpCstIntTimetableI(IloEnv env,IloInt timeMin, IloInt timeMax,
										   IloInt defaultValue):
_timeMin(timeMin),
_timeMax(timeMax),
_env(env)
{
	m_head = new(env) OCP_CpMaillon(defaultValue,_timeMin,_timeMax);
}

//*********************
OCP_CpCstIntTimetableI::OCP_CpCstIntTimetableI(IloEnv env,OCP_CpTimeSlot* interval,
										   IloInt defaultValue):
_timeMin(interval->getBegin()),
_timeMax(interval->getEnd()),
_env(env)
{
	m_head = new(env) OCP_CpMaillon(defaultValue,_timeMin,_timeMax);
}

//*********************
/**
 * premier maillon dont la fin est strictement supérieure à la date demandée
 * @param time date demandée
 * @return le premier maillon dont la fin est strictement supérieure à la date demandée
 */
OCP_CpMaillon* OCP_CpCstIntTimetableI::getFirstElement(IloInt time)
{
	OCP_CpMaillon* p;
	for (p=m_head; p!=NULL && p->_endTime<=time; p=p->_next);
	return p;
}

//*********************
void OCP_CpCstIntTimetableI::insert_befor(OCP_CpMaillon* p1, OCP_CpMaillon* p)
{
	p->_next=p1;
	p->_previous=p1->_previous;
	p1->_previous=p;
	if (p->_previous==NULL) // insertion en t�te
		m_head=p;
	else p->_previous->_next=p;
}

//*********************
void OCP_CpCstIntTimetableI::insert_after(OCP_CpMaillon* p1, OCP_CpMaillon* p)
{
	p->_previous=p1;
	p->_next=p1->_next;
	p1->_next=p;
	if (p->_next!=NULL)
		p->_next->_previous=p;
}

//*********************
void OCP_CpCstIntTimetableI::supress(OCP_CpMaillon* p)
{
	OCP_CpMaillon* prev=p->_previous;
	OCP_CpMaillon* next=p->_next;
	if (prev!=NULL) prev->_next=next;
	if (next!=NULL) next->_previous=prev;
}

//*********************
void OCP_CpCstIntTimetableI::setValue(IloInt start,IloInt end, IloInt val)
{
	assert(start>=_timeMin && end<=_timeMax);
	OCP_CpMaillon* p1=getFirstElement(start);
	for (OCP_CpMaillon* p=p1; p!=NULL && p->_startTime<end; p=p->_next)
	{
		if (p->_val==val) continue;
		if (p->_startTime<start) // cr�er un maillon avant
		{
			OCP_CpMaillon* newP= new(_env) OCP_CpMaillon(p->_val,p->_startTime,start);
			insert_befor(p,newP);
			p->_startTime=start;
		}
		if (p->_endTime>end) // cr�er un maillon apr�s
		{
			OCP_CpMaillon* newP= new(_env) OCP_CpMaillon(p->_val,end,p->_endTime);
			insert_after(p,newP);
			p->_endTime=end;
		}
		OCP_CpMaillon* prev=p->_previous;
		if (prev!=NULL && prev->_val==val)
		{
			prev->_endTime=p->_endTime;
			supress(p);
			p=prev;
		}
		else p->_val=val;
	}

/*	// fusionner avec le pr�c�dent si m�me valeur 
// !! ne marche pas avec add et substract car l'interval du curseur est alors modifi�
	if (p!=NULL)
	{
		OCP_CpMaillon* prev=p->_previous;
		if (prev!=NULL && prev->_val==p->_val)
		{
			prev->_endTime=p->_endTime;
			supress(p);
		}
	}*/

}

/**
* décrément de la timetable sur une plage
* @param timeMin début de plage
* @param timeMax fin de plage
* @param val incrément sur toute la plage
* @return renvoie le niveau le plus bas sur la plage après modification
*/
IloInt OCP_CpCstIntTimetableI::substract(IloInt timeMin, IloInt timeMax, IloInt val)
{
	OCP_CpCstIntTimetableCursor iter(this,timeMin);
	bool minFound = false;
	IloInt min = 0;
	while (iter.ok())
	{
		IloInt newVal=iter.getValue()-val;
		if(!minFound)
		{
		    minFound = true;
		    min = newVal;
		}
		else if(newVal<min)
		{
		    min = newVal;
		}
		IloInt start=IlcMax(iter.getTimeMin(),timeMin);
		IloInt end=IlcMin(iter.getTimeMax(),timeMax);
		setValue(start,end,newVal);
		if (end==timeMax) break;
		++iter;
	}
	return min;
}

/**
 * incrément de la timetable sur une plage
 * @param timeMin début de plage
 * @param timeMax fin de plage
 * @param val incrément sur toute la plage
 * @return renvoie le niveau le plus haut sur la plage après modification
 */
IloInt OCP_CpCstIntTimetableI::add(IloInt timeMin, IloInt timeMax, IloInt val)
{
    bool maxFound = false;
    IloInt max = 0;
	OCP_CpCstIntTimetableCursor iter(this,timeMin);
	while (iter.ok())
	{
		IloInt newVal=iter.getValue()+val;
		if(!maxFound)
		{
		    maxFound = true;
		    max = newVal;
		}
		else if(newVal>max)
		{
		    max = newVal;
		}
		IloInt start=IlcMax(iter.getTimeMin(),timeMin);
		IloInt end=IlcMin(iter.getTimeMax(),timeMax);
		setValue(start,end,newVal);
		if (end==timeMax) break;
		++iter;
	}
	return max;
}
//**************************************************************************
void OCP_CpCstIntTimetable::display()
{
	OCP_CpCstIntTimetableCursor curs(*this, getTimeMin());
	for (;curs.ok();++curs)
	{
	    PHRDate aBegin(curs.getTimeMin() , PHRDate::_TimeFormat);
	    PHRDate aEnd(curs.getTimeMax() , PHRDate::_TimeFormat);

	    boost::format msg = boost::format(_("valeur %1$d dans [%2$s,%3$s] [%4$d,%5$d] "))
	                 %  curs.getValue()
	                 % aBegin.getStringValue()
	                 % aEnd.getStringValue()
	                 % curs.getTimeMin()
	                 % curs.getTimeMax();

	    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);
	}
}

/**
 * accès à la valeur du maillon le plus haut de la time table entre deux dates
 * @param a début de plage
 * @param b fin de plage
 * @return la valeur du maillon le plus haut de la time table entre deux dates
 */
IloInt OCP_CpCstIntTimetable::getMax(IloInt a, IloInt b)
{
	OCP_CpCstIntTimetableCursor curs(*this, a);
	assert(curs.ok()); 
	IloInt max=curs.getValue();
	for (;curs.ok();++curs)
	{
		if (curs.getTimeMin()>=b) break;
		max=IlcMax(max,curs.getValue()); 
	}
	return max;
}

/**
 * accès à la valeur du maillon le plus bas de la time table entre deux dates
 * @param a début de plage
 * @param b fin de plage
 * @return la valeur du maillon le plus bas de la time table entre deux dates
 */
IloInt OCP_CpCstIntTimetable::getMin(IloInt a, IloInt b)
{
	OCP_CpCstIntTimetableCursor curs(*this, a);
	assert(curs.ok()); 
	IloInt min=curs.getValue();
	for (;curs.ok();++curs)
	{
		if (curs.getTimeMin()>=b) break;
		min=IlcMin(min,curs.getValue()); 
	}
	return min;
}

//**************************************************************************
IloInt OCP_CpCstIntTimetable::getSum(IloInt a, IloInt b, IloInt sign)
{
	OCP_CpCstIntTimetableCursor curs(*this, a);
	assert(curs.ok()); 
	IloInt sum=0;
	for (;curs.ok();++curs)
	{
		if (curs.getTimeMin()>=b) break;
		IloInt val=curs.getValue();
		if (!sign || sign*val>0) 
			sum+=val*(IlcMin(b,curs.getTimeMax())-IlcMax(a,curs.getTimeMin()));
	}
	return sum;
}

//**************************************************************************
//********************************** Cursor ********************************
//**************************************************************************

OCP_CpCstIntTimetableCursor::OCP_CpCstIntTimetableCursor(OCP_CpCstIntTimetable table,IloInt time):
_table(table)
{
	if (time<table.getTimeMin() || time>=table.getTimeMax())
	{	_ok=IlcFalse; _current=NULL; }
	else
	{
		_ok=IlcTrue;
		_current=_table.getImpl()->getFirstElement(time);
		_val=_current->_val;
		_timeMin=_current->_startTime;
		_timeMax=_current->_endTime;
	}
}

OCP_CpCstIntTimetableCursor::~OCP_CpCstIntTimetableCursor()
{
}

void OCP_CpCstIntTimetableCursor::operator++()
{
	_current=_current->_next;
	_ok=(_current!=NULL);
	if (_ok)
	{
		_val=_current->_val;
		_timeMin=_current->_startTime;
		_timeMax=_current->_endTime;
	}
}
