/*
 * $Id: main.cpp 943 2010-12-21 13:12:51Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>


#include "context/OCP_CpModel.h"
#include "planif/OCP_Solver.h"
#include "OCP_CpManager.h"


#include <Commons.h>
#include <OcpPeriod.h>
#include <OcpXmlTypes.h>
#include <OcpParameters.h>

#include <PHRDate.h>


#include <CommonsIo.h>
#include <OcpParameters.h>
#include <OcpLogManager.h>
#include <OcpLogBookManager.h>
#include <ConfAccel.h>

#ifdef ILM_GENIGRAPH_LICENSE
    #include "ilmmanager.h"
#endif


using namespace std;
using namespace ocp::commons::io;
using namespace boost;
using namespace ocp;
using namespace ocp::solver;


class OcpSolver;

/**
 * Initialisation du singleton de la classe OCP_CpModel à NULL
 */
OCP_CpModel* OCP_CpModel::_cpModel=NULL;
bool OCP_CpModel::instanceFlag = false;
bool OCP_CpModel::_loadAllResources = true;


/**
 * Initialisation des champs statiques du manager
 */
OCP_CpManager* OCP_CpManager::cpManager = NULL;
bool OCP_CpManager::instanceFlag = false;



/**
 * Le DoIt do Solver
 *
 * @param argc Le nombre d'argument sur la ligne de commande
 * @param argv tableau de chaîne de caractère contenant chaque élément de la ligne de commande
 * @return Code de sortie du programme. Valeur EXIT_FAILURE en cas d'erreur ou EXIT_SUCESS si réussi.
 *
 */
int main (int argc, char *argv[])
{
    //artf675030 : traitement des fuites memoires
    //SUP_ScopeLogFunc();

    string dataFile, paramFile, outputFile;
    string logProperties;
    int result = EXIT_SUCCESS;



    /** ============================================================= */
    /** Partie NON modifiable du main */

    /**
     * Initialisation des différents librairies "core" du solver.
     */
    /* Initialisation du logueur */
    if (argc == 4)
    {
       /* Configuration du logger en utilisant le configurateur de base de log4cxx (console output) */
        logProperties = "default";
        OcpLogManager::initInstance();
        dataFile = argv[1];
        paramFile = argv[2];
        outputFile = argv[3];

    }
    else if (argc == 5)
    {
        logProperties = argv[1];
        /* Si l'argument argv[1] est égal à --noLogProperties alors on ne modifie pas le PropertyConfigurator */
        if ( logProperties.compare("--noLogProperties") != 0 )
        {
            try
            {
                /* Configuration du logger en utilisant un configurateur défini dans un fichier */
                OcpLogManager::initInstance( logProperties );
            }
            catch (OcpLogException& err)
            {
                OcpLogManager::initInstance();
                OcpLogManager::getInstance()->error(loggerInst, boost::format(_("Wrong log configuration file (%1$s). Using default configuration.")) % err.getMessage());
            }
        }
        dataFile = argv[2];
        paramFile = argv[3];
        outputFile = argv[4];
    }
    else
    {
        boost::format logMsg = boost::format(_("Nombre d'argument incorrect. syntaxe : %1$s [log.properties] <input.xml> <param.xml> <output.xml>")) % argv[0];
        OcpLogBookManager::getInstance()->addMessage(OcpLogBookManager::ALARM, logMsg);
        OcpLogManager::getInstance()->error(loggerInst, logMsg);
        return EXIT_FAILURE;
    }

    /* Démarrage de l'externalisation des chaînes de caractères */
    StrExtern::initExtern();
    OcpLogManager::getInstance()->debug( stringify(ocp::solver::main), _("Démarrage du solver") );

    /* Mise en place de la gestion des signaux */
    struct sigaction action;
    sigset_t newMask;
    memset(&action, 0, sizeof(action));

    sigemptyset(&newMask);
    sigaddset(&newMask, SIGUSR1);

    action.sa_handler = signalHandler;
    action.sa_mask = newMask;
    // On applique la gestion du signal
    if ( sigaction(SIGUSR1, &action, NULL) < 0 )
    {
        exit(EXIT_FAILURE);
    }

#ifdef ILM_GENIGRAPH_LICENSE
    OcpLogManager::getInstance()->info(loggerInst, _("registerSolverLicense..."));
    registerSolverLicense();
 #endif

    /** ============================================================= */
    /** Partie modifiable du main */

    /*Lecture du contexte*/
    try
    {
        OcpXml::getInstance()->read(dataFile, paramFile, false, false);

        OcpLogManager::getInstance()->debug(loggerInst, _("Fichiers contexte et paramétrage chargés"));

        // Exemple d'utilisation de getParameters
        OcpParametersPtr params = OcpXml::getInstance()->getParameters();
        OcpLogManager::getInstance()->debug(loggerInst, boost::format(_("Paramètres du calcul : timeout='%1$d'  -  global='%2$d'"))
                % params->getTimeout() % params->areSetsGlobal());
        OcpLogManager::getInstance()->debug(loggerInst, boost::format(_("                       horizon='[%1$s;%2$s]'"))
                % params->getHorizon()->getStart().getStringValue() % params->getHorizon()->getEnd().getStringValue());

        BOOST_FOREACH( const OcpSatParams::value_type &element, params->getSetType4EachSatellite() )
        {
            OcpLogManager::getInstance()->debug( loggerInst, boost::format(_("                       Ressource='( %1$s, %2$s )'"))
                    % element.first->getName() % OcpConstraint::getStringFrom(element.second) );
        }
        BOOST_FOREACH( const OcpStaParams::value_type &element, params->getSetType4EachStation() )
        {
            OcpLogManager::getInstance()->debug( loggerInst, boost::format(_("                       Ressource='( %1$s, %2$s )'"))
                    % element.first->getName() % OcpConstraint::getStringFrom(element.second) );
        }
        boost::format types2Use = boost::format(_("                       Types de contraintes="));
        bool firstType = true;
        BOOST_FOREACH( const VectorConstraintTypes::value_type &element, params->getSatelliteConstraintTypes2Use() )
        {
            if (firstType)
            {
                types2Use = boost::format("%1$s%2$s") % types2Use % OcpConstraint::getStringFrom(element);
                firstType = false;
            }
            else
            {
                types2Use = boost::format("%1$s, %2$s") % types2Use % OcpConstraint::getStringFrom(element);
            }
        }
        BOOST_FOREACH( const VectorConstraintTypes::value_type &element, params->getStationConstraintTypes2Use() )
        {
            if (firstType)
            {
                types2Use = boost::format("%1$s%2$s") % types2Use % OcpConstraint::getStringFrom(element);
                firstType = false;
            }
            else
            {
                types2Use = boost::format("%1$s, %2$s") % types2Use % OcpConstraint::getStringFrom(element);
            }
        }
        OcpLogManager::getInstance()->debug( loggerInst, boost::format("%1$s.") % types2Use );

        if ( (params->areSetsGlobal()) && (params->getSetTypes2Use().size() <=0) )
            throw OCP_XRoot(" paramètres globaux non renseignés ");

        OCP_CpModel::getInstance()->pilot(params);
        /* Ecriture du contexte */
        OcpXml::getInstance()->write(outputFile);

        OcpLogManager::getInstance()->info(loggerInst, _("Le solver a terminé son travail"));
        return result;

    }
    catch (OcpDataAccessException& err)
    {
        OcpLogManager::getInstance()->error(loggerInst,
                boost::format(_("Une erreur s'est produite lors de l'accès aux données XML en entrée ou sortie (à la lecture ou à l'écriture). Raison: %1$s")) % err.what());
        exit(EXIT_FAILURE);
    }
    catch (OcpXMLInvalidField& err)
    {
        OcpLogManager::getInstance()->error(loggerInst,
                boost::format(_("Erreur lors de la lecture des paramètres du calcul. Raison: %1$s")) % err.what());
        exit(EXIT_FAILURE);
    }
    catch(std::exception& err)
    {
        OcpLogManager::getInstance()->error(loggerInst,
                        boost::format(_("Une erreur inconnue s'est produite. Raison: %1$s")) % err.what());
        exit(EXIT_FAILURE);
    }


}
