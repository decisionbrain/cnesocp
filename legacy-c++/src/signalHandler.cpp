/*
 * $Id: signalHandler.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */ 

#include <boost/format.hpp>

#include <iostream>
#include <string>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#include <Commons.h>

#include "context/OCP_CpModel.h"


using namespace ocp;

static const std::string signalInst = "fr.cnes.ocp.solver::signalHandler";

/**
 * Gestionnaire générique des signaux
 * Signaux aujourd'hui géré :
 *   - SIGUSR1 : signal de communication interprocess standard sous les systèmes POSIX (UNIX, Linux, etc.)
 * @param sigID
 */
void signalHandler (int sigID)
{
    switch (sigID)
    {
        case SIGUSR1:

            OcpLogManager::getInstance()->info(signalInst, _("Le signal 'SIGUSR1' a été reçu. Le programme va s'arrêter..."));

            OCP_CpModel::getInstance()->onSignalHandler();

            exit(EXIT_SUCCESS);
            break;

        default:
            OcpLogManager::getInstance()->warning(signalInst, _("Un signal non géré a été reçu par le programme."));

            // Le code ci-dessous est préféré car il est plus robuste à l'externalisation des chaînes de caractère
            // plutôt que d'utiliser l'opérateur '+' de concaténation des chaînes.
            boost::format logMsg = boost::format( _(" --> information complémentaire : le signal est '%1$d'") ) % sigID;
            OcpLogManager::getInstance()->warning(signalInst, logMsg);

            exit(EXIT_FAILURE);
            break;
    }

}

