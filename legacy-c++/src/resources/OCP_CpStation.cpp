/*
 * $Id: OCP_CpStation.cpp 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStation.cpp
 *
 */
#include <string>
#include <boost/shared_ptr.hpp>

#include "context/OCP_CpObject.h"
#include "context/OCP_CpModel.h"
#include "resources/OCP_CpStation.h"
#include "constraints/OCP_CpInternalConstraint.h"
#include <boost/format.hpp>
#include "planif/OCP_CpMaintenanceSlot.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpCstIntTimetable.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpStation);

/**
 * constructeur avec pointeur sur l'objet métier origine
 * @param station_ la station origine dans le modèle métier
 * @param scheduled : la ressource est à planifier par le moteur
 * @return
 */
OCP_CpStation::OCP_CpStation(OcpStationPtr station_, bool scheduled) :
    _station(station_),
    OCP_CpTrackingResource(station_->getName(),scheduled)
{
    OCP_CpModel* cpModel = OCP_CpModel::getInstance();
    IloEnv env = cpModel->getEnv();

    int bound = -cpModel->getOrigin()+ cpModel->getHorizon() ;

    boost::format logMsg = boost::format(_("Maintenance Duration variable for station  %1$s borne supérieure %2$d"))
        % station_->getName() % bound ;

    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, logMsg  );

    _maintenanceDuration = IloIntVar(env, 0 , bound , logMsg.str().c_str());
    _maintenanceSlots = IloAnyArray(env);
    _maintenanceSlotsWithBookingOrders = IloAnyArray(env);
    _trackingDuration = IloIntVar();//renseigné à la pose de la contrainte interne

    /*
     * affichage
     */

    FrequencyBands::FrequencyBands_ENUM frequencyBand = station_->getFrequencyBand();
    boost::format msg, prefix;


    switch(frequencyBand)
    {
        case FrequencyBands::S:
            prefix = boost::format ( _("S") );
            break;

        case FrequencyBands::X:
            prefix = boost::format ( _("X") );
            break;

        case FrequencyBands::SX:
            prefix = boost::format ( _("SX") );
            break;

        default:
            prefix = boost::format ( _("?") );
            break;
    }

    msg = boost::format( _("Nouvelle station %1$s %2$s scheduled %3$d") ) % station_->getName() % prefix % _scheduled;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());

    /*
     * affichage des jours de fermeture de station
     */
    msg = boost::format( _("Jours de fermeture de la station : ") ) ;

    std::vector <WeekDays::WeekDays_ENUM> daysOff = station_->getDaysOff();
    for(std::vector <WeekDays::WeekDays_ENUM>::iterator it = daysOff.begin(); it != daysOff.end() ; it++)
    {
        int indexDay = *it;
        switch (indexDay)
        {
            case WeekDays::SATURDAY : msg = boost::format( _("%1$s SAMEDI ") ) % msg.str();break;
            case WeekDays::SUNDAY : msg = boost::format( _("%1$s DIMANCHE ") ) % msg.str();break;
            case WeekDays::MONDAY : msg = boost::format( _("%1$s LUNDI ") ) % msg.str();break;
            case WeekDays::TUESDAY : msg = boost::format( _("%1$s MARDI ") ) % msg.str();break;
            case WeekDays::WEDNESDAY : msg = boost::format( _("%1$s MERCREDI ") ) % msg.str();break;
            case WeekDays::THURSDAY : msg = boost::format( _("%1$s JEUDI ") ) % msg.str();break;
            case WeekDays::FRIDAY : msg = boost::format( _("%1$s VENDREDI ") ) % msg.str();break;
            default:msg = boost::format( _("%1$s UNKNOWN ") ) % msg.str();break;
        }
    }
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg);

}

/**
 * rajout d'un slot de maintenance à la liste des slots de maintenance
 * @param slot nouveau slot de maintenance
 */
void OCP_CpStation::addMaintenanceSlot(OCP_CpMaintenanceSlot* slot)
{
	_maintenanceSlots.add(slot);
}
/**
* rajout d'un slot de satellite  à la liste des slots satellite
* @param trackingSlot nouveau slot de passage satellite
*/
void OCP_CpStation::addTrackingSlot(OCP_CpTrackingSlot* trackingSlot)
{
    OCP_CpTrackingResource::addTrackingSlot(trackingSlot);

}

/**
 * une fois le début fin renseigné par SAT4, on rajoute sur la grille des temps les débuts fins de tracking et maintenance
 * @param trackingSlot : le nouveau slot dont les débuts-fins (tracking et start-end) sont à prendre en compte
 */
void OCP_CpStation::addTimePoints(OCP_CpTrackingSlot* trackingSlot)
{
    OCP_CpDate origin = OCP_CpModel::getInstance()->getOrigin();
    OCP_CpDate horizon = OCP_CpModel::getInstance()->getHorizon();

    //on charge les slots hors horizon mais on ne notifie pas leurs dates hors horizon pour le calcul des slots de maintenance
    _beginTrackings.push_back( trackingSlot->getBegin());
    _endTrackings.push_back( trackingSlot->getEnd() );


    if(trackingSlot->getBegin()>=origin && trackingSlot->getBegin()<=horizon)
        _beginsForMaintenance.push_back(trackingSlot->getAmplitude()->getBegin());

    if(trackingSlot->getEnd()>=origin && trackingSlot->getEnd()<=horizon)
        _endsForMaintenance.push_back(trackingSlot->getAmplitude()->getEnd());

}

/**
 * ajout d'une nouvelle plage de maintenance potentielle [aEndForMaintenance , aBeginForMaintenance]
 * @param aEndForMaintenance : le début de la plage de maintenance potentielle
 * @param aBeginForMaintenance : la fin de la plage de maintenance potentielle
 */
void OCP_CpStation::addSpecialTimePoints(OCP_CpDate aEndForMaintenance, OCP_CpDate aBeginForMaintenance)
{
    OCP_CpDate origin = OCP_CpModel::getInstance()->getOrigin();
    OCP_CpDate horizon = OCP_CpModel::getInstance()->getHorizon();

    if(aEndForMaintenance>=origin && aEndForMaintenance<=horizon)
        _endsForMaintenance.push_back(aEndForMaintenance);

    if(aBeginForMaintenance>=origin && aBeginForMaintenance<=horizon)
        _beginsForMaintenance.push_back(aBeginForMaintenance );

}

/**
 * indique si à la date du jour, la station est fermée : pas de slots de maintenance, pas de pose de contrainte STA1 ou STA2 ce jour
 * @param origin date en secondes correspondant au début de portée jour
 * @return vrai ssi la station est fermée à la date origin
 */
bool OCP_CpStation::isClosed(OCP_CpDate origin) const
{
    PHRDate aDate(origin);
    int indexDay = aDate.getDayInWeek();
    std::vector <WeekDays::WeekDays_ENUM> daysOff = getOrigin()->getDaysOff();
    for(std::vector <WeekDays::WeekDays_ENUM>::iterator it = daysOff.begin(); it != daysOff.end() ; it++)
    {
        if (indexDay == *it)
        {

            return true;
        }
    }
    return false;
}

/**
 * indique si à la date du jour, la station est ouverte et si les jours suivants dans la semaine, elle ne l'est plus
 * @param origin date en secondes correspondant au début de portée jour
 * @return vrai ssi origin est le dernier jour d'ouverture de la station dans la semaine
 */
bool OCP_CpStation::isLastOpenDayInItsWeek(OCP_CpDate origin) const
{
    if (isClosed(origin))
        return false;//ce jour est déjà en fermeture
    else
    {
        PHRDate aDate(origin);
        int indexDay = aDate.getDayInWeek();
        if (indexDay == WeekDays::SUNDAY)
        {
            return true;//si la station est ouverte le dimanche, ce jour est forcément le dernier ouvert de la semaine
        }
        else //on vérifie si tous les jours suivants (il y en a 7-indexDay) sont tous en dayOff
        {
            int nbDaysOffAfterCurrent = 0;//nombre
            std::vector <WeekDays::WeekDays_ENUM> daysOff = getOrigin()->getDaysOff();
            for(std::vector <WeekDays::WeekDays_ENUM>::iterator it = daysOff.begin(); it != daysOff.end() ; it++)
            {
                if (indexDay < (*it) || (*it)==WeekDays::SUNDAY)//dans ce cas, ce dayoff est bien postérieur au jour courant
                {
                    nbDaysOffAfterCurrent++;
                }
            }
            return nbDaysOffAfterCurrent==7-indexDay;//tous les suivants du jour courant dans sa semaine sont des jours de fermeture
        }

    }
}

/**
 * préparation des structures de données listant les points debut fin des periodes "ouverture-fermeture" où la station
 * peut être en maintenance (présence d'opérateurs)
 * @param ofs  la liste des périodes ouverture fermeture
 * @return vrai ssi on a lieu de créer des slots de maintenance sur cette station
 */
bool OCP_CpStation::prepareMaintenanceSlots(VectorOCP_CpTimeSlot& ofs)//ceux intersectant la fenêtre en cours)
{
    static const std::string pseudoClazz = _Clazz + "::prepareMaintenanceSlots";

    if (getOcpReserveConstraint().size() == 0)//aucune contrainte réservant des créneaux de maintenance
    {
        boost::format logMsg = boost::format( _("Station %1$s : aucune contrainte OCP de type CStaType1, CStaType2 ou CStaType3 donc aucun créneau de maintenance ne sera lu ni traité par OCP pour  ") )
                                % getName() ;
        OcpLogManager::getInstance()->warning(pseudoClazz, logMsg );
        return false;
    }



    /*
     * Lecture des jours de fermeture de la station
     */
    OCP_CpDate origin = OCP_CpModel::getInstance()->getOrigin();
    OCP_CpDate horizon = OCP_CpModel::getInstance()->getHorizon() ;
    OCP_CpTimeSlot scope( origin, horizon);
    PHRDate aDate(origin);

    if(isClosed(origin))
    {

        boost::format msg = boost::format( _(" pas de slots de maintenance généré pour le jour %1$s car c'est un jour de fermeture de la station %2$s "))
        % aDate.getStringValue() % getName() ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
        return false;
    }


    /*
     * parcourir les périodes d'ouverture fermeture de la station qui intersectent l'horizon
     */

    try
    {
        VectorOcpTimePeriod allOfs = getOrigin()->getWorkingPeriods();

        for(VectorOcpTimePeriod::iterator it = allOfs.begin() ; it != allOfs.end() ; it++)
        {
            OcpTimePeriodPtr aPeriod = (*it);
            OCP_CpTimeSlot offset ( aPeriod );

            OCP_CpConstraint::buildTimeAreas(ofs,scope.getBegin(),scope.getEnd(),offset.getBegin(),offset.getEnd());
        }
    }
    catch(OcpXMLInvalidField& e)
    {
        boost::format msg = boost::format( _(" pas de périodes de maintenances renseignées : maintenances possible partout pour la station %1$s "))
        % getName() ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
        OCP_CpTimeSlotPtr tiPtr( new OCP_CpTimeSlot(scope) );
        ofs.push_back( tiPtr);
    }
    for(VectorOCP_CpTimeSlot::iterator it = ofs.begin() ; it != ofs.end() ; it++)
    {
        OCP_CpDate ofBegin = (*it)->getBegin();
        OCP_CpDate ofEnd = (*it)->getEnd();

        if(ofBegin>= scope.getBegin() && ofBegin <= scope.getEnd())
            _endsForMaintenance.push_back(ofBegin );
        if(ofEnd>= scope.getBegin() && ofEnd <= scope.getEnd())
            _beginsForMaintenance.push_back(ofEnd);

        PHRDate aofBegin(ofBegin , PHRDate::_TimeFormat);
        PHRDate aofEnd(ofEnd , PHRDate::_TimeFormat);
        boost::format msg = boost::format( _("période disponible opérateurs maintenance de la station %1$s : %2$s %3$s"))
        % getName() % aofBegin.getStringValue() % aofEnd.getStringValue() ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

    }


    int nbOfs = ofs.size();
    if(nbOfs==0)
    {
        boost::format msg = boost::format( _(" pas de slots de maintenance généré pour le jour %1$s car en dehors de période disponible pour les opérateurs de la station %2$s"))
        % aDate.getStringValue() % getName() ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
        return false;
    }
    else
    {
        _beginsForMaintenance.push_back(origin);
        _beginsForMaintenance.push_back(horizon);
        _endsForMaintenance.push_back(origin);
        _endsForMaintenance.push_back(horizon);

        std::sort(_beginsForMaintenance.begin() , _beginsForMaintenance.end() );
        std::sort(_endsForMaintenance.begin() , _endsForMaintenance.end() );



        /*
         * remove des doublons possibles
         */
        OCP_CpDate prev = -1;
        std::vector<OCP_CpDate>::iterator it = _beginsForMaintenance.begin();
        while(it!=_beginsForMaintenance.end())
        {
            OCP_CpDate current = (*it);
            if (current == prev)
                _beginsForMaintenance.erase(it);//supprimer le doublon courant, it pointe vers son successeur
            else
            {
                prev = current;//mise à jour du nouveau prédécesseur pour la boucle suivante
                ++it;
            }
        }

        prev = -1;
        std::vector<OCP_CpDate>::iterator it2 = _endsForMaintenance.begin();
        while(it2!=_endsForMaintenance.end())
        {
            OCP_CpDate current = (*it2);
            if (current == prev)
                _endsForMaintenance.erase(it2);//supprimer le doublon courant, it2 pointe vers son successeur
            else
            {
                prev = current;//mise à jour du nouveau prédécesseur pour la boucle suivante
                it2++;
            }
        }
        return true;
    }
}

/**
    * génération des slots de maintenance pour la station. On tient compte des passages satellites et des heures de dispo opérateurs
    * attention, ceci doit être appelé après avoir renseigné tous les débuts fins possibles des slots satellites défilants
    * @param ofs : liste en entrée des points d'ouverture-fermeture station
    */
void OCP_CpStation::generateMaintenanceSlots(VectorOCP_CpTimeSlot& ofs)
{

    OCP_CpDate horizon = OCP_CpModel::getInstance()->getHorizon() ;

    /*
     * Lecture de la durée minimale d'un créneau de maintenance
     */

    OCP_CpDate minMaintenanceDuration = 1;//au moins une seconde pour un créneau de maintenance
    try
    {
        minMaintenanceDuration = (OCP_CpDate) 60 * getOrigin()->getStationMinMaintenanceDuration();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst,
                boost::format( _("minMaintenanceDuration sur la station %1$s : %2$d secondes")) % getName() % minMaintenanceDuration );

    }
    catch(OcpXMLInvalidField& e )
    {
        //pas de propriétés lues, rien à faire
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst,
                boost::format( _("Erreur de lecture de minMaintenanceDuration sur la station %1$s : %2$s ")) % getName() % e.what() );

    }

    /*
     * supprimer les doublons des points de temps avant création des intervalles de maintenance
     */
    suppressDuplicateMaintenanceTimePoints();

    VectorOCP_CpTimeSlot::iterator itOf = ofs.begin();
    OCP_CpDate ofBegin = (*itOf)->getBegin();
    OCP_CpDate ofEnd = (*itOf)->getEnd();

    for(std::vector<OCP_CpDate>::iterator iterEnd1 = _endsForMaintenance.begin(); iterEnd1!=_endsForMaintenance.end(); iterEnd1++)
    {
        OCP_CpDate e = (*iterEnd1);

        if(e>=ofEnd)//passer à la plage de maintenance suivante
        {
            itOf++;
            if(e>=horizon || itOf == ofs.end() )
                break;//plus d'intervalle à créer
            else
            {
                ofBegin = (*itOf)->getBegin();
                ofEnd = (*itOf)->getEnd();
            }
        }
        if (e>=ofBegin && e<ofEnd && e<horizon)//debut de plage de maintenance possible
        {
            for(std::vector<OCP_CpDate>::iterator iterBegin2 = _beginsForMaintenance.begin(); iterBegin2!=_beginsForMaintenance.end(); iterBegin2++)
            {
                OCP_CpDate b = (*iterBegin2);
                if(b>e && b<=ofEnd)//fin de plage de maintenance possible
                {
                    OCP_CpTimeSlot candidate( e , std::min(b,horizon) );
                    if (candidate.getDuration()>0 && candidate.getDuration()>=minMaintenanceDuration)
                    {
                        OCP_CpStationPtr stationPtr(this);
                        OCP_CpMaintenanceSlot* tmp = new OCP_CpMaintenanceSlot(stationPtr,  candidate.getBegin(), candidate.getEnd());
                        addMaintenanceSlot(tmp);
                    }
                }
                if (b>ofEnd) break;//toutes les plages ultérieures intersectent une zone non valide
            }
        }
    }

    //PASSE 2 : slots de maintenance préfixés par utilisateur doivent être maintenus quoi qu'il arrive
    addReservedMaintenanceSlots();

    // PASSE 3 : caracterisation offVisibility
    setOffVisibility();

    boost::format msg = boost::format( _("%1$d slots de maintenance généré pour la station %2$s ")) % _maintenanceSlots.getSize() % getName() ;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

    int nbSlots = _maintenanceSlots.getSize();
    msg = boost::format( _("%1$d slots de maintenance générés ou préreservés sur %2$s ")) % nbSlots % getName();
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
    for(int i = 0 ; i < _maintenanceSlots.getSize() ; i++)
    {
        OCP_CpMaintenanceSlot* slot = (OCP_CpMaintenanceSlot*) _maintenanceSlots[i];
        msg = boost::format( _("%1$s  ")) % slot->getFullName();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
    }
}

/**
 * avant génération des slots de maintenance, on trie les structure de points de temps associées
 * et on supprime les doublons
 */
void OCP_CpStation::suppressDuplicateMaintenanceTimePoints()
{
    std::sort(_beginsForMaintenance.begin() , _beginsForMaintenance.end() );
    OCP_CpDate prev = -1;
    std::vector<OCP_CpDate>::iterator it = _beginsForMaintenance.begin();
    while(it!=_beginsForMaintenance.end())
    {
        OCP_CpDate current = (*it);
        if (current == prev)
            _beginsForMaintenance.erase(it);//supprimer le doublon courant, it pointe vers son successeur
        else
        {
            prev = current;//mise à jour du nouveau prédécesseur pour la boucle suivante
            ++it;
        }
    }

    std::sort(_endsForMaintenance.begin() , _endsForMaintenance.end() );
    OCP_CpDate prev2 = -1;
    std::vector<OCP_CpDate>::iterator it2 = _endsForMaintenance.begin();
    while(it2!=_endsForMaintenance.end())
    {
        OCP_CpDate current = (*it2);
        if (current == prev2)
            _endsForMaintenance.erase(it2);//supprimer le doublon courant, it2 pointe vers son successeur
        else
        {
            prev2 = current;//mise à jour du nouveau prédécesseur pour la boucle suivante
            ++it2;
        }
    }
}


/**
 * on indique sur chaque slot de maintenance généré s'il intersecte une visibilité satellite sur cette station
 */
void OCP_CpStation::setOffVisibility()
{
    for(int i=0; i<_maintenanceSlots.getSize(); i++)
    {
        OCP_CpMaintenanceSlot* currentMaintenanceSlot = (OCP_CpMaintenanceSlot*)(_maintenanceSlots[i]);
        for(int j=0; j<_trackingSlots.getSize(); j++)
        {
            OCP_CpTrackingSlot* currentTrackingSlot = (OCP_CpTrackingSlot*)(_trackingSlots[j]);
            if(!currentTrackingSlot->toRemove() && currentMaintenanceSlot->intersect(currentTrackingSlot->getAmplitude()))
            {
                currentMaintenanceSlot->setOnVisibility();
                break;
            }
        }
    }
}



/**
* vérification de l'inclusion d'un slot satellite dans une des plages horaires de tracking de sa station
* @param ocpSatSlot intervalle temporel en secondes du slot origine
* @return
*/
bool OCP_CpStation::isTrackable(const OCP_CpTimeSlot& ocpSatSlot)
{
    OCP_CpTimeSlot ts(ocpSatSlot.getBegin() % PHRDate::SECONDS_IN_A_DAY , ocpSatSlot.getBegin() % PHRDate::SECONDS_IN_A_DAY);
    if (ts.getEnd() < ts.getBegin() )
        ts.setEnd(ts.getEnd() + PHRDate::SECONDS_IN_A_DAY);

    VectorOcpTimePeriod satSupportPeriods;
    try
    {
        satSupportPeriods = getOrigin()->getSupportAvailablePeriods();
    }
    catch(OcpXMLInvalidField& e)
    {
        //pas de période renseignée, on considère que le slot est toujours trackable par sa station
        return true;
    }

    for(VectorOcpTimePeriod::iterator it = satSupportPeriods.begin() ; it != satSupportPeriods.end() ; it++)
    {
        OcpTimePeriodPtr trackPeriod = (*it);//période HH:MM
        OCP_CpDate beginOffset = (OCP_CpDate) trackPeriod->getStart().getSeconds();
        OCP_CpDate endOffset = (OCP_CpDate) trackPeriod->getEnd().getSeconds();
        if (endOffset==beginOffset && beginOffset!=0)
        {
            boost::format errMsg = boost::format( _("Lecture des périodes de tracking incohérentes station %3$s %1$d %2$d")) % beginOffset % endOffset % getName();
            OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg );
            throw OCP_XRoot(errMsg.str());
        }
        if (endOffset<beginOffset) //périodes inversées [a,b<a], créer deux périodes [a-24,b] et [a,b+24]
        {
            OCP_CpTimeSlot atHorse1( beginOffset - PHRDate::SECONDS_IN_A_DAY ,  endOffset) ;
            OCP_CpTimeSlot atHorse2(beginOffset ,  endOffset + PHRDate::SECONDS_IN_A_DAY) ;
            return ts.isInside(&atHorse1) || ts.isInside(&atHorse2);
        }
        else if(endOffset == beginOffset) //plage 00:00->OO:00 ou non renseignée <=> 1 jour complet
        {
            return true;
        }
        else //endOffset > beginOffset, pas de plage à cheval
        {
            OCP_CpTimeSlot onePlage(  beginOffset  ,  endOffset) ;
            return ts.isInside(&onePlage);
        }
    }
    return false;//ce slot n'est inclus dans aucune des plages horaires
}

/**
 * en lisant les paramètres stations :
 * parcours des zones de tracking possible sur le jour de calcul (avec plages à cheval possible) et notification des dates
 * de début et fin de tracking possible pour calcul ultérieur de split des créneaux SAT1-visibilité partielle
 * @param optimScope : fenêtre de calcul courante
 */
void OCP_CpStation::notifyTrackingZones(OCP_CpTimeSlotPtr optimScope)
{
    OCP_CpDate beginRange  = optimScope->getBegin();
    VectorOcpTimePeriod satSupportPeriods;
    try
    {
        satSupportPeriods = getOrigin()->getSupportAvailablePeriods();
    }
    catch(OcpXMLInvalidField& e)
    {
        //pas de période renseignée, on considère que le slot est toujours trackable par sa station
        return;
    }

    for(VectorOcpTimePeriod::iterator it = satSupportPeriods.begin() ; it != satSupportPeriods.end() ; it++)
    {
        OcpTimePeriodPtr trackPeriod = (*it);//période HH:MM
        OCP_CpDate beginOffset = (OCP_CpDate) trackPeriod->getStart().getSeconds();
        OCP_CpDate endOffset = (OCP_CpDate) trackPeriod->getEnd().getSeconds();
        if (endOffset==beginOffset && beginOffset!=0)
        {
            boost::format errMsg = boost::format( _("Lecture des périodes de tracking incohérentes station %3$s %1$d %2$d")) % beginOffset % endOffset % getName();
            OcpLogManager::getInstance()->error(ocp::solver::loggerInst, errMsg );
            throw OCP_XRoot(errMsg.str());
        }
        if (endOffset<beginOffset) //périodes inversées [a,b<a], créer deux périodes [a-24,b] et [a,b+24]
        {
            OCP_CpTimeSlot atHorse1( beginRange + beginOffset - PHRDate::SECONDS_IN_A_DAY ,beginRange+  endOffset) ;
            OCP_CpTimeSlot atHorse2(beginRange + beginOffset , beginRange+ endOffset + PHRDate::SECONDS_IN_A_DAY) ;
            _beginTrackings.push_back(atHorse1.getBegin());
            _beginTrackings.push_back(atHorse2.getBegin());
            _endTrackings.push_back(atHorse1.getEnd());
            _endTrackings.push_back(atHorse2.getEnd());
        }
        else if(endOffset > beginOffset) //plage 00:00->OO:00 , rien à notifier
         {
            OCP_CpTimeSlot onePlage( beginRange + beginOffset  ,beginRange +  endOffset) ;
            _beginTrackings.push_back(onePlage.getBegin());
            _endTrackings.push_back(onePlage.getEnd());
        }
    }
}



/**
 * affichage des slots de maintenance possibles
 */
void OCP_CpStation::printMaintenanceSlots()
{

	IloAnySet possibleSet = _maintenanceSlotSet.getPossibleSet();

	for(IloAnySetIterator iterPossible(possibleSet); iterPossible.ok(); ++iterPossible)
	{
		IloAny currentPossible = *iterPossible;
		OCP_CpMaintenanceSlot* currentSlot = (OCP_CpMaintenanceSlot*)currentPossible;

		OcpLogManager::getInstance()->debug(ocp::solver::loggerInst,  boost::format( _("OCP_CpMaintenanceSlot entre %1$i et %2$i") )
		        % currentSlot->getBegin() % currentSlot->getEnd() );
	}
}

/**
 * création de la variable ensembliste ayant pour domaine les slots de maintenance possibles pour la station
 * @return le nombre de slots de maintenance
 */
int OCP_CpStation::createMaintenanceSlots() {
	OCP_CpModel* cpModel = OCP_CpModel::getInstance();
	IloEnv env = cpModel->getEnv();

	int n = _maintenanceSlots.getSize();
	_maintenanceSlotSet = IloAnySetVar(env, _maintenanceSlots);
	cpModel->getModel().add(_maintenanceSlotSet);
	return n;
}

/**
 * rajoute les contraintes internes
 * non ubiquité
 * durée cumulée de maintenance
 * lien avec la variable globale
 */
void OCP_CpStation::addInternalConstraints()
{
	OCP_CpModel* cpModel = OCP_CpModel::getInstance();
	IloEnv env = cpModel->getEnv();
	IloAnySetVar maintenanceSlot = getMaintenanceSlotSet();
	IloAnySetVar trackingSlot = getTrackingSlotSet();

	cpModel->getModel().add(IloNonUbiquityConstraint(env, trackingSlot, maintenanceSlot));
	//cpModel->getModel().add(IloNonUbiquityConstraint(env, maintenanceSlot, maintenanceSlot));
	// TODO: implement another search goal based only on slots because solver default generate first intVars and caused bounding at 0 of all duration variables!
	OCP_CpTimeSlot* horizon = new OCP_CpTimeSlot(cpModel->getOrigin(),cpModel->getHorizon());
	OCP_CpTimeSlotPtr horizonPtr (horizon);

	cpModel->getModel().add(IloSumSlotConstraint(env, maintenanceSlot, _maintenanceDuration,horizonPtr));
	cpModel->getModel().add(IloLinkGlobalSlotConstraint(env, maintenanceSlot));

	/*
	 * on peut poser une contrainte bornant la somme des durées de tracking POUR UNE STATION (par pour un satellite)
	 * , la borne étant la taille de la fenêtre de calcul, plus précisemment l'amplitude entre la date de début du premier passage
	 * et la date de fin du dernier passage. Cette contrainte peut aider à propager l'aspect unaire de la station
	 * Note : on ne pose cette contrainte que pour les stations à planifier
	 */

	if(_scheduled)
	{
	    std::vector<OCP_CpDate> cpybeginTrackings;//copie des débuts de tracking
	    std::vector<OCP_CpDate> cpyendTrackings;//copie des débuts de tracking
	    for(std::vector<OCP_CpDate>::iterator it = _beginTrackings.begin() ; it != _beginTrackings.end() ; it++)
	    {
	        cpybeginTrackings.push_back(*it);
	    }
	    for(std::vector<OCP_CpDate>::iterator it = _endTrackings.begin() ; it != _endTrackings.end() ; it++)
	    {
	        cpyendTrackings.push_back(*it);
	    }

	    boost::format logMsg;
	    OCP_CpDate trackingAmplitude = -cpModel->getOrigin() + cpModel->getHorizon() ;
	    if(cpybeginTrackings.size()>0 && cpyendTrackings.size()>0 )
	    {
	        std::sort(cpybeginTrackings.begin() , cpybeginTrackings.end() );
	        std::sort(cpyendTrackings.begin() , cpyendTrackings.end() );
	        //
	        OCP_CpDate aBegin = cpybeginTrackings.front();
	        OCP_CpDate aEnd = cpyendTrackings.back();
	        logMsg = boost::format(_("premier début de tracking sur %1$s %2$d , derniere fin de tracking %3$d"))
	        % getName() % aBegin % aEnd;
	        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, logMsg  );
	        if(aEnd < aBegin)
	        {
	            logMsg = boost::format(_("OCP_CpStation::addInternalConstraints : erreur dans la pose de la contrainte interne pour la station %1$s "))
	            % getName();
	            throw OCP_XRoot(logMsg.str());
	        }
	        trackingAmplitude =  aEnd - aBegin;
	    }

	    logMsg = boost::format(_("_trackingDuration_Station_%1$s_Amplitude_%2$d"))
	    % getName() % trackingAmplitude;
	    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, logMsg  );
	    _trackingDuration = IloIntVar(env, 0 , trackingAmplitude, logMsg.str().c_str());
	    cpModel->getModel().add(IloSumSlotConstraint(env, trackingSlot, _trackingDuration,horizonPtr));
	}

	OCP_CpTrackingResource::addInternalConstraints();
}

/**
 * affichage des maintenances requises dans la solution courante
 * @param solver le solver ILOG SOLVER
 */
void OCP_CpStation::displayMaintenanceSlotVar(IloSolver solver)
{
    boost::format logMsg;
    boost::format slotList;

	IloAnySetVar act = getMaintenanceSlotSet();

	for(IlcAnySetIterator it(solver.getAnySetVar(act).getRequiredSet()); it.ok(); ++it)
	{
		OCP_CpMaintenanceSlot* ts = (OCP_CpMaintenanceSlot*)(*it);
		slotList = boost::format("%1$s [%2$i; %3$i], ") % slotList % ts->getBegin() % ts->getEnd();
	}

	logMsg = boost::format(_("Créneaux de maintenance - %1$s - pour une durée minimum de %2$i"))
            % slotList % solver.getIntVar(getMaintenanceDuration()).getMin();
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, logMsg);
}

/**
 * affichage de la solution (maintenance et tracking)
 * @param solver le solver ILOG SOLVER
 */
void OCP_CpStation::printSolution(IloSolver solver)
{
	displayTrackingSlotVar(solver);
	displayMaintenanceSlotVar(solver);
}

/**
* sauvegarde des slots de la station
* @param solver solver de la station
* @param nbTrackings nombre de slots de tracking sauvegardés
* @param nbMaintenances nombre de slots de maintenance sauvegardés
*/
void OCP_CpStation::saveSlots(IloSolver solver,int& nbTrackings,int& nbMaintenances)
{
    /*
     * mémoriser les father splits sur cette station pour traitement spécifique
     */
    std::map<OCP_CpTrackingSlot*, OCP_CpTrackingSlot*> fatherSplits;

    /*
     * sauvegarde des défilants
     */
    int nbNormalSlots =saveTrackingSlots(solver, fatherSplits);//les slots non splittables, mais le nombre compte tout (splittables et non splittables)
    nbTrackings += nbNormalSlots;

    boost::format logMsg = boost::format(_("Station %1$s nbNormalSlots %2$d nbTrackings %3$d"))
    % getName() % nbNormalSlots % nbTrackings;
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, logMsg);

    /*
     * sauvegarde des slots de splits
     */
    for(std::map<OCP_CpTrackingSlot*,OCP_CpTrackingSlot*>::iterator itS = fatherSplits.begin() ; itS != fatherSplits.end(); itS++)
    {
        OCP_CpTrackingSlot* fatherSplit = (*itS).first;
        int nbSplitSlots = fatherSplit->saveSplitSons(solver);

        logMsg = boost::format(_("Station %1$s nbSplitSlots %2$d nbTrackings %3$d"))
        % getName() % nbSplitSlots % nbTrackings;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, logMsg);

    }

    nbMaintenances = saveMaintenanceSlots(solver);
}


/**
* Sauvegarde des slots de maintenance
* @param solver : solver PPC ILOG
* @return nombre de créneaux de maintenance sauvegardés
*/
int OCP_CpStation::saveMaintenanceSlots(IloSolver solver)
{
    OcpContextPtr context = OcpXml::getInstance()->getContext();
    IloAnySetVar act = getMaintenanceSlotSet();
    IlcAnySet requiredSet = solver.getAnySetVar(act).getRequiredSet();
    int nbSaved = requiredSet.getSize() ;

    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("%1$d slots de maintenance à sauver ")) % nbSaved );

    for(IlcAnySetIterator it(solver.getAnySetVar(act).getRequiredSet()); it.ok(); ++it)
    {
        OCP_CpMaintenanceSlot* ts = (OCP_CpMaintenanceSlot*)(*it);

        boost::format msg = boost::format( _("Sauvegarde du slot de maintenance %1$s") ) % ts->getFullName().str();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str() );

        ts->postOptim();//notification des TYPE_CRENEAU choisis

        /*
         * sauvegarde sur le contexte pour les créneaux non préreservés
         */
        if(!ts->isReserved())
        {
            context->addStationSlot(ts->getSlotOrigin());
        }
    }
    return nbSaved;
}


/**
 *
 * @return fréquence associée à la station
 */
FrequencyBands::FrequencyBands_ENUM OCP_CpStation::getStationFrequencyBand() const
{
    return _station->getFrequencyBand();
}

/**
* calcule la somme des durées des slots possibles entre deux dates
* @param begin début de la fenêtre de calcul
* @param end fin de la fenêtre de calcul
* @return le cumul des durées d'intersection avec la fenêtre
*/
OCP_CpDate OCP_CpStation::getPossibleMaintenanceDuration(OCP_CpDate begin, OCP_CpDate end) const
{
    OCP_CpDate result = 0;
    OCP_CpTimeSlot area(begin,end);
    for(int i=0; i<_maintenanceSlots.getSize(); i++)
    {
        OCP_CpMaintenanceSlot* currentMaintenanceSlot = (OCP_CpMaintenanceSlot*)(_maintenanceSlots[i]);
        if (! currentMaintenanceSlot->hasBookingOrder())
            result += currentMaintenanceSlot->getIntersectionDuration(&area);
    }
    return result;
}

/**
* calcule le nombre de slots d'une durée minimale dans la fenêtre de calcul
* @param begin début de la fenêtre de calcul
* @param end fin de la fenêtre de calcul

* @param filterMinDuration filtre de durée minimale sur la partie intersectant la fenêtre
* @return le nombre de slots candidats
*/
int OCP_CpStation::getNbPossibleMaintenanceCandidates(OCP_CpDate begin, OCP_CpDate end, OCP_CpDate filterMinDuration) const
{
    int result = 0;
    OCP_CpTimeSlot area(begin,end);
    for(int i=0; i<_maintenanceSlots.getSize(); i++)
    {
        OCP_CpMaintenanceSlot* currentMaintenanceSlot = (OCP_CpMaintenanceSlot*)(_maintenanceSlots[i]);
        OCP_CpDate duration = currentMaintenanceSlot->getIntersectionDuration(&area);
        if(!currentMaintenanceSlot->hasBookingOrder() && duration >= filterMinDuration)
            result++;
    }
    return result;
}



/**
 * ajout des créneaux de maintenance pré-reservés par l'utilisateur
 */
void OCP_CpStation::addReservedMaintenanceSlots()
{
    static const std::string pseudoClazz = _Clazz + "::addReservedMaintenanceSlots";

    OCP_CpDate origin = OCP_CpModel::getInstance()->getOrigin();
    OCP_CpDate horizon = OCP_CpModel::getInstance()->getHorizon() ;
    OCP_CpTimeSlot scope (origin , horizon);


    PHRDate aBegin(origin);
    PHRDate aEnd(horizon);

    //récupérer tous les slots de maintenance mémorisés sur la fenêtre courante
    OcpContextPtr context = OcpXml::getInstance()->getContext();
    VectorStationSlot maintenancePast = context->getStationSlotsByPeriod(aBegin, aEnd, getOrigin());
    for(VectorStationSlot::iterator it = maintenancePast.begin() ; it != maintenancePast.end() ; it++)
    {
        OcpStationSlotPtr aMaintenanceSlot = (*it);
        OCP_CpMaintenanceSlot* maintenanceSlot = new OCP_CpMaintenanceSlot(aMaintenanceSlot);
        if (maintenanceSlot->isReserved() )
        {
            if (maintenanceSlot->isInside(&scope) )
            {
                addMaintenanceSlot(maintenanceSlot);
                if (maintenanceSlot->hasBookingOrder())
                    _maintenanceSlotsWithBookingOrders.add(maintenanceSlot);
            }
            else
            {
                boost::format msg = boost::format( _("%1$s préfixé en dehors de la période ") )
                % maintenanceSlot->getFullName() % aBegin.getStringValue() % aEnd.getStringValue();
                throw OCP_XRoot(msg.str());
            }
        }
    }

}

/**
 * une fois supprimé les slots superflus, on peut calculer une priorité aggrégée pour chaque slot*
 * en tenant compte des priorités de SAT4, STA6 et des conflits potentiels
 */
void OCP_CpStation::updateTrackingSlotsPriorities()
{

    IloEnv env = OCP_CpModel::getInstance()->getEnv();

    vector<OCP_CpTrackingSlot*> slotsByBegin;//liste des slots possibles triés par début
    vector<OCP_CpTrackingSlot*> slotsByEnd;//liste des slots possibles triés par fin décroissante
    for(int i=0; i < _trackingSlots.getSize() ; i++)
    {
        OCP_CpTrackingSlot*  slot = (OCP_CpTrackingSlot*) (_trackingSlots[i]);
        if(!slot->toRemove() && !slot->isReserved())//jamais choisis ou toujours choisis : pas de notion de priorité dans la sélection
        {
            slotsByBegin.push_back(slot);
            slotsByEnd.push_back(slot);
        }
    }

    if (slotsByBegin.size() == 0)
        return;

    std::sort(slotsByBegin.begin(), slotsByBegin.end(), OCP_CpTimeSlot::slotsBeginSortPredicate);
    std::sort(slotsByEnd.begin(), slotsByEnd.end(), OCP_CpTimeSlot::slotsReverseEndSortPredicate);

    OCP_CpDate begin = slotsByBegin.front()->getBegin();
    OCP_CpDate end = slotsByEnd.front()->getEnd();

    OCP_CpCstIntTimetable cumul(env, begin, end);

    /*
     * première étape : construction de la timetable des conflits
     */
    for(vector<OCP_CpTrackingSlot*>::iterator it = slotsByBegin.begin() ; it != slotsByBegin.end() ; it++)
    {
        OCP_CpTrackingSlot*  slot =  *it;
        cumul.add(slot->getBegin() , slot->getEnd() , 1);
    }

    /**
     * seconde étape : mise à jour de la priorité en cas de conflit : utilisation de la priorité de STA6
     */
    for(vector<OCP_CpTrackingSlot*>::iterator it = slotsByBegin.begin() ; it != slotsByBegin.end() ; it++)
    {
        OCP_CpTrackingSlot*  slot =  *it;
        IloInt nbConflicts = cumul.getMax(slot->getBegin() , slot->getEnd() );
        if (nbConflicts > 1) //au moins deux slots concurrents, on doit arbitrer via la priorité station
        {
            slot->updateSearchPriority();
        }
    }

}

/**
 * accès via map des durées de configuration déconfiguration antenne d'un satellite sur une station
 * @param aSatellite : satellite
 * @param reconf : temps de reconfiguration en secondes
 * @param deconf : temps de deconfiguration en secondes
 */
void OCP_CpStation::getReconfDeconfDuration(OCP_CpSatellite* aSatellite, uint32_t& reconf, uint32_t& deconf)
{
    reconf = 0;
    deconf = 0;

    std::map<OCP_CpSatellite*, uint32_t>::iterator itReconf = _reconf.find( aSatellite );
    std::map<OCP_CpSatellite*, uint32_t>::iterator itDeconf = _deconf.find( aSatellite );
    if (itReconf == _reconf.end() ) //création de l'entrée de la clé pour la station
    {
        try
        {
            getOrigin()->getReconfDeconfDuration(aSatellite->getOrigin()->getFamily(),reconf,deconf);
            reconf *= 60 ;//valeur originale en minutes
            deconf *=60;//valeur originale en minutes
            boost::format msg = boost::format( _("getReconfDeconfDuration du satellite %1$s sur station %2$s : reconf %3$d secondes, deconf %4$d secondes") )
            %  aSatellite->getName() % getName() % reconf % deconf;
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg.str());
        }
        catch (OcpXMLInvalidField& e)
        {
            boost::format msg = boost::format( _("Problème dans la getReconfDeconfDuration sur le satellite %1$s sur la station %2$s: les temps de configuration et déconfiguration seront mis à zéro ") )
            %  aSatellite->getName() % getName();
            OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg.str());
        }

        _reconf.insert(std::map<OCP_CpSatellite*, uint32_t>::value_type(aSatellite,reconf) );
        _deconf.insert(std::map<OCP_CpSatellite*, uint32_t>::value_type(aSatellite,deconf) );

    }
    else
    {
        reconf = (*itReconf).second;
        deconf = (*itDeconf).second;
    }

}
