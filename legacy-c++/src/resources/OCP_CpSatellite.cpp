/*
 * $Id: OCP_CpSatellite.cpp 942 2010-12-21 13:11:22Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatellite.cpp
 *
 */
#include <string>

#include "resources/OCP_CpTrackingResource.h"
#include "context/OCP_CpModel.h"
#include "resources/OCP_CpSatellite.h"
#include "constraints/OCP_CpSatelliteConstraint8.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpSatellite);

/**
* constructeur de l'objet utilisé par la CP
* @param satellite_
* @param scheduled : la ressource est à planifier par le moteur
* @return
*/
OCP_CpSatellite::OCP_CpSatellite(OcpSatellitePtr satellite_, bool scheduled) :
OCP_CpTrackingResource(satellite_->getName(),scheduled),
_satellite(satellite_)
{

}


/**
* mémorisation des propriétés d'une station après lecture de la contrainte SAT4 pour le satellite
* @param stationProperty : les propriétés vues lors de la lecture de la contrainte SAT4 pour le satellite
*/
void OCP_CpSatellite::storeOcpStationProperties(OcpStationPropertiesPtr stationProperty)
{
    static const std::string pseudoClazz = _Clazz + "::storeOcpStationProperties";

    OcpStationPtr aStation = stationProperty->getStation();
    float minElevationProperty = 0;
    int minDuration = 0;

    /*
     * lecture de l'élévation minimale requise
     */

    try
    {
        minElevationProperty =  stationProperty->getMinElevation() ;
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
        boost::format msg = boost::format( _("minElevationProperty non renseignée : on prendra élévation 0 par défaut "));
        OcpLogManager::getInstance()->warning(pseudoClazz, msg);
    }

    try
    {
        minDuration = stationProperty->getMinDuration();
    }
    catch(OcpXMLInvalidField& e)
    {
        //rien à faire si champs non renseigné
        boost::format msg = boost::format( _("minDuration non renseignée : on prendra 0 par défaut "));
        OcpLogManager::getInstance()->warning(pseudoClazz, msg);
    }

    _sat4Elevation.insert(std::map<OcpStationPtr, float>::value_type(aStation , minElevationProperty ));
    _sat4MinDuration.insert(std::map<OcpStationPtr, int>::value_type(aStation , minDuration ));

}


/**
* accède via un cache aux informations renseignées par SAT4
* @param aStation la station sur laquelle on recherche les propriétés
* @param minElevationProperty propriété d'élévation min à renseigner
* @param minDuration propriété de durée min à renseigner
*/
void OCP_CpSatellite::getSat4ElevationAndDuration(OcpStationPtr aStation, float& minElevationProperty ,int& minDuration) const
        {
    static const std::string pseudoClazz = _Clazz + "::getSat4ElevationAndDuration";


    std::map<OcpStationPtr, float>::const_iterator it0 = _sat4Elevation.find(aStation);
    minElevationProperty = (*it0).second;

    std::map<OcpStationPtr, int>::const_iterator it1 = _sat4MinDuration.find(aStation);
    minDuration = (*it1).second;

        }


/**
* ajout d'une période "SAT1-visibilité partielle" où l'on devra splitter les créneaux
* @param zone
*/
void OCP_CpSatellite::addSplitZone(OCP_CpTrackingSlotPtr zone)
{
    int found = false;
    for(VectorOCP_CpTrackingSlot::iterator it = _splitZones.begin() ; it != _splitZones.end() && !found ; it++)
    {
        if (zone == *it)
            found = true;
    }

    if (!found)
        _splitZones.push_back(zone);
}

/**
* generation de nouveaux slots splittés depuis des slots intersectant une zone de split
* L'algorithme de découpage des créneaux est comme suit : pour le satellite courant, renseigner deux tableaux
* correspondant aux aos et los des créneaux. Chaque intervalle aos-los correspondra à un intervalle intersectant la zone de split
* On initialise aos et los avec ceux du créneau courant à splitter.
* regarder les créneaux C de tous les satellites qui intersectent la zone de split et l'intervalle à splitter
* ajouter à los le début de C-temps de déconfiguration du satellite courant
* ajouter à aos des crénaux à splitter sur d'autres stations que la station courante
*
* @param splitCandidates : liste aggrégée des nouveau slots splittés sur l'ensemble des satellites
*/
void OCP_CpSatellite::generateSplitSlots(std::vector <  OCP_CpTrackingSlot* >& splitCandidates)
{
    std::vector<OCP_CpTrackingSlot*> candidates;
    std::map<OCP_CpStationPtr, VectorOCP_CpDate*> aos;
    std::map<OCP_CpStationPtr, VectorOCP_CpDate*> los;

    /*
     * 1. collecte des candidats intersectant une zone de split
     */
    collectCandidates(candidates,aos,los);

    /*
     * 2. tri des points debut/fin de satellite intersectant un candidat
     */
    std::map<OCP_CpStationPtr,OCP_CpStationPtr> stations;
    initializeAosLos(candidates,aos,los,stations);
    addAdditionalSat8Points(aos,los,stations);
    updateAosLos(candidates,aos,los);



    /*
     * 3. tri chronologique et suppression des doublons puis création des nouveaux slots splittés
     */
    sortAosLos(aos,los);
    applyAosLos(candidates,splitCandidates,aos,los);

}

/**
 * parcours des contraintes SAT8 du satellite courant pour notifier de nouveaux points aos los sur les stations concernées
 * @param aos : par station, liste des aos de split
 * @param los : par station, liste des los de split
 * @param stations : liste des stations vues sur les candidats au split
 */
void OCP_CpSatellite::addAdditionalSat8Points(std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& aos,std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& los,std::map<OCP_CpStationPtr,OCP_CpStationPtr>& stations)
{
    OCP_CpStationPtr aStation;

    for(VectorOCP_CpConstraint::iterator it = _cpReserveConstraints.begin() ; it != _cpReserveConstraints.end() ; it++)
    {
        OCP_CpConstraintPtr aCtr = *it;
        if( aCtr->isSAT8())
        {
            OCP_CpSatelliteConstraint8Ptr cstrSat8 = boost::dynamic_pointer_cast<OCP_CpSatelliteConstraint8>(aCtr);
            StationMap sat8Stations =  cstrSat8->getStations();

            /*
             * renseigner de nouveaux aos los sur les stations en fonction des pas significatifs de SAT8
             * on le fait sur les stations de SAT8 si celles-ci sont renseignées, sinon on prend toutes les stations
             * concernées par le split
             */
            int sat8nbStations = sat8Stations.size();
            for(StationMap::iterator itSta = sat8Stations.begin() ; itSta != sat8Stations.end() ; ++itSta)
            {
                aStation = (*itSta).second;
                loopSat8Infos(aStation,cstrSat8,aos,los);
            }
            for(std::map<OCP_CpStationPtr,OCP_CpStationPtr>::iterator itSta2 = stations.begin() ; sat8nbStations ==0 && itSta2 != stations.end() ; itSta2++)
            {
                aStation = (*itSta2).second;
                loopSat8Infos(aStation,cstrSat8,aos,los);
            }
        }
    }
}

/**
 * tenir compte des points de temps significatifs de la contrainte SAT8 pour mettre à jour les aos los potentiels dans le découpage des split slots
 * @param aStation : station concernée par le découpage
 * @param cstrSat8 : contrainte SAT8 origine
 * @param aos : par station, liste des aos de split
 * @param los : par station, liste des los de split
 */
void OCP_CpSatellite::loopSat8Infos(OCP_CpStationPtr aStation, OCP_CpSatelliteConstraint8Ptr cstrSat8,std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& aos,std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& los)
{
    OCP_CpTimeSlotPtr support = cstrSat8->getSupport();
    OCP_CpDate durationMin = cstrSat8->getDurationMin();
    OCP_CpDate durationMax = cstrSat8->getDurationMax();
    OCP_CpDate begin = support->getBegin();
    OCP_CpDate end = support->getEnd();
    OCP_CpDate aDate;

    VectorOCP_CpDate* aoss = aos[aStation];
    VectorOCP_CpDate* loss = los[aStation];;

    /*
     * dans une première approche, on va rajouter les points multiples de durationMin, durationMax, et durationMin+durationMax
     */
    for(aDate = begin+durationMin ; durationMin>0 && aDate <= end ; )
    {

        aDate+=durationMin;
        aoss->push_back(aDate);
        loss->push_back(aDate);
    }

    for(aDate = begin+durationMax ; durationMax>0 && aDate <= end ; )
    {

        aDate+=durationMax;
        aoss->push_back(aDate);
        loss->push_back(aDate);
    }

    for(aDate = begin+durationMin+durationMax ; durationMin+durationMax>0 && aDate <= end ; )
    {

        aDate+=durationMin+durationMax;
        aoss->push_back(aDate);
        loss->push_back(aDate);
    }
}

/**
* collecte des slots concernés par une zone de split. Note : si la station n'est pas renseignée sur la zone de split, tout créneau
* intersectant la zone, quelle que soit sa station, sera candidate
* @param candidates : renseigné par la méthode, liste des candidats collectés
* @param aos : par station, liste des aos de split
* @param los : par station, liste des los de split
*/
void OCP_CpSatellite::collectCandidates(std::vector<OCP_CpTrackingSlot*>& candidates, std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& aos,std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& los)
{
    OCP_CpTrackingSlotPtr aSplit;
    OCP_CpTrackingSlot* trackingSlot;

    for(VectorOCP_CpTrackingSlot::iterator itSplit =  _splitZones.begin() ; itSplit != _splitZones.end(); itSplit++)
    {
        /*
         * zone (trackingSlot artificiel) sur laquelle on doit splitter les passages du satellite
         * ce split a été créé par une contrainte SAT7 ou SAT8
         */
        aSplit = (*itSplit);


        //attention, stocker temporairement la liste des passages candidats
        for(int i = 0; i<getTrackingSlots().getSize(); i++)
        {
            // accès à l'un des créneaux d'origine
            trackingSlot = (OCP_CpTrackingSlot*)(getTrackingSlots()[i]);

            /*
             * test : faut-il splitter ce créneau origine - associé à  Oui ssi :
             * . la zone de split demandée intersecte le créneau origine
             * . la zone de split concerne la station du créneau origine ou toutes les stations
             *     (auquel cas la station associée au split slot n'est pas renseignée)
             */

            if ( ( (aSplit->getStation() == NULL) || (aSplit->getStation() == trackingSlot->getStation()) )
                    && (aSplit->intersect(trackingSlot)) )
            {
                candidates.push_back(trackingSlot);
                if(aSplit->getBegin() >  trackingSlot->getBegin() )
                    pushAos(trackingSlot->getStation(), aSplit->getBegin(), aos );
                if(aSplit->getEnd() <  trackingSlot->getEnd() )
                    pushLos(trackingSlot->getStation(), aSplit->getEnd(), los );
            }
        }
    }
}


/**
* Initialisation des tableaux aos los pour chaque station concernée par un créneau à splitter
* @param candidates : candidats en entrée intersectant des zones de split
* @param aos : par station, liste des aos de split
* @param los : par station, liste des los de split
* @param stations : la liste des stations rencontrées sur les différents candidats
*
*/
void OCP_CpSatellite::initializeAosLos(std::vector<OCP_CpTrackingSlot*>&candidates , std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& aos,std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& los,std::map<OCP_CpStationPtr,OCP_CpStationPtr>& stations)
{
    OCP_CpTrackingSlot* aCandidate;
    OCP_CpTrackingSlotPtr aSplit;

    /*
     * initialisation des aos et los à ceux des créneaux à splitter
     */
    for(std::vector<OCP_CpTrackingSlot*>::iterator itTracking =  candidates.begin() ; itTracking != candidates.end(); itTracking++)
    {
        aCandidate = (*itTracking);
        OCP_CpStationPtr aStation = aCandidate->getStation();

        /*
         * HACK artf646051 lecture des temps de configuration / déconfiguration : les retrancher des slots de 24h pour recréer artificiellement les aos los utiles pour la décomposition
         * en effet, il n'y a pas à la lecture de temps de reconf deconf sur les slots de 24h (mais on en veut sur les slots découpés !!)
         */
        uint32_t reconf=0;
        uint32_t deconf=0;
        aStation->getReconfDeconfDuration(this, reconf, deconf);

        stations.insert(std::map<OCP_CpStationPtr, OCP_CpStationPtr>::value_type(aStation , aStation ));

        pushAos(aStation, aCandidate->getBegin() + reconf , aos );//HACK artf646051
        pushLos(aStation, aCandidate->getEnd() - deconf, los );//HACK artf646051
    }


    /*
     * ajout aux aos los des débuts et fins de zones de split
     */
    for(VectorOCP_CpTrackingSlot::iterator itSplit =  _splitZones.begin() ; itSplit != _splitZones.end(); itSplit++)
    {
        /*
         * zone (trackingSlot artificiel) sur laquelle on doit splitter les passages du satellite
         * ce split a été créé par une contrainte SAT7 ou SAT8
         */
        aSplit = (*itSplit);

        for(std::map<OCP_CpStationPtr, OCP_CpStationPtr>::iterator it = stations.begin() ; it != stations.end() ; it++)
        {
            OCP_CpStationPtr aStation = (*it).first;
            pushAos(aStation, aSplit->getBegin(), aos );
            pushLos(aStation, aSplit->getEnd(), los );
        }
    }
}

/**
* ajout à la structure d'un nouvel aos
* @param aStation station sur laquelle on mémorise le nouvel aos
* @param newaos nouvel aos de début de split
* @param aos map des aos par station
*/
void OCP_CpSatellite::pushAos(OCP_CpStationPtr aStation,OCP_CpDate newaos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos)
{
    VectorOCP_CpDate* aoss;
    std::map<OCP_CpStationPtr, VectorOCP_CpDate*>::iterator itAos = aos.find( aStation );
    if (itAos == aos.end() ) //création de l'entrée de la clé pour la station
    {
        aoss = new VectorOCP_CpDate();
        aos.insert(std::map<OCP_CpStationPtr, VectorOCP_CpDate*>::value_type(aStation,aoss) );
    }
    else
    {
        aoss = (*itAos).second;
    }
    aoss->push_back(newaos);
}

/**
* ajout à la structure d'un nouvel los
* @param aStation station sur laquelle on mémorise le nouvel aos
* @param newlos nouvel aos de début de split
* @param los map des aos par station
*/
void OCP_CpSatellite::pushLos(OCP_CpStationPtr aStation,OCP_CpDate newlos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los)
{
    VectorOCP_CpDate* loss;
    std::map<OCP_CpStationPtr, VectorOCP_CpDate*>::iterator itLos = los.find( aStation );
    if (itLos == los.end() ) //création de l'entrée de la clé pour la station
    {
        loss = new VectorOCP_CpDate();
        los.insert(std::map<OCP_CpStationPtr, VectorOCP_CpDate*>::value_type(aStation,loss) );
    }
    else
    {
        loss = (*itLos).second;
    }


    loss->push_back(newlos);
}



/**
* impacter le passage des slots défilants. Un créneau défilant C intersectant un créneau candidat (les deux créneaux sur la même station)
* notifie le tableau los sur cette station avec los = C-temps de déconfiguration du satellite courant sur cette station
* @param candidates : candidats initiaux au split
* @param aos : par station, liste des aos de split
* @param los : par station, liste des los de split
*/
void OCP_CpSatellite::updateAosLos(std::vector<OCP_CpTrackingSlot*>&candidates , std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& aos, std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& los)
{
    OCP_CpTrackingSlot* tmp;
    OCP_CpTimeSlotPtr amplitude;
    OCP_CpTrackingSlot* aCandidate;
    VectorOCP_CpDate* aoss;
    VectorOCP_CpDate* loss;


    /*
     * le debut du satellite coupe les créneaux à splitter : son début-deconf marquera la los d'un slot splitté sur la même
     * station et l'aos d'un slot splitté sur une station différente
     */
    for(std::vector<OCP_CpTrackingSlot*>::iterator itCandidat = candidates.begin(); itCandidat != candidates.end() ; itCandidat++)
    {
        aCandidate = *itCandidat;
        OCP_CpStationPtr aStation = aCandidate->getStation();
        loss = los[aStation];

        uint32_t reconf=0;
        uint32_t deconf=0;
        aStation->getReconfDeconfDuration(this, reconf, deconf);

        for(int i=0; i<aStation->getTrackingSlots().getSize(); i++)
        {
            tmp = (OCP_CpTrackingSlot*)(aStation->getTrackingSlots()[i]);
            amplitude = tmp->getAmplitude();
            if(amplitude->intersect(aCandidate)
                    && amplitude->getBegin() - deconf > aCandidate->getBegin()
                    && amplitude->getBegin() - deconf < aCandidate->getEnd()
            )
            {
                boost::format msg = boost::format( _("OCP_CpSatellite::updateAosLos entre candidat %1$s et défilant %2$s ") )
                % aCandidate->getFullName() %  tmp->getFullName() ;
                OcpLogManager::getInstance()->warning(ocp::solver::loggerInst, msg.str());

                OCP_CpDate aDate = amplitude->getBegin() - deconf;

                /*
                 * 1. créer un los égal au début de tmp - deconf sur la même station
                 */

                loss->push_back(aDate);
                PHRDate aDateDisplay( aDate ,  PHRDate::_TimeFormat );
                msg = boost::format( _("MAJ LOS sur station %1$s %2$s (%3$d) ") ) % aStation->getName() % aDateDisplay.getStringValue() %  (aDate) ;
                OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());

                /*
                 * 2. parcourir les candidats des autres stations pour créer des aos
                 */

                for(std::vector<OCP_CpTrackingSlot*>::iterator itCandidatOtherStation = candidates.begin(); itCandidatOtherStation != candidates.end() ; itCandidatOtherStation++)
                {
                    aCandidate = *itCandidatOtherStation;
                    OCP_CpStationPtr otherStation = aCandidate->getStation();

                    if(otherStation != aStation && aDate>aCandidate->getBegin() && aDate<aCandidate->getEnd())
                    {
                        aoss = aos[otherStation];
                        aoss->push_back(aDate);
                        msg = boost::format( _("MAJ AOS %1$s %2$s (%3$d) ") ) % otherStation->getName() % aDateDisplay.getStringValue() %  (aDate) ;
                        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str());
                    }
                }
            }
        }
    }
}

/**
 * tri chronologique et suppression des doublons des aoslos des nouveaux slots splittés à créer
* @param aos : par station, liste des aos de split
* @param los : par station, liste des los de split
 */
void OCP_CpSatellite::sortAosLos(std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& aos, std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& los)
{
    VectorOCP_CpDate* aoss;
    VectorOCP_CpDate* loss;


    for(std::map<OCP_CpStationPtr, VectorOCP_CpDate*>::iterator it = los.begin() ; it != los.end() ; it++)
    {
        OCP_CpStationPtr aStation = (*it).first;
        loss = los[aStation];
        aoss = aos[aStation];

        /*
         * tri chronologique sans doublons des deux tableaux aos et los
         */
        std::sort(aoss->begin() , aoss->end() );
        OCP_CpDate prev = -1;
        std::vector<OCP_CpDate>::iterator it = aoss->begin();
        while(it!=aoss->end())
        {
            OCP_CpDate current = (*it);
            if (current == prev)
                aoss->erase(it);//supprimer le doublon courant, it pointe vers son successeur
            else
            {
                prev = current;//mise à jour du nouveau prédécesseur pour la boucle suivante
                ++it;
            }
        }

        std::sort(loss->begin() , loss->end() );
        OCP_CpDate prev2 = -1;
        std::vector<OCP_CpDate>::iterator it2 = loss->begin();
        while(it2!=loss->end())
        {
            OCP_CpDate current = (*it2);
            if (current == prev2)
                loss->erase(it2);//supprimer le doublon courant, it2 pointe vers son successeur
            else
            {
                prev2 = current;//mise à jour du nouveau prédécesseur pour la boucle suivante
                ++it2;
            }
        }
    }
}


/**
* création des nouveaux slots splittés
* @param candidates : candidats origine pour le split
* @param splitCandidates : liste aggrégée des nouveau slots splittés sur l'ensemble des satellites
* @param aos : par station, liste des aos de split
* @param los : par station, liste des los de split
*/
void OCP_CpSatellite::applyAosLos(std::vector<OCP_CpTrackingSlot*>&candidates ,std::vector <  OCP_CpTrackingSlot* >& splitCandidates,std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& aos, std::map<OCP_CpStationPtr, VectorOCP_CpDate*>& los)
{
    VectorOCP_CpDate* aoss;
    VectorOCP_CpDate* loss;

    /**
     * accès à la durée minimum renseignée par SAT4 qui doit éviter de faire des slots trop courts
     */
    float minElevationProperty = 0;
    int minDuration = 0;
    OcpContextPtr context = OcpXml::getInstance()->getContext();
    uint16_t minSplit = 60 * context->getMinLengthToSplitSlot();


    for(std::map<OCP_CpStationPtr, VectorOCP_CpDate*>::iterator it = los.begin() ; it != los.end() ; it++)
    {
        OCP_CpStationPtr aStation = (*it).first;
        loss = los[aStation];
        aoss = aos[aStation];
        /**
        * accès à la durée minimum renseignée par SAT4 qui doit éviter de faire des slots trop courts
        */
        getSat4ElevationAndDuration(aStation->getOrigin() , minElevationProperty , minDuration);

        /*
         * lecture des temps de configuration / déconfiguration
         */
        uint32_t reconf=0;
        uint32_t deconf=0;
        aStation->getReconfDeconfDuration(this, reconf, deconf);

        for(std::vector<OCP_CpDate>::iterator it1 = aoss->begin();it1 != aoss->end() ; it1++)
        {
            OCP_CpDate aAos = *it1;
            for(std::vector<OCP_CpDate>::iterator itEnd = loss->begin();itEnd != loss->end() ; itEnd++)
            {
                OCP_CpDate aLos = *itEnd;

                //PHRDate aLosPrint(aLos);
                //cout << aLosPrint.getStringValue() << endl;

                if(aLos-aAos >= minDuration )
                {
                    OCP_CpTimeSlot area( aAos - reconf , aLos + deconf );
                    if (area.getDuration()>=minSplit && aStation->isTrackable(area) )
                    {
                        /*
                         * recherche de créneau origine
                         */
                        for(std::vector<OCP_CpTrackingSlot*>::iterator itCandidat = candidates.begin() ; itCandidat != candidates.end(); itCandidat++)
                        {
                            OCP_CpTrackingSlot* aCandidat = *itCandidat;
                            if (aCandidat->getStation() == aStation && area.isInside(aCandidat->getAmplitude()) )
                            {
                                addOneSplitTrackingSlot(aCandidat,splitCandidates,area,reconf, deconf);
                            }
                        }
                    }
                }
            }
        }
    }
}


/**
* ajout d'un nouveau tracking slot
* @param slotOrigine : slot origine qu'on va découper sur la zone de split
* @param splitCandidates liste des nouveaux tracking slots
* @param area intervalle de temps potentiel incluant la reconf / déconf
* @param reconf temps de reconfiguration de l'antenne
* @param deconf temps de déconfiguration de l'antenne
* @return le nouveau slot splitté créé
*/
OCP_CpTrackingSlot* OCP_CpSatellite::addOneSplitTrackingSlot(OCP_CpTrackingSlot* slotOrigine,std::vector <  OCP_CpTrackingSlot* >& splitCandidates,const OCP_CpTimeSlot& area,uint32_t reconf, uint32_t deconf)
{
    OcpSatelliteSlotPtr aRootSlot = slotOrigine->getSlotOrigin();
    OcpSatelliteSlotPtr aSplitSlot = boost::dynamic_pointer_cast<OcpSatelliteSlot>(OcpSatelliteSlot::createInstance());

    /**
    * Rattachement du slot à une visibilité existante. Permet de renseigner satellite et station
    */
   aSplitSlot->setVisibility(aRootSlot->getVisibility()) ;

   /*
    * recopie du statut bookingState
    */
   aSplitSlot->setBookingState(aRootSlot->getBookingState());

   /*
    * création de l'aoslosref et notification sur le nouveau slot TODO est-ce nécessaire ??
    */

   boost::shared_ptr < OcpAosLos > aoslos = boost::dynamic_pointer_cast<OcpAosLos> (OcpAosLos::createInstance());
   PHRDate aBegin(area.getBegin() + reconf );
   PHRDate aEnd(area.getEnd() - deconf );
   aoslos->setStart( aBegin );
   aoslos->setEnd( aEnd );
   aoslos->setType( aRootSlot->getAosLosRef()->getType() );//copie du type du slot père
   aoslos->setElevation(aRootSlot->getAosLosRef()->getElevation());//copie de l'élévation du slot père
   aSplitSlot->setAosLosRef(aoslos);

   /*
    * création de la période englobante tenant compte du temps de reconfiguration/déconfiguration antenne
    *  et notification sur le nouveau slot
    */
   OcpPeriodPtr aPeriod = boost::dynamic_pointer_cast<OcpPeriod>(OcpPeriod::createInstance());
   PHRDate aBeginReconf(area.getBegin() );
   PHRDate aEndReconf(area.getEnd() );
   aPeriod->setStart(aBeginReconf);
   aPeriod->setEnd(aEndReconf);
   aSplitSlot->setStartEnd(aPeriod);

   OCP_CpTrackingSlot* tmp = new OCP_CpTrackingSlot(OCP_CpSatellitePtr(this), slotOrigine->getStation(), aSplitSlot) ;
   tmp->setBegin(area.getBegin() + reconf);
   tmp->setEnd(area.getEnd() - deconf);
   tmp->setFatherSplit(slotOrigine);

   /*
    * reprendre les priorités du slot origine
    */
   tmp->setPrioritySatellite(slotOrigine->getSatellitePriority());
   tmp->setPriorityStation(slotOrigine->getStationPriority());
   tmp->setPrioritySearch(slotOrigine->getSearchPriority());


   OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format( _("créneau splitté %1$s ")) % tmp->getFullName() );
   splitCandidates.push_back(tmp);

   return tmp;
}

/**
 * ajout à la collection des informations sur les contraintes SAT1Min
 * @para idCtr id de la contrainte SAT1 origine
 * @param sat1Var variable ensembliste sur laquelle la contrainte IloCard est posée
 * @param sat1Lb borne min de la contrainte SAT1
 * @param sat1Domain slots candidats (domaine de sat1Var après propagation de l'intersection)
 */
void OCP_CpSatellite::pushStableCandidate(int idCtr, IloAnySetVar sat1Var, int sat1Lb, IloAnyArray sat1Domain)
{
    _sat1Ids.push_back(idCtr);
    _sat1StableCandidates.push_back(sat1Var);
    _sat1StableLbs.push_back(sat1Lb);
    _sat1StableDomains.push_back(sat1Domain);
    _sat1StableConflicts.push_back(0);//initialisation des conflits

    /*
     * calculer pour chaque slot dans combien de contraintes il apparait
     */
    for(int i=0;i<sat1Domain.getSize();i++)
    {
        IloAny slot = sat1Domain[i];
        std::map<IloAny,int>::iterator it = _nbSat1BySlot.find(slot);
        if (it == _nbSat1BySlot.end())
            _nbSat1BySlot.insert( std::map<IloAny,int>::value_type(slot,1));
        else
            _nbSat1BySlot[slot] =1+ (*it).second;//increment
    }
}

/**
 * calcule une partition des créneaux collectés via les SAT1. Pour chaque créneau, on comptabilise son
 * nombre d'occurences. On regarde la contrainte pour laquelle il y a le plus de conflits en sommant le nombre
 * d'occurences >1 de chacun des créneaux de cette contrainte. Si ce nombre de conflits est non nul, on enlève
 * la contrainte de la liste et on remet à jour les nombres d'occurence pour itérer le processus
 * TODO cette version est assez violente car on peut avoir des contraintes englobantes avec beaucoup de conflits
 * qui seraient meilleures à conserver que l'union des contraintes disjointes qu'elle intersecte
 */
void OCP_CpSatellite::partitionStable()
{
    boost::format msg = boost::format( _("partitionStable sur %1$s") ) %  getName().c_str();
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

    int i=0;

    int maxi = -1;//indice de la contrainte ayant le plus de conflits
    int maxConflicts = 0;//nombre maximum de conflits sur la contrainte d'indice maxi

    for(std::vector<IloAnyArray>::iterator it = _sat1StableDomains.begin();
            it != _sat1StableDomains.end() ; it++, i++)
    {
        IloAnyArray slots = (*it);
        int nbConflicts = 0;
        for(int s=0;s<slots.getSize();s++)
        {
            IloAny slot = slots[s];
            int nbOccurs = _nbSat1BySlot[slot];
            if (nbOccurs>1)
                nbConflicts++;
        }
        _sat1StableConflicts[i] = nbConflicts;
        if (nbConflicts>maxConflicts)
        {
            maxi = i;
            maxConflicts = nbConflicts;
        }

        msg = boost::format( _("ctrSat1 %1$d a %2$d conflits") ) %  _sat1Ids[i] %  nbConflicts;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
    }

    /*
     * itérations : éliminer la contrainte ayant le plus de conflits et mettre à jour les occurences
     */
    while(maxi>=0)
    {
        /*
         * décrémenter les occurences des slots de la contrainte éliminée
         */
        _sat1StableConflicts[maxi] = -1;//flag pour désactiver cette contrainte
        msg = boost::format( _("MAJ %1$d : supprimé de la liste") ) %  _sat1Ids[maxi] ;
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );

        IloAnyArray slots = _sat1StableDomains[maxi];
        for(int s=0;s<slots.getSize();s++)
        {
            IloAny slot = slots[s];
            _nbSat1BySlot[slot]--;
        }

        i=0;
        maxi = -1;
        maxConflicts = 0;
        /*
         * reparcourir chacune des contraintes en conflit pour décrémenter leur niveau de conflit
         * en fonction des nouvelle occurences
         */
        for(std::vector<IloAnyArray>::iterator it = _sat1StableDomains.begin();
                it != _sat1StableDomains.end() ; it++, i++)
        {
            if (_sat1StableConflicts[i]>0)
            {
                IloAnyArray slots = (*it);
                int nbConflicts = 0;
                for(int s=0;s<slots.getSize();s++)
                {
                    IloAny slot = slots[s];
                    int nbOccurs = _nbSat1BySlot[slot];
                    if (nbOccurs>1)
                        nbConflicts++;
                }
                if(nbConflicts < _sat1StableConflicts[i])
                {
                    msg = boost::format( _("MAJ %1$d : ancien nb conflits= %2$d nouveau = %3$d") )
                    %  _sat1Ids[i] % _sat1StableConflicts[i] % nbConflicts ;
                    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg );
                    _sat1StableConflicts[i] = nbConflicts;
                }
                if (nbConflicts>maxConflicts)
                {
                    maxi = i;
                    maxConflicts = nbConflicts;
                }
            }
        }
    }

}

/**
* Après partition des créneaux apparaissant dans des SAT1, collecte l'ensemble des variables ensemblistes
* correspondant à ces SAT1, l'union des domaines de ces variables
* @param vars variables ensemblistes liées aux SAT1 candidates à la recherche d'un stable max
* @param allslots union des slots apparaissant dans toutes les vars (multi-satellites)
* @return la somme des cardinalités minimales des SAT1 pour le satellite courant
*/
int OCP_CpSatellite::collectStable(IloAnySetVarArray vars,IloAnyArray allslots)
{
    int i=0;
    int sumCard = 0;//somme des cardinalités des SAT1

    std::vector<int>::iterator itCard = _sat1StableLbs.begin();
    std::vector<IloAnySetVar>::iterator itVars = _sat1StableCandidates.begin();

    for(std::vector<IloAnyArray>::iterator it = _sat1StableDomains.begin();
            it != _sat1StableDomains.end() ; it++, i++, itCard++)
    {
        if (_sat1StableConflicts[i] == 0)
        {
            /*
             * mise à jour des slots
             */
            IloAnyArray slots = (*it);
            allslots.add(slots);

            /*
             * mise à jour des variables
             */
            IloAnySetVar setVar = (*itVars);
            vars.add( setVar );

            /*
             * mise à jour des cardinalités
             */
            sumCard += (*itCard);

        }
    }

    return sumCard;
}
