/*
 * $Id: OCP_CpTrackingResource.cpp 942 2010-12-21 13:11:22Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpTrackingResource.cpp
 *
 */
#include <string>
#include <boost/format.hpp>

#include "resources/OCP_CpTrackingResource.h"
#include "resources/OCP_CpSatellite.h"
#include "resources/OCP_CpStation.h"
#include "context/OCP_CpObject.h"
#include "context/OCP_CpModel.h"
#include "constraints/OCP_CpInternalConstraint.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpCstIntTimetable.h"

using namespace std;

static const std::string _Clazz = ocp::solver::loggerInst + "." + stringify(OCP_CpTrackingResource);

/**
 * constructeur d'une ressource de tracking (satellite ou station)
 * @param newName nom de la ressource
 * @param scheduled : la ressource est à planifier par le moteur
 * @return l'instance de l'objet créé
 */
OCP_CpTrackingResource::OCP_CpTrackingResource(string newName, bool scheduled):
        OCP_CpObject(newName),_hasSat4(false),_scheduled(scheduled)
{
	OCP_CpModel* cpModel = OCP_CpModel::getInstance();
	IloEnv env = cpModel->getEnv();

	_trackingSlots = IloAnyArray(env);
	_ocpConstraints = IloConstraintArray(env);
	_trackingByOrigin.clear();
}

/**
* rajout d'un créneau de passage satellite
* @param le nouveau créneau de passage satellite
*/
void OCP_CpTrackingResource::addTrackingSlot(OCP_CpTrackingSlot* slot)
{
    _trackingSlots.add(slot);
    _trackingByOrigin.insert(std::map <  OcpSatelliteSlotPtr , OCP_CpTrackingSlot* >::value_type( slot->getSlotOrigin() , slot ));
}

/**
* indique si un créneau satellite fait partie des slots concernés par la ressource
* @param slot : le créneau satellite
* @return vrai ssi le slot est renseigné sur la map des slots concernés par la ressource
*/
bool OCP_CpTrackingResource::containsTrackingSlots(OcpSatelliteSlotPtr slot) const
{
    return _trackingByOrigin.find(slot) != _trackingByOrigin.end();
}

/**
* création de la variable ensembliste de domaine l'ensemble des passages satellites pour cette ressource
*/
void OCP_CpTrackingResource::createTrackingSlotVar()
{
	OCP_CpModel* cpModel = OCP_CpModel::getInstance();
	IloEnv env = cpModel->getEnv();

	_trackingSlotSet = IloAnySetVar(env, _trackingSlots,("TRACKING_"+getName()).c_str());
	cpModel->getModel().add(_trackingSlotSet);
	//if(isSatellite() && isScheduled())
	  //  cpModel->addOneSetVarToTrace(_trackingSlotSet);
}

/**
 * création des contraintes internes ILOG SOLVER propres à cette ressource
 * Cette méthode traite selon la portée (station ou satellite) la liste des trackingSlot vus côté station ou côté satellite
 *  IloLinkSlotConstraint indique que dès qu'un élèment <Sat,Sta> est requis dans ce trackingSlotSet, alors il est requis dans le
 *  trackingSlotSet de l'objet symétrique (la station Sta si l'objet est Sta et vice versa)
 */
void OCP_CpTrackingResource::addInternalConstraints()
{
	OCP_CpModel* cpModel = OCP_CpModel::getInstance();
	IloEnv env = cpModel->getEnv();
	IloAnySetVar trackingSlot = getTrackingSlotSet();

	//cpModel->getModel().add(IloNonUbiquityIntraConstraint(env, trackingSlot));//non : satellite n'est pas ressource disjonctive
	//cpModel->getModel().add(IloSumSlotConstraint(env, trackingSlot, _trackingDuration)); déplacé sur la station, faux ici !
	cpModel->getModel().add(IloLinkSlotConstraint(env, trackingSlot));
	cpModel->getModel().add(IloLinkGlobalSlotConstraint(env, trackingSlot));

}

/**
 * affichage console du domaine de la variable ensembliste des passages de créneaux satellites
 * @param solver solver ILOG SOLVER lié à la résolution
 */
void OCP_CpTrackingResource::displayTrackingSlotVar(IloSolver solver)
{
    boost::format logMsg;
    boost::format slotList;

	IloAnySetVar act = getTrackingSlotSet();

	for(IlcAnySetIterator it(solver.getAnySetVar(act).getRequiredSet()); it.ok(); ++it)
	{
		OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*)(*it);
		slotList = boost::format("%1$s passage de %2$s pendant [%3$i; %4$i], ")
                % slotList % ts->getSatellite()->getName() % ts->getBegin() % ts->getEnd();
	}

    logMsg = boost::format(_("Passages satellites : %1$s")) % slotList;
	OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, logMsg);
}

/**
* Sauvegarde des slots satellite d'une ressource
* @param solver ILOG SOLVER lié à la résolution
* @param fatherSplits : map des slots splittés sur cette station
* @return nombre de slots de trackings sauvegardés
*/
int OCP_CpTrackingResource::saveTrackingSlots(IloSolver solver,std::map<OCP_CpTrackingSlot*, OCP_CpTrackingSlot*>& fatherSplits)
{
    OcpContextPtr context = OcpXml::getInstance()->getContext();

    IloAnySetVar act = getTrackingSlotSet();
    IlcAnySet requiredSet = solver.getAnySetVar(act).getRequiredSet();
    IlcAnySet possibledSet = solver.getAnySetVar(act).getPossibleSet();

    int nbSavedTrackings = requiredSet.getSize();

    /*
     * test entre taille des requis et taille des possibles pour vérifier qu'il n'y ait pas de slots non encore alloués
     */
    OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, boost::format(_("%1$d slots de tracking à sauver ")) %  nbSavedTrackings);
    if (possibledSet.getSize() != requiredSet.getSize())
    {
        boost::format msg = boost::format( _("Erreur possible requis de taille %1$i possible %2$i") ) % requiredSet.getSize() % possibledSet.getSize()  ;
        OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg.str() );
        for(IlcAnySetIterator it(possibledSet); it.ok(); ++it)
        {
            if (!requiredSet.isIn(*it))
            {
                OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*)(*it);
                msg = boost::format( _("Slot non instancié %1$s") ) % ts->getFullName().str() ;
                OcpLogManager::getInstance()->error(ocp::solver::loggerInst, msg.str() );
            }
        }
    }


    /*
     * sauvegarde des créneaux existants hors split et renseignement des splits slots origine quand ils ont au moins un fils à sauver
     */
    for(IlcAnySetIterator it(requiredSet); it.ok(); ++it)
    {
        OCP_CpTrackingSlot* ts = (OCP_CpTrackingSlot*)(*it);
        saveOneTrackingSlot(ts,fatherSplits);
    }

    return nbSavedTrackings;
}

/**
 * sauvegarde d'un créneau satellite sur la ressource
 * @param slot_ slot à sauvegarder
 * @param fatherSplits : map des slots splittés sur cette station
 * @return vrai ssi il ne s'agit pas d'un split slot
 */
bool OCP_CpTrackingResource::saveOneTrackingSlot(OCP_CpTrackingSlot* slot_,std::map<OCP_CpTrackingSlot*, OCP_CpTrackingSlot*>& fatherSplits)
{

    if(slot_->getFatherSplit() != NULL)
    {
        fatherSplits.insert(std::map <  OCP_CpTrackingSlot* , OCP_CpTrackingSlot* >::value_type( slot_->getFatherSplit() , slot_->getFatherSplit() ));
        /*
         * pour les slots de split, il y a traitement spécifique une fois qu'ils sont tous collectés
         */
        return false;
    }
    else
    {
        OcpContextPtr context = OcpXml::getInstance()->getContext();
        OcpSatelliteSlotPtr origine = slot_->getSlotOrigin();

        boost::format msg = boost::format( _("Sauvegarde du satellite slot %1$s") ) % slot_->getFullName().str();
        OcpLogManager::getInstance()->debug(ocp::solver::loggerInst, msg.str() );

        /*
         * notification des TYPE_CRENEAU et NIVEAU_CRENEAU par les contraintes ad-hoc SAT1 ou SAT8
         */
        slot_->postOptim();

        return true;

    }

}


/**
 * affichage console du domaine de la variable ensembliste des passages de créneaux satellites
 * @param solver solver ILOG SOLVER lié à la résolution
 */
void OCP_CpTrackingResource::printSolution(IloSolver solver)
{
	displayTrackingSlotVar(solver);
}

/**
 * algorithme de suppression des créneaux superflus (artf618698)
 * @param originSlots : les slots candidats au filtrage pour la ressource
 * @return : renvoie le nombre de créneaux supprimés
 */
int OCP_CpTrackingResource::removeUselessSlots(IloAnyArray originSlots)
{

    static const std::string pseudoClazz = _Clazz + "::removeUselessSlots";

    IloEnv env = OCP_CpModel::getInstance()->getEnv();

    /*
     * initialiser le domaine des possibles
     */
    IloAnySet toRemove( env , originSlots );

    for(VectorOCP_CpConstraint::iterator it = _cpReserveConstraints.begin() ; it != _cpReserveConstraints.end() ; it++ )
    {
        OCP_CpConstraintPtr aCtr = (*it);

        if (!aCtr->isReserveConstraints())
        {
            boost::format msg = boost::format( _("%1$s n'est pas candidate à la suppression des créneaux superflus" ) ) % aCtr->getName();
            OcpLogManager::getInstance()->debug(pseudoClazz, msg );
            throw OCP_XRoot( msg.str() );
        }
        else
        {
            IloAnyArray domain = aCtr->getDomainSlots();
            if (domain.getImpl() != NULL)
            {
                for(int i = 0; i < domain.getSize(); i++)
                {
                    OCP_CpTimeSlot* aSlot = (OCP_CpTimeSlot*) domain[i];
                    toRemove.remove(aSlot);
                }
            }
        }
    }

    int nbRemove = toRemove.getSize();
    boost::format msg = boost::format( _("%1$d slots superflus supprimés pour  %2$s " ) ) % nbRemove % getName();
    OcpLogManager::getInstance()->debug(pseudoClazz, msg );

    IloAnySetIterator it(toRemove);
    while(it.ok())
    {
        OCP_CpTimeSlot* aSlot = (OCP_CpTimeSlot*) (*it);
        if(!aSlot->isReserved())
        {
            aSlot->setToRemove();
            msg = boost::format( _("superflu : %1$s  " ) ) % aSlot->getFullName() ;
            OcpLogManager::getInstance()->debug(pseudoClazz, msg );
        }
        ++it;
    }

    return nbRemove;

}

/**
 * ajout d'une nouvelle contrainte OCP à la liste des contraintes de la ressource
 * @param aCtr la nouvelle contrainte OCP
 */
void OCP_CpTrackingResource::storeOcpConstraintOrigin(OCP_CpConstraintPtr aCtr)
{
    static const std::string pseudoClazz = _Clazz + "::storeOcpConstraintOrigin";

    _cpConstraintsOrigin.push_back(aCtr) ;
    if (aCtr->isReserveConstraints())
    {
        _cpReserveConstraints.push_back(aCtr);
    }
}
