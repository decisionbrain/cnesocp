/*
 * $Id: OCP_CpManager.cpp 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpManager.cpp
 *
 */
#include <string>
#include <sstream>

#include "OCP_CpDate.h"
#include "OCP_CpManager.h"


OCP_CpManager::OCP_CpManager()
{
	instanceFlag = true;
    _idCpt = 0;
}

OCP_CpManager::~OCP_CpManager() {
	instanceFlag = false;
}

OCP_CpManager* OCP_CpManager::getInstance()
{
    if(! instanceFlag) {
        cpManager = new OCP_CpManager();
        instanceFlag = true;
        return cpManager;
    }
    else {
        return cpManager;
    }
}

