/*
 * $Id: OCP_CpSatellite.h 942 2010-12-21 13:11:22Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatellite.h
 *
 */

#ifndef OCP_CpSatellite_H_
#define OCP_CpSatellite_H_

#include <string>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>
#include <resources/OcpSatellite.h>
#include <planif/OCP_CpTimeSlot.h>

#include "resources/OCP_CpTrackingResource.h"

using namespace std ;
using namespace ocp::commons::io;

class OCP_CpStation;
class OCP_CpSatelliteConstraint8;
//class OCP_CpTrackingSlot;
//typedef  boost::shared_ptr< OCP_CpTrackingSlot > OCP_CpTrackingSlotPtr;

class OCP_CpSatellite : public OCP_CpTrackingResource
{
private:

    /**
     * Référence à l'objet origine
     */
    OcpSatellitePtr _satellite;


    /**
     * accès à l'élévation renseignée par SAT4
     */
    std::map<OcpStationPtr,float> _sat4Elevation;

    /**
     * accès à la durée min renseignée par SAT4
     */
    std::map<OcpStationPtr,int> _sat4MinDuration;

    /**
     * en l'absence de SAT4, on ne peut créer les créneaux satellite correctement pour un satellite
     */
    bool _hasSat4Constraint;

    /**
    * plages horaires où la contrainte SAT1 permet des visibilités partielles
    */
    std::vector <  boost::shared_ptr < OCP_CpTrackingSlot > > _splitZones;

    /**
    * collection des id des contraintes SAT1 avant la partition des slots concernés
    */
    std::vector<int> _sat1Ids;

    /**
     * collection des variables ensemblistes sur lesquelles des contraintes SAT1Min sont posées
     */
    std::vector<IloAnySetVar> _sat1StableCandidates;

    /**
     * bornes min des contraintes SAT1
     */
    std::vector<int> _sat1StableLbs;

    /**
     * liste des domaines des sat1StableCandidates
     */
    std::vector<IloAnyArray> _sat1StableDomains;

    /**
     * pour tout slot candidat à une SAT1, indique pour combien de contraintes il est candidat
     */
    std::map<IloAny,int> _nbSat1BySlot;

    /**
     * indique pour chaque contrainte SAT1 si elle contient des candidats vus par d'autres SAT1
     */
    std::vector<int> _sat1StableConflicts;


protected:
    /**
    *
    * @return vrai ssi la ressource est un satellite
    */
    bool isSatellite() {return true;}

    /**
    *
    * @return vrai ssi la ressource est une station
    */
    bool isStation() {return false;}


public:

    /**
     * constructeur de l'objet utilisé par la CP
     * @param satellite_
     * @param scheduled : la ressource est à planifier par le moteur
     * @return
     */
    OCP_CpSatellite(OcpSatellitePtr satellite_, bool scheduled);


    /**
     * en l'absence de SAT4, on ne peut créer les créneaux satellite correctement pour un satellite
     * @return vrai ssi on a une contrainte SAT4 positionnée pour ce satellite
     */
    bool hasSat4Constraint() const {return _hasSat4Constraint;}

    /**
     * notifie qu'il existe une contrainte SAT4 pour ce satellite
     */
    void setHasSat4Constraint() {_hasSat4Constraint = true;}


    /**
     * mémorisation des propriétés d'une station après lecture de la contrainte SAT4 pour le satellite
     * @param properties : les propriétés vues lors de la lecture de la contrainte SAT4 pour le satellite
     */
    void storeOcpStationProperties(OcpStationPropertiesPtr stationProperty);

    /**
     * Destructeur de l'objet
     * @return
     */
    virtual ~OCP_CpSatellite() {};

    /**
     * accès au satellite origine côté façade
     * @return le satellite origine côté façade
     */
    OcpSatellitePtr getOrigin() const {return _satellite;}

    /**
     * caractérisation des géostationnaires
     * @return vrai ssi le satellite est géostationnaire
     */
    bool isGeoStationary() const {return getOrigin()->getType() == SatelliteTypes::GEO;}


    /**
      * accède via un cache aux informations renseignées par SAT4
      * @param aStation la station sur laquelle on recherche les propriétés
      * @param minElevationProperty propriété d'élévation min à renseigner
      * @param minDuration propriété de durée min à renseigner
      */
     void getSat4ElevationAndDuration(OcpStationPtr aStation, float& minElevationProperty ,int& minDuration) const;

     /**
       * ajout d'une période "SAT1-visibilité partielle" où l'on devra splitter les créneaux
       * @param zone
       */
      void addSplitZone(boost::shared_ptr < OCP_CpTrackingSlot > zone);

      /**
       * generation de nouveaux slots splittés depuis des slots intersectant une zone de split
       * @param splitCandidates : liste aggrégée des nouveau slots splittés sur l'ensemble des satellites
       */
      void generateSplitSlots(std::vector <  OCP_CpTrackingSlot* >& splitCandidates);

      /**
      * ajout d'un nouveau tracking slot
      * @param slotOrigine : slot origine qu'on va découper sur la zone de split
      * @param splitCandidates liste des nouveaux tracking slots
      * @param area intervalle de temps potentiel incluant la reconf / déconf
      * @param reconf temps de reconfiguration de l'antenne
      * @param deconf temps de déconfiguration de l'antenne
      * @return le nouveau slot splitté créé
      */
      OCP_CpTrackingSlot* addOneSplitTrackingSlot(OCP_CpTrackingSlot* slotOrigine,std::vector <  OCP_CpTrackingSlot* >& splitCandidates,const OCP_CpTimeSlot& area,uint32_t reconf, uint32_t deconf);

      /**
       * ajout à la collection des informations sur les contraintes SAT1Min
       * @para idCtr id de la contrainte SAT1 origine
       * @param sat1Var variable ensembliste sur laquelle la contrainte IloCard est posée
       * @param sat1Lb borne min de la contrainte SAT1
       * @param sat1Domain slots candidats (domaine de sat1Var après propagation de l'intersection)
       */
      void pushStableCandidate(int idCtr, IloAnySetVar sat1Var, int sat1Lb, IloAnyArray sat1Domain);


      /**
       * calcule une partition des créneaux collectés via les SAT1. Pour chaque créneau, on comptabilise son
       * nombre d'occurences. On regarde la contrainte pour laquelle il y a le plus de conflits en sommant le nombre
       * d'occurences >1 de chacun des créneaux de cette contrainte. Si ce nombre de conflits est non nul, on enlève
       * la contrainte de la liste et on remet à jour les nombres d'occurence pour itérer le processus
       * TODO cette version est assez violente car on peut avoir des contraintes englobantes avec beaucoup de conflits
       * qui seraient meilleures à conserver que l'union des contraintes disjointes qu'elle intersecte
       */
      void partitionStable();

      /**
       * Après partition des créneaux apparaissant dans des SAT1, collecte l'ensemble des variables ensemblistes
       * correspondant à ces SAT1, l'union des domaines de ces variables
       * @param vars variables ensemblistes liées aux SAT1 candidates à la recherche d'un stable max
       * @param allslots union des slots apparaissant dans toutes les vars (multi-satellites)
       * @return la somme des cardinalités minimales des SAT1 pour le satellite courant
       */
      int collectStable(IloAnySetVarArray vars,IloAnyArray allslots);

	
protected:

      /**
      * collecte des slots concernés par une zone de split. Note : si la station n'est pas renseignée sur la zone de split, tout créneau
      * intersectant la zone, quelle que soit sa station, sera candidate
      * @param candidates : renseigné par la méthode, liste des candidats collectés
      * @param aos : par station, liste des aos de split
      * @param los : par station, liste des los de split
      */
      void collectCandidates(std::vector<OCP_CpTrackingSlot*>& candidates, std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los)
;

      /**
      * Initialisation des tableaux aos los pour chaque station concernée par un créneau à splitter
      * @param candidates : candidats en entrée intersectant des zones de split
      * @param aos : par station, liste des aos de split
      * @param los : par station, liste des los de split
      * @param stations : la liste des stations rencontrées sur les différents candidats
      */
      void initializeAosLos(std::vector<OCP_CpTrackingSlot*>&candidates , std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los,std::map<boost::shared_ptr < OCP_CpStation >,boost::shared_ptr < OCP_CpStation > >& stations);

      /**
       * impacter le passage des slots défilants. Un créneau défilant C intersectant un créneau candidat (les deux créneaux sur la même station)
       * notifie le tableau los sur cette station avec los = C-temps de déconfiguration du satellite courant sur cette station
       * @param candidates
      * @param aos : par station, liste des aos de split
      * @param los : par station, liste des los de split
       */
      void updateAosLos(std::vector<OCP_CpTrackingSlot*>&candidates , std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos, std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los);

      /**
       * tri chronologique et suppression des doublones des aoslos des nouveaux slots splittés à créer
      * @param aos : par station, liste des aos de split
      * @param los : par station, liste des los de split
       */
      void sortAosLos(std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos, std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los);


      /**
      * création des nouveaux slots splittés
      * @param candidates : candidats origine pour le split
      * @param splitCandidates : liste aggrégée des nouveau slots splittés sur l'ensemble des satellites
      * @param aos : par station, liste des aos de split
      * @param los : par station, liste des los de split
      */
      void applyAosLos(std::vector<OCP_CpTrackingSlot*>&candidates ,std::vector <  OCP_CpTrackingSlot* >& splitCandidates,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos, std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los);


      /**
       * ajout à la structure d'un nouvel aos
       * @param aStation station sur laquelle on mémorise le nouvel aos
       * @param newaos nouvel aos de début de split
       * @param aos map des aos par station
       */
      void pushAos(boost::shared_ptr < OCP_CpStation > aStation,OCP_CpDate newaos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos);

      /**
       * ajout à la structure d'un nouvel los
       * @param aStation station sur laquelle on mémorise le nouvel aos
       * @param newlos nouvel aos de début de split
       * @param los map des aos par station
       */
      void pushLos(boost::shared_ptr < OCP_CpStation > aStation,OCP_CpDate newlos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los);

      /**
       * tenir compte des points de temps significatifs de la contrainte SAT8 pour mettre à jour les aos los potentiels dans le découpage des split slots
       * @param aStation : station concernée par le découpage
       * @param cstrSat8 : contrainte SAT8 origine
       * @param aos : par station, liste des aos de split
       * @param los : par station, liste des los de split
       */
      void loopSat8Infos(boost::shared_ptr < OCP_CpStation > aStation, boost::shared_ptr <OCP_CpSatelliteConstraint8> cstrSat8,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los);

      /**
       * parcours des contraintes SAT8 du satellite courant pour notifier de nouveaux points aos los sur les stations concernées
       * @param aos : par station, liste des aos de split
       * @param los : par station, liste des los de split
       * @param stations : liste des stations vues sur les candidats au split
       */
      void addAdditionalSat8Points(std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& aos,std::map<boost::shared_ptr < OCP_CpStation >, VectorOCP_CpDate*>& los,std::map<boost::shared_ptr < OCP_CpStation >,boost::shared_ptr < OCP_CpStation > >& stations);


};

typedef boost::shared_ptr < OCP_CpSatellite > OCP_CpSatellitePtr;
typedef std::map<OcpSatellitePtr, OCP_CpSatellitePtr> SatelliteMap;
typedef std::list<OCP_CpSatellitePtr> SatelliteList;



#endif /* OCP_CpSatellite_H_ */
