/*
 * $Id: OCP_CpTrackingResource.h 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpTrackingResource.h
 *
 */

#ifndef OCP_CpTrackingResource_H_
#define OCP_CpTrackingResource_H_

#include <string>
#include <list>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

#include "context/OCP_CpObject.h"
//#include "planif/OCP_CpTrackingSlot.h"
//#include "planif/OCP_CpMaintenanceSlot.h"
#include "OCP_CpDate.h"
#include <constraints/OCP_CpConstraint.h>

using namespace std ;
using namespace ocp::commons::io;

typedef list<OCP_CpDate> dateList;

class OCP_CpTrackingSlot;

class OCP_CpTrackingResource : public OCP_CpObject
{
private:
    /**
     * variable ensembliste dont le domaine est constitué des slots de passage satellite
     */
	IloAnySetVar _trackingSlotSet;

	/**
	 * La liste des contraintes OCP associées à un satellite
	 */
	IloConstraintArray _ocpConstraints;

	/**
	 * indique pour une ressource satellite qu'une contrainte SAT4 a été positionnée
	 */
	bool _hasSat4;



protected:

	/**
     * Liste des contraintes OCP pour ce satellite
     */
    VectorOCP_CpConstraint _cpConstraintsOrigin;


    /**
     * Liste des contraintes OCP qui réservent des créneaux pour ce satellite : SAT1, SAT6, SAT8
     */
    VectorOCP_CpConstraint _cpReserveConstraints;

    /**
	 * liste des créneaux
	 */
 	IloAnyArray _trackingSlots;

 	/**
 	 * map des créneaux
 	 */
 	std::map <  OcpSatelliteSlotPtr , OCP_CpTrackingSlot* > _trackingByOrigin;

 	/**
 	 * indique si la ressource est à planifier par le moteur
 	 */
 	bool _scheduled;


protected:

 	/**
 	 *
 	 * @return vrai ssi la ressource est un satellite
 	 */
 	virtual bool isSatellite() = 0;

 	/**
 	 *
 	 * @return vrai ssi la ressource est une station
 	 */
 	virtual bool isStation() = 0;

 	/**
 	 * sauvegarde d'un créneau satellite sur la ressource
 	 * @param slot_ slot à sauvegarder
 	 * @param fatherSplits : map des slots splittés sur cette station
 	 * @return vrai ssi il ne s'agit pas d'un split slot
 	 */
 	bool saveOneTrackingSlot(OCP_CpTrackingSlot* slot_,std::map<OCP_CpTrackingSlot*, OCP_CpTrackingSlot*>& fatherSplits);




public:

 	/**
 	 * constructeur d'une ressource de tracking (satellite ou station)
 	 * @param newName nom de la ressource
 	 * @param scheduled : la ressource est à planifier par le moteur
 	 * @return l'instance de l'objet créé
 	 */
	OCP_CpTrackingResource(string newName, bool scheduled);

	/**
	 * la ressource est-elle à planifier par le moteur ?
	 * @return vrai ssi la ressource est à planifier par le moteur
	 */
	bool isScheduled() const {return _scheduled;}

	/**
	 * accès à la variable ensembliste de tracking
	 * @return la variable ensembliste de tracking
	 */
	IloAnySetVar getTrackingSlotSet() {return _trackingSlotSet;}

	/**
	 * accès aux slots possibles du point de vue ILOG SOLVER
	 * @return les slots possibles du point de vue ILOG SOLVER
	 */
	IloAnyArray getTrackingSlots() {return _trackingSlots;}

	/**
	 * accès à la liste des contraintes OCP propres à cette ressource
	 * @return la liste des contraintes OCP propres à cette ressource
	 */
	IloConstraintArray getOcpConstraints(){return _ocpConstraints;}

	/**
	 * ajout d'une contrainte OCP à la ressource
	 * @param c la nouvelle contrainte OCP
	 */
	void addOcpConstraint(IloConstraint c){_ocpConstraints.add(c);}

	/**
	 * rajout d'un créneau de passage satellite
	 * @param le nouveau créneau de passage satellite
	 */
	virtual void addTrackingSlot(OCP_CpTrackingSlot*);

	/**
	 * indique si un créneau satellite fait partie des slots concernés par la ressource
	 * @param slot : le créneau satellite
	 * @return vrai ssi le slot est renseigné sur la map des slots concernés par la ressource
	 */
	bool containsTrackingSlots(OcpSatelliteSlotPtr slot) const;

	/**
	 * création de la variable ensembliste de domaine l'ensemble des passages satellites pour cette ressource
	 */
	virtual void createTrackingSlotVar();

	/**
	 * création des contraintes internes ILOG SOLVER propres à cette ressource
	 * Cette méthode traite selon la portée (station ou satellite) la liste des trackingSlot vus côté station ou côté satellite
	 *  IloLinkSlotConstraint indique que dès qu'un élèment <Sat,Sta> est requis dans ce trackingSlotSet, alors il est requis dans le
	 *  trackingSlotSet de l'objet symétrique (la station Sta si l'objet est Sta et vice versa)
	 */
	virtual void addInternalConstraints();

	/**
	 * affichage console du domaine de la variable ensembliste des passages de créneaux satellites
	 * @param solver solver ILOG SOLVER
	 */
	void displayTrackingSlotVar(IloSolver);

	/**
	 * affichage console du domaine de la variable ensembliste des passages de créneaux satellites
	 * @param solver solver ILOG SOLVER lié à la résolution
	 */
	virtual void printSolution(IloSolver);

	/**
	* Sauvegarde des slots satellite d'une ressource
	* @param solver ILOG SOLVER lié à la résolution
	* @param fatherSplits : map des slots splittés sur cette station
	* @return nombre de slots de trackings sauvegardés
	*/
	int saveTrackingSlots(IloSolver solver,std::map<OCP_CpTrackingSlot*, OCP_CpTrackingSlot*>& fatherSplits);

    /**
    * accès à la liste des contraintes satellites réservant des créneaux pour le satellite courant : SAT1, SAT7, SAT8
    * @return la liste des contraintes satellites réservant des créneaux pour le satellite courant
    */
    VectorOCP_CpConstraint& getOcpReserveConstraint() {return _cpReserveConstraints;}


    /**
     * algorithme de suppression des créneaux superflus (artf618698)
     * @param originSlots : les slots candidats au filtrage pour la ressource
     * @return : renvoie le nombre de créneaux supprimés
     */
    int removeUselessSlots(IloAnyArray originSlots);

    /**
     * ajout d'une nouvelle contrainte OCP à la liste des contraintes de la ressource
     * @param aCtr la nouvelle contrainte OCP
     */
    void storeOcpConstraintOrigin(OCP_CpConstraintPtr aCtr);

    /**
     * accès à la liste des contraintes satellites du satellite courant
     * @return la liste des contraintes satellites du satellite courant
     */
    VectorOCP_CpConstraint& getOcpConstraintOrigins() {return _cpConstraintsOrigin;}



};

#endif /* OCP_CpTrackingResource_H_ */
