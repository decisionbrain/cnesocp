/*
 * $Id: OCP_CpStation.h 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStation.h
 *
 */

#ifndef OCP_CpStation_H_
#define OCP_CpStation_H_

#include <string>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>
#include <resources/OcpStation.h>

#include "resources/OCP_CpSatellite.h"
//#include "planif/OCP_CpTrackingSlot.h"
//#include "planif/OCP_CpMaintenanceSlot.h"
#include "planif/OCP_CpTimeSlot.h"



using namespace std ;
using namespace ocp::commons::io;

//class OCP_CpTimeSlot;
class OCP_CpTrackingSlot;
class OCP_CpMaintenanceSlot;
class OCP_CpSatellite;


class OCP_CpStation : public OCP_CpTrackingResource
{
private:

    /**
    * Station origine dans le modèle des objets métiers
    */
    OcpStationPtr _station;

    /**
    * Variable ensembliste ayant comme possible l'ensemble des slots de maintenance sur l'horizon
    */
    IloAnySetVar _maintenanceSlotSet;

    /**
    * variable entière cumulant la durée de maintenance sur la fenêtre de calcul
    */
    IloIntVar _maintenanceDuration;

    /**
    * variable entière cumulant la durée de passage satellite sur la fenêtre de calcul
    */
    IloIntVar _trackingDuration;


    /**
    * ensemble des slots de maintenance sur l'horizon
    */
    IloAnyArray _maintenanceSlots;

    /**
     * ensemble des slots de maintenance sur l'horizon de type Booking Order
     */
    IloAnyArray _maintenanceSlotsWithBookingOrders;

    /**
    * Dates de début possibles de tous les slots de maintenance
    */
    std::vector<OCP_CpDate> _beginsForMaintenance;

    /**
    * Dates de fin possibles de tous les slots de maintenance
    */
    std::vector<OCP_CpDate> _endsForMaintenance;

    /**
    * débuts de tous les passages satellites concernés par la station
    */
    std::vector<OCP_CpDate> _beginTrackings;

    /**
    * fin de tous les passages satellites concernés par la station
    *
    */
    std::vector<OCP_CpDate> _endTrackings;

    /**
     * accès au temps de reconfiguration antenne par satellité
     */
    std::map<OCP_CpSatellite*,uint32_t> _reconf;

    /**
     * accès au temps de deconfiguration antenne par satellité
     */
    std::map<OCP_CpSatellite*,uint32_t> _deconf;

protected:
    /**
    *
    * @return vrai ssi la ressource est un satellite
    */
    bool isSatellite() {return false;}

    /**
    *
    * @return vrai ssi la ressource est une station
    */
    bool isStation() {return true;}


public:
    /**
    * constructeur avec pointeur sur l'objet métier origine
    * @param station_ la station origine dans le modèle métier
    * @param scheduled : la ressource est à planifier par le moteur
    * @return
    */
    OCP_CpStation(OcpStationPtr station_, bool scheduled);

    /**
    * lien vers l'objet métier associé
    * @return l'origine métier de l'objet courant
    */
    OcpStationPtr getOrigin() const {return _station;}

    /**
    * Destructeur de l'objet
    * @return
    */
    virtual ~OCP_CpStation() {};


    /**
    * renvoie la variable ensembliste associée aux slots de maintenance
    * @return la variable ensembliste associée aux slots de maintenance
    */
    IloAnySetVar getMaintenanceSlotSet() {return _maintenanceSlotSet;}

    /**
    * accès aux slots de maintenance
    * @return la collection ILOG des slots de maintennance
    */
    IloAnyArray getMaintenanceSlots() {return _maintenanceSlots;}

    /**
    * accès aux slots de maintenance
    * @return la collection ILOG des slots de maintennance
    */
    IloAnyArray getMaintenanceSlotsWithBookingOrder() {return _maintenanceSlotsWithBookingOrders;}

    /**
    * la variable cumulant la durée de maintenance sur la fenêtre de calcul
    * @return variable entière Solver
    */
    IloIntVar getMaintenanceDuration() {return _maintenanceDuration;}

    /**
    * la variable cumulant la durée de passage satellite sur la fenêtre de calcul
    * @return variable entière Solver
    */
    IloIntVar getTrackingDuration() {return _trackingDuration;}


    /**
    * rajout d'un slot de maintenance à la liste des slots de maintenance
    * @param slot nouveau slot de maintenance
    */
    void addMaintenanceSlot(OCP_CpMaintenanceSlot* maintenanceSlot);

    /**
    * rajout d'un slot de satellite  à la liste des slots satellite
    * @param trackingSlot nouveau slot de passage satellite
    */
    virtual void addTrackingSlot(OCP_CpTrackingSlot* trackingSlot);

    /**
     * une fois le début fin renseigné par SAT4, on rajoute sur la grille des temps les débuts fins de tracking et maintenance
     * @param trackingSlot : le nouveau slot dont les débuts-fins (tracking et start-end) sont à prendre en compte
     */
    void addTimePoints(OCP_CpTrackingSlot* trackingSlot);

    /**
     * ajout d'une nouvelle plage de maintenance potentielle [aEndForMaintenance , aBeginForMaintenance]
     * @param aEndForMaintenance : le début de la plage de maintenance potentielle
     * @param aBeginForMaintenance : la fin de la plage de maintenance potentielle
     */
    void addSpecialTimePoints(OCP_CpDate aEndForMaintenance, OCP_CpDate aBeginForMaintenance);


    /**
    * préparation des structures de données listant les points debut fin des periodes "ouverture-fermeture" où la station
    * peut être en maintenance (présence d'opérateurs)
    * @param ofs  la liste des périodes ouverture fermeture
    * @return vrai ssi on a lieu de créer des slots de maintenance sur cette station
    */
    bool prepareMaintenanceSlots(VectorOCP_CpTimeSlot& ofs);



    /**
    * génération des slots de maintenance pour la station. On tient compte des passages satellites et des heures de dispo opérateurs
    * attention, ceci doit être appelé après avoir renseigné tous les débuts fins possibles des slots satellites défilants
    * @param ofs : liste en entrée des points d'ouverture-fermeture station
    */
    void generateMaintenanceSlots(VectorOCP_CpTimeSlot& ofs);

    /**
    * création de la variable ensembliste ayant pour domaine les slots de maintenance possibles pour la station
    * @return le nombre de slots de maintenance
    */
    int createMaintenanceSlots();

    /**
    * affichage des slots de maintenance possibles
    */
    void printMaintenanceSlots();

    /**
    * Sauvegarde des slots de maintenance
    * @param solver : solver PPC ILOG
    * @return nombre de créneaux de maintenance sauvegardés
    */
    int saveMaintenanceSlots(IloSolver solver) ;

    /**
    * rajoute les contraintes internes
    * non ubiquité
    * durée cumulée de maintenance
    * lien avec la variable globale
    */
    virtual void addInternalConstraints();

    /**
    * affichage des maintenances requises dans la solution courante
    * @param solver le solver ILOG SOLVER
    */
    void displayMaintenanceSlotVar(IloSolver);

    /**
    * affichage de la solution (maintenance et tracking)
    * @param solver le solver ILOG SOLVER
    */
    virtual void printSolution(IloSolver);

    /**
    * calcule la somme des durées des slots possibles entre deux dates
    * @param begin début de la fenêtre de calcul
    * @param end fin de la fenêtre de calcul
    * @return le cumul des durées d'intersection avec la fenêtre
    */
    OCP_CpDate getPossibleMaintenanceDuration(OCP_CpDate begin, OCP_CpDate end) const;

    /**
    * calcule le nombre de slots d'une durée minimale dans la fenêtre de calcul
    * @param begin début de la fenêtre de calcul
    * @param end fin de la fenêtre de calcul
    * @param filterMinDuration filtre de durée minimale sur la partie intersectant la fenêtre
    * @return le nombre de slots candidats
    */
    int getNbPossibleMaintenanceCandidates(const OCP_CpDate begin, OCP_CpDate end, OCP_CpDate filterMinDuration) const;


    /**
     * sauvegarde des slots de la station
     * @param solver solver de la station
     * @param nbTrackings nombre de slots de tracking sauvegardés
     * @param nbMaintenances nombre de slots de maintenance sauvegardés
     */
    void saveSlots(IloSolver solver,int& nbTrackings,int& nbMaintenances);

    /**
    *
    * @return fréquence associée à la station
    */
    FrequencyBands::FrequencyBands_ENUM getStationFrequencyBand() const;

    /**
    * vérification de l'inclusion d'un slot satellite dans une des plages horaires de tracking de sa station
    * @param ocpSatSlot intervalle temporel en secondes du slot origine
    * @return
    */
    bool isTrackable(const OCP_CpTimeSlot& ocpSatSlot);

    /**
    * en lisant les paramètres stations :
    * parcours des zones de tracking possible sur le jour de calcul (avec plages à cheval possible) et notification des dates
    * de début et fin de tracking possible pour calcul ultérieur de split des créneaux SAT1-visibilité partielle
    * @param optimScope : fenêtre de calcul courante
    */
    void notifyTrackingZones(OCP_CpTimeSlotPtr optimScope);

    /**
     * une fois supprimé les slots superflus, on peut calculer une priorité aggrégée pour chaque slot*
     * en tenant compte des priorités de SAT4, STA6 et des conflits potentiels
     */
    void updateTrackingSlotsPriorities();

    /**
     * indique si à la date du jour, la station est fermée : pas de slots de maintenance, pas de pose de contrainte STA1 ou STA2 ce jour
     * @param origin date en secondes correspondant au début de portée jour
     * @return vrai ssi la station est fermée à la date origin
     */
    bool isClosed(OCP_CpDate origin) const;

    /**
     * indique si à la date du jour, la station est ouverte et si les jours suivants dans la semaine, elle ne l'est plus
     * @param origin date en secondes correspondant au début de portée jour
     * @return vrai ssi origin est le dernier jour d'ouverture de la station dans la semaine
     */
    bool isLastOpenDayInItsWeek(OCP_CpDate origin) const;

    /**
     * accès via map des durées de configuration déconfiguration antenne d'un satellite sur une station
     * @param aSatellite : satellite
     * @param reconf : temps de reconfiguration en secondes
     * @param deconf : temps de deconfiguration en secondes
     */
    void getReconfDeconfDuration(OCP_CpSatellite* aSatellite, uint32_t& reconf, uint32_t& deconf);

    /**
     * accès aux points de temps de "début" des intervalles de maintenance
     * @return les points de temps de "début" des intervalles de maintenance
     */
    std::vector<OCP_CpDate> getBeginsForMaintenance() {return _beginsForMaintenance;}

    /**
    * accès aux points de temps de "fin" des intervalles de maintenance
    * @return les points de temps de "fin" des intervalles de maintenance
    */
    std::vector<OCP_CpDate> getEndsForMaintenance() {return _endsForMaintenance;}

protected:

    /**
     * on indique sur chaque slot de maintenance généré s'il intersecte une visibilité satellite sur cette station
     */
    void setOffVisibility();

    /**
     * ajout des créneaux de maintenance pré-reservés par l'utilisateur
     */
    void addReservedMaintenanceSlots();

    /**
     * avant génération des slots de maintenance, on supprime les doublons dans les structure de points de temps associées
     */
    void suppressDuplicateMaintenanceTimePoints();


};

typedef boost::shared_ptr < OCP_CpStation > OCP_CpStationPtr;
typedef std::map<OcpStationPtr, OCP_CpStationPtr> StationMap;
typedef boost::shared_ptr < StationMap >    StationMapPtr;
typedef std::list<OCP_CpStationPtr> StationList;

#endif /* OCP_CpStation */
