/*
 * $Id: OCP_CpExternWrapper.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */


#ifndef OCP_CpExternWrapper_H_
#define OCP_CpExternWrapper_H_


/**
 *  Cet header permet la définition externe des contraintes définies via des wrappers ILOG dans les contraintes
 *  -> si on les met dans les .h des contraintes, il y a des problèmes de référence circulaire quand on passe
 *  l'instance de la classe en paramètre de la méthode créant l' IloConstraint
 *  */

using namespace std;

ILOSTLBEGIN

#include <iostream>
#include <map>
#include <list>
#include <ilsolver/ilosolver.h>
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"
#include <OcpXml.h>
#include "planif/OCP_Solver.h"

//extern IloConstraint IloSatelliteConstraint2(IloEnv, OCP_CpDate, OCP_CpDate, IloAnySetVar, OCP_CpSatelliteConstraint2*, const char* name=0);
extern IloConstraint IloSatelliteConstraint2(IloEnv, IloAnySetVar, OCP_CpDate, const char* name=0);
extern IloConstraint IloSatelliteConstraint2Max(IloEnv, IloAnySetVar, OCP_CpDate, const char* name=0);
extern IloConstraint IloSatelliteConstraint3(IloEnv, OCP_CpDate, OCP_CpDate, IloAnySetVar, OCP_CpDate, SpecialDurationPtr ,const char* name=0);
extern IloConstraint IloSatelliteConstraint4(IloEnv, IloAnySetVar,StationMapPtr,const char* name=0);
extern IloConstraint IloSatelliteConstraint5(IloEnv env, int capaMax, IloAnySetVar slotsSetVar, OCP_CpTimeSlotPtr plageHoraire,const char* name=0);
extern IloConstraint IloSatelliteConstraint5_V2(IloEnv env, int capaMax, IloAnySetVar slotsSetVar, OCP_CpTimeSlotPtr plageHoraire,const char* name=0);
extern IloConstraint IloSatelliteConstraint6(IloEnv, OCP_CpStationPtr, OCP_CpStationPtr,OCP_CpDate, IloAnySetVar, const char* name=0);
extern IloConstraint IloSatelliteConstraint7(IloEnv env, int nbSupports, IloAnySetVar slotsSetVar, OCP_CpTimeSlotPtr plageHoraire,const char* name=0);
extern IloConstraint IloSatelliteConstraint7_v2(IloEnv env, int nbSupports, IloAnySetVar slotsSetVar, OCP_CpTimeSlotPtr plageHoraire,const char* name=0);
extern IloConstraint IloSatelliteConstraint7_v4(IloEnv env, int nbSupports, IloAnySetVar slotsSetVar,OCP_CpTimeSlotPtr plageHoraire,const char* name=0);
extern IloConstraint IloSatelliteConstraint8(IloEnv env, int capaMin, int maxHole,IloAnySetVar slotsSetVar, OCP_CpTimeSlotPtr plageHoraire,const char* name=0);

extern IloConstraint IloStationConstraint1(IloEnv, OCP_CpTimeSlotPtr,  bool , IloAnySetVar, int, int, int,const char* name=0);
extern IloConstraint IloStationConstraint5(IloEnv, OCP_CpTimeSlotPtr, IloAnySetVar, int, const char* name=0);

#endif /* OCP_CpExternWrapper_H_ */
