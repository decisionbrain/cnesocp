/*
 * $Id: OCP_CpObject.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpObject.h
 *
 */

#ifndef OCP_CpObject_H_
#define OCP_CpObject_H_

#include <string>
#include <OcpXml.h>

using namespace std ;

class OCP_CpObject
{
private:
	string _name;
	int _id;
public:
	/**
	 * Constructeur par défaut
	 * @return
	 */
    OCP_CpObject();
    /**
     * Constructeur
     * @param newName
     * @return
     */
    OCP_CpObject(string newName);
    /**
     * Destructeur
     * @return
     */
    virtual ~OCP_CpObject() {};

	void setName(string newName);
	virtual string getName() const {return _name;}
	int getId() const {return _id;}
};

#endif /* OCP_CpObject_H_ */
