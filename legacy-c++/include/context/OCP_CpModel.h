/*
 * $Id: OCP_CpModel.h 947 2011-01-07 14:43:53Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * VERSION : 2-0 : FA : artf660903 : 30/11/2010 : Calcul du planning apr�s r�servation manuelle de cr�neaux NOK (https://coconet2.capgemini.com/sf/go/artf660903/")
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */


#ifndef OCP_CpModel_H_
#define OCP_CpModel_H_

#include <iostream>
#include <map>
#include <list>
#include <ilsolver/ilosolver.h>


#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"
#include "planif/OCP_CpTimeSlot.h"
#include "OCP_CpDate.h"
#include "planif/OCP_Solver.h"
#include "constraints/OCP_CpConstraint.h"
#include "planif/OCP_CpTrackingSlot.h"
#include "constraints/OCP_CpSatelliteConstraint3.h"


#include <OcpXml.h>
#include <PHRDate.h>
#include <OcpParameters.h>

using namespace std ;
using namespace ocp;
using namespace ocp::commons;
using namespace ocp::commons::io;





/**
* La classe OCP_CpModel encapsule le modèle d'optimisation
* Elle englobe tous les obkets Ilog Solver (env, model, solver)
* Les objets satellites et stations sont également contenus dans
* cette classe.
*/

class OCP_CpModel
{
private:
    static bool instanceFlag;

    /**
    * Instance unique (singleton) de l'objet OCP_CpModel
    */
    static OCP_CpModel *_cpModel;

    /**
     * liste chronologique des id de slots branchés (en négatif pour le backtrack)
     */
    std::vector<long> _branchingIds;

    /**
     * liste chronologique des types de slots (true pour tracking , false pour maintenance)
     */
    std::vector<long> _branchingTypes;

    /**
     * numéro des critères de sélection des créneaux ordonnés par priorité croissante
     */
    std::vector<int> _rankedSearchCriterias;

    /**
     * temporaire : permet de basculer entre l'ancien mode (false) et le nouveau (true) pour le chargement de toutes les ressources
     * et non seules celles à planifier
     */
    static bool _loadAllResources;


    /**
    * Environnement solver
    */
    IloEnv _env;

    /**
    * Modèle Solver
    */
    IloModel _model;

    /**
    * Solver Ilog
    */
    IloSolver _solver;

    /**
    * Date de début du calcul
    */
    OCP_CpDate _origin;

    /**
    * Horizon de calcul (date de fin)
    */
    OCP_CpDate _horizon;

    /**
     * commentaire à renseigner sur tous les slots sauvegardés (artf658881)
     */
    std::string _comment;

    /**
     * indique qu'un calcul a été lancé avec succès le jour J-1. Ce flag est remis à jour dans le pilote
     * et est à faux le premier jour du calcul
     */
    bool _solutionDayBefore;

    /**
    * Map des satellites
    */
    SatelliteMap _allSatellites;

    /**
     * pour chaque satellite, quel est l'intervalle maximal déduit de la portée et des contraintes pour charger ses passages
     */
    std::map<OCP_CpSatellitePtr, OCP_CpTimeSlotPtr> _satelliteScopes;

    /**
    * Map des stations
    */
    StationMap _allStations;

    /**
     * Liste des contraintes OCP satellite et stations
     */
    VectorOCP_CpConstraint _cpConstraints;


    /**
    * Liste des contraintes SAT4
    */
    VectorOCP_CpConstraint _cpSAT4Constraints;

    /**
    * Liste des contraintes STA6
    */
    VectorOCP_CpConstraint _cpSTA6Constraints;

    /**
    * Liste des contraintes STA1
    */
    VectorOCP_CpConstraint _cpSTA1Constraints;


    /**
    * Variable globale ayant pour domaine la réunion de tous les slots tracking et maintenance
    */
    IloAnySetVar _globalSlotSetVar;

    /**
     * domaine initial de la variable ensembliste globale
     */
    IloAnyArray _globalSlots;

    /**
     * ensemble de tous les créneaux satellite
     */
    IloAnyArray _allTrackingSlots;

    /**
    * Durée maximale allouée au calcul
    */
    unsigned int _maxComputingDuration;

    /**
    * Variables des créneaux
    */
    IloAnySetVar _globalSlotSet;

    /**
     * pour debug, liste de slots préfixés depuis listener.conf
     */
    std::map<int,int> _fixedSlots;

    /**
     * uniquement pour debug : trace de propagation d'une variable ensembliste donnée
     */
    IloAnySetVarArray _setVarsToTrace;

    /*
     * liste des variables ensemblistes associées aux différentes instances de SAT1
     */
    IloAnySetVarArray _allCardinalitySetVars;

    /**
     * liste des variables de cardinalité (_allCardinalityVars[i] == IloCard(_allCardinalitySetVars[i]) )
     * associées aux différentes instances de SAT1
     */
    IloIntVarArray _allCardinalityVars;

    /**
     * indicateur réversible calculé à un noeud de l'arbre et indiquant s'il reste des contraintes
     * SAT1 localement violées par rapport à leur borne inférieure
     */
    IlcRevBool _hasSlackMinPass;

    /**
     * mémorisation des indices des variables ensemblistes vues dans SAT1 ayant le plus grand potentiel
     * de satisfaire leur borne min couramment violée
     */
    std::map<int,int> _bestSlackMinPassSupport;

    /**
     * indicateur réversible calculé à un noeud de l'arbre et indiquant s'il reste des contraintes
     * SAT3 localement violées par rapport à leur borne inférieure
     */
    IlcRevBool _hasSlackSat3MinDur;

    /**
     * mémorisation des contraintes IloSatelliteConstraint3 pour exploitation ultérieure dans la stratégie de recherche
     */
    IloConstraintArray _sat3IloConstraints;

    /**
     * mémorisation des indices des contraintes SAT3 ayant le plus grand potentiel
     * de satisfaire leur borne min couramment violée
     */
    std::map<int,int> _bestSlackSat3MinDurSupport;

    /**
     * variable liée à la fonction objectif
     */
    IloIntVar _sumPrioVar;


public:

    /**
     * debug : trace d'une variable ensembliste
     * @param setVarToTrace_
     */
    void addOneSetVarToTrace(IloAnySetVar setVarToTrace_);

    /**
     * A la création des instances des SAT1, on mémorise la variable ensembliste et sa cardinalité
     * @param setVar  variable ensembliste liée à la contrainte SAT1
     * @param cardVar cardinalité de la variable ensembliste liée à la contrainte SAT1
     * @param trackingDomain créneaux candidats (peut différer à la création du domaine de setVar)
     */
    void addCardinalityInfo(IloAnySetVar setVar, IloIntVar cardVar,IloAnyArray trackingDomain);

    static bool loadAllRessources() {return _loadAllResources;}

    /**
     * accès aux critères ordonnés
     * @return la liste des numéros de critère du plus important au moins important
     */
    std::vector<int>& getRankedCriterias() {return _rankedSearchCriterias;}

    /**
     * ajout d'une instance de contrainte IloSatelliteConstraint3
     * @param sat3Ctr
     * @param trackingDomain les slots candidats pour cette contrainte
     */
    void addSat3IloConstraint(IloConstraint sat3Ctr,IloAnyArray trackingDomain);




protected :

    /**
    * Préparation du nouvel environnement ILOG Solver après libération de l'ancien et des maps de satellites et stations
    * @param modeInstanciationDisplay : NOMINAL/REDUIT/MINIMAL , permet de renseigner la chaîne commentaire en post-optim sur les
    * @param beginDay jour de la fenêtre de calcul
    * @param solutionDayBefore : flag indiquant qu'il y a eu un calcul avec succès la veille
    * créneaux sauvegardés
    */
    void prepare(std::string modeInstanciationDisplay,PHRDate& beginDay, bool solutionDayBefore);

    /**
     * lecture des critères de priorité depuis le fichier de conf. on trie ces critères du plus important
     * au moins important et on range ces critères dans le tableau _rankedSearchCriterias
     */
    void initRankSearchCriterias();

    /**
    * libération de l'ancien environnement ILOG SOLVER et des maps de satellites et stations
    */
    void free();


    /**
    * Map des objets OCP_CpSatellite et OCP_CpStation à partir des paramètres de lancement
    * création de leurs contraintes
    * @param optimScope : fenêtre courante de calcul
    * @param parameters : paramètres de lancement */
    void mapSatellitesAndStations(OCP_CpTimeSlotPtr optimScope,OcpParametersPtr parameters);

    /**
    * Map des objets OCP_CpSatellite à partir des paramètres de lancement
    * création de leurs contraintes
    * @param optimScope : fenêtre courante de calcul
    * @param parameters : paramètres de lancement */
    void mapSatellites(OCP_CpTimeSlotPtr optimScope,OcpParametersPtr parameters);

    /**
    * Map des objets OCP_CpStation à partir des paramètres de lancement
    * création de leurs contraintes
    * @param parameters : paramètres de lancement */
    void mapStations(OcpParametersPtr parameters);


    /**
     * parcourt les contrainte satellites et modifie en conséquence l'intervalle de chargement des slots du satellite
     */
    void updateSatelliteScope();

    /**
     * Instanciation des contraintes OCP en vue de créer les contraintes ILOG
     * @param optimScope
     */
    void instantiateOCPConstraints(OCP_CpTimeSlotPtr optimScope);

    /**
     * Sauvegarde de l'information du mode d'instanciation pour chacune des ressources à l'issue du calcul sur un jour
     * @param beginDay le jour de calcul
     * @param parameters paramètres d'entrée, global ou manuel
     * @param scope rang courant dans le tableau parameters->getSetTypes2Use()
     */
    void notifySavedResources(PHRDate beginDay,OcpParametersPtr parameters, int scope);

    /**
     * pour chaque satellite, on partitionne les slots candidats (ou une partie d'entre eux) en contraintes SAT1
     * (une partie d'entre elles de manière à ce qu'il n'y ait pas de candidat commun entre deux SAT1)
     * de manière à pouvoir poser ultérieurement une contrainte de stable max sur l'ensemble des partitions
     * multi-satellites en sommant les bornes inférieures des contraintes SAT1
     */
    void partitionStableCandidates();

private :
    /**
    * Constructeur par défaut
    * @return objet OCP_CpModel
    * privé : utilisation du singleton pour appel facilité depuis n'importe quel contrainte en création
    */
    OCP_CpModel();

public:

    /**
     * pattern de création du modèle de résolution sur une journée.
     * @return une nouvelle instance à chaque nouvelle fenêtre de calcul, sinon l'instance en cours
     */
    static OCP_CpModel* getInstance();


    /**
    * Destructeur par défaut
    * @return
    */
    virtual ~OCP_CpModel();


    /**
    * Traitement en cas d'interruption utilisateur.
    * TODO : sauvegarder les résultats jusqu'au jour J-1 où J=jour de calcul en cours
    */
    void onSignalHandler();


    /**
    * Accesseur de l'ID courant
    * @return int _idCpt
    */
    //    int getCurrentId(){return ++_idCpt;}




    /**
    * instanciation sur un horizon via les paramètres de choix des contraintes
    * @param parameters : l'objet contenant les modes de lancement et d'instanciation des contraintes
    */
    void pilot(OcpParametersPtr parameters);



    /**
     * accès à l'environnement ILOG
     * @return l'environnement ILOG
     */
    IloEnv getEnv() {return _env;}

    /**
     * accès au modèle ILOG courant
     * @return le modèle ILOG courant
     */
    IloModel getModel() {return _model;}

    /**
     * accès au solveur ILOG courant
     * @return le solveur ILOG courant
     */
    IloSolver getSolver() {return _solver;}

    /**
     * accès à la date en secondes du début du modèle de résolution courant (début de la journée de calcul)
     * @return le début de la journée de calcul
     */
    OCP_CpDate getOrigin() {return _origin;}

    /**
     * accès à la date en secondes de la fin du modèle de résolution courant (fin de la journée de calcul)
     * @return la fin de la journée de calcul
     */
    OCP_CpDate getHorizon() {return _horizon;}

    /**
     * accès commentaire à renseigner sur tous les slots sauvegardés (artf658881)
     * @return le commentaire à renseigner sur tous les slots sauvegardés
     */
    std::string getComment() const {return _comment;}

    /**
     * map des satellites
     * @return la map des satellites (clé OcpSatellitePtr valeur OCP_CpSatellitePtr)
     */
    SatelliteMap getSatellites() {return _allSatellites;}

    /**
     * map des stations
     * @return la map des stations (clé OcpStationPtr, valeur OCP_CpStationPtr)
     */
    StationMap getStations() {return _allStations;}

    /**
     * accès à la variable ensembliste globale
     * @return variable ensembliste globale
     */
    IloAnySetVar getGlobalSlotSetVar() {return _globalSlotSetVar;}

    /**
     * accès aux slots initiaux de la variable globale
     * @return les slots initiaux de la variable globale
     */
    IloAnyArray getGlobalSlots() {return _globalSlots;}

    /**
    * Récupération de l'objet OCP_CpSatellite
    * @param aSat satellite origine
    * @return l'objet OCP_CpSatellite
    */
     OCP_CpSatellitePtr getSatellite(OcpSatellitePtr aSat);

    /**
    * Récupération de l'objet OCP_CpStation
    * @param aStation : station origine
    * @return l'objet OCP_CpStation
    */
    OCP_CpStationPtr getStation(OcpStationPtr aStation);

    /**
    * Ajout à l'ensemble des requis de la variable globale des slots reservés
    * Suppression des possibles des slots interdits
    * Appel depuis un goal
    * @param solver le solver interne PPC
    */
    void addReservationConstraints(IloSolver solver);


public:

     /**
     * stockage d'une contrainte OCP dans le modèle
     * @param aCtr contrainte OCP
     * @param resource : la ressource satellite ou station
     */
     void storeOneOcpConstraint(OCP_CpConstraintPtr aCtr,OCP_CpTrackingResource* resource);

     /**
      * nombre de contraintes STA6 positionnées
      * @return le nombre de contraintes STA6 positionnées
      */
     int getNbSta6Constraints() const {return _cpSTA6Constraints.size();}

     /**
      * ajout d'un slot branché sur la pile
      * @param slotId
      * @param isTracking vrai ssi le slot branché est slot de tracking
      */
     void push(long slotId,bool isTracking) {_branchingIds.push_back(slotId); _branchingTypes.push_back(isTracking);}

     /**
     * ajout d'un slot backtracké sur la pile
     * @param slotId
     * @param isTracking vrai ssi le slot branché est slot de tracking
     */
     void back(long slotId,bool isTracking) {_branchingIds.push_back(-slotId);_branchingTypes.push_back(isTracking);}

     /**
      * accès au statut du calcul précédent (la veille)
      * @return vrai ssi dans le pilote courant, on a lancé un calcul la veille et que celui-ci a fourni une solution
      */
     int hasSolutionCalculatedDayBefore() const {return _solutionDayBefore;}

     /**
      * affichage des cardinalités courantes des variables ensemblistes collectées par SAT1
      */
     void displayCardinalityInfos();

     /**
      * renvoie le meilleur gain possible si on assignait le slot, le gain étant mesuré comme le maximum
      * parmi les contraintes SAT1 contenant le slot du ratio A/B avec
      * A = écart courant à la borne min de la cardinalité courante de la variable ensembliste = max(0, card(setVar.required)-lb)
      * B = nombre de créneaux encore possible = card(setVar.possible() - card(setVar.required() )
      * @param slot slot courant dont on examine le gain possible
      * @return vrai ssi le slot est dans le support d'une contrainte la plus à améliorer
      */
     bool hasBestImprovementSat1MinPass(OCP_CpTrackingSlot* slot);


     /**
      * parcourt les différentes instances des contraintes SAT1 et regarde celles qui n'atteignent pas leur
      * borne min
      * @return vrai ssi il existe au moins une contrainte SAT1 qui n'atteint pas sa borne min
      */
     bool computeBestImprovementMinPass();


     /**
      * accès à l'information "reste-t-il des contraintes SAT1 non satisfaites au regard de leur borne inférieure"
      * @return vrai ssi il des contraintes SAT1 non satisfaites au regard de leur borne inférieure"
      */
     bool hasSlackMinPass() {return _hasSlackMinPass.getValue();}

     /**
      * parcourt les différentes instances des contraintes SAT3 et regarde celles qui n'atteignent pas leur
      * borne min
      * @return vrai ssi il existe au moins une contrainte SAT3 qui n'atteint pas sa borne min
      */
     bool computeBestImprovementSat3MinDur();

     /**
      * accès à l'information "reste-t-il des contraintes SAT3 non satisfaites au regard de leur borne inférieure"
      * @return vrai ssi il des contraintes SAT3 non satisfaites au regard de leur borne inférieure"
      */
     bool hasSlackSat3MinDur() {return _hasSlackSat3MinDur.getValue();}

     /**
      * renvoie le meilleur gain possible si on assignait le slot, le gain étant mesuré comme le maximum
      * parmi les contraintes SAT3 contenant le slot du ratio A/B avec
      * A = écart courant à la durée min
      * B = durée max courante - durée min courante
      * @param slot slot courant dont on examine le gain possible
      * @return vrai ssi le slot est dans le support d'une contrainte la plus à améliorer
      */
     bool hasBestImprovementSat3MinDur(OCP_CpTrackingSlot* slot);


protected:

     /**
       * Ajout d'un slot satellite à la liste
       * @param aSlot : passage satellite
       * @param optimScope fenêtre de calcul courante
       * @return vrai ssi le slot est candidat (non invalidé par une contrainte SAT4)
       */
     bool addTrackingSlot(OcpSatelliteSlotPtr aSatSlot,OCP_CpTimeSlotPtr optimScope);

    /**
     * chargement de tous les slots d'un satellite, avec élargissement potentiel de la fenêtre de jour autour de la fenêtre de calcul
     * pour les satellites ayant des contraintes l'imposant. Dans ce cas, les slots demandés commençant avant le début de la fenêtre de calcul
     * seront filtrés par le fait qu'ils sont reservés
     * @param satellite : satellite sur lequel on charge les slots
     * @param optimScope : la fenêtre de calcul jour
     */
    void parseSlot(OCP_CpSatellitePtr satellite , OCP_CpTimeSlotPtr optimScope);


    /**
     * création d'un tracking Slot
     * @param aSatSlot créneau satellite origine
     * @param optimScope fenêtre de calcul courante
     * @return vrai ssi on a pu créer un tracking slot
     */
    bool parseOneSlot(OcpSatelliteSlotPtr aSatSlot,OCP_CpTimeSlotPtr optimScope);

    /**
     * chargement de tous les slots satellite par satellite
     * @param optimScope : fenêtre jour de calcul courante
     * @parameters paramètres d'entrée donnant accès aux satellites
     */
    void parseAllSlots(OCP_CpTimeSlotPtr optimScope,OcpParametersPtr parameters);

    /**
     * split des créneaux vus dans des contraintes SAT7 ou SAT8
     * @parameters paramètres d'entrée donnant accès aux satellites
     */
    void splitAllSlots(OcpParametersPtr parameters);


    /**
    * Génération des variables Solver liés aux créneaux de maintenance
    */
    void generateMaintenanceSlots();

    /**
     * création des variables ensemblistes associées aux passages satellites, mémorisés à la fois sur les satellites et les stations
     */
    void createTrackingSlotVars();


    /**
    * création des slots associés aux maintenances, mémorisés sur les stations
    */
    void createMaintenanceSlots();

    /**
    * création de la variable ensembliste globale dont le domaine est l'union des passages satellites et des slots de maintenance
    */
    void createGlobalSlots();

    /**
    * Instanciations de toutes les contraintes satellite sur une portée jour
    * @param beginDay : début du jour courant
    * @param beginNextDay : début du prochain jour
    * @param param : paramètres NOMINAL/REDUIT...
    * @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
    */
    void createOCPSatelliteConstraints(PHRDate beginDay, PHRDate beginNextDay,OcpParametersPtr parameters, int scope);

    /**
    * instanciation d'une contrainte type satellite
    * @param ctrType le type de contrainte satellite
    * @param period  la portée d'instanciation
    * @param aCtr  la contrainte métier origine
    * @param satellite  le satellite associé
    * @return la contrainte créée
    */
    OCP_CpConstraintPtr createOneOCPSatelliteConstraint(ConstraintTypesEnum ctrType, OCP_CpTimeSlotPtr period, OcpConstraintPtr aCtr , OCP_CpSatellitePtr satellite);

    /**
    * Instanciations de toutes les contraintes station sur une portée jour
    * @param beginDay : début du jour courant
    * @param beginNextDay : début du prochain jour
    * @param param : paramètres NOMINAL/REDUIT...
    * @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
    */
    void createOCPStationConstraints(PHRDate beginDay, PHRDate beginNextDay,OcpParametersPtr parameters, int scope);

    /**
    * instanciation d'une contrainte type station
    * @param ctrType le type de contrainte station
    * @param period  la portée d'instanciation
    * @param aCtr  la contrainte métier origine
    * @param satellite  le satellite associé
    * @return la contrainte créée
    */
    OCP_CpConstraintPtr createOneOCPStationConstraint(ConstraintTypesEnum ctrType, OCP_CpTimeSlotPtr period, OcpConstraintPtr aCtr , OCP_CpStationPtr station);

    /**
    * création des contraintes internes liant les variables ensemblistes
    */
    void addInternalConstraints();


    /**
     * ajout d'une fonction objectif à maximiser : somme des (10- prioSAT4)+(10-prioSTA6) pour chacun des créneaux satellites
     * dans la solution
     */
    void addObjective();

    /**
    * ajout au modèle PPC des contraintes métiers traduites en OCP_CpConstraints
    */
    void addOCPConstraints();


    void printSolution();

    /**
     * Flaggue les slots choisis par le moteur. Les figés le restent, les autres sont au status RESERVED
     * @param beginDay début du jour de sauvegarde
     * @param nbSavedTrackings nombre de slots de trackings sauvegardés
     * @param nbSavedMaintenances nombre de slots de maintenance sauvegardés
     */
    void saveSlots(PHRDate beginDay,int& nbSavedTrackings, int& nbSavedMaintenances);


    void displayVarForDebug(IloAnySetVar act);

    /**
    * export dans format XML pour Schedulerviewer
    * @param beginDay debut de la fenetre de calcul
    * @param beginNextDay fin de la fenetre de calcul
    */
    void exportSolution2Xml(PHRDate beginDay,PHRDate beginNextDay);

    /**
    * export dans format texte pour préparation de données pour IBM-ILOG OPL
    * @param beginDay debut de la fenetre de calcul
    */
    void exportOplModel(PHRDate beginDay);

    /**
     * parcours des zones de tracking de chaque station sur le jour de calcul (avec plages à cheval possible) et notification des dates
     * de début et fin de tracking possible pour calcul ultérieur de split des créneaux SAT1-visibilité partielle
     * @param optimScope : fenêtre de calcul courante
     * @param parameters : paramètres d'entrée pour lecture des stations concernées
     */
    void notifyStationTrackingZones(OCP_CpTimeSlotPtr optimScope, OcpParametersPtr parameters);

    /**
    * Remplissage du modèle sur une portée [beginDay, beginNextDay) qui doit être un intervalle semi-ouvert
    * @param optimScope : fenêtre de calcul courante
    * @param parameters paramètres d'entrée donnant accès aux satellites
    */
    void feedModel(OCP_CpTimeSlotPtr optimScope, OcpParametersPtr parameters);

    /**
     * choix du goal de recherche
     * @return le goal de recherch
     */
    IloGoal chooseGoal();

    /**
    * Lancement du modèle  sur une portée [beginDay, beginNextDay) qui doit être un intervalle semi-ouvert
    * @param beginDay : le début du jour courant
    * @param beginNextDay : le début du jour suivant
    * @param parameters: les paramètres de lancement, globaux ou par satellite et station
    * @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
    * @param rankDay : le rang du jour courant (de 1 à nombre de jours) pour affichage au jdb
    * @param modeInstanciationDisplay : mode NOMINAL/... pour affichage au jdb
    * @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
    * @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
    * @param totalTime temps total d'éxécution
    * @param totalRun nombre de run successifs pour trouver (ou échouer) une solution en mode restart
    * @param timeModel : temps consommé pour la création du modèle dans le ou les runs
    * @param solutionDayBefore : flag indiquant qu'il y a eu un calcul avec succès la veille
    * @return le status du solveur 0 : solution trouvée, 1 absence de solution prouvée, 2 pas de solution au bout du timeout
    */
    int run(PHRDate& beginDay, PHRDate& beginNextDay, OcpParametersPtr parameters, int scope, int rankDay, ConstraintSetTypesEnum modeInstanciationLevel,int& nbSavedTrackings,int& nbSavedMaintenances, IloNum& totalTime,int& totalRun, IlcFloat& timeModel,IloBool solutionDayBefore);

    /**
    * lancement de la résolution ILOG Solver et arrêt dès la première solution réalisable
    * @param goalChoisi goal choisi pour la recherche
    * @param beginDay : le début du jour courant
    * @param beginNextDay : le début du jour suivant
    * @param parameters: les paramètres de lancement, globaux ou par satellite et station
    * @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
    * @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
    * @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
    * @param timerEnv : timer pour comptabilisation des temps
    * @return le status du solveur 0 : solution trouvée, 1 absence de solution prouvée, 2 pas de solution au bout du timeout
    */
    int searchFeasibility(IloGoal goalChoisi,PHRDate& beginDay,PHRDate& beginNextDay, OcpParametersPtr parameters, int scope,int& nbSavedTrackings,int& nbSavedMaintenances,IloEnv timerEnv);

    /**
    * lancement de la résolution ILOG Solver et recherche de la meilleure solution au regard du critère de la fonction objectif
    * @param goalChoisi goal choisi pour la recherche
    * @param beginDay : le début du jour courant
    * @param beginNextDay : le début du jour suivant
    * @param parameters: les paramètres de lancement, globaux ou par satellite et station
    * @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
    * @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
    * @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
    * @param twoPhase : indique qu'on lance en mode restart
    * @param timerEnv : timer pour comptabilisation des temps
    * @param timeOut : rappel du timeOut pour la seconde passe d'optimisation
    * @return le status du solveur 0 : solution trouvée, 1 absence de solution prouvée, 2 pas de solution au bout du timeout
    */
    int searchOptimality(IloGoal goalChoisi,PHRDate& beginDay,PHRDate& beginNextDay, OcpParametersPtr parameters, int scope,int& nbSavedTrackings,int& nbSavedMaintenances,IloBool twoPhase,IloEnv timerEnv,IloNum timeOut);


    /**
    * sauvegarde des slots réservés à l'issue du calcul ILOG solver
    * @param beginDay : le début du jour courant
    * @param beginNextDay : le début du jour suivant
    * @param parameters: les paramètres de lancement, globaux ou par satellite et station
    * @param nbSavedTrackings : nombre de créneaux de tracking sauvegardés
    * @param nbSavedMaintenances : nombre de créneaux de maintenance sauvegardés
    * @param scope : rang courant dans le tableau parameters->getSetTypes2Use()
    */
    void postOptim(PHRDate& beginDay,PHRDate& beginNextDay, OcpParametersPtr parameters, int scope,int& nbSavedTrackings,int& nbSavedMaintenances);

    /**
     * suppression des slots superflus (artf618698)
     * @param parameters paramètres d'entrée donnant accès aux satellites
     * @return renvoie le nombre de créneaux supprimés
     */
    int removeUselessSlots(OcpParametersPtr parameters);

    /**
     * utile en debug pour lire une liste de slots imposés depuis listener.conf
     * @param slotId : id du slot candidat
     * @return vrai ssi l'id du slot candidat est dans la liste des slots figés de listener.conf
     */
    bool isReservationForced(uint32_t slotId);

    /**
     * une fois supprimé les slots superflus, on peut calculer une priorité aggrégée pour chaque slot*
     * en tenant compte des priorités de SAT4, STA6 et des conflits potentiels
     * @param parameters paramètres d'entrée donnant accès aux satellites
     */
    void updateTrackingSlotsPriorities(OcpParametersPtr parameters);

    /**
     * définition des trace ILOG
     */
    void prepareTraceIlogInfos();

    /**
     * affichage de l'arbre de branchement parcouru
     */
    void logTraceChoicePoints();

    /**
     * affichage des paramètres de configuration pour la résolution solveur
     */
    void displayConfParameters();

    /**
     * ajout de la contrainte redondante basée sur la cardinalité minimale d'un stable maximum
     * dans le graphe des conflits
     */
    void addStableMaxConstraint();

    /**
     * création des structures permettant de collecter les différentes variables ensemblistes et leurs variables de
     * cardinalité sur chacune des contraintes SAT1 instanciées
     */
    void prepareSat1CardinalityInfos();

    /**
     * positionne le flag réversible liées à la violation des bornes min des contraintes SAT1
     */
    void initReversibleFlags();

};


#endif /* OCP_CpModel_H_ */
