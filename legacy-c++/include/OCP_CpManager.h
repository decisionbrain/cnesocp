/*
 * $Id: OCP_CpManager.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ======================================
 */

/*
 * OCP_CpManager.h
 *
 */

#ifndef OCP_CPMANAGER_H_
#define OCP_CPMANAGER_H_

#include <iostream>

#include "OCP_CpDate.h"

using namespace std;

class OCP_CpManager
{
private:
    static bool instanceFlag;
    static OCP_CpManager *cpManager;
    OCP_CpManager();



	/**
	* Id courant utilisé pour la génération de fichier XML pour scheduler viewer
	*/
	int _idCpt;

public:
    static OCP_CpManager* getInstance();
    virtual ~OCP_CpManager();

    /**
    * Accesseur de l'ID courant
    * @return int _idCpt
    */
    int getCurrentId(){return ++_idCpt;}



};

#endif /* OCP_CPMANAGER_H_ */
