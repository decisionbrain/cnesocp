/*
 * $Id: OCP_CpDate.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpDate.h
 *
 */

#ifndef OCP_CpDate_H_
#define OCP_CpDate_H_

#include <stdint.h>

#define INFINI 86401; // 24h * 60m * 60s +1

typedef int64_t OCP_CpDate;

#endif /* OCP_CpDate_H_ */
