/*
 * $Id: OCP_CpParameter.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpParameter.h
 *
 */

#ifndef OCP_CpParameter_H_
#define OCP_CpParameter_H_

#include <OcpXmlTypes.h>
#include <constraints/OcpConstraint.h>

//#include <phr/don/ocpp1domainp1constraints/ConstraintSetTypes.h>
//using namespace phr::don::ocpp1domainp1constraints;
using namespace ocp::commons::io;

class OCP_CpManager;



namespace ocp{
namespace solver{

/**
 * mode de lecture des filtres
 */
typedef enum
{
    RIEN = 0,// pas de lecture
    MEM = 1, //lecture depuis le modèle instancié
    TXT = 2  //lecture depuis un reader texte
} filter;


/**
 * index pour filtrer le mode de lecture (texte, modèle instancié , ou rien) des slots et contraintes
 */
/*
typedef enum
{
    slots = 0,             //!< slots
            stationType1,  //!< stationType1
            stationType2,  //!< stationType2
            stationType3,  //!< stationType3
            stationType4,  //!< stationType4
            stationType5,  //!< stationType5
            stationType6,  //!< stationType6
            satelliteType1,//!< satelliteType1
            satelliteType2,//!< satelliteType2
            satelliteType3,//!< satelliteType3
            satelliteType4,//!< satelliteType4
            satelliteType5,//!< satelliteType5
            satelliteType6,//!< satelliteType6
            satelliteType7,//!< satelliteType7
            satelliteType8 //!< satelliteType8

}
paramindex;
*/
/**
 * niveau de lancement NOMINAL/MINIMAL/REDUIT/SAM
 */
/*typedef enum
{
    NOMINAL = 0,        //!< NOMINAL
            REDUIT = 1, //!< REDUIT
            MINIMAL = 2,//!< MINIMAL
            SAM = 3     //!< SAM
} constraintSetType;
*/
}
}


;
/**
 * Paramètres de lancement du pilote OCP
 */
class OCP_CpParameter
{
    /**
     * Pointeur vers les readerTexte (NULL si pas de reader texte)
     */
    OCP_CpManager* _cpManager;

    /**
     * flag de filtre des contraintes par niveau et type de contrainte (6 ctrStations + 8 ctrSatellites)
     */
    ocp::solver::filter* _filters;

    /**
     * flag de lecture des slots
     */
    ocp::solver::filter _slots;

    /**
     * niveau NOMINAL, REDUIT, MINIMAL ou SAM
     */
    //ocp::solver::constraintSetType _cs;
    ConstraintSetTypes::ConstraintSetTypes_ENUM _cs;


public :
    /**
     *
     * @param cpManager : donne accès aux readers texte
     * @param filters : filtres pour la lecture des contraintes (txt , memoire, ou rien)
     * @param cs : rappel du niveau NOMINAL, REDUIT, MINIMAL ou SAM
     * @return
     */
    OCP_CpParameter(OCP_CpManager* cpManager,
            ocp::solver::filter* filters,
            ocp::solver::filter slots,
            //ocp::solver::constraintSetType cs
            ConstraintSetTypes::ConstraintSetTypes_ENUM cs
            );

    /**
     * renvoie le filtre sur un index de type de contrainte ou slot
     * @param index : index de type de contrainte ou slot
     * @return
     */
    ocp::solver::filter getFilter(ConstraintTypesEnum index/*ocp::solver::paramindex index*/);

    /**
     *
     * @return le niveau de lecture des slots (texte, mémoire, ou rien)
     */
    ocp::solver::filter getSlots() const;
    /**
     *
     * @return : le niveau NOMINAL/MINIMAL/REDUIT/SAM
     */
    //ocp::solver::constraintSetType getCs() const {return _cs;}
    ConstraintSetTypes::ConstraintSetTypes_ENUM getCs() const {return _cs;}

};

#endif /* OCP_CpParameter_H_ */
