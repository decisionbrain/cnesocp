/*
 * $Id: OCP_CpCstIntTimetable.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#ifndef OCP_CpCstIntTimetable_H_
#define OCP_CpCstIntTimetable_H_



#include "planif/OCP_CpTimeSlot.h"
#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>


ILOSTLBEGIN

//ILCHANDLE
//ILCREV

class OCP_CpMaillon;
ILCREV(OCP_CpMaillon);//permet la réversibilité de la structure pour ILOG SOLVER

class OCP_CpMaillon
{
public:
    OCP_CpMaillon* _previous;
    OCP_CpMaillon* _next;
    IloInt _val;
    IloInt _startTime;
    IloInt _endTime;

    OCP_CpMaillon() {};
    OCP_CpMaillon(IloInt val,IloInt start, IloInt end, OCP_CpMaillon* prev=NULL,
                OCP_CpMaillon* next=NULL):
    _val(val),_startTime(start),_endTime(end),_previous(prev),_next(next)
    {};
    virtual ~OCP_CpMaillon() {}
};

class OCP_CpCstIntTimetableI;
ILCREV(OCP_CpCstIntTimetableI);//permet la réversibilité de la structure pour ILOG SOLVER

class OCP_CpCstIntTimetableI
{
	friend class CTsOCstIntTimetableCursor;
private:
	IloInt _timeMin;
	IloInt _timeMax;
	OCP_CpMaillon* m_head;
	//IloSolver _solver;
	IloEnv _env;
	void insert_befor(OCP_CpMaillon* p1, OCP_CpMaillon* p);
	void insert_after(OCP_CpMaillon* p1, OCP_CpMaillon* p);
	void supress(OCP_CpMaillon* p);
protected:
public:
    OCP_CpMaillon* getFirstElement(IloInt time);
	OCP_CpCstIntTimetableI(IloEnv env,OCP_CpTimeSlot* interval,IloInt defaultValue);
	OCP_CpCstIntTimetableI(IloEnv env,IloInt timeMin, IloInt timeMax,IloInt defaultValue);
	void setValue(OCP_CpTimeSlot* interval, IloInt val)
	{setValue(interval->getBegin(),interval->getEnd(),val);}
	void setValue(IloInt a,IloInt b, IloInt val);
	void substract(OCP_CpTimeSlot* interval, IloInt val)
	{substract(interval->getBegin(),interval->getEnd(),val);}
	void add(OCP_CpTimeSlot* interval, IloInt val)
	{add(interval->getBegin(),interval->getEnd(),val);}

	/**
	 * incrément de la timetable sur une plage
	 * @param timeMin début de plage
	 * @param timeMax fin de plage
	 * @param val incrément sur toute la plage
	 * @return renvoie le niveau le plus haut sur la plage après modification
	 */
	IloInt add(IloInt timeMin,IloInt timeMax, IloInt val);

	/**
	* décrément de la timetable sur une plage
	* @param timeMin début de plage
	* @param timeMax fin de plage
	* @param val incrément sur toute la plage
	* @return renvoie le niveau le plus bas sur la plage après modification
	*/
	IloInt substract(IloInt timeMin,IloInt timeMax, IloInt val);


	IloInt getTimeMin() {return _timeMin;}
	IloInt getTimeMax() {return _timeMax;}
	//IloEnv getEnv() {return _env;}
};

class  OCP_CpCstIntTimetable
{
    OCP_CpCstIntTimetableI* _impl;


public:

    OCP_CpCstIntTimetableI* getImpl() {return _impl;}

	OCP_CpCstIntTimetable(IloEnv env,OCP_CpTimeSlot* interval,IloInt defaultValue=0)
	{
		_impl = new(env)
			OCP_CpCstIntTimetableI(env,interval,defaultValue);
	}
	OCP_CpCstIntTimetable(IloEnv env,IloInt timeMin, IloInt timeMax,IloInt defaultValue=0)
	{
		_impl = new(env)
			OCP_CpCstIntTimetableI(env,timeMin,timeMax,defaultValue);
	}
	OCP_CpCstIntTimetable(OCP_CpCstIntTimetableI* impl = 0)
	{
	    _impl = impl;
	}

	/**
	* incrément de la timetable sur une plage
	* @param timeMin début de plage
	* @param timeMax fin de plage
	* @param val incrément sur toute la plage
	* @return renvoie le niveau maximal modifié sur toute la plage
	*/
	IloInt add(IloInt timeMin,IloInt timeMax, IloInt val) {return _impl->add(timeMin,timeMax,val);}

	/**
	* décrément de la timetable sur une plage
	* @param timeMin début de plage
	* @param timeMax fin de plage
	* @param val incrément sur toute la plage
	* @return renvoie le niveau le plus bas sur la plage après modification
	*/
	IloInt substract(IloInt timeMin,IloInt timeMax, IloInt val)  {return _impl->substract(timeMin,timeMax,val);}

	/**
	 * figer la timetable à une valeur sur un intervalle
	 * @param ts intervalle de figement
	 * @param aVal valeur de la timetable sur l'intervalle de figement
	 */
	void setValue(OCP_CpTimeSlot* ts, IloInt aVal) {_impl->setValue(ts,aVal);}

/*	TSOCALL2(setValue, OCP_CpTimeSlot*, IloInt);
	TSOCALL3(setValue, IloInt,IloInt, IloInt);
	TSOCALL2(substract, OCP_CpTimeSlot*, IloInt);
	TSOCALL3(substract, IloInt, IloInt, IloInt);
	TSOCALL2(add, OCP_CpTimeSlot*, IloInt);
	TSOCALL3(add, IloInt, IloInt, IloInt);
	TSORETURN0(IloInt, getTimeMin);
	TSORETURN0(IloInt, getTimeMax);*/

	//void setValue()

	IloInt getTimeMin() {return _impl->getTimeMin();}
	IloInt getTimeMax() {return _impl->getTimeMax();}

	void display();

	/**
	 * accès à la valeur du maillon le plus haut de la time table entre deux dates
	 * @param a début de plage
	 * @param b fin de plage
	 * @return la valeur du maillon le plus haut de la time table entre deux dates
	 */
	IloInt getMax(IloInt a, IloInt b);

	IloInt getMax(OCP_CpTimeSlot* interval)
	{ return getMax(interval->getBegin(),interval->getEnd());}

	/**
	 * accès à la valeur du maillon le plus bas de la time table entre deux dates
	 * @param a début de plage
	 * @param b fin de plage
	 * @return la valeur du maillon le plus bas de la time table entre deux dates
	 */
	IloInt getMin(IloInt a, IloInt b);

	IloInt getMin(OCP_CpTimeSlot* interval)
	{ return getMin(interval->getBegin(),interval->getEnd());}

	IloInt getSum(IloInt a, IloInt b, IloInt sign);
	IloInt getSum(OCP_CpTimeSlot* interval, IloInt sign)
	{ return getSum(interval->getBegin(),interval->getEnd(),sign);}

};

class  OCP_CpCstIntTimetableCursor
{

protected:
    IloInt _val;
    IloInt _timeMin;
    IloInt _timeMax;
    IlcBool _ok;
    OCP_CpCstIntTimetable _table;
	OCP_CpMaillon* _current;

public:
	OCP_CpCstIntTimetableCursor(OCP_CpCstIntTimetable table,IloInt time);
	//OCP_CpCstIntTimetableCursor() {}
	virtual ~OCP_CpCstIntTimetableCursor();
	IloInt getValue() {return _val;}
    IloBool ok() const {return _ok;}
    void operator++();
    IloInt getTimeMin() const {return _timeMin;}
    IloInt getTimeMax() const {return _timeMax;}
};

#endif // !defined(OCP_CpCstIntTimetable_H_)
