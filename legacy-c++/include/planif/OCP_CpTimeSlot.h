/*
 * $Id: OCP_CpTimeSlot.h 926 2010-11-12 14:56:15Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpTimeSlot.h
 *
 */

#ifndef OCP_CpTimeSlot_H_
#define OCP_CpTimeSlot_H_

#include <string>
#include <list>

#include "planif/OCP_Solver.h"
#include "context/OCP_CpObject.h"
#include "OCP_CpDate.h"
#include <OcpPeriod.h>
#include <OcpTimePeriod.h>
//#include "resources/OCP_CpSatellite.h"
//#include "resources/OCP_CpStation.h"


using namespace std ;
using namespace ocp::commons::io;

class OCP_CpConstraint;

class OCP_CpTimeSlot: public OCP_CpObject
{
private:

	/**
	 * valuation de l'intervalle dans une timetable
	 */
    int _capacity;

protected:
    /**
     * debut en secondes
     */
    OCP_CpDate _begin;

    /**
     * fin en secondes
     */
    OCP_CpDate _end;

    /**
     * ce slot doit-il être impérativement dans l'ensemble des requis
     */
    bool _reserved;

    /**
    * ce slot doit-il être enlevé des possibles (suite à traitement par SAT4 ou lecture des ouvertures stations)
    */
    bool _toRemove;


    /**
     *  un slot requis "exceptionnel" mais qui n'interviendra pas dans des contraintes "de routine" sommant des durées ou des passages
     */
    bool _hasBookingOrder;


    /**
      * contraintes OCP pour lesquelles ce tracking slot est candidat
      */
     std::vector < boost::shared_ptr < OCP_CpConstraint > > _cpConstraints;


public:
	/**
	 * Constructeur d'un objet OCP_CpTimeSlot à partir de 2 dates
	 * @param x Date de début
	 * @param y Date de fin
	 * @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot
	 */
	OCP_CpTimeSlot(OCP_CpDate x, OCP_CpDate y);

	/**
	 * Constructeur de copie d'un objet OCP_CpTimeSlot. Cela permet de contrôler la duplication
	 * d'une instance d'un tel objet
	 * @param ts l'objet OCP_CpTimeSlot source de la copie
	 * @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot identique à ts
	 */
	OCP_CpTimeSlot(const OCP_CpTimeSlot& ts);

	/**
	 * constructeur à partir d'un intervalle métier : conversion en secondes
	 * @param period l'intervalle métier
	 * @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot basé sur la conversion en secondes de l'intervalle métier
	 */
	OCP_CpTimeSlot(OcpPeriodPtr period);

    /**
     * constructeur à partir d'un intervalle métier : conversion en secondes
     * @param period l'intervalle métier
     * @return renvoie une nouvelle instance d'un objet OCP_CpTimeSlot basé sur la conversion en secondes de l'intervalle métier
     */
    OCP_CpTimeSlot(OcpTimePeriodPtr period);


	/**
	 * Destructeur de l'objet OCP_CpTimeSlot
	 * @return
	 */
	virtual ~OCP_CpTimeSlot() {};

	/**
	 * slot id d'un slot de tracking ou maintenance
	 * @return id d'un slot de tracking ou maintenance
	 */
	virtual long getSlotId() const {return -1;}

	/**
	 * debut de l'intervalle. Erreur si non renseigné
	 * @return debut de l'intervalle
	 */
	OCP_CpDate getBegin() const;

    /**
     * fin de l'intervalle. Erreur si non renseigné
     * @return fin de l'intervalle
     */
	OCP_CpDate getEnd() const;

	/**
	 * amplitude d'un intervalle temporel en secondes
	 * @return nombre de secondes entre le début et la fin de l'intervalle
	 */
	virtual OCP_CpDate getDuration() const {return getEnd() - getBegin();}

	/**
	 * deux intervalles s'intersectent-ils ?
	 * @param timeSlot intervalle source
	 * @return vrai ssi this et timeSlot s'intersectent
	 *///TODO enlever une fois que les OCP_CpTimeSlotPtr sont généralisés
	bool intersect(const OCP_CpTimeSlot* timeSlot) const;

	/**
	* deux intervalles s'intersectent-ils ?
	* @param timeSlot intervalle source
	* @return vrai ssi this et timeSlot s'intersectent
	*/
	bool intersect(boost::shared_ptr < OCP_CpTimeSlot > timeSlot) const;

	/**
	* l'intervalle courant est-il inclus dans l'intervalle source
	* @param timeSlot intervalle source
	* @return vrai ssi this est inclus dans timeSlot
	*///TODO enlever une fois que les OCP_CpTimeSlotPtr sont généralisés
	bool isInside(const OCP_CpTimeSlot* timeSlot) const;

	/**
	* l'intervalle courant est-il inclus dans l'intervalle source
	* @param timeSlot intervalle source
	* @return vrai ssi this est inclus dans timeSlot
	*/
	bool isInside(boost::shared_ptr < OCP_CpTimeSlot > timeSlot) const;

	/**             * . la zone de split concerne le satellite du créneau origine
	 *
	 * durée d'intersection entre deux intervalles
	 * @param timeSlot intervalle source
	 * @return durée d'intersection entre this et la timeSlot
	 *///TODO à supprimer
	OCP_CpDate getIntersectionDuration(OCP_CpTimeSlot* timeSlot) const;

	/**
	* durée d'intersection entre deux intervalles
	* @param timeSlot intervalle source
	* @return durée d'intersection entre this et la timeSlot
	*/
	OCP_CpDate getIntersectionDuration(boost::shared_ptr < OCP_CpTimeSlot > timeSlot) const;

    /**
     * renvoie la durée d'intersection avec deux intervalles qui s'intersectent
     * @param itv1 premier intervalle (doit intersecter this)
     * @param itv2 second intervalle (doit intersecter this et itv1)
     * @return la durée d'intersection des 3 intervalles
     */
    OCP_CpDate getIntersectionDuration(boost::shared_ptr < OCP_CpTimeSlot > itv1 , boost::shared_ptr < OCP_CpTimeSlot > itv2) const throw(OCP_XRoot);

	/**
	 * renvoie l'intersection de deux intervalles
	 * @param timeSlot intervalle à intersecter
	 * @return l'intervalle d'intersection entre this et timeSlot
	 */
	OCP_CpTimeSlot getIntersection(const OCP_CpTimeSlot& timeSlot) const throw(OCP_XRoot);

	/**
	 * satellite associé à un tracking slot
	 * @return le satellite associé au tracking slot
	 */
	//virtual	OCP_CpSatellitePtr getSatellite() const {return OCP_CpSatellitePtr();}

	/**
	 * station associée à un tracking ou maintenance slot
	 * @return la station associée au tracking ou maintenance slot
	 */
	//virtual OCP_CpStationPtr getStation() const {return OCP_CpStationPtr();}

	/**
	 * le slot est-il un tracking slot
	 * @return vrai ssi le slot est un tracking slot
	 */
	virtual bool isTrackingSlot() const {return false;}

	/**
	 * le slot est-il un slot de maintenance
	 * @return vrai ssi le slot est un slot de maintenance
	 */
	virtual bool isMaintenanceSlot() const {return false;}

	/**
	 * modification du début
	 * @param b nouveau début
	 */
    void setBegin(OCP_CpDate b) {_begin = b;}

    /**
     * modification de la fin
     * @param e nouvelle fin
     */
    void setEnd(OCP_CpDate e) {_end = e;}

    /**
     * capacité d'un intervalle dans une timetable
     * @return
     */
    int getCapacity() const {return _capacity;}

    /**
     * modification de la capacité dans une timetable
     * @param capacity
     */
    void setCapacity(int capacity) {_capacity = capacity;}

    /**
     * incrément de la capacité d'une unité dans une timetable
     */
    void increaseCapacity() {_capacity++;}

    /**
     * décrément de la capacité d'une unité dans une timetable
     */
    void decreaseCapacity() {_capacity--;}

    /**
     * modifie l'intervalle courant de manière à ce qu'il couvre l'intervalle source
     * @param source intervalle qui permettra d'allonger l'intervalle courant
     */
    void enlarge(const OCP_CpTimeSlot& source);

    /**
     * traitement du créneau satellite pour la sauvegarde : renseignemenent des champs TYPE_CRENEAU, NIVEAU_CRENEAU
     */
    virtual void postOptim() {/*SUPPRESS_UNUSED_PARAMETER_WARNING();*/}

    /**
     * notification que le créneau est candidat pour une contrainte
     * @param aCtr la contrainte ayant ce créneau comme candidat
     */
    void addOcpConstraint(boost::shared_ptr < OCP_CpConstraint > aCtr);

    /**
     * accès au slot origine
     * @return le slot d'origine (tracking ou satellite)
     */
    virtual boost::shared_ptr <OcpRsrcSlot> getRscSlot() const {return boost::shared_ptr <OcpRsrcSlot>();}

    /**
     * indique si le slot origine est reservé : ce slot sera requis dans la solution
     * @return vrai ssi le slot est requis
     */
    bool isReserved() const {return _reserved;}

    /**
     * indique si le slot ne peut pas appartenir à la solution
     * @return vrai ssi le slot doit être enlevé des slots possibles
     */
    bool toRemove() const {return _toRemove;}

    /**
     * indique que le slot ne pourra être retenu comme possible dans le calcul
     * lance une exception si le slot était initialement reservé
     */
    void setToRemove();

    /**
     * indique si le slot est reservé avec un booking order : ce slot sera requis dans la solution mais
     * n'intervient pas dans des contraintes de routine sommant durées ou occurences de passages
     * @return vrai ssi le slot est fixé avec un booking order
     */
    bool hasBookingOrder() const {return _hasBookingOrder;}

    /**
     * affichage du slot avec info satellite , station, debut, fin
     * @return affichage complet
     */
    virtual boost::format getFullName() const;

    /**
     * predicat de comparaison par début pour deux passages
     * @param t1
     * @param t2
     * @return
     */
    static bool slotsBeginSortPredicate(OCP_CpTimeSlot* t1, OCP_CpTimeSlot* t2);

    /**
     * predicat de comparaison par fin pour deux passages
     * @param t1
     * @param t2
     * @return
     */
    static bool slotsEndSortPredicate(OCP_CpTimeSlot* t1, OCP_CpTimeSlot* t2);


    /**
     * predicat de comparaison par début pour deux passages
     * @param t1
     * @param t2
     * @return
     */
    static bool slotsReverseEndSortPredicate(OCP_CpTimeSlot* t1, OCP_CpTimeSlot* t2);

    /**
     * accès à la liste des contraintes OCP pour lequel le créneau est candidat
     * @return la liste des contraintes OCP pour lequel le créneau est candidat
     */
    std::vector < boost::shared_ptr < OCP_CpConstraint > > getOcpConstraints() {return _cpConstraints;}


};

typedef boost::shared_ptr < OCP_CpTimeSlot >    OCP_CpTimeSlotPtr;
typedef std::vector <  OCP_CpTimeSlotPtr >  VectorOCP_CpTimeSlot;
typedef std::map<OCP_CpTimeSlot*, OCP_CpDate> SpecialDuration;//TODO OCP_CpTimeSlotPtr
typedef boost::shared_ptr < SpecialDuration >    SpecialDurationPtr;
typedef std::vector< OCP_CpDate > VectorOCP_CpDate;


#endif /* OCP_CpTimeSlot_H_ */
