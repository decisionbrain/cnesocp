/*
 * $Id: OCP_Solver.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */ 


#ifndef SOLVER_H_
#define SOLVER_H_

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <list>

#include <Commons.h>
#include <CommonsIo.h>

#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>

#include <OcpLogManager.h>
#include <OcpLogBookManager.h>


/**
 * Définition de _ comme remplacement à StrExtern::StrExt
 */
#define _ ocp::commons::StrExtern::StrExt

/**
 * TODO Eléments à déplacer dans un fichier header.
 */

namespace ocp
{
namespace solver
{

/**
 * Constante à utiliser pour la création de l'instance du logger.
 */
static const std::string loggerInst = "fr.cnes.ocp.solver";

class OcpSolver
{
private:

public:
    /**
     * Constructeur par défaut
     * @return Retourne un objet de type OcpXml
     */
    OcpSolver();

    /**
     * Destructeur par défaut
     * @return
     */
    virtual ~OcpSolver();

};


}
}

extern void signalHandler (int);


#endif /* SOLVER_H_ */
