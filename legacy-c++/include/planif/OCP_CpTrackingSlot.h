/*
 * $Id: OCP_CpTrackingSlot.h 942 2010-12-21 13:11:22Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpTrackingSlot.h
 *
 */

#ifndef OCP_CpTrackingSlot_H_
#define OCP_CpTrackingSlot_H_

#include <string>


#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpSatellite.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"


using namespace std ;
using namespace ocp::commons::io;

class OCP_CpTrackingSlot: public OCP_CpTimeSlot
{
private:

    /**
     * intervalle start end origine, incluant temps de déconf, reconf
     */
    OCP_CpTimeSlotPtr _amplitude;

    /**
     * satellite associé au trackingSlot
     */
    OCP_CpSatellitePtr _satellite;


	/**
	 * station associée au trackingSlot
	 */
	OCP_CpStationPtr _station;

	/**
	 * Slot origine dans le modèle métier
	 */
	OcpSatelliteSlotPtr _slot;


	/**
	 * priorité du passage satellite, renseignée par SAT4, -1 si non renseignée
	 */
	int _prioritySatellite;

	/**
	* priorité du passage satellite, renseignée par STA6, -1 si non renseignée
	*/
	int _priorityStation;

	/**
	* priorité agrégée, utilisée par la stratégie de recherche, -1 si non renseignée
	*/
	int _searchPriority;


	/*
	 * aos los de référence (amplitude déduite du temps de reconf/déconf antenne)
	 */
	//OCP_CpTimeSlot _aosLosRef;

	/**
	 * Suite à l'impact des contraintes SAT4, ,les durées sont amputées de la durée du trou.
	 */
	OCP_CpDate _durationHole;

	/**
	 * slot origine du découpage en cas de split slot
	 */
	OCP_CpTrackingSlot* _fatherSplit;

	/**
	 * slots fils d'un slot splitté
	 */
	std::vector<OCP_CpTrackingSlot*> _sonsSplit;

	/**
	 * indique qu'on a procédé au renseignement des slots buddies
	 */
	bool _processedBuddies;

	/**
	 * indique que le passage est le plus long parmi ceux candidats pour une contrainte SAT1
	 */
	bool _isLongestInSat1;

	/**
	 * flag maintenu par la contrainte SAT2MAX : toujours faux si ce slot n'intervient pas dans une SAT2MAX
	 * sinon, il est positionné de manière réversible à vrai si le slot est le candidat compatible le plus éloigné
	 */
	IlcRevBool _isNextSAT2MAXCandidate;

	/**
	 * chacune des SAT1 est mémorisée par un indice . Si le slot courant est candidat pour cette SAT1, cet indice
	 * apparait dans la clé , le contenu à cette clé est la variable ensembliste de SAT1
	 */
	std::vector<int> _sat1Indexes;

	/**
	* chacune des SAT1 est mémorisée par un indice . Si le slot courant est candidat pour cette SAT1, cet indice
	* apparait dans la clé , le contenu à cette clé est la variable ensembliste de SAT1
	*/
	std::vector<int> _sat3Indexes;

public:

    /**
     *
     * @param satellite
     * @param station
     * @param slot_ origine dans le modèle métier
     * @return
     */
    OCP_CpTrackingSlot(OCP_CpSatellitePtr, OCP_CpStationPtr,  OcpSatelliteSlotPtr slot_);



    /**
     * récupère sur un passage reservé dans le passé l'aoslos à partir des informations courantes renseignées sur la contrainte
     * SAT4
     * @param slot_ slot origine
     * @return l'aos los associé au slot ayant une durée supérieure à la durée min renseignée par SAT4 au dessus de l'élévation
     * renseignée par SAT4
     */
    static OCP_CpTimeSlot getPastAosLos(OcpSatelliteSlotPtr slot_);

    /**
     * satellite associé
     * @return le satellite du slot
     */
	virtual OCP_CpSatellitePtr getSatellite() const {return _satellite;}

	/**
	 * la station associée au slot
	 * @return station associée
	 */
	virtual OCP_CpStationPtr getStation() const {return _station;}

	/**
	 * chaîne de caractères pour le slot
	 * @return une chaîne de caractères représentant le slot
	 */
	std::string getName() const;

	/**
	 * redéfinition de l'amplitude pour tenir compte des durées des trous si positionnés
	 * @return l'amplitude de l'intervalle éventuellement amputée de la somme des durées des trous
	 */
	OCP_CpDate getDuration() const;

	/**
	 * renseigne l'amplitude amputée des trous
	 * @param durationMinusHole_ l'amplitude fin - début - somme des trous (en provenance de contrainte SAT4)
	 */
	void setDurationHole(OCP_CpDate durationHole_) {_durationHole = durationHole_;}

	/**
	 * renvoie la durée du trou de visibilité positionnée par SAT4
	 * @return la durée du trou de visibilité positionnée par SAT4
	 */
	OCP_CpDate getDurationHole() const {return _durationHole;}

	/**
	 *
	 * @return : le slot d'origine dans le modèle métier
	 */
	OcpSatelliteSlotPtr getSlotOrigin() const {return _slot;}


	/**
	 * affichage du slot avec info satellite , station, debut, fin
	 * @return affichage complet
	 */
	boost::format getFullName() const;

	/**
	 * raccourci pour afficher le nom d'un satellite slot
	 * @param aSatSlot satellite slot
	 * @return le nom complet du passage satellite
	 */
	static boost::format getOriginFullName(OcpSatelliteSlotPtr aSatSlot);

	/**
	 * SAT4 renseigne la priorité des stations pour un satellite
	 * @param prioritySatellite_ priorité pour ce satellite de la station associée au passage courant
	 */
	void setPrioritySatellite(int prioritySatellite_) { _prioritySatellite = prioritySatellite_; }

    /**
     * SAT4 renseigne la priorité des stations pour un satellite
     * @param prioritySatellite_ priorité pour ce satellite de la station associée au passage courant
     */
    void setPrioritySearch(int prioritySatellite_) { _searchPriority = prioritySatellite_ ;}


	/**
	 * STA6 renseigne la priorité des satellites pour une station
	 * @param priorityStation_ priorité pour cette station du satellite associé au passage courant
	 */
	void setPriorityStation(int priorityStation_) { _priorityStation = priorityStation_;}

	/**
	 * priorité d'un passage satellite, croissant : 0 non prioritaire, 1 le plus prioritaire, 2 moins prioritaire...
	 * @return niveau de priorité.
	 */
	int getSearchPriority() const {return _searchPriority;}

	/**
	 * accès à la priorité satellite renseignée par SAT4
	 * @return la priorité satellite renseignée par SAT4
	 */
	int getSatellitePriority() const {return _prioritySatellite;}

	/**
	* accès à la priorité station renseignée par STA6
	* @return la priorité station renseignée par STA6
	*/
	int getStationPriority() const {return _prioritySatellite;}

	/**
	 * mise à jour de la priorité d'un slot de tracking en fonction des informations contenues dans STA6
	 */
	void updateSearchPriority();

	/**
	 * predicat de comparaison par priorité des slots satellites
	 * @param slot1 : premier slot à comparer
	 * @param slot2 : second slot à comparer
	 * @return vrai ssi slot1 est plus prioritaire que slot2
	 */
	static bool prioritySortPredicate(OCP_CpTrackingSlot* slot1, OCP_CpTrackingSlot* slot2);

	/**
	 * redéfinition de la classe mère OCP_CpTimeSlot
	 * @return true
	 */
	bool isTrackingSlot() const {return true;}

	/**
	 * accès à l'amplitude (start-end incluant temps de reconfiguration déconfiguration)
	 * @return l'amplitude start-end
	 */
	OCP_CpTimeSlotPtr getAmplitude() const {return _amplitude;}


	/**
	 * traitement du créneau satellite pour la sauvegarde : renseignemenent des champs TYPE_CRENEAU, NIVEAU_CRENEAU
	 */
	void postOptim();

	/**
	* accès au slot origine
	* @return le slot de tracking origine
	*/
	boost::shared_ptr <OcpRsrcSlot> getRscSlot() const {return _slot;}

	/**
	 * renseigne le père d'un slot splitté
	 * @param fatherSlot père du slot splitté
	 */
	void setFatherSplit(OCP_CpTrackingSlot* fatherSlot);

	/**
	 * accès au père du slot courant quand celui-ci est splitté
	 * @return le père du slot courant quand celui-ci est splitté
	 */
	OCP_CpTrackingSlot* getFatherSplit() const {return _fatherSplit;}

	/**
	 * renseigne un nouveau tracking splitté sur le slot origine
	 * @param sonSplit
	 */
	void addSonSplit(OCP_CpTrackingSlot* sonSplit) {_sonsSplit.push_back(sonSplit);}


	/**
	 * positionne le flag indiquant que le créneau est vu "le plus long possible" pour une contrainte SAT1
	 */
	void setIsLongestInSat1() {_isLongestInSat1 = true;}


    /**
     * passage le plus long pour une SAT1
     * @return vrai ssi le créneau est candidat le plus long pour une SAT1
     */
    bool isLongestInSat1() const {return _isLongestInSat1;}


    /**
     * accès aux slots splittés depuis le slot origine
     * @return slots splittés fils
     */
    std::vector<OCP_CpTrackingSlot*>& getSonsSplits() {return _sonsSplit;}


    /**
     * sauvegarde des fils à sauvegarder d'un tracking slot splitté
     * ce traitement fusionne les mini-slots contigus de la solution et crée des nouveaux slots pour les trous
     * @param solver : solver ILOG
     * @return le nombre de splits slots issus de fatherSplit et qu'il faut sauvegarder
     */
    int saveSplitSons(IloSolver solver);

    /**
    * slot id d'un slot de tracking
    * @return l'id du slot de tracking
    */
    long getSlotId() const;

    /**
     * accès au flag maintenu par la contrainte SAT2MAX : toujours faux si ce slot n'intervient pas dans une SAT2MAX
     * sinon, il est positionné de manière réversible à vrai si le slot est le candidat compatible le plus éloigné
     * @return vrai ssi le slot est le candidat compatible le plus éloigné
     */
    IlcBool isNextSAT2MaxCandidate() const {return _isNextSAT2MAXCandidate.getValue();}

    /**
     * positionne le flag "suis-je le prochain bon candidat pour SAT2MAX"
     * @param solver solver de résolution
     * @param flag vrai ssi le candidat est le prochain bon candidat pour SAT2MAX
     */
    void setNextSat2MaxCandidate(IloSolver solver, IlcBool flag) {_isNextSAT2MAXCandidate.setValue(solver,flag);}

    /**
     * mémorisation d'une contrainte SAT1 vue par le slot courant
     * @param sat1Index index de la SAT1 dans le modèle
     */
    void addSat1Index(int sat1Index) {_sat1Indexes.push_back(sat1Index);}

    /**
     * accès aux indices de contraintes SAT1
     * @return la liste des indices des contraintes SAT1 référençant ce slot
     */
    std::vector<int>& getSat1Indexes() {return _sat1Indexes;}

    /**
     * mémorisation d'une contrainte SAT3 vue par le slot courant
     * @param sat3Index index de la SAT3 dans le modèle
     */
    void addSat3Index(int sat3Index) {_sat3Indexes.push_back(sat3Index);}

    /**
     * accès aux indices de contraintes SAT3
     * @return la liste des indices des contraintes SAT3 référençant ce slot
     */
    std::vector<int>& getSat3Indexes() {return _sat3Indexes;}

protected:


	/**
	 * on a traité l'aspect slot buddies sur ce créneau splitté
	 */
	void setIsSlotBuddiesProcessed() {_processedBuddies = true;}

	/**
	 * statut reservé du slot orgine
	 * @return vrai ssi le slot origine est reservé
	 */
	bool isOriginReserved() const;

	/**
	* algorithme de mise en relation des slots buddies
	*/
	void processSlotBuddies();

};

typedef boost::shared_ptr < OCP_CpTrackingSlot >    OCP_CpTrackingSlotPtr;
typedef std::vector <  OCP_CpTrackingSlotPtr >  VectorOCP_CpTrackingSlot;

#endif /* OCP_CpTrackingSlot_H_ */
