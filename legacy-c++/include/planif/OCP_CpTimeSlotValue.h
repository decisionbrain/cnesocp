/*
 * $Id: OCP_CpTimeSlotValue.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#ifndef OCP_CpTimeSlotValue_H_
#define OCP_CpTimeSlotValue_H_



#include "planif/OCP_CpTimeSlot.h"
#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>


ILOSTLBEGIN

class OCP_CpTimeSlotValue
{
    /**
     * solver ILOG pendant la résolution
     */
    IloSolver _s;

    /**
     * maintient l' expression "somme des slots requis intersectant la plage"
     */
    IlcRevInt _levelMin;   // reversible

    /**
     * maintient l' expression "somme des slots possibles intersectant la plage"
     */
    IlcRevInt _levelMax;   // reversible

    /**
     * plage de temps
     */
    OCP_CpTimeSlot* _plage;

    /**
     * tous les slots intersectant l'intervalle
     */
    std::vector<OCP_CpTimeSlot*> _initialSlots;

private :

    /**
     * renseigner un nouveau niveau min
     * @param val
     */
    void setLevelMin(IlcInt val) {
        _levelMin.setValue(_s, val);
            }

    /**
     * renseigner un nouveau niveau max
     * @param val
     */
    void setLevelMax(IlcInt val) {
        _levelMax.setValue(_s, val);
            }

public:
    /**
     * constructeur de l'info plage
     * @param s ILOG solver
     * @param la plage temporellel
     * @return nouvelle instance de l'info plage
     */
    OCP_CpTimeSlotValue(IloSolver s, OCP_CpTimeSlot* plage):
        _s(s), _plage(plage) { setLevelMin(0); setLevelMax(0); }

    ~OCP_CpTimeSlotValue(){}

    /**
     * niveau des requis sur la plage
     * @return
     */
    IlcInt getLevelMin() const { return _levelMin; }

    /**
     * niveau des possibles sur la plage
     * @return
     */
    IlcInt getLevelMax() const { return _levelMax; }


    /**
     * incrément des requis sur la plage
     * @return le nouveau niveau des requis après incrément
     */
    int incLevelMin() {int prev = getLevelMin(); setLevelMin(prev+1); return getLevelMin();}

    /**
     * incrément des possibles sur la plage (initialisation)
     * @return le nouveau niveau des possibles après incrément
     */
    int incLevelMax() {int prev = getLevelMax(); setLevelMax(prev+1); return getLevelMax();}


    /**
     * décrément des possibles sur la plage
     * @return le nouveau niveau des possibles après décrément
     */
    int decLevelMax() {int prev = getLevelMax(); setLevelMax(prev-1);return getLevelMax();}

    /**
     * plage origine
     * @return renvoie la plage origine
     */
    OCP_CpTimeSlot* getPlage() const {return _plage;}

    /**
     * mémorisation initiale d'un slot candidat
     * @param slot
     * @param requis vrai ssi le slot est déjà vu comme requis
     */
    void addInitialSlot(OCP_CpTimeSlot* slot,bool isRequired);

    /**
    * affichage de la plage et niveaux
    * @param withPHR : affichage des dates formattées (sinon entier)
    */
    boost::format getName(bool withPHR) const;

    /**
     * accès aux slots initiaux
     * @return le vecteur des slots initiaux
     */
    std::vector<OCP_CpTimeSlot*>& getInitialSlots() {return _initialSlots;}
};

#endif // !defined(OCP_CpTimeSlotValue_H_)
