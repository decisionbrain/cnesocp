/*
 * $Id: OCP_CpTimeSlotTimetable.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#ifndef OCP_CpCstIntTimetable_H_
#define OCP_CpCstIntTimetable_H_



#include "planif/OCP_CpTimeSlot.h"
#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>
#include "OCP_CpTimeSlotValue.h"


ILOSTLBEGIN



class OCP_CpTimeSlotTimetable
{

private:

    /**
     * solver ILOG
     */
    IloSolver _solver;

    /**
     * liste des plages impactées par la modification d'un slot
     */
	std::vector<OCP_CpTimeSlotValue*> _plages;

	/**
	 * variable ensembliste de domaine l'ensemble des time slots concernés par la timetable
	 */
	IlcAnySetVar _tracking;

	/**
	 * étendue concerné par la timetable (les plages sont inclues dans cet intervalle)
	 */
	OCP_CpTimeSlotPtr _scope;

	/**
	 * cette timetable doit dans la solution respecter un niveau min sur tout le scope (-1 si non traité)
	 */
	int _levelMinRequired;

	/**
	* cette timetable doit dans la solution respecter un niveau max sur tout le scope (-1 si non traité)
	*/
	int _levelMaxRequired;

	/**
	 * indice de première plage intersectée par slot candidat
	 */
	std::map<OCP_CpTimeSlot*, int> _firstPlage;

	/**
	* indice de première plage intersectée par slot candidat
	*/
	std::map<OCP_CpTimeSlot*, int> _lastPlage;

public:

	/**
	 * initialiser la timetable avec tous les possibles
	 */
	void initialize();

	/**
	 * constructeur de timetable
	 * @param solver solveur ILOG
	 * @param tracking variable ensembliste de domaine l'ensemble des time slots concernés par la timetable
	 * @param scope étendue concernée par la timetable
	 * @param levelMinRequired niveau min requis sur l'ensemble du scope
	 * @param levelMaxRequired niveau max requis sur l'ensemble du scope
	 * @return nouvelle instance de timetable
	 */
	OCP_CpTimeSlotTimetable(IloSolver solver,IlcAnySetVar tracking,OCP_CpTimeSlotPtr scope, int levelMinRequired, int levelMaxRequired);

	virtual ~OCP_CpTimeSlotTimetable() {}

	/**
	 * affichage de la timetable
	 * @param withPHR : affichage des dates formattées (sinon entier)
	 */
	void display(bool withPHR);

	/**
	 * niveau min requis sur l'ensemble du scope
	 * @return le niveau min requis sur l'ensemble du scope (-1 si ce niveau min n'est pas requis)
	 */
	int getLeveMinRequired() const {return _levelMinRequired;}

	/**
	* niveau max requis sur l'ensemble du scope
	* @return le niveau max requis sur l'ensemble du scope (-1 si ce niveau max n'est pas requis)
	*/
	int getLeveMaxRequired() const {return _levelMaxRequired;}

	/**
	 * ajout d'un requis à la timetable
	 * @param newRequired nouveau requis
	 * @param atBound renseigne les plages ayant atteint la borne supérieure (si renseignée)
	 * @return vrai ssi le niveau max n'est pas dépassé
	 */
	bool increase(OCP_CpTimeSlot* newRequired, std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>& atBound );

	/**
	* suppression d'un possible à la timetable
	* @param oldPossible ancien possible
	* @param atBound renseigne les plages ayant atteint la borne inférieure (si renseignée)
	* @return vrai ssi on reste au-dessus du niveau min
	*/
	bool decrease(OCP_CpTimeSlot* oldPossible, std::map<OCP_CpTimeSlot*, OCP_CpTimeSlot*>& atBound );

	/**
	 * renvoie le maximum des niveau requis sur le scope
	 * @return le maximum des niveau requis sur le scope
	 */
	int getMaxOfLevelMin();

	/**
	* renvoie le minimum des niveau possibles sur le scope
	* @return le minimum des niveau possibles sur le scope
	*/
	int getMinOfLevelMax();

protected:

	/**
	 * initialise les dates des plages
	 */
	void initializeDates();

	/**
	 * initialise les valeurs des plages
	 */
	void initializeValues();
};


#endif // !defined(OCP_CpCstIntTimetable_H_)
