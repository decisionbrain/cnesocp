/*
 * $Id: OCP_CpCumulTimeSlot.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#ifndef OCP_CpCumulTimeSlot_H_
#define OCP_CpCumulTimeSlot_H_

#include <string>
#include <list>

#include <ilsolver/ilosolver.h>

#include "planif/OCP_CpTimeSlot.h"


using namespace std ;

typedef std::vector<OCP_CpTimeSlotPtr> TTList;

class OCP_CpCumulTimeSlot;
ILCREV(OCP_CpCumulTimeSlot);//permet la réversibilité de la structure pour ILOG SOLVER

class OCP_CpCumulTimeSlot
{
private:
    /**
     * capacité minimale de la timetable
     */
    int _capacityMin;

    /**
      * capacité maximale de la timetable
      */
	int _capacityMax;

	/**
	 * liste ordonnée chronologiquement d'intervalles temporels disjoints
	 */
	TTList _timeTable;

	/**
	 * taille maximale d'une zone à 0 sur la timetable (-1 si valeur non traitée)
	 */
	int _maxHole;

public:
	/**
	 * constructeur de la structure réversible de timetable
	 * @param capacityMin capacité min de la timetable (0 si non renseignée)
	 * @param capacityMax capacité max de la timetable (-1 si non renseignée)
	 * @return instance de timetable
	 */
	OCP_CpCumulTimeSlot(int capacityMin, int capacityMax);

	/**
	 * ajout d'un intervalle temporel et modification de la timetable
	 * @param tsCAND nouvel intervalle temporel
	 * @return vrai ssi la capacitée mise à jour ne dépasse pas strictement la capacité max
	 */
	bool addCapacity(OCP_CpTimeSlotPtr tsCAND);

	/**
	* retrait d'un intervalle temporel et modification de la timetable
	* @param tsCAND nouvel intervalle temporel
	* @return vrai ssi la capacitée mise à jour reste supérieure ou égale à la capacité min
	*/
	bool decreaseCapacity(OCP_CpTimeSlotPtr tsCAND);

	/**
	 * affichage de tous les intervalles de la timetable
	 */
	void showCapacitySlots();


    /**
     * renseigner le trou maximal de visibilité : durée maximale d'une zone à 0 sur la timetable
     * @param maxHole_
     */
    void setMaxHole(int maxHole_) {_maxHole = maxHole_;}

    /**
     * calcule l'intervalle fournissant le niveau (capacité) minimum/maximum parmi tous les intervalles
     * @return l'intervalle fournissant le niveau (capacité) minimum/maximum parmi tous les intervalles
     */
    OCP_CpTimeSlot getMinMaxLevels() const;


private:
	/**
	 * traitement de la partie "droite" de l'intersection de deux intervalles
	 * @param tsCAND intervalle candidat
	 * @param tsREF intervalle de référence
	 * @return l'intervalle allant de la fin la plus tôt à la fin la plus tard
	 */
	OCP_CpTimeSlotPtr getForwardTimeSlot(OCP_CpTimeSlotPtr tsCAND, OCP_CpTimeSlotPtr tsREF);

	/**
	 * traitement de la partie "gauche" de l'intersection de deux intervalles
	 * @param tsCAND intervalle candidat
	 * @param tsREF intervalle de référence
	 * @return l'intervalle allant du début le plus tôt au début le plus tard
	 */
	OCP_CpTimeSlotPtr getBackwardTimeSlot(OCP_CpTimeSlotPtr tsCAND, OCP_CpTimeSlotPtr tsREF);

	/**
	 * intervalle d'intersection entre deux intervalles qui s'intersectent, sans calcul de capacité sur l'intervalle retourné
	 * @param tsCAND intervalle candidat
	 * @param tsREF intervalle de référence
	 * @return intervalle d'intersection entre le candidat et la référence
	 */
	OCP_CpTimeSlotPtr getIntersectTimeSlot(OCP_CpTimeSlotPtr tsCAND, OCP_CpTimeSlotPtr tsREF);

	/**
	 * capacité maximale de la timetable
	 * @return niveau de capacité maximale
	 */
	int getCapacityMax() const {return _capacityMax;}

	/**
	* capacité minimale de la timetable
	* @return niveau de capacité minimale
	*/
	int getCapacityMin() const {return _capacityMin;}

	/**
	 * pour un nouvel intervalle de capacité nulle, parcourir les autres intervalles de capacité nulle autour de cet intervalle
	 * @param current le nouvel intervalle de capacité nulle
	 * @return la somme des durées des intervalles de durée nulle autour de l'intervalle courant (y compris celui-ci)
	 */
	int computeLocalHole(TTList::iterator current);
};


#endif /* OCP_CpCumulTimeSlot_H_ */
