/*
 * $Id: OCP_CpSearch.h 943 2010-12-21 13:12:51Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSearch.h
 *
 */

#ifndef OCP_CpSearch_H_
#define OCP_CpSearch_H_

#include <ilsolver/ilosolver.h>
#include <planif/OCP_CpTimeSlot.h>

using namespace std;

ILOSTLBEGIN

IlcGoal OCP_CpSimpleInstantiate(IloSolver s, IlcIntVar x);
IlcGoal OCP_CpSimpleGenerate(IloSolver s, IlcIntVarArray x);
IloGoal OCP_CpSimpleConcertGenerate (IloEnv env, IloIntVarArray x);

/*
 *  GOALS SUR DES VARIABLES ENSEMBLISTES
 *  */
IloGoal OCP_CpCompleteInstantiateAny(IloEnv env,IloAnySetVar x,int traceChoicePoints);
IloGoal OCP_CpCompleteGenerate(IloEnv env,IloAnySetVar x);
IloGoal OCP_CpInstantiate(IloEnv env,IloAnySetVar x);
IloGoal OCP_CpReserveSlots(IloEnv env);
IloGoal OCP_CpPropagate(IloEnv env);
IloGoal OCP_CpRemoveFirst(IloEnv env,IloAnySetVar x,bool traceChoicePoints);


/**
 * sélecteur du prochain élèment à placer dans les requis d'une variable ensembliste
 */
class OCP_CpSelectorI: public IlcIntSetSelectI
{
public:
    OCP_CpSelectorI(){};

    /**
     * sélection du prochain créneau
     * @param var variable ensembliste globale
     * @return le prochain créneau sélectionné, NULL si fin de l'arbre
     */
    virtual IlcAny select(IlcAnySetVar var);

protected:

    /**
     * comparaison par niveau hiérarchique de critère
     * @param levelCrit : niveau hiérarchique de critère
     * @param bestSlot : meilleur slot courant
     * @param slot : slot candidat
     * @return : 1 si si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT(int levelCrit , OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT0 : Un créneau NULL est toujours dominé
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT0(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT1 : Un créneau satellite est avant un crénau de maintenance
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT1(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;


    /**
     * comparaison suivant le critère CRIT2 : Un créneau de maintenance de durée d1 est avant un créneau maintenance de durée d2 > d1
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT2(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT3 : Un créneau satellite prioritaire pour SAT1 (plus long créneau) est avant un créneau satellite qui ne l'est pas
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT3(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT4 : Un créneau satellite améliorant une contrainte SAT1 MIN la moins satisfaite est prioritaire
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT4(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT5 : un créneau le plus éloigné du dernier SAT2MAX assigné est prioritaire par rapport à un créneau qui ne l'est pas
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT5(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT6 : Un créneau satellite de priorité aggrégée PR1 est avant un créneau satellite de priorité PR2 si PR1!=0 && PR1<PR2
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT6(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT7 : Un créneau satellite de priorité station PR1 est avant un créneau satellite de priorité station PR2 si PR1!=0 && PR1<PR2
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT7(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT8 : un créneau d'id interne id id1 est avant un créneau d'id interne id2 si id1 < id2
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT8(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

    /**
     * comparaison suivant le critère CRIT9 : satisfaire d'abord les contraintes SAT3 qui n'atteignent pas leur durée min
     * @param bestSlot : meilleur slot pour l'instant
     * @param slot : slot courant
     * @return 1 si bestSlot domine slot, -1 si slot domine bestSlot, 0 sinon
     */
    int compareCRIT9(OCP_CpTimeSlot* bestSlot, OCP_CpTimeSlot* slot) const;

};


#endif /* OCP_CpSearch_H_ */
