/*
 * $Id: OCP_CpMaintenanceSlot.h 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpMaintenanceSlot.h
 *
 */

#ifndef OCP_CpMaintenanceSlot_H_
#define OCP_CpMaintenanceSlot_H_

#include <string>

#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

class OCP_CpStation;

using namespace std ;

class OCP_CpMaintenanceSlot: public OCP_CpTimeSlot
{
private:

    /**
     * slot origine, renseigné pour les créneaux reservés ou créé
     * pour ceux créés par l'algorithme de création des créneaux de maintenance
     */
    OcpStationSlotPtr _slotOrigine;

    /**
     * station origine
     */
    OCP_CpStationPtr _station;

	/**
	 * flag indiquant que ce slot de maintenance n'intersecte aucun tracking slot
	 */
	bool _offVisibility;

protected:

	   /**
	    * accès au slot origine
	    * @return le slot de maintenance origine
	    */
	    boost::shared_ptr <OcpRsrcSlot> getRscSlot() const {return _slotOrigine;}

public:

	/**
	 * constructeur généré par l'algorithme de création des créneaux de maintenance
	 * @param aStation station origine
	 * @param begin debut du créneau
	 * @param end fin du créneau
	 * @return nouvel objet de type slot de maintenance pour solveur
	 */
    OCP_CpMaintenanceSlot(OCP_CpStationPtr aStation,  OCP_CpDate begin , OCP_CpDate end);

    /**
     * constructeur depuis un slot de maintenance donné préfixé par l'utilisateur
     * @param slot_
     * @return nouvel objet de type slot de maintenance pour solveur
     */
    OCP_CpMaintenanceSlot(OcpStationSlotPtr slot_);

    /**
     * station associée au slot de maintenance
     * @return la station associée au slot de maintenance
     */
	OCP_CpStationPtr getStation() const {return _station;}

	/**
	 * accès au slot origine, créé ou connu en cas de préréservation
	 * @return le slot origine
	 */
	OcpStationSlotPtr getSlotOrigin() const {return _slotOrigine;}

	/**
	 * nom du slot de maintenance
	 * @return le nom du slot de maintenance
	 */
	std::string getName() const;

	/**
	 *
	 * @return vrai si et seulement si le slot de maintenance n'intersecte aucun tracking slot
	 */
	bool isOffVisibility() {return _offVisibility;}

	/**
	 * notifie le slot de maintenance qu'il intersecte une visibilité satellite
	 */
	void setOnVisibility() {_offVisibility = false;}

	/**
	* notifie le slot de maintenance qu'il n'intersecte aucune visibilité satellite
	*/
	void setOffVisibility() {_offVisibility = true;}

	/**
	* redéfinition de la classe mère OCP_CpTimeSlot
	* @return true
	*/
	bool isMaintenanceSlot() const {return true;}

    /**
     * traitement du créneau satellite pour la sauvegarde : renseignemenent des champs TYPE_CRENEAU, NIVEAU_CRENEAU
     */
    void postOptim();

    /**
     * chaine plus complete
     * @return description complète
     */
    boost::format getFullName() const;

    /**
    * slot id d'un slot de maintenance
    * @return l'id du slot de maintenance
    */
    long getSlotId() const;


};

#endif /* OCP_CpMaintenanceSlot_H_ */
