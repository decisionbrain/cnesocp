/*
 * $Id: OCP_CpConstraint.h 918 2010-11-04 14:41:09Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpConstraint.h
 *
 */

#ifndef OCP_CpConstraint_H_
#define OCP_CpConstraint_H_

#include <string>
#include <OcpPeriod.h>

#include "context/OCP_CpObject.h"
#include "planif/OCP_CpTimeSlot.h"

#include <ilsolver/ilosolver.h>

using namespace std ;
using namespace ocp::commons::io;

class OCP_CpTrackingSlot;

class OCP_CpConstraint: public OCP_CpObject
{
private:

	/**
	 * intervalle de validité de la contrainte métier origine (converti en secondes)
	 */
	OCP_CpTimeSlotPtr _validity;

	/**
	 * la contrainte ILOG Solver issue de l'instanciation de la contrainte OCP_CpConstraint
	 */
	IloConstraint _constraint;

	/**
	 * renseigné à la lecture, ce flag évite d'instancier la contrainte
	 */
	bool _hasInstance;

	/**
	 * indique le passage au mode SAM pour la contrainte
	 */
	bool _atBestMode;

protected:

	/**
	* contrainte OCP origine
	*/
	OcpConstraintPtr _origin;


    /**
     * liste des slots candidats pour une contrainte réservant des slots
     */
    IloAnyArray _domainSlots;

    /**
     * liste des slots à supprimer pour cette contrainte suite à l'algorithme de suppression des slots superflus
     */
    IloAnyArray _complementarySlots;



protected:

	/**
	 * renseigner la contrainte ILOG SOLVER
	 * @param newConstraint la contrainte ILOG SOLVER issue de l'instanciation de la contrainte OCP_CpConstraint
	 */
	void setIloConstraint(IloConstraint newConstraint){_constraint = newConstraint; _constraint.setObject(this);}

	/**
	 * indique que la contrainte a vu un créneau de tracking
	 * @param slot le créneau de tracking
	 */
	void attachSlot(OCP_CpTrackingSlot* slot);

	/**
	 * ajout d'un slot de tracking aux slots candidats pour la contrainte
	 * @param slot : le candidat
	 */
	void addCandidateToDomain(OCP_CpTimeSlot* slot);


public:

    /**
     * création de la contrainte ILOG SOLVER
     * @param optimScope fenêtre de calcul courante
     */
    virtual void instantiate(OCP_CpTimeSlotPtr optimScope) = 0;

    /**
     * indique si la contrainte est une contrainte de type Satellite
     * @return vrai ssi la contrainte est une contrainte de type Satellite
     */
    virtual bool isSatelliteConstraint() const =0;



    /**
     * classe mère de toutes les contraintes encapsulant des contraintes ILOG SOLVER
     * @param period période de validité de la contrainte
     * @param origine contrainte métier origine
     * @param atBestMode indique si la contrainte sera traitée en mode at Best
     * @return l'instance de la contrainte
     */
    OCP_CpConstraint(OCP_CpTimeSlotPtr period, OcpConstraintPtr origine, bool atBestMode_ = false);

    /**
     * début de la validité de la contrainte
     * @return le début de la validité de la contrainte en secondes
     */
    OCP_CpDate getValidityBegin() const {return _validity->getBegin();}

    /**
     * date de fin de la validité de la contrainte métier origine
     * @return la fin de la validité de la contrainte en secondes
     */
    OCP_CpDate getValidityEnd() const {return _validity->getEnd();}

    /**
     * accès à la contrainte ILOG SOLVER
     * @return la contrainte ILOG SOLVER issue de l'instanciation de l'OCP_CpConstraint
     */
	IloConstraint getIloConstraint(){return _constraint;}

	/**
	 * création des plages horaires sur une fenêtre de calcul , avec traitement des périodes, éventuellement à cheval entre deux jours
	 * @param plageHoraires les plages horaires à renseigner
	 * @param beginRange début de la portée globale
	 * @param endRange fin de la portée globale
	 * @param beginOffset début relatif de la plage horaire
	 * @param endOffset fin relative de la plage horaire
	 * @param leftRight indique pour une plage en offset à cheval sur deux jours si l'on doit charger la plage de droite ET celle de gauche, sinon, on ne charge que celle de droite
	 */
	static void buildTimeAreas(VectorOCP_CpTimeSlot& plageHoraires , OCP_CpDate beginRange,OCP_CpDate endRange, OCP_CpDate beginOffset, OCP_CpDate endOffset, bool leftRight = false);

	/**
	 * eviter d'instancier une contrainte (champs non renseignés...)
	 * @return vrai ssi il n'y a pas lieu d'instancier la contrainte
	 */
	bool hasInstance() const {return _hasInstance;}

	/**
	 * indique que la contrainte ne devra pas être instanciée
	 */
	void setNoInstance() {_hasInstance = false;}

	/**
	 * mode SAM ou "At Best"
	 * @return vrai ssi la contrainte est traitée en mode SAM
	 */
	bool isModeAtBest() const {return _atBestMode;}

    /**
     * flag des contraintes OCP liées à des contraintes réservant des créneaux
     * SAT1, SAT7, SAT8 réservent des créneaux satellites
     * STA1, STA2, STA3 réservent des créneaux de maintenance
     * @return vrai ssi la contrainte réserve des créneaux (satellites ou stations)
     */
    bool isReserveConstraints() const;

    /**
    * indique si la contrainte est de type SAT1
    * @return vrai ssi la contrainte est de type SAT1
    */
    virtual bool isSAT1() const {return false;}

    /**
    * indique si la contrainte est de type SAT7
    * @return vrai ssi la contrainte est de type SAT7
    */
    virtual bool isSAT7() const {return false;}

    /**
    * indique si la contrainte est de type SAT8
    * @return vrai ssi la contrainte est de type SAT8
    */
    virtual bool isSAT8() const {return false;}

    /**
    * indique si la contrainte est de type SAT8
    * @return vrai ssi la contrainte est de type SAT4
    */
    virtual bool isSAT4() const {return false;}

    /**
    * indique si la contrainte est de type STA1
    * @return vrai ssi la contrainte est de type STA1
    */
    virtual bool isSTA1() const {return false;}

    /**
    * indique si la contrainte est de type STA2
    * @return vrai ssi la contrainte est de type STA2
    */
    virtual bool isSTA2() const {return false;}


    /**
    * indique si la contrainte est de type STA3
    * @return vrai ssi la contrainte est de type STA3
    */
    virtual bool isSTA3() const {return false;}

    /**
    * indique si la contrainte est de type STA4
    * @return vrai ssi la contrainte est de type STA4
    */
    virtual bool isSTA4() const {return false;}

    /**
    * indique si la contrainte est de type STA6
    * @return vrai ssi la contrainte est de type STA6
    */
    virtual bool isSTA6() const {return false;}



    /**
     * liste des slots candidats
     * @return la liste des slots candidats
     */
    IloAnyArray getDomainSlots() const {return _domainSlots;}

    /**
     * liste des slots complémentaires : non candidats mais inclus dans les plages horaires de la contrainte
     * @return
     */
    IloAnyArray getComplementarySlots() const {return _complementarySlots;}

    /**
     * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
     * @param slot le créneau à sauver
     */
    virtual void postOptim(OCP_CpTimeSlot* slot) {SUPPRESS_UNUSED_PARAMETER_WARNING(slot);}

    /**
     * en post-optimisation, modifie le champs NIVEAU_CRENEAU
     * @param slot : le créneau à modifier
     * @param editable : paramètre positionné par la contrainte
     */
    static void setSlotLevel(OCP_CpTimeSlot* slot, bool editable);

    /**
     * en post-optimisation, modifie le champs TYPE_CRENEAU
     * @param slot : le créneau à modifier
     * @param slotType : la chaine de caractère renseignée sur la contrainte satellite qui positionne l'information
     */
    static void setSupportType(OCP_CpTimeSlot* slot, std::string slotType);

    /**
     * export dans format texte pour préparation de données pour IBM-ILOG OPL
     * @param file
     */
    virtual void exportOplModel(ofstream& file);


};

typedef boost::shared_ptr < OCP_CpConstraint >    OCP_CpConstraintPtr;
typedef std::vector < OCP_CpConstraintPtr > VectorOCP_CpConstraint;

#endif /* OCP_CpConstraint_H_ */
