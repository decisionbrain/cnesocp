/*
 * $Id: OCP_CpSatelliteBaseConstraint.h 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteBaseConstraint.h
 *
 */

#ifndef OCP_CpSatelliteBaseConstraint_H_
#define OCP_CpSatelliteBaseConstraint_H_

#include <string>

#include "constraints/OCP_CpConstraint.h"
#include "resources/OCP_CpSatellite.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"


#include <ilsolver/ilosolver.h>

using namespace std ;

#define SUPPRESS_UNUSED_PARAMETER_WARNING(x) (void)x

/*
* OCP_CpSatelliteBaseConstraint : 
*
*/
class OCP_CpSatelliteBaseConstraint: public OCP_CpConstraint
{
protected:
    /**
     * le satellite associé à la contrainte
     */
	OCP_CpSatellitePtr _satellite;

	/**
	 * période étendue pour rechercher les passages candidats  : tient compte de la validité et des plages horaires, mais n'étend pas
	 * l'horizon aux contraintes mode multi-jours
	 */
	OCP_CpTimeSlotPtr _extendedScope;//TODO peut-être à déplacer sur la classe mère , ou bien utiliser directement la map des durées étendues par satellite sur OCP_CpModel

public:

	/**
	 * constructeur de classe mère des contraintes satellites
	 * @param period periode d'instanciation de la contrainte
	 * @param origine contrainte métier origine
	 * @param s satellite
	 * @return
	 */
	OCP_CpSatelliteBaseConstraint(OCP_CpTimeSlotPtr period, OcpConstraintPtr origine,  OCP_CpSatellitePtr s);

    /**
     *
     * @return le satellite associé à la contrainte
     */
    OCP_CpSatellitePtr getSatellite() const {return _satellite;}

    /**
     * renvoie la période étendue tenant compte de la validité, des caractéristiques de la contrainte : validité, périodes horaires,
     * jours précédents ou suivants
     * @return la période étendue
     */
    OCP_CpTimeSlotPtr getExtendedScope() const {return _extendedScope;}

    /**
    * redéfinie : indique si la contrainte est une contrainte de type Satellite
    * @return vrai ssi la contrainte est une contrainte de type Satellite
    */
    bool isSatelliteConstraint() const {return true;}

protected:

    /**
     * lance une exception si la station n'est pas compatible avec la fréquence demandée pour la contrainte
     * @param origin : contrainte OCP origine
     * @param aStation la station
     * @param satellite satellite de la contrainte origine
     * @param frequencyBand la fréquence demandée pour la contrainte
     */
    static void onFrequencyError(OcpConstraintPtr origin, OcpStationPtr aStation,OCP_CpSatellitePtr satellite, FrequencyBands::FrequencyBands_ENUM frequencyBand) throw(OCP_XRoot);

    /**
     * compatibilité entre la fréquence associée à une contrainte et la fréquence associée à une station donnée
     * @param constraintFrequency fréquence associée à la contrainte
     * @param stationFrequency fréquence de la station
     * @return vrai ssi les fréquences sont compatibles (même fréquence ou patterns (X,SX), (S,SX)
     */
    static bool frequencyCompatible(FrequencyBands::FrequencyBands_ENUM constraintFrequency , FrequencyBands::FrequencyBands_ENUM stationFrequency );

    /**
     * étend la portée d'une contrainte au delà de la fenêtre de calcul suite à l'analyse de ses plages horaires
     * @param timeSlot : la plage horaire
     */
    void enlargeExtenDedScope(const OCP_CpTimeSlot& timeSlot );

};

typedef boost::shared_ptr < OCP_CpSatelliteBaseConstraint >    OCP_CpSatelliteBaseConstraintPtr;

#endif /* OCP_CpSatelliteBaseConstraint_H_ */
