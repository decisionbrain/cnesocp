/*
 * $Id: OCP_CpSatelliteConstraint2.h 920 2010-11-04 18:11:31Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint2.h
 *
 */

#ifndef OCP_CpSatelliteConstraint2_H_
#define OCP_CpSatelliteConstraint2_H_

#include <string>
#include <constraints/OcpConstraintSatType2.h>


#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"
#include "planif/OCP_CpTimeSlot.h"

#include <ilsolver/ilosolver.h>

using namespace std ;
using namespace ocp::commons::io;


/*
* OCP_CpSatelliteConstraint2 : duree minimale entre 2 trackingSlots sur une periode de temps
*
*/
class OCP_CpSatelliteConstraint2: public OCP_CpSatelliteBaseConstraint
{
private:
    /**
     * filtre sur la durée minimale des passages concernés
     */
    OCP_CpDate _minDuration;

    /**
     * écart inter satellite
     */
	OCP_CpDate _delta;

	/*
	 *
	 * Type min ou max de l'écart demandé
	 */
	SatConstraintTypes::SatConstraintTypes_ENUM _typeEcart;

	/**
	 * Filtre sur les passages satellites par la fréquence de la station associée
	 */
	FrequencyBands::FrequencyBands_ENUM _frequencyBand;


public:
	/**
	 * accès à la valeur de l'écart en secondes
	 * @return écart (min ou max) inter satellites
	 */
	OCP_CpDate getDelta() {return _delta;}

	/**
	 * accès au type d'écart demandé (min ou max)
	 * @return le type d'écart inter satellites
	 */
	SatConstraintTypes::SatConstraintTypes_ENUM getTypeEcart() const {return _typeEcart;}


    /**
     * Constructeur à partir de l'objet métier
     * @param optimScope portée courante d'instanciation de la contrainte
     * @param cstrSat2 contrainte métier origine
     * @param satellite satellite associé
     * @return
     */
    OCP_CpSatelliteConstraint2(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType2Ptr aSat2Ct , OCP_CpSatellitePtr satellite);

    /**
     * export dans format texte pour préparation de données pour IBM-ILOG OPL
     * @param file
     */
    void exportOplModel(ofstream& file);



protected:

    /**
    * création de la contrainte ILOG SOLVER
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

    /**
     * pour une contrainte de type MAX, s'il existe un dernier créneau la veille à plus de l'écart, la contrainte est en erreur
     * @param optimScope fenêtre courante de calcul
     * @param maxDate : renseigne la date du dernier créneau candidat la veille (-1 si pas de créneau)
     * @return vrai ssi il existe un créneau "candidat la veille" à plus de max du premier créneau candidat
     */
    bool lastTrackingBeyondScope(OCP_CpTimeSlotPtr optimScope,    OCP_CpDate& maxDate);

};
//class OCP_CpSatelliteConstraint2;


#endif /* OCP_CpSatelliteConstraint2_H_ */
