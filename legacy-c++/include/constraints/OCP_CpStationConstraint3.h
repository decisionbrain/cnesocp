/*
 * $Id: OCP_CpStationConstraint3.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint3.h
 *
 */

#ifndef OCP_CpStationConstraint3_H_
#define OCP_CpStationConstraint3_H_

#include <string>

#include <constraints/OcpConstraintStaType3.h>

#include "constraints/OCP_CpStationBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;

/*
* OCP_CpStationConstraint3 : 
*
*/
class OCP_CpStationConstraint3: public OCP_CpStationBaseConstraint
{
private:
    /**
     * nombre de jours à prendre en compte
     */
    int _horizon;

    /**
     * durée minimale en secondes
     */
    OCP_CpDate _minimalDuration;

    /**
     * type des slots : lu depuis la contrainte origine et à positionner sur les slots référencés par cette contrainte
     */
    std::string _slotType;


public:

    /**
        * Constructeur à partir de la contrainte métier
        * @param period période d'instanciation de la contrainte
        * @param ctrSta3 contrainte métier origine
        * @param station station associé à la contrainte
        * @return
        */
      OCP_CpStationConstraint3(OCP_CpTimeSlotPtr period, OcpConstraintStaType3Ptr ctrSta3,OCP_CpStationPtr station);

      /**
      * indique si la contrainte est de type STA3
      * @return vrai ssi la contrainte est de type STA3
      */
      bool isSTA3() const {return true;}

      /**
       * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
       * @param slot le créneau à sauver
       */
      void postOptim(OCP_CpTimeSlot* slot);

protected:
      /**
      * création de la contrainte ILOG SOLVER
      * @param optimScope fenêtre de calcul courante
      */
      void instantiate(OCP_CpTimeSlotPtr optimScope);

      /**
       * pose la contrainte ILOG sur la cardinalité des slots candidats. Il en faut au moins un de durée minimale dans la fenêtre de calcul
       * @param optimScope fenêtre de calcul
       */
      void setCardinalityConstraint(OCP_CpTimeSlotPtr optimScope);

};

//extern IloConstraint IloStationConstraint3(IloEnv, OCP_CpDate, OCP_CpDate, IloAnySetVar, IloAnySetVar, int, int, const char* name=0);

#endif /* OCP_CpStationConstraint3_H_ */
