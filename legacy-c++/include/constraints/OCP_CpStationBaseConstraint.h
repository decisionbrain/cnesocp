/*
 * $Id: OCP_CpStationBaseConstraint.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationBaseConstraint.h
 *
 */

#ifndef OCP_CpStationBaseConstraint_H_
#define OCP_CpStationBaseConstraint_H_

#include <string>

#include "constraints/OCP_CpConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;

/**
 * classe mère des contraintes PPC associées à une station
 */
class OCP_CpStationBaseConstraint: public OCP_CpConstraint
{
private:
    /**
     * la station associée à la contrainte
     */
	OCP_CpStationPtr _station;

public:

    /**
     * constructeur de la classe mère des contraintes station
     * @param period période d'instanciation de la contrainte
     * @param origine contrainte métier origine
     * @param station station associée
     * @return
     */
    OCP_CpStationBaseConstraint(OCP_CpTimeSlotPtr period, OcpConstraintPtr origine,OCP_CpStationPtr station);


    /**
     * renvoie la station associée à la contrainte
     * @return la station associée à la contrainte
     */
    OCP_CpStationPtr getStation(){return _station;}

    /**
    * redéfinie : indique si la contrainte est une contrainte de type Satellite
    * @return vrai ssi la contrainte est une contrainte de type Satellite
    */
    bool isSatelliteConstraint() const {return false;}


};

#endif /* OCP_CpStationBaseConstraint_H_ */
