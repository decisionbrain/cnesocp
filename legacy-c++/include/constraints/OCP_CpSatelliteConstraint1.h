/*
 * $Id: OCP_CpSatelliteConstraint1.h 918 2010-11-04 14:41:09Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint1.h
 *
 */

#ifndef OCP_CpSatelliteConstraint1_H_
#define OCP_CpSatelliteConstraint1_H_

#include <string>
#include <constraints/OcpConstraintSatType1.h>

#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"


#include <ilsolver/ilosolver.h>

using namespace std ;

/*
* OCP_CpSatelliteConstraint1 : 
*	nbMin de suivi de duree superieure a un seuil (_threshold), sur une periode de temps
*
*/
class OCP_CpSatelliteConstraint1: public OCP_CpSatelliteBaseConstraint
{
private:

    /**
     * Filtre sur les passages satellites par la fréquence de la station associée
     */
    FrequencyBands::FrequencyBands_ENUM _frequencyBand;

    /**
     * Nombre minimum de passages (ou créneaux de support)
     */
    int32_t _nbMinPassages;

    /**
     * Nombre maximum de passages (ou créneaux de support). TODO Un code
     * spécifique doit permettre de signifier qu’on désire le maximum de
     * passages (ou créneaux de support) disponibles.
     */
    int32_t _nbMaxPassages;

    /**
     * liste stations concernées par la contrainte
     */
    std::vector <OcpStationPtr> _stations;

    /**
     * map des stations concernées par la contrainte
     */
    StationMap _stationsMap;

    /**
     * Durée minimale du passage ou créneau de support (en secondes). TODO Un
     * code doit permettre de sélectionner le passage le plus long disponible.
     */
    int32_t _minDuration;

    /**
     * indicateur visant à sélectionner en priorité le créneau de durée la plus longue
     */
    bool _useGreatestDuration;

    /**
     * indicateur visant à sélectionner le maximum de passages
     */
    bool _useGreatestFlightPass;


    /**
     * les plages horaires HH:MM origine
     */
    VectorOcpTimePeriod _plagesOffset;

    /**
     * plages horaires limitant le scope de la contrainte : seuls les passages de ces plages sont comptabilisés
     */
    VectorOCP_CpTimeSlot _plagesHoraires;

    /**
     * Les jours concernant la contrainte : en pratique, on instanciera une contrainte solver sur le dernier jour
     * en tenant compte des réservations précédentes (TODO comment ?) sinon, il faudra donner un ordre de priorité pour les slots
     * candidats des jours précédents
     */
    std::vector <WeekDays::WeekDays_ENUM> _constraintDays;


    /**
     * type des slots : lu depuis la contrainte origine et à positionner sur les slots référencés par cette contrainte
     */
    std::string _slotType;

    /**
     * indique le status du NIVEAU_CRENEAU : les slots reservés par cette contrainte seront RESERVED (si _editable à TRUE)
     * ou RESERVED_LOCKED (si _editable à FALSE)
     */
    bool _editable;

    /**
     * indique la présence d'une ou plusieurs plages à cheval sur un jour
     */
    bool _atHorse;


public:

    /**
     * Constructeur à partir de la contrainte métier
     * @param period période d'instanciation de la contrainte
     * @param ctrSat1 contrainte métier origine
     * @param satellite satellite associé à la contrainte
     * @return
     */
    OCP_CpSatelliteConstraint1(OCP_CpTimeSlotPtr period, OcpConstraintSatType1Ptr ctrSat1, OCP_CpSatellitePtr satellite);

protected:

    /**
    * création de la contrainte ILOG SOLVER
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

    /**
    * création de la contrainte ILOG SOLVER en mode multi-jours
    * @param optimScope fenêtre de calcul courante
    */
    void instantiateWeekMode(OCP_CpTimeSlotPtr optimScope);

    /**
    * création de la contrainte ILOG SOLVER en mode jour sans période à cheval
    * @param optimScope fenêtre de calcul courante
    */
    void instantiateNotAtHorse(OCP_CpTimeSlotPtr optimScope);

    /**
    * création de la contrainte ILOG SOLVER en mode jour avec période à cheval
    * @param optimScope fenêtre de calcul courante
    */
    void instantiateAtHorse(OCP_CpTimeSlotPtr optimScope);

    /**
     * instantiation d'une contrainte en mode "jour à cheval"
     * @param candidates les slots candidats de la journée courante
     * @param ctrAggregate la contrainte aggrégeant le jour de la fenêtre de calcul et la veille
     * @param minPassages nombre min de passages sur la journée courante
     * @param maxPassages nombre max de passages sur la journée courante
     */
    void instantiateOneDayAtHorse(IloAnyArray candidates,IloAnd ctrAggregate,int minPassages, int maxPassages);

    /**
    * calcul du nombre de passages concernés sur tous les jours avant le jour de la plage courante
    * il est nécessaire de le faire à ce moment car certains slots peuvent être à la fois reservés dans un calcul précédent et faire
    * partie des possibles du satellite sur la fenêtre courante. Il faut alors ne pas doublonner ceux-ci.
    * @param optimScope
    * @return nombre de passages passés en mode semaine
    */
    int computePastPassages(OCP_CpTimeSlotPtr optimScope);

    /**
    * collection des slots candidats sur la portée courante et calcul sur le passé si nécessaire
    * @param optimScope fenêtre de calcul courante
    * @param lastDay renseigné pour indiquer si l'on est le dernier jour de la série
    * @param pastPassages renseigne le nombre de slots candidats en mode semaine dans le passé
    * @param candidates le tableau des crénaux potentiels sur la fenêtre courante
    * @param candidatesDayBefore : le tableau des créneaux potentiels instanciant la contrainte la veille en cas de plage à cheval
    * @param nbReserved : nombre de créneaux reservés sur la fenêtre courante
    * @param nbReservedBefore : nombre de créneaux reservés sur la fenêtre de la veille (mode jour et plage à cheval)
    */
    void collectAllSlots(OCP_CpTimeSlotPtr optimScope,bool& lastDay,int& pastPassages,IloAnyArray candidates, IloAnyArray candidatesDayBefore,int& nbReserved, int& nbReservedBefore);

    /**
    * indique si la contrainte est de type SAT1
    * @return vrai ssi la contrainte est de type SAT1
    */
    virtual bool isSAT1() const {return true;}

    /**
     * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
     * @param slot le créneau à sauver
     */
    void postOptim(OCP_CpTimeSlot* slot);

    /**
     * export dans format texte pour préparation de données pour IBM-ILOG OPL
     * @param file
     */
    void exportOplModel(ofstream& file);


};

#endif /* OCP_CpSatelliteConstraint1_H_ */
