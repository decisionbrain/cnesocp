/*
 * $Id: OCP_CpObjectiveOverSlots.h 906 2010-10-22 13:29:40Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpObjectiveOverSlots.h
 *
 */

#ifndef OCP_CpObjectiveOverSlots_H
#define OCP_CpObjectiveOverSlots_H

#include <list>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>


#include <planif/OCP_CpTimeSlot.h>
#include <planif/OCP_CpTrackingSlot.h>


/*
 *
 */


class IlcObjectiveOverSlotsConstraintI : public IlcConstraintI
{
protected:

    /**
     * variable ensembliste
     */
    IlcAnySetVar _set;

    /**
     * objectif cumulé sur les slots
     */
    IlcIntVar _cumul;

public:
    IlcObjectiveOverSlotsConstraintI(IloSolver s, IlcAnySetVar x, IlcIntVar y) :
        IlcConstraintI(s), _set(x), _cumul(y) {}

    ~IlcObjectiveOverSlotsConstraintI() {}

    virtual void post();

    virtual void propagate();

    void propagateSetOnCumul();

protected:
    /**
     * pour chacun des slots, calcule son poids correspondant dans la fonction objectif
     * @param slot le slot candidat, tracking ou maintenance
     * @return le poids du slot candidat dans la fonction objectif
     */
    int computeWeight(IlcAny slot);
};


#endif /* OCP_CpObjectiveOverSlots_H */
