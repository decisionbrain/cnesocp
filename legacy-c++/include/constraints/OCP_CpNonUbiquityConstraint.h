/*
 * $Id: OCP_CpNonUbiquityConstraint.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpConstraint.h
 *
 */

#ifndef OCP_CpNonUbiquityConstraint_H
#define OCP_CpNonUbiquityConstraint_H

#include <list>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>


#include <planif/OCP_CpTimeSlot.h>
#include <planif/OCP_CpTrackingSlot.h>


/*
 *  IlcNonUbiquityIntraConstraintI: non ubiquite dans UNE activite (tracking ou maintenance)
 *  quand un slot est retenu, on elimine ceux non encore retenus qui l'intersecte
 *
 */
/*
 *  IlcNonUbiquityInterConstraintI: non ubiquite entre DEUX activites (tracking ET maintenance)
 *  quand un slot est retenu dans une activite,
 *  on elimine ceux non encore retenus dans la seconde activite qui l'intersecte
 */


class IlcNonUbiquityConstraintI : public IlcConstraintI
{
protected:
    IlcAnySetVar _set1;
    IlcAnySetVar _set2;
public:
    IlcNonUbiquityConstraintI(IloSolver s, IlcAnySetVar x, IlcAnySetVar y);
    ~IlcNonUbiquityConstraintI() {}

    virtual void post();
    virtual void propagate();

    void propagateNonUbiquityWithDemon(IlcAnySetVar x, IlcAnySetVar y);

    /**
     * intersection entre deux créneaux de la station
     * @param slot1 premier créneau
     * @param slot2 second créneau
     * @return vrai ssi les deux créneaux s'intersectent. Tient compte de l'amplitude pour un crénau satellite
     */
    bool intersect(IlcAny slot1, IlcAny slot2);

    /**
     * supprimer de l'ensemble des possibles les candidats possibles intersectant un nouveau requis
     * @param slotRequired : nouveau slot requis
     * @param possibleSetVar : ensemble des créneaux possibles concernés par le remove
     */
    void removeFromPossible(IlcAny slotRequired, IlcAnySetVar possibleSetVar);
};


#endif /* OCP_CpNonUbiquityConstraint_H */
