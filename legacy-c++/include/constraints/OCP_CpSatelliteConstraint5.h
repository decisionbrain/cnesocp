/*
 * $Id: OCP_CpSatelliteConstraint5.h 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint5.h
 *
 */

#ifndef OCP_CpSatelliteConstraint5_H_
#define OCP_CpSatelliteConstraint5_H_

#include <string>
#include <map>
#include <constraints/OcpConstraintSatType5.h>
#include <OCP_CpHomere.h>

#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"


#include <ilsolver/ilosolver.h>

using namespace std ;
using namespace ocp::commons::io;

/*
* OCP_CpSatelliteConstraint5 : 
*
*/

/**
 * nombre de passages simultanés max
 * 1 seul passage satellite simultané parmi SPOT2, SPOT4 et SPOT5 entre 14h30 et 24h, quelle que soit la
station
2 passages satellites simultanés parmi ELISA1, ELISA2, ELISA3 et ELISA4 entre 16h et 20h pour les
stations KRN et SKRN01
 *
 */
class OCP_CpSatelliteConstraint5: public OCP_CpSatelliteBaseConstraint
{
private:

    /**
     *    Nombre de satellites autorisés simultanément (NB_SAT_AUT)
     *
     */
    uint16_t _nbSimultSat;

     /**
      * map des stations concernées par la contrainte
      */
    StationMap _stationsMap;

     /**
      * map des satellites concernées par la contrainte
      */
     SatelliteMap _satelliteMap;

     /**
      * la plage horaire , éventuellement dupliquée en cas de chevauchement 24/24
      */
     VectorOCP_CpTimeSlot _plagesHoraires;

public:

    /**
     * Constructeur à partir de la contrainte métier
     * @param optimScope période d'instanciation de la contrainte
     * @param ctrSat contrainte métier origine
     * @param satellite satellite associé à la contrainte
     * @return
     */
    OCP_CpSatelliteConstraint5(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType5Ptr ctrSat1,OCP_CpSatellitePtr satellite);



protected:

    /**
    * création de la contrainte ILOG SOLVER
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

    /**
    * creation d'une contrainte ILOG SOLVER sur une plage horaire
    * @param plageHoraire plage horaire demandée
    * @param ctrAggregate contrainte aggrégeant la contrainte créée sur cette plage horaire
    * @param cpt : compteur pour nommage de variable ensembliste
    */
    void createOneConstraint(OCP_CpTimeSlotPtr plageHoraire,IloAnd ctrAggregate,int cpt);

};



#endif /* OCP_CpSatelliteConstraint5_H_ */
