/*
 * $Id: OCP_CpStationConstraint4.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint4.h
 *
 */

#ifndef OCP_CpStationConstraint4_H_
#define OCP_CpStationConstraint4_H_

#include <string>

#include <constraints/OcpConstraintStaType4.h>

#include "constraints/OCP_CpStationBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;

/*
* OCP_CpStationConstraint4 : 
*	nb max de suivi, sur une periode de temps (la journee)
*
*/
class OCP_CpStationConstraint4: public OCP_CpStationBaseConstraint
{
private:
    /**
     * Nombre maximum de passages commençant dans la période de validité de la contrainte
     */
    uint16_t _maxPassages;

public:

    /**
       * Constructeur à partir de la contrainte métier
       * @param period période d'instanciation de la contrainte
       * @param ctrSat1 contrainte métier origine
       * @param station station associé à la contrainte
       * @return
       */
     OCP_CpStationConstraint4(OCP_CpTimeSlotPtr period, OcpConstraintStaType4Ptr ctrSat4,OCP_CpStationPtr station);

     /**
     * indique si la contrainte est de type STA4
     * @return vrai ssi la contrainte est de type STA4
     */
     virtual bool isSTA4() const {return true;}

protected:
     /**
     * création de la contrainte ILOG SOLVER
     * @param optimScope fenêtre de calcul courante
     */
     void instantiate(OCP_CpTimeSlotPtr optimScope);
};

#endif /* OCP_CpStationConstraint4_H_ */
