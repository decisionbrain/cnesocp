/*
 * $Id: OCP_IloCpSatelliteConstraint2Min.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2.cpp
 *
 */
#ifndef OCP_CpIloSatelliteConstraint2Min_H_
#define OCP_CpIloSatelliteConstraint2Min_H_


#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"
#include "constraints/OCP_CpSatelliteConstraint2.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

//class OCP_CpSatelliteConstraint2;

using namespace ocp::commons::io;

ILOSTLBEGIN

////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////               TRAITEMENT DU MODE ECART min INTER SATELLITES //////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
/* GLOBAL IDEA FOR THIS CONSTRAINT:
    at each adding operation from the tracking possible set, remove all other tracking slots in the interval
    begin - delta, end + delta
*/
class IlcSatelliteConstraint2I : public IlcConstraintI {
protected:
    /**
     * la variable ensembliste des slots candidats
     */
    IlcAnySetVar _tracking;

    /**
     * l'écart minimum entre deux slots requis
     */
    OCP_CpDate _delta;

public:

    /**
     * constructeur de l'implémentation IlcConstraintI
     * @param s solveur
     * @param d1 debut de validité
     * @param d2 fin de validité
     * @param trackingSlot liste des passages satellites concernés
     * @param delta écart min inter satellites
     * @return
     */
    IlcSatelliteConstraint2I(IloSolver s,  IlcAnySetVar trackingSlot, OCP_CpDate delta);

    ~IlcSatelliteConstraint2I() {}
    virtual void post();
    virtual void propagate();
    void propagateDeltaWithDemon();
};

#endif /* OCP_CpIloSatelliteConstraint2Min_H_ */
