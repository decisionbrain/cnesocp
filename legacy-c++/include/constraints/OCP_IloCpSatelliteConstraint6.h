/*
 * $Id: OCP_IloCpSatelliteConstraint6.h 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2.cpp
 *
 */
#ifndef OCP_CpIloSatelliteConstraint6_H_
#define OCP_CpIloSatelliteConstraint6_H_


#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"
#include "constraints/OCP_CpSatelliteConstraint2.h"


#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>


using namespace ocp::commons::io;

ILOSTLBEGIN

/** implémentation  PPC de la contraitne de recouvrement de durée minimale entre passages sur deux stations données
*/
class IlcSatelliteConstraint6I : public IlcConstraintI {
protected:

    /**
     * variable ensembliste concernant aux passages concernés par la contrainte
     */
    IlcAnySetVar _tracking;

    /**
     * première station concernée
     */
    OCP_CpStationPtr _station1;

    /**
     * seconde station concernée
     */
    OCP_CpStationPtr _station2;

    /**
     * Durée de recouvrement en secondes : tout passage sur station1 doit etre couvert par station2 avec au moins _dureeRecouv d'intersection
     */
    OCP_CpDate _dureeRecouv;

public:


    /**
     *
     * @param s solver
     * @param station1 premiere station
     * @param station2 seconde station
     * @param dureeRecouv duree en secondes de recouvrement minimal souhaité
     * @param trackingVar visibilités candidates
     * @return implémentation de la contrainte solver
     */
    IlcSatelliteConstraint6I(IloSolver s, OCP_CpStationPtr station1, OCP_CpStationPtr station2, OCP_CpDate dureeRecouv, IlcAnySetVar trackingVar)
        : IlcConstraintI(s),  _tracking(trackingVar), _station1(station1), _station2(station2), _dureeRecouv(dureeRecouv) {}

    virtual ~IlcSatelliteConstraint6I() {}

    /**
     * méthode nécessairement publique associée à un wrapper de démon de propagation
     */
    void propagateDureeRecouvWithDemon();

    /**
     * propagation d'un nouveau requis (ou existant)
     * @param requiredSlot : slot requis
     */
    void propagateRequiredSlot(OCP_CpTrackingSlot* requiredSlot);

protected:

    /**
     * pose des démons de propagation
     */
    virtual void post();

    /**
     * ancien propagate appelé une fois seulement avant le post
     */
    virtual void propagate();




};

#endif /* OCP_CpIloSatelliteConstraint6_H_ */
