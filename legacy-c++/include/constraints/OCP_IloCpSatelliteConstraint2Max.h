/*
 * $Id: OCP_IloCpSatelliteConstraint2Max.h 920 2010-11-04 18:11:31Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2.cpp
 *
 */
#ifndef OCP_CpIloSatelliteConstraint2Max_H_
#define OCP_CpIloSatelliteConstraint2Max_H_


#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"
#include "constraints/OCP_CpSatelliteConstraint2.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

//class OCP_CpSatelliteConstraint2;

using namespace ocp::commons::io;

ILOSTLBEGIN

/** implémentation  PPC de la contraitne d'écart maximum entre deux passages consécutifs d'un même satellite
*/
class IlcSatelliteConstraint2MaxI : public IlcConstraintI {
protected:

    /**
     * variable ensembliste concernant aux passages concernés par la contrainte
     */
    IlcAnySetVar _tracking;

    /**
     * écart maximal entre deux passages consécutifs (en secondes)
     */
    OCP_CpDate _delta;

    /**
     * flag réversible indiquant qu'on doit traiter la propagation pour la sélection du prochain créneau candidat
     */
    IlcRevBool _propagateSelection;

public:

    /**
     * constructeur de la contrainte PPC
     * @param s solver
     * @param validityBegin debut de validité de la contrainte
     * @param validityEnd fin de validité de la contrainte
     * @param trackingVar variable ensembliste concernant aux passages concernés par la contrainte
     * @param delta écart maximal entre deux passages consécutifs
     * @return
     */
    IlcSatelliteConstraint2MaxI(IloSolver s, IlcAnySetVar trackingVar, OCP_CpDate delta);

    virtual ~IlcSatelliteConstraint2MaxI() {}

    /*
     * note : les méthodes suivantes sont nécessairement publiques car associées à un wrapper de démon de propagation
     */

    /**
     * propagation sur les variables ensemblistes
     */
    void propagateDeltaMaxWithDemon();

protected:

    /**
     * propagation incrémentale sur les flags réversibles des candidats pour maintenir le prochain candidat
     * @param slotsByEnd slots triés par fin décroissantes
     */
    void propagateNextCandidateWithDemon(vector<OCP_CpTimeSlot*>& slotsByEnd);

    /**
     * propagation intiale sur les flags réversibles des candidats pour maintenir le prochain candidat
     * @param slotsByEnd slots triés par fin décroissantes
     */
    void propagateInitialCandidateWithDemon(vector<OCP_CpTimeSlot*>& slotsByEnd);


    /**
     * pose des démons de propagation
     */
    virtual void post();

    /**
     * ancien propagate appelé une fois seulement avant le post
     */
    virtual void propagate();



    /**
    * propagation sur les possibles précédants le requis courant
    * @param slotsByEnd liste des possibles triées par fin décroissantes
    * @param currentRequired le requis courant
    * @return le premier requis dont la fin est < au début du requis courant-max
    */
    OCP_CpTimeSlot* propagateBackward(std::vector<OCP_CpTimeSlot*>& slotsByEnd, OCP_CpTimeSlot* currentRequired);

    /**
    * propagation sur les possibles précédants le requis courant
    * @param slotsByBegin liste des possibles triées par débuts croissants
    * @param currentRequired le requis courant
    * @return le premier requis dont le début est  > fin du requis courant+max
    */
    OCP_CpTimeSlot* propagateForward(std::vector<OCP_CpTimeSlot*>& slotsByBegin, OCP_CpTimeSlot* currentRequired);



};

#endif /* OCP_CpIloSatelliteConstraint2Max_H_ */
