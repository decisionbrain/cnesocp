/*
 * $Id: OCP_CpSatelliteConstraint4.h 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint4.h
 *
 */

#ifndef OCP_CpSatelliteConstraint4_H_
#define OCP_CpSatelliteConstraint4_H_

#include <string>
#include <list>
#include <constraints/OcpConstraintSatType4.h>


#include "OCP_CpHomere.h"
#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"


#include <ilsolver/ilosolver.h>

typedef list<string> stationList;

using namespace std ;
using namespace ocp::commons::io;

/**
 * OCP_CpSatelliteConstraint4 :
 * ordre de priorité des stations pour un satellite donné.
Plusieurs stations peuvent avoir le même niveau de priorité.
On peut également préciser :
        les jours et créneaux d’utilisation possibles pour chaque station
        le site minimum utilisable pour la station
        la prise en compte des masques bord
        la prise en compte des masques station
        la durée minimale d’un trou de visibilité
        Il est de plus possible de préciser si les passages consécutifs sur une même station sont autorisés ou
        interdits.
Exemples :
        Priorité 1 pour les stations TTCET (SAUS01 et SKRN01, site minimum de 10° accessibles entre 0h et
                                                                                           )
        24h, priorité 2 pour les stations HBK, KRU et KER (site minimum de 10° accessibles entre 8h et 12h et
                                                                                     )
        14h et 18h
 *
 */
class OCP_CpSatelliteConstraint4: public OCP_CpSatelliteBaseConstraint
{
private:

	/**
	 * map des stations pouvant filtrer la contrainte
	 */
	StationMap _stations;

	/**
	 * Flag autorisant (true) ou interdisant (false) les passages consécutifs d'un satellite sur une même station
	 */
	bool _consecutive;

	/**
	 * map des propriétés d'une station pour la contrainte courante
	 */
	std::map<OcpStationPtr, OcpStationPropertiesPtr> _properties;


protected:

	/**
	* contrainte de non-consecutivité entre créneaux du satellite. Sur une boucle chronologique de visibilités
	* STA1->STA2->STA3->...->STA1->
	* on doit assurer que STA1 n'a pas pour suivant/précédent STA1
	* @param optimScope fenêtre de calcul courante
	*/
	void createNonConsecutiveConstraint(OCP_CpTimeSlotPtr optimScope);
	/**
	 * renseigner et afficher les propriétés de la contrainte en parcourant les stationProperties
	 * @param stationProperties : propriétés renseignées sur la contrainte
	 */
	void mapProperties(VectorOcpStationProperties& stationProperties);

	/**
	 * indique si un passage satellite est à prioritiser par cette contrainte, et éventuellement flaggué impossible
	 * @param optimScope fenêtre de calcul
	 * @param slot : le passage satellite
	 * @param stationProperty : les propriétés d'une station
	 * @param dayIndex : index du jour courant du calcul
	 * @return vrai ssi le slot est conservé pour le calcul
	 */
	bool processOneSlot(OCP_CpTimeSlotPtr optimScope,OCP_CpTrackingSlot* slot, OcpStationPropertiesPtr stationProperty,phr::int16u dayIndex) const;

	/**
	* création de la contrainte ILOG SOLVER
	* @param optimScope fenêtre de calcul courante
	*/
	void instantiate(OCP_CpTimeSlotPtr optimScope);

	/**
	 * regarde s'il faut étendre l'optimScope de la contrainte en fonction des périodes horaires
	 * @param optimScope fenêtre de calcul
	 * @param stationProperty : les propriétés d'une station
	 * @param dayIndex : index du jour courant du calcul. Si ce jour ne correspond pas à l'un des jours de la semaine pour lequel la station est disponible, rien à faire
	 */
	void searchForExtendedScope(OCP_CpTimeSlotPtr optimScope,OcpStationPropertiesPtr stationProperty,phr::int16u dayIndex);


public:


    /**
     * Constructeur à partir de la contrainte métier
     * @param optimScope fenêtre de calcul
     * @param ctrSat contrainte métier origine
     * @param satellite satellite associé à la contrainte
     * @return
     */
    OCP_CpSatelliteConstraint4(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType4Ptr ctrSat1,OCP_CpSatellitePtr satellite);

    /**
     * renseigner les propriétés d'un slot
     * @param optimScope fenêtre de calcul
     * @return vrai ssi le créneau candidat n'est pas à prendre en compte
     */
    bool notifyOneTrackingSlot(OCP_CpTimeSlotPtr optimScope, OCP_CpTrackingSlot* tmp);


    /**
    * indique si la contrainte est de type SAT8
    * @return vrai ssi la contrainte est de type SAT4
    */
    virtual bool isSAT4() const {return true;}

    /**
     * mémorise les stations de la veille correspondant au(x) dernier()s passages
     * @param optimScope fenêtre de calcul coutante
     * @return la map des stations interdites
     */
    StationMapPtr storeForbiddenStations(OCP_CpTimeSlotPtr optimScope);

    /**
     * accès au champs de consécutivité
     * @return vrai ssi le champs de consécutivité est à vrai
     */
    bool hasConsecutive() const {return _consecutive;}

};

typedef boost::shared_ptr < OCP_CpSatelliteConstraint4 >    OCP_CpSatelliteConstraint4Ptr;

#endif /* OCP_CpSatelliteConstraint4_H_ */
