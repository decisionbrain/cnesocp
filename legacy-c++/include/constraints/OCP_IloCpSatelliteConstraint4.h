/*
 * $Id: OCP_IloCpSatelliteConstraint4.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2.cpp
 *
 */
#ifndef OCP_CpIloSatelliteConstraint4_H_
#define OCP_CpIloSatelliteConstraint4_H_


#include "planif/OCP_CpTrackingSlot.h"
#include "planif/OCP_CpTimeSlot.h"
#include "resources/OCP_CpStation.h"
#include "resources/OCP_CpSatellite.h"

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>



using namespace ocp::commons::io;

ILOSTLBEGIN

/** implémentation  PPC de la contraitne d'écart maximum entre deux passages consécutifs d'un même satellite
*/
class IlcSatelliteConstraint4I : public IlcConstraintI {
protected:
    /**
     * debut de validité de la contrainte
     */
    OCP_CpDate _validityBegin;

    /**
     * fin de validité de la contrainte
     */
    OCP_CpDate _validityEnd;

    /**
     * variable ensembliste concernant aux passages concernés par la contrainte
     */
    IlcAnySetVar _tracking;

    /**
    * liste (en général un singleton) des stations interdites pour le premier créneau concerné par la contrainte
    * cette liste est renseignée si la contrainte impose la non-consécutivité et correspond aux stations du (ou des)
    * derniers passages la veille
    */
    StationMapPtr _forbiddenStations;



public:

    /**
     * constructeur de la contrainte PPC
     * @param s solver
     * @param trackingVar variable ensembliste concernant aux passages concernés par la contrainte
     * @param forbiddenStations stations interdites pour le premier créneau
     * @return
     */
    IlcSatelliteConstraint4I(IloSolver s, IlcAnySetVar trackingVar,StationMapPtr forbiddenStations);


    virtual ~IlcSatelliteConstraint4I() {}

    /**
     * méthode nécessairement publique associée à un wrapper de démon de propagation
     */
    void propagateNoConsecutive();

protected:

    /**
     * pose des démons de propagation
     */
    virtual void post();

    /**
     * ancien propagate appelé une fois seulement avant le post
     */
    virtual void propagate();



    /**
    * propagation sur les possibles précédants le requis courant
    * @param slotsByEnd liste des possibles triées par fin décroissantes
    * @param currentRequired le requis courant
    * @return le premier requis dont la fin est < au début du requis courant-max
    */
    OCP_CpTrackingSlot* propagateBackward(std::vector<OCP_CpTrackingSlot*>& slotsByEnd, OCP_CpTrackingSlot* currentRequired);

    /**
    * propagation sur les possibles précédants le requis courant
    * @param slotsByBegin liste des possibles triées par débuts croissants
    * @param currentRequired le requis courant
    * @return le premier requis dont le début est  > fin du requis courant+max
    */
    OCP_CpTrackingSlot* propagateForward(std::vector<OCP_CpTrackingSlot*>& slotsByBegin, OCP_CpTrackingSlot* currentRequired);


    /**
     * propagation des stations interdites pour le premier créneau
     * @param firstWrongRequired : le premier requis , couramment sur une station interdite
     */
    void propagateForbiddenStations(OCP_CpTrackingSlot* firstWrongRequired);

    /**
     * le slot a-t-il une station "interdite en début de journée"
     * @param slot slot candidat
     * @return vrai ssi la station du slot est interdite en début de journée
     */
    bool isStationForbidden(OCP_CpTrackingSlot* slot) const;

    /**
     * en cas de stations interdites, renvoie le premier requis lorsque celui-ci a une station interdite
     * @return le premier requis lorsque celui-ci a une station interdite
     */
    OCP_CpTrackingSlot* getFirstWrongRequired() const;


    /**
    *  pour debug affichage courant de l'état possible/requis des slots de la contrainte
    * @param newRequired : nouveau requis
    * @param slotsByBegin : les possibles triés par début croissant
    */
    void displaySlotStates(OCP_CpTrackingSlot* newRequired,vector<OCP_CpTrackingSlot*>& slotsByBegin);

    /**
     * pour debug affichage d'une des contraintes locales de cardinalité >=1
     * @param forward : origine de la propagation (forward ou backward)
     * @param currentRequired : déclencheur requis de la contrainte
     * @param set1 : l'ensemble sur lequel on pose la contrainte
     */
    void displayLocalCardConstraint(bool forward,OCP_CpTrackingSlot* currentRequired, std::vector<OCP_CpTrackingSlot*>& set1);


};

#endif /* OCP_CpIloSatelliteConstraint4_H_ */
