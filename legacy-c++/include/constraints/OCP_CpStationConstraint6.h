/*
 * $Id: OCP_CpStationConstraint6.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint5.h
 *
 */

#ifndef OCP_CpStationConstraint6_H_
#define OCP_CpStationConstraint6_H_

#include <string>

#include <constraints/OcpConstraintStaType6.h>

#include "constraints/OCP_CpStationBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;

/*
* OCP_CpStationConstraint6 : duree de maintenance cumulee maximale sur une periode de temps (la journee)
*
*/
class OCP_CpStationConstraint6: public OCP_CpStationBaseConstraint
{
private:
    /**
     * les priorités à renseigner pour chacun des satellites de cette station
     */
    VectorOcpSatelliteProperties _satelliteProperties;

public:

    /**
       * Constructeur à partir de la contrainte métier
       * @param period période d'instanciation de la contrainte
       * @param ctrSat1 contrainte métier origine
       * @param station station associé à la contrainte
       * @return
       */
     OCP_CpStationConstraint6(OCP_CpTimeSlotPtr period, OcpConstraintStaType6Ptr ctrSat6,OCP_CpStationPtr station);

     /**
     * indique si la contrainte est de type STA6
     * @return vrai ssi la contrainte est de type STA6
     */
     bool isSTA6() const {return true;}

     /**
      * indique si le satellite apparait dans la liste des propriétés de la station
      * @param aSat : satellite à examiner
      * @return vrai ssi le satellite apparait dans la liste des propriétés de la station
      */
     bool concerns(OcpSatellitePtr aSat ) const;

protected:
     /**
     * création de la contrainte ILOG SOLVER
     * @param optimScope fenêtre de calcul courante
     */
     void instantiate(OCP_CpTimeSlotPtr optimScope);

};

typedef boost::shared_ptr < OCP_CpStationConstraint6 >    OCP_CpStationConstraint6Ptr;

#endif /* OCP_CpStationConstraint5_H_ */
