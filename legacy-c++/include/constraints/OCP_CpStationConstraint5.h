/*
 * $Id: OCP_CpStationConstraint5.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint5.h
 *
 */

#ifndef OCP_CpStationConstraint5_H_
#define OCP_CpStationConstraint5_H_

#include <string>

#include <constraints/OcpConstraintStaType5.h>

#include "constraints/OCP_CpStationBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;

/*
* OCP_CpStationConstraint5 : duree de tracking cumulee maximale sur une periode de temps (la journee)
*
*/
class OCP_CpStationConstraint5: public OCP_CpStationBaseConstraint
{
private:
    /**
     * Durée maximale de visibilité cumulée pour une journée (en secondes)
     */
    OCP_CpDate _maxVisiDuration;

public:

    /**
       * Constructeur à partir de la contrainte métier
       * @param period période d'instanciation de la contrainte
       * @param ctrSat1 contrainte métier origine
       * @param station station associé à la contrainte
       * @return
       */
     OCP_CpStationConstraint5(OCP_CpTimeSlotPtr period, OcpConstraintStaType5Ptr ctrSat5,OCP_CpStationPtr station);

protected:
     /**
     * création de la contrainte ILOG SOLVER
     * @param optimScope fenêtre de calcul courante
     */
     void instantiate(OCP_CpTimeSlotPtr optimScope);

};


#endif /* OCP_CpStationConstraint5_H_ */
