/*
 * $Id: OCP_CpStableMaxConstraint.h 928 2010-11-30 16:34:23Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStableMaxConstraint.h
 *
 */

#ifndef OCP_CpStableMaxConstraint_H
#define OCP_CpStableMaxConstraint_H

#include <list>

#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>


#include <planif/OCP_CpTimeSlot.h>
#include <planif/OCP_CpTrackingSlot.h>


/*
 *
 */


class IlcStableMaxConstraintI : public IlcConstraintI
{
protected:

    /**
     * variable ensembliste initiale , union des domaines des contraintes SAT1
     */
    IlcAnySetVar _domain;

    /**
     * un représentant de stable maximum sur le graphe des conflits
     */
    IlcAnySetVar _indep;

    /**
     * borne inférieure de la cardinalité d'un stable maximum
     */
    int _minCardinality;

public:

    /**
     * constructeur de l'IlcConstraint
     * @param s : instance de l'IloSolver ILOG
     * @param domain : union des domaines des différentes SAT1 incriminées
     * @param minCardinality : borne inférieure de la cardinalité d'un stable maximum
     * @return l' instance de contrainte IlcConstraintI
     */
    IlcStableMaxConstraintI(IloSolver s, IlcAnySetVar domain, int minCardinality);

    ~IlcStableMaxConstraintI() {}

    virtual void post();

    virtual void propagate();

    /**
     * propagation incrémentale
     */
    void propagateRemoveFromStable();

protected:

    /**
     * calcul initial ou recalcul incrémental d'un stable maximum
     * @return : renvoie la cardinalité d'un stable maximum
     */
    int computeStableMax();

    /**
     * indique que deux slots s'intersectent sur la même station
     * @param s1 : slot1
     * @param s2 : slot 2
     * @return vrai ssi les deux slots s'intersectent sur la mêmem station
     */
    bool conflict(OCP_CpTrackingSlot* s1, OCP_CpTrackingSlot* s2);

};


#endif /* OCP_CpStableMaxConstraint_H */
