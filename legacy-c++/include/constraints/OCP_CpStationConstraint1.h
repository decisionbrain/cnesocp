/*
 * $Id: OCP_CpStationConstraint1.h 843 2010-08-31 15:01:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint1.h
 *
 */

#ifndef OCP_CpStationConstraint1_H_
#define OCP_CpStationConstraint1_H_

#include <string>

#include "constraints/OCP_CpStationBaseConstraint.h"
#include <constraints/OcpConstraintStaType1.h>
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;

/*
* OCP_CpStationConstraint1 : duree de maintenance cumulee minimale sur une periode de temps
*
*/
class OCP_CpStationConstraint1: public OCP_CpStationBaseConstraint
{
private:
    /**
     * borne min de cumul de maintenance sur la semaine
     */
	OCP_CpDate _cumulMin;

    /**
     * borne min de cumul de maintenance sur la semaine
     */
    OCP_CpDate _cumulMax;

	/**
	 * écart minimal entre deux slots consécutifs de maintenance
	 */
	OCP_CpDate _interSlotMinDuration;

    /**
     * type des slots : lu depuis la contrainte origine et à positionner sur les slots référencés par cette contrainte
     */
    std::string _slotType;


public:


    /**
      * Constructeur à partir de la contrainte métier
      * @param optimScope période d'instanciation de la contrainte
      * @param ctrSta1 contrainte métier origine
      * @param station station associé à la contrainte
      * @return
      */
    OCP_CpStationConstraint1(OCP_CpTimeSlotPtr optimScope, OcpConstraintStaType1Ptr ctrSta1,OCP_CpStationPtr station);

    /**
    * indique si la contrainte est de type STA1
    * @return vrai ssi la contrainte est de type STA1
    */
    bool isSTA1() const {return true;}

    /**
     * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
     * @param slot le créneau à sauver
     */
    void postOptim(OCP_CpTimeSlot* slot);

    /**
     * tient compte des propriétés de durée min et max sur la fenêtre courante, ainsi que de l'écart interslot, pour positionner
     * des points de temps additionnels permettant la création de slots de maintenance candidats pour la contrainte
     */
    void updateStationMaintenanceTimePoints();


protected:

    /**
    * création de la contrainte ILOG SOLVER. La partie interSlotMinDuration est posée sur chaque fenêtre de calcul tandis
    * que la partie durée cumulée ne peut être posée que le dimanche
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

    /**
     * instanciation finale de la contrainte avec reconnaissance des booking order
     * @param optimScope : fenêtre de calcul courante
     * @param minTarget : cible min de durée de maintenance
     * @param maxTarget : cible max de durée de maintenance
     */
    void instantiateWithFilter(OCP_CpTimeSlotPtr optimScope, int minTarget, int maxTarget);


    /**
     * cumul des maintenances passés sur les 6 premiers jours de la semaine
     * @param optimScope fenêtre de calcul courante
     * @return la durée cumulée de maintenance
     */
    int computeCumulDone(OCP_CpTimeSlotPtr optimScope);


};

typedef boost::shared_ptr < OCP_CpStationConstraint1 >    OCP_CpStationConstraint1Ptr;

#endif /* OCP_CpStationConstraint1_H_ */
