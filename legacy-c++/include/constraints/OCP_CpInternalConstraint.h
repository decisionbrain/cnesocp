/*
 * $Id: OCP_CpInternalConstraint.h 926 2010-11-12 14:56:15Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpInternalConstraint.h
 *
 */

#ifndef OCP_CpInternalConstraint_H_
#define OCP_CpInternalConstraint_H_

#include "planif/OCP_CpTimeSlot.h"

using namespace std ;

extern IloConstraint IloLinkSlotConstraint(IloEnv, IloAnySetVar, const char* name=0);

extern IloConstraint IloNonUbiquityConstraint(IloEnv, IloAnySetVar, IloAnySetVar, const char* name=0);

extern IloConstraint IloSumSlotConstraint(IloEnv, IloAnySetVar, IloIntVar, OCP_CpTimeSlotPtr,const char* name=0);

extern IloConstraint IloLinkGlobalSlotConstraint(IloEnv, IloAnySetVar, const char* name=0);

extern IloConstraint IloObjectiveOverSlotsConstraint(IloEnv, IloAnySetVar, IloIntVar, const char* name=0);

extern IloConstraint IloStableMaxConstraint(IloEnv, IloAnySetVar, int,const char* name=0);

#endif /* OCP_CpInternalConstraint_H_ */
