/*
 * $Id: OCP_IloCpSatelliteConstraint4.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2.cpp
 *
 */
#ifndef OCP_CpIloSatelliteConstraint3_H_
#define OCP_CpIloSatelliteConstraint3_H_



#include "planif/OCP_Solver.h"
#include "planif/OCP_CpTrackingSlot.h"


#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>

ILOSTLBEGIN

using namespace ocp::commons;

/* GLOBAL IDEA FOR THIS CONSTRAINT:
    at each adding operation from the possible set, modify theremove all other tracking slots in the interval
    begin - delta, end + delta
*/
class IlcSatelliteConstraint3I : public IlcConstraintI
{
protected:
    OCP_CpDate _dMin;//donnees utilisateur : la duree min requise
    OCP_CpDate _dMax;//donnees utilisateur : la duree max requise
    OCP_CpDate _max;//majorant = somme des durees de tous les slots candidats
    IlcAnySetVar _tracking;//passages satellites ou maintenance
    IlcIntVar _cumulMin;
    IlcIntVar _cumulMax;

    /**
     * map des durées spécifiques à cette contrainte par slot candidat
     */
    SpecialDuration _specialDurations;

public:
    IlcSatelliteConstraint3I(IloSolver s, OCP_CpDate dMin, OCP_CpDate dMax, IlcAnySetVar x, OCP_CpDate max,SpecialDurationPtr specialDurations);

    /**
     * mesure la distance à la borne min de la contrainte, divisée par l'amplitude max-min courante
     * plus ce ratio est grand, moins il y a de liberté pour satisfaire cette contrainte
     * @return
     */
    IlcFloat getRelativeSlack();

    /**
     * pour debug, affichage des candidats possibles/requis pour une instance courante qui n'a pas encore atteint sa borne min
     */
    void displayCandidates();

    ~IlcSatelliteConstraint3I() {}
    virtual void post();
    virtual void propagate();

    /**
     * méthode invoquée par le démon de propagation
     */
    void propagateCumulWithDemon();
};

#endif /* OCP_CpIloSatelliteConstraint3_H_ */
