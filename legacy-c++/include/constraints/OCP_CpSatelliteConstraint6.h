/*
 * $Id: OCP_CpSatelliteConstraint6.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint6.h
 *
 */

#ifndef OCP_CpSatelliteConstraint6_H_
#define OCP_CpSatelliteConstraint6_H_

#include <string>
#include <constraints/OcpConstraintSatType6.h>


#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;
using namespace ocp::commons::io;
/*
* OCP_CpSatelliteConstraint6 : 
*
*/
class OCP_CpSatelliteConstraint6: public OCP_CpSatelliteBaseConstraint
{
private:
    /**
     * première station concernée
     */
    OCP_CpStationPtr _station1;

    /**
     * seconde station concernée
     */
    OCP_CpStationPtr _station2;

    /**
     * Filtre sur les passages satellites par la fréquence de la station associée
     */
    FrequencyBands::FrequencyBands_ENUM _frequencyBand;

    /**
     * Durée de recouvrement en secondes : tout passage sur station1 doit etre couvert par station2 avec au moins _dureeRecouv d'intersection
     */
    OCP_CpDate _dureeRecouv;


public:

    /**
     * Constructeur à partir de la contrainte métier
     * @param period période d'instanciation de la contrainte
     * @param ctrSat contrainte métier origine
     * @param satellite satellite associé à la contrainte
     * @return
     */
    OCP_CpSatelliteConstraint6(OCP_CpTimeSlotPtr period, OcpConstraintSatType6Ptr ctrSat1,OCP_CpSatellitePtr satellite);



protected:

    /**
    * création de la contrainte ILOG SOLVER
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

};

#endif /* OCP_CpSatelliteConstraint6_H_ */
