/*
 * $Id: OCP_CpSatelliteConstraint3.h 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint3.h
 *
 */

#ifndef OCP_CpSatelliteConstraint3_H_
#define OCP_CpSatelliteConstraint3_H_

#include <string>
#include <list>
#include <constraints/OcpConstraintSatType3.h>


#include "OCP_CpHomere.h"
#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"


#include <ilsolver/ilosolver.h>

typedef list<string> stationList;

using namespace std ;
using namespace ocp::commons::io;

/*
* OCP_CpSatelliteConstraint3 : 
*	cumul de suivi, avec borne min et Max, sur certaines stations, sur une periode de temps
*
*/
class OCP_CpSatelliteConstraint3: public OCP_CpSatelliteBaseConstraint
{
private:
    /**
     * borne min en cas de contrainte type MIN, 0 sinon
     */
	OCP_CpDate _durationMin;

	/**
	 * borne max en cas de contrainte type MAX, un majorant du cumul des durées sinon
	 */
	OCP_CpDate _durationMax;

	/**
	* map des stations concernées par la contrainte
	*/
	StationMap _stationsMap;

	/**
	* Filtre sur les passages satellites par la fréquence de la station associée
	*/
	FrequencyBands::FrequencyBands_ENUM _frequencyBand;

	/*
	 *
	 * Type de durée (min ou max)
	 */
	SatConstraintTypes::SatConstraintTypes_ENUM _typeEcart;


	/**
	 * duree cumulee de passages ou créneaux de supports en secondes
	 */
	uint32_t _dureeCumulee;

	/**
	 * liste des plages horaires : la contrainte s'instancie par tranche horaire
	 */
	VectorOCP_CpTimeSlot _plagesHoraires;



public:


    /**
     * Constructeur à partir de la contrainte métier
     * @param optimScope période d'instanciation de la contrainte
     * @param ctrSat contrainte métier origine
     * @param satellite satellite associé à la contrainte
     * @return
     */
    OCP_CpSatelliteConstraint3(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType3Ptr ctrSat1,OCP_CpSatellitePtr satellite);



protected:

    /**
    * création de la contrainte ILOG SOLVER
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

    /**
     * création d'une instance sur "la" plage horaire de la contrainte
     * @param plageHoraire la plage horaire, potentiellement à cheval sur la fenêtre de calcul
     * @param ctrAggregate contrainte aggrégeant les instances sur deux plages horaires à cheval
     */
    void createOneConstraint(OCP_CpTimeSlotPtr plageHoraire, IloAnd ctrAggregate) ;
};



#endif /* OCP_CpSatelliteConstraint3_H_ */
