/*
 * $Id: OCP_CpStationConstraint2.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpStationConstraint2.h
 *
 */

#ifndef OCP_CpStationConstraint2_H_
#define OCP_CpStationConstraint2_H_

#include <string>

#include <constraints/OcpConstraintStaType2.h>

#include "constraints/OCP_CpStationBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;

/*
* OCP_CpStationConstraint2 : 
*	creneau de maintenance de duree min, sur une periode de temps
*
*/
class OCP_CpStationConstraint2: public OCP_CpStationBaseConstraint
{
private:
    /**
     * filtre sur la durée minimale des slots de maintenance
     */
	OCP_CpDate _durationMin;

	/**
	 * lorsque ce flag est à true, on filtre sur les maintenance pour garder uniquement celles hors-visibilité satellites
	 */
	bool _offVisibility;

	/**
	 * plage où l'on demande la maintenance (cette plage ne peut pas chevaucher 24/24)
	 */
	OCP_CpTimeSlotPtr _maintenanceScope;

    /**
     * type des slots : lu depuis la contrainte origine et à positionner sur les slots référencés par cette contrainte
     */
    std::string _slotType;


public:

    /**
    * Constructeur à partir de la contrainte métier
    * @param period période d'instanciation de la contrainte
    * @param ctrSta2 contrainte métier origine
    * @param station station associé à la contrainte
    * @return
    */
    OCP_CpStationConstraint2(OCP_CpTimeSlotPtr period, OcpConstraintStaType2Ptr ctrSta2,OCP_CpStationPtr station);

    /**
    * indique si la contrainte est de type STA2
    * @return vrai ssi la contrainte est de type STA2
    */
    bool isSTA2() const {return true;}

    /**
     * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
     * @param slot le créneau à sauver
     */
    void postOptim(OCP_CpTimeSlot* slot);

protected:
    /**
    * création de la contrainte ILOG SOLVER
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

    /**
     * prendre en compte la durée min et les plages horaires de la contrainte pour la mise à jour des créations de slots de maintenance
     */
    void updateStationMaintenanceTimePoints();

};

#endif /* OCP_CpStationConstraint2_H_ */
