/*
 * $Id: OCP_IloCpSatelliteConstraint7_v4.h 841 2010-08-30 16:39:27Z pcauquil $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_IloCpSatelliteConstraint2.cpp
 *
 */
#ifndef OCP_CpIloSatelliteConstraint7_H_4
#define OCP_CpIloSatelliteConstraint7_H_4


#include <ilsolver/ilosolver.h>
#include <ilsolver/ilosolverset.h>
#include "planif/OCP_CpTimeSlot.h"
#include "planif/OCP_CpTimeSlotTimetable.h"

class OCP_CpTimeSlotTimetable;

class IlcSatelliteConstraint7I_v4 : public IlcConstraintI
{
protected:
    /**
     * capacité max autorisée de la timetable (on autorise jusqu'à ce nombre simultané d'intervalles,mais pas au dessus)
     */
    int _nbSupports;

    /**
     * variables ensemblistes associés aux slots candidats
     */
    IlcAnySetVar _tracking;

    /**
     * plage horaire où l'on maintient la contrainte (l'intégrale doit etre respectée sur cette plage, pas autour)
     */
    OCP_CpTimeSlotPtr _plageHoraire;

    bool _withMin;//TODO remove (test only)

    bool _withMax;//TODO remove (test only)

    /**
     * timetable des possibles et requis
     */
    OCP_CpTimeSlotTimetable* _timetable;

public:
    /**
     * constructeur de l'implémentation de la contrainte spécifique ILOG
     * @param solver : solver IloSolver
     * @param nbSupports : nombre de supports requis
     * @param trackingSet : variable ensembliste des créneaux candidats
     * @param plageHoraire : intervalle où la contrainte d'égalité au nombre de supports doit être posé
     * @return
     */
    IlcSatelliteConstraint7I_v4(IloSolver solver, int nbSupports, IlcAnySetVar trackingSet,OCP_CpTimeSlotPtr plageHoraire,bool withMin,bool withMax);

    ~IlcSatelliteConstraint7I_v4() {}
    virtual void post();
    virtual void propagate();

    /**
     * lorsqu'on retire un possible, on décrémente la timetable des possibles d'une unité sur l'intervalle intersectant la plage horaire
     * lorsqu'on ajoute un requis, on incrémente la timetable des requis d'une unité sur l'intervalle intersectant la plage horaire
    */
    void propagateTimeTableWithDemon();


};



#endif /* OCP_CpIloSatelliteConstraint7_H_4 */
