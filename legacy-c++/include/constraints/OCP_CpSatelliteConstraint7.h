/*
 * $Id: OCP_CpSatelliteConstraint7.h 896 2010-10-14 13:40:18Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */

/*
 * OCP_CpSatelliteConstraint7.h
 *
 */

#ifndef OCP_CpSatelliteConstraint7_H_
#define OCP_CpSatelliteConstraint7_H_

#include <string>
#include <constraints/OcpConstraintSatType7.h>


#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"

#include <ilsolver/ilosolver.h>

using namespace std ;
using namespace ocp::commons::io;
/*
* OCP_CpSatelliteConstraint7 : 
* Après discussion avec Emmanuelle et Josian, il apparait qu’il y a une petite erreur dans la CSatType7 :
* dans notre modèle, il manque le paramètre NB_SUPPORT.
Ce champ est rajouté en ce moment même par Emmanuelle : PHR-DON évoluera donc pour assurer la lecture de ce paramètre.
*
*/
class OCP_CpSatelliteConstraint7: public OCP_CpSatelliteBaseConstraint
{
private:
    /**
     * Filtre sur les passages satellites par la fréquence de la station associée
     */
    FrequencyBands::FrequencyBands_ENUM _frequencyBand;

    /**
     * plage horaire, dupliquée en cas de chevauchement 24/24
     */
    VectorOCP_CpTimeSlot _plagesHoraires;

    /**
     * nombre minimal de supports requis sur chacune des plages horaires
     */
    int _nbSupports;


public:

    /**
    * Constructeur à partir de la contrainte métier
    * @param  période d'instanciation de la contrainte
    * @param ctrSat contrainte métier origine
    * @param satellite satellite associé à la contrainte
    * @return
    */
    OCP_CpSatelliteConstraint7(OCP_CpTimeSlotPtr , OcpConstraintSatType7Ptr ctrSat1,OCP_CpSatellitePtr satellite);


    /**
    * indique si la contrainte est de type SAT7
    * @return vrai ssi la contrainte est de type SAT7
    */
    virtual bool isSAT7() const {return true;}


protected:

    /**
     * creation d'une contrainte ILOG SOLVER sur une plage horaire
     * @param plageHoraire plage horaire demandée
    * @param ctrAggregate contrainte aggrégeant la contrainte créée sur cette plage horaire
    * @param cpt compteur pour nommage de variable ensembliste
    */
    void createOneConstraint(OCP_CpTimeSlotPtr plageHoraire, IloAnd ctrAggregate, int cpt);

    /**
    * création de la contrainte ILOG SOLVER
    * @param optimScope fenêtre de calcul courante
    */
    void instantiate(OCP_CpTimeSlotPtr optimScope);

    /**
     * split de la zone de support
     */
    void addSplitZone();




};

#endif /* OCP_CpSatelliteConstraint7_H_ */
