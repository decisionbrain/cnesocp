/*
 * $Id: OCP_CpSatelliteConstraint8.h 913 2010-10-29 14:26:41Z x-dgravo $
 *
 * ===================================================
 * Projet : OCP HOMERE
 * Produit par Capgemini.
 * ===================================================
 * HISTORIQUE
 * VERSION : 1.0 : Creation
 * VERSION : 1-0-0 : FA : artf638546 : 18/06/2010 : Maj des cartouches des sources (https://coconet2.capgemini.com/sf/go/artf638546/)
 * VERSION : 1-0-0 : FA : artf651066 : 30/08/2010 : Mise � jour du cartouche et ddc pour V1-0-0 (https://coconet2.capgemini.com/sf/go/artf651066/)
 * 
 * FIN-HISTORIQUE
 * ===================================================
 */
#ifndef OCP_CpSatelliteConstraint8_H_
#define OCP_CpSatelliteConstraint8_H_

#include <string>
#include <list>

#include "OCP_CpHomere.h"
#include <constraints/OcpConstraintSatType8.h>
#include "constraints/OCP_CpSatelliteBaseConstraint.h"
#include "resources/OCP_CpStation.h"
#include "OCP_CpDate.h"


#include <ilsolver/ilosolver.h>

using namespace std ;
using namespace ocp::commons::io;
/*
* OCP_CpSatelliteConstraint8 : 
*
*/
class OCP_CpSatelliteConstraint8: public OCP_CpSatelliteBaseConstraint
{
private:
    /**
     * Filtre sur les passages satellites par la fréquence de la station associée
     */
    FrequencyBands::FrequencyBands_ENUM _frequencyBand;

    /**
     * filtre sur la durée min des passages concernés
     */
	OCP_CpDate _durationMin;

    /**
     * filtre sur la durée max des passages concernés
     */
	OCP_CpDate _durationMax;

	/**
	 * durée maximale du support "continu" demandé
	 */
	OCP_CpDate _maxCumulDuration;

	/*
	 * tolérance : on peut assurer un suivi continu d'au moins _maxCumulDuration - _tolerance
	 */
	OCP_CpDate _tolerance;

	/**
	 * écart maximum entre deux slots candidats
	 */
	OCP_CpDate _interSlotMaxDuration;

	/**
	 * portée du support continu
	 */
	OCP_CpTimeSlotPtr _support;

	/**
	* map des stations concernées par la contrainte
	*/
	StationMap _stationsMap;

	/**
	 * indique si la fenêtre de calcul est le dernier jour du support de suivi continu
	 */
	bool _lastDay;

    /**
     * type des slots : lu depuis la contrainte origine et à positionner sur les slots référencés par cette contrainte
     */
    std::string _slotType;


    /**
     * indique le status du NIVEAU_CRENEAU : les slots reservés par cette contrainte seront RESERVED (si _editable à TRUE)
     * ou RESERVED_LOCKED (si _editable à FALSE)
     */
    bool _editable;

public:

	/**
	* Constructeur à partir de la contrainte métier
	* @param optimScope période d'instanciation de la contrainte
	* @param ctrSat contrainte métier origine
	* @param satellite satellite associé à la contrainte
	* @return
	*/
	OCP_CpSatelliteConstraint8(OCP_CpTimeSlotPtr optimScope, OcpConstraintSatType8Ptr ctrSat1,OCP_CpSatellitePtr);


    /**
    * indique si la contrainte est de type SAT8
    * @return vrai ssi la contrainte est de type SAT8
    */
    virtual bool isSAT8() const {return true;}

    /**
     * notifier à la sauvegarde du créneau les informations post-optim sur un créneau
     * @param slot le créneau à sauver
     */
    void postOptim(OCP_CpTimeSlot* slot);

    /**
     * accès à la liste des stations concernées par SAT8
     * @return la liste des stations concernées par SAT8
     */
    StationMap getStations() {return _stationsMap;}

    /**
     * accès au support
     * @return le support
     */
    OCP_CpTimeSlotPtr  getSupport() const {return _support;}

    /**
     * accès au filtre de durée minimale pour les créneaux concernés par la contrainte
     * @return la durée minimale pour les créneaux concernés par la contrainte
     */
    OCP_CpDate getDurationMin() const {return _durationMin;}

    /**
     * accès au filtre de durée maximale pour les créneaux concernés par la contrainte
     * @return la durée maximale pour les créneaux concernés par la contrainte
     */
    OCP_CpDate getDurationMax() const {return _durationMax;}


protected:
	/**
	* création de la contrainte ILOG SOLVER
	* @param optimScope fenêtre de calcul courante
	*/
	void instantiate(OCP_CpTimeSlotPtr optimScope);

	/**
	 * calcule en dehors des slots variables (ceux de la fenêtre de calcul), la durée des slots candidats sur la partie
	 * effectuée les jours avant le dernier jour
	 * @param optimScope fenêtre de calcul courante
	 * @return la durée en secondes de la visibilité "continue" avant le jour de la fenêtre de calcul quand celui-ci est le dernier jour
	 */
	OCP_CpDate computePastDuration(OCP_CpTimeSlotPtr optimScope);

    /**
     * split de la zone de support
     * @param splitArea : partie du support dans la fenêtre de calcul
     */
    void addSplitZone(const OCP_CpTimeSlot& splitArea);

};

typedef boost::shared_ptr < OCP_CpSatelliteConstraint8 >    OCP_CpSatelliteConstraint8Ptr;

#endif /* OCP_CpSatelliteConstraint8_H_ */
