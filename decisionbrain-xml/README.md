# Decisionbrain-xml

Option[JavaType] is supported

Option[ScalaType] not really

e.g. not Option[Int] but Option[Integer]

     Option[String] works though

or you need to implement a converter to use Option[Int]

e.g. testOptionAttribute in ParserTest


Option[\_] is ok

Seq[\_] is ok

Seq[Option[\_]] and Option[Seq[\_]] not really (untested)
