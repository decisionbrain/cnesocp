package com.decisionbrain.xml.annotation;

public interface ValueConverter<T> {
    T fromString(String xmlValue);
}
