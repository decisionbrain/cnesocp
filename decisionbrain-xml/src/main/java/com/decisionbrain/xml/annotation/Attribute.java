package com.decisionbrain.xml.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Attribute {
    /**
     * Name of the attribute. If name="", defaults to the text of the xml node
     */
    String path() default "";

    String namespace() default "";
}
