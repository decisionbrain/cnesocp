package com.decisionbrain.xml

import scala.xml._
import scala.xml.transform.{RewriteRule, RuleTransformer}

object Updater {


  /**
    * This functions update a XML object. It looks for markups 'label', and if its attribute selector has a
    * value in selectorValueToAttr, then its attribute are either created or replaced with values from the map.
    */
  def update(xml: Node, label: String, selector: String, selectorValueToAttr: Map[String, Seq[(String, String)]]): Seq[Node] = {

    val rule = new RewriteRule {
      override def transform(ns: Node): Seq[Node] = ns.map{
        case e: Elem if e.label == label =>
          selectorValueToAttr.get(e.attributes(selector).text) match {
            case None =>                        e
            case Some(tupleSeq) =>
              tupleSeq.foldLeft(e){ case (acc, (attrName, attrValue)) => acc % Attribute(None, attrName, Text(attrValue), Null) }
          }
        case other => other
      }
    }

    val newXml = new RuleTransformer(rule).transform(xml)
    newXml
  }


  /**
    * This function updates the `rootLabel` node of the xml object. It appends `label` nodes to it, one per element
    * in the Iterable. Each node has attributes as in the Map `key=value`
    */
  def append(xml: Node, rootLabel: String, label: String, attributes: Iterable[Map[String, String]]): Seq[Node] = {

    val newChildren = attributes.map{ attrs =>
      val xmlAttrs = attrs.foldLeft(Null: MetaData){ case (acc, (k, v)) =>  new UnprefixedAttribute(k, v, acc) }
      new Elem(null, label, xmlAttrs, TopScope, true)
    }

    val rule = new RewriteRule {
      override def transform(ns: Node): Seq[Node] = ns.map{
        case Elem(prefix, elabel, attribs, scope, child @ _*) if elabel == rootLabel =>
          Elem(prefix, elabel, attribs, scope, true, child ++ newChildren: _*)
        case other => other
      }
    }
    rule transform xml
  }

  /**
    * This function updates the `rootLabel` node of the xml object. It appends `label` nodes to it, one per element
    * in the Iterable.
    * Each node has attributes as in the Map `key=value` where value is a String.
    * If value is a Map[String, Object] then a child node is created recursively
    */
  def appendRecursive(xml: Node, rootLabel: String, label: String, attributes: Iterable[Map[String, Object]]): Seq[Node] = {
    // Function to build attributes of current node
    val buildAttributes: Map[String, Object] => MetaData = (attributeMap: Map[String, Object]) =>
      attributeMap.foldLeft(Null: MetaData){
        case (acc, (k, v: String))  =>  new UnprefixedAttribute(k, v, acc)
        case (acc, _)               =>  acc
      }

    // Recursive function to build node and sub-nodes
    def buildChildren(attributeMap: Map[String, Object]): Seq[Node] =
      attributeMap.flatMap{
        case (_, v: String) =>
          Nil
        case (k, v: Map[String, Object]) =>
          new Elem(null, k, buildAttributes(v), TopScope, true, buildChildren(v): _*)  ++ Text("\n")
        case _ =>
          throw new NullPointerException("Parameter attributes is not correctly defined !")
      }.toSeq

    val newChildren = attributes.map{ attrs =>
      new Elem(null, label, buildAttributes(attrs), TopScope, true, buildChildren(attrs): _*)
    }

    val rule = new RewriteRule {
      override def transform(ns: Node): Seq[Node] = ns.map{
        case Elem(prefix, elabel, attribs, scope, child @ _*) if elabel == rootLabel =>
          Elem(prefix, elabel, attribs, scope, false, child ++ newChildren: _*)
        case other => other
      }
    }
    rule transform xml
  }



}
