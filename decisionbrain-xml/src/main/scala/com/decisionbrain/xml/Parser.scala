package com.decisionbrain.xml

import java.lang.reflect.{Constructor, Parameter, ParameterizedType}

import com.decisionbrain.xml.Utils._
import com.decisionbrain.xml.annotation.{Attribute, Converter, Element}

import scala.xml.{Elem, Node}
import scala.collection.{breakOut, mutable}

class Parser {

  private val constructorToParameterAnnotations =
    mutable.Map[Constructor[_], Array[(Parameter, Attribute, Element, Converter)]]()

  def parse[T](xml: Elem, annotatedClass: Class[T]): T = {
    val elementAnnotation = annotatedClass.getAnnotation(classOf[Element])
    if (elementAnnotation == null)
      throw new IllegalArgumentException(s"Class $annotatedClass is not annotated!")

    parseElement[T](xml, annotatedClass, elementAnnotation)
  }

  def parseSeq[T](xml: Elem, annotatedClass: Class[T]): Seq[T] = {
    val elementAnnotation = annotatedClass.getAnnotation(classOf[Element])
    if (elementAnnotation == null)
      throw new IllegalArgumentException(s"Class $annotatedClass is not annotated!")

    parseElementSeq[T](xml, annotatedClass, elementAnnotation)
  }

  private def parseElement[T](xml: Elem, annotatedClass: Class[T], elementAnnotation: Element): T = {
    //Convert the annotation path and look for it in the XML
    val root = xml.getChildren(getSplitPath(elementAnnotation.path()))
    val myConstructor = annotatedClass.getConstructors.head.asInstanceOf[Constructor[T]]
    val paramsToAnnotations =
      constructorToParameterAnnotations.getOrElseUpdate(myConstructor, Utils.getParamsAnnotations(myConstructor))

    if(root.isEmpty)  throw EmptyXml("Empty XML but attribute requested!")
    else              readNode(root.head, myConstructor, paramsToAnnotations)
  }

  private def parseElementSeq[T](xml: Elem, annotatedClass: Class[T], elementAnnotation: Element): Seq[T] = {
    //Convert the annotation path and look for it in the XML
    val root = xml.getChildren(getSplitPath(elementAnnotation.path()))
    val myConstructor = annotatedClass.getConstructors.head.asInstanceOf[Constructor[T]]
    val paramsToAnnotations =
      constructorToParameterAnnotations.getOrElseUpdate(myConstructor, Utils.getParamsAnnotations(myConstructor))

    root.map{ node => readNode(node, myConstructor, paramsToAnnotations) }
  }

  private def readNode[T](node: Node,
                          myConstructor: Constructor[T],
                          paramToAnnotations: Array[(Parameter, Attribute, Element, Converter)]): T = {
    val args: Array[Object] = paramToAnnotations.map{ case (param, attr, elem, converter) =>
      if(attr != null){
        val (parseType, optional) = param.getParameterizedType match {
          case paramType: ParameterizedType if paramType.getRawType == classOf[Option[_]] =>
            (Class.forName(paramType.getActualTypeArguments()(0).getTypeName), true)
          case _ =>
            (param.getType, false)
        }
        if(converter != null){
          val value = Parser.extract(node, attr.path(), classOf[String], attr.namespace(), optional)
          converter.value().newInstance().fromString(value).asInstanceOf[Object]
        } else {
          val value = Parser.extract(node, attr.path(), parseType, attr.namespace(), optional)

          if(optional && value == null) None.asInstanceOf[Object]
          else if(optional)             Some(value).asInstanceOf[Object]
          else                          value.asInstanceOf[Object]
        }
      } else if (elem != null){
        //The constructor expects a Seq
        if(param.getType == classOf[Seq[_]]){
          val paramType = param.getParameterizedType.asInstanceOf[ParameterizedType]
          val actualType = paramType.getActualTypeArguments()(0)
          parseElementSeq(node.asInstanceOf[Elem], Class.forName(actualType.getTypeName), elem).asInstanceOf[Object]
        } else {
          // We assume it is a single object
          parseElement(node.asInstanceOf[Elem], param.getType, elem).asInstanceOf[Object]
        }
      } else
        throw new NullPointerException(s"Param not annotated: ${param.getName}")
    }(breakOut)
    myConstructor.newInstance(args: _*)
  }
}

object Parser {
  private def extract[U](elem: Node,
                         attributeName: String,
                         attributeType: Class[U],
                         attributeNamespace: String,
                         optional: Boolean): U = {
    val splitPath = getSplitPath(attributeName)
    val children = splitPath.take(splitPath.length - 1)
    val attrName = splitPath.last
    val node = elem.getChild(children)

    val targetNode =
      if(attrName == "")                  node
      else if (attributeNamespace == "")  node.attributes(attrName)
      else                                node.attribute(attributeNamespace, attrName).get

    val value = if(optional && targetNode == null)
      return null.asInstanceOf[U]
    else if (targetNode == null)
      throw new NullPointerException(s"Null node but attribute is not optional : ${attrName}")
    else
      targetNode.text

    if(attributeType == classOf[Int] || attributeType == classOf[java.lang.Integer]){
      Integer.parseInt(value).asInstanceOf[U]
    } else if (attributeType == classOf[String]){
      value.asInstanceOf[U]
    } else if (attributeType == classOf[Double] || attributeType == classOf[java.lang.Double]){
      value.toDouble.asInstanceOf[U]
    } else if (attributeType == classOf[Long]){
      value.toLong.asInstanceOf[U]
    } else if (attributeType == classOf[Boolean]){
      value.toBoolean.asInstanceOf[U]
    } else {
      if(optional)
        throw new NullPointerException(s"Type not mapped: $attributeType. With Option, please use a java type")
      else
        throw new NullPointerException(s"Type not mapped: $attributeType")
    }
  }
}