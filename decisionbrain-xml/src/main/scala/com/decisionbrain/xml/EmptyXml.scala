package com.decisionbrain.xml

final case class EmptyXml(private val message: String,
                          private val cause: Throwable = None.orNull) extends Throwable(message, cause)
