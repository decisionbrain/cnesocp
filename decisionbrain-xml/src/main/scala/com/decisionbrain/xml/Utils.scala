package com.decisionbrain.xml

import java.lang.reflect.{Constructor, Parameter}

import com.decisionbrain.xml.annotation.{Attribute, Converter, Element}

import scala.xml.{Node, NodeSeq}



object Utils {
  def getSplitPath(path: String): Seq[String] = path.split('\\').map(_.trim)

  // Attribute and Element annotations are exclusive on constructor's parameters
  // A Converter is only possible if param has an Attribute annotation
  def getParamsAnnotations[T](myConstructor: Constructor[T]): Array[(Parameter, Attribute, Element, Converter)] =
    myConstructor.getParameters.map { param =>
      val attribute = param.getAnnotation(classOf[Attribute])
      val (element, converter) =
        if(attribute == null) (param.getAnnotation(classOf[Element]), null)
        else                  (null, param.getAnnotation(classOf[Converter]))
      (param, attribute, element, converter)
    }

  implicit class NodeWrapper(xml: Node){
    def getChild(children: Seq[String]): Node = getChildren(children).head
    def getChildren(children: Seq[String]): NodeSeq =
      children.foldLeft(NodeSeq.fromSeq(Seq(xml))){ (acc, elem) => acc \ elem }
  }
}
