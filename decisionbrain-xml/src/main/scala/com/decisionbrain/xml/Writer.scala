package com.decisionbrain.xml

import java.lang.reflect.Constructor

import com.decisionbrain.xml.Utils._
import com.decisionbrain.xml.annotation.{Element, Attribute}

import scala.xml._

object Writer {

  def write[T](obj: T)(implicit m: Manifest[T]): Node = {
    val annotatedClass = m.runtimeClass.asInstanceOf[Class[T]]
    val elementAnnotation = annotatedClass.getAnnotation(classOf[Element])
    if (elementAnnotation == null)
      throw new IllegalArgumentException(s"Class $annotatedClass is not annotated!")

    val splitPath = getSplitPath(elementAnnotation.path())
    write(splitPath, obj, elementAnnotation)
  }

  private def write[T](paths: Seq[String], obj: T, elementAnnotation: Element): Node =
    paths match {
      case Seq(markup) =>
        toXml[T](markup, obj.getClass, obj)
      case _ =>
        new Elem(null, paths.head, Null, TopScope, true, write(paths.tail, obj, elementAnnotation): _*)
    }


  private def toXml[T](markup: String, annotatedClass: Class[_ <: T], obj: T): Elem = {
    val myConstructor = annotatedClass.getConstructors.head.asInstanceOf[Constructor[T]]

    val xmlAttributes = myConstructor.getParameters.filter(_.getAnnotation(classOf[Attribute]) != null)
      .foldLeft(Null: MetaData){ case (acc, param) =>
        val attr = param.getAnnotation(classOf[Attribute])
        val field = annotatedClass.getDeclaredField(param.getName)
        field.setAccessible(true) //To access private field anyway
        new UnprefixedAttribute(attr.path(), field.get(obj).toString, acc)
      }

    val subElements = myConstructor.getParameters.filter(_.getAnnotation(classOf[Element]) != null)
      .flatMap{ param =>
        val elem = param.getAnnotation(classOf[Element])
        val field = annotatedClass.getDeclaredField(param.getName)
        field.setAccessible(true) //To access private field anyway
        val newPath = getSplitPath(elem.path())

        if(param.getType == classOf[Seq[_]]) {
          field.get(obj).asInstanceOf[Seq[_]].map(o => write(newPath, o, elem))
        } else {
          write(newPath, field.get(obj), elem)
        }
      }

    new Elem(null, markup, xmlAttributes, TopScope, true, subElements: _*)
  }
}
