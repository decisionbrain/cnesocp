package com.decisionbrain.xml


import java.lang.reflect.Constructor
import java.time.ZonedDateTime

import com.decisionbrain.xml.annotation.{Attribute, Converter, Element, ValueConverter}
import org.junit.Test
import org.scalatest.Matchers._
import org.scalatest.PrivateMethodTester._





@Element(path = "stations \\ station")
case class Station(@Attribute(path = "identifier")  id  : Int,
                   @Attribute(path = "name")        name: String,
                   @Element(path = "constraint")    constraints: Seq[Constraint])

case class Constraint(@Attribute(path = "identifier") id  : String )

@Element(path = "constraint")
case class ConstraintWithDomain(@Attribute(path = "identifier", namespace = "http://www.mywebsite.com") id  : String )

@Element(path = "parsableTypes")
case class ParsableType(@Attribute(path = "int")     myInt     : Int,
                        @Attribute(path = "double")  myDouble  : Double,
                        @Attribute(path = "long")    myLong    : Long,
                        @Attribute(path = "str")     myString  : String,
                        @Attribute(path = "bool")    myBoolean : Boolean)
@Element(path = "attrInChild")
case class AttrInChild(@Attribute(path = "child \\ attr") myInt     : Int)

case class DateConverter() extends ValueConverter[ZonedDateTime] {
  override def fromString(xmlValue: String): ZonedDateTime = ZonedDateTime.parse(xmlValue)
}

@Element(path = "converter")
case class ConverterXML(@Attribute(path = "date") @Converter(classOf[DateConverter]) myDate: ZonedDateTime)

case class AddOneConverter() extends ValueConverter[Option[Int]] {
  override def fromString(xmlValue: String): Option[Int] =
    if(xmlValue == null) None else Some(Integer.parseInt(xmlValue) + 1)
}
@Element(path = "optional")
case class OptionalXML(@Attribute(path = "option")
                       myOption: Option[Integer],
                       @Attribute(path = "option2") @Converter(classOf[AddOneConverter])
                       myOption2: Option[Int])

case class NoAttributeXml(@Attribute(path = "")
                          day: String)
@Element(path = "days")
case class DaysXml(@Element(path = "day")
                   days: Seq[NoAttributeXml])

class ParserTest {

  // This is not really a test, more a main
  def test1(): Unit = {
    val xml =
      <Context>
        <satellites>
          <satellite identifier="A" band="S">
            <constraint identifier="cons1"></constraint>
          </satellite>
          <satellite identifier="B" band="X">
            <constraint identifier="cons2"></constraint>
          </satellite>
          <satellite identifier="C" band="XS"></satellite>
        </satellites>
        <stations>
          <station identifier="1" name="France"></station>
          <station identifier="2" name="Spain"></station>
          <station identifier="3" name="Portugal"></station>
        </stations>
      </Context>


/*
    val stations = parser.parse(com.decisionbain.xml, classOf[Station])
    stations should contain(Station(1, "France"))
    stations should contain(Station(2, "Spain"))
    stations should contain(Station(3, "Portugal"))*/


    val satellites = new Parser().parseSeq(xml, classOf[Satellite])
    satellites should contain (Satellite("A", "S", Constraint("cons1")))
    satellites should contain (Satellite("B", "X", Constraint("cons2")))
    satellites should contain (Satellite("C", "XS", null))
  }

  /**
    * Test basic object
    */
  @Test
  def test2(): Unit = {
    val xml = <constraint identifier="cons1"></constraint>

    val readNode = PrivateMethod[Constraint]('readNode) //This allows to test private method
    val paramToAnnot = Utils.getParamsAnnotations(classOf[Constraint].getConstructors.head)
    val c = new Parser() invokePrivate readNode(xml, classOf[Constraint].getConstructors.head, paramToAnnot)
    c should equal (Constraint("cons1"))
  }

  /**
    * Test recursion with single object
    */
  @Test
  def test3(): Unit = {
    val xml =
      <satellite identifier="A" band="S">
        <constraint identifier="cons1"></constraint>
      </satellite>

    val readNode = PrivateMethod[Satellite]('readNode) //This allows to test private method
    val myConstructor = classOf[Satellite].getConstructors.head.asInstanceOf[Constructor[Satellite]]
    val paramToAnnot = Utils.getParamsAnnotations(myConstructor)
    val s = new Parser() invokePrivate readNode(xml, myConstructor, paramToAnnot)

    s should equal (Satellite("A", "S", Constraint("cons1")))
  }

  /**
    * Test recursion with lists
    */
  @Test
  def test4(): Unit = {
    val xml =
      <station identifier="1" name="France">
        <constraint identifier="cons1"></constraint>
        <constraint identifier="cons2"></constraint>
      </station>

    val readNode = PrivateMethod[Station]('readNode) //This allows to test private method
    val myConstructor = classOf[Station].getConstructors.head.asInstanceOf[Constructor[Station]]
    val paramToAnnot = Utils.getParamsAnnotations(myConstructor)
    val s = new Parser() invokePrivate readNode(xml, myConstructor, paramToAnnot)

    s should equal (Station(1, "France", List(Constraint("cons1"), Constraint("cons2"))))
  }

  @Test
  def testParser(): Unit = {
    val xml =
      <Context>
        <stations>
          <station identifier="1" name="France">
            <constraint identifier="cons1"></constraint>
            <constraint identifier="cons2"></constraint>
          </station>
          <station identifier="2" name="Spain"></station>
          <station identifier="3" name="Portugal"></station>
        </stations>
      </Context>

    val s =  new Parser().parseSeq(xml, classOf[Station])
    s should contain (Station(1, "France", List(Constraint("cons1"), Constraint("cons2"))))
    s should contain (Station(2, "Spain", List()))
    s should contain (Station(3, "Portugal", List()))
  }

  /**
    * This should fail because the satellite does not have any constraint
    */
  @Test(expected = classOf[EmptyXml])
  def testMissingAttributeInXML(): Unit = {
    val xml =
      <Context>
        <satellites>
          <satellite identifier="C" band="XS"></satellite>
        </satellites>
      </Context>

    new Parser().parseSeq(xml, classOf[Satellite])
  }

  /**
    * Test object with namespace
    */
  @Test
  def testNamespaceAttribute(): Unit = {
    val xml = <Context xmlns:xmi="http://www.mywebsite.com">
      <constraint xmi:identifier="cons1"></constraint>
    </Context>
    val c = new Parser().parse(xml, classOf[ConstraintWithDomain])

    c should equal (ConstraintWithDomain("cons1"))
  }

  @Test
  def testAllTypes(): Unit = {
    val xml =
      <Context>
        <parsableTypes int="42"
                        double="1.1"
                        long="100"
                        str="Hello"
                        bool="true"></parsableTypes>
      </Context>
    val c = new Parser().parse(xml, classOf[ParsableType])

    c should equal (ParsableType(42, 1.1, 100, "Hello", true))
  }


  @Test
  def testConverter(): Unit = {
    val xml =
      <Context>
        <converter date="1970-01-01T07:00:00Z"></converter>
      </Context>
    val c = new Parser().parse(xml, classOf[ConverterXML])
    c should equal (ConverterXML(ZonedDateTime.parse("1970-01-01T07:00:00Z")))
  }

  @Test
  def testAttributeInChild(): Unit = {
    val xml =
      <Context>
        <attrInChild>
          <child attr="1"/>
        </attrInChild>
      </Context>
    val c = new Parser().parse(xml, classOf[AttrInChild])
    c should equal (AttrInChild(1))
  }

  @Test
  def testOptionAttribute(): Unit = {
    val xml =
      <Context>
        <optional option2="0"/>
        <optional option="0"/>
      </Context>
    val c = new Parser().parseSeq(xml, classOf[OptionalXML])
    c should contain allOf (
      OptionalXML(None, Some(1)),
      OptionalXML(Some(0), None)
    )
  }

  @Test
  def testNoAttributeButText(): Unit = {
    val xml =
      <Context>
        <days>
          <day>monday</day>
          <day>tuesday</day>
        </days>
      </Context>
    val c = new Parser().parse(xml, classOf[DaysXml])
    c.days.map(_.day) should contain allOf ("monday", "tuesday")
  }


}
