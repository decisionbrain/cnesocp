package com.decisionbrain.xml


import com.decisionbrain.xml.annotation.{Attribute, Element}
import org.junit.Test
import org.scalatest.Matchers._



@Element(path = "satellites \\ satellite")
case class Satellite(
                      @Attribute(path = "identifier")  id            : String,
                      @Attribute(path = "band")        frequencyBand : String,
                      @Element(path = "constraint") constraint: Constraint
                    )
@Element(path = "stations \\ station")
case class StationW(
                     @Attribute(path = "identifier") id  : Int,
                     @Attribute(path = "name")       name: String,
                     @Element(path = "constraint") constraint: ConstraintW
                   )
@Element(path = "stations \\ station")
case class StationW2(
                      @Attribute(path = "identifier") id  : Int,
                      @Attribute(path = "name")       name: String,
                      @Element(path = "constraint") constraints: Seq[ConstraintW]
                    )

case class ConstraintW(@Attribute(path = "identifier") id  : String )


class WriterTest {

  /**
    * Test writing simple recursive element
    */
  @Test
  def test1(): Unit = {
    val station = StationW(1, "first_station",  ConstraintW("first_constraint"))
    val xml = Writer.write(station)

    val result = <stations><station name="first_station" identifier="1"><constraint identifier="first_constraint"/></station></stations>
    xml should equal (result)
  }

  /**
    * Test writing recursive element with Seq
    */
  @Test
  def test2(): Unit = {
    val station = StationW2(1, "first_station", Seq(ConstraintW("first_constraint"), ConstraintW("second_constraint")))
    val xml = Writer.write(station)

    val result = <stations><station name="first_station" identifier="1"><constraint identifier="first_constraint"/><constraint identifier="second_constraint"/></station></stations>
    xml should equal (result)
  }

}
