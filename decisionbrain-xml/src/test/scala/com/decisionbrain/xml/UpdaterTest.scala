package com.decisionbrain.xml

import org.junit.Test
import org.scalatest.Matchers._
import scala.xml.Utility.trim

class UpdaterTest {

  // Add attributes
  @Test
  def test1(): Unit = {
    val xml =
      <data>
        <station identifier="1" name="France">
          <constraint identifier="cons1"></constraint>
          <constraint identifier="cons2"></constraint>
        </station>
      </data>

    val newXml = Updater.update(xml, "constraint", "identifier", Map("cons2" -> Seq(("oui", "non"))))

    newXml should equal (
      <data>
        <station identifier="1" name="France">
          <constraint identifier="cons1"></constraint>
          <constraint oui="non" identifier="cons2"></constraint>
        </station>
      </data>)
  }


  // Modify attributes
  @Test
  def test2(): Unit = {
    val xml =
      <data>
        <station identifier="1" name="France">
          <constraint identifier="cons1" attr="a"></constraint>
          <constraint identifier="cons2"></constraint>
        </station>
      </data>

    val newXml = Updater.update(xml, "constraint", "identifier", Map("cons1" -> Seq(("attr", "b"))))

    newXml should equal (
      <data>
        <station identifier="1" name="France">
          <constraint identifier="cons1" attr="b"></constraint>
          <constraint identifier="cons2"></constraint>
        </station>
      </data>)
  }


  //Add nodes
  @Test
  def test3(): Unit = {
    val xml =
      <data>
        <station identifier="1" name="France">
          <constraint identifier="cons1" attr="a"></constraint>
          <constraint identifier="cons2"></constraint>
        </station>
      </data>

    val newXml = Updater.append(xml, "data", "station", List(Map("a" -> "1"), Map("b" -> "2")))

    val result =  <data>
      <station identifier="1" name="France">
        <constraint identifier="cons1" attr="a"></constraint>
        <constraint identifier="cons2"></constraint>
      </station>
      <station a="1"></station>
      <station b="2"></station>
    </data>

    trim(newXml.head) should equal (trim(result))
  }

  //Add nodes and sub nodes
  @Test
  def test4(): Unit = {
    val xml =
      <data>
        <station identifier="1" name="France">
          <constraint identifier="cons1" attr="a"></constraint>
          <constraint identifier="cons2"></constraint>
        </station>
      </data>

    val newXml = Updater.appendRecursive(xml, "data", "station", List(Map("a" -> "1", "type" ->  Map("myType" -> "2"))))

    val result =  <data>
      <station identifier="1" name="France">
        <constraint identifier="cons1" attr="a"></constraint>
        <constraint identifier="cons2"></constraint>
      </station>
      <station a="1">
        <type myType="2"></type>
      </station>
    </data>

    trim(newXml.head) should equal (trim(result))
  }

}
