package com.cnes.ocp.io.xml

import java.time.format.DateTimeFormatter
import java.time.{LocalTime, ZonedDateTime}

import com.cnes.ocp.model.{Model, Parameters}
import com.cnes.ocp.io.xml.objects.Band._
import com.cnes.ocp.io.xml.objects.SpecificSchedule
import com.cnes.ocp.io.xml.objects.SpecificSchedule.INDIFFERENT
import org.scalatest.Matchers._
import org.junit.Test
import org.scalatest.PrivateMethodTester._

class XmlReaderTest {

/*
  @Test
  def basicInputReader1(): Unit = {
    val xmlReader = new XmlReaderA("src/test/resources/input3.xml")
    val xmlModel = xmlReader.parseInput()

    xmlModel.satellites.size should equal (30)
    xmlModel.satellites.count(_.band == S)  should equal (27)
    xmlModel.satellites.count(_.band == X)  should equal (2)
    xmlModel.satellites.count(_.band == SX) should equal (1)
    xmlModel.satellites.map(_.id) should contain ("Satellite_672369")

    val satellite = xmlModel.satellites.find(_.id == "Satellite_672323").get
    satellite.sat1.size should equal (62)
    val sat1Cst = satellite.sat1.find(_.id == "2228073").get
    sat1Cst.band      should equal (S)
    sat1Cst.minPass   should equal (1)
    sat1Cst.maxPass   should equal (2)
    sat1Cst.mostPass  should equal (false)
    sat1Cst.startTime should equal (LocalTime.parse("08:40"))
    sat1Cst.endTime   should equal (LocalTime.parse("12:52"))

    satellite.sat4.size should equal (2)
    val sat4Cst = satellite.sat4.find(_.id == "2228141").get
    sat4Cst.band            should equal (S)
    sat4Cst.consecutivePass should equal (true)

    xmlModel.stations.size should equal (41)
    xmlModel.stations.count(_.band == S)  should equal (31)
    xmlModel.stations.count(_.band == X)  should equal (3)
    xmlModel.stations.count(_.band == SX) should equal (7)
    xmlModel.stations.map(_.id) should contain ("Station_672273")

    val stationNoSta2 = xmlModel.stations.find(_.id == "Station_672273").get
    stationNoSta2.sta2.size should equal (0)
    val station = xmlModel.stations.find(_.id == "Station_672277").get
    station.sta2.size should equal (4)
    val sta2Cstr = station.sta2.find(_.id == "2228001").get
    sta2Cstr.minDuration  should equal (120)
    sta2Cstr.startTime    should equal (LocalTime.parse("07:00"))
    sta2Cstr.endTime      should equal (LocalTime.parse("17:00"))

    xmlModel.visibilities.size should equal(4903)
  }

  @Test
  def basicParamReader1(): Unit = {
    val xmlReader = new XmlReaderA("src/test/resources/param3.xml")
    val xmlParameter = xmlReader.parseParameters()
    xmlParameter.start  should equal (ZonedDateTime.parse("2018-11-20T00:00Z"))
    xmlParameter.end    should equal (ZonedDateTime.parse("2018-11-23T00:00Z"))
  }
*/

  @Test
  def basicInputImporter1(): Unit = {
    val build = PrivateMethod[Model]('build) //This allows to test private method
    val xmlInput = XmlReader.parseInput(getClass.getResourceAsStream("/input3.xml"))

    val buildParams = PrivateMethod[Parameters]('buildParameters) //This allows to test private method
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream("/param3.xml"))

    val params  = Model invokePrivate buildParams(xmlParams, true,false)
    val model   = Model invokePrivate build(xmlInput, params)
    model.satellites.size   should equal (3)
    model.stations.size     should equal (40)
    model.visibilities.size should equal (346 )
    model.reservations.size should equal (78)

    val satellite = model.satellites.find(_.id == "Satellite_672328").get
    val sat1Cstr = satellite.sat1Cstr.find(_.id == "2228225-0-Satellite_672328").get
    sat1Cstr.windows.head._1  should equal (ZonedDateTime.parse("2018-11-20T00:15Z[UTC]"))
    sat1Cstr.windows.head._2  should equal (ZonedDateTime.parse("2018-11-20T04:00Z[UTC]"))

    val station = model.stations.find(_.id == "Station_672312").get
    val sta2Cstr1 = station.sta2Cstr.find(_.id == "2228060-0").get
    val sta2Cstr2 = station.sta2Cstr.find(_.id == "2228060-1").get
    val sta2Cstr3 = station.sta2Cstr.find(_.id == "2228060-2").get
    sta2Cstr1.windows.size should equal (1)
    sta2Cstr1.windows should contain (
      (ZonedDateTime.parse("2018-11-20T10:00Z"), ZonedDateTime.parse("2018-11-20T15:00Z")))
    sta2Cstr2.windows should contain (
      (ZonedDateTime.parse("2018-11-21T10:00Z"), ZonedDateTime.parse("2018-11-21T15:00Z")))
    sta2Cstr3.windows should contain (
      (ZonedDateTime.parse("2018-11-22T10:00Z"), ZonedDateTime.parse("2018-11-22T15:00Z")))
  }

  @Test
  def basicParamsImporter1(): Unit = {
    val buildParameters = PrivateMethod[Parameters]('buildParameters) //This allows to test private method
    val xmlParameter = XmlReader.parseParameters(getClass.getResourceAsStream("/param3.xml"))
    val params = Model invokePrivate buildParameters(xmlParameter, true,false)
    params.start  should equal (ZonedDateTime.parse("2018-11-20T00:00:00Z[UTC]"))
    params.end    should equal (ZonedDateTime.parse("2018-11-23T00:00:00Z[UTC]"))
  }

  @Test
  def basicInputReader1(): Unit = {
    val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

    val xmlModel = XmlReader.parseInput(getClass.getResourceAsStream("/input3.xml"))

    xmlModel.satellites.size should equal (30)
    xmlModel.satellites.count(_.band == S)  should equal (27)
    xmlModel.satellites.count(_.band == X)  should equal (2)
    xmlModel.satellites.count(_.band == SX) should equal (1)
    xmlModel.satellites.map(_.id) should contain ("Satellite_672369")

    val satellite = xmlModel.satellites.find(_.id == "Satellite_672323").get
    satellite.typeId    should equal ("ResourceType_155100")
    satellite.sat1.size should equal (62)
    val sat1Cst = satellite.sat1.find(_.id == "2228073").get
    sat1Cst.band              should equal (S)
    sat1Cst.specificSchedule  should equal (INDIFFERENT)
    sat1Cst.minDuration       should equal (-1)
    sat1Cst.minPass           should equal (1)
    sat1Cst.maxPass           should equal (2)
    sat1Cst.mostPass          should equal (false)
    sat1Cst.slots.head.start  should equal (LocalTime.parse("08:40"))
    sat1Cst.slots.head.end    should equal (LocalTime.parse("12:52"))
    sat1Cst.stations.get      should equal ("Station_672279 Station_672281 Station_672282 Station_672277")

    satellite.sat4.size should equal (2)
    val sat4Cst = satellite.sat4.find(_.id == "2228141").get
    sat4Cst.band            should equal (S)
    sat4Cst.consecutivePass should equal (true)
    sat4Cst.stationProperties.size should equal (6)
    val stationProperty = sat4Cst.stationProperties.find(_.stationId == "Station_672277").get
    stationProperty.minDuration   should equal (360)
    stationProperty.minElevation  should equal (5.0)
    stationProperty.priority      should equal (1)

    xmlModel.stations.size should equal (41)
    xmlModel.stations.map(_.id) should contain ("Station_672273")

    val stationNoSta2 = xmlModel.stations.find(_.id == "Station_672273").get
    stationNoSta2.sta2.size should equal (0)
    stationNoSta2.workingPeriods.size should equal (6)
    stationNoSta2.workingPeriods.head.day should equal (ZonedDateTime.parse("2018-11-19T00:00:00.000+0000", dateFormat))
    stationNoSta2.workingPeriods.head.workingPeriods(0).start should equal (ZonedDateTime.parse("2018-11-19T06:00:00.000+0000", dateFormat))
    stationNoSta2.workingPeriods.head.workingPeriods(1).start should equal (ZonedDateTime.parse("2018-11-19T11:30:00.000+0000", dateFormat))

    val station = xmlModel.stations.find(_.id == "Station_672277").get
    station.sta2.size                 should equal (4)
    station.satelliteProperties.size  should equal (8)

    val sta2Cstr = station.sta2.find(_.id == "2228001").get
    sta2Cstr.minDuration  should equal (120)
    sta2Cstr.start    should equal (LocalTime.parse("07:00"))
    sta2Cstr.end      should equal (LocalTime.parse("17:00"))

    xmlModel.visibilities.size should equal(4903)
    val visibility = xmlModel.visibilities.find(_.id == "Visibility_342994370").get
    visibility.aosLos.elevation     should equal (0.0)
    visibility.aosLos.period.start  should equal (ZonedDateTime.parse("2018-11-23T18:06:24.000+0000", dateFormat))
    visibility.aosLos.period.end    should equal (ZonedDateTime.parse("2018-11-23T18:19:11.000+0000", dateFormat))
  }

}
