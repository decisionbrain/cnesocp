package com.cnes.ocp.io.xml

import com.cnes.ocp.io.xml.debug.GanttXmlExporter
import com.cnes.ocp.model.Model
import com.cnes.ocp.optim.SatisfactionProblemCPO
import com.cnes.ocp.utils.Main.info
import com.cnes.ocp.utils.ModelDebugger
import ilog.cp.IloCP
import org.scalatest.Matchers._
import org.junit.Test
import org.scalatest.PrivateMethodTester._


class XmlGantt {
  @Test
  def test1(): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream("/issue0ct2019/param3.xml"))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream("/issue0ct2019/input3.xml"))
    val model = Model.fromXml(xmlData, xmlParams.copy(timeLimit = 60))


    ModelDebugger.printSta2Infos(model)

    info(s"Horizon: ${model.parameters.start} -> ${model.parameters.end}")

    val satPb = SatisfactionProblemCPO(model)
    //satPb.cp.setParameter(IloCP.IntParam.SolutionLimit,1)
    satPb.buildSolve()

    GanttXmlExporter.export(model, satPb.satSolution, "tmp.xml")

  }

  @Test
  def readSolution(): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream("/issue0ct2019/param3.xml"))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream("/issue0ct2019/output4.xml"))
    val model = Model.fromXml(xmlData, xmlParams.copy(timeLimit = 30))

    info(s"Horizon: ${model.parameters.start} -> ${model.parameters.end}")

    val satPb = SatisfactionProblemCPO(model)
    val solution = satPb.buildSolutionFromReservations

    GanttXmlExporter.export(model, solution, "tmp.xml")

  }
}
