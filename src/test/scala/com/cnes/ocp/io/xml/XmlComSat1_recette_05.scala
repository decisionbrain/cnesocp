package com.cnes.ocp.io.xml

import com.cnes.ocp.model.Model
import com.cnes.ocp.utils.{Logging, ModelDebugger}
import org.junit.Test
import org.scalatest.Matchers._

/**
  * 06-CRM-sat1-PL	1 CSAT-1 le plus long 	SPOT1
  * we check that each sat1 solution is a singleton of highest duration
  */
class XmlComSat1_recette_05  extends Logging{

  val paths = List(
    //( "/11-jeux/05-CST-sat1-SX/resa3.xml" , "/11-jeux/05-CST-sat1-SX/param3.xml", "11-jeux-05-CST-sat1-SX-output.csv") ,
    ( "/11-jeux/06-CRM-sat1-PL/input3.xml" , "/11-jeux/06-CRM-sat1-PL/param3.xml", "11-jeux-06-CRM-sat1-PL-output.csv")
  )

  @Test
  def run() = {
    paths.foreach{ case(i,p,c) => importAndSolve(i,p,c)}
  }

  def importAndSolve(inputPath : String, paramPath : String, outCsvPath : String ): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream(paramPath))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream(inputPath))
    val model = Model.fromXml(xmlData, xmlParams)

    ModelDebugger.printSat1Infos(model)
    ModelDebugger.printSta2Infos(model)
    ModelDebugger.printSat1Details(model)

    val visisat = model.reservationsBySat1.map{ case(sat1,vs) => vs.map(v => (v,sat1)) }.flatten
    visisat.forall{ case(v,sat1) => v.band.equals(sat1.band) } should equal (true)

  }
}
