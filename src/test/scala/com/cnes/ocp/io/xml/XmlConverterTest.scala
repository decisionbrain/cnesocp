package com.cnes.ocp.io.xml

import com.cnes.ocp.io.xml.objects.LocalTimeWithZoneConverter
import org.scalatest.Matchers._
import org.junit.Test

class XmlConverterTest {
  @Test
  def testLocalTimeWithZoneConverter1(): Unit = {
    val converter = new LocalTimeWithZoneConverter
    val zonedDateTime = converter.fromString("1970-01-01T17:00:00Z")
    zonedDateTime.toString should equal ("17:00")
  }
}
