package com.cnes.ocp.io.xml

import com.cnes.ocp.model.{Model, Parameters, Sat1Constraint, Visibility}
import com.cnes.ocp.optim.SatisfactionProblemCPO
import com.cnes.ocp.utils.Logging
import org.junit.Test
import org.scalatest.Matchers._
import org.scalatest.PrivateMethodTester._
import scala.collection.breakOut

/**
  * 06-CRM-sat1-PL	1 CSAT-1 le plus long 	SPOT1
  * we check that each sat1 solution is a singleton of highest duration
  */
class XmlComSat1_recette_06  extends Logging{

  val paths = List(
    //( "/11-jeux/06-CRM-sat1-PL/resa3.xml" , "/11-jeux/06-CRM-sat1-PL/param3.xml", "11-jeux-06-CRM-sat1-PL-output.csv") ,
    ( "/11-jeux/06-CRM-sat1-PL/input3.xml" , "/11-jeux/06-CRM-sat1-PL/param3.xml", "11-jeux-06-CRM-sat1-PL-output.csv")
  )

  @Test
  def run() = {
    paths.foreach{ case(i,p,c) => importAndSolve(i,p,c)}
  }

  def importAndSolve(inputPath : String, paramPath : String, outCsvPath : String ): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream(paramPath))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream(inputPath))
    val model = Model.fromXml(xmlData, xmlParams)

    val problem = SatisfactionProblemCPO(model)

    val result = problem.buildSolve()

    result should equal (true)

    val resaBySat1 = model.reservationsBySat1.filter(_._1.longestPass)
    val candidatesBySat1 = model.candidatesBySat1.filter(_._1.longestPass)
    val sat1s = (resaBySat1.keySet ++ candidatesBySat1.keySet)

    val resaAndCandidates : Map[Sat1Constraint, Seq[Visibility]] = sat1s.map(sat1 =>
      sat1 -> (resaBySat1.getOrElse(sat1,Nil) ++ candidatesBySat1.getOrElse(sat1,Nil)).sortBy(v => -v.getAosDuration))(breakOut)


    println("Initial longest pass candidates +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    def visibilitiesInfo(visibilities : Seq[Visibility]) = s"${visibilities.map{v => s"${v.id}-dur-${v.getAosDuration}"}.mkString(" ")}"
    resaAndCandidates.map{ case(sat1, visibilities) => s"${sat1.id}\t${visibilitiesInfo(visibilities)}}"}.foreach(println)
    println(" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    val longestPasses = resaAndCandidates.map{ case(sat1, visibilities) =>
      (sat1,visibilities.sortBy(v => -v.getAosDuration).head)
    }
    info(s"${longestPasses.keySet.size} sat1 constraints with longest pass candidates")
    info(longestPasses.keySet.map(_.id).mkString(" "))
    longestPasses.foreach{ case(sat1, v) => info(s"${sat1.id} longest pass candidate is ${v.id} duration ${v.getAosDuration}")}

    val sat1LongestPassSolution =  problem.satSolution.satelliteVisibilities.filter(_._1._2.longestPass).map{case(sat1,visis) => (sat1._2,visis)}

    val resaAndSol : Map[Sat1Constraint, Seq[Visibility]] = sat1s.map(sat1 =>
      sat1 -> (resaBySat1.getOrElse(sat1,Nil) ++ sat1LongestPassSolution.getOrElse(sat1,Nil) )
    )(breakOut)

      info("Solution for sat1 with longest pass")
    resaAndSol.map{ case(sat1, visibilities) =>
      s"${sat1.id}\t${visibilitiesInfo(visibilities)} recall ${longestPasses(sat1).getAosDuration}"}.foreach(println)

    //we select only one candidate to satisfy the constraint
    //note that this could be wrong if another candidate for this sat1 would be also candidate for another sat1 ...
    resaAndSol.values.forall(_.size==1) should be (true) //
    resaAndSol.forall{ case(sat1, visibilities) => visibilities.head.getAosDuration == longestPasses(sat1).getAosDuration} should be(true)


    //info(s"${sat1LongestPassCandidates.size} visibilities candidate for longest pass constraints and ${sat1LongestPassSolution.size} in solution")

    //sat1LongestPassCandidates should equal(sat1LongestPassSolution)


    // identifier="2233859" for max flight pass

    //CsvExporter.export(model, problem.satSolution, xmlInput, outCsvPath)

  }
}
