package com.cnes.ocp.io.xml

import com.cnes.ocp.model.{Model, Parameters, Sat1Constraint, Visibility}
import com.cnes.ocp.optim.SatisfactionProblemCPO
import com.cnes.ocp.utils.Logging
import org.junit.{Ignore, Test}
import org.scalatest.Matchers._
import org.scalatest.PrivateMethodTester._

import scala.collection.breakOut

/**
  * 06-CRM-sat1-PL	1 CSAT-1 le plus long 	SPOT1
  * we check that each sat1 solution is a singleton of highest duration
  */
class XmlComSat1_recette_12  extends Logging{

  val paths = List(
        ( "/11-jeux/12-CTO-routine-1sem/input3.xml" , "/11-jeux/12-CTO-routine-1sem/param3.xml", "11-jeux-12-CTO-routine-1sem-output.csv") ,
    //    ( "/11-jeux/12-CTO-simu-4sem/input3.xml" , "/11-jeux/12-CTO-simu-4sem/param3.xml", "11-jeux-12-CTO-simu-4sem-output.csv") ,
    //( "/11-jeux/12-CTO-simu-9sem/input3.xml" , "/11-jeux/12-CTO-simu-9sem/param3.xml", "11-jeux-12-CTO-simu-9sem-output.csv") ,
  )

  @Test
  def run() = {
    paths.foreach{ case(i,p,c) => importAndSolve(i,p,c)}
  }

  def importAndSolve(inputPath : String, paramPath : String, outCsvPath : String ): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream(paramPath))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream(inputPath))
    val model = Model.fromXml(xmlData, xmlParams.copy(timeLimit = 30))
    val problem = SatisfactionProblemCPO(model)

    val result = problem.buildSolve()

    result should equal (true)

  }
}
