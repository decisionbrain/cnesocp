package com.cnes.ocp.io.xml

import com.cnes.ocp.model.{Model, Parameters}
import com.cnes.ocp.optim.SatisfactionProblemCPO
import com.cnes.ocp.utils.Logging
import org.junit.Test
import org.scalatest.Matchers._
import org.scalatest.PrivateMethodTester._

/**
  * 08-CRM-sat1-MIN	1 CSAT-1 durée minimum	SPOT1
  * TODO Que voulez-vous tester ? Durée minimum de tous les créneaux candidats ?
  */
class XmlComSat1_recette_08  extends Logging{

  val paths = List(
    ( "/11-jeux/08-COM-sat1-MIN/input3.xml" , "/11-jeux/08-COM-sat1-MIN/param3.xml", "11-jeux-08-COM-sat1-MIN-output.csv") ,
    //( "/11-jeux/14-COM-sta2-LOC/input3.xml" , "/11-jeux/14-COM-sta2-LOC/param3.xml","11-jeux-14-COM-sta2-LOC-output.csv") ,
    //( "/11-jeux/13-COM-sta2-UTC/input3.xml" , "/11-jeux/13-COM-sta2-UTC/param3.xml","11-jeux-13-COM-sta2-UTC-output.csv")
  )

  @Test
  def run() = {
    paths.foreach{ case(i,p,c) => importAndSolve(i,p,c)}
  }

  def importAndSolve(inputPath : String, paramPath : String, outCsvPath : String ): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream(paramPath))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream(inputPath))
    val model = Model.fromXml(xmlData, xmlParams)

    val problem = SatisfactionProblemCPO(model)

    val result = problem.buildSolve()

    result should equal (true)

    val sat1MostPassCandidates = model.candidatesBySat1.filter(_._1.mostPass).flatMap(_._2).toSet

    val sat1MostPassSolution =  problem.satSolution.satelliteVisibilities.filter(_._1._2.mostPass).values.flatten.toSet

    info(s"${sat1MostPassCandidates.size} visibilities candidate for mostPass constraints and ${sat1MostPassSolution.size} in solution")

    sat1MostPassCandidates should equal(sat1MostPassSolution)

    // identifier="2233859" for max flight pass

    //CsvExporter.export(model, problem.satSolution, xmlInput, outCsvPath)

  }
}
