package com.cnes.ocp.io.xml

import com.cnes.ocp.io.csv.CsvExporter
import com.cnes.ocp.model.{Model, Parameters}
import com.cnes.ocp.optim.SatisfactionProblemCPO
import com.cnes.ocp.utils.Logging
import org.junit.Test
import org.scalatest.Matchers._
import org.scalatest.PrivateMethodTester._

/**
  * 10-CTO-sat1-JS	1 CSAT-1 un jour de la semaine	MEGHA	10/29/2018	11/5/2018	X
  * TODO je vois qu'il n'y a pas de weekDays pour les contraintes SAT1 à traiter. Que signigie ici 'un jour de la semaine' ?
  */
class XmlComSat1_recette_10  extends Logging{

  val paths = List(
    ( "/11-jeux/10-COM-sat1-JS/input3.xml" , "/11-jeux/10-COM-sat1-JS/param3.xml", "11-jeux-10-COM-sat1-JS-output.csv") ,
    //( "/11-jeux/14-COM-sta2-LOC/input3.xml" , "/11-jeux/14-COM-sta2-LOC/param3.xml","11-jeux-14-COM-sta2-LOC-output.csv") ,
    //( "/11-jeux/13-COM-sta2-UTC/input3.xml" , "/11-jeux/13-COM-sta2-UTC/param3.xml","11-jeux-13-COM-sta2-UTC-output.csv")
  )

  @Test
  def run() = {
    paths.foreach{ case(i,p,c) => importAndSolve(i,p,c)}
  }

  def importAndSolve(inputPath : String, paramPath : String, outCsvPath : String ): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream(paramPath))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream(inputPath))
    val model = Model.fromXml(xmlData, xmlParams.copy(timeLimit = 5))
    val problem = SatisfactionProblemCPO(model)

    val result = problem.buildSolve()

    result should equal (true)
  }
}
