package com.cnes.ocp.io.xml

import com.cnes.ocp.model.Model
import com.cnes.ocp.optim.SatisfactionProblemCPO
import org.junit.Test
import org.scalatest.Matchers._

class XmlComSat1_recette_01 {
  val (i, p, c) = ( "/11-jeux/01-COM-sat1/input3.xml" , "/11-jeux/01-COM-sat1/param3.xml", "11-jeux-01-COM-sat1-output.csv")


  @Test
  def run(): Unit = {
    importAndSolve(i,p,c)
  }

  def importAndSolve(inputPath : String, paramPath : String, outCsvPath : String ): Unit = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream(paramPath))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream(inputPath))
    val model = Model.fromXml(xmlData, xmlParams)

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true)
    problem.satSolution.maintenances.size should equal (0)
  }
}
