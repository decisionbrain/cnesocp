package com.cnes.ocp.utils

import com.cnes.ocp.model.Model

// Object to print infos about objects inside model
object ModelDebugger extends Logging {

  def printSat1Infos(model: Model): Unit =
    model.satellites.foreach{ satellite =>
      val sat1Ids = satellite.sat1Cstr.map(_.id)
      val xmlSat1Ids = sat1Ids.map{ id => id.split('-')(0)}.toSet

      debug(s"${satellite.id} has ${xmlSat1Ids.size} sat1 constraints (${xmlSat1Ids.mkString(" ")}) " +
        s"resulting in  ${sat1Ids.size} instances : ${sat1Ids.mkString(" ")}")
    }

  def printSat1Details(model: Model): Unit =
    model.satellites.foreach{ satellite =>
      satellite.sat1Cstr.foreach{ sat1 =>
        val nbCandidates  = model.candidatesBySat1(sat1).size
        val nbResa        = model.reservationsBySat1(sat1).size
        val w = sat1.windows.map{ case (s, e) => s"$s->$e"}.mkString(", ")
        debug(s"sat1 $sat1 has $nbCandidates candidates and $nbResa reservations over intervals $w")
      }
    }

  def printSta2Infos(model: Model): Unit =
    model.stations.foreach{ station =>
      if(station.sta2Cstr.nonEmpty)
        debug(s"${station.id} has ${station.sta2Cstr.size} sta2 constraints to be satisfied" +
          s" by the solver ${station.sta2Cstr.map(_.id).mkString(" ")}")
    }



}
