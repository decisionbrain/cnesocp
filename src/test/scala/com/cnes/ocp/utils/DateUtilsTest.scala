package com.cnes.ocp.utils

import java.time._

import com.cnes.ocp.io.xml.objects.ConstraintType
import com.cnes.ocp.model.Parameters
import com.cnes.ocp.utils.DateUtils.{generateSat1Intervals, intersects}
import com.cnes.ocp.utils.DateUtils._
import org.scalatest.Matchers._
import org.junit.Test

class DateUtilsTest {
  val zoneId: ZoneId = ZoneId.of("UTC")
  val params = Parameters(
    ZonedDateTime.of(2019, 1, 1, 0, 0, 0, 0, zoneId),
    ZonedDateTime.of(2019, 1, 3, 0, 0, 0, 0, zoneId),
    10,
    Nil, Nil, ConstraintType.values.toList,
    removeFailingAtomicConstraints = true,exportCpoModel = false)

  @Test
  def testDateIntersection(): Unit = {
    val d1 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(0),  ZoneId.of("GMT+1"))
    val d2 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(10), ZoneId.of("GMT+1"))
    val d3 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(5),  ZoneId.of("GMT+1"))
    val d4 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(15), ZoneId.of("GMT+1"))

    intersects((d1, d2), (d3, d4)) should equal (true)
    intersects((d1, d3), (d2, d4)) should equal (false)
  }

  @Test
  def testFitInside(): Unit = {
    val d1 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(0),  zoneId)
    val d2 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(5),  zoneId)
    val d3 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(10), zoneId)
    val d4 = ZonedDateTime.ofInstant(Instant.ofEpochSecond(15), zoneId)

    (d2, d3).fitInside(d1, d4) should equal (true)
    (d2, d3).fitInside(d2, d3) should equal (true)
    (d1, d4).fitInside(d2, d3) should equal (false)
    (d1, d3).fitInside(d2, d3) should equal (false)
    (d1, d4).fitInside(d1, d3) should equal (false)
  }

  @Test //start == end
  def testToStartAndDuration(): Unit = {
    val (start, duration) = toStartAndDurations(LocalTime.of(0, 0), LocalTime.of(0, 0))

    start should equal (LocalTime.of(0, 0))
    duration should equal (24 * 60 * 60)
  }

  @Test //end before start
  def testToStartAndDuration2(): Unit = {
    val (start, duration) = toStartAndDurations(LocalTime.of(18, 0), LocalTime.of(8, 0))

    start should equal (LocalTime.of(18, 0))
    duration should equal (14 * 60 * 60)
  }

  @Test //Basic
  def testGenerateSat1Intervals1(): Unit = {
    val timeIntervals = Seq((LocalTime.of(8, 0), LocalTime.of(18, 0)))
    val res = generateSat1Intervals(timeIntervals, Nil, None, params.start, params.end)

    res.size should equal (2)
    res.flatten should contain allOf(
      (ZonedDateTime.of(2019, 1, 1, 8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2, 8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 18, 0, 0, 0, zoneId))
    )
  }

  @Test //end before start
  def testGenerateSat1Intervals2(): Unit = {
    val timeIntervals = Seq((LocalTime.of(18, 0), LocalTime.of(8, 0)))
    val res = generateSat1Intervals(timeIntervals, Nil, None, params.start, params.end)

    res.size should equal (2) //3 if we allow J0-1 interval
    res.flatten should contain allOf(
      //(ZonedDateTime.of(2018, 12, 31, 18, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 8, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 1, 18, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 8, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2, 18, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 3, 8, 0, 0, 0, zoneId))
    )
  }

  @Test //Multiple time intervals
  def testGenerateSat1Intervals3(): Unit = {
    val timeIntervals = Seq(
      (LocalTime.of(8, 0), LocalTime.of(10, 0)),
      (LocalTime.of(14, 0), LocalTime.of(18, 0)))
    val res = generateSat1Intervals(timeIntervals, Nil, None, params.start, params.end)

    res.size should equal (2)
    res.flatten should contain allOf (
      (ZonedDateTime.of(2019, 1, 1,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 1, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 18, 0, 0, 0, zoneId))
    )
  }

  @Test
  def testGenerateSat1IntervalsWithDays(): Unit = {
    // horizon is inside the first week of 2019
    val timeIntervals = Seq(
      (LocalTime.of(8, 0), LocalTime.of(10, 0)),
      (LocalTime.of(14, 0), LocalTime.of(18, 0)))
    val res = generateSat1Intervals(timeIntervals, List(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY), None, params.start, params.end)

    res.size should equal (1)
    res.flatten should contain allOf (
      (ZonedDateTime.of(2018, 12, 31,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 31, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2018, 12, 31, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 31, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 18, 0, 0, 0, zoneId))
    )
  }

  @Test
  def testGenerateSat1IntervalsWithDays2(): Unit = {
    val params2 = Parameters( ZonedDateTime.of(2018, 12, 27, 0, 0, 0, 0, zoneId),
      ZonedDateTime.of(2019, 1, 10, 0, 0, 0, 0, zoneId),
      10,
      Nil, Nil, ConstraintType.values.toList,
      removeFailingAtomicConstraints = true,exportCpoModel = false)
    val timeIntervals = Seq(
      (LocalTime.of(8, 0), LocalTime.of(10, 0)),
      (LocalTime.of(14, 0), LocalTime.of(18, 0)))
    val res = generateSat1Intervals(timeIntervals, List(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY), None, params2.start, params2.end)

    res.size should equal (3)
    res.flatten should contain allOf (
      (ZonedDateTime.of(2018, 12, 24,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 24, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2018, 12, 24,  14, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 24, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2018, 12, 26,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 26, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2018, 12, 26,  14, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 26, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2018, 12, 31,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 31, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2018, 12, 31, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 31, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 7,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 7, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 7, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 7, 18, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 9,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 9, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 9, 14, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 9, 18, 0, 0, 0, zoneId))
    )
  }

  @Test
  def testDoNotGenerateNextWeekDaily(): Unit = {
    val params2 = Parameters( ZonedDateTime.of(2018, 12, 31, 0, 0, 0, 0, zoneId),
      ZonedDateTime.of(2019, 1, 7, 0, 0, 0, 0, zoneId),
      10,
      Nil, Nil, ConstraintType.values.toList,
      removeFailingAtomicConstraints = true,exportCpoModel = false)
    val timeIntervals = Seq( (LocalTime.of(8, 0), LocalTime.of(10, 0)) )
    val res = generateSat1Intervals(timeIntervals, List(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY), None, params2.start, params2.end)

    res.size should equal (1)
    res.flatten.size should equal (2)
    res.flatten should contain allOf (
      (ZonedDateTime.of(2018, 12, 31,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 31, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019,  1,  2,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019,  1,  2, 10, 0, 0, 0, zoneId))
    )
  }

  @Test
  def testDoNotGenerateNextWeek(): Unit = {
    val params2 = Parameters( ZonedDateTime.of(2018, 12, 31, 0, 0, 0, 0, zoneId),
      ZonedDateTime.of(2019, 1, 7, 0, 0, 0, 0, zoneId),
      10,
      Nil, Nil, ConstraintType.values.toList,
      removeFailingAtomicConstraints = true,exportCpoModel = false)
    val timeIntervals = Seq( (LocalTime.of(8, 0), LocalTime.of(10, 0)) )
    val res = generateSat1Intervals(timeIntervals, List(), None, params2.start, params2.end)

    res.size should equal (7)
    res.flatten.size should equal (7)
    res.flatten should contain allOf (
      (ZonedDateTime.of(2018, 12, 31,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2018, 12, 31, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019,  1,  1,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019,  1,  1, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019,  1,  2,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019,  1,  2, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019,  1,  3,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019,  1,  3, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019,  1,  4,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019,  1,  4, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019,  1,  5,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019,  1,  5, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019,  1,  6,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019,  1,  6, 10, 0, 0, 0, zoneId))
    )
  }

  @Test
  def testGenerateSat1IntervalsWithPeriod(): Unit = {
    // horizon is inside the first week of 2019
    val timeIntervals = Seq(
      (LocalTime.of(8, 0), LocalTime.of(10, 0)))
    val res = generateSat1Intervals(timeIntervals, Nil, Some(4, ZonedDateTime.of(2018, 12, 28, 10, 0, 0, 0, zoneId)), params.start, params.end)

    res.size should equal (1)
    res.flatten should contain (
      (ZonedDateTime.of(2019, 1, 2, 8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 10, 0, 0, 0, zoneId))
    )
  }

  /**
    * workingHours : 10->11 , 15->20 on 1st Jan 2019
    * sta2 : Local time is 14->18
    * we expect sta2 application intervals is singleton 15->18
    */
  @Test
  def testGenerateSta2Intervals(): Unit = {
    val workingHours = Seq(
      (ZonedDateTime.of(2019, 1, 1, 10, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 11, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 1, 15, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 20, 0, 0, 0, zoneId)))
    val window = (LocalTime.of(14, 0), LocalTime.of(18, 0))
    val res = generateSta2Intervals(20, window, workingHours)

    res.size should equal (1)
    res should contain (
      (ZonedDateTime.of(2019, 1, 1, 15, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 18, 0, 0, 0, zoneId))
    )
  }

  @Test
  def checkOneMaintenanceIntervalPerDay(): Unit = {
    //sta2 period 9->15
    val window = (LocalTime.of(9, 0), LocalTime.of(15, 0))

    //day1 :  8->10 + 16->18
    val workingHours1 = Seq(
      (ZonedDateTime.of(2019, 1, 1,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 1, 16, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 18, 0, 0, 0, zoneId))
    )
    //day2 :  8->10 + 12->15
    val workingHours2 = Seq(
      (ZonedDateTime.of(2019, 1, 2,  8, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2, 12, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 15, 0, 0, 0, zoneId))
    )

    val res1 = generateSta2Intervals(20, window, workingHours1)
    val res2 = generateSta2Intervals(20, window, workingHours2)

    res1.size should be(1)
    res1 should contain (
      (ZonedDateTime.of(2019, 1, 1, 9, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 1, 10, 0, 0, 0, zoneId))
    )

    res2.size should be(2)
    res2 should contain allOf(
      (ZonedDateTime.of(2019, 1, 2,  9, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 10, 0, 0, 0, zoneId)),
      (ZonedDateTime.of(2019, 1, 2, 12, 0, 0, 0, zoneId), ZonedDateTime.of(2019, 1, 2, 15, 0, 0, 0, zoneId))
    )
  }



}
