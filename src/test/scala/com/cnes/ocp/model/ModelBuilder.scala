package com.cnes.ocp.model

import java.time.ZonedDateTime
import java.time.ZoneId

import com.cnes.ocp.io.xml.objects.Status.AVAILABLE
import com.cnes.ocp.io.xml.objects.Band.{Band, X}
import com.cnes.ocp.io.xml.objects.ConstraintType
import com.cnes.ocp.io.xml.objects.ConstraintType.ConstraintType
import com.cnes.ocp.io.xml.objects.SpecificSchedule.{INDIFFERENT, SpecificSchedule}
import com.cnes.ocp.utils.DateUtils._

class ModelBuilder {
  implicit val origin: ZonedDateTime = ZonedDateTime.of(2019, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC"))
  var sat1Counter = 0
  var sat4Counter = 0
  var sta2Counter = 0
  var stationCounter = 0
  var satelliteCounter = 0
  var visibilityCounter = 0

  def makeSat1Constraint(stations: Seq[String], minPass: Int, maxPass: Int, windows: Seq[(Int, Int)],
                         minDuration: Int = -1, band: Band = X, specificSchedule: SpecificSchedule = INDIFFERENT,
                         mostPass: Boolean = false, longestPass: Boolean = false
                        ): Sat1Constraint = {
    sat1Counter += 1
    Sat1Constraint(s"Sat1-$sat1Counter", band, specificSchedule, minDuration, minPass, maxPass,
      mostPass, longestPass, stations, windows.map{ case (s, e) => (s.toZonedDateTime, e.toZonedDateTime) })
  }

  //TODO pass allowJamming and reservePhysicVisibilityHole and  reserveRFVisibilityHole for tests
  def makeSatellite(sat1s: List[Sat1Constraint], stationProps: Map[String, StationProperties],
                    band: Band = X, sat4Band: Band = X, consecPass: Boolean = true, typeId: String = "type1",
                    allowJamming     : Boolean = true,
                    allowReservePhysicVisibilityHole : Boolean = true,
                    allowReserveRFVisibilityHole     : Boolean = true): Satellite = {
    satelliteCounter += 1
    Satellite(s"Satellite-$satelliteCounter",s"Satellite-$satelliteCounter", band, sat4Band, typeId, sat1s, consecPass, allowJamming, allowReservePhysicVisibilityHole,allowReserveRFVisibilityHole, stationProps)
  }

  def makeSta2Constraint(windows: Seq[(Int, Int)], minDuration: Int = 3600): Sta2Constraint = {
    sta2Counter += 1
    Sta2Constraint(s"Sta2Cons-$sta2Counter", minDuration, windows.map{ case (s, e) => (s.toZonedDateTime, e.toZonedDateTime) })
  }

  def makeStation(satelliteProps: Map[String, (Int, Int)] = Map("type1" -> (5, 5)), sta2Ctrs : Seq[Sta2Constraint] = Nil): Station = {
    stationCounter += 1
    Station(s"Station-$stationCounter", s"Station-$stationCounter",  satelliteProps, sta2Ctrs, Nil)
  }

  def makeVisibility(station: Station, satellite: Satellite, start: Int, end: Int, band: Band = X, elevation: Double = 5.0, offsetAosLos0 : Int = 10,
                     jammed      : Boolean=false,
                     physHole    : Boolean=false,
                     rfHole      : Boolean=false): Visibility = {
    visibilityCounter += 1
    val aosLos = AosLos(elevation, start.toZonedDateTime, end.toZonedDateTime)
    val aosLos0 = AosLos(elevation, (start - offsetAosLos0).toZonedDateTime, (end + offsetAosLos0).toZonedDateTime)
    Visibility(s"Visi-$visibilityCounter", band, station, satellite, AVAILABLE, 0, jammed, physHole, rfHole, aosLos0, aosLos)
  }

  def makeParameters(end: Int, timeLimit: Int = 10,
                     usedSatellites: Seq[String] = Nil,
                     usedStations: Seq[String] = Nil,
                     constraintTypes: Seq[ConstraintType] = ConstraintType.values.toList,
                     removeFailingAtomicConstraints: Boolean = true,
                     exportCpoModel : Boolean = false): Parameters =
    Parameters(origin, end.toZonedDateTime, timeLimit, usedSatellites, usedStations, constraintTypes, removeFailingAtomicConstraints,exportCpoModel)

}


object ModelBuilder {
  def makeStationProperty(minDuration: Int, minElevation: Double = 5.0, priority: Int = 1): StationProperties =
    StationProperties(minDuration, minElevation, priority)

  def makeModel(parameters  : Parameters,
                stations    : List[Station],
                satellites  : List[Satellite],
                visibilities: List[Visibility],
                reservations: List[Visibility] = Nil,
                extraPasses : List[Visibility] = Nil
               ) = Model(parameters, stations, satellites, visibilities, reservations, extraPasses)

  // helper to create a workingHour interval on a given date from origin
  def toInterval(day : Int, startHour : Int, endHour : Int): (Int, Int) =
    (day * 24 * 3600 + startHour * 3600, day * 24 * 3600 + endHour * 3600)

  def toInt(day : Int, startHour : Int, startMinutes : Int = 0): Int = day * 24 * 3600 + startHour * 3600 + startMinutes * 60


}