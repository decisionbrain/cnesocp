package com.cnes.ocp.model

//import monocle.Lens
//import monocle.Monocle._
//import monocle.macros.GenLens

//object ModelModifier {
//  val satelliteLens : Lens[Model, List[Satellite]]  = GenLens[Model](_.satellites)
//  val stationPropLens: Lens[Satellite, Map[String, StationProperties]] = GenLens[Satellite](_.stationProperties)
//  val sat1Lens: Lens[Satellite, List[Sat1Constraint]] = GenLens[Satellite](_.sat1Cstr)
//  val minDurationLens: Lens[StationProperties, Int] = GenLens[StationProperties](_.minDuration)
//  val minPassLens: Lens[Sat1Constraint, Int] = GenLens[Sat1Constraint](_.minPass)
//
//  val stationLens   : Lens[Model, List[Station]]    = GenLens[Model](_.stations)
//
//  private val satelliteSelector = (i: Int) => satelliteLens composeOptional index(i)
//  private val sat1Selector = (i: Int) => sat1Lens composeOptional index(i)
//  private val stationSelector = (i: Int) => stationLens composeOptional index(i)
//  private val stationPropSelector = (s: String) => stationPropLens composeOptional index(s)
//
//
//  implicit class ModelWrapper(model: Model){
//
//    def setMinDuration(satelliteIndex: Int, stationId: String, value: Int): Model =
//      (satelliteSelector(satelliteIndex)
//        composeOptional stationPropSelector(stationId)
//        composeLens minDurationLens).set(value)(model)
//
//    def setMinPassModel(satelliteIndex: Int, sat1Index: Int, value: Int): Model =
//       (satelliteSelector(satelliteIndex)
//         composeOptional sat1Selector(sat1Index)
//         composeLens minPassLens).set(value)(model)
//  }
//}
