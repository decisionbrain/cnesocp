package com.cnes.ocp.optim

import java.time.{ZoneId}

import com.cnes.ocp.model.{Model, ModelBuilder}
import com.cnes.ocp.model.ModelBuilder.{makeModel, makeStationProperty}
import com.cnes.ocp.utils.DateUtils
import com.cnes.ocp.utils.DateUtils._
import org.junit.Test
import org.scalatest.Matchers._

/**
  * Station2 test (maintenance to insert into dedicated area)
  */
class Sta2Test {
  val mb = new ModelBuilder
  val zoneId: ZoneId = ZoneId.of("UTC")


  /**
    * workingHours :  8->10 + 16->18 on 1st Jan 2019
    * workingHours :  8->10 + 12->15 on 2nd Jan 2019
    * sta2 : area for maintenance  is 9->15, min duration one hour
    * we expect one potential intervals for day1 and 2 for day2
    */
  @Test
  def checkOneMaintenanceIntervalPerDay(): Unit = {
    val params = mb.makeParameters(60 * 2 * 1440) //2 days horizon

    //sta2 period 9->15
    //day1 :  8->10 + 16->18 intersects 9->15 = 9->10
    //day2 :  8->10 + 12->15 intersects 9->15 = 9->10 + 12->15
    val workingHours1 = Seq(ModelBuilder.toInterval(0, 9, 10))
    val workingHours2 = Seq(ModelBuilder.toInterval(1, 8, 10), ModelBuilder.toInterval(1, 12, 15))
    val sta2_1 = mb.makeSta2Constraint(workingHours1,3600)
    val sta2_2 = mb.makeSta2Constraint(workingHours2,3600)

    val station1 = mb.makeStation(Map("type1" -> (5, 5)), sta2_1::sta2_2::Nil)
    val stations = List(station1)

    val model = makeModel(params, stations, List(), List())
    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    //one maintenance var for each instance of sat2 (per day)
    problem.cp.getAllIloIntervalVars.length should be(2)

    result should equal (true)

    //all interval vars minimum duration should be the one of sta2 constraint
    problem.satSolution.maintenances.values.forall(ms =>
      ms._2.toEpochSecond - ms._1.toEpochSecond == sta2_1.minDuration
    ) should be (true)

    //all maintenance intervals should be included in one of the existing sta2 area
    problem.satSolution.maintenances.forall{ case (sta2, area) => sta2.windows.exists(w => area.fitInside(w)) }

    val allMaintenanceIntervals = problem.satSolution.maintenances.values
    val pairs = for(itv1 <- allMaintenanceIntervals; itv2 <- allMaintenanceIntervals if itv1 != itv2) yield (itv1, itv2)
    pairs.forall{ case (itv1, itv2) => !DateUtils.intersects(itv1, itv2) }  should be(true)

  }


  /**
    * Start from Same test as checkOneMaintenanceIntervalPerDay :
    *
    * workingHours :  8->10 + 16->18 on 1st Jan 2019
    * workingHours :  8->10 + 12->15 on 2nd Jan 2019
    * sta2 : area for maintenance  is 9->15, min duration one hour
    * we expect one potential intervals for day1 and 2 for day2
    *
    * In addition, we have one sat1 that requires one visibility every day in the slot 12-14 //TODO this is not true. David: update or remove this test
    */
  @Test
  def checkSta2AndSat1(): Unit = {
    val params = mb.makeParameters(60 * 2 * 1440) //2 days horizon

    //sta2 period 9->15
    //day1 :  8->10 + 16->18 intersects 9->15 = 9->10
    //day2 :  8->10 + 12->15 intersects 9->15 = 9->10 + 12->15
    val workingHours1 = Seq(ModelBuilder.toInterval(0, 9, 10))
    val workingHours2 = Seq(ModelBuilder.toInterval(1, 8, 10), ModelBuilder.toInterval(1, 12, 15))
    val sta2MinDurationOneHour = 3600
    val sta2_1 = mb.makeSta2Constraint(workingHours1,sta2MinDurationOneHour)
    val sta2_2 = mb.makeSta2Constraint(workingHours2,sta2MinDurationOneHour)

    val station1 = mb.makeStation(Map("type1" -> (5, 5)), sta2_1::sta2_2::Nil)
    val stations = List(station1)

    //SAT1 infos
    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val areaSat1 = ModelBuilder.toInterval(1, 8, 10) //same area as Sta2-2 on second day
    // we create Sat1Constraint to force usage of sat2-2 first slot (using a minimum sat1 duration of one hour + 10 seconds, therefore the area 8-10 of day 2 cannot be
    // used anymore to assign maintenance slot of sta2-2
    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, List(areaSat1), 3610)
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps)
    val visibility1 = mb.makeVisibility(station1, satellite, ModelBuilder.toInt(1,8,30),ModelBuilder.toInt(1,9,40))

    implicit val model: Model = makeModel(params, stations, List(satellite),List(visibility1), List())
    val problem = SatisfactionProblemCPO(model)

    val result = problem.buildSolve()

    result should equal (true)
    val maintenanceSlots = problem.satSolution.maintenances.values
    maintenanceSlots.size should be(2) //one for each day

    //all interval vars minimum duration should be the one of sta2 constraint
    maintenanceSlots.forall(ms =>  ms._2.toEpochSecond - ms._1.toEpochSecond == sta2_1.minDuration) should be (true)

    //all maintenance intervals should be included in one of the existing sta2 area
    problem.satSolution.maintenances.forall{ case (sta2, area) => sta2.windows.exists(w => area.fitInside(w)) }

    problem.satSolution.maintenances.keySet.size should be(2)//one maintenance for each sta2
    val (maintenanceDay1Start, maintenanceDay1End) = problem.satSolution.maintenances(sta2_1)
    val (maintenanceDay2Start, _) = problem.satSolution.maintenances(sta2_2)

    maintenanceDay1Start.toInt  should equal (ModelBuilder.toInt(0, 9))
    maintenanceDay1End.toInt    should equal (ModelBuilder.toInt(0, 10))

    //day2 can only be positioned in the second area
    maintenanceDay2Start.toInt < ModelBuilder.toInt(1, 12) should be(false)

    val solutionVisi = problem.satSolution.satelliteVisibilities.values.flatten
    solutionVisi.size should be(1)
    solutionVisi should contain(visibility1)
  }

}
