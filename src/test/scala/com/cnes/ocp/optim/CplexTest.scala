package com.cnes.ocp.optim

import com.cnes.ocp.model.ModelBuilder.{makeModel, makeStationProperty}
import com.cnes.ocp.model.ModelBuilder
import ilog.cp.IloCP
import org.junit.Test
import org.scalatest.Matchers._

class CplexTest {

  val mb = new ModelBuilder

  @Test
  def basicCplex(): Unit = {
    val cplex = new IloCP()
    cplex.getVersion should equal ("12.9.0.0")
  }

  @Test
  def buildBasicModel(): Unit = {
    val mb = new ModelBuilder
    val station1 = mb.makeStation()
    val station2 = mb.makeStation()
    val stations = List(station1, station2)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10),
      station2.id -> makeStationProperty(minDuration = 20))

    val sat1Constraint = mb.makeSat1Constraint(stations.map(_.id), 1, 2, Seq((0, 35)))
    val satellite = mb.makeSatellite(List(sat1Constraint), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val visibility2 = mb.makeVisibility(station2, satellite, 20, 30)
    val params = mb.makeParameters(60)

   val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2))

    val problem = SatisfactionProblemCPO(model)
    problem.buildSolve()

    val satelliteAssignmt = problem.satSolution.satelliteVisibilities(satellite, sat1Constraint)
    satelliteAssignmt.size should equal (1)
    satelliteAssignmt should contain (visibility1)
  }

  //Test that setup times are enforced (by default they are 5 seconds) -> this model is infeasible
  @Test
  def buildBasicModel2(): Unit = {
    val mb = new ModelBuilder
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val sat1Constraint = mb.makeSat1Constraint(stations.map(_.id), 2, 2, Seq((0, 35)))
    val satellite = mb.makeSatellite(List(sat1Constraint), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val visibility2 = mb.makeVisibility(station1, satellite, 10, 30)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2))

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (false)
  }

  @Test
  def propertiesJamminPhysicalHoleRfHole(): Unit = {
    val mb = new ModelBuilder
    val station1 = mb.makeStation()
    val station2 = mb.makeStation()
    val stations = List(station1, station2)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10),
      station2.id -> makeStationProperty(minDuration = 20))

    val sat1Constraint = mb.makeSat1Constraint(stations.map(_.id), 1, 2, Seq((0, 35)))
    val satellite = mb.makeSatellite(List(sat1Constraint), stationProps, allowJamming = false, allowReservePhysicVisibilityHole = false,allowReserveRFVisibilityHole = false)

    val visibility1Jammed = mb.makeVisibility(station1, satellite, 0, 10,jammed = true)
    val visibility1PhyHole = mb.makeVisibility(station1, satellite, 0, 10,physHole = true)
    val visibility1RfHole = mb.makeVisibility(station1, satellite, 0, 10,rfHole = true)

    val params = mb.makeParameters(60, removeFailingAtomicConstraints = false)

    val model = makeModel(params, stations, List(satellite), List(visibility1Jammed,visibility1PhyHole,visibility1RfHole))

    val problem = SatisfactionProblemCPO(model)
    problem.buildSolve() should be (false)

  }

}
