package com.cnes.ocp.optim

import java.io.{File, FileOutputStream}

import com.cnes.ocp.io.csv.CsvExporter
import com.cnes.ocp.io.xml.{XmlExporter, XmlReader}
import com.cnes.ocp.model.Model
import org.junit.Test
import org.scalatest.Matchers._

class ProtoTest {

  def runFromFolder(folder: String, timeLimit: Int = 5): Boolean = {
    val xmlParams = XmlReader.parseParameters(getClass.getResourceAsStream(s"${folder}param3.xml"))
    val xmlData   = XmlReader.parseInput(getClass.getResourceAsStream(s"${folder}input3.xml"))
    val model = Model.fromXml(xmlData, xmlParams.copy(timeLimit = timeLimit))

    val satPb = SatisfactionProblemCPO(model)
    val result = satPb.buildSolve()
//    if(result){
//      CsvExporter.export(model, satPb.satSolution, xmlData, "input3-result.csv")
//      XmlExporter.export(model.stations, satPb.satSolution, xmlData, getClass.getResource(s"${folder}input3.xml").getPath, "input3-result.xml")
//    }
//    //Export conflict
//    val stream = new FileOutputStream(s"conflict.clp")
//    stream.write(satPb.conflict.toByteArray)
    result
  }

  @Test def test1() : Unit = runFromFolder("/11-jeux/01-COM-sat1/")           should equal (true)
  @Test def test2() : Unit = runFromFolder("/11-jeux/02-COM-sat1/")           should equal (true)
  @Test def test3() : Unit = runFromFolder("/11-jeux/03-COM-sat1/")           should equal (true)
  @Test def test4() : Unit = runFromFolder("/11-jeux/04-CRM-sat1-3NDJ/")      should equal (true)
  @Test def test5() : Unit = runFromFolder("/11-jeux/05-CST-sat1-SX/")        should equal (true)
  @Test def test6() : Unit = runFromFolder("/11-jeux/06-CRM-sat1-PL/")        should equal (true)
  @Test def test7() : Unit = runFromFolder("/11-jeux/07-CRM-sat1-DP/")        should equal (true)
  @Test def test8() : Unit = runFromFolder("/11-jeux/08-COM-sat1-MIN/")       should equal (true)
  @Test def test9() : Unit = runFromFolder("/11-jeux/09-COM-sat1-PGN/")       should equal (true)
  @Test def test10(): Unit = runFromFolder("/11-jeux/10-COM-sat1-JS/")        should equal (true)
  @Test def test11(): Unit = runFromFolder("/11-jeux/11-CSP-sat1-PER/")       should equal (true)
  @Test def test12a(): Unit = runFromFolder("/11-jeux/12-CTO-routine-1sem/")  should equal (true)
  @Test def test12b(): Unit = runFromFolder("/11-jeux/12-CTO-simu-4sem/", 30) should equal (true)
  @Test def test12c(): Unit = runFromFolder("/11-jeux/12-CTO-simu-9sem/", 120)should equal (true)
  @Test def test13(): Unit = runFromFolder("/11-jeux/13-COM-sta2-UTC/")       should equal (true)
  @Test def test14(): Unit = runFromFolder("/11-jeux/14-COM-sta2-LOC/")       should equal (true)
  @Test def test15(): Unit = runFromFolder("/11-jeux/15-COM-sta2-3/")         should equal (true)
  @Test def test16(): Unit = runFromFolder("/11-jeux/16-COM-sat1-IMP1/")      should equal (true)
  @Test def test17(): Unit = runFromFolder("/11-jeux/17-COM-sat1-IMP2/")      should equal (false)
  @Test def test18(): Unit = runFromFolder("/11-jeux/18-COM-sat1-IMP3/")      should equal (true)

}
