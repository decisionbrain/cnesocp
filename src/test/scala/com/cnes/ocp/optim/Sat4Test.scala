package com.cnes.ocp.optim

import java.time.ZoneId

import com.cnes.ocp.model.ModelBuilder
import com.cnes.ocp.model.ModelBuilder.{makeModel, makeStationProperty}
import org.junit.Test
import org.scalatest.Matchers._

class Sat4Test {

  val mb = new ModelBuilder
  val zoneId: ZoneId = ZoneId.of("UTC")


  /**
    * we have three consecutive visibilities on station 1, 1, and 2
    * SAT1 is set with mostPass so normal assignment would assign all three visibilities
    */
  @Test
  def allowConsecutivesWithMostPass(): Unit = {
    val station1 = mb.makeStation()
    val station2 = mb.makeStation()
    val stations =  List(station1, station2)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10),
      station2.id -> makeStationProperty(minDuration = 10)
    )

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 2, 3, Seq((0, 100)), mostPass = true)
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps, consecPass = true)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val visibility2 = mb.makeVisibility(station1, satellite, 40, 50)
    val visibility3 = mb.makeVisibility(station2, satellite, 80, 90)
    val params = mb.makeParameters(150)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2, visibility3))
    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true)

    val planned = problem.satSolution.satelliteVisibilities(satellite, sat1ConstraintA)
    planned.size should equal (3)
    planned should contain allOf(visibility1, visibility2, visibility3)
  }

  /**
    * we have three consecutive visibilities on station 1, 1, and 2
    * SAT1 is set with mostPass so normal assignment would assign all three visibilities
    * however, if we set consecutive pass to false on the satellite, intermediate visibility on station 1 will be skipped
    */
  @Test
  def forbidConsecutivesWithMostPass(): Unit = {
    val station1 = mb.makeStation()
    val station2 = mb.makeStation()
    val stations =  List(station1, station2)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10),
      station2.id -> makeStationProperty(minDuration = 10)
    )

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 2, 3, Seq((0, 100)), mostPass = true)
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps, consecPass = false)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val visibility2 = mb.makeVisibility(station1, satellite, 40, 50)
    val visibility3 = mb.makeVisibility(station2, satellite, 80, 90)
    val params = mb.makeParameters(150)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2, visibility3))
    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true)

    val planned = problem.satSolution.satelliteVisibilities(satellite, sat1ConstraintA)
    planned.size should equal (2)
    planned should contain allOf(visibility2, visibility3)
  }



}