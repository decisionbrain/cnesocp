package com.cnes.ocp.optim

import java.time.ZoneId

import com.cnes.ocp.model.ModelBuilder.{makeModel, makeStationProperty}
import com.cnes.ocp.model.{ModelBuilder, Sat1Constraint, Visibility}
import org.junit.{Ignore, Test}
import org.scalatest.Matchers._

import scala.collection.breakOut

/**
  * we create two sat1 asking for one visibility each. When they share visibility, this one should be counted twice in the optimal solution
  * since it will minimize the overall cost
  */
class Sat1Test {
  val mb = new ModelBuilder
  val zoneId: ZoneId = ZoneId.of("UTC")

  //two sat1 instances on two different days
  @Test
  def oneVisibilityPerDayForOneSat1(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val day1_8_10 = ModelBuilder.toInterval(0, 8, 10)
    val day2_8_10 = ModelBuilder. toInterval(1, 8, 10)

    val sat1ConstraintDay1 = mb.makeSat1Constraint(stations.map(_.id), 1, 1, List(day1_8_10))
    val sat1ConstraintDay2 = mb.makeSat1Constraint(stations.map(_.id), 1, 1, List(day2_8_10))
    val satellite = mb.makeSatellite(List(sat1ConstraintDay1,sat1ConstraintDay2), stationProps)

    val (day1_9, day1_10) = ModelBuilder.toInterval(0, 9, 10)
    val (day2_9, day2_10) = ModelBuilder.toInterval(1, 9, 10)
    val visibility1 = mb.makeVisibility(station1, satellite, day1_9, day1_10)
    val visibility2 = mb.makeVisibility(station1, satellite, day2_9, day2_10)
    val params = mb.makeParameters(3600 * 24 * 2, removeFailingAtomicConstraints = false) //2 days horizon

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2))

    val test =model.candidatesBySat1(sat1ConstraintDay1)

    model.candidatesBySat1(sat1ConstraintDay1).size should be (1)
    model.candidatesBySat1(sat1ConstraintDay2).size should be (1)
    model.candidatesBySat1(sat1ConstraintDay1) should contain(visibility1)
    model.candidatesBySat1(sat1ConstraintDay2) should contain(visibility2)

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true)
    val visiChosen = problem.satSolution.satelliteVisibilities.values.flatten
    visiChosen.size should be(2) //all visibilities are assigned
  }


  //Test one sat1 with no compatible candidate fails
  @Test
  def failIfNoCandidate(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 35)))
    //val sat1ConstraintB = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((10, 55)))
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps)

    val visibility2 = mb.makeVisibility(station1, satellite, 20, 25)
    val visibility3 = mb.makeVisibility(station1, satellite, 40, 50)
    val params = mb.makeParameters(60, removeFailingAtomicConstraints = false)

    val model = makeModel(params, stations, List(satellite), List(visibility2, visibility3))

    val sat1ACandidates = model.candidatesBySat1(sat1ConstraintA).toSet
    sat1ACandidates.contains(visibility2) should be (false) //does not respect the min duration of the station
    sat1ACandidates.contains(visibility3) should be (false) //does not respect the time window
    sat1ACandidates.isEmpty should be (true)

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (false)
    problem.satSolution should be (null)
  }

  //Test that one sat1 will filter out candidates and keep the only one that matches
  @Test
  def filterOutSomeCandidates(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 35)))
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val visibility2 = mb.makeVisibility(station1, satellite, 20, 25)
    val visibility3 = mb.makeVisibility(station1, satellite, 40, 50)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2,visibility3))

    val sat1ACandidates = model.candidatesBySat1(sat1ConstraintA).toSet
    sat1ACandidates.contains(visibility1) should be (true)
    sat1ACandidates.contains(visibility2) should be (false) //does not respect the min duration of the station
    sat1ACandidates.contains(visibility3) should be (false) //does not respect the time window

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true)
    val chosenVisibilities = problem.satSolution.satelliteVisibilities.values.flatten.toSet

    chosenVisibilities.size should be (1)
  }

  /**
    * SAT1[1-1]
    * visi1 : one candidate for SAT1
    * visi2 : an other reservation not candidate for SAT1, but his presence will desactivate visi1
    * the model is therefore inconsistent
    */
  @Test
  def reservationCreatesConflictWithSAT1(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 35)))
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val visibility2 = mb.makeVisibility(station1, satellite, 10, 15)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1), List(visibility2))

    model.candidatesBySat1(sat1ConstraintA).toSet.equals(Set(visibility1)) should be (true)

    model.reservationsBySat1(sat1ConstraintA).toSet.isEmpty should be (true) //this reservation of duration 5 does not respect station properties

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (false) //conflict between the no overlap on station and the sat1
  }

  /**
    * SAT1[1-1]
    * visibility 1 : candidate for SAT1
    * visibility 2 : reserved and candidate for SAT1, but overlap visibility 1
    * the model is consistent and should select visibility2
    */
  @Test
  def reservationCompatibleWithSAT1(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 35)))
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val visibility2 = mb.makeVisibility(station1, satellite, 10, 20)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1), List(visibility2)) //visibilites + reservations + extra passes

    model.candidatesBySat1(sat1ConstraintA).toSet.equals(Set(visibility1)) should be (true)
    model.reservationsBySat1(sat1ConstraintA).toSet.equals(Set(visibility2)) should be (true) //the reservation is compatible

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true) //conflict between the no overlap on station and the sat1
    val chosenVisibilities = problem.satSolution.satelliteVisibilities.values.flatten.toSet
    chosenVisibilities.isEmpty should be (true) //reservations are not mapped to solution
  }

  /**
    * SAT1[1-1]
    * visibility 1 : extra pass reserved , should not be seen as compatible with SAT1
    * the model is inconsistent since an extra pass is not counted within a SAT1 and there is no other candidate nor reservation
    */
  @Test
  def extraPassDoesNotSatisfySAT1(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 35)))
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10)
    val params = mb.makeParameters(60, removeFailingAtomicConstraints = false)

    val model = makeModel(params, stations, List(satellite), Nil,Nil, List(visibility1)) //visibilites + reservations + extra passes

    model.candidatesBySat1(sat1ConstraintA).isEmpty should be (true)
    model.reservationsBySat1(sat1ConstraintA).isEmpty should be (true) //the reservation extra pass is not compatible

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (false) //conflict between the no overlap on station and the sat1
  }


  //Test that if a visibility can be chosen for two concurrent sat1, then it is chosen
  @Test
  def shareVisibility(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 35)))
    val sat1ConstraintB = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((10, 55)))
    val satellite = mb.makeSatellite(List(sat1ConstraintA,sat1ConstraintB), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10) //for sat1A but not fot sat1B
    val visibility2 = mb.makeVisibility(station1, satellite, 10, 30) //for sat1A and sat1B
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2))

    val sat1ACandidates = model.candidatesBySat1(sat1ConstraintA).toSet
    val sat1BCandidates = model.candidatesBySat1(sat1ConstraintB).toSet
    model.visibilities.forall( v => sat1ACandidates.contains(v)) should be (true) //visibility 1 and 2 are candidates
    model.visibilities.forall( v => sat1BCandidates.contains(v)) should be (false) //visibility 2 ony is candidate

    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true)
    val chosenVisibilities = problem.satSolution.satelliteVisibilities.values.flatten.toSet

    chosenVisibilities.size should be (1)
    chosenVisibilities.contains(visibility2) should be (true)
    chosenVisibilities.contains(visibility1) should be (false)
  }

  //a SAT1 with an area 23:00->01:00 that overlaps 00:00 will be satisfied on the first day with either intervals that are included in the horizon
  //or only reservations if candidates start before start of horizon
  //Test that if a visibility can be chosen for two concurrent sat1, then it is chosen
  @Test
  def overlap24(): Unit = {
    val station1 = mb.makeStation()
    val stations = List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10))

    val params = mb.makeParameters(24 * 3600 * 1, removeFailingAtomicConstraints = false) //one day

    val prevDay_23_01 = ModelBuilder.toInterval(-1, 23, 25)
    val currentDay_23_01 = ModelBuilder.toInterval(0, 23, 25)

    val sat11 = mb.makeSat1Constraint(stations.map(_.id) , 1, 1, List(prevDay_23_01))
    val sat12 = mb.makeSat1Constraint(stations.map(_.id) , 1, 1, List(currentDay_23_01))

    val satellite = mb.makeSatellite(List(sat11, sat12), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite , ModelBuilder.toInt(-1, 23), ModelBuilder.toInt(-1,23,30) )
    val visibility2 = mb.makeVisibility(station1, satellite , ModelBuilder.toInt(0, 12), ModelBuilder.toInt(0,23) )
    val visibility3 = mb.makeVisibility(station1, satellite , ModelBuilder.toInt(0, 23), ModelBuilder.toInt(0,23,30) )

    //*****************************   *****************************   *****************************   *****************************   *****************************
    //*****************************   First model with only visibilities but no reservations : only visibility 2 is candidate for the second constraint
    //*****************************   *****************************   *****************************   *****************************   *****************************
    val model1 = makeModel(params, stations, List(satellite), List(visibility2, visibility3))
    model1.reservations.isEmpty should be(true)

    val problem1 = SatisfactionProblemCPO(model1)
    val result1 = problem1.buildSolve()

    model1.candidatesBySat1(sat11).isEmpty should be(true)
    model1.candidatesBySat1(sat12).size should be(1)
    model1.candidatesBySat1(sat12) should contain(visibility3)
    result1 should equal (false)

    //*****************************   *****************************   *****************************   *****************************   *****************************
    //*****************************   Second model with visibility 1 reserved and candidate for the overlap sat11 constraint
    //*****************************   *****************************   *****************************   *****************************   *****************************

    val model2 = makeModel(params, stations, List(satellite), List(visibility2, visibility3), List(visibility1))
    model2.reservations.size should be (1)
    model2.reservations should contain(visibility1)

    val problem2 = SatisfactionProblemCPO(model2)
    val result2 = problem2.buildSolve()

    model2.candidatesBySat1(sat11).size should be(0)
    model2.reservationsBySat1(sat11).size should be(1)
    model2.candidatesBySat1(sat12).size should be(1)
    model2.candidatesBySat1(sat12) should contain(visibility3)

    result2 should equal (true)
  }

  //a SAT1 with a flag longest pass should select the visibility with longest aos duration
  @Test
  def longestPass(): Unit = {
    val station1 = mb.makeStation(Map("type1" -> (0,0)))
    val stations =  List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10),
    )

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)), mostPass = false, longestPass = true)
    val satellite = mb.makeSatellite(List(sat1ConstraintA), stationProps, consecPass = true)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10,offsetAosLos0 = 0)
    val visibility2 = mb.makeVisibility(station1, satellite, 40, 60,offsetAosLos0 = 0)
    val visibility3 = mb.makeVisibility(station1, satellite, 70, 95,offsetAosLos0 = 0)
    val visibilities = List(visibility1, visibility3, visibility2)
    val params = mb.makeParameters(150)

    visibilities.map{ v => s"${v.id} dur ${v.getAosDuration}"}.foreach(println)
    val longestVisibility = visibilities.sortBy(v => -v.getAosDuration).head

    val model = makeModel(params, stations, List(satellite), visibilities)
    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()

    result should equal (true)

    val planned = problem.satSolution.satelliteVisibilities(satellite, sat1ConstraintA)
    //planned.size should equal (1)
    planned should contain (longestVisibility)
  }

  /**
    * 2 sat1 with longest pass and cardinality 1 for each sat1
    * first one has visibilities durations V1(10),V2(20),V3(25)
    * second one has visibilities durations V3(25) ,V4(30)
    * we expect either V3 or V4 chosen
    */
  @Test
  def longestPassWithTwoSat1_UC1(): Unit = {
    val station1 = mb.makeStation(Map("type1" -> (0,0)))
    val stations =  List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10),
    )

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)), mostPass = false, longestPass = true)
    val sat1ConstraintB = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((70, 150)), mostPass = false, longestPass = true)
    val sat1s = List(sat1ConstraintA,sat1ConstraintB)
    val satellite = mb.makeSatellite(sat1s, stationProps, consecPass = true)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10,offsetAosLos0 = 0) //A
    val visibility2 = mb.makeVisibility(station1, satellite, 40, 60,offsetAosLos0 = 0) //A
    val visibility3 = mb.makeVisibility(station1, satellite, 70, 95,offsetAosLos0 = 0)//A,B
    val visibility4 = mb.makeVisibility(station1, satellite, 100, 130,offsetAosLos0 = 0)//B
    val visibilities = List(visibility1, visibility2, visibility3, visibility4)
    val params = mb.makeParameters(150)

    visibilities.map{ v => s"${v.id} dur ${v.getAosDuration}"}.foreach(println)


    val model = makeModel(params, stations, List(satellite), visibilities)
    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()
    result should equal (true)

    val longestVisibility : Map[Sat1Constraint, Int] = sat1s.map(sat1 => sat1 -> model.candidatesBySat1(sat1).sortBy(v => -v.getAosDuration).head.getAosDuration)(breakOut)
    val hasSharedVisi : Map[Sat1Constraint, Boolean] = sat1s.map{ case sat1 =>
      sat1 -> model.candidatesBySat1(sat1).exists( v => model.candidatesBySat1.filter(_._1.id != sat1.id).values.flatten.toSet.contains(v) )
    }(breakOut)

    val chosenVisibilities : Map[Sat1Constraint, Seq[Visibility]] = problem.satSolution.satelliteVisibilities.map{ case (sat1,visi) => sat1._2 -> visi}(breakOut)

    val (sat1Shared, sat1Unshared) = sat1s.partition( hasSharedVisi )

    //each sat1 with unshared visibility shall have its longest visibility chosen
    sat1Unshared.forall( sat1 => chosenVisibilities(sat1).sortBy(v => -v.getAosDuration).head.getAosDuration == longestVisibility(sat1))

    //among sat1 with shared visibilities, at least one of them should select its longest visibility
    sat1Shared.exists( sat1 => chosenVisibilities(sat1).sortBy(v => -v.getAosDuration).head.getAosDuration == longestVisibility(sat1))
  }

  /**
    * 2 sat1 with longest pass and cardinality 1 for each sat1
    * first one has visibilities durations V1(10),V2(20),V3(25)
    * second one has visibilities durations V3(25) ,V4(20)
    * we expect either V3 or V4 chosen
    */
  @Test
  def longestPassWithTwoSat1_UC2(): Unit = {
    val station1 = mb.makeStation(Map("type1" -> (0,0)))
    val stations =  List(station1)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 10),
    )

    val sat1ConstraintA = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)), mostPass = false, longestPass = true)
    val sat1ConstraintB = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((70, 150)), mostPass = false, longestPass = true)
    val sat1s = List(sat1ConstraintA,sat1ConstraintB)
    val satellite = mb.makeSatellite(sat1s, stationProps, consecPass = true)

    val visibility1 = mb.makeVisibility(station1, satellite, 0, 10,offsetAosLos0 = 0) //A
    val visibility2 = mb.makeVisibility(station1, satellite, 40, 60,offsetAosLos0 = 0) //A
    val visibility3 = mb.makeVisibility(station1, satellite, 70, 95,offsetAosLos0 = 0)//A,B
    val visibility4 = mb.makeVisibility(station1, satellite, 100, 120,offsetAosLos0 = 0)//B
    val visibilities = List(visibility1, visibility2, visibility3, visibility4)
    val params = mb.makeParameters(150)

    visibilities.map{ v => s"${v.id} dur ${v.getAosDuration}"}.foreach(println)


    val model = makeModel(params, stations, List(satellite), visibilities)
    val problem = SatisfactionProblemCPO(model)
    val result = problem.buildSolve()
    result should equal (true)

    val longestVisibility : Map[Sat1Constraint, Int] = sat1s.map(sat1 => sat1 -> model.candidatesBySat1(sat1).sortBy(v => -v.getAosDuration).head.getAosDuration)(breakOut)
    val hasSharedVisi : Map[Sat1Constraint, Boolean] = sat1s.map{ case sat1 =>
      sat1 -> model.candidatesBySat1(sat1).exists( v => model.candidatesBySat1.filter(_._1.id != sat1.id).values.flatten.toSet.contains(v) )
    }(breakOut)

    val chosenVisibilities : Map[Sat1Constraint, Seq[Visibility]] = problem.satSolution.satelliteVisibilities.map{ case (sat1,visi) => sat1._2 -> visi}(breakOut)

    val (sat1Shared, sat1Unshared) = sat1s.partition( hasSharedVisi )

    //each sat1 with unshared visibility shall have its longest visibility chosen
    sat1Unshared.forall( sat1 => chosenVisibilities(sat1).sortBy(v => -v.getAosDuration).head.getAosDuration == longestVisibility(sat1))

    //among sat1 with shared visibilities, at least one of them should select its longest visibility
    sat1Shared.exists( sat1 => chosenVisibilities(sat1).sortBy(v => -v.getAosDuration).head.getAosDuration == longestVisibility(sat1))
  }

}
