package com.cnes.ocp.optim

import com.cnes.ocp.io.xml.XmlReader
import com.cnes.ocp.io.xml.objects.SpecificSchedule
import com.cnes.ocp.io.xml.objects.SpecificSchedule.{FIRST, LAST}
import com.cnes.ocp.model.{Model, ModelBuilder, Parameters}
import com.cnes.ocp.model.ModelBuilder.{makeModel, makeStationProperty}
import com.cnes.ocp.optim.components.SPVariables
import com.decisionbrain.cplex.cp.IntervalVar
import org.junit.Test
import org.scalatest.Matchers._
import org.scalatest.PrivateMethodTester._

class ObjectiveFunctionTest {
  val mb = new ModelBuilder

  @Test
  def priorityCoeffTest(): Unit = {
    val station1 = mb.makeStation()
    val station2 = mb.makeStation()
    val stations = List(station1, station2)

    val stationProps = Map(
      station1.id -> makeStationProperty(minDuration = 1, priority = 5),
      station2.id -> makeStationProperty(minDuration = 1, priority = 2))

    val sat1Constraint = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)))
    val satellite = mb.makeSatellite(List(sat1Constraint), stationProps)

    val visibility1 = mb.makeVisibility(station1, satellite, 20, 25)
    val visibility2 = mb.makeVisibility(station2, satellite, 40, 50)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2))
    val problem = SatisfactionProblemCPO(model)

    val makeVariables = PrivateMethod[SPVariables]('makeVariables) //This allows to test private method
    val variables  = problem invokePrivate makeVariables()

    val makePriorityCoefs = PrivateMethod[Map[IntervalVar, Int]]('makePriorityCoefs) //This allows to test private method
    val coefs  = problem invokePrivate makePriorityCoefs(variables)

    val intVar1 = variables.candidatesVisibilityVars(visibility1)._1
    val intVar2 = variables.candidatesVisibilityVars(visibility2)._1
    coefs(intVar1) should equal (5)
    coefs(intVar2) should equal (2)
  }

  @Test
  def specificScheduleTest(): Unit = {
    val station = mb.makeStation()
    val stations = List(station)

    val stationProps = Map(
      station.id -> makeStationProperty(minDuration = 1))

    val sat1Constraint1 = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)), specificSchedule = FIRST)
    val sat1Constraint2 = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)), specificSchedule = LAST)
    val satellite1 = mb.makeSatellite(List(sat1Constraint1), stationProps)
    val satellite2 = mb.makeSatellite(List(sat1Constraint2), stationProps)

    val visibility1 = mb.makeVisibility(station, satellite1, 20, 25)
    val visibility2 = mb.makeVisibility(station, satellite1, 25, 30)
    val visibility3 = mb.makeVisibility(station, satellite2, 40, 50)
    val visibility4 = mb.makeVisibility(station, satellite2, 50, 60)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite1, satellite2), List(visibility1, visibility2, visibility3, visibility4))
    val problem = SatisfactionProblemCPO(model)

    val makeVariables = PrivateMethod[SPVariables]('makeVariables) //This allows to test private method
    val variables  = problem invokePrivate makeVariables()

    val makeSpecificScheduleCoefs = PrivateMethod[Map[IntervalVar, Int]]('makeSpecificScheduleCoefs) //This allows to test private method
    val coefs  = problem invokePrivate makeSpecificScheduleCoefs(variables)

    val intVar1 = variables.candidatesVisibilityVars(visibility1)._1
    val intVar2 = variables.candidatesVisibilityVars(visibility2)._1
    val intVar3 = variables.candidatesVisibilityVars(visibility3)._1
    val intVar4 = variables.candidatesVisibilityVars(visibility4)._1
    coefs(intVar1) should equal (0)
    coefs(intVar2) should equal (1)
    coefs(intVar3) should equal (1)
    coefs(intVar4) should equal (0)
  }

  @Test
  def maxVisibilityCoeffTest(): Unit = {
    val station = mb.makeStation()
    val stations = List(station)

    val stationProps = Map(
      station.id -> makeStationProperty(minDuration = 1))

    val sat1Constraint = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)), mostPass = true)
    val satellite = mb.makeSatellite(List(sat1Constraint), stationProps)

    val visibility1 = mb.makeVisibility(station, satellite, 20, 25)
    val visibility2 = mb.makeVisibility(station, satellite, 25, 30)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2))
    val problem = SatisfactionProblemCPO(model)

    val makeVariables = PrivateMethod[SPVariables]('makeVariables) //This allows to test private method
    val variables  = problem invokePrivate makeVariables()

    val makeMaxVisibilityCoefs = PrivateMethod[Map[IntervalVar, Int]]('makeMaxVisibilityCoefs) //This allows to test private method
    val coefs  = problem invokePrivate makeMaxVisibilityCoefs(variables)

    val intVar1 = variables.candidatesVisibilityVars(visibility1)._1
    val intVar2 = variables.candidatesVisibilityVars(visibility2)._1
    coefs(intVar1) should equal (-1)
    coefs(intVar2) should equal (-1)
  }

  @Test
  def longestVisibilityCoeffTest(): Unit = {
    val station = mb.makeStation()
    val stations = List(station)

    val stationProps = Map(
      station.id -> makeStationProperty(minDuration = 1))

    val sat1Constraint = mb.makeSat1Constraint(stations.map(_.id), 1, 1, Seq((0, 100)), longestPass = true)
    val satellite = mb.makeSatellite(List(sat1Constraint), stationProps)

    val visibility1 = mb.makeVisibility(station, satellite, 20, 25)
    val visibility2 = mb.makeVisibility(station, satellite, 25, 35)
    val params = mb.makeParameters(60)

    val model = makeModel(params, stations, List(satellite), List(visibility1, visibility2))
    val problem = SatisfactionProblemCPO(model)

    val makeVariables = PrivateMethod[SPVariables]('makeVariables) //This allows to test private method
    val variables  = problem invokePrivate makeVariables()

    val makeLongestVisibilityCoefs = PrivateMethod[Map[IntervalVar, Int]]('makeLongestVisibilityCoefs) //This allows to test private method
    val coefs  = problem invokePrivate makeLongestVisibilityCoefs(variables)

    val intVar1 = variables.candidatesVisibilityVars(visibility1)._1
    val intVar2 = variables.candidatesVisibilityVars(visibility2)._1
    coefs(intVar1) should equal (1)
    coefs(intVar2) should equal (0)
  }

}
