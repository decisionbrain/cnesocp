package com.cnes.ocp.optim

import java.io.{ByteArrayOutputStream, FileOutputStream}
import java.time.ZonedDateTime

import com.cnes.ocp.model._
import com.cnes.ocp.optim.components._
import com.cnes.ocp.utils.Tracing
import ilog.cp.IloCP
import com.cnes.ocp.utils.DateUtils._
import ilog.concert.cppimpl.IloAlgorithm.Status

import scala.collection.breakOut

case class SatisfactionProblemCPO(override val modelObject: Model) extends OptimComponentTrait
  with VariableMaker with ConstraintMaker with ObjectiveMaker with Tracing {
  val nbWorkers: Int = -1
  var satSolution: Solution = _
  var conflict: ByteArrayOutputStream = _

  override def buildSolve(): Boolean = {
    val variables = makeVariables()
    makeConstraints(variables)
    makeObjective(variables)
    val success = solveInternal
    if(success)
      satSolution = saveSolution(variables)
    success
  }

  private def solveInternal: Boolean = {
    cp.setParameter(IloCP.DoubleParam.TimeLimit, modelObject.parameters.timeLimit)
    cp.setParameter(IloCP.IntParam.Workers, nbWorkers)

    if(modelObject.parameters.exportCpoModel){
      info("Exporting cpo model")
      exportModel("model.cpo")
    }

    info(s"nb interval variables: ${cp.getAllIloIntervalVars.length}")

    val success = solve()
    if(success) {
      info(s"Solution status: ${cp.getStatus}")
      info(s"CPLEX status: ${cp.getStatusString}")
      info(s"CPLEX Objective value and gap: ${cp.getObjValue} - ${cp.getObjGap}")
    } else if(cp.getStatus.equals(Status.Infeasible)) {
      info(s"No solution exists ! Conflict detected, refine conflict")
      if (cp.refineConflict()){
        conflict = new ByteArrayOutputStream()
        cp.exportConflict(conflict)
      } else
        warn("Unable to refine conflict")
    } else {
      info(s"No solution exists !")
    }

    success
  }

  private def saveSolution(variables: SPVariables): Solution = {
    implicit val _ = modelObject.parameters.start
    val satelliteSat1 = for(s <- modelObject.satellites; sat1 <- s.sat1Cstr) yield (s, sat1)
    val sta2s = for(s <- modelObject.stations; sta2 <- s.sta2Cstr) yield sta2

    val assignments: Map[(Satellite, Sat1Constraint), Seq[Visibility]] = satelliteSat1.map{ case (satellite, sat1) =>
      val usedV = modelObject.candidatesBySat1(sat1).filter{ v =>
        variables.candidatesVisibilityVars.get(v).exists{ case (satelliteVar, _) => isPresent(satelliteVar) } }
      (satellite, sat1) -> usedV
    }(breakOut)

    val maintenances: Map[Sta2Constraint, (ZonedDateTime, ZonedDateTime)] = sta2s.map{ sta2 =>
      val iloVar = variables.maintenanceVars(sta2)
      sta2 -> (getStart(iloVar).toZonedDateTime, getEnd(iloVar).toZonedDateTime)
    }(breakOut)

    Solution(assignments, maintenances)
  }

  def buildSolutionFromReservations : InputSolution = {
    val satelliteSat1 = for(s <- modelObject.satellites; sat1 <- s.sat1Cstr) yield (s, sat1)
    val assignments: Map[(Satellite, Sat1Constraint), Seq[Visibility]] = satelliteSat1.map{ case (satellite, sat1) =>
      val usedV = modelObject.reservationsBySat1(sat1)
      (satellite, sat1) -> usedV
    }(breakOut)

    val allReservedMaintenances : Map[Station, Seq[(ZonedDateTime, ZonedDateTime)]] = modelObject.stations.map(sta => (sta->sta.reservedMaintenances)) (breakOut)

    InputSolution(assignments, allReservedMaintenances)
  }

}
