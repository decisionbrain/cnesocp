package com.cnes.ocp.optim.components

import java.time.ZonedDateTime

import com.cnes.ocp.model.{Sta2Constraint, Visibility}
import com.cnes.ocp.utils.Tracing
import com.decisionbrain.cplex.cp.IntervalVar
import com.cnes.ocp.utils.DateUtils._

import scala.collection.{breakOut, mutable}

trait VariableMaker extends Tracing {
  this: OptimComponentTrait =>

  protected def makeVariables(): SPVariables = {
    val candidatesBySat1 = for(s <- modelObject.satellites; sat <- s.sat1Cstr) yield modelObject.candidatesBySat1(sat)
    val reservations = modelObject.reservations
    val extraPasses = modelObject.extraPasses
    val sta2s = for(s <- modelObject.stations; sat2 <- s.sta2Cstr) yield sat2

    val visiForAlpha = for(s <- modelObject.satellites; sat <- s.sat1Cstr if !s.consecutivePass)
      yield modelObject.candidatesBySat1(sat)
    val resaAndExtraPassForAlpha = (reservations ++ extraPasses).filter(!_.satellite.consecutivePass)

    SPVariables(
      traceTimeAndCount("make candidate vars")     (makeCandidateVars(candidatesBySat1)),
      traceTimeAndCount("make reservation vars")   (makeReservationVars(reservations)),
      traceTimeAndCount("make extra pass vars")    (makeExtraPassVars(extraPasses)),
      traceTimeAndCount("make maintenance vars")   (makeMaintenanceVars(sta2s)),
      traceTimeAndCount("make alpha vars")         (makeAlphaVars(visiForAlpha, resaAndExtraPassForAlpha))
    )
  }

  /**
    * building decision variables reading by satellite, then the list of all of its sat1 visibilities (skiping reservation and extra pass)
    * @param sat1s sequence for each satellite of the aggregated sequence of all visibilities refered by all sat1 of this satellite
    * @return for each visibility the pair of interval variables from satellite and station stand point
    */
  private def makeCandidateVars(sat1s: Seq[Seq[Visibility]]): Map[Visibility, (IntervalVar, IntervalVar)] = {
    val result = mutable.Set[String]()
    sat1s.flatMap{ sat1 =>
      sat1.collect{ case v if result.add(v.id) =>
        val (conf, deconf) = v.getConfDeconfTimes
        val stationVar    = makeVar(s"${v.id}-sta", v.aos0.start.toInt - conf, v.aos0.end.toInt + deconf, mandatory = false)
        val satelliteVar  = makeVar(s"${v.id}-sat", v.aos.start.toInt, v.aos.end.toInt, mandatory = false)
        v -> (satelliteVar, stationVar)
      }
    }(breakOut)
  }

  private def makeReservationVars(reservations: Seq[Visibility]): Map[Visibility, (IntervalVar, IntervalVar)] =
    reservations.map{ r =>
      val (conf, deconf) = r.getConfDeconfTimes
      val stationVar    = makeVar(s"${r.id}-sta", r.aos0.start.toInt - conf, r.aos0.end.toInt + deconf, mandatory = true)
      val satelliteVar  = makeVar(s"${r.id}-sat", r.aos.start.toInt, r.aos.end.toInt, mandatory = true)
      r -> (satelliteVar, stationVar)
    }(breakOut)

  private def makeExtraPassVars(extraPass: Seq[Visibility]): Map[Visibility, IntervalVar] =
    extraPass.map{ r =>
      val (conf, deconf) = r.getConfDeconfTimes
      val extraPassVar = makeVar(s"${r.id}-sta", r.aos0.start.toInt - conf, r.aos0.end.toInt + deconf, mandatory = true)
      r -> extraPassVar
    }(breakOut)

  private def makeMaintenanceVars(sta2s: Seq[Sta2Constraint]): Map[Sta2Constraint, IntervalVar] =
    sta2s.map{ sta2 => sta2 -> makeVar(sta2.id, sta2.minDuration, sta2.windows) }(breakOut)

  private def makeAlphaVars(sat1s: Seq[Seq[Visibility]], reservationsAndExtraPass: Seq[Visibility])
  : Map[Visibility, IntervalVar] = {
    val res1: Map[Visibility, IntervalVar] = reservationsAndExtraPass.map{ r =>
      r -> makeVar(s"${r.id}-sta", r.aos0.start.toInt, r.aos0.start.toInt + 1, mandatory = true)
    }(breakOut)

    val result = mutable.Set[Visibility]()
    val res2: Map[Visibility, IntervalVar] = sat1s.flatMap{ sat1 =>
      sat1.collect{ case v if result.add(v) =>
        val alphaVar = makeVar(s"${v.id}-alph", v.aos0.start.toInt, v.aos0.start.toInt + 1, mandatory = false)
        v -> alphaVar
      }
    }(breakOut)
    res1 ++ res2
  }

  private def makeVar(name: String, startMin: Int, endMax: Int, mandatory: Boolean): IntervalVar = {
    val sz = endMax - startMin
    val myVar = intervalVar(sz, name)
    myVar.setStartMin(startMin)
    myVar.setStartMax(startMin)
    myVar.setEndMax(endMax)
    myVar.setEndMin(endMax)
    if(mandatory) myVar.setPresent() else myVar.setOptional()
    myVar
  }

  /**
    * maintenance var creation
    * @param size fixed duration corresponding to the Sta2 min duration
    * @param window list of possible areas, one of them being chosen to assign the maintenance interval var (note that this will be handled
    *               by the specific constraint Sat2 instanciation, while here we simply bound the interval with the earliest area start and latest area end)
    */
  private def makeVar(name: String, size: Int, window: Seq[(ZonedDateTime, ZonedDateTime)]): IntervalVar = {
    val startMin = window.minBy(_._1.toInt)._1.toInt
    val endMax = window.maxBy(_._2.toInt)._2.toInt
    val myVar = intervalVar(size, name)
    myVar.setPresent()
    myVar.setStartMin(startMin)
    myVar.setEndMax(endMax)
    myVar
  }
}
