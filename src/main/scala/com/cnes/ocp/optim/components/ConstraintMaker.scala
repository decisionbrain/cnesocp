package com.cnes.ocp.optim.components

import com.cnes.ocp.io.xml.objects.ConstraintType
import com.cnes.ocp.model._
import com.cnes.ocp.utils.Tracing
import com.decisionbrain.cplex._
import com.cnes.ocp.utils.DateUtils._
import com.decisionbrain.cplex.cp.IntervalVar

import scala.collection.breakOut

trait ConstraintMaker extends Tracing {
  this: OptimComponentTrait =>

  protected def makeConstraints(implicit variables: SPVariables): Unit = {
    val sat1s = modelObject.satellites.flatMap(_.sat1Cstr)

    val sta2ToStation: Map[Sta2Constraint, Station] =
      modelObject.stations.flatMap{ station => station.sta2Cstr.map{ sta2 => sta2 -> station} }(breakOut)

    val extraPassesBySatellite = modelObject.extraPasses.filter(!_.satellite.consecutivePass).groupBy(_.satellite)
    val satelliteToAllVisi: Map[Satellite, Seq[Visibility]] =
      modelObject.satellites.collect{ case satellite if !satellite.consecutivePass =>
        val allVisibilities = satellite.sat1Cstr.flatMap{ sat1 =>
          modelObject.reservationsBySat1(sat1) ++ modelObject.candidatesBySat1(sat1)
        }.distinct //we merge visibilities since they can be shared by several sat1 constraints
        satellite -> (allVisibilities ++ extraPassesBySatellite.getOrElse(satellite, Nil))
      }(breakOut)

    traceAndCount("sat1 constraints")                   (makeSat1Constraints(sat1s))
    traceAndCount("link satellite/station visi vars")   (makeSatelliteStationConstraints)
    traceAndCount("station no overlap constraints")     (makeStationNoOverlap)
    traceAndCount("sta2 constraints")                   (makeSta2Constraints(sta2ToStation))
    traceAndCount("non consecutivity constraints")      (makeNonConsecutivityConstraints(satelliteToAllVisi))
  }

  private def makeSat1Constraints(sat1s: Seq[Sat1Constraint])(implicit variables: SPVariables): Array[Addable] =
    sat1s.flatMap{ sat1 =>
      val nbResa = modelObject.reservationsBySat1(sat1).size
      val candidateVisibilities = modelObject.candidatesBySat1(sat1)

      if(sat1.minPass - nbResa > candidateVisibilities.size && modelObject.parameters.removeFailingAtomicConstraints){
        val w = sat1.windows.map{ case (s, e) => s"$s->$e"}.mkString(", ")
        warn(s"Removing infeasible sat1: $sat1 : $w")
        Nil
      } else {
        if(sat1.minPass - nbResa > candidateVisibilities.size){
          val w = sat1.windows.map{ case (s, e) => s"$s->$e"}.mkString(", ")
          warn(s"Infeasible sat1: $sat1 : $w")
        }
        val expr = sum(
          candidateVisibilities.map{ v => presenceOf(variables.candidatesVisibilityVars(v)._1) }
        )

        val constraint1 = (expr >= sat1.minPass - nbResa) setName s"${sat1.id}-min"
        val constraint2 = (expr <= sat1.maxPass - nbResa) setName s"${sat1.id}-max"
        List(constraint1, constraint2)
      }
    }(breakOut)

  /**
    * equality between station and satellite presence
    */
  private def makeSatelliteStationConstraints(implicit variables: SPVariables): Array[Addable] =
    variables.candidatesVisibilityVars.map { case (v, (satelliteVar, stationVar)) =>
      (presenceOf(satelliteVar) == presenceOf(stationVar)) setName s"samePresence_${v.id}"
    }(breakOut)

  private def makeStationNoOverlap(implicit variables: SPVariables): Array[Addable] = {
    val candidates = variables.candidatesVisibilityVars.groupBy(_._1.station).map{ case (station, visiToVar) =>
      station -> visiToVar.map{ case (_, (_, stationVar)) => stationVar }.toSeq
    }
    val reservations = variables.reservationVars.groupBy(_._1.station).map{ case (station, visiToVar) =>
      station -> visiToVar.map{ case (_, (_, reservVar)) => reservVar }.toSeq
    }
    val extraPasses = variables.extraPassVars.groupBy(_._1.station).map{ case (station, visiToVar) =>
      station -> visiToVar.map{ case (_, extraPassVar) => extraPassVar }.toSeq
    }

    modelObject.stations.map{ station =>
      val maintenanceVars = station.sta2Cstr.map{ sta2 => variables.maintenanceVars(sta2) }
      val myVars = candidates.getOrElse(station, Nil) ++
        reservations.getOrElse(station, Nil) ++
        extraPasses.getOrElse(station, Nil) ++
        maintenanceVars
      val ctr = noOverlap(myVars.toArray)
      ctr.setName(s"noOverlap_Station_${station.id}")
      ctr
    }(breakOut)
  }

  /**
    * each maintenance interval is associated to an instance of sat2 on a particular window (day)
    * we forbid the maintenance interval to overlap any time window oustide of the window areas
    */
  private def makeSta2Constraints(sta2ToStation: Map[Sta2Constraint, Station])
                                 (implicit variables: SPVariables): Array[Addable] =
    variables.maintenanceVars.flatMap{ case (sta2, iloVar) =>
      val available = numToNumStepFunction()
      sta2.windows.foreach{ w => available.addValue(w._1.toDouble, w._2.toDouble, 1d) }
      sta2ToStation(sta2).reservedMaintenances.foreach{ case (s, e) => available.addValue(s.toDouble, e.toDouble, 0d) }

      if(sta2.windows.isEmpty && modelObject.parameters.removeFailingAtomicConstraints) {
        warn(s"Removing infeasible Sta2: ${sta2.id}")
        None
      } else {
        if(sta2.windows.isEmpty)
          warn(s"Infeasible Sta2: ${sta2.id}")
        Some(forbidExtent(iloVar, available))
      }
    }(breakOut)


  private def makeNonConsecutivityConstraints(satelliteToAllVisi: Map[Satellite, Seq[Visibility]])
                                             (implicit variables: SPVariables): Array[Addable] =
    satelliteToAllVisi.flatMap{ case (satellite, visibilities) =>
      //Associate alphaVars and intervalVars
      val varsForSatellite = visibilities.map{ v =>
        val intervalVar =
          if(variables.candidatesVisibilityVars.contains(v)) variables.candidatesVisibilityVars(v)._2
          else if (variables.reservationVars.contains(v))    variables.reservationVars(v)._2
          else                                               variables.extraPassVars(v)

        val alphaVar = variables.alphaVars(v)
        (v, alphaVar, intervalVar)
      }

      // Synchronize alphaVars and intervalVars
      val synchronizationConstraints = varsForSatellite.map{ case (visibility, alphaVar, intervalVar) =>
        (presenceOf(alphaVar) == presenceOf(intervalVar)) setName s"a-presence_${visibility.id}"
      }

      // Associate a type to a station
      val stationToType: Map[Station, Int] =
        visibilities.map(_.station).toSet.zipWithIndex.map{ case (station, i) => station -> i }(breakOut)

      // noOverlap with transition matrix
      val myVars: Array[IntervalVar] = varsForSatellite.map{ case (_, alphaVar, _) => alphaVar }(breakOut)
      val myTypes: Array[Int] = varsForSatellite.map{ case (visibility, _, _) => stationToType(visibility.station) }(breakOut)
      val sequence = intervalSequenceVar(myVars, myTypes)
      val transitions = transitionDistance(stationToType.size, s"${satellite.id}_matrix")
      stationToType.foreach{ case (_, myType) => transitions.setValue(myType, myType, Int.MaxValue) }
      noOverlap(sequence, transitions, direct = true) +: synchronizationConstraints
    }(breakOut)

  private def traceAndCount(name: String)(constraints: Array[Addable]): Unit = add(traceTimeAndCount(name, constraints))
}
