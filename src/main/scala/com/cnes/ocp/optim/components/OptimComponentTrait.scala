package com.cnes.ocp.optim.components

import com.cnes.ocp.model.Model
import com.decisionbrain.cplex.cp.CpModel

trait OptimComponentTrait extends CpModel {
  implicit def modelObject: Model
  def buildSolve(): Boolean
}
