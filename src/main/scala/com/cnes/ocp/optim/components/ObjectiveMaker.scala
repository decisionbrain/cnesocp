package com.cnes.ocp.optim.components


import com.cnes.ocp.utils.Tracing
import com.cnes.ocp.io.xml.objects.SpecificSchedule.{FIRST, INDIFFERENT}
import com.decisionbrain.cplex.cp.IntervalVar

trait ObjectiveMaker extends Tracing {
  this: OptimComponentTrait =>

  def makeObjective(variables: SPVariables): Unit = {
    val coefficients1 = traceTime("make priority obj")            (makePriorityCoefs(variables))
    val coefficients2 = traceTime("make specific schedule obj")   (makeSpecificScheduleCoefs(variables))
    val coefficients3 = traceTime("make max visibility obj")      (makeMaxVisibilityCoefs(variables))
    val coefficients4 = traceTime("make longest visibility obj")  (makeLongestVisibilityCoefs(variables))

    traceTime("make and add objective"){
      val obj = sum(
       (coefficients1 ++ coefficients2 ++ coefficients3 ++ coefficients4).map{ case (myVar, coef) =>
         presenceOf(myVar) * coef }
      )
      add(minimize(obj) setName "objective")
    }
  }

  protected def makePriorityCoefs(variables: SPVariables): Map[IntervalVar, Int] =
    modelObject.candidatesBySat1.foldLeft(Map[IntervalVar, Int]()){ case (acc, (_, visibilities)) =>
      acc ++ visibilities.map{ v =>
        variables.candidatesVisibilityVars(v)._1 -> v.satellite.stationProperties(v.station.id).priority
      }
    }

  protected def makeSpecificScheduleCoefs(variables: SPVariables): Map[IntervalVar, Int] =
    modelObject.candidatesBySat1.foldLeft(Map[IntervalVar, Int]()){
      case (acc, (sat1, visibilities)) if sat1.specificSchedule != INDIFFERENT =>
        val orderedCoeffs =
          if(sat1.specificSchedule == FIRST)  visibilities.sortBy(_.aos0.start.toEpochSecond)
          else                                visibilities.sortBy(-_.aos0.end.toEpochSecond)

        val newCoeffs = orderedCoeffs.zipWithIndex.map{ case (v, index) =>
          val currentCoeff = acc.getOrElse(variables.candidatesVisibilityVars(v)._1, Integer.MAX_VALUE)
          if(index < currentCoeff)  variables.candidatesVisibilityVars(v)._1 -> index
          else                      variables.candidatesVisibilityVars(v)._1 -> currentCoeff
        }
        acc ++ newCoeffs
      case (acc, _) => acc
    }

  protected def makeMaxVisibilityCoefs(variables: SPVariables): Map[IntervalVar, Int] =
    modelObject.candidatesBySat1.foldLeft(Map[IntervalVar, Int]()){
      case (acc, (sat1, visibilities)) if sat1.mostPass =>
        acc ++ visibilities.map{ v => variables.candidatesVisibilityVars(v)._1 -> -1 }
      case (acc, _) => acc
    }

  protected def makeLongestVisibilityCoefs(variables: SPVariables): Map[IntervalVar, Int] =
    modelObject.candidatesBySat1.foldLeft(Map[IntervalVar, Int]()){
      case (acc, (sat1, visibilities)) if sat1.longestPass =>
        val newCoeffs = visibilities.sortBy(-_.getAosDuration).zipWithIndex.map{ case (v, index) =>
          val itvVar = variables.candidatesVisibilityVars(v)._1
          val currentCoeff = acc.getOrElse(itvVar, Integer.MAX_VALUE)
          if(index < currentCoeff)  itvVar -> index
          else                      itvVar -> currentCoeff
        }
        acc ++ newCoeffs
      case (acc, _) => acc
    }

}
