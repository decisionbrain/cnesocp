package com.cnes.ocp.optim.components

import com.cnes.ocp.model.{Sta2Constraint, Visibility}
import com.decisionbrain.cplex.cp.IntervalVar

case class SPVariables(candidatesVisibilityVars : Map[Visibility, (IntervalVar, IntervalVar)], // candidate -> (satelliteVar, stationVar)
                       reservationVars          : Map[Visibility, (IntervalVar, IntervalVar)], // reservation -> (satelliteVar, stationVar)
                       extraPassVars            : Map[Visibility, IntervalVar], // extraPass -> stationVar
                       maintenanceVars          : Map[Sta2Constraint, IntervalVar],
                       alphaVars                : Map[Visibility, IntervalVar] // based on stationsVar
                  )
