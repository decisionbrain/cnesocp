package com.cnes.ocp.model

import java.time.{ZoneId, ZonedDateTime}
import java.time.temporal.ChronoUnit

import com.cnes.ocp.io.xml.objects.Band.Band
import com.cnes.ocp.io.xml.objects._
import com.cnes.ocp.utils.DateUtils.{generateSta2Intervals, intersects, _}
import com.cnes.ocp.utils.{DateUtils, Tracing}

import scala.collection.breakOut

trait FromXml extends Tracing {

  def fromXml(xmlData: XmlModel, xmlParams: XmlParams, removeFailingAtomicConstraints: Boolean = true, exportCpoModel : Boolean = false): Model = {
    val params = traceTime("Building params"){ buildParameters(xmlParams, removeFailingAtomicConstraints, exportCpoModel) }
    traceTime("Building object model"){ build(xmlData, params) }
  }

  //private[model] to allow testing
  private[model] def buildParameters(xmlParams: XmlParams, removeFailingAtomicConstraints: Boolean, exportCpoModel : Boolean): Parameters = {
    val usedSatellites = xmlParams.satellites.collect{ case xmlSatellite if xmlSatellite.used => xmlSatellite.satelliteId }
    val usedStations = xmlParams.stations.collect{ case xmlStation if xmlStation.used => xmlStation.satelliteId}

    val start = xmlParams.period.start.withZoneSameInstant(ZoneId.of("UTC"))
    val end   = xmlParams.period.end.withZoneSameInstant(ZoneId.of("UTC"))
    val nbDays = ChronoUnit.DAYS.between(start, end).toInt
    info(s"$nbDays days in horizon")
    info(s"Horizon: $start -> $end")

    Parameters(
      start, end, xmlParams.timeLimit, usedSatellites,
      usedStations, xmlParams.constraintTypes.map(_.constraintType),
      removeFailingAtomicConstraints,exportCpoModel
    )
  }

  private def debug(xmlModel: XmlModel): Unit = {
    val plage1 = ( ZonedDateTime.parse("2018-09-13T00:15+01:00") , ZonedDateTime.parse("2018-09-13T04:00+01:00"))
    val plages = List(plage1)

    val slotsById = xmlModel.satelliteSlots.groupBy(_.visibilityId)
    val visibilitiesAndState = xmlModel.visibilities.toList.map{ xmlVisi => (xmlVisi, slotsById(xmlVisi.id).head) }

    visibilitiesAndState.filter{ case(v,s) =>
      v.satelliteId =="Satellite_673711" &&
        // utils.DateUtils.intersects((v.aosLos.period.start, v.aosLos.period.end), (params.start, params.end)) &&
        plages.exists( pl => (v.aosLos.period.start, v.aosLos.period.end).fitInside(pl)) &&
        v.aosLos.period.start.getDayOfMonth == 5
      /*&&
      (v.aosLos.period.start.getDayOfMonth == 19 || v.aosLos.period.start.getDayOfMonth == 20) &&
        (v.aosLos.period.start.getHour >= 16 || v.aosLos.period.start.getHour < 1 )
       &&  ChronoUnit.SECONDS.between(v.aosLos.period.start, v.aosLos.period.end) >=400 */
    }.filter{ case(v,w) =>
      val aosOption = v.otherAosLos.find(_.elevation == 5.0)
      aosOption match{
        case Some(aos) => ChronoUnit.SECONDS.between(aos.period.start, aos.period.end) >=400
        case _ => false
      }

    }.foreach{ case (v,s) =>
      //println(v.otherAosLos.find(_.elevation == 5.0).get.period)
      println(s"${v.id} ${s.state} ${v.otherAosLos.find(_.elevation == 5.0).get.period} ")
    }
  }

  //private[model] to allow testing
  private[model] def build(xmlModel: XmlModel, params: Parameters): Model = {
    implicit val _ = params

    //debug(xmlModel)


    // Build a list of ids for:
    //      used satellites or related to used station
    //      used stations   or related to used satellites
    val (vSatelliteId, vStationId) = xmlModel.visibilities
      .withFilter{ v => params.usedSatellites.contains(v.satelliteId) || params.usedStations.contains(v.stationId)}
      .map{ v => (v.satelliteId, v.stationId) }.unzip
    val allSatelliteId = params.usedSatellites.toSet ++ vSatelliteId.toSet
    val allStationId = params.usedStations.toSet ++ vStationId.toSet

    // -------- Build stations and satellites
    val (primarySatellites, secondarySatellites) = xmlModel.satellites
      .filter{ s => allSatelliteId.contains(s.id) }
      .partition{ s => params.usedSatellites.contains(s.id) && s.validity.headOption.forall(v => intersects(v, params)) }

    val (primaryStations, secondaryStations) = xmlModel.stations
      .filter{ s => allStationId.contains(s.id) }
      .partition{ s => params.usedStations.contains(s.id) && s.validity.headOption.forall(v => intersects(v, params)) }

    val stationSlotsByStationId = xmlModel.stationSlots.groupBy(_.stationId)
    val stationById    = buildStations(primaryStations, stationSlotsByStationId)
    val satellitesById = buildSatellites(primarySatellites, true)
    val allStationById    = buildStations(secondaryStations, stationSlotsByStationId) ++ stationById
    val allSatellitesById = buildSatellites(secondarySatellites, false) ++ satellitesById

    // ----------- Build visibilites: candidates and reservations
    val slotsById = xmlModel.satelliteSlots.groupBy(_.visibilityId)
    val visibilitiesAndState = xmlModel.visibilities.toList.map{ xmlVisi => (xmlVisi, slotsById(xmlVisi.id).head) }

    val primaryStationIds   = primaryStations.map(_.id)
    val primarySatelliteIds = primarySatellites.map(_.id)

    val (primaryVisibilities, secondaryVisibilities) = visibilitiesAndState.partition{ case (v, _) =>
      primarySatelliteIds.contains(v.satelliteId) && primaryStationIds.contains(v.stationId) }

    val (avail, reservs) = primaryVisibilities.partition(_._2.state == Status.AVAILABLE)
    val availStartInsideHorizon = avail.filter{ v => v._1.aosLos.period.start.fitInside(params.start, params.end) }
    val candidates       = buildVisibilities(availStartInsideHorizon, satellitesById, stationById)

    val (xmlPrimaryExtraPass, xmlPrimaryReservations) = reservs.partition(_._2.extraPass)
    val reservations  = buildVisibilities(xmlPrimaryReservations, satellitesById, stationById)
    val extraPass     = buildVisibilities(xmlPrimaryExtraPass, satellitesById, stationById)

    val (xmlSecondaryExtraPass, xmlSecondaryReservations) = secondaryVisibilities.filter(_._2.state != Status.AVAILABLE).partition( _._2.extraPass)
    val secondaryReservations = buildVisibilities(xmlSecondaryReservations, allSatellitesById, allStationById)
    val secondaryExtraPass    = buildVisibilities(xmlSecondaryExtraPass, allSatellitesById, allStationById)

    if(stationById.values.size != params.usedStations.size)
      warn(s"Imported less stations than expected, missing: ${params.usedStations.diff(stationById.keySet.toSeq).mkString(", ")}")
    if(satellitesById.values.size != params.usedSatellites.size)
      warn(s"Imported less satellites than expected, missing: ${params.usedSatellites.diff(satellitesById.keySet.toSeq).mkString(", ")}")

    // Note: In the model, some availability visibilities might not be associated with any Sat1
    // Beware: extra reservations refers either to a satellite or station which will not be in the model
    Model(params,
      stationById.values.toList,
      satellitesById.values.toList,
      candidates,
      reservations ++ secondaryReservations,
      extraPass ++ secondaryExtraPass)
  }

  private def buildStations(xmlStations             : Seq[XmlStation],
                            stationSlotsByStationId : Map[String, Seq[XmlStationSlot]])
                           (implicit params: Parameters): Map[String, Station] =
    xmlStations.map{ xmlStation =>
      val needSta2 = params.constraintTypes.contains(ConstraintType.CStaType2)
      val sta2s = if(needSta2) buildSta2Constraints(xmlStation, params) else Seq()

      val satProps: Map[String, (Int, Int)] = xmlStation.satelliteProperties.map{
        case XmlSatelliteProperties(typeId, reconf, deconf) =>
          typeId -> (reconf.map(_.toInt*60).getOrElse(0), deconf.map(_.toInt*60).getOrElse(0))
      }(breakOut)

      val reservedMaintenances = stationSlotsByStationId.getOrElse(xmlStation.id, Nil).map{ xmlStationSlot =>
        (xmlStationSlot.period.start, xmlStationSlot.period.end)
      }
      val toSatisfySta2 = sta2s.filterNot{ sta2 => reservedMaintenances.exists{ rm => sta2.isSatisfiedBy(rm) } }

      xmlStation.id -> Station(xmlStation.id, xmlStation.name, satProps, toSatisfySta2, reservedMaintenances)
    }(breakOut)

  private def buildSatellites(xmlSatellites: Seq[XmlSatellite], isPrimary: Boolean)
                             (implicit params: Parameters): Map[String, Satellite] =
    xmlSatellites.map{ xmlSatellite =>
      // Sat4 Infos
      val (sat4Band, consecPass, stationProperties) = buildSat4Infos(xmlSatellite.sat4, isPrimary, params)

      // Create Sat1 constraints
      val needSat1 = params.constraintTypes.contains(ConstraintType.CSatType1)
      val sat1Cstrs =
        if (needSat1)
          xmlSatellite.sat1
            .withFilter( sat1 => intersects(sat1.validity, params) && sat1.typeSet == SatCstrType.NOMINAL )
            .flatMap{ sat1  => //TODO take into account SAT4 band. Lot 2
              buildSat1Constraints(sat1, xmlSatellite.id, params.start, params.end, stationProperties.keySet.toSeq)
            }
        else Seq()

      xmlSatellite.id -> Satellite(xmlSatellite.id, xmlSatellite.name, xmlSatellite.band, sat4Band,
        xmlSatellite.typeId, sat1Cstrs, consecPass, xmlSatellite.allowJamming, xmlSatellite.reservePhysicVisibilityHole,xmlSatellite.reserveRFVisibilityHole, stationProperties)
    }(breakOut)


  private def buildVisibilities(xmlVisibilities: List[(XmlVisibility, XmlSatelliteSlot)],
                                satelliteById: Map[String, Satellite],
                                stationById: Map[String, Station])
                               (implicit params: Parameters): List[Visibility] =
    xmlVisibilities.flatMap{ case (xmlVisibility, xmlSatelliteSlot) =>
      val (station, satellite) =  (stationById(xmlVisibility.stationId), satelliteById(xmlVisibility.satelliteId))
      if(satellite.stationProperties.contains(station.id)){
        val aos0 =  AosLos(xmlVisibility.aosLos.elevation,
          xmlVisibility.aosLos.period.start,
          xmlVisibility.aosLos.period.end)

        val aosLosPotential = xmlVisibility.otherAosLos.filter{ case XmlAosLos(elevation, _) =>
          // If getMinElevation doesn't have a property for the station, we discard the visibility
          try{ elevation >= satellite.getMinElevation(station.id) } catch {
            case _: Throwable if xmlSatelliteSlot.state == Status.AVAILABLE => false
            case _: Throwable => throw new NullPointerException("Something unexpected happened while importing visibility")
          }
        }
        if(aosLosPotential.isEmpty && xmlSatelliteSlot.extraPass)
          Some(Visibility(xmlVisibility.id, xmlVisibility.band, station, satellite,
            xmlSatelliteSlot.state, xmlVisibility.holeDuration.toInt,xmlVisibility.jammed, xmlVisibility.phyHole,xmlVisibility.rfHole, aos0, null))
        else if(aosLosPotential.isEmpty)
          None // no correct elevation, no need to keep it
        else {
          val xmlAos = aosLosPotential.minBy(_.elevation)
          val aos = AosLos(xmlAos.elevation, xmlAos.period.start, xmlAos.period.end)
          Some(Visibility(xmlVisibility.id, xmlVisibility.band, station, satellite,
            xmlSatelliteSlot.state, xmlVisibility.holeDuration.toInt,xmlVisibility.jammed, xmlVisibility.phyHole,xmlVisibility.rfHole, aos0, aos))
        }
      } else{
        None
      }
    }

  private def buildSta2Constraints(xmlStation: XmlStation, params: Parameters): Seq[Sta2Constraint] =
    xmlStation.sta2.withFilter(sta2 => sta2.typeSet == SatCstrType.NOMINAL).flatMap { sta2 =>
      val duration = sta2.minDuration * 60

      val workingPeriodsNew = xmlStation.workingPeriods.collect { case xmlWorkPeriod
        if xmlWorkPeriod.day.fitInside(params.start, params.end) && xmlWorkPeriod.day.fitInside(sta2.validity.start, sta2.validity.end) =>
        xmlWorkPeriod.workingPeriods.map { wp => (wp.start, wp.end) }
      }

      workingPeriodsNew.zipWithIndex.map{ case (dailyWorkingPeriods, index) =>
        val intervals = generateSta2Intervals(duration, (sta2.start, sta2.end), dailyWorkingPeriods)
        Sta2Constraint(s"${sta2.id}-$index", duration, intervals)
      }
    }

  /**
    * from the slot(s) local time defined in xml, we can expand them in corresponding ZonedDateTime for each period of the horizon
    */
  private def buildSat1Constraints(xmlSat1: XmlSat1Constraint,
                                   satelliteId : String,
                                   horizonStart: ZonedDateTime,
                                   horizonEnd: ZonedDateTime,
                                   stationIds: Seq[String])
  : List[Sat1Constraint] = {
    val days = xmlSat1.weekDays.map(_.weekDay)
    val stations = xmlSat1.stations.map(_.split(" ").toSeq.filter(stationIds.contains)).getOrElse(stationIds)

    //build a sequence of sequence of intervals. The first level corresponds to the period of instanciation of the Sat1, either one day or one week
    //the second level contains for this range all the HH:MM set of periods included in the day(s) of this range
    val periodOption = xmlSat1.firstApplication match {
      case Some(zonedDateTime) if xmlSat1.period > 0  => Some(xmlSat1.period, zonedDateTime)
      case _                                          => None
    }

    val validityStart = xmlSat1.validity.start.withZoneSameInstant(ZoneId.of("UTC"))
    val validityEnd   = xmlSat1.validity.end.withZoneSameInstant(ZoneId.of("UTC"))
    val (start, end) = intersection((validityStart, validityEnd), (horizonStart, horizonEnd))
    val localWindows = xmlSat1.slots.map{case XmlSlot(s, e) => (s, e)}
    val intervals = DateUtils.generateSat1Intervals(localWindows, days, periodOption, start, end)

    intervals.zipWithIndex.map{ case (intervalSeq, index)=>
      Sat1Constraint(s"${xmlSat1.id}-$index-$satelliteId",
        xmlSat1.band, xmlSat1.specificSchedule, xmlSat1.minDuration,
        xmlSat1.minPass, xmlSat1.maxPass, xmlSat1.mostPass, xmlSat1.longestPass,
        stations, intervalSeq
      )
    }(breakOut)
  }

  private def buildSat4Infos(xmlSat4s: Seq[XmlSat4Constraint], isPrimary: Boolean, params: Parameters)
  : (Band, Boolean, Map[String, StationProperties]) = {
    val sat4 =
      if(isPrimary) xmlSat4s.find(sat4 => sat4.typeSet == SatCstrType.NOMINAL && intersects(sat4.validity, params)).get
      else          xmlSat4s.find(sat4 => sat4.typeSet == SatCstrType.NOMINAL).get

    val stationProps: Map[String, StationProperties] = sat4.stationProperties.map{ prop =>
      prop.stationId -> StationProperties(prop.minDuration, prop.minElevation, prop.priority)
    }(breakOut)
    (sat4.band, sat4.consecutivePass, stationProps)
  }

}
