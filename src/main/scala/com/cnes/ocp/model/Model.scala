package com.cnes.ocp.model

import com.cnes.ocp.io.xml.objects.Band
import com.cnes.ocp.utils.DateUtils._

import scala.collection.breakOut

case class Model(
  parameters  : Parameters,
  stations    : List[Station],
  satellites  : List[Satellite],
  visibilities: List[Visibility],
  reservations: List[Visibility],
  extraPasses : List[Visibility]
) {
  val candidatesBySat1  : Map[Sat1Constraint, Seq[Visibility]] = computeVisibilitiesBySat1(visibilities)
  val reservationsBySat1: Map[Sat1Constraint, Seq[Visibility]] = computeVisibilitiesBySat1(reservations)

  /**
    * iterates over all visibilities and keep the candidates for SAT1 according to the criteria
    * <ul>
    *   <li>same satellite</li>
    *   <li>station candidates</li>
    *   <li>aos included in one of the areas of the sat1</li>
    *   <li>start of aos after origin</li>
    * </ul>
    */
  def computeVisibilitiesBySat1(visibilities: Seq[Visibility]): Map[Sat1Constraint, Seq[Visibility]] = {
    val hoversBySatellite = visibilities.groupBy(_.satellite.id)
    val satellitesSat1 = for(s <- satellites; sat <- s.sat1Cstr) yield (s, sat)

    satellitesSat1.map{ case (satellite, sat1) =>
      val sat1Visibilities = hoversBySatellite.getOrElse(satellite.id, Nil).filter{ v =>
        val stationId   = v.station.id
        val duration    = v.getAosDuration
        val minDuration = satellite.getMinDuration(sat1, stationId)

        // TODO Why do I have bands everywhere (Satellite+sat1+sat4) (visibility + station) ? Lot 2
        Band.compatible(v.band, sat1.band) &&
          duration >= minDuration &&
          sat1.stations.contains(stationId) &&
          v.aos.fitIn(sat1) &&
          (satellite.allowJamming || !v.jammed) &&
          (satellite.allowReservePhysicVisibilityHole || !v.physHole) &&
          (satellite.allowReserveRFVisibilityHole || !v.rfHole)
      }
      sat1 -> sat1Visibilities
    }(breakOut)
  }

}

object Model extends FromXml { }