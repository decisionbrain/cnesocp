package com.cnes.ocp.model


import java.time.temporal.ChronoUnit
import java.time.ZonedDateTime

import com.cnes.ocp.io.xml.objects.Status.Status
import com.cnes.ocp.io.xml.objects.Band.Band
import com.cnes.ocp.io.xml.objects.ConstraintType.ConstraintType
import com.cnes.ocp.utils.DateUtils._
import com.cnes.ocp.io.xml.objects.SpecificSchedule.SpecificSchedule

abstract class ModelObject(val id: String) { override def toString: String = id }

case class Satellite(override val id: String,
                     name             : String,
                     band             : Band,
                     sat4Band         : Band,
                     typeId           : String,
                     sat1Cstr         : Seq[Sat1Constraint],
                     consecutivePass  : Boolean,
                     allowJamming     : Boolean,
                     allowReservePhysicVisibilityHole : Boolean,
                     allowReserveRFVisibilityHole     : Boolean,
                     stationProperties: Map[String, StationProperties]) extends ModelObject(id) {

  def getMinDuration(sat1: Sat1Constraint, stationId: String): Second =
    if(sat1.minDuration == -1)  stationProperties(stationId).minDuration
    else                        sat1.minDuration

  def getMinElevation(stationId: String) : Double = stationProperties(stationId).minElevation
}

case class Station(override val id      : String,
                   name                 : String,
                   satelliteProperties  : Map[String, (Int, Int)], // satellite typeId -> (reconf, deconf)
                   sta2Cstr             : Seq[Sta2Constraint],
                   reservedMaintenances : Seq[(ZonedDateTime, ZonedDateTime)]) extends ModelObject(id)

case class Visibility(override val id: String,
                      band        : Band,
                      station     : Station,
                      satellite   : Satellite,
                      status      : Status,
                      holeDuration: Second,
                      jammed      : Boolean,
                      physHole    : Boolean,
                      rfHole      : Boolean,
                      aos0        : AosLos,
                      aos         : AosLos) extends ModelObject(id) {
  def getAos0Duration : Second = ChronoUnit.SECONDS.between(aos0.start, aos0.end).toInt - holeDuration
  def getAosDuration  : Second = ChronoUnit.SECONDS.between(aos.start, aos.end).toInt - holeDuration
  def getConfDeconfTimes: (Second, Second) = station.satelliteProperties.getOrElse(satellite.typeId, (0, 0))
}

case class AosLos(elevation: Double,
                  start : ZonedDateTime,
                  end   : ZonedDateTime){
  def fitIn(sat1: Sat1Constraint): Boolean = sat1.windows.exists{ w => (start, end).fitInside(w) }
}

case class Sat1Constraint(override val id: String,
                          band            : Band,
                          specificSchedule: SpecificSchedule,
                          private[model] val minDuration: Second, // private because overloaded from Satellite
                          minPass         : Int,
                          maxPass         : Int,
                          mostPass        : Boolean,
                          longestPass     : Boolean,
                          stations        : Seq[String],
                          windows         : Seq[(ZonedDateTime, ZonedDateTime)]) extends ModelObject(id){
  override def toString: String = s"$id [$minPass $maxPass]"
}

/**
  * @param minDuration in seconds, minimum duration to take into account when assigning a visibility to this station
  */
case class StationProperties(minDuration  : Second,
                             minElevation : Double,
                             priority     : Int)

/**
  * @param minDuration converted into seconds
  * @param windows a set of potential areas where to assign a maintenance slot of minDuration duration
  */
case class Sta2Constraint(override val id : String,
                          minDuration     : Second,
                          windows         : Seq[(ZonedDateTime, ZonedDateTime)]) extends ModelObject(id) {

  def isSatisfiedBy(reservedMaintenance: (ZonedDateTime, ZonedDateTime)): Boolean = {
    val duration = ChronoUnit.SECONDS.between(reservedMaintenance._1, reservedMaintenance._2).toInt
    duration >= minDuration && windows.exists{ w => reservedMaintenance.fitInside(w) }
  }
}

case class Parameters(start           : ZonedDateTime,
                      end             : ZonedDateTime,
                      timeLimit       : Int,
                      usedSatellites  : Seq[String],
                      usedStations    : Seq[String],
                      constraintTypes : Seq[ConstraintType],
                      removeFailingAtomicConstraints: Boolean,
                      exportCpoModel : Boolean)
