package com.cnes.ocp.model

import java.time.ZonedDateTime

case class Solution(satelliteVisibilities : Map[(Satellite, Sat1Constraint), Seq[Visibility]],
                    maintenances          : Map[Sta2Constraint, (ZonedDateTime, ZonedDateTime)])

case class InputSolution(satelliteVisibilities : Map[(Satellite, Sat1Constraint), Seq[Visibility]],
                         maintenances : Map[Station,Seq[(ZonedDateTime, ZonedDateTime)]] )
