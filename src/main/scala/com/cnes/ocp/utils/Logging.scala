package com.cnes.ocp.utils

import org.slf4j.{Logger, LoggerFactory}

trait Logging {
  protected val LOGGER: Logger = LoggerFactory.getLogger(getClass.getName)

  def debug(msg: String): Unit  = LOGGER.debug(msg)
  def info(msg: String): Unit   = LOGGER.info(msg)
  def warn(msg: String): Unit   = LOGGER.warn(msg)
  def error(msg: String, throwable: Throwable = null): Unit = LOGGER.error(msg, throwable)

}
