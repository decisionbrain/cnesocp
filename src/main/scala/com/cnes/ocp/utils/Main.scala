package com.cnes.ocp.utils

import java.io.{File, FileInputStream, FileOutputStream}
import java.nio.file.{Files, Paths}
import java.time.temporal.ChronoUnit

import com.cnes.ocp.io.csv.CsvExporter
import com.cnes.ocp.io.xml.debug.GanttXmlExporter
import com.cnes.ocp.optim.SatisfactionProblemCPO
import com.cnes.ocp.io.xml.{XmlExporter, XmlReader}
import com.cnes.ocp.model.Model

object Main extends Logging {
  val usage = "Usage: ./ocp -i input.xml -p param.xml -o output.xml -c output.csv [-t timeLimitInSeconds] [-r (remove conflicting atomic constraints) true/false] [-e (export cpo model) true/false]"

  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    if (options.size < 4 ){
      info(usage)
      System.exit(1)
    }

    val inputPath = options.get('i) match {
      case Some(ip) if Files.exists(Paths.get(ip)) => ip
      case Some(ip) => error(s"File '$ip' does not exists !"); System.exit(1); ""
      case None     => error("Missing parameter -i"); info(usage); System.exit(1); ""
    }
    val paramPath   = options.get('p) match {
      case Some(pp) if Files.exists(Paths.get(pp)) => pp
      case Some(pp) => error(s"File '$pp' does not exists !"); System.exit(1); ""
      case None     => error("Missing parameter -p"); info(usage); System.exit(1); ""
    }
    val outXmlPath  = options.get('o) match {
      case Some(ox) =>
        if (Files.exists(Paths.get(ox))) warn(s"File '$ox' already exists, will be overwritten !")
        ox
      case None     => error("Missing parameter -o"); info(usage); System.exit(1); ""
    }
    val outXmlGanttPath  = options.get('g) match {
      case Some(ox) =>
        if (Files.exists(Paths.get(ox))) warn(s"File '$ox' already exists, will be overwritten !")
        Some(ox)
      case None     => None
    }
    val outCsvPath  = options.get('c) match {
      case Some(oc) =>
        if (Files.exists(Paths.get(oc))) warn(s"File '$oc' already exists, will be overwritten !")
        oc
      case None     => error("Missing parameter -c"); info(usage); System.exit(1); ""
    }

    val xmlParams = XmlReader.parseParameters(new FileInputStream(paramPath))
    val xmlData   = XmlReader.parseInput(new FileInputStream(inputPath))

    val xmlParamsTimeLimit = options.get('t) match {
      case Some(tl) if tl.forall(_.isDigit) => xmlParams.copy(timeLimit = tl.toInt)
      case Some(tl) =>
        warn(s"Time limit parameter could not be parsed as a number of seconds: $tl. Using time limit from xml parameter file.")
        xmlParams
      case _        => xmlParams
    }

    val removeConstraints = options.get('r) match {
      case Some(boolString) if boolString == "true" || boolString == "false" =>
        boolString.toBoolean
      case Some(boolString) =>
        warn(s"Remove conflicting atomic constraint parameter could not be parsed as a boolean: $boolString. Using the default value: true")
        true
      case _ =>
        true
    }

    val exportCpoModel = options.get('e) match {
      case Some(boolString) if boolString == "true" || boolString == "false" =>
        boolString.toBoolean
      case Some(boolString) =>
        warn(s"Export cpo model parameter could not be parsed as a boolean: $boolString. Using the default value: false")
        false
      case _ =>
        false
    }


    val model = Model.fromXml(xmlData, xmlParamsTimeLimit, removeConstraints, exportCpoModel)

    val satPb = SatisfactionProblemCPO(model)
    if(satPb.buildSolve()){
      CsvExporter.export(model, satPb.satSolution, xmlData, outCsvPath)
      XmlExporter.export(model.stations, satPb.satSolution, xmlData, inputPath, outXmlPath)
      if(outXmlGanttPath.isDefined)
        GanttXmlExporter.export(model,satPb.satSolution,outXmlGanttPath.get)
    } else if (satPb.conflict != null){
      val stream = new FileOutputStream(s"${new File(outXmlPath).getParent}/conflict.cpo")
      stream.write(satPb.conflict.toByteArray)
    }

  }

  def nextOption(map: Map[Symbol, String], list: List[String]): Map[Symbol, String] = {
    list match {
      case Nil => map
      case "-i" :: value :: tail =>
        nextOption(map + ('i -> value), tail)
      case "-p" :: value :: tail =>
        nextOption(map + ('p -> value), tail)
      case "-o" :: value :: tail =>
        nextOption(map + ('o -> value), tail)
      case "-c" :: value :: tail =>
        nextOption(map + ('c -> value), tail)
      case "-l" :: value :: tail =>
        nextOption(map + ('l -> value), tail)
      case "-t" :: value :: tail =>
        nextOption(map + ('t -> value), tail)
      case "-r" :: value :: tail =>
        nextOption(map + ('r -> value), tail)
      case "-e" :: value :: tail =>
        nextOption(map + ('e -> value), tail)
      case "-g" :: value :: tail =>
        nextOption(map + ('g -> value), tail)
      case "-usage" :: tail =>
        info(usage)
        System.exit(1)
        nextOption(map , tail)
      case option :: tail =>
        info(s"Unknown option: $option")
        nextOption(map, tail)
    }
  }
}
