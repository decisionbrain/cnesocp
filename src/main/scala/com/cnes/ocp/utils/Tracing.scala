package com.cnes.ocp.utils

import scala.collection.TraversableLike

trait Tracing extends Logging {

  def traceTime[T](title: String)(f: => T): T = {
    val start = System.currentTimeMillis()
    val res = f
    info(title + s" - ${System.currentTimeMillis() - start} ms")
    res
  }

  def traceTimeAndCount[T, C <: Traversable[_]](title: String)(f: => C with TraversableLike[T, C]): C = {
    val start = System.currentTimeMillis()
    val res = f
    info(title + s": ${res.size} elems - ${System.currentTimeMillis() - start} ms")
    res
  }

  def traceTimeAndCount[T](title: String, f: => Array[T]): Array[T] = {
    val start = System.currentTimeMillis()
    val res = f
    info(title + s": ${res.size} elems - ${System.currentTimeMillis() - start} ms")
    res
  }

}
