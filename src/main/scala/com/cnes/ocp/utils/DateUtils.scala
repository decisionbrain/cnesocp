package com.cnes.ocp.utils

import java.time.format.DateTimeFormatter
import java.time._
import java.time.temporal.ChronoUnit

import com.cnes.ocp.model.{Model, Parameters}
import com.cnes.ocp.io.xml.objects.XmlPeriod

object DateUtils extends Logging {
  def intersects(interval1: (ZonedDateTime, ZonedDateTime), interval2: (ZonedDateTime, ZonedDateTime)): Boolean =
    interval1._1.isBefore(interval2._2) && interval2._1.isBefore(interval1._2)

  // Overloading for convenience
  def intersects(xmlValidity: XmlPeriod, params: Parameters): Boolean =
    intersects((xmlValidity.start, xmlValidity.end), (params.start, params.end))

  def intersection(interval1: (ZonedDateTime, ZonedDateTime), interval2: (ZonedDateTime, ZonedDateTime))
  : (ZonedDateTime, ZonedDateTime) = {
    val start = if (interval1._1.isAfter(interval2._1)) interval1._1 else interval2._1 //Max start
    val end   = if (interval1._2.isBefore(interval2._2)) interval1._2 else interval2._2 //Min end
    (start, end)
  }

  /**
    * for a given interval (s,e) representing a local time interval (HH:MM HH:MM) we transform it into start and duration
    * @return (s, duration), duration in seconds
    */
  def toStartAndDurations(s: LocalTime, e: LocalTime): (LocalTime, Long) = {
    if      (s.isBefore(e)) (s, ChronoUnit.SECONDS.between(s, e))
    else if (s.equals(e))   (s, 24 * 60 * 60L)
    else                    (s, 24 * 60 * 60 - ChronoUnit.SECONDS.between(e, s))
  }

  /**
    * Generate the set of interval ranges corresponding to some constraint that iterate on a daily basis,
    * with for each day a set of applied local time (eg HH:MM for each day)
    * @return a collection made for each day of the horizon by a sequence of the local time area projected on the current day
    *         plus the previous day areas if one of its area intersects the horizon
    */
  def generateSat1Intervals(intervals: Seq[(LocalTime, LocalTime)], days: Seq[DayOfWeek], periodOption: Option[(Int, ZonedDateTime)], horizonStart: ZonedDateTime, horizonEnd: ZonedDateTime): Seq[Seq[(ZonedDateTime, ZonedDateTime)]] = {
    val startAndDurations = intervals.map{ case (s, e) => toStartAndDurations(s, e) }

    if(days.isEmpty){
      periodOption match {
        case Some((period, firstApplication)) =>
          val daysCount = ChronoUnit.DAYS.between(horizonStart, horizonEnd).toInt
          (0 until daysCount).map{ dCount =>
            val currentDay = horizonStart.plusDays(dCount)
            val nbDaysBetween = ChronoUnit.DAYS.between(firstApplication, currentDay).toInt
            if(nbDaysBetween >= 0 && nbDaysBetween % period == 0){
              startAndDurations.map{ case (start, duration) =>
                (currentDay.`with`(start), currentDay.`with`(start).plusSeconds(duration))
              }
            } else {
              Nil
            }
          }.filter(_.nonEmpty) //remove Nil
        case None =>
          val daysCount = ChronoUnit.DAYS.between(horizonStart, horizonEnd).toInt
          (0 until daysCount).map{ dCount =>
            val currentDay = horizonStart.plusDays(dCount)
            startAndDurations.map{ case (start, duration) =>
              (currentDay.`with`(start), currentDay.`with`(start).plusSeconds(duration))
            }
          }
      }
    } else {
      var currentMonday = LocalDate.from(horizonStart).`with`(DayOfWeek.MONDAY)
      val localEnd      = LocalDate.from(horizonEnd)
      var result = Seq[Seq[(ZonedDateTime, ZonedDateTime)]]()

      while(currentMonday.isBefore(localEnd)){
        val listIntervalsInsideWeek = days.flatMap{ dayOfWeek =>
          val currentDay = currentMonday.atStartOfDay(horizonStart.getZone)
          startAndDurations.map{ case (start, duration) =>
            (currentDay.`with`(dayOfWeek).`with`(start), currentDay.`with`(dayOfWeek).`with`(start).plusSeconds(duration))
          }
        }
        result :+= listIntervalsInsideWeek
        currentMonday = currentMonday.plusDays(7)
      }
      result
    }
  }

  /**
    * @param minDuration minimum duration of a maintenance slot
    * @param timeWindow the slot period start and end (local time eg 08:00 -> 12:00)
    * @param workingHours all possible time windows of workers availability on the station over a day
    * @return a set of potential intervals I of maintenance created by intersecting the slotPeriod of sat2 with worker availabilities in the day
    */
  def generateSta2Intervals(minDuration: Int, timeWindow: (LocalTime, LocalTime), workingHours: Seq[(ZonedDateTime, ZonedDateTime)])
  : Seq[(ZonedDateTime, ZonedDateTime)] = {
    val (start, duration) = toStartAndDurations(timeWindow._1, timeWindow._2)
    workingHours.flatMap{ case (s, e) =>
      val a = s.`with`(start)
      val b = a.plusSeconds(duration)
      val (intersectionStart, intersectionEnd) = intersection((a, b), (s, e))
      if(ChronoUnit.SECONDS.between(intersectionStart, intersectionEnd).toInt >= minDuration)
        Some((intersectionStart, intersectionEnd))
      else
        None
    }
  }

  implicit class ZonedDateTimeWrapper(zdt: ZonedDateTime){
    // /!\ WARNING: if you modify this, change the implementation of toZonedDateTime below as well !
    def toInt(implicit model: Model)    : Int     = ChronoUnit.SECONDS.between(model.parameters.start, zdt).toInt
    def toDouble(implicit model: Model) : Double  = ChronoUnit.SECONDS.between(model.parameters.start, zdt).toInt

    def fitInside(sw: ZonedDateTime, ew: ZonedDateTime): Boolean = !zdt.isBefore(sw) && zdt.isBefore(ew)

    def serialize: String = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(zdt.withZoneSameInstant(ZoneId.of("UTC")))
    def serializeXML: String = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(zdt.withZoneSameInstant(ZoneId.of("UTC")))
  }

  implicit class IntWrapper(s: Int){
    def toZonedDateTime(implicit origin: ZonedDateTime): ZonedDateTime = origin.plusSeconds(s)
  }

  implicit class ZonedDateTimeTupleWrapper(tuple: (ZonedDateTime, ZonedDateTime)){
    val (s, e) = tuple
    def fitInside(w: (ZonedDateTime, ZonedDateTime)): Boolean =  !s.isBefore(w._1) && !e.isAfter(w._2)
    def intersects(interval2: (ZonedDateTime, ZonedDateTime)): Boolean = DateUtils.intersects(tuple, interval2)
  }
}
