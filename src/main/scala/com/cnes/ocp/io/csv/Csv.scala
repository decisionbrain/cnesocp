package com.cnes.ocp.io.csv

import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

import com.cnes.ocp.model.Visibility
import com.cnes.ocp.io.xml.objects._
import com.cnes.ocp.utils.DateUtils._

trait Csv[T]{
  def toCsv(t: T): String
}

object Csv {
  def apply[T](implicit csv: Csv[T]): Csv[T] = csv

  implicit class CsvOps[T: Csv](t: T) {
    def toCsv: String = Csv[T].toCsv(t)
  }

  def getHeaders: String = "SAT, STA, START, END, AOS_O, LOS_0, AOS_CS4, LOS_CS4, Freq, Type de Support, Categorie, Etat, trou physique, trou RF, brouillage, DUREE_UTIL, Commentaire, Info Ocp, Misc orig visibility,Initial State, inSat1\n"

  /**
    * reservations done by the engine and initially RESERVED availabilities
    */
  type CsvReservation = (Visibility, XmlVisibility, XmlSatellite, XmlStation, XmlSatelliteSlot, XmlSlotType, XmlSlotCategory, Boolean)
  implicit val reservationToCsv: Csv[CsvReservation] =
    new Csv[CsvReservation]{
      override def toCsv(reservationTuple: CsvReservation): String = {
        val (v, xmlVisibility, xmlSatellite, xmlStation, xmlSatSlot, xmlSlotType, xmlSlotCategory, inSat1) = reservationTuple

        val (conf, deconf) = v.getConfDeconfTimes
        val (start, end) = (v.aos0.start.minusSeconds(conf), v.aos0.end.plusSeconds(deconf))
        val state = if (xmlSatSlot.state == Status.AVAILABLE) Status.RESERVED else xmlSatSlot.state
        val utilDuration = ChronoUnit.SECONDS.between(v.aos.start, v.aos.end).toInt - xmlVisibility.holeDuration.toInt

        val attributes = List[String](
          xmlSatellite.name, xmlStation.name,
          start.serialize       , end.serialize,
          v.aos0.start.serialize, v.aos0.end.serialize,
          v.aos.start.serialize , v.aos.end.serialize,
          v.band.toString,
          xmlSlotType.name,
          xmlSlotCategory.name,
          state.toString,
          xmlVisibility.phyHole.toString,
          xmlVisibility.rfHole.toString,
          xmlVisibility.jammed.toString,
          utilDuration.toString,
          xmlSatSlot.comment.getOrElse(""),
          xmlSatSlot.slotTrace.getOrElse(""),
          v.id,
          xmlSatSlot.state.toString,
          if(inSat1) "inSat1" else ""
        )

        attributes.mkString(",").concat("\n")
      }
    }

  /**
    * initial availabilities minus optim-reservations
    */
  type CsvAvailability = (Visibility, XmlVisibility, XmlSatellite, XmlStation, XmlSatelliteSlot)
  implicit val visibilityToCsv: Csv[CsvAvailability] =
    new Csv[CsvAvailability]{
      override def toCsv(visibilityTuple: CsvAvailability): String = {
        val (v, xmlVisibility, xmlSatellite, xmlStation, xmlSatSlot) = visibilityTuple

        val (conf, deconf) = v.getConfDeconfTimes
        val (start, end) = (v.aos0.start.minusSeconds(conf), v.aos0.end.plusSeconds(deconf))
        val utilDuration = ChronoUnit.SECONDS.between(v.aos.start, v.aos.end).toInt - xmlVisibility.holeDuration.toInt

        val attributes = List[String](
          xmlSatellite.name, xmlStation.name,
          start.serialize       , end.serialize,
          v.aos0.start.serialize, v.aos0.end.serialize,
          v.aos.start.serialize , v.aos.end.serialize,
          v.band.toString,
          "",
          "",
          xmlSatSlot.state.toString,
          xmlVisibility.phyHole.toString,
          xmlVisibility.rfHole.toString,
          xmlVisibility.jammed.toString,
          utilDuration.toString,
          xmlSatSlot.comment.getOrElse(""),
          xmlSatSlot.slotTrace.getOrElse(""),
          v.id,
          xmlSatSlot.state.toString
        )

        attributes.mkString(",").concat("\n")
      }
    }

  type CsvMaintenance = (XmlStation, (ZonedDateTime, ZonedDateTime))
  implicit val maintenanceToCsv: Csv[CsvMaintenance] =
    new Csv[CsvMaintenance]{
      override def toCsv(maintenanceTuple: CsvMaintenance): String = {
        val (xmlStation, (start, end)) = maintenanceTuple

        val attributes = List[String](
          "MAINTEN-", xmlStation.name,
          start.serialize, end.serialize,
          start.serialize, end.serialize,
          start.serialize, end.serialize,
          "", "MNT", "MNT",
          Status.RESERVED.toString,
          "false", "false", "false",
          "", // duree utile
          "",
          "",
          "")

        attributes.mkString(",").concat("\n")
      }
    }

  type CsvReservedMaintenance = (XmlStation, XmlStationSlot)
  implicit val reservedMaintenanceToCsv: Csv[CsvReservedMaintenance] =
    new Csv[CsvReservedMaintenance]{
      override def toCsv(reservedMaintenance: CsvReservedMaintenance): String = {
        val (xmlStation, xmlStationSlot) = reservedMaintenance
        val start = xmlStationSlot.period.start
        val end   = xmlStationSlot.period.end

        val attributes = List[String](
          "MAINTEN-", xmlStation.name,
          start.serialize, end.serialize,
          start.serialize, end.serialize,
          start.serialize, end.serialize,
          "", "MNT", "MNT",
          Status.RESERVED.toString,
          "false", "false", "false",
          "", // duree utile
          "",
          "",
          "")

        attributes.mkString(",").concat("\n")
      }
    }
}
