package com.cnes.ocp.io.csv

import java.io.{BufferedWriter, FileWriter}

import com.cnes.ocp.model._
import com.cnes.ocp.io.csv.Csv._
import com.cnes.ocp.utils.Tracing
import com.cnes.ocp.io.xml.objects._

import scala.collection.{breakOut, mutable}

object CsvExporter extends Tracing {

  def export(model: Model, solution: Solution, xmlModel: XmlModel, pathToFile: String): Unit =
    traceTime("Export to CSV"){
      val xmlVisibilitiesById : Map[String, XmlVisibility]  = xmlModel.visibilities.map{ s => s.id -> s }(breakOut)
      val xmlSatelliteById    : Map[String, XmlSatellite]   = xmlModel.satellites.map{ s => s.id -> s }(breakOut)
      val xmlStationById      : Map[String, XmlStation]     = xmlModel.stations.map{ s => s.id -> s }(breakOut)
      val xmlSatelliteSlotById: Map[String, XmlSatelliteSlot] = xmlModel.satelliteSlots.map{ s => s.visibilityId -> s }(breakOut)
      val xmlSlotTypeById     : Map[String, XmlSlotType]      = xmlModel.slotTypes.map{ s => s.id -> s }(breakOut)
      val xmlSlotCategoryById : Map[String, XmlSlotCategory]  = xmlModel.slotCategories.map{ s => s.id -> s }(breakOut)
      val xmlSat1ById         : Map[String, XmlSat1Constraint] = xmlModel.satellites.flatMap{ s =>
        s.sat1.map{ sat1 => sat1.id -> sat1} }(breakOut)

      val processedVisibilities = mutable.Set[String]()
      //optim reservations
      val toExport = mutable.Buffer[CsvReservation]()
      solution.satelliteVisibilities.foreach{ case ((_, sat1), visiSeq) =>
        visiSeq.foreach{ v =>
          if(processedVisibilities.add(v.id)){
            val xmlSatId = sat1.id.split('-')(0) // Sat1 Ids are generated from XmlSat1 Ids
            val xmlSat1 = xmlSat1ById(xmlSatId)
            toExport.append((v, xmlVisibilitiesById(v.id),
              xmlSatelliteById(v.satellite.id),
              xmlStationById(v.station.id),
              xmlSatelliteSlotById(v.id),
              xmlSlotTypeById(xmlSat1.slotTypeId),
              xmlSlotCategoryById( xmlSat1.slotCategoryId),
              true
            ))
          }
        }
      }

      val satelliteSat1 = for(s <- model.satellites; sat1 <- s.sat1Cstr) yield (s, sat1)
      val reservations: Map[(Satellite, Sat1Constraint), Seq[Visibility]] = satelliteSat1.map{ case (satellite, sat1) =>
        val usedV = model.reservationsBySat1(sat1)
        (satellite, sat1) -> usedV
      }(breakOut)
      val reservedInSat1 = reservations.values.flatten.toSet

      //optim pre-reserved availabilities
      val resaToExport = model.reservations.map{ r =>
        val satelliteSlot = xmlSatelliteSlotById(r.id)
        (r, xmlVisibilitiesById(r.id),
          xmlSatelliteById(r.satellite.id),
          xmlStationById(r.station.id),
          satelliteSlot,
          xmlSlotTypeById(satelliteSlot.slotTypeId.get),
          xmlSlotCategoryById(satelliteSlot.slotCategoryId.get),
          reservedInSat1.contains(r)
        )
      }

      val extraPassesToExport = model.extraPasses.map{ r =>
        val satelliteSlot = xmlSatelliteSlotById(r.id)
        (r, xmlVisibilitiesById(r.id),
          xmlSatelliteById(r.satellite.id),
          xmlStationById(r.station.id),
          satelliteSlot,
          xmlSlotTypeById(satelliteSlot.slotTypeId.get),
          xmlSlotCategoryById(satelliteSlot.slotCategoryId.get)
          ,false
        )
      }

      //all availabilities minus those chosen by the optimization
      val availabilities = model.visibilities.collect{case v if !processedVisibilities.contains(v.id) =>
        val satelliteSlot = xmlSatelliteSlotById(v.id)
        (v, xmlVisibilitiesById(v.id),
          xmlSatelliteById(v.satellite.id),
          xmlStationById(v.station.id),
          satelliteSlot)
      }

      val maintenances = for(s <- model.stations; sta2 <- s.sta2Cstr) yield (xmlStationById(s.id), solution.maintenances(sta2))
      val reservedMaintenance = xmlModel.stationSlots.map{ staSlot => (xmlStationById(staSlot.stationId), staSlot) }

      val bw = new BufferedWriter(new FileWriter(pathToFile))
      bw.write('\ufeff') //This is UTF-8 BOM character (allows excel to understand it is utf-8 and display accents properly)
      bw.write(Csv.getHeaders)
      toExport            foreach ( v => bw.write(v.toCsv) )
      resaToExport        foreach ( v => bw.write(v.toCsv) )
      extraPassesToExport foreach ( v => bw.write(v.toCsv) )
      availabilities      foreach ( v => bw.write(v.toCsv) )
      maintenances        foreach ( v => bw.write(v.toCsv) )
      reservedMaintenance foreach ( v => bw.write(v.toCsv) )
      bw.close()
    }

}