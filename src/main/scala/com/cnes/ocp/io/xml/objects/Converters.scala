package com.cnes.ocp.io.xml.objects

import java.time.format.DateTimeFormatter
import java.time.{DayOfWeek, LocalTime, ZonedDateTime}

import com.cnes.ocp.io.xml.objects.Status.Status
import com.cnes.ocp.io.xml.objects.Band.Band
import com.cnes.ocp.io.xml.objects.ConstraintType.ConstraintType
import com.cnes.ocp.io.xml.objects.SatCstrType.SatCstrType
import com.cnes.ocp.io.xml.objects.SpecificSchedule.SpecificSchedule
import com.decisionbrain.xml.annotation.ValueConverter

object SatCstrType extends Enumeration {
  type SatCstrType = Value
  val NOMINAL, REDUCED, MINIMAL = Value
}
class SatCstrTypeConverter extends ValueConverter[SatCstrType] {
  override def fromString(xmlValue: String): SatCstrType = SatCstrType.withName(xmlValue)
}

object Band extends Enumeration {
  type Band = Value
  val S, X, SX = Value
  def compatible(a: Band, b: Band): Boolean =
    a match {
      case S if b == S || b == SX => true
      case X if b == X || b == SX => true
      case SX                     => true
      case _                      => false
    }
}
class BandConverter extends ValueConverter[Band] {
  override def fromString(xmlValue: String): Band = Band.withName(xmlValue)
}

object Status extends Enumeration {
  type Status = Value
  val AVAILABLE, RESERVED, RESERVED_LOCKED, RESERVATION_IN_PROGRESS, IMPOSED = Value
}
class StatusConverter extends ValueConverter[Status] {
  override def fromString(xmlValue: String): Status = Status.withName(xmlValue)
}

object ConstraintType extends Enumeration {
  type ConstraintType = Value
  val CSatType1, CSatType2, CSatType3, CSatType4, CSatType5, CSatType6, CSatType8, CSatType9, CSatType10, CStaType2, CStaType4, CStaType5, CStaType6 = Value
}
class ConstraintTypeConverter extends ValueConverter[ConstraintType] {
  override def fromString(xmlValue: String): ConstraintType = ConstraintType.withName(xmlValue)
}

object SpecificSchedule extends Enumeration {
  type SpecificSchedule = Value
  val INDIFFERENT, FIRST, LAST = Value
}
class SpecificScheduleConverter extends ValueConverter[SpecificSchedule] {
  override def fromString(xmlValue: String): SpecificSchedule = SpecificSchedule.withName(xmlValue)
}

class DayConverter extends ValueConverter[DayOfWeek] {
  override def fromString(xmlValue: String): DayOfWeek = if (xmlValue.isEmpty) null else DayOfWeek.valueOf(xmlValue)
}

class LocalTimeConverter extends ValueConverter[LocalTime] {
  override def fromString(xmlValue: String): LocalTime = LocalTime.parse(xmlValue)
}

class LocalTimeWithZoneConverter extends ValueConverter[LocalTime] {
  // Just drop the time zone, hours are to be considered GMT+1
  override def fromString(xmlValue: String): LocalTime = ZonedDateTime.parse(xmlValue).toLocalTime
}

class ZonedDateTimeConverter extends ValueConverter[ZonedDateTime] {
  override def fromString(xmlValue: String): ZonedDateTime = {
    val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    ZonedDateTime.parse(xmlValue, dateFormat)
  }
}

class OptionZonedDateTimeConverter extends ValueConverter[Option[ZonedDateTime]] {
  override def fromString(xmlValue: String): Option[ZonedDateTime] = {
    if(xmlValue == null) None
    else Some(ZonedDateTime.parse(xmlValue, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")))
  }
}

class StringCleaner extends ValueConverter[Option[String]] {
  //Remove all control characters (tab, new line)
  override def fromString(xmlValue: String): Option[String] =
    if (xmlValue == null) None else Some(xmlValue.filter(_ >= ' '))
}