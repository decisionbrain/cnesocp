package com.cnes.ocp.io.xml.objects

case class XmlModel(
                     stations       : Seq[XmlStation],
                     satellites     : Seq[XmlSatellite],
                     visibilities   : Seq[XmlVisibility],
                     satelliteSlots : Seq[XmlSatelliteSlot],
                     slotTypes      : Seq[XmlSlotType],
                     slotCategories : Seq[XmlSlotCategory],
                     stationSlots   : Seq[XmlStationSlot])
