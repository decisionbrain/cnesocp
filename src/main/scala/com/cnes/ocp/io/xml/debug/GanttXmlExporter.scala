package com.cnes.ocp.io.xml.debug

import java.time.{ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

import com.cnes.ocp.io.csv.CsvExporter.traceTime
import com.cnes.ocp.model.{InputSolution, Model, Sat1Constraint, Solution}
import com.decisionbrain.xml.Writer

import scala.collection.mutable
import scala.xml.XML
import scala.collection.breakOut

object GanttXmlExporter {
  val dateFormat: String = "M-d-yyyy HH:m:s"

  def toString(zdt: ZonedDateTime): String = DateTimeFormatter.ofPattern(dateFormat).format(zdt.withZoneSameInstant(ZoneId.of("UTC")))

  def export(model: Model, solution: Solution, outputPath: String): Unit = {
    traceTime(s"Export to Gantt $outputPath"){
      var minStart = if (model.visibilities.isEmpty) None else Some(model.visibilities.head.aos.start)

      var maxEnd = if (model.visibilities.isEmpty) None else Some(model.visibilities.head.aos.end)
      val activityIds = mutable.Set[String]()
      val activityList = solution.satelliteVisibilities.flatMap{ case (_, visiSeq) =>
        visiSeq.collect{ case v if activityIds.add(v.id) =>
          if(v.aos.start.isBefore(minStart.head))  minStart = Some(v.aos.start)
          if(maxEnd.head.isBefore(v.aos.end))      maxEnd = Some(v.aos.end)
          val activitySat = Activity(s"${v.id}-sat", s"${v.id}-sat", toString(v.aos.start), toString(v.aos.end))
          val (conf, deconf) = v.getConfDeconfTimes
          val activitySta = Activity(s"${v.id}-sta", s"${v.id}-sta", toString(v.aos.start.minusSeconds(conf)), toString(v.aos.end.plusSeconds(deconf)))
          List(activitySat,activitySta)
        }.flatten
      }


      val maintenanceActivities = solution.maintenances.map{ case (sta2, (start, end)) =>
        if(!minStart.isDefined || start.isBefore(minStart.head))
          minStart = Some(start)
        if(!maxEnd.isDefined || maxEnd.head.isBefore(end))
          maxEnd = Some(end)
        Activity(sta2.id, sta2.id, toString(start), toString(end))
      }


      val activities = Activities(dateFormat,
        ActivityMain(
          toString(minStart.head),
          toString(maxEnd.head),
          activityList.toSeq ++ maintenanceActivities)
      )


      val satelliteResources = model.satellites.map{ s => Resource(s.id, s.name) }
      val stationResources = model.stations.map{ s => Resource(s.id, s.name) }
      val prefixSat1 : Map[Sat1Constraint, String] = model.candidatesBySat1.keySet.map{ case sat1 =>
        val prefix = sat1.id.split("-")(0)
        sat1 -> prefix
      } (breakOut)
      val sat1Resources = prefixSat1.values.toSet.map{ sat1P => Resource(sat1P, sat1P, 1) }
      val allResources = satelliteResources ++ stationResources ++ sat1Resources
      val resources = Resources(ResourceMain(allResources, allResources.size))

      val activityIds2 = mutable.Set[String]()
      val resaSatSta = solution.satelliteVisibilities.flatMap{ case ((_,sat1), visiSeq) =>
        visiSeq.flatMap{
          case v
            if activityIds2.add(v.id) => List(Reservation(s"${v.id}-sta", v.station.id), Reservation(s"${v.id}-sat", v.satellite.id))
          case _ => Nil
        }
      }.toSeq

      val resaSat1 = solution.satelliteVisibilities.flatMap{ case ((_,sat1), visiSeq) =>
        visiSeq.map{ v => Reservation(s"${v.id}-sat", prefixSat1(sat1))       }
      }.toSeq

      val maintenanceResa = model.stations.flatMap{ station =>
        station.sta2Cstr.map{ sta2 => Reservation(sta2.id, station.id) }
      }
      val reservations = Reservations(resaSatSta ++ resaSat1 ++ maintenanceResa)

      val schedule = Schedule("3.5", resources, activities, reservations)

      val xml = Writer.write(schedule)
      XML.save(outputPath, xml)
    }
  }

  /**
    * export with an input solution from xml file
    * @param model
    * @param solution
    * @param outputPath
    */
  def export(model: Model, solution: InputSolution, outputPath: String): Unit = {
    traceTime(s"Export Input solution to Gantt $outputPath"){
      var minStart = if (model.visibilities.isEmpty) None else Some(model.visibilities.head.aos.start)
      var maxEnd = if (model.visibilities.isEmpty) None else Some(model.visibilities.head.aos.end)
      val activityIds = mutable.Set[String]()
      val activityList = solution.satelliteVisibilities.flatMap{ case (_, visiSeq) =>
        visiSeq.collect{ case v if activityIds.add(v.id) =>
          if(v.aos.start.isBefore(minStart.head))  minStart = Some(v.aos.start)
          if(maxEnd.head.isBefore(v.aos.end))      maxEnd = Some(v.aos.end)
          val activitySat = Activity(s"${v.id}-sat", s"${v.id}-sat", toString(v.aos.start), toString(v.aos.end))
          val (conf, deconf) = v.getConfDeconfTimes
          val activitySta = Activity(s"${v.id}-sta", s"${v.id}-sta", toString(v.aos.start.minusSeconds(conf)), toString(v.aos.end.plusSeconds(deconf)))
          List(activitySat,activitySta)
        }.flatten
      }

      val (maintenanceActivities,maintenanceResa)   = solution.maintenances.flatMap{ case (sta,maintenances) => maintenances.zipWithIndex.map{
        case((start,end),i) =>
          if(!minStart.isDefined || start.isBefore(minStart.head))
            minStart = Some(start)
          if(!maxEnd.isDefined || maxEnd.head.isBefore(end))
            maxEnd = Some(end)
          val info = (sta,i,start,end)
          val activity = Activity(s"${sta.id}_$i", sta.id, toString(start), toString(end))
          val resa = Reservation(activity.id, sta.id)
          (activity,resa)
      } }.unzip



      val activities = Activities(dateFormat,
        ActivityMain(
          toString(minStart.head),
          toString(maxEnd.head),
          activityList.toSeq ++ maintenanceActivities)
      )


      val satelliteResources = model.satellites.map{ s => Resource(s.id, s.name) }
      val stationResources = model.stations.map{ s => Resource(s.id, s.name) }
      val prefixSat1 : Map[Sat1Constraint, String] = model.candidatesBySat1.keySet.map{ case sat1 =>
        val prefix = sat1.id.split("-")(0)
        sat1 -> prefix
      } (breakOut)
      val sat1Resources = prefixSat1.values.toSet.map{ sat1P => Resource(sat1P, sat1P, 1) }
      val allResources = satelliteResources ++ stationResources ++ sat1Resources
      val resources = Resources(ResourceMain(allResources, allResources.size))

      val activityIds2 = mutable.Set[String]()
      val resaSatSta = solution.satelliteVisibilities.flatMap{ case ((_,sat1), visiSeq) =>
        visiSeq.flatMap{
          case v
            if activityIds2.add(v.id) => List(Reservation(s"${v.id}-sta", v.station.id), Reservation(s"${v.id}-sat", v.satellite.id))
          case _ => Nil
        }
      }.toSeq

      val resaSat1 = solution.satelliteVisibilities.flatMap{ case ((_,sat1), visiSeq) =>
        visiSeq.map{ v => Reservation(s"${v.id}-sat", prefixSat1(sat1))       }
      }.toSeq


      val reservations = Reservations(resaSatSta ++ resaSat1 ++ maintenanceResa)

      val schedule = Schedule("3.5", resources, activities, reservations)

      val xml = Writer.write(schedule)
      XML.save(outputPath, xml)
    }

  }
}
