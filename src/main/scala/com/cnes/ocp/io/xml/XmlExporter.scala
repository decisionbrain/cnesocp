package com.cnes.ocp.io.xml

import java.io.FileInputStream
import java.time.LocalDateTime

import com.cnes.ocp.io.xml.objects.{XmlModel, XmlSta2Constraint}
import com.cnes.ocp.model.{Solution, Station, Visibility}
import com.cnes.ocp.utils.Tracing
import com.decisionbrain.xml.Updater
import com.cnes.ocp.utils.DateUtils._

import scala.collection.breakOut
import scala.xml.XML

object XmlExporter extends Tracing {

  def export(stations: Seq[Station], solution: Solution, xmlModel: XmlModel, inputPath: String, outputPath: String): Unit = {
    val xml = traceTime("Loading XML data")(XML.load(new FileInputStream(inputPath)))

    val slotTypeCategoryBySat1Id: Map[String, (String, String)] = xmlModel.satellites.flatMap{ satellite =>
      satellite.sat1.map{ sat1 => sat1.id -> (sat1.slotTypeId, sat1.slotCategoryId) }
    }(breakOut)

    val newXml = traceTime("Updating XML data"){
      val t = solution.satelliteVisibilities.flatMap{ case ((_, sat1), visiSeq) =>
        // /!\ Warning /!\
        // here if a visibility satisfies multiples Sat1, we only keep the last one (and use its slotType and slotCategory)
        val (slotType, slotCategory) = slotTypeCategoryBySat1Id(sat1.id.split('-')(0))
        visiSeq.map{ v => v.id -> Seq(("state", "RESERVED"),
                                      ("slotType", slotType),
                                      ("slotCategory", slotCategory),
                                      ("comment", s"Réservé le [${LocalDateTime.now}] par le calcul du solveur"))
        }
      }

      //TODO This is an example of what is required (see OCP-18). To be removed when export to Xml is finalized. Lot2.
      <stationSlots
      comment="Réservé le [02-May-2019 12:19:24] par le calcul de la journée du [2019-04-29] en mode [NOMINAL]"
      identifier="0"
      manual="false"
      manualComment=""
      slotCategory="SlotCategory_3"
      slotID="0"
      slotType="SlotType_2"
      state="RESERVED"
      station="Station_56292"
      supportType="TM">
        <startEnd end="2018-11-19T07:10:00.000+0000" begin="2018-11-19T06:35:00.000+0000"/>
      </stationSlots>

      val xmlSta2ById: Map[String, XmlSta2Constraint] = xmlModel.stations.flatMap{ station =>
        station.sta2.map{ sta2 => sta2.id -> sta2}
      }(breakOut)

      val maintenances = stations.flatMap{ station =>
        station.sta2Cstr.map{ sta2 =>
          val (start, end) = solution.maintenances(sta2)
          val xmlSta2 = xmlSta2ById(sta2.id.split('-')(0))  // because model's Sta2 are atomic, id's are generated
          Map(
            "manual"        -> "false",
            "state"         -> "RESERVED",
            "supportType"   -> "TM",
            "station"       -> station.id,
            "slotType"      -> xmlSta2.slotTypeId,
            "slotCategory"  -> xmlSta2.slotCategoryId,
            "startEnd"      -> Map("begin" -> start.serializeXML, "end" -> end.serializeXML),
            "comment"       -> s"Réservé le [${LocalDateTime.now}] par le calcul du solveur"
          )
        }
      }


      // look in the original xml satellite slots whose visibility matches t keys
      // update xml with the tuple from t values
      val xml2 = Updater.update(xml, "satelliteSlots" , "visibility", t)
      // Append reserved maintenances (stationSlots nodes) to the Context node
      Updater.appendRecursive(xml2.head, "Context", "stationSlots", maintenances)
    }

    traceTime("Write XML file"){ XML.save(outputPath, newXml.head) }
  }
}
