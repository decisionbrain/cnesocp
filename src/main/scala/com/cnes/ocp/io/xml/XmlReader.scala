package com.cnes.ocp.io.xml

import java.io.InputStream

import com.cnes.ocp.utils.Tracing
import com.decisionbrain.xml.Parser
import com.cnes.ocp.io.xml.objects._

import scala.xml.XML

object XmlReader extends Tracing {

  def parseInput(input: InputStream): XmlModel = {
    traceTime("Reading XML data") {
      val xml = XML.load(input)
      val parser = new Parser()
      val satellites      = parser.parseSeq(xml, classOf[XmlSatellite])
      val stations        = parser.parseSeq(xml, classOf[XmlStation])
      val visibilities    = parser.parseSeq(xml, classOf[XmlVisibility])
      val satelliteSlots  = parser.parseSeq(xml, classOf[XmlSatelliteSlot])
      val slotTypes       = parser.parseSeq(xml, classOf[XmlSlotType])
      val slotCategories  = parser.parseSeq(xml, classOf[XmlSlotCategory])
      val stationSlots    = parser.parseSeq(xml, classOf[XmlStationSlot])

      XmlModel(stations, satellites, visibilities, satelliteSlots, slotTypes, slotCategories, stationSlots)
    }
  }

  def parseParameters(input: InputStream): XmlParams = {
    traceTime("Reading XML params") {
      val xml = XML.load(input)
      new Parser().parse(xml, classOf[XmlParams])
    }
  }

}
