package com.cnes.ocp.io.xml.debug

import com.decisionbrain.xml.annotation.{Attribute, Element}

class DavidXmlObjects {}

@Element(path = "schedule")
case class Schedule(@Attribute(path = "version")    version: String,
                    @Element(path = "resources")    resources: Resources,
                    @Element(path = "activities")   activities: Activities,
                    @Element(path = "reservations") reservations: Reservations)



case class Resources(@Element(path = "resource") resource: ResourceMain)
case class ResourceMain(@Element(path = "resource")   resources: Seq[Resource],
                        @Attribute(path = "quantity") quantity: Int,
                        @Attribute(path = "id")       id: String = "All Resources",
                        @Attribute(path = "name")     name: String = "All Resources")
//For satellites and stations
case class Resource(@Attribute(path = "id")       id: String,
                    @Attribute(path = "name")     name: String,
                    @Attribute(path = "quantity") quantity: Int = 1,
                    )



case class Activities(@Attribute(path = "dateFormat") dateFormat: String,
                      @Element(path = "activity")     activity: ActivityMain)
case class ActivityMain(@Attribute(path = "start")  start: String,
                        @Attribute(path = "end")    end: String,
                        @Element(path = "activity") activities: Seq[Activity],
                        @Attribute(path = "name")   name: String = "All Activities",
                        @Attribute(path = "id")     id: String = "All Activities")
//For reservations
case class Activity(@Attribute(path = "name")   name: String,
                    @Attribute(path = "id")     id: String,
                    @Attribute(path = "start")  start: String,
                    @Attribute(path = "end")    end: String)

case class Reservations(@Element(path = "reservation") reservations: Seq[Reservation])
case class Reservation(@Attribute(path = "activity") activityId: String,
                       @Attribute(path = "resource") resourceId: String)