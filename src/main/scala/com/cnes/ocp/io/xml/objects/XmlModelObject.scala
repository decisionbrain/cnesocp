package com.cnes.ocp.io.xml.objects

import java.time.{DayOfWeek, LocalTime, ZonedDateTime}

import com.cnes.ocp.io.xml.objects.Status.Status
import com.cnes.ocp.io.xml.objects.Band.Band
import com.cnes.ocp.io.xml.objects.ConstraintType.ConstraintType
import com.cnes.ocp.io.xml.objects.SatCstrType.SatCstrType
import com.cnes.ocp.io.xml.objects.SpecificSchedule.SpecificSchedule
import com.decisionbrain.xml.annotation.{Attribute, Converter, Element}

abstract class XmlModelObject(val id: String)

case class XmlPeriod(@Attribute(path = "begin") @Converter(value = classOf[ZonedDateTimeConverter])
                     start: ZonedDateTime,
                     @Attribute(path = "end") @Converter(value = classOf[ZonedDateTimeConverter])
                     end: ZonedDateTime)
case class XmlSlot(@Attribute(path = "begin") @Converter(value = classOf[LocalTimeConverter])
                   start: LocalTime,
                   @Attribute(path = "end") @Converter(value = classOf[LocalTimeConverter])
                   end: LocalTime)
case class XmlWorkingPeriod(@Attribute(path = "key") @Converter(value = classOf[ZonedDateTimeConverter])
                            day: ZonedDateTime,
                            @Element(path = "value")
                            workingPeriods: Seq[XmlPeriod])
case class XmlStationProperties(@Attribute(path = "station")
                                stationId: String,
                                @Attribute(path = "minDuration")
                                minDuration: Int,//in seconds
                                @Attribute(path = "minElevation")
                                minElevation: Double,
                                @Attribute(path = "stationPriority")
                                priority: Int)
case class XmlSatelliteProperties(@Attribute(path = "satFamily")
                                  typeId: String,
                                  @Attribute(path = "reconfSatFamily")
                                  reconfTime: Option[java.lang.Integer],
                                  @Attribute(path = "deconfSatFamily")
                                  deconfTime: Option[java.lang.Integer])
case class XmlAosLos(@Attribute(path = "elevation")
                     elevation: Double,
                     @Element(path = "period")
                     period: XmlPeriod)

@Element(path = "satellites")
case class XmlSatellite(@Attribute(path = "id", namespace = "http://www.omg.org/XMI")
                        override val id: String,
                        @Attribute(path = "band") @Converter(value = classOf[BandConverter])
                        band: Band,
                        @Attribute(path = "family")
                        typeId: String,
                        @Attribute(path = "allowJamming")
                        allowJamming: Boolean,
                        @Attribute(path = "reservePhysicVisibilityHole")
                        reservePhysicVisibilityHole: Boolean,
                        @Attribute(path = "reserveRFVisibilityHole")
                        reserveRFVisibilityHole: Boolean,
                        @Element(path = "validityPeriod")
                        validity: Seq[XmlPeriod],  //Seq because it might be missing (treat as an Option)
                        @Element(path = "constraintsCSatType1")
                        sat1: Seq[XmlSat1Constraint],
                        @Element(path = "constraintsCSatType4")
                        sat4: Seq[XmlSat4Constraint],
                        // This is only for export
                        @Attribute(path = "name")
                        name: String
                       ) extends XmlModelObject(id)

@Element(path = "stations")
case class XmlStation(@Attribute(path = "id", namespace = "http://www.omg.org/XMI")
                      override val id: String,
                      @Element(path = "validityPeriod")
                      validity: Seq[XmlPeriod], //Seq because it might be missing (treat as an Option)
                      @Element( path = "mapWorkingPeriods")
                      workingPeriods: Seq[XmlWorkingPeriod],
                      @Element(path = "satFamilyProperties")
                      satelliteProperties: Seq[XmlSatelliteProperties],
                      @Element(path = "constraintsCStaType2")
                      sta2: Seq[XmlSta2Constraint],
                      // This is only for export
                      @Attribute(path = "name")
                      name: String
                     ) extends XmlModelObject(id)

@Element(path = "visibilities")
case class XmlVisibility(@Attribute(path = "id", namespace = "http://www.omg.org/XMI")
                         override val id: String,
                         @Attribute(path = "frequencyBand") @Converter(value = classOf[BandConverter])
                         band: Band,
                         @Attribute(path = "station")
                         stationId: String,
                         @Attribute(path = "satellite")
                         satelliteId: String,
                         @Attribute(path = "phyHoleTotalDuration")
                         holeDuration: Double,
                         @Element(path = "aosLos")
                         aosLos: XmlAosLos,
                         @Element(path = "otherAosLos")
                         otherAosLos: Seq[XmlAosLos],
                         // This is only for export
                         @Attribute(path = "physHole")
                         phyHole: Boolean,
                         @Attribute(path = "rfHole")
                         rfHole: Boolean,
                         @Attribute(path = "jammed")
                         jammed: Boolean) extends XmlModelObject(id)

case class XmlDay(@Attribute(path = "")@Converter(value = classOf[DayConverter])
                  weekDay: DayOfWeek)

case class XmlSat1Constraint(@Attribute(path = "identifier")
                             override val id: String,
                             @Attribute(path = "freqBand") @Converter(value = classOf[BandConverter])
                             band: Band,
                             @Element(path = "weekDays")
                             weekDays: Seq[XmlDay],
                             @Attribute(path = "specificSchedule") @Converter(value = classOf[SpecificScheduleConverter])
                             specificSchedule: SpecificSchedule,
                             @Element(path = "validityPeriod")
                             validity: XmlPeriod,
                             @Attribute(path = "minDuration")
                             minDuration: Int,
                             @Attribute(path = "flightpass \\ min")
                             minPass: Int,
                             @Attribute(path = "flightpass \\ max")
                             maxPass: Int,
                             @Attribute(path = "useGreatestFlightPass")
                             mostPass: Boolean,
                             @Attribute(path = "useGreatestDuration")
                             longestPass: Boolean,
                             @Attribute(path = "stations")
                             stations: Option[String],
                             @Element(path = "slotPeriods")
                             slots: Seq[XmlSlot],
                             @Attribute(path = "period")
                             period: Int,
                             @Attribute(path = "firstApplication") @Converter(value = classOf[OptionZonedDateTimeConverter])
                             firstApplication: Option[ZonedDateTime],
                             @Attribute(path = "typeSet") @Converter(value = classOf[SatCstrTypeConverter])
                             typeSet: SatCstrType,
                             // This is only for export
                             @Attribute(path = "slotType")
                             slotTypeId: String,
                             @Attribute(path = "slotCategory")
                             slotCategoryId: String) extends XmlModelObject(id)

case class XmlSat4Constraint(@Attribute(path = "identifier")
                             override val id: String,
                             @Attribute(path = "freqBand") @Converter(value = classOf[BandConverter])
                             band: Band,
                             @Element(path = "validityPeriod")
                             validity: XmlPeriod,
                             @Element(path = "stationProperties")
                             stationProperties: Seq[XmlStationProperties],
                             @Attribute(path = "consecutivePass")
                             consecutivePass: Boolean,
                             @Attribute(path = "typeSet") @Converter(value = classOf[SatCstrTypeConverter])
                             typeSet: SatCstrType) extends XmlModelObject(id)

case class XmlSta2Constraint(@Attribute(path = "identifier")
                             override val id: String,
                             @Element(path = "validityPeriod")
                             validity: XmlPeriod,
                             @Attribute(path = "minDuration")
                             minDuration: Int, //read duration in minutes
                             @Attribute(path = "slotPeriod \\ begin") @Converter(value = classOf[LocalTimeWithZoneConverter])
                             start: LocalTime,
                             @Attribute(path = "slotPeriod \\ end") @Converter(value = classOf[LocalTimeWithZoneConverter])
                             end: LocalTime,
                             // This is only for export
                             @Attribute(path = "slotType")
                             slotTypeId: String,
                             @Attribute(path = "slotCategory")
                             slotCategoryId: String,
                             @Attribute(path = "typeSet") @Converter(value = classOf[SatCstrTypeConverter])
                             typeSet: SatCstrType) extends XmlModelObject(id)

// no ids !
@Element(path = "satelliteSlots")
case class XmlSatelliteSlot(@Attribute(path = "visibility")
                            visibilityId: String,
                            @Attribute(path = "state") @Converter(value = classOf[StatusConverter])
                            state: Status,
                            @Attribute(path = "extraPass")
                            extraPass: Boolean,
                            // This is only for export
                            @Attribute(path = "slotType")
                            slotTypeId: Option[String], //Empty for visibilities but not for reservations
                            @Attribute(path = "slotCategory")
                            slotCategoryId: Option[String],  //Empty for visibilities but not for reservations
                            @Attribute(path = "comment")
                            comment: Option[String],
                            @Attribute(path = "slotTrace") @Converter(value = classOf[StringCleaner])
                            slotTrace: Option[String])

@Element(path = "stationSlots")
case class XmlStationSlot(@Attribute(path = "station")
                          stationId: String,
                          @Element(path = "startEnd")
                          period: XmlPeriod)

@Element(path = "slotTypes")
case class XmlSlotType(@Attribute(path = "id", namespace = "http://www.omg.org/XMI")
                       override val id: String,
                       @Attribute(path = "slotType")
                       name: String) extends XmlModelObject(id)

@Element(path = "slotCategory")
case class XmlSlotCategory(@Attribute(path = "id", namespace = "http://www.omg.org/XMI")
                           override val id: String,
                           @Attribute(path = "name")
                           name: String) extends XmlModelObject(id)


case class XmlConstraintType(@Attribute(path = "") @Converter(value = classOf[ConstraintTypeConverter])
                             constraintType: ConstraintType)

@Element(path = "ComputationParameters")
case class XmlParams(@Element(path = "horizon")
                     period: XmlPeriod,
                     @Attribute(path = "timeout")
                     timeLimit: Int,
                     @Element(path = "satellites")
                     satellites: Seq[XmlSatelliteParam],
                     @Element(path = "stations")
                     stations: Seq[XmlSatelliteParam],
                     @Element(path = "chosenConstraintTypes")
                     constraintTypes: Seq[XmlConstraintType])

case class XmlSatelliteParam(@Attribute(path = "rsrc")
                             satelliteId: String,
                             @Attribute(path = "used")
                             used: Boolean)
case class XmlStationParam(@Attribute(path = "rsrc")
                           stationId: String,
                           @Attribute(path = "used")
                           used: Boolean)
