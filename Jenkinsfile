/**
 * Extracts the variables from the env.VARIABLES json value (that comes from the json config file in the pipeline).
 */
private void readConfigFile() {
    def variables = readJSON file: env.VARIABLES

    env.NEXUS_URL = variables["nexusUrl"]
    env.SONAR_URL = variables["sonarUrl"]
    env.DOCKER_REGISTRY_URL = variables["dockerRegistry"]
    env.BITBUCKET_URL = variables["bitbucketUrl"]

    echo "Extracting project variables : [NEXUS_URL=${env.NEXUS_URL}], [SONAR_URL=${env.SONAR_URL}], [DOCKER_REGISTRY_URL=${env.DOCKER_REGISTRY_URL}]"
}

/**
 * Extracts the project.version value from the build.gradle file.
 */
private void getProjectVersion() {
    def buildGradle = readFile 'build.gradle'

    def match = (buildGradle =~ /(?m)^\s*(project\.)?version[\s=]+['\"](.*)['\"]/)
    if (match.find()) {
        env.PROJECT_VERSION = match.group(2)
    } else {
        env.PROJECT_VERSION = env.PROJECT_DEFAULT_VERSION
    }

    echo "Extracting project version : ${env.PROJECT_VERSION}"
}

/**
 * Extracts the branch id from the current pull request.
 * @return The found branch id, null if none
 */
private String getBranchId() {
    // The httpRequest jenkins plugin fails when using the = character in the URL...
    def curl = sh(returnStdout: true, script: "curl -u $BITBUCKET_USER_USR:$BITBUCKET_USER_PSW $BITBUCKET_URL/cnesocp/pullrequests?q=source.branch.name=\\\"${env.BRANCH_NAME}\\\"")

    def content = readJSON text: curl
    def values = content["values"]

    if (values.size() > 0) {
        return values[0]["id"]
    } else {
        return null
    }
}

/**
 * Sends the given message as a comment in the bitbucket current pull request.
 * @param message
 */
private void sendBitbucketComment(String message) {
    def branchId = getBranchId()

    if (branchId != null){
        httpRequest authentication: 'BITBUCKET_USER', contentType: 'APPLICATION_JSON', url: "$BITBUCKET_URL/cnesocp/pullrequests/$branchId/comments", httpMode: 'POST', requestBody: "{\"content\": {\"raw\" : \"$message\"}}"
    }
}

/**
 * Sends the current build status as a badge inside a comment to the bitbucket pull request.
 */
private void sendBuildStatus(){
    def color = currentBuild.currentResult == 'SUCCESS' ? 'green' : 'red'

    sendBitbucketComment("![${currentBuild.currentResult}](https://img.shields.io/badge/Jenkins-${currentBuild.currentResult}-${color}.svg) : [Build ${currentBuild.displayName}](${currentBuild.absoluteUrl})")
}

pipeline {
    // The agent must be set to none because some steps run in docker, some not
    agent none

    // Poll the project repository every 2 minutes
    triggers {
        pollSCM 'H/2 * * * *'
    }

    options {
        // Number of builds to keep
        buildDiscarder(logRotator(numToKeepStr: '15', artifactNumToKeepStr: '15'))

        disableConcurrentBuilds()
    }

    environment {
        NEXUS_USER = credentials('NEXUS_USER')
        BITBUCKET_USER = credentials('BITBUCKET_USER')
        PROJECT_DEFAULT_VERSION = "UNDEFINED"
    }

    stages {
        stage('Config file reading') {
            agent any

            steps {
                // Reads the variables-config file and extract the variables
                configFileProvider([configFile(fileId: 'variables-config', variable: 'VARIABLES')]) {
                    script {
                        readConfigFile()
                    }
                }
            }
        }

        stage('checkout') {
            when { anyOf {branch 'master'; branch 'develop' } }
            agent any
            steps {
                echo "Pulling : " + BRANCH_NAME
                checkout scm
                script {
                    getProjectVersion()
                }
            }
        }

        stage('Simple build') {
            when { anyOf {branch 'master'; branch 'develop' } }
            agent {
              docker{
                  reuseNode true
                  image 'docker-registry.decisionbrain.loc/cplex-studio-java:12.9-jdk8-1.0.0'
                  // Needed to work inside the docker environment and get access to the nexus.loc
                  args "--network=dockernet254"
                  registryUrl 'https://docker-registry.decisionbrain.loc'
                  registryCredentialsId 'NEXUS_USER'
              }
            }

            steps {
                sh " chmod ugo+x ./gradlew"
                sh " chmod -R ugo+rx ./gradle"
                sh "./gradlew clean build -P cplexStudioHome=/home/decisionbrain/cplex_studio -P NEXUS_URL=$NEXUS_URL -P MAVEN_USER=$NEXUS_USER_USR -P MAVEN_PASSWORD=$NEXUS_USER_PSW"
            }
        }

    }

    post {
        always {
            node('vm-jenkins1') {
                //junit testResults: '**/build/test-results/test/*.xml', allowEmptyResults: true

                script {
                    sendBuildStatus()
                }
            }
        }
        failure {
            emailext (
               subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                to: "'guillaume.vantroeyen@decisionbrain.com' , 'david.gravot@decisionbrain.com'",
                body: """FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]': Check console output at ${env.BUILD_URL}'${env.JOB_NAME} [${env.BUILD_NUMBER}]""",
                recipientProviders: [[$class: 'DevelopersRecipientProvider']]
            )
        }
    }
}
