/*********************************************

 helper for CNES project
 see https://www.ibm.com/developerworks/community/forums/html/topic?id=fcb4d76e-6c0e-46f2-a091-2140777cdc8c#repliesPg=0
 *********************************************/

using CP;

//initial data
tuple Slot{	int start;	int end;	int station;}
{Slot} slots = { 	<10,14,1>,	<12,15,1>,	<13,18,2>};

//transitions 	
{int} stations = { s.station | s in slots};
tuple Transition { int station1 ; int station2 ; int duration;};
{Transition} transitions = { <s1, s2, (s1==s2?10000 : 0)> | s1 in stations, s2 in stations};	
	
dvar interval slotsItv[s in slots]  optional in s.start .. s.end size (s.end-s.start);
dvar interval startSlots[s in slots] optional in s.start .. (s.start+1) size 1;

dvar sequence stationsSeq in all(s in slots) startSlots[s] types all(s in slots) s.station;

maximize sum(s in slots) presenceOf(slotsItv[s]);

constraints{
	noOverlap(stationsSeq,transitions);
	forall(s in slots)
	  presenceOf(slotsItv[s]) == presenceOf(startSlots[s]);
} 