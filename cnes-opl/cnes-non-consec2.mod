/*********************************************

 helper for CNES project
 see https://www.ibm.com/developerworks/community/forums/html/topic?id=fcb4d76e-6c0e-46f2-a091-2140777cdc8c#repliesPg=0
 *********************************************/

using CP;

//initial data
tuple Slot{	int start;	int end;	int station;}
{Slot} slots = { 	<10,14,1>,	<12,15,1>,	<13,18,2>};

dvar interval itv[s in slots]  optional in s.start .. s.end size (s.end-s.start);
dvar sequence stationsSeq in all(s in slots) itv[s] types all(s in slots) s.station;


maximize sum(s in slots) presenceOf(itv[s]);

constraints{
	forall(s in slots)
	  startOf(itv[s]) <=  startOfNext(stationsSeq, itv[s], s.start);
	  
	forall(s in slots)
	  typeOfNext(stationsSeq, itv[s], s.station+1, s.station+1) != s.station;
} 