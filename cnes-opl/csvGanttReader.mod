/*********************************************
 * OPL 12.8.0.0 Model
 * Author: dgravot
 * Creation Date: May 13, 2019 at 4:30:20 PM
 *********************************************/

using CP;

execute {
	settings.bigMapThreshold = 100000;
}

 //SAT	 STA	 START	 END	 AOS_O	 LOS_0	 AOS_CS4	 LOS_CS4	 Freq	 Type de Support	 Categorie	 Etat	 trou physique	 trou RF	 brouillage	 DUREE_UTIL	 Commentaire	 Info Ocp
  string csvPath = ...;

 tuple Reservation{ key string id; string satellite ; string station; int stationStart ; int stationEnd; int aosCs4 ; int losCs4;}
 tuple Maintenance{ string station; int start; int end;}

{Reservation} reservations0;
{Maintenance} maintenances0;
execute LOAD_RESERVATIONS_AND_MAINTENANCES
{
	var origin = new Date("01/09/2018 00:00:00");
	var nbRecords = 0;
	
	function convertToLocalTime(str){
		
		var sDay = str.substring(0,2);
		var sMonth = str.substring(3,5);	
		var sYear = str.substring(6,10);		
		var postFix=str.substring(10);
		//writeln("Origin date = ", str, "sDay=",sDay, " sMonth "= sMonth," sYear=",sYear);
		var result = sMonth + "/" + sDay + "/" + sYear+postFix;
		var aDate = new Date(result);
		var result = Opl.ftoi((aDate.getTime() - origin.getTime()) / 1000);
		//writeln(str,"->",result);
		return result; 
	}

	var f=new IloOplInputFile(csvPath);
	var str=f.readline();//first line 
	while (!f.eof){
		nbRecords++;	
		var str=f.readline();
		if(str.length==0) break;//last line may be empty
		writeln(nbRecords," ",str);
		var ar=str.split(",");
		if(ar[11] == "AVAILABLE") continue;//we keep only the reserved one (including the one computed by optimization)
		var satellite = ar[0];										//satellite
		var station = ar[1];										//station
		var stationStart = convertToLocalTime(ar[2]);				//stationStart (aos0) - conf
		var stationEnd = convertToLocalTime(ar[3]);					//stationEnd (los0) + deconf
		
			
		if(ar[0].substring(0,7)=="MAINTEN"){
			maintenances0.add(station, stationStart, stationEnd)	;
		} else{
			var visibility = ar[18];
			var initialState = ar[19];
			var inSat1 = ar[20];
			if(inSat1.length >0){//tmp to filter out all reservations or candidates not in sat1
				var aos = convertToLocalTime(ar[6]);						//aos according to SAT4 elevation
				var los = convertToLocalTime(ar[7]);						//los according to SAT4 elevation
				if(stationStart >= stationEnd || aos >= los ){
					writeln("Skipping entry due to ERROR in date ordering  ", str);
				} else{
					var id = satellite + "___" + station + "___" + visibility+"___"+initialState;
					writeln(nbRecords,":",id);
					reservations0.add(id,satellite, station, stationStart, stationEnd, aos, los);			
				}
			}
			
							
		}
	}
	f.close();
	writeln(nbRecords,"\trecords in total (available slots + reserved ones + maintenance slots)");
	writeln(Opl.card(reservations0),"\treservations");
	writeln(Opl.card(maintenances0),"\tmaintenances");
}
int offset = minl( min(r in reservations0) r.stationStart , min(m in maintenances0) m.start);

{Reservation} reservations = { <r.id, r.satellite,r.station,r.stationStart-offset,r.stationEnd-offset,r.aosCs4-offset,r.losCs4-offset> | r in reservations0};
{Maintenance} maintenances = { <m.station, m.start-offset, m.end-offset> | m in maintenances0};

assert forall(r in reservations) errSizeStationSlot : r.stationEnd > r.stationStart;
assert forall(r in reservations) errSizeSatSlot : r.losCs4 > r.aosCs4;


{string} satellites = { r.satellite | r in reservations};
{string} stations = { r.station | r in reservations} union {m.station | m in maintenances};


{Reservation} bySat[sat in satellites] = { r | r in reservations : r.satellite == sat};

dvar interval stationSlot[r in reservations]  in r.stationStart .. r.stationEnd size (r.stationEnd - r.stationStart);
dvar interval satelliteSlot[r in reservations]  in r.aosCs4 .. r.losCs4 size (r.losCs4 - r.aosCs4);  
dvar interval maintenanceSlot[m in maintenances]   in m.start .. m.end size (m.end - m.start);

dvar sequence staSequence[sta in stations] in append( 
	all(r in reservations : r.station == sta) stationSlot[r], 
	all(m in maintenances : m.station==sta) maintenanceSlot[m] );
dvar sequence satSequence[sat in satellites] in all(r in reservations : r.satellite == sat) satelliteSlot[r];

constraints{
	forall(sta in stations)
	  noOverlap(staSequence[sta]);
	  
	forall(sat in satellites, r1 in bySat[sat], r2 in bySat[sat] : r1.aosCs4 < r2.aosCs4)
	  before(satSequence[sat], satelliteSlot[r1], satelliteSlot[r2]);
}

//C:\sources\cnesocp\src\test\resources\out\output3.csv